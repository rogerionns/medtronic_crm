<%@page import="br.com.plusoft.fw.util.Tools"%>
<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.fw.constantes.PlusoftSystemProperty"%>
<%@page import="br.com.plusoft.csi.adm.action.generic.PluginThread"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashSet"%>
<%@page import="br.com.plusoft.csi.adm.action.generic.Empresas"%>
<%@page import="br.com.plusoft.fw.log.*"%>

<%
	if(request.getParameter("acao") != null){
		if(request.getParameter("acao").equals("parar")){		
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - PARAR THREAD] - INICIO");
			Empresas.pararPlugins();
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - PARAR THREAD] - FIM");
		}else if(request.getParameter("acao").equals("iniciar")){
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - INICIAR THREAD] - INICIO");
			Empresas.iniciarPlugins();
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - INICIAR THREAD] - FIM");
		}else if(request.getParameter("acao").equals("recarregarEmpresas")){
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - RECARREGAR EMPRESAS] - INICIO");
			Empresas.pararThread();
			Empresas.carregaEmpresas();
			Empresas.recarregarEmpresasPlugin();
			Empresas.iniciarPlugins();
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - RECARREGAR EMPRESAS] - FIM");
			%>
				<BR> Empresas carregadas...
			<%
		}
		else if(request.getParameter("acao").equals("recarregarEmpresa")){
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - RECARREGAR EMPRESA] - INICIO");
			String empresa = request.getParameter("idEmpresa");
			Empresas.pararPlugins();
			Empresas.carregaEmpresa(empresa);
			Empresas.recarregarEmpresasPlugin();
			Empresas.iniciarPlugins();
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - RECARREGAR EMPRESA] - FIM");
			%>
				<BR> Empresa carregada...
			<%
		}
		else if(request.getParameter("acao").equals("criar")){
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - CRIAR THREAD] - INICIO");
			Empresas.iniciarThread(this.getServletConfig());
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - CRIAR THREAD] - FIM");
		}
		else if(request.getParameter("acao").equals("destruir")){
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - DESTRUIR THREAD] - INICIO");
			Empresas.pararThread();
			Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - DESTRUIR THREAD] - FIM");
		}
	}

	%>
	Server Ip: <%=InetAddress.getByName("localhost").getHostAddress()%>
	<BR>
	<BR>
	<%


	HashSet plugins = Empresas.pluginsThread;
	Thread.currentThread().sleep(2000);
	
	%>
		Plugins:
	<%
	
	Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - LOOP THREADS] - SIZE: " + plugins.size());
	for(Iterator it = plugins.iterator(); it.hasNext() ; ){
		PluginThread pThread = (PluginThread)it.next();
		Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - LOOP EMPRESAS]" + pThread.getName());
		%>
			<BR>
			<BR>
			Plugin : <%=pThread.config.getServletContext().getServletContextName() %>
			<BR>
			Status:  <%=pThread.isStarted==true?"Executando":"Parado"%>
		<%
	}
	
	%>
		<BR>
		<BR>
		<BR>
			Empresas.
		<BR>
	<%
	Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - LOOP EMPRESAS] - SIZE: " + Empresas.empresas.size());
	for(Iterator it = Empresas.empresas.iterator(); it.hasNext() ; ){
		CsCdtbEmpresaEmprVo empresa = (CsCdtbEmpresaEmprVo)it.next();
		Log.log(this.getClass(), Log.INFOPLUS, "[PLUGIN JSP - LOOP EMPRESAS]" + empresa.getEmprDsEmpresa());
		
		//Chamado Riachuelo - 04/07/2013 - Vinicius
		//trecho criado para usar a propriedade definida no system do servidor de aplicação como link do plugin
		try{
			if(System.getProperty(PlusoftSystemProperty.PLUSOFT_LINK_PLUGIN) != null){
				if(empresa.getEmprDsLinkPlugin().indexOf(Constantes.CONFIGURACOES_LINK_PLUGIN) > -1){
					empresa.setEmprDsLinkPlugin(Tools.strReplace(Constantes.CONFIGURACOES_LINK_PLUGIN, System.getProperty(PlusoftSystemProperty.PLUSOFT_LINK_PLUGIN), empresa.getEmprDsLinkPlugin()));
				}
			}
		}catch(Exception e){
			Log.log(this.getClass(), Log.ERRORPLUS, "ERRO AO PEGAR/REPLACE DA CONFIGURAÇÃO DO SISTEMA DO LINK PLUGIN:"+empresa.getEmprDsLinkPlugin());
			Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
		}
		
		%>
			<BR>
			<BR>
			Empresa : <%=empresa.getEmprDsEmpresa() %>
			<BR>
			Url Plugin : <%=empresa.getEmprDsLinkPlugin() %> 
		<%
	}
	
	
%>
<BR>
<BR>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.Socket"%>
<%@page import="java.net.InetAddress"%>
<html>

	<form action="plugin.jsp">
		<input type="hidden" value="criar" name="acao"/>
		<input type="submit" value="Criar Thread">
	</form>
	
	<form action="plugin.jsp">
		<input type="hidden" value="destruir" name="acao"/>
		<input type="submit" value="Destruir Thread">
	</form>
	
	<form action="plugin.jsp">
		<input type="hidden" value="parar" name="acao"/>
		<input type="submit" value="Parar Plugins">
	</form>
	
	<form action="plugin.jsp">
		<input type="hidden" value="iniciar" name="acao"/>
		<input type="submit" value="Iniciar Plugins">
	</form>
	
	<form action="plugin.jsp">
		<input type="hidden" value="recarregarEmpresas" name="acao"/>
		<input type="submit" value="Recarregar Todas as Empresas">
	</form>
	
	<form action="plugin.jsp">
		<input type="hidden" value="recarregarEmpresa" name="acao"/>
		<select name="idEmpresa">
			<%
			for(Iterator it = Empresas.empresas.iterator(); it.hasNext() ; ){
				CsCdtbEmpresaEmprVo empresa = (CsCdtbEmpresaEmprVo)it.next();
				
				//Chamado Riachuelo - 04/07/2013 - Vinicius
				//trecho criado para usar a propriedade definida no system do servidor de aplicação como link do plugin
				try{
					if(System.getProperty(PlusoftSystemProperty.PLUSOFT_LINK_PLUGIN) != null){
						if(empresa.getEmprDsLinkPlugin().indexOf(Constantes.CONFIGURACOES_LINK_PLUGIN) > -1){
							empresa.setEmprDsLinkPlugin(Tools.strReplace(Constantes.CONFIGURACOES_LINK_PLUGIN, System.getProperty(PlusoftSystemProperty.PLUSOFT_LINK_PLUGIN), empresa.getEmprDsLinkPlugin()));
						}
					}
				}catch(Exception e){
					Log.log(this.getClass(), Log.ERRORPLUS, "ERRO AO PEGAR/REPLACE DA CONFIGURAÇÃO DO SISTEMA DO LINK PLUGIN:"+empresa.getEmprDsLinkPlugin());
					Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
				}
				
				%>
					<option value="<%=empresa.getIdEmprCdEmpresa() %>"><%=empresa.getEmprDsEmpresa() %></option>
				<%
			}
			%>
		</select>
		<input type="submit" value="Recarregar Empresa Selecionada">
	</form>
</html>