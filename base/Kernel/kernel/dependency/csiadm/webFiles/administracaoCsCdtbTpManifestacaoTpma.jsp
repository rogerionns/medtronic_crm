<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");

	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	

	long idEmpresa = empresaVo.getIdEmprCdEmpresa();
	
	String maxRegistros = "";
	try{
		maxRegistros = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_LIMITE_LINHAS_RESULTADO,idEmpresa);
	}catch(Exception e){}
		
	if(maxRegistros == null || maxRegistros.equals("")){
		maxRegistros = "100";
	}
	
	//pagina��o****************************************
	long numRegTotal=0;
	if (request.getAttribute("csCdtbTpManifestacaoTpmaVector")!=null){
		Vector v = ((java.util.Vector)request.getAttribute("csCdtbTpManifestacaoTpmaVector"));
		if (v.size() > 0){
			numRegTotal = ((CsCdtbTpManifestacaoTpmaVo)v.get(0)).getNumRegTotal();
		}
	}

	long regDe=0;
	long regAte = 0;

	if (request.getParameter("regDe") != null)
		regDe = Long.parseLong((String)request.getParameter("regDe"));
	if (request.getParameter("regAte") != null)
		regAte  = Long.parseLong((String)request.getParameter("regAte"));
	//***************************************
	
%>

<%@page import="java.util.Vector"%><html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

	function setaDeAte(){
	
		if(parent.document.editCsCdtbTpManifestacaoTpmaForm.regDe.value == "0" && parent.document.editCsCdtbTpManifestacaoTpmaForm.regAte.value == "0"){			
			
			parent.document.editCsCdtbTpManifestacaoTpmaForm.regDe.value='<%=regDe%>';
			parent.document.editCsCdtbTpManifestacaoTpmaForm.regAte.value='<%=regAte%>';
			parent.initPaginacao();
		}	
	
	}
	
	function inicio(){
	
		parent.setPaginacao(<%=regDe%>,<%=regAte%>);
		parent.atualizaPaginacao(<%=numRegTotal%>);
			
	}

</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form styleId="editCsCdtbTpManifestacaoTpmaForm" action="/AdministracaoCsCdtbTpManifestacaoTpma.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idTpmaCdTpManifestacao"/>

<script>
	var possuiRegistros=false;
	var totalRegistros=0;
</script>

<div id="lstandamento" style="overflow: auto; width: 777px; height:325px; " onScroll="parent.barraTitulo.scrollLeft = this.scrollLeft">

<table width="1070" border="0" cellspacing="0" cellpadding="0" align="top">

  <logic:iterate id="ccttrtVector" name="csCdtbTpManifestacaoTpmaVector" indexId="sequencia"> 
  <script>
  	possuiRegistros=true;
  	totalRegistros=<%=sequencia%>;
  </script>
  <tr> 
    <td width="3%" class="principalLstPar"> <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />')"> 
    </td>

    <td width="2%" class="principalLstPar"> 
    	<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMATPMA_IDTM.xml','<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />')">
    </td>
    
    <td class="principalLstParMao" width="5%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />')"> 
      <div align="left">
        	&nbsp;<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" /> 
      </div>
    </td>
    
        <td class="principalLstParMao" align="left" width="15%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />')"> 
      <div align="left">
        	&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.matpDsManifTipo" />', 24);</script>            
      </div>
    </td>
    
    
    <td class="principalLstParMao" align="left" width="15%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />')"> 
      <div align="left">
        	&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="csCdtbGrupoManifestacaoGrmaVo.grmaDsGrupoManifestacao" />', 24);</script>            
      </div>
    </td>
    <td class="principalLstParMao" align="left" width="15%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />')"> 
      <div align="left">
        	&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="tpmaDsTpManifestacao" />', 25);</script>
      </div>
    </td>
    <td class="principalLstParMao" align="left" width="15%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />')">  
      <div align="left">
        	&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="csCdtbFuncionarioFuncVo.funcNmFuncionario" />', 18);</script>
      </div>
    </td>
    <td class="principalLstParMao" align="left" width="12%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />')">  
      <div align="left">
        	&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="csCdtbClassifmaniClmaVo.clmaDsClassifmanif" />', 20);</script>      
      </div>
    </td>
    <td class="principalLstParMao" align="left" width="6%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />')"> 
      <div align="center">
        	<bean:write name="ccttrtVector" property="tpmaNrDiasResolucao" />
        &nbsp; 	
	  </div>
    </td>
    <td class="principalLstParMao" align="center" width="9%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />')"> 
      <div align="center">
        &nbsp;<bean:write name="ccttrtVector" property="tpmaDhInativo" /></div>
    </td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr> 


</table>

</div>

<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_EXCLUSAO_CHAVE%>', editCsCdtbTpManifestacaoTpmaForm.lixeira);
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>