<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">


<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposConfiguracao(){
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.idConfCdConfiguracao"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.idGrcoCdGrupoconfig"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confDsConfiguracao"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confDsChave"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confInTipocampo"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confNrMaxlen"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confDsMascara"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confInTpcombo"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confDsOpcoescombo"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confDsValstring"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confNrValnumber"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confDhValdatetime"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.confDsFuncionalidade"].disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm.inInativo.disabled= true;
		document.administracaoCsDmtbConfiguracaoConfForm.inVisivel.disabled= true;		
	}	
</script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');desabilitaCamposConfiguracao();">

<html:form styleId="administracaoCsDmtbConfiguracaoConfForm" action="/AdministracaoCsDmtbConfiguracaoConf.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="csDmtbConfiguracaoConfVo.confDhInativo"/>
	
	<br>
	<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
        	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
        <td width="45%"> 
           	<html:text property="csDmtbConfiguracaoConfVo.idConfCdConfiguracao" styleClass="text" disabled="true" maxlength="10"/>
	    </td>
        <td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.CodigoGrupoConfig"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
		</td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.idGrcoCdGrupoconfig" styleClass="text" maxlength="10" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.descricaoConfiguracao"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confDsConfiguracao" styleClass="text" maxlength="255" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.descricaoChave"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confDsChave" styleClass="text" maxlength="60" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.tipoCampo"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confInTipocampo" styleClass="text" maxlength="1" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.maxLen"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confNrMaxlen" styleClass="text" maxlength="4" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.mascara"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confDsMascara" styleClass="text" maxlength="20" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.tipoCombo"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confInTpcombo" styleClass="text" maxlength="1" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.opcoesCombo"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confDsOpcoescombo" styleClass="text" maxlength="1000" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.valString"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confDsValstring" styleClass="text" maxlength="1000" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.numero"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confNrValnumber" styleClass="text" maxlength="10"/> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.data"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confDhValdatetime" styleClass="text" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/> 
		</td>
		<td width="15%"><!--img src="webFiles/images/botoes/calendar.gif" name="imgCalendario" width="16" height="15" onclick="show_calendar('administracaoCsDmtbConfiguracaoConfForm[\'csDmtbConfiguracaoConfVo.confDhValdatetime\']')" class="principalLstParMao" --></td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.funcionalidade"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csDmtbConfiguracaoConfVo.confDsFuncionalidade" styleClass="text" maxlength="40" /> 
		</td>
		<td width="31%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%">&nbsp;</td>
		<td colspan="3">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%">&nbsp;</td>
		<td width="45%">&nbsp;</td>
		<td class="principalLabel" width="15%"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
				<td align="right" width="50%"> 
					<html:checkbox value="true" property="inInativo"/><!-- @@ --></td>
            	<td class="principalLabel" width="50%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
			</tr>
            </table>
		</td>
		<td width="31%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%">&nbsp;</td>
		<td width="45%">&nbsp;</td>
		<td class="principalLabel" width="15%"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
				<td align="right" width="50%"> 
					<html:checkbox value="true" property="inVisivel"/><!-- @@ --></td>
            	<td class="principalLabel" width="50%">&nbsp;<bean:message key="prompt.visivel"/><!-- ## --> 
                </td>
			</tr>
            </table>
		</td>
		<td width="55%">&nbsp;</td>
	</tr>
	</table>

</html:form>
</body>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposConfiguracao();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_CONFIGURACAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsDmtbConfiguracaoConfForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_CONFIGURACAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposConfiguracao();
			}else{
				//document.administracaoCsDmtbConfiguracaoConfForm.idEstadoCivil.disabled= false;
				//document.administracaoCsDmtbConfiguracaoConfForm.idEstadoCivil.value= '';
				//document.administracaoCsDmtbConfiguracaoConfForm.idEstadoCivil.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_CONFIGURACAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_CONFIGURACAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsDmtbConfiguracaoConfForm.imgGravar);	
				desabilitaCamposConfiguracao();
			}
		</script>
</logic:equal>

</html>

<script>
	//try{document.administracaoCsDmtbConfiguracaoConfForm.dsEstadoCivil.focus();}
	//catch(e){}
</script>