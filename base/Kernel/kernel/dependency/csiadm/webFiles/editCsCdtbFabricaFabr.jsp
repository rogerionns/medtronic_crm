<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbFabricaFabrForm.idFabrCdFabrica.value);
	}
</script>	
	
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbFabricaFabrForm" action="/AdministracaoCsCdtbFabricaFabr.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> 
      <!-- ## -->
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="10%"> <html:text property="idFabrCdFabrica" styleClass="text" disabled="true" /> 
    </td>
    <td width="28%">&nbsp;</td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/>
      <!-- ## -->
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="fabrDsFabrica" styleClass="text" maxlength="60" /> 
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td width="10%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="19%"> <html:checkbox value="true" property="fabrInLaboratorio"/> 
          </td>
          <td class="principalLabel" width="81%"> 
            <div align="left"><%=getMessage("prompt.Laboratorio",request)%> </div><!-- Chamado: 92969 - 11/02/2014 - Carlos Nunes -->
          </td>
        </tr>
        <tr> 
          <td align="right" width="19%"><html:checkbox value="true" property="fabrDhInativo"/> 
          </td>
          <td class="principalLabel" width="81%"> 
            <div align="left">&nbsp;<bean:message key="prompt.inativo"/> </div>
          </td>
        </tr>
      </table>
    </td>
    <td class="principalLabel" width="28%">&nbsp; </td>
    <td width="31%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbFabricaFabrForm.fabrDsFabrica.disabled= true;	
			document.administracaoCsCdtbFabricaFabrForm.fabrDhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbFabricaFabrForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_INCLUSAO_CHAVE%>')){
				document.administracaoCsCdtbFabricaFabrForm.fabrDsFabrica.disabled= true;	
				document.administracaoCsCdtbFabricaFabrForm.fabrInLaboratorio.disabled= true;
				document.administracaoCsCdtbFabricaFabrForm.fabrDhInativo.disabled= true;
			}			
			document.administracaoCsCdtbFabricaFabrForm.idFabrCdFabrica.disabled= false;
			document.administracaoCsCdtbFabricaFabrForm.idFabrCdFabrica.value= '';
			document.administracaoCsCdtbFabricaFabrForm.idFabrCdFabrica.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_ALTERACAO_CHAVE%>', parent.administracaoCsCdtbAreaAreaForm.imgGravar);	
	
				document.administracaoCsCdtbFabricaFabrForm.fabrDsFabrica.disabled= true;
				document.administracaoCsCdtbFabricaFabrForm.fabrInLaboratorio.disabled= true;
				document.administracaoCsCdtbFabricaFabrForm.fabrDhInativo.disabled= true;
			}
		</script>
</logic:equal>

</html>