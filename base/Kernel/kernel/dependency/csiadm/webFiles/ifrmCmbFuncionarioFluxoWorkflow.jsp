<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
<!--ifrmCmbFuncionarioFluxoWorkflow.jsp-->
</script>
</head>

<script language="JavaScript">
</script>

<body class="principalBgrPageIFRM" text="#000000" scroll="no">
<html:form styleId="fluxoWorkflowForm" action="/CarregaCmbFuncionarioFluxoWorkflow.do">
<html:select property="id_func_cd_funcionario" styleClass="principalObjForm"  > 
	<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
	<logic:present name="csCdtbFuncionarioFuncVector">
		<html:options collection="csCdtbFuncionarioFuncVector" property="field(id_func_cd_funcionario)" labelProperty="field(func_nm_funcionario)"/>
	</logic:present> 
</html:select>
</html:form>
</body>
</html>