<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>
function atualizaGrupo() {
	parent.document.administracaoCsCdtbEventoFollowUpEvfuForm.idSugrCdSupergrupo.value = administracaoCsCdtbEventoFollowUpEvfuForm.idSugrCdSupergrupo.value;
	
	document.administracaoCsCdtbEventoFollowUpEvfuForm.target = parent.ifrmCmbGrupomanifestacao.name;
	document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value = '<%= MAConstantes.TELA_CMB_GRUPOMANIFESTACAO %>';
	document.administracaoCsCdtbEventoFollowUpEvfuForm.submit();
}

function inicio(){
	if(parent.document.administracaoCsCdtbEventoFollowUpEvfuForm.todosSuperGrupos.checked){
		administracaoCsCdtbEventoFollowUpEvfuForm.idSugrCdSupergrupo.selectedIndex = 0;
		administracaoCsCdtbEventoFollowUpEvfuForm.idSugrCdSupergrupo.disabled = true;
	} else {
		if(administracaoCsCdtbEventoFollowUpEvfuForm.idSugrCdSupergrupo.length == 2)
			administracaoCsCdtbEventoFollowUpEvfuForm.idSugrCdSupergrupo.selectedIndex = 1;
	}

	atualizaGrupo();
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" leftmargin=0 bottommargin=0 topmargin=0 rightmargin=0>

<html:form styleId="administracaoCsCdtbEventoFollowUpEvfuForm" action="/AdministracaoCsCdtbEventoFollowUpEvfu.do">

	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
	<html:hidden property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />
	<html:hidden property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />
		
	<html:select property="idSugrCdSupergrupo" styleClass="principalObjForm" onchange="atualizaGrupo();">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		<html:options collection="combo" property="idSugrCdSupergrupo" labelProperty="sugrDsSupergrupo"/>
	</html:select>
</html:form>
</body>
</html>
