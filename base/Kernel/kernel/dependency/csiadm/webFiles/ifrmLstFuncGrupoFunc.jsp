<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

function moveLineTr(tudo, origem, destino){
	var mensagem = false;
	for (var i = 0; i <= administracaoCsCdtbGrupopreatendGrprForm.qtdFunc.value; i++){
		
		if (document.getElementById("chk" + origem + i).checked){
			
			//Caso a origem seja os funcionarios selecionados					
			if (origem == 'FuncSel'){
				document.getElementById("txtFuncMesa" + i).setAttribute("excluido","S");
			}else{
				document.getElementById("txtFuncMesa" + i).setAttribute("excluido","N");
			}
				
			document.getElementById("tr" + origem + i).style.display = "none";
			document.getElementById("chk" + origem + i).checked = false;
			
			document.getElementById("tr" + destino + i).style.display = "block";
			document.getElementById("chk" + destino + i).checked = false;
		}
	}
	
	if (administracaoCsCdtbGrupopreatendGrprForm.todos.checked)
		administracaoCsCdtbGrupopreatendGrprForm.todos.checked = false;

	if (administracaoCsCdtbGrupopreatendGrprForm.todosGrupo.checked)
		administracaoCsCdtbGrupopreatendGrprForm.todosGrupo.checked = false;
		
	
	if (mensagem){
		
		alert("<bean:message key="prompt.campanha.operador.alert.Nao_foram_movidos_todos_os_operadores_pois_os_mesmos_ja_foram_ativados_anteriormente"/>");
		
	}
	
}

function marcaTodos(objPai, objFilho){
	for (var i = 0; i <= document.administracaoCsCdtbGrupopreatendGrprForm.qtdFunc.value; i++){
		document.getElementById(objFilho + i).checked =	objPai.checked;
	}
}
	
</script>
<body class= "principalBgrPageIFRM" >
<html:form styleId="administracaoCsCdtbGrupopreatendGrprForm" action="/AdministracaoCsCdtbGrupopreatendGrpr.do">
<input type="hidden" name="qtdFunc"/>

<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="48%" class="principalLabel">
			<input name="todos" type="checkbox" onclick="javascript: marcaTodos(this, 'chkFuncDisp');">&nbsp;<bean:message key="prompt.todos"/>		
		</td>	
		<td width="4%" class="principalLabel">&nbsp;</td>
		<td width="48%" class="principalLabel">
			<input name="todosGrupo" type="checkbox" onclick="javascript: marcaTodos(this, 'chkFuncSel');">&nbsp;<bean:message key="prompt.todos"/>	
		</td>	
	</tr>	
  <tr>          	
	<td width="48%">
		<div style="
			width: 310px;
			height: 106px;
			border-top: #CDCDCD 1px solid;
			border-left: #CDCDCD 1px solid;
			border-right: #CDCDCD 1px solid;
			border-bottom: #CDCDCD 1px solid;
			background-color: #FFFFFF;
			overflow: auto">
			
			<table width="100%" cellpadding=0 cellspacing=0 border=0>
				 <logic:present name="funcionariosVector">
					  <logic:iterate id="funcDispVector" name="funcionariosVector" indexId="numero">
							<script>
								administracaoCsCdtbGrupopreatendGrprForm.qtdFunc.value="<%= numero%>";
							</script>
							
							<tr colspan="2" id="trFuncDisp<%= numero%>" 
								idfunc="<bean:write name="funcDispVector" property="idFuncCdFuncionario" />"
								idArea="<bean:write name="funcDispVector" property="csCdtbAreaAreaVo.idAreaCdArea" />"  
								name="trFuncDispName<bean:write name="funcDispVector" property="idFuncCdFuncionario" />"
								suprimido="N" 
								insercao=""
								ativacao=""
								status=""
								nmFunc="<bean:write name="funcDispVector" property="funcNmFuncionario" />"
								style="display: none">

								<td class="principalLstPar" width=5><input name="chkOperadorSelecionado" type="checkbox" id="chkFuncDisp<%= numero %>"></td>
								<td class="principalLstPar"><bean:write name="funcDispVector" property="funcNmFuncionario" /></td>
							</tr>
						
				 		</logic:iterate>
				 </logic:present>
			</table>
		</div>          	
	  </td>	          	
  	
	 

	  <td width="4%" align="left">
	  		<table>
	  			<tr>
	  				<td><img src="webFiles/images/botoes/avancar_unico.gif" id="imgAvancar" width=21 height=18 title="<bean:message key="prompt.adicionaraogrupo"/>" class=geralCursoHand onclick="javascript:moveLineTr(false, 'FuncDisp', 'FuncSel')"></td>
	  			</tr>
	  			<!--
	  			<tr>
	  				<td><img src="webFiles/images/botoes/setaRight.gif" width=21 height=18 class=geralCursoHand onclick="javascript:moveLineTr(true, 'FuncDisp', 'FuncSel')"></td>
	  			</tr>
	  			<tr>
	  				<td><img src="webFiles/images/botoes/setaLeft.gif" width=21 height=18 class=geralCursoHand onclick="javascript:moveLineTr(true, 'FuncSel', 'FuncDisp')"></td>
	  			</tr>
	  			-->
	  			<tr>
	  				<td><img src="webFiles/images/botoes/voltar_unico.gif" id="imgVoltar" width=21 height=18 title="<bean:message key="prompt.retirardogrupo"/>" class=geralCursoHand onclick="javascript:moveLineTr(false, 'FuncSel', 'FuncDisp')"></td>
	  			</tr>
	  		</table>
	  </td>	
		
	  <td width="48%">
	  	
		<div style="
			width: 320px;
			height: 106px;
			border-top: #CDCDCD 1px solid;
			border-left: #CDCDCD 1px solid;
			border-right: #CDCDCD 1px solid;
			border-bottom: #CDCDCD 1px solid;
			background-color: #FFFFFF;
			overflow: auto">
			
			<table width="100%" cellpadding=0 cellspacing=0 border=0>
				 <logic:present name="funcionariosVector">
					  <logic:iterate id="funcSelVector" name="funcionariosVector" indexId="numero">
					
							<tr id="trFuncSel<%= numero %>" 
								name="trFuncSelName<bean:write name="funcSelVector" property="idFuncCdFuncionario" />"
								idfunc="<bean:write name="funcSelVector" property="idFuncCdFuncionario" />" 
								idArea="<bean:write name="funcSelVector" property="csCdtbAreaAreaVo.idAreaCdArea" />" 
								suprimido="N" 
								insercao=""
								ativacao=""
								style="display: none">
								<td class="principalLstPar" width=5><input type="checkbox" id="chkFuncSel<%= numero %>"></td>
								<td class="principalLstPar" width=250>
									<script>
										acronym('<bean:write name="funcSelVector" property="funcNmFuncionario" />', 60);
									</script>
								</td>
								<td class="principalLstPar" width=25>
									<input type="text" 
										   class="text"
										   maxlength=3  
										   id="txtFuncMesa<%= numero %>" 
										   name="txtFuncMesa<%= numero %>" 
										   value=""
										   banco="N"
										   excluido="S" 
										   onkeypress="isDigito(this)" onblur="isDigito(this)"
									>
								</td>										
							</tr>
						
				 		</logic:iterate>
				 </logic:present>
			</table>
		</div>          				  	
	  </td>	
  </tr>
	<tr>
		<td width="48%" class="principalLabel" id="totalFuncionarios">
			<logic:present name="funcionariosVector">
				<bean:message key="prompt.total"/>: <%= ((java.util.Vector)request.getAttribute("funcionariosVector")).size() %>
			</logic:present>
		</td>	
		<td width="4%" class="principalLabel">&nbsp;</td>
		<td width="48%" class="principalLabel" id="totalFuncGrupo">
			<logic:present name="funcionariosVector">
				<bean:message key="prompt.total"/>: <%= ((java.util.Vector)request.getAttribute("funcionariosGrupoVector")).size() %>
			</logic:present>
		</td>	
		
	</tr>
    

</table>

 <!-- CODIGO RESPONSAVEL POR TRAZER OS ITENS SELECIONADOS-->
 <script>
 <logic:present name="funcionariosGrupoVector">
	  <logic:iterate id="funcSelVector" name="funcionariosGrupoVector" indexId="numero">
			//Desmarca os itens disponiveis
			for (var i = 0; i <= document.administracaoCsCdtbGrupopreatendGrprForm.qtdFunc.value; i++){
				if (document.getElementById("trFuncDisp" + i).getAttribute("idfunc") == "<bean:write name="funcSelVector" property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />"){
					document.getElementById("trFuncDisp" + i).style.display = "none";
					document.getElementById("trFuncSel" + i).style.display = "block";
					 
					 var txtFuncMesa = document.getElementById("txtFuncMesa" + i);
					 
					 txtFuncMesa.value = "<bean:write name="funcSelVector" property="furpDsMesa" />";
					 txtFuncMesa.setAttribute("banco","S");
					 txtFuncMesa.setAttribute("excluido","N");
					 
					break;
				}
			}
	  </logic:iterate>
 </logic:present>
 </script>
	 <!-- FIM DO CODIGO RESPONSAVEL POR TRAZER OS ITENS SELECIONADOS-->	 


</html:form>
</body>
</html>	