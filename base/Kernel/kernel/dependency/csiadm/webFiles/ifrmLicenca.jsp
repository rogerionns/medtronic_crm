<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<html>
<head></head>
<body class="principalBgrPageIFRM">
<%
//Primeiro verifica se a sess�o j� foi invalidada, se j� foi n�o tem que ficar dando refresh 
if(request.getAttribute("sessionInvalidate")==null) { 
	if(request.getSession().getAttribute("encerraSessao")==null || request.getSession().getAttribute("encerraSessao").equals("false")){%>
		<meta http-equiv="refresh" content="<%=br.com.plusoft.licenca.helper.LicencaHelper.TEMPO_VALIDACAO_LICENCA %>">
	<%}else{%>
		<meta http-equiv="refresh" content="60">
		<script>
			alert("<bean:message key='prompt.atencaoSuaSessaoIraExpirarEm1Minuto'/>");
		</script>	
	<%}
}
%>

</body>
</html>