<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsAstbSuperGrupoManifSugmForm.target = admIframe.name;
	document.administracaoCsAstbSuperGrupoManifSugmForm.acao.value ='filtrar';
	document.administracaoCsAstbSuperGrupoManifSugmForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsAstbSuperGrupoManifSugmForm.filtro.value = '';
}

function submeteFormIncluir() {
	editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.target = editIframe.name;
	editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_SUPERGRUPOMANIF_SUGM%>';
	editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo1,codigo2){
	submeteFormEdit(codigo1, 0, codigo2);
}
function submeteFormEdit(idSugrCdSupergrupo,idMatpCdManiftipo,idGrmaCdGrupoManifestacao){
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.idSugrCdSupergrupo.value = idSugrCdSupergrupo;
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.idMatpCdManiftipo.value = idMatpCdManiftipo;
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.idGrmaCdGrupoManifestacao.value = idGrmaCdGrupoManifestacao;
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_SUPERGRUPOMANIF_SUGM%>';
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.target = editIframe.name;
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){

	if(window.document.all.item("Manifestacao").style.display == "none"){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else{	
		if (tab.document.administracaoCsAstbSuperGrupoManifSugmForm.idGrmaCdGrupoManifestacao.value == "") {
			alert("<bean:message key="prompt.O_campo_Grupo_Manifestacao_e_obrigatorio"/>.");
			tab.document.administracaoCsAstbSuperGrupoManifSugmForm.idGrmaCdGrupoManifestacao.focus();
			return false;
		}

		tab.document.administracaoCsAstbSuperGrupoManifSugmForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_SUPERGRUPOMANIF_SUGM%>';
		tab.document.administracaoCsAstbSuperGrupoManifSugmForm.target = admIframe.name;
		disableEnable(tab.document.administracaoCsAstbSuperGrupoManifSugmForm.idSugrCdSupergrupo, false);
		tab.document.administracaoCsAstbSuperGrupoManifSugmForm.submit();

		inicio();
	}
}

function submeteExcluir(codigo1,codigo2) {
	disableEnable(editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.idSugrCdSupergrupo, false);
	disableEnable(editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.idMatpCdManiftipo, false);
	
	disableEnable(editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.idGrmaCdGrupoManifestacao, false);
	
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.idSugrCdSupergrupo.value = codigo1;
	tab.addOptionComboItem(tab.document.administracaoCsAstbSuperGrupoManifSugmForm.idGrmaCdGrupoManifestacao, codigo2, "", null);
	
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.idGrmaCdGrupoManifestacao.value = codigo2;
	
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_SUPERGRUPOMANIF_SUGM%>';
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.target = editIframe.name;
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsAstbSuperGrupoManifSugmForm.submit();
	
	/////////////////////////////////////
	//AtivarPasta(editIframe);
	//MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
	/////////////////////////////////////	
}

function setConfirm(confirmacao){
	if (confirmacao == true){
		editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_SUPERGRUPOMANIF_SUGM%>';
		editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.target = admIframe.name;
		editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.idSugrCdSupergrupo, false);
		disableEnable(editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.idGrmaCdGrupoManifestacao, false);
		disableEnable(editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.idMatpCdManiftipo, false);
		editIframe.document.administracaoCsAstbSuperGrupoManifSugmForm.submit();

		tab.document.administracaoCsAstbSuperGrupoManifSugmForm.idGrmaCdGrupoManifestacao.value = "";
	}else{
		//cancel();	
	}
}

function continuaConfirm(){
	
}

function cancel(){

	editIframe.location = 'AdministracaoCsAstbSuperGrupoManifSugm.do?tela=editCsAstbSuperGrupoManifSugm&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsAstbSuperGrupoManifSugmForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsAstbSuperGrupoManifSugmForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_SUPERGRUPOMANIF_SUGM%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			break;
		case editIframe:
			tab.document.administracaoCsAstbSuperGrupoManifSugmForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_SUPERGRUPOMANIF_SUGM%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function inicio() {
	if(editIframe.lstArqCarga==undefined) {
		setTimeout('inicio()', 500);
		return;
	}
	
	editIframe.carregaGrupomanifestacao();
	editIframe.lstArqCarga.location.reload();
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); ">

<html:form styleId="administracaoCsAstbSuperGrupoManifSugmForm"	action="/AdministracaoCsAstbSuperGrupoManifSugm.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="100%" width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="400px" valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/><!-- ## --></td>
									<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.GrupoSuper"/><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="98%"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="width: 97%; height: 100%;display: none"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="100%"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsAstbSuperGrupoManifSugm.do?tela=editCsAstbSuperGrupoManifSugm&acao=incluir"
										width="100%" height="400px" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="width: 99%; height: 400px;display: block">
													<table style="width: 99%; height: 100%" border="0" cellspacing="0" 
														cellpadding="0" align="center">
														<tr>

															<td class="principalLabel" width="8%" colspan="2"><bean:message
																	key="prompt.nomeDocumento" /></td>

														</tr>

														<tr>

															<td colspan="3">
																<table border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="25%"><html:text property="filtro"
																				styleClass="principalObjForm"
																				onkeydown="if(event.keyCode==13) filtrar();" /></td>
																		<td width="05%">&nbsp;<img
																			src="webFiles/images/botoes/setaDown.gif" width="21"
																			height="18" class="geralCursoHand"
																			title="<bean:message key='prompt.aplicarFiltro'/>"
																			onclick="filtrar()">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>


														<tr valign="top" >

															<td colspan="3" height="100%" >
																<!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
																<iframe id=admIframe name="admIframe"
																	src="AdministracaoCsAstbSuperGrupoManifSugm.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
																	width="100%" height="98%" scrolling="Default"
																	frameborder="0" marginwidth="0" marginheight="0"></iframe>
															</td>
														</tr>
													</table>
												</div>

							
                </td>
							<td width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="100%" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_SEGMENTOGRUPO_INCLUSAO_CHAVE%>', document.administracaoCsAstbSuperGrupoManifSugmForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_SEGMENTOGRUPO_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_SEGMENTOGRUPO_ALTERACAO_CHAVE%>')){
			document.administracaoCsAstbSuperGrupoManifSugmForm.imgGravar.disabled=true;
			document.administracaoCsAstbSuperGrupoManifSugmForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsAstbSuperGrupoManifSugmForm.imgGravar.title='';
	   }
</script>




</body>
</html>
