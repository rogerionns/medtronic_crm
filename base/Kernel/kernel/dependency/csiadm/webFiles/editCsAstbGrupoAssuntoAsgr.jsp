<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
 	function desabilitaCampos(){
 		document.administracaoCsAstbGrupoAssuntoAsgrForm.idAsmeCdAssuntoMail.disabled= true;
		document.administracaoCsAstbGrupoAssuntoAsgrForm.idGrfuCdGrupoFunc.disabled= true;	
	}	
</script>


<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsAstbGrupoAssuntoAsgrForm" action="/AdministracaoCsAstbGrupoAssuntoAsgr.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.assuntoEMail"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
		      	
		      	<html:select property="idAsmeCdAssuntoMail" styleClass="principalObjForm" disabled="true"> 
		          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
		          <html:options collection="csCdtbAssuntoMailAsmeVector" property="idAsmeCdAssuntoMail" labelProperty="asmeDsAssuntoMail"/> 
		        </html:select> 
		                  
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.grupoFuncionario"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 

		      	<html:select property="idGrfuCdGrupoFunc" styleClass="principalObjForm" disabled="true"> 
		          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
		          <html:options collection="csCdtbGrupoFuncGrfuVector" property="idGrfuCdGrupoFunc" labelProperty="grfuDsGrupoFunc"/> 
		        </html:select> 

          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			document.administracaoCsAstbGrupoAssuntoAsgrForm.idAsmeCdAssuntoMail.disabled= false;
			document.administracaoCsAstbGrupoAssuntoAsgrForm.idGrfuCdGrupoFunc.disabled= false;
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_GRUPOASSUNTO_INCLUSAO_CHAVE%>', parent.document.administracaoCsAstbGrupoAssuntoAsgrForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_GRUPOASSUNTO_INCLUSAO_CHAVE%>')){
				desabilitaCampos();
			}		
			document.administracaoCsAstbGrupoAssuntoAsgrForm.idAsmeCdAssuntoMail.disabled= false;
			document.administracaoCsAstbGrupoAssuntoAsgrForm.idGrfuCdGrupoFunc.disabled= false;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_GRUPOASSUNTO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_GRUPOASSUNTO_ALTERACAO_CHAVE%>', parent.document.administracaoCsAstbGrupoAssuntoAsgrForm.imgGravar);	
				desabilitaCampos();
			}
		</script>
</logic:equal>
</html>