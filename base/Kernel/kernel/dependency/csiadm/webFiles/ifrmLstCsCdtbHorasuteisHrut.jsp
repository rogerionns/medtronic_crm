<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>
<body class="principalBgrPageIFRM" text="#000000"  topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsCdtbHorasuteisHrutForm" action="/AdministracaoCsCdtbHorasuteisHrut.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csCdtbHorasuteisHrutListaVector" indexId="sequencia"> 
  <tr> 
    <td width="5%" > 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" id="lixeira" width="18" height="18" class="principalLstParMao" title="<bean:message key='prompt.excluir'/>" onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="idHrutCdHorasuteis" />','<bean:write name="ccttrtVector" property="hrutNrSequencia" />');" > 
    </td>
   <td  width="38%" class="principalLstParMao" onclick="parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idHrutCdHorasuteis" />','<bean:write name="ccttrtVector" property="hrutNrSequencia" />');">
     <bean:write name="ccttrtVector" property="hrutDsInicio" /> - <bean:write name="ccttrtVector" property="hrutDsFim" /> 
    </td>
     <td width="36%" class="principalLstParMao" onclick="parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idHrutCdHorasuteis" />','<bean:write name="ccttrtVector" property="hrutNrSequencia" />');" >
     &nbsp;
     	<logic:equal name="ccttrtVector" property="hrutInTipo" value="M">
     		<bean:message key="prompt.manifestacao"/>
     	</logic:equal>
        <logic:equal name="ccttrtVector" property="hrutInTipo" value="C">
     		<bean:message key="prompt.chat"/>
     	</logic:equal>
     	<logic:equal name="ccttrtVector" property="hrutInTipo" value="P">
     		<bean:message key="prompt.campanha"/>
     	</logic:equal>
    </td>
    <td width="21%" class="principalLstParMao" onclick="parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idHrutCdHorasuteis" />','<bean:write name="ccttrtVector" property="hrutNrSequencia" />');" >
     &nbsp;<bean:write name="ccttrtVector" property="hrutDhInativo" /> 
    </td>
  </tr>
  </logic:iterate>
</table>

<script language="JavaScript">
	//setPermissaoImageDisable("<!--%=PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_HORASUTEIS_EXCLUSAO_CHAVE%-->", document.editCsCdtbHorasuteisHrutForm.lixeira);
</script>

</html:form>
</body>
</html>