<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">


<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposGrupotelGrte(){
		document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"].disabled= true;
		document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.grteDsGrupotel"].disabled= true;
		document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.grteNrGrupotel"].disabled= true;
		document.administracaoCsCdtbGrupotelGrteForm.inInativo.disabled= true;
	}	
	
	function inicio(){
		setaChavePrimaria(administracaoCsCdtbGrupotelGrteForm['csCdtbGrupotelGrteVo.idGrteCdGrupotel'].value);
	}	
</script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbGrupotelGrteForm" action="/AdministracaoCsCdtbGrupotelGrte.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="csCdtbGrupotelGrteVo.grteDhInativo"/>
	
	<br>
	<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
        	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
        <td width="45%">
        	<table width="40%" border="0" cellspacing="0" cellpadding="0">
	        	<tr>
		        	<td>
		           		<html:text property="csCdtbGrupotelGrteVo.idGrteCdGrupotel" styleClass="text" disabled="true" maxlength="10"/>
		           	</td>
	           	</tr>
           	</table>
	    </td>
        <td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.descricaoGrupoTelefone"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csCdtbGrupotelGrteVo.grteDsGrupotel" styleClass="text" maxlength="60" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
	
	<tr> 
		<td width="25%" align="right" class="principalLabel"><bean:message key="prompt.numero"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="45%"> 
			<html:text property="csCdtbGrupotelGrteVo.grteNrGrupotel" styleClass="text" maxlength="10" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" /> 
		</td>
		<td width="15%">&nbsp;</td>
        <td width="15%">&nbsp;</td>
	</tr>
			
	<tr> 
		<td width="25%">&nbsp;</td>
		<td width="45%">&nbsp;</td>
		<td class="principalLabel" width="15%"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
				<td align="right" width="50%"> 
					<html:checkbox value="true" property="inInativo"/><!-- @@ --></td>
            	<td class="principalLabel" width="50%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
			</tr>
            </table>
		</td>
		<td width="31%">&nbsp;</td>
	</tr>
	
	</table>

</html:form>
</body>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposGrupotelGrte();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_GRUPOTELEFONE_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbGrupotelGrteForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_GRUPOTELEFONE_INCLUSAO_CHAVE%>')){
				desabilitaCamposGrupotelGrte();
			}else{
				document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"].disabled= false;
				document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"].value= '';
				document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"].disabled= true;
				document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.grteDsGrupotel"].disabled= false;
				document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.grteNrGrupotel"].disabled= false;
				document.administracaoCsCdtbGrupotelGrteForm.inInativo.disabled= false;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_GRUPOTELEFONE_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_GRUPOTELEFONE_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbGrupotelGrteForm.imgGravar);	
				desabilitaCamposGrupotelGrte();
			}
		</script>
</logic:equal>

</html>

<script>
	//try{administracaoCsCdtbEstadoCivilEsciForm.dsEstadoCivil.focus();}
	//catch(e){}
</script>