<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	
function inicio(){
	showError('<%=request.getAttribute("msgerro")%>');
	validarPermissao();
	atualizaListaPesqFunc();
	
	
	if (administracaoCsCatbPesquisaFuncPefuForm.acao.value == '<%= Constantes.ACAO_EXCLUIR %>'){
		trataExclusao();
	}
}	

function trataExclusao(){
	if (lstFuncionariosGrupo.document.readyState != 'complete')
		setTimeout("trataExclusao()",100);
	else{	
		administracaoCsCatbPesquisaFuncPefuForm.idPesqCdPesquisa.disabled= true;
		administracaoCsCatbPesquisaFuncPefuForm['csCdtbAreaAreaVo.idAreaCdArea'].disabled= true;
		administracaoCsCatbPesquisaFuncPefuForm.funcNmFuncionario.disabled= true;
		lstFuncionarios.document.forms[0].idFuncCdFuncionario.disabled= true;
		lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.disabled= true;
		
		confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
		parent.setConfirm(confirmacao);
	}	
}

function validarPermissao(){
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQFUNC_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQFUNC_ALTERACAO_CHAVE%>', parent.administracaoCsCatbPesquisaFuncPefuForm.imgGravar);
			desabilitaCamposGrupofuncperm();
		}
	</logic:equal>
}

function desabilitaCamposGrupofuncperm(){
	try{
		setPermissaoImageDisable('', administracaoCsCatbPesquisaFuncPefuForm.imgAvancar);
		setPermissaoImageDisable('', administracaoCsCatbPesquisaFuncPefuForm.imgVoltar);

		administracaoCsCatbPesquisaFuncPefuForm.idPesqCdPesquisa.disabled=true;
		administracaoCsCatbPesquisaFuncPefuForm["csCdtbAreaAreaVo.idAreaCdArea"].disabled=true;
		
		lstFuncionarios.document.forms[0].idFuncCdFuncionario.disabled=true;
		lstFuncionarios.document.forms[0].todos.disabled=true;
		
		lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.disabled=true;
		lstFuncionariosGrupo.document.forms[0].todos.disabled=true;
	}
	catch(e){
		setTimeout("desabilitaCamposGrupofuncperm();", 300);
	}
}

function atualizaListaPesqFunc(){
	var url="";
	
	url = "AdministracaoCsCatbPesquisaFuncPefu.do?tela=<%=MAConstantes.TELA_LST_PESQUISAFUNC_GRUPO %>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR %>";
	url = url + "&idPesqCdPesquisa=" + administracaoCsCatbPesquisaFuncPefuForm.idPesqCdPesquisa.value;
	
	lstFuncionariosGrupo.location.href = url;
	
	//limpa lista de funcionarios
	administracaoCsCatbPesquisaFuncPefuForm['csCdtbAreaAreaVo.idAreaCdArea'].value = "0";
	atualizaListaFuncionarios(true);
	
}

function atualizaListaFuncionarios(bOrigemClick) {
	
	if(bOrigemClick){
		administracaoCsCatbPesquisaFuncPefuForm["funcNmFuncionario"].value="";
	}
	
	oldAcao = administracaoCsCatbPesquisaFuncPefuForm.acao.value;
	oldTela = administracaoCsCatbPesquisaFuncPefuForm.tela.value;
	
	administracaoCsCatbPesquisaFuncPefuForm.acao.value = '<%= Constantes.ACAO_CONSULTAR %>';
	administracaoCsCatbPesquisaFuncPefuForm.tela.value = '<%= MAConstantes.TELA_LST_PESQUISAFUNC %>';
	administracaoCsCatbPesquisaFuncPefuForm.target = lstFuncionarios.name;
	//Chamado: 89852 - 01/08/2013 - Carlos Nunes
	administracaoCsCatbPesquisaFuncPefuForm.submit();
	
	administracaoCsCatbPesquisaFuncPefuForm.acao.value = oldAcao;
	administracaoCsCatbPesquisaFuncPefuForm.tela.value = oldTela;
}	

function moveToRight() {
	selecionou = 0;
	if (lstFuncionarios.document.forms[0].idFuncCdFuncionario.length == 0) {
		alert('<bean:message key="prompt.nao_existem_funcionarios_na_lista_para_serem_selecionados"/>');
		return false;
	}
	for (i = 0; i < lstFuncionarios.document.forms[0].idFuncCdFuncionario.length; i++) {
		if (lstFuncionarios.document.forms[0].idFuncCdFuncionario.options[i].selected) { 
			selecionou = 1;
			if (!(verificaExistencia(lstFuncionarios.document.forms[0].idFuncCdFuncionario.options[i].value, lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario))) {
				alert('<bean:message key="prompt.o_funcionario"/> ' + lstFuncionarios.document.forms[0].idFuncCdFuncionario.options[i].text + ' <bean:message key="prompt.ja_foi_selecionado_para_este_grupo"/>');
			}
			else {
				lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.options[lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.length] = new Option(lstFuncionarios.document.forms[0].idFuncCdFuncionario.options[i].text, lstFuncionarios.document.forms[0].idFuncCdFuncionario.options[i].value);											
				lstFuncionarios.document.forms[0].idFuncCdFuncionario.options[i] = null;
				i--;
			}
		}
	}
	lstFuncionarios.atualizaTotal(lstFuncionarios.document.forms[0].idFuncCdFuncionario.length);
	lstFuncionariosGrupo.atualizaTotal(lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.length);
	
	if (lstFuncionarios.document.forms[0].todos.checked)
		lstFuncionarios.document.forms[0].todos.checked = false;
	
	if (selecionou == 0)
		alert('<bean:message key="prompt.Por_favor_selecione_um_funcionario"/>');
}

function moveToLeft() {
	selecionou = 0;
	if (lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.length == 0) {
		alert('<bean:message key="prompt.nao_existem_funcionarios_na_lista_para_serem_selecionados"/>');
		return false;
	}
	for (i = 0; i < lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.length; i++) {
		if (lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.options[i].selected) { 
			selecionou = 1;
			if ((verificaExistencia(lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.options[i].value, lstFuncionarios.document.forms[0].idFuncCdFuncionario))) {
				lstFuncionarios.document.forms[0].idFuncCdFuncionario.options[lstFuncionarios.document.forms[0].idFuncCdFuncionario.length] = new Option(lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.options[i].text, lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.options[i].value);											
			}
			lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.options[i] = null;
			i--;
		}
	}
	lstFuncionarios.atualizaTotal(lstFuncionarios.document.forms[0].idFuncCdFuncionario.length);
	lstFuncionariosGrupo.atualizaTotal(lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.length);

	if (lstFuncionariosGrupo.document.forms[0].todos.checked)
		lstFuncionariosGrupo.document.forms[0].todos.checked = false;

	if (selecionou == 0)
		alert('<bean:message key="prompt.Por_favor_selecione_um_funcionario"/>');
}

// Verifica se o elemento ja foi copiado
function verificaExistencia(valor, obj) {
	for (j = 0; j < obj.length; j++) {
		if (obj.options[j].value == valor)
			return false;	
	}
	return true;
}

function filtrar(event) {
	if (event.keyCode == 13){
		//Chamado: 89852 - 29/07/2013 - Carlos Nunes
		administracaoCsCatbPesquisaFuncPefuForm.acao.value = '<%= Constantes.ACAO_CONSULTAR %>';
		administracaoCsCatbPesquisaFuncPefuForm.tela.value = '<%= MAConstantes.TELA_LST_PESQUISAFUNC %>';
		
		atualizaListaFuncionarios(false);
	}	
}

function getListaFuncionariosGrupo() {
	var html = "";
	for (i = 0; i < lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.length; i++) {
		html += "<input type=\"hidden\" name=\"idPesqFunc\" value=\"" + lstFuncionariosGrupo.document.forms[0].idFuncCdFuncionario.options[i].value + "\">";
	}
	document.getElementById("listaFuncionarios").innerHTML = html;
}
</script>
<body class= "principalBgrPageIFRM" onload="inicio()">

<html:form styleId="administracaoCsCatbPesquisaFuncPefuForm" action="/AdministracaoCsCatbPesquisaFuncPefu.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>	
	
	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.pesquisa"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%" colspan="2"> 


			<html:select property="idPesqCdPesquisa" styleClass="principalObjForm" onchange="atualizaListaPesqFunc();">
				<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
				<logic:present name="vectorPesquisa">
					<html:options collection="vectorPesquisa" property="idPesqCdPesquisa" labelProperty="pesqDsPesquisa"/>
				</logic:present>
			</html:select>


          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel">&nbsp;<!-- ## --> 
            <!--img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"--></td>
          <td colspan="2"> 
            &nbsp;
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                	&nbsp;
                </td>
            
                <td class="principalLabel" width="17%">&nbsp;<!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        
        <!-- INICIO ABA DE FUNCIONARIO -->
        <tr>
        	<td colspan="4">
	        	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td height="254">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"	align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
								<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado" id="funcionario" name="funcionario">
										<bean:message key="prompt.funcionario" />
									</td>
								</tr>
								</table>
							</td>
							<td width="4">
								<img src="/webFiles/images/separadores/pxTranp.gif" width="1" height="1">
							</td>
						</tr>
						</table>

						<table width="100%" border="0" cellspacing="0" cellpadding="0"	align="center">
						<tr>
							<td valign="top" class="principalBgrQuadro" height="70">
								
								<!-- TABELA DE FORA -->
								<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
								<tr>
									<!-- LISTA DE FUNCIONARIOS -->
									<td width="48%" height="30">
										<table border="0" cellpadding="0" cellspacing="4" width="100%" height="100%">
										<tr>
											<td class="principalLabel" height="15" colspan="2">
												<bean:message key="prompt.area"/><br>
												<html:select property="csCdtbAreaAreaVo.idAreaCdArea" styleClass="principalObjForm" onchange="atualizaListaFuncionarios(true);">
													<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
													<logic:present name="vectorArea">
														<html:options collection="vectorArea" property="idAreaCdArea" labelProperty="areaDsArea"/>
													</logic:present>
												</html:select>
											</td>													
										</tr>
										<tr>
											<td class="principalLabel" colspan="2" valign="top">
												<iframe name="lstFuncionarios" id="lstFuncionarios"	src="AdministracaoCsCatbPesquisaFuncPefu.do?tela=<%= MAConstantes.TELA_LST_PESQUISAFUNC %>&acao=<%= Constantes.ACAO_CONSULTAR %>"	width="100%" height="175" scrolling="no" frameborder="0"	marginwidth="0" marginheight="0"></iframe>
											</td>	
										</tr>
										</table>	
									</td>
									<!-- LISTA DE FUNCIONARIOS -->
									
									<!-- BOTOES DE MOVIMENTACAO -->
									<td valign="top" width="4%" height="100"> <!--Jonathan | Adequa��o para o IE 10-->
										<table cellpadding="0" cellspacing="0" width="100%" height="80%" border="0" align="center">
										<tr><td height="80"></td></tr>
			  							<tr>
			  								<td valign="top" align="center"><img src="webFiles/images/botoes/avancar_unico.gif" name="imgAvancar" width=21 height=18 class=geralCursoHand title="<bean:message key="prompt.adicionaraogrupo"/>" onclick="moveToRight();"></td>
			  							</tr>
			  				  			<tr>
			  								<td valign="top" align="center"><img src="webFiles/images/botoes/voltar_unico.gif" name="imgVoltar" width=21 height=18 class=geralCursoHand title="<bean:message key="prompt.retirardogrupo"/>" onclick="moveToLeft();"></td>
							  			</tr>
			  							</table>
									</td>
									<!-- BOTOES DE MOVIMENTACAO -->
									
									<!-- LISTA DE FUNCIONARIOS DO GRUPO CADASTRADO -->
									<td height="100">
										<table border="0" cellpadding="0" cellspacing="4" width="100%" height="100%">
										<tr>
											<td class="principalLabel" height="15">
												<bean:message key="prompt.nome"/><br>
												<html:text property="funcNmFuncionario" styleClass="principalObjForm" onkeypress="filtrar(event);"/>
											</td>	
										</tr>
										<tr>
											<td class="principalLabel" colspan="2" valign="top">
												<iframe name="lstFuncionariosGrupo" id="lstFuncionariosGrupo" src="AdministracaoCsCatbPesquisaFuncPefu.do?tela=<%= MAConstantes.TELA_LST_PESQUISAFUNC_GRUPO %>&acao=<%= Constantes.ACAO_CONSULTAR %>&idPesqCdPesquisa=" + <bean:write name="administracaoCsCatbPesquisaFuncPefuForm" property="idPesqCdPesquisa"/>	width="100%" height="175" scrolling="no" frameborder="0"	marginwidth="0" marginheight="0"></iframe>
											</td>	
										</tr>
										</table>	
									</td>
									<!-- LISTA DE FUNCIONARIOS DO GRUPO CADASTRADO -->
									
								</tr>
								</table>
								<!-- FINAL DA TABELA DE FORA -->
								
							</td>
							<td width="4" height="230"><img	src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>						
						</tr>
						<tr>
							<td width="100%" height="8" valign="top"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
							<td width="4" height="8" valign="top"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
        <!-- FINAL ABA DE FUNCIONARIO -->
        
      </table>

      <div id="listaFuncionarios"></div>
</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQFUNC_INCLUSAO_CHAVE%>', parent.administracaoCsCatbPesquisaFuncPefuForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQFUNC_INCLUSAO_CHAVE%>')){
				desabilitaCamposGrupofuncperm();
			}else{
				//administracaoCsCatbPesquisaFuncPefuForm["csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].disabled= false;
				//administracaoCsCatbPesquisaFuncPefuForm["csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].value= '';
				//administracaoCsCatbPesquisaFuncPefuForm["csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].disabled= true;
			}
		</script>
</logic:equal>

</html>

<script>
	try{administracaoCsCatbPesquisaFuncPefuForm.idPesqCdPesquisa.focus();}
	catch(e){}
</script>
