<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsCdtbGrupopreatendGrprForm" action="/AdministracaoCsCdtbGrupopreatendGrpr.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="csCdtbGrupopreatendGrprVo.idGrprCdGrupopreatend"/>

<script>var possuiRegistros=false;</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="CsCdtbGrupopreatendGrprVector" indexId="sequencia">
  <script>possuiRegistros=true;</script>
  <tr> 
    <td width="3%" class="principalLstPar">
      <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="idGrprCdGrupopreatend" />')">
    </td>
    <td class="principalLstParMao" width="12%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idGrprCdGrupopreatend" />')">
      <bean:write name="ccttrtVector" property="idGrprCdGrupopreatend" />
    </td>
    <td class="principalLstParMao" align="left" width="58%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idGrprCdGrupopreatend" />')">
      <bean:write name="ccttrtVector" property="grprDsAssuntogrupo1" />
	</td>
	<td class="principalLstParMao" align="center" width="25%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idGrprCdGrupopreatend" />')">
	&nbsp;
      <bean:write name="ccttrtVector" property="grprDhInativo" />
	</td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>

<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_PREATENDIMENTO_EXCLUSAO_CHAVE%>', administracaoCsCdtbGrupopreatendGrprForm.lixeira);
	
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>