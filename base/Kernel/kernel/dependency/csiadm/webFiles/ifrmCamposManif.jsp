<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class="principalBgrPageIFRM" text="#000000" onload="obtemValor();showError('<%=request.getAttribute("msgerro")%>')">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
var modalWin;

function MM_openBrWindow(theURL,winName,features) { //v2.0
  if (modalWin!=null && !modalWin.closed){
	modalWin.focus();
  }else{
	 modalWin = window.open(theURL,winName,features);
  }
}	


function obtemValor(){

	parent.document.getElementById("parametrosManifestacao").innerHTML = "";

	strTxt = "";
	strTxt += "    <input type=\"hidden\" name=\"tpmaNrDiasResolucao\"  id=\"tpmaNrDiasResolucao\"  value=\"" + administracaoCsAstbProdutoManifPrmaForm.tpmaNrDiasResolucao.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"tpmaInTempoResolucao\" id=\"tpmaInTempoResolucao\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.tpmaInTempoResolucao.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"idFuncCdFuncionario\"  id=\"idFuncCdFuncionario\"  value=\"" + administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionario.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"idTxpmCdTxpadraomanif\" id=\"idTxpmCdTxpadraomanif\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idTxpmCdTxpadraomanif.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"idClmaCdClassifmanif\"  id=\"idClmaCdClassifmanif\"  value=\"" + administracaoCsAstbProdutoManifPrmaForm.idClmaCdClassifmanif.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"idDocuCdDocumento\"    id=\"idDocuCdDocumento\"  value=\"" + administracaoCsAstbProdutoManifPrmaForm.idDocuCdDocumento.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"tpmaInConclui\"        id=\"tpmaInConclui\"      value=\"" + administracaoCsAstbProdutoManifPrmaForm.tpmaInConclui.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"tpmaInOperadorResp\"   id=\"tpmaInOperadorResp\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.tpmaInOperadorResp.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"idDeprCdDesenhoProcesso\" id=\"idDeprCdDesenhoProcesso\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idDeprCdDesenhoProcesso.value + "\" > ";
	//strTxt += "    <input type=\"hidden\" name=\"tpmaDhInativo\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.tpmaDhInativo.value + "\" > ";
	//strTxt += "    <input type=\"hidden\" name=\"idFuncCdFuncionarioCopCombo\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"tpmaTxProcedimento\" id=\"tpmaTxProcedimento\" value=\"" + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"tpmaTxOrientacao\"   id=\"tpmaTxOrientacao\"   value=\"" + "\" > ";
	
	parent.document.getElementById("parametrosManifestacao").innerHTML += strTxt;

	window.parent.document.getElementById("tpmaTxProcedimento").value = administracaoCsAstbProdutoManifPrmaForm.tpmaTxProcedimento.value;
	window.parent.document.getElementById("tpmaTxOrientacao").value = administracaoCsAstbProdutoManifPrmaForm.tpmaTxOrientacao.value;

}


</script>

<html:form styleId="administracaoCsAstbProdutoManifPrmaForm" action="/AdministracaoCsAstbProdutoManifPrma.do" >

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />

	<html:hidden property="tpmaNrDiasResolucao" />
	<html:hidden property="tpmaInTempoResolucao" />
	<html:hidden property="idFuncCdFuncionario" />
	<html:hidden property="idTxpmCdTxpadraomanif" />
	<html:hidden property="idClmaCdClassifmanif" />
	<html:hidden property="idDeprCdDesenhoProcesso" />
	<html:hidden property="idDocuCdDocumento" />
	<html:hidden property="tpmaInConclui" />
	<html:hidden property="tpmaInOperadorResp" />
	<html:hidden property="tpmaTxProcedimento" />
	<html:hidden property="tpmaTxOrientacao" />

	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">

		<tr>
		</tr>
	</table>

</html:form>
</body>
</html>