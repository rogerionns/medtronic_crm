<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript">

var nCountCarregaTpManifestacao = 0;

function carregaTpManifestacao() {
	try{
		document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
		document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_TPMANIFESTACAO_TPMA%>';
		document.administracaoCsAstbComposicaoCompForm.target = parent.cmbTpManifestacao.name;
		document.administracaoCsAstbComposicaoCompForm.submit();
	}catch(e){
		if(nCountCarregaTpManifestacao < 5){
			setTimeout('carregaTpManifestacao()',500);
			nCountCarregaTpManifestacao++;
		}
	}
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaTpManifestacao()">
<html:form styleId="administracaoCsAstbComposicaoCompForm" action="/AdministracaoCsAstbComposicaoComp.do">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idTpmaCdTpManifestacao" />
  
  <html:select property="idGrmaCdGrupoManifestacao" styleClass="principalObjForm" onchange="carregaTpManifestacao()">
    <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
    <html:options collection="csCdtbGrupoManifestacaoGrmaVector" property="idGrmaCdGrupoManifestacao" labelProperty="grmaDsGrupoManifestacao" />
  </html:select>
</html:form>
</body>
</html>