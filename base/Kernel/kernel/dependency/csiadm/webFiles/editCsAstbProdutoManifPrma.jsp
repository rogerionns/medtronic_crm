<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
var flag = 0;
var desabilita = false;

function getFlagProduto(){
	return flag;
}

function setFlagProduto(valor){
	flag = valor;
}

function inicio(){
	showError('<%=request.getAttribute("msgerro")%>');
	validarPermissao();
}

function validarPermissao(){
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFESTACAOPRODUTO_INCLUSAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFESTACAOPRODUTO_ALTERACAO_CHAVE%>', parent.document.administracaoCsAstbProdutoManifPrmaForm.imgGravar);	
			desabilitaCamposProdutoManif();	
		}
	</logic:equal>
}

function desabilitaCamposProdutoManif(){
	try{
	
		ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.disabled=true;
		ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled =  true;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
			ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].disabled =  true;
			administracaoCsAstbProdutoManifPrmaForm.chkProdutoTipo.disabled =  true;			
		<%}%>
		
		ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.disabled = true;
		ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.disabled = true;
		ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled = true;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.disabled = true;
		<%}%>
		
		administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.disabled =  true;
		administracaoCsAstbProdutoManifPrmaForm.chkGrupo.disabled = true;
		administracaoCsAstbProdutoManifPrmaForm.chkGrupoManifestacao.disabled = true;
		
		administracaoCsAstbProdutoManifPrmaForm.optProduto.disabled = true;
		administracaoCsAstbProdutoManifPrmaForm.optAssunto.disabled = true;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
		    administracaoCsAstbProdutoManifPrmaForm.chkSuperGrupoManif.disabled = true;
		    administracaoCsAstbProdutoManifPrmaForm.chkSuperGrupoManif.checked = false;
			ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.disabled = true;
		<%}%>
		
	}
	catch(e){
		setTimeout("desabilitaCamposProdutoManif();",100);
	}
}


function MontaLista(){
	lstArqCarga.location.href = "AdministracaoCsAstbProdutoManifPrma.do?tela=administracaoLstCsAstbProdutoManifPrma&acao=<%=Constantes.ACAO_VISUALIZAR%>&idAsn1CdAssuntoNivel1=" + ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value;
}


function atualizaPesquisa() {
	document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
	document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_PESQ %>';
	document.administracaoCsAstbProdutoManifPrmaForm.target = ifrmCmbPesq.name;
	document.administracaoCsAstbProdutoManifPrmaForm.submit();
}

function atualizaLinha() {
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
		// jvarandas - Inclu�do para limpar a sele��o anterior, caso contr�rio ele sempre incluia a sele��o anterior na lista
		document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value = "";
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
		document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_LINHA %>';
		document.administracaoCsAstbProdutoManifPrmaForm.target = ifrmCmbLinhaLinh.name;
		document.administracaoCsAstbProdutoManifPrmaForm.submit();
	<%}else{%>
		atualizaProduto();
	<%}%>
}

function atualizaProduto() {
	document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value = 1;
	document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
	document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_PRODUTO %>';
	document.administracaoCsAstbProdutoManifPrmaForm.target = ifrmCmbLinhaProduto.name;
	document.administracaoCsAstbProdutoManifPrmaForm.submit();
}

function todosDesabilitados(){
//	if(ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value > 0){
	
		if(administracaoCsAstbProdutoManifPrmaForm.acao.value != '<%= Constantes.ACAO_EDITAR %>'){
		
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			if(administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.checked == false && administracaoCsAstbProdutoManifPrmaForm.chkProdutoTipo.checked == false && administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.checked == true){
			<%}else{%>
			if(administracaoCsAstbProdutoManifPrmaForm.chkProdutoTipo.checked == false && administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.checked == true){
			<%}%>
							
				ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.disabled = true;
				ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value = 0;
				ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.disabled = true;
				ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value = 0;
				ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled = true;
				ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value = 0;
		
				administracaoCsAstbProdutoManifPrmaForm.chkGrupo.disabled = true;
				administracaoCsAstbProdutoManifPrmaForm.chkGrupoManifestacao.disabled = true;

				administracaoCsAstbProdutoManifPrmaForm.chkGrupo.checked = false;
				administracaoCsAstbProdutoManifPrmaForm.chkGrupoManifestacao.checked = false;

				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
				administracaoCsAstbProdutoManifPrmaForm.chkSuperGrupoManif.disabled = true;
				administracaoCsAstbProdutoManifPrmaForm.chkSuperGrupoManif.checked = false;
				ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.disabled = true;
				ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.value=0;
				<%}%>

			}
			else{
				ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.disabled = false;
				
				if(!administracaoCsAstbProdutoManifPrmaForm.chkGrupoManifestacao.checked){
					ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.disabled = false;
					
					if(!administracaoCsAstbProdutoManifPrmaForm.chkGrupo.checked)
						ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled = false;
				}
				
				administracaoCsAstbProdutoManifPrmaForm.chkGrupo.disabled = false;
				administracaoCsAstbProdutoManifPrmaForm.chkGrupoManifestacao.disabled = false;

				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
				administracaoCsAstbProdutoManifPrmaForm.chkSuperGrupoManif.disabled = false;
				ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.disabled = false;
				<%}%>
			}
		}
//	}
}

function validaChk(obj){

	bValor=false;
	if(obj.checked) bValor=true;

	switch (obj.name){
		case 'chkTipoLinha':
			
			bValor=false;
			if(obj.checked) bValor=true;
			
			ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled=bValor;
			ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value=0;
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
				ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].disabled=bValor;
				ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].value=0;
				administracaoCsAstbProdutoManifPrmaForm.chkProdutoTipo.checked=false;
			<%}%>
			administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.checked=false;
			
			todosDesabilitados();
			
			break;
			
		case 'chkProdutoTipo':

			bValor=false;
			if(obj.checked) bValor=true;

			ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled=false;
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
				ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].disabled=bValor;
				ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].value=0;
			<%}%>
			
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
				administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.checked=false;
			<%}%>
			
			administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.checked=false;
			todosDesabilitados();
			
			break;
	
		case 'chkManifestacao':

			bValor=false;
			if(obj.checked) bValor=true;
		
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
				try{
					ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].disabled=false;
				}catch(e){}
				administracaoCsAstbProdutoManifPrmaForm.chkProdutoTipo.checked=false;
			<%}%>
			ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled=false;

			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
				administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.checked=false;
			<%}%>
			
			todosDesabilitados();
			
			break;
	
		case 'chkGrupoManifestacao':

			ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.disabled=bValor;
			ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value=0;
			ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled=bValor;
			ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value=0;

			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
			ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.disabled=false;
			administracaoCsAstbProdutoManifPrmaForm.chkSuperGrupoManif.checked = false;
			<%}%>

			administracaoCsAstbProdutoManifPrmaForm.chkGrupo.checked=false;
			break;
	
		case 'chkSuperGrupoManif':

			//ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.disabled=false;
			ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.disabled=bValor;
			ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value=0;
			ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled=bValor;
			ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value=0;

			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
			ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.disabled=bValor;
			ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.value=0;
			<%}%>

			administracaoCsAstbProdutoManifPrmaForm.chkGrupo.checked = false;
			administracaoCsAstbProdutoManifPrmaForm.chkGrupoManifestacao.checked = false;
			break;
	
		case 'chkGrupo':
			ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.disabled=false;
			ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled=bValor;
			ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value=0;

			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
			ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.disabled=false;
			administracaoCsAstbProdutoManifPrmaForm.chkSuperGrupoManif.checked = false;
			<%}%>
		
			administracaoCsAstbProdutoManifPrmaForm.chkGrupoManifestacao.checked=false;
			break;
	}
	
}

function validaChkCombo(){

try{
/*	if(ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value > 0){
		if(administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.checked == false && administracaoCsAstbProdutoManifPrmaForm.chkProdutoTipo.checked == false && administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.checked == false){
			if(administracaoCsAstbProdutoManifPrmaForm.acao.value != '<%= Constantes.ACAO_EDITAR %>'){
				administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.checked = true;
			}
		}
	}*/
	todosDesabilitados();
	
	var temLinha= false;
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
		temLinha =true;
	<%}%>
	
	if(administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.checked && temLinha == true){
		ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled=true;
		ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value=0;
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
			ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].value=0;
			ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].disabled=true;
		<%}%>	
		
	}else if(administracaoCsAstbProdutoManifPrmaForm.chkProdutoTipo.checked){
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
			ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].disabled=bValor;
			ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].value=0;
		<%}%>
	
	}else if(administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.checked){
	}
	if(administracaoCsAstbProdutoManifPrmaForm.chkGrupoManifestacao.checked){
		ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.disabled=bValor;
		ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value=0;

		ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled=bValor;
		ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value=0;
	}else if(administracaoCsAstbProdutoManifPrmaForm.chkSuperGrupoManif.checked){
		ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.disabled=bValor;
		ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.value=0;

		ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.disabled=bValor;
		ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value=0;

		ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled=bValor;
		ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value=0;
	}else if(administracaoCsAstbProdutoManifPrmaForm.chkGrupo.checked){
		ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled=bValor;
		ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value=0;
	
	}
}catch(x){}
}

function filtrar() {

		//Henrique (16/02/2006)	- Codigo retornado pois quando o usuario clica sobre o botao novo seleciona linha, produto e clica na lupa a lista nao retorna nada
		document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value = ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value = ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value;
		<%}else{%>
			document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value = 1;
		<%}%>
		
		document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value = ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value;
		document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value = ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value;
		document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value = ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value;
		
	    <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>				
	    	document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value = ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].value;
	    <%}%>
		
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= Constantes.ACAO_FITRAR %>';
		document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_LST_CS_ASTB_PRODUTOMANIF_PRMA %>';
		document.administracaoCsAstbProdutoManifPrmaForm.target = lstArqCarga.name;
		window.top.document.all.item('LayerAguarde').style.visibility = 'visible';
		document.administracaoCsAstbProdutoManifPrmaForm.submit();
	//}
}

function marcaDesmarca(bMarca){
	obj=lstArqCarga.document.administracaoCsAstbProdutoManifPrmaForm.listaExcluir;
	if(obj.length!=undefined){
		for(i=0;i<obj.length;i++){
			obj[i].checked=bMarca;
		}
	}else{
		obj.checked=bMarca;
	}
}

function abreManifestacao(){

	var idLinhCdLinha = 1;
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
		idLinhCdLinha = ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value
	<%}%>
	
    <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>				
		showModalDialog('AdministracaoCsAstbProdutoManifPrma.do?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MAConstantes.TELA_PARAMETROS_MANIFESTACAO %>&idAsn1CdAssuntoNivel1=' + ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value + '&idLinhCdLinha=' + idLinhCdLinha + '&idAsn2CdAssuntoNivel2=' + ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].value + '&idTpmaCdTpManifestacao=' + ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:450px,dialogTop:0px,dialogLeft:650px');
	<%}else{%>
		showModalDialog('AdministracaoCsAstbProdutoManifPrma.do?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MAConstantes.TELA_PARAMETROS_MANIFESTACAO %>&idAsn1CdAssuntoNivel1=' + ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value + '&idLinhCdLinha=' + idLinhCdLinha + '&idTpmaCdTpManifestacao=' + ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:450px,dialogTop:0px,dialogLeft:650px');	
	<%}%>
}
</script>
<body class= "principalBgrPageIFRM" onload="inicio()" style="overflow: hidden;">
<html:form styleId="administracaoCsAstbProdutoManifPrmaForm" action="/AdministracaoCsAstbProdutoManifPrma.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="idLinhCdLinha" />
	<html:hidden property="idAsn1CdAssuntoNivel1" />
	<html:hidden property="idAsn2CdAssuntoNivel2" />	
	<html:hidden property="idTpmaCdTpManifestacao" />
	<html:hidden property="idDeprCdDestinoproduto" />	
	<html:hidden property="idPesqCdPesquisa" />
	<html:hidden property="idMatpCdManifTipo" />	
	<html:hidden property="idGrmaCdGrupoManifestacao" />
	<html:hidden property="listaExcluir" />
	
<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
  
  <tr>
	  <td colspan="4" align="center" class="principalLabel">
		  <table width="60%" border="0">
		  <tr>
			  <td class="principalLabel" width="3%">
				  <html:radio property="tipoLinha" value="S" styleId="optProduto" onclick="atualizaLinha();"/>
			  </td>
			  <td class="principalLabel"  width="35%">
				  <bean:message key="prompt.produto"/>
			  </td>
			  <td class="principalLabel" width="3%">
				  <html:radio property="tipoLinha" styleId="optAssunto" value="N" onclick="atualizaLinha();"/>
			  </td>
			  <td class="principalLabel">
				  <bean:message key="prompt.assunto"/>
			  </td>
		  </tr>
		  </table>	
	  </td>
  </tr>	
	  
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")){%>
  <tr>
	  <td width="20%" align="right" class="principalLabel"><%= getMessage("prompt.linha", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
  	  <td colspan="2" valign="middle"> 
	  	  <iframe id=ifrmCmbLinhaLinh name="ifrmCmbLinhaLinh" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_LINHA %>&idLinhCdLinha=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idLinhCdLinha'/>&idAsn1CdAssuntoNivel1=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idAsn1CdAssuntoNivel1'/>&tipoLinha=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='tipoLinha'/>&idAsn2CdAssuntoNivel2=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idAsn2CdAssuntoNivel2'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
  	  </td>
    <td class="principalLabel" width="32%"> 
      <input type="checkbox" id="chkTipoLinha" name="chkTipoLinha" onclick="validaChk(this);" >
      <bean:message key="prompt.todos_tipos_produtos_linha"/>
      </td>
  </tr>
  
  <%}%>
    <tr> 
    
  <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
	<td width="17%" align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel1", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	<td colspan="2" valign="middle">
    	<iframe id=ifrmCmbLinhaProduto name="ifrmCmbLinhaProduto" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_PRODUTO %>&idAsn1CdAssuntoNivel1=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idAsn1CdAssuntoNivel1'/>&idLinhCdLinha=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idLinhCdLinha'/>&idAsn2CdAssuntoNivel2=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idAsn2CdAssuntoNivel2'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
	</td>

	<td class="principalLabel" width="32%"> 
      <input type="checkbox" id="chkProdutoTipo" name="chkProdutoTipo" onclick="validaChk(this);" >
      <bean:message key="prompt.todos_produtos_tipo_produto"/>
	</td>
  <%}else{%>
	<td style="border: #7088c5; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;" width="17%" align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel1", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	<td style="border: #7088c5; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;" colspan="2" valign="middle">
    	<iframe id=ifrmCmbLinhaProduto name="ifrmCmbLinhaProduto" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_PRODUTO %>&idAsn1CdAssuntoNivel1=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idAsn1CdAssuntoNivel1'/>&idLinhCdLinha=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idLinhCdLinha'/>&idAsn2CdAssuntoNivel2=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idAsn2CdAssuntoNivel2'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
		<div style="position: absolute; visibility: hidden;"><input type="checkbox" id="chkProdutoTipo" name="chkProdutoTipo" onclick="validaChk(this);" ></div>
	</td>
    <td style="border: #7088c5; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;" class="principalLabel" width="32%"> 
      <input type="checkbox" id="chkManifestacao" name="chkManifestacao" onclick="validaChk(this);" >
      <bean:message key="prompt.todas_manifestacoes"/>
	</td>
  <%}%>
  </tr>
  <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
  <tr> 
		<td style="border: #7088c5; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;" width="17%" align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel2", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td style="border: #7088c5; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;" colspan="2" valign="middle">
    	<iframe id=ifrmCmbLinhaProdutoVariedade name="ifrmCmbLinhaProdutoVariedade" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_VARIEDADE %>&idAsn1CdAssuntoNivel1=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idAsn1CdAssuntoNivel1'/>&idLinhCdLinha=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idLinhCdLinha'/>&idAsn2CdAssuntoNivel2=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idAsn2CdAssuntoNivel2'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
    </td>
		<td style="border: #7088c5; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;" class="principalLabel" width="32%"> 
		  <input type="checkbox" id="chkManifestacao" name="chkManifestacao" onclick="validaChk(this);" >
		  <bean:message key="prompt.todas_manifestacoes"/>
		</td>
  </tr>
  <%}%>

  <!-- MANIFESTACAO -->		
  <tr> 
    <td width="20%" align="right" class="principalLabel"><%= getMessage("prompt.manifestacao", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2" valign="middle">
    	<iframe id="ifrmCmbManifestacao" name="ifrmCmbManifestacao" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_MANIFESTACAO %>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
    	<!--iframe id=ifrmCmbManifestacao name="ifrmCmbManifestacao" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_MANIFESTACAO %>&idMatpCdManifTipo=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idMatpCdManifTipo'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe-->
   	</td>
    <td class="principalLabel" width="32%"> 
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
      <input type="checkbox" id="chkSuperGrupoManif" name="chkSuperGrupoManif" onclick="validaChk(this);" >
      <bean:message key="prompt.todos_supergrupos_manifestacao"/>
<%} else { %>
      <input type="checkbox" id="chkGrupoManifestacao" name="chkGrupoManifestacao" onclick="validaChk(this);" >
      <bean:message key="prompt.todos_grupos_manifestacao"/>
<%}%>
    </td>
  </tr>
  
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
  <tr>
	  <td width="20%" align="right" class="principalLabel"><%= getMessage("prompt.Supergrupo", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
  	  <td colspan="2" valign="middle"> 
	  	  <iframe id="ifrmCmbSuperGrupoManif" name="ifrmCmbSuperGrupoManif" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
  	  </td>
    <td class="principalLabel" width="32%"> 
      <input type="checkbox" id="chkGrupoManifestacao" name="chkGrupoManifestacao" onclick="validaChk(this);" >
      <bean:message key="prompt.todos_grupos_manifestacao"/>
    </td>
  </tr>
<%} %>

  <!-- GRUPO DA MANIFESTACAO -->		
 <tr> 
    <td width="20%" align="right" class="principalLabel"><%= getMessage("prompt.grupomanif", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2" valign="middle">
    	<iframe id=ifrmCmbGrupoManif name="ifrmCmbGrupoManif" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
    	<!--iframe id=ifrmCmbGrupoManif name="ifrmCmbGrupoManif" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_GRUPOMANIF%>&idGrmaCdGrupoManifestacao=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idGrmaCdGrupoManifestacao'/>&idMatpCdManifTipo=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idMatpCdManifTipo'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe-->
   	</td>
    <td class="principalLabel" width="32%"> 
      <input type="checkbox" id="chkGrupo" name="chkGrupo" onclick="validaChk(this);" >
      <bean:message key="prompt.todos_tipos_grupo"/>
    </td>
  </tr>

  	
  <!-- TIPO DE MANIFESTACAO -->		
  <tr>     
    <td width="17%" align="right" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; height: 17px; border: #7088c5; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #f4f4f4"><%= getMessage("prompt.tipomanif", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2" valign="middle" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; text-align: center; height: 17px; border: #7088c5; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #f4f4f4">
    	<iframe id=ifrmCmbLinhaTipoManif name="ifrmCmbLinhaTipoManif" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
    	<!--iframe id=ifrmCmbLinhaTipoManif name="ifrmCmbLinhaTipoManif" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_TIPOMANIF %>&idTpmaCdTpManifestacao=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idTpmaCdTpManifestacao'/>&idGrmaCdGrupoManifestacao=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idGrmaCdGrupoManifestacao'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe-->
   	</td>
    <td width="25%" align="left" class="principalLabel">
    	<table border="0" cellpadding="0" cellspacing="0" align="left">
    	<tr>
    		<td style="cursor: pointer;">
    			<img src="webFiles/images/botoes/lupa.gif" width="18" height="18" title="<bean:message key="prompt.atualizar"/>" onclick="filtrar();">	
    		</td>
    	</tr>
    	</table>	
    
    </td>
  </tr>

 <!-- Pesquisa -->  
 <tr> 
    <td width="20%" align="right" class="principalLabel"><br><bean:message key="prompt.pesquisa"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2" valign="middle" class="principalProdManif">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
          <td align="right" width="2%">
            <html:checkbox value="true" property="receptiva" onclick="atualizaPesquisa();"/> 
          </td>
          <td class="principalLabel" width="10%">
            <div align="left">&nbsp;<bean:message key="prompt.receptiva"/></div>
          </td>
          <td class="principalLabel" width="2%">
          	&nbsp;
          	<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
            	<html:checkbox value="true" property="ativa" onclick="atualizaPesquisa();"/>
			<%}else{ %>
            	&nbsp;
            <%}%>
          </td>
          <td class="principalLabel" width="8%">
          	<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
            	<div align="left">&nbsp;<bean:message key="prompt.ativa"/></div>
            <%}else{ %>
            	&nbsp;
            <%}%>
          </td>
          <td class="principalLabel" width="2%">
            <html:checkbox value="true" property="questionario" onclick="atualizaPesquisa();"/> 
          </td>
          <td class="principalLabel" width="10%">
            <div align="left">&nbsp;<bean:message key="prompt.questionario"/></div>
          </td>
          <% // 91850 - 05/11/2013 - Jaider Alba %>
          <td class="principalLabel" width="2%">
            <html:checkbox value="true" property="prmaInWeb" /> 
          </td>
          <td class="principalLabel" width="10%">
            <div align="left">&nbsp;<bean:message key="prompt.adm.web"/></div>
          </td>
        </tr>
      </table>
      <iframe id=ifrmCmbPesq name="ifrmCmbPesq" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_PESQ %>&idPesqCdPesquisa=0&receptiva=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='receptiva'/>&ativa=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='ativa'/>&questionario=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='questionario'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
   	</td>
    <td width="32%" valign="bottom" align="center"><img src="webFiles/images/botoes/Alterar.gif" width="22" height="23" onClick="abreManifestacao()" class="geralCursoHand" title="<bean:message key='prompt.parametrosManif'/>"></td>
  </tr>
  
  <!-- Destino produto --> 
  <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
  <!-- Pedido do ZECA colocar flag CONSUMIDOR 08/07/05-->
  <tr> 
      <td width="20%" align="right" class="principalLabel">
      <bean:message key="prompt.destinoProduto"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
    </td>
    <td colspan="2" valign="middle">
    	<iframe id=ifrmCmbDestprod name="ifrmCmbDestprod" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_DESTPROD %>&idDeprCdDestinoproduto=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idDeprCdDestinoproduto'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
   	</td>
    <td width="25%">&nbsp;</td>
  </tr>
<%}else{%>
  <tr> 
    <td width="20%" align="right" class="principalLabel">
		<div style="visibility: hidden">
    		<bean:message key="prompt.destinoProduto"/> 
    		<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
    	</div>
    </td>
    <td colspan="2" valign="middle">
		<div style="visibility: hidden">
	    	<iframe id=ifrmCmbDestprod name="ifrmCmbDestprod" src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_DESTPROD %>&idDeprCdDestinoproduto=<bean:write name='administracaoCsAstbProdutoManifPrmaForm' property='idDeprCdDestinoproduto'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
		</div>
   	</td>
    <td width="25%">&nbsp;</td>
  </tr>
<%}%>

  <tr> 
    <td colspan="4" height="100%"> 
		<div id="manifProduto" style="scroll: no; overflow: hidden; width: 700; visibility: visible">
      	<table width="810" border="0" cellspacing="0" cellpadding="0" align="center">
		        <tr> 
		          <td width="4%" class="principalLstCab" valign="top">
		            <div id="Layer1" style="position:absolute; width:59px; height:25px; z-index:1"> 
					  <table width="35" border="0" cellspacing="0" cellpadding="0">
					    <tr align="center"> 
					      <td><img src="webFiles/images/icones/bt_selecionar_nao.gif" onclick="marcaDesmarca(false);" width="16" height="16" class="geralCursoHand"></td>
					      <td><img src="webFiles/images/icones/bt_selecionar_sim.gif" onclick="marcaDesmarca(true);" width="16" height="16" class="geralCursoHand"></td>
					    </tr>
					  </table>
					</div>
		          </td>
		          
		          <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
		          	<td class="principalLstCab" width="8%" align="left">&nbsp;&nbsp;<bean:message key="prompt.linha"/></td>
		          	<td class="principalLstCab" width="10%" align="left">&nbsp;<bean:message key="prompt.produto"/></td>
		          <%}else{%>
		          	<td class="principalLstCab" width="22%" align="left">&nbsp;<bean:message key="prompt.produto"/></td>
		          <%}%>
		          	
		          <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>				
		            <td class="principalLstCab" width="12%" align="left"><bean:message key="prompt.assuntoNivel2"/></td>
		            <td class="principalLstCab" width="15%" align="left"><%= getMessage("prompt.manifAbrev", request)%></td>
		          <%}else{%>
		          	<td class="principalLstCab" width="23%" align="left"><%= getMessage("prompt.manifAbrev", request)%></td>
		          <%}%>
		          
		          <td class="principalLstCab" width="15%" align="left"><%= getMessage("prompt.grupoManifAbrev", request)%></td>
		          <td class="principalLstCab" width="18%" align="left"><%= getMessage("prompt.tipoManifAbrev", request)%></td>
		          <td class="principalLstCab" width="18%" align="left"><bean:message key="prompt.pesquisa"/></td>
		        </tr>    
	      	</table>
      </div>    
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">      
	<tr valign="top"> 
		<td height="160" valign="top" class="principalBordaQuadro"><iframe id="lstArqCarga" name="lstArqCarga" src="AdministracaoCsAstbProdutoManifPrma.do?tela=administracaoLstCsAstbProdutoManifPrma&acao=<%=Constantes.ACAO_VISUALIZAR%>&idLinhCdLinha=<bean:write name="administracaoCsAstbProdutoManifPrmaForm" property="idLinhCdLinha"/>&idAsn1CdAssuntoNivel1=<bean:write name="administracaoCsAstbProdutoManifPrmaForm" property="idAsn1CdAssuntoNivel1"/>&idAsn2CdAssuntoNivel2=<bean:write name="administracaoCsAstbProdutoManifPrmaForm" property="idAsn2CdAssuntoNivel2"/>" width="100%" height="100%" scrolling="no" frameborder="0" ></iframe></td>
	</tr>
</table>

<iframe id=ifrmCamposManif name="ifrmCamposManif" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="0" height="0"></iframe>
<div name="parametrosManifestacao" id="parametrosManifestacao" style="scroll: no; overflow: hidden; width: 0; visibility: hidden">
</div>
        
</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			//ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled= true;
			//ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled= true;
			//confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
			//parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFESTACAOPRODUTO_INCLUSAO_CHAVE%>', parent.document.administracaoCsAstbProdutoManifPrmaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled= false;
			document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled= false;
	
		</script>
</logic:equal>

</html>

<script>
	
	function setaFocoLinha(){
		var a;
		if (ifrmCmbLinhaLinh.document.readyState != 'complete') {
			a = setTimeout('setaFocoLinha()', 100);
		} else {
			clearTimeout(a);
			//ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.focus();
		}
	}
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
		setaFocoLinha();
	<%}%>
	
</script>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>