<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Entity"%>
<%@page import="br.com.plusoft.fw.entity.EntityInstance"%>
<%@page import="br.com.plusoft.csi.adm.form.IdiomaForm"%>
<%@page import="br.com.plusoft.fw.entity.Field"%>
<%@page import="br.com.plusoft.fw.entity.View"%>
<html>
<head>
<meta http-equiv="Page-Enter" content="blendTrans(Duration=1.0)">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>

	function salvar(){
	
		idiomaForm.hashIdiomaViewState.value = document.getElementById("hashIdiomaViewState").value;
		idiomaForm.hashIdiomaNovoViewState.value = document.getElementById("hashIdiomaNovoViewState").value;
		
		idiomaForm.idIdioCdIdiomaAnterior.value = idiomaForm.idIdioCdIdioma.value;
		idiomaForm.acao.value = '<%=Constantes.ACAO_GRAVAR%>';
		idiomaForm.tela.value = 'ifrmDetalheIdioma';
		
		try{
			if(document.getElementsByName("descricaoCampo")[0].type == "text"){
				idiomaForm.campo1.value = document.getElementsByName("descricaoCampo")[0].value;
			}else{
				idiomaForm.campo1.value = document.getElementsByName("descricaoCampo")[0].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(document.getElementsByName("descricaoCampo")[1].type == "text"){
				idiomaForm.campo2.value = document.getElementsByName("descricaoCampo")[1].value;
			}else{
				idiomaForm.campo2.value = document.getElementsByName("descricaoCampo")[1].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(document.getElementsByName("descricaoCampo")[2].type == "text"){
				idiomaForm.campo3.value = document.getElementsByName("descricaoCampo")[2].value;
			}else{
				idiomaForm.campo3.value = document.getElementsByName("descricaoCampo")[2].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(document.getElementsByName("descricaoCampo")[3].type == "text"){
				idiomaForm.campo4.value = document.getElementsByName("descricaoCampo")[3].value;
			}else{
				idiomaForm.campo4.value = document.getElementsByName("descricaoCampo")[3].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(document.getElementsByName("descricaoCampo")[4].type == "text"){
				idiomaForm.campo5.value = document.getElementsByName("descricaoCampo")[4].value;
			}else{
				idiomaForm.campo5.value = document.getElementsByName("descricaoCampo")[4].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(document.getElementsByName("descricaoCampo")[5].type == "text"){
				idiomaForm.campo6.value = document.getElementsByName("descricaoCampo")[5].value;
			}else{
				idiomaForm.campo6.value = document.getElementsByName("descricaoCampo")[5].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(document.getElementsByName("descricaoCampo")[6].type == "text"){
				idiomaForm.campo7.value = document.getElementsByName("descricaoCampo")[6].value;
			}else{
				idiomaForm.campo7.value = document.getElementsByName("descricaoCampo")[6].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(document.getElementsByName("descricaoCampo")[7].type == "text"){
				idiomaForm.campo8.value = document.getElementsByName("descricaoCampo")[7].value;
			}else{
				idiomaForm.campo8.value = document.getElementsByName("descricaoCampo")[7].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(document.getElementsByName("descricaoCampo")[8].type == "text"){
				idiomaForm.campo9.value = document.getElementsByName("descricaoCampo")[8].value;
			}else{
				idiomaForm.campo9.value = document.getElementsByName("descricaoCampo")[8].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(document.getElementsByName("descricaoCampo")[9].type == "text"){
				idiomaForm.campo10.value = document.getElementsByName("descricaoCampo")[9].value;
			}else{
				idiomaForm.campo10.value = document.getElementsByName("descricaoCampo")[9].innerHTML;
			}
		}catch(e){
		}
		
		idiomaForm.submit();
		
	}
	
	function inicio(){
		
		if(document.getElementById("hashIdiomaViewState").value != "" && document.getElementById("hashIdiomaViewState").value != "null"){
			parent.document.getElementById("hashIdiomaViewState").value	= document.getElementById("hashIdiomaViewState").value;
		}
		
		if(document.getElementById("hashIdiomaNovoViewState").value != "" && document.getElementById("hashIdiomaNovoViewState").value != "null"){
			parent.document.getElementById("hashIdiomaNovoViewState").value	= document.getElementById("hashIdiomaNovoViewState").value;
		}
		
		
		<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_GRAVAR %>">
			
			<logic:present name="obrigatoriosPreenchidos">
				alert('<bean:message key="prompt.idiomasObrigatoriosNaoPreenchidos"/>');
				return;		
			</logic:present>
			
			//if(idiomaForm.chavePrimaria.value != '' && idiomaForm.chavePrimaria.value != '0'){
			<%if(request.getAttribute("gravarIdioma") != null && ((String)request.getAttribute("gravarIdioma")).equals("true")){%>
				alert('<bean:message key="prompt.dados_gravados_com_sucesso"/>');
			<%}else{%>
				alert('<bean:message key="prompt.alert.dadosConfirmadosGravacaoSeraRealizadaAoSalvar"/>');
			<%}%>
			window.close();
			
		</logic:equal>
	
	}
  
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  	window.open(theURL,winName,features);
	}
	
	function setFunction(cRetorno, campo){
		if(campo=="document.idiomaForm.divCampo1"){
			document.getElementsByName("descricaoCampo")[0].innerHTML = cRetorno;
			eval(campo).value=cRetorno;
		}
		if(campo=="document.idiomaForm.divCampo2"){
			document.getElementsByName("descricaoCampo")[1].innerHTML = cRetorno;
			eval(campo).value=cRetorno;
		}
		if(campo=="document.idiomaForm.divCampo3"){
			document.getElementsByName("descricaoCampo")[2].innerHTML = cRetorno;
			eval(campo).value=cRetorno;
		}
		if(campo=="document.idiomaForm.divCampo4"){
			document.getElementsByName("descricaoCampo")[3].innerHTML = cRetorno;
			eval(campo).value=cRetorno;
		}
		
	}
	
	function fecharTela(){
		<logic:present name="obrigatoriosPreenchidos">
			idiomaForm.acao.value = 'limparIdiomasSessao';
			idiomaForm.submit();
		</logic:present>
	}
	
	function botaoSairClick(){
		<!--logic:present name="obrigatoriosPreenchidos"-->
			if(confirm('<bean:message key="prompt.dadosIdiomaSeraoPerdidos"/>')){
				fecharTela();
				parent.window.close();	
			}
		<!--/logic:present-->
	}
	
	
</script>

</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" topmargin="0">
<html:form styleId="idiomaForm" action="/Idioma.do"> 

	<input type="hidden" id="hashIdiomaViewState" name="hashIdiomaViewState" value="<%=request.getAttribute("hashIdiomaViewState")%>"/>
	<input type="hidden" id="hashIdiomaNovoViewState" name="hashIdiomaNovoViewState" value="<%=request.getAttribute("hashIdiomaNovoViewState")%>"/>

	<html:hidden property="tela"/>
	<html:hidden property="acao"/>
	<html:hidden property="arquivoXml"/>
	<html:hidden property="idIdioCdIdioma"/>
	<html:hidden property="idIdioCdIdiomaAnterior"/>
	<html:hidden property="campo1"/>
	<html:hidden property="campo2"/>
	<html:hidden property="campo3"/>
	<html:hidden property="campo4"/>
	<html:hidden property="campo5"/>
	<html:hidden property="campo6"/>
	<html:hidden property="campo7"/>
	<html:hidden property="campo8"/>
	<html:hidden property="campo9"/>
	<html:hidden property="campo10"/>
	
	<input type="hidden" name="divCampo1"></input>
	<input type="hidden" name="divCampo2"></input>
	<input type="hidden" name="divCampo3"></input>
	<input type="hidden" name="divCampo4"></input>
	<input type="hidden" name="divCampo5"></input>
	<input type="hidden" name="divCampo6"></input>
	<input type="hidden" name="divCampo7"></input>
	<input type="hidden" name="divCampo8"></input>
	<input type="hidden" name="divCampo9"></input>
	<input type="hidden" name="divCampo10"></input>
	
	<html:hidden property="chavePrimaria"/>

	<div id="camposTemp" style="position:absolute; width:0px; height:0px; visibility:hidden"></div>

	<table width="100%" border="0" cellspacing="4" cellpadding="0" align="center">

		
			<% 
				Vo vo = new Vo();
				
				Vector v = (Vector)request.getAttribute("detalheIdiomaVector");
				if(v != null){
					for(int i =0; i<v.size();i++){
						vo = (Vo)v.get(i);
					}
				}				
			%>
					
			<%
			
				String objName = "";
				String objSize = "0";
				int objMaxlenght = 0;
				Entity entity = null;
				entity = EntityInstance.getEntity(MAConstantes.PACKAGE_ENTITY_ADM_KERNEL + ((IdiomaForm)request.getAttribute("baseForm")).getArquivoXml());
				Vector lista = entity.getFieldByOrder();
				int campoNum = 0;
				
				for (int i = 0; i < lista.size(); i++){
					Field field = (Field)lista.get(i);
					objName = field.getName();
					objMaxlenght = field.getLen();
					View view = field.getViewByName("detidioma");
					
					//Caso o campo o nao seja um campo fisico ou esteja configurado na view que o atributo nao pode ser visualizado deve-se pular esse campo
					if (field.isNoField() || view == null || (view != null && !view.getVisible())){continue;}
					campoNum++;
			%>

				<tr>
					
					<td width="20%" align="center" class="principalLabel">
						<%= getMessage(field.getLabel(), request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
					
					<td width="70%">
						<% if(objMaxlenght >=2000){%>
							<div id="descricaoCampo" name="descricaoCampo" class="text" style="position:relative; overflow: auto; width: 300px; height: 50px; background-color: #FFFFFF; border: 1px none #000000; display: block;"><%=vo.getField("campo"+campoNum)!=null?vo.getField("campo"+campoNum):""%></div>
							<script>
								var objDiv = eval("idiomaForm.divCampo<%=campoNum%>");
								objDiv.value = '<%=vo.getFieldAsString("campo"+campoNum).replace("\'", "\\'").replace("\r\n", "\\r\\n")%>'
							</script>
						<%}else{ %>
							<input type="text" id="descricaoCampo" name="descricaoCampo" class="text" style="width:300px" maxlength="<%= objMaxlenght%>" value="<%=vo.getFieldAsString("campo"+campoNum).replace("\"", "&quot;")%>"></input>
						<%} %>
					</td>
					
					<td width="10%">
						<% if(objMaxlenght >=2000){%>
							<img src="webFiles/images/botoes/bt_CriarCarta.gif" name="imgEditor" width="25" height="22" class="geralCursoHand" border="0" title="<bean:message key="prompt.tituloCorresp"/>" onClick="MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=document.idiomaForm.divCampo<%=campoNum%>&carta=true&tamanho=<%=objMaxlenght%>','Documento','width=850,height=494,top=0,left=0')"> 
						<% } %>
					</td>
				
				</tr>
				
			<% } %>
				
			<script>
				parent.document.getElementById("tdIdioma").innerHTML = '<marquee loop="1" scrollamount="10" width="100" behavior="slide"><%=(String)request.getAttribute("idio_ds_idioma")%></marquee>'
			</script>
			
</table>

</html:form>
</body>
</html>