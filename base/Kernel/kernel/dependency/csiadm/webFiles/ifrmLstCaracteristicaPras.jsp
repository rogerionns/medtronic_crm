<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">

var result=0;

function excluirCaract(idPrca,idCapr,idRtcp){
	if (!confirm('<bean:message key="prompt.deseja_excluir_esta_caracteristica"/>?')){
		return false;
	}

	var idAsn1 = parent.parent.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value;
	var idAsn2 = parent.parent.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value;

	administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idAsn1CdAssuntonivel1'].value = idAsn1;
	administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idAsn2CdAssuntonivel2'].value = idAsn2;
	
	administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idPrcaCdProdutoCaract'].value = idPrca;
	administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd'].value = idCapr;
	administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd'].value = idRtcp;
	
	administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = '<%=MAConstantes.TELA_LST_CARACTERISTICA_PRAS%>';
	administracaoCsCdtbProdutoAssuntoPrasForm.acao.value = '<%=Constantes.ACAO_EXCLUIR%>';
	
	administracaoCsCdtbProdutoAssuntoPrasForm.submit();

}

</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbProdutoAssuntoPrasForm" action="/AdministracaoCsCdtbProdutoAssuntoPras.do">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="csCatbProdutoCaractPrcaVo.idAsn1CdAssuntonivel1"/>
<html:hidden property="csCatbProdutoCaractPrcaVo.idAsn2CdAssuntonivel2"/>
<html:hidden property="csCatbProdutoCaractPrcaVo.idPrcaCdProdutoCaract"/>
<html:hidden property="csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd"/>
<html:hidden property="csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd"/>

<div id="layer" style="width:100%; height:50px; overflow: auto;">
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="left">
	<logic:iterate id="ccttrtVector" name="csCatbProdutoCaractPrcaVector">
		<script>
			result++;
		</script> 
		<tr>
			<td width="5%" class="principalLstPar">&nbsp;<img src="webFiles/images/botoes/lixeira18x18.gif" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="excluirCaract(<bean:write name="ccttrtVector" property="idPrcaCdProdutoCaract" />,<bean:write name="ccttrtVector" property="idCaprCdCaracteristicaProd" />,<bean:write name="ccttrtVector" property="idRtcpRespTabCaractProd" />);">
				<input type="hidden" name="idPrcaCdProdutoCaract" value='<bean:write name="ccttrtVector" property="idPrcaCdProdutoCaract" />'>
				<input type="hidden" name="idCaprCdCaracteristicaProd" value='<bean:write name="ccttrtVector" property="idCaprCdCaracteristicaProd" />'>
				<input type="hidden" name="idRtcpRespTabCaractProd" value='<bean:write name="ccttrtVector" property="idRtcpRespTabCaractProd" />'>
				<input type="hidden" name="caprDsCaracteristicaProd" value='<bean:write name="ccttrtVector" property="caprDsCaracteristicaProd" />'>
				<input type="hidden" name="rtcpDsRespTabCaractProd" value='<bean:write name="ccttrtVector" property="rtcpDsRespTabCaractProd" />'>
			</td>
			<td width="50%" class="principalLstPar">&nbsp;<bean:write name="ccttrtVector" property="caprDsCaracteristicaProd" /> </td>
			<td width="45%" class="principalLstPar">&nbsp;<bean:write name="ccttrtVector" property="rtcpDsRespTabCaractProd" /> </td>
		</tr>
	</logic:iterate>	
	</table>			
</div>	
</html:form>
</body>
</html>