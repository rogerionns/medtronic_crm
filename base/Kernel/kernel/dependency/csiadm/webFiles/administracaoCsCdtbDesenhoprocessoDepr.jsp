<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="csCdtbDesenhoprocessoDeprForm" action="/ListaTodasCsCdtbDesenhoprocessoDepr.do">

<script>var possuiRegistros=false;</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:present name="csCdtbDesenhoprocessoDeprVector">
	  <logic:iterate id="resultVector" name="csCdtbDesenhoprocessoDeprVector">
	  <script>possuiRegistros=true;</script>
	  <tr> 
	    <td width="3%" class="principalLstParMao">
	      <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="resultVector" property="field(id_depr_cd_desenhoprocesso)" />')">
	    </td>		  
	    <td class="principalLstParMao" width="10%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="resultVector" property="field(id_depr_cd_desenhoprocesso)" />')">
	      	&nbsp;&nbsp;<bean:write name="resultVector" property="field(id_depr_cd_desenhoprocesso)" />
	    </td>
	    <td class="principalLstParMao" align="left" width="65%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="resultVector" property="field(id_depr_cd_desenhoprocesso)" />')">
	      	&nbsp;&nbsp;<bean:write name="resultVector" property="field(depr_ds_desenhoprocesso)" />
		</td>
		<td class="principalLstParMao" align="left" width="22%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="resultVector" property="field(id_depr_cd_desenhoprocesso)" />')">
	      	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="resultVector" property="field(depr_dh_inativo)" />
		</td>
	  </tr>
	  </logic:iterate>
  </logic:present>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>