<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<style>
.intercalaLst0 {  background-color: #f4f4f4; font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-color: black black #CCCCCC; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;}
.intercalaLst1 {  background-color: #CED8E1; font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-color: black black #CCCCCC; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px;}
</style>

<html:html>
<head>

<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<style type="text/css">
	.formrow { text-align: left;  font-style: normal; margin-left: 5px;}	
	.conteudoResultado { text-align: left; font-style: normal; margin-left: 5px; }
</style>

</head>

<body>
	   	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
		    <tr> 
		      <td width="100px;" class="principalLstCab" height="13">&nbsp;Comando</td>
		      <td class="principalLstCab" height="13">&nbsp;Resultado</td>
		    </tr>
		    <logic:present name="ping">
			    <tr class="intercalaLst0"> 
			      <td width="50px;" class="principalLabel">&nbsp;Ping</td>
			      <td class="principalLabel">&nbsp;<bean:write name="retornoVo" property="field(resultado_ping)"/></td>
			    </tr>
			</logic:present>
			<logic:present name="telnet">
			    <logic:present name="ping">
			    	<tr class="intercalaLst1"> 
			    </logic:present>
			    <logic:notPresent name="ping">
			    	<tr class="intercalaLst0"> 
			    </logic:notPresent> 
			      <td width="50px;" class="principalLabel">&nbsp;Telnet</td>
			      <td class="principalLabel">&nbsp;<bean:write name="retornoVo" property="field(resultado_telnet)"/></td>
			    </tr>
			</logic:present>
			<logic:present name="recebimento">
			    <logic:present name="ping">
			        <logic:present name="telnet">
			    		<tr class="intercalaLst0"> 
			    	</logic:present>
			    </logic:present>
			    <logic:present name="ping">
			        <logic:notPresent name="telnet">
			    		<tr class="intercalaLst1"> 
			    	</logic:notPresent>
			    </logic:present>
			    <logic:notPresent name="ping">
			        <logic:notPresent name="telnet">
			    		<tr class="intercalaLst0"> 
			    	</logic:notPresent>
			    </logic:notPresent>
			    <logic:notPresent name="ping">
			        <logic:present name="telnet">
			    		<tr class="intercalaLst1"> 
			    	</logic:present>
			    </logic:notPresent>
			      <td width="50px;" class="principalLabel">&nbsp;Recebimento</td>
			      <td class="principalLabel">&nbsp;<bean:write name="retornoVo" property="field(resultado_recebimento)"/></td>
			    </tr>
			</logic:present>
			<logic:present name="envio">
			    <logic:present name="ping">
			        <logic:present name="telnet">
			    		<tr class="intercalaLst0"> 
			    	</logic:present>
			    </logic:present>
			    <logic:present name="ping">
			        <logic:notPresent name="telnet">
			    		<tr class="intercalaLst1"> 
			    	</logic:notPresent>
			    </logic:present>
			    <logic:notPresent name="ping">
			        <logic:notPresent name="telnet">
			    		<tr class="intercalaLst0"> 
			    	</logic:notPresent>
			    </logic:notPresent>
			    <logic:notPresent name="ping">
			        <logic:present name="telnet">
			    		<tr class="intercalaLst1"> 
			    	</logic:present>
			    </logic:notPresent>
			      <td width="50px;" class="principalLabel">&nbsp;Envio</td>
			      <td class="principalLabel">&nbsp;<bean:write name="retornoVo" property="field(resultado_envio)"/></td>
			    </tr>
			</logic:present>
		  </table>
 
</body>

</html:html>