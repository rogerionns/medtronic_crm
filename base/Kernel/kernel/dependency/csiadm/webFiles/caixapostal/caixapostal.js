    	
    	
    	var envio = "N";

    	$(".btnreceber").bind("click", function(event) {
    		envio = "N";
    		validarCaixaPostal();
    	});

    	$(".btnenviar").bind("click", function(event) {
    		envio = "S";
    		validarCaixaPostal();
    	});

        var optPingAux   = "N";
        var optTelnetAux = "N";
        var optRecebimentoAux = "N";
        var optEnvioAux = "N";
        var dsEmailAux = "";
        var optPoolAux = "N";
        //Chamado: 86614 - 16/09/2013 - Carlos Nunes
        var optSslAux = "N";

        function resetOpcao()
        {
        	optPingAux   = "N";
            optTelnetAux = "N";
            optRecebimentoAux = "N";
            optEnvioAux = "N";
            dsEmailAux = "";
            optPoolAux = "N";
            //Chamado: 86614 - 16/09/2013 - Carlos Nunes
            optSslAux = "N";
        }
        
    	function validarCaixaPostal() {
    		resetOpcao(); 
    		$.modal.show("/csiadm/caixapostal/AbrirOpcaoValidacao.do", {
				validarEnvio       : envio
			}, opcaoValidacao, { width:400, height: 300, 
				                 buttons: [{ label : "Executar", 
					                         click : function(event) {
				                	                  dsEmailAux = $(".dsEmail").val();
				                	                  dsEmailEnvioAux = $(".dsEmailEnvio").val();
								                	 if(optEnvioAux == "S" && (dsEmailAux == "" || dsEmailEnvioAux == ""))
				                                     {
				                                         alert("� obrigat�rio preencher algum e-mail v�lido.");
				                                         return false;
				                                     }
													 $.modal.showconfirm(essa_acao_poder_demorar, 
															             { buttons: [
							                                         				 { label : "Sim", click : function(event) {
									                                         			$.modal.show("/csiadm/caixapostal/ValidarCaixaPostal.do", {
							                                         						validarEnvio       : envio,
							                                         						capoDsServidorRec  : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsServidorRec.value,
							                                         						capoDsPortaRec     : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPortaRec.value,
							                                         						capoDsProtocoloRec : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsProtocoloRec.value,
							                                         						capoDsUsuarioRec   : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsUsuarioRec.value,
							                                         						capoDsSenhaRec     : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsSenhaRec.value,
							                                         						capoDsServidorEnv  : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsServidorEnv.value,
							                                         						capoDsPortaEnv     : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPortaEnv.value,
							                                         						capoDsProtocoloEnv : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsProtocoloEnv.value,
							                                         						capoDsUsuarioEnv   : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsUsuarioEnv.value,
							                                         						capoDsSenhaEnv     : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsSenhaEnv.value,
							                                         						capoDsPoolRec      : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPoolRec.value,
							                                         						capoDsPoolEnv      : document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPoolEnv.value,
							                                         						outrosParametrosRec: document.administracaoCsCdtbCaixaPostalCapoForm.capoDsBancodadosRec.value,
							                                         						outrosParametrosEnv: document.administracaoCsCdtbCaixaPostalCapoForm.capoDsBancodadosEnv.value,
							                                         						optPing            : optPingAux,
							                                         						optTelnet          : optTelnetAux,
							                                         						optRecebimento     : optRecebimentoAux,
							                                         						optEnvioMail       : optEnvioAux,
							                                         						dsEmail            : dsEmailAux,
							                                         						dsEmailEnvio       : dsEmailEnvioAux,
							                                         						optPool            : optPoolAux,
							                                         						optSsl             : optSslAux
							                                         					 }, 
							                                         					    resultadoValidacao, { width:600, height: 350, 
								                                         					                      buttons: [{ label : "Sair", click : $.modal.close }] 
							                               							                            }
		                               							                      );
							                                         				}
							                                         				},
							                                         				{ label : "N�o", click : $.modal.close }
							                                         			] });
			                                                                } },
			                               { label : "Sair", click : $.modal.close }]
			                   },
	         				     function() { 
	         					     $(".optEnvioMail").bind("click", function() {
	          					                                    if( $(".optEnvioMail").attr("checked") == "checked")
	          					                                    {
	          					                                    	$(".dsEmail").attr("disabled", false);  
	          					                                    	$(".dsEmailEnvio").attr("disabled", false);  
	          					                                    }
	          					                                    else
	          					                                    {
	          					                                    	$(".dsEmail").attr("disabled", true); 
	          					                                    	$(".dsEmailEnvio").attr("disabled", true); 
	          					                                    }
          					                                  }),
	                                 $(".optPing").bind("click", function() {
          					                                    if( $(".optPing").attr("checked") == "checked")
          					                                    {
          					                                    	optPingAux = "S";  
          					                                    }
          					                                    else
          					                                    {
          					                                    	optPingAux = "N"; 
          					                                    }
          					                                  }),
          					        $(".optTelnet").bind("click", function() {
          					                                    if( $(".optTelnet").attr("checked") == "checked")
          					                                    {
          					                                    	optTelnetAux = "S";  
          					                                    }
          					                                    else
          					                                    {
          					                                    	optTelnetAux = "N"; 
          					                                    }
          					                                  }),
	                                $(".optPool").bind("click", function() {
							                                    if( $(".optPool").attr("checked") == "checked")
							                                    {
							                                    	optPoolAux = "S"; 
							                                    }
							                                    else
							                                    {
							                                    	optPoolAux = "N"; 
							                                    }
							                                  }),
          					        $(".optRecebimento").bind("click", function() {
          					                                    if( $(".optRecebimento").attr("checked") == "checked")
          					                                    {
          					                                    	optRecebimentoAux = "S";  
          					                                    }
          					                                    else
          					                                    {
          					                                    	optRecebimentoAux = "N"; 
          					                                    }
          					                                  }),
          					        $(".optEnvioMail").bind("click", function() {
          					                                    if( $(".optEnvioMail").attr("checked") == "checked")
          					                                    {
          					                                    	optEnvioAux = "S";  
          					                                    	dsEmailAux = $(".dsEmail").val();
          					                                    	dsEmailEnvioAux = $(".dsEmailEnvio").val();
          					                                    }
          					                                    else
          					                                    {
          					                                    	optEnvioAux = "N"; 
          					                                    	dsEmailAux = "";
          					                                    	dsEmailEnvioAux = "";
          					                                    }
          					                                  }),
	                                $(".optSsl").bind("click", function() {  //Chamado: 86614 - 16/09/2013 - Carlos Nunes
							                                    if( $(".optSsl").attr("checked") == "checked")
							                                    {
							                                    	optSslAux = "S"; 
							                                    }
							                                    else
							                                    {
							                                    	optSslAux = "N"; 
							                                    }
							                                  });
	         				      }
			);
    	}