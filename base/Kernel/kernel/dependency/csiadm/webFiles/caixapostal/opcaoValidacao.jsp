<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">


<html:html>
<head>

<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<style type="text/css">
	.formrow { text-align: left;  font-style: normal; margin-left: 5px;}	
	.conteudoResultado { text-align: left; font-style: normal; margin-left: 5px; }
</style>

<script>

	function validaEmail(obj){
		if(obj.value != ""){
			var cEmail = obj.value;
			obj.value = trim(cEmail.toLowerCase());
	
			if(window.top.jQuery) {
				cEmail = window.top.jQuery.trim(cEmail);
			} else if (window.dialogArguments) {
				cEmail = window.dialogArguments.top.jQuery.trim(cEmail);
			}
			
			if (cEmail.search(/\S/) != -1) {
				regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{1,}\.[A-Za-z0-9]{2,}/
				if (cEmail.length < 7 || cEmail.search(regExp) == -1){
					alert ('<bean:message key="prompt.alert.email.correto" />');
					obj.focus();
				    return false;
				}						
			}
			num1 = cEmail.indexOf("@");
			num2 = cEmail.lastIndexOf("@");
			if (num1 != num2){
			    alert ('<bean:message key="prompt.alert.email.correto" />');
			    obj.focus();
				return false;
			}
		}
	}

</script>

</head>

<body>
	<div style="clear:left;height: 20px;">&nbsp;</div>
	<div class="formrow" style="display: block;">
		Selecione os itens que deseja validar
	</div>
	<div style="clear:left;height: 20px;">&nbsp;</div>
	<div style="clear:left;width:98%; display: block;">
		<p>
			<input type="checkbox" class="optPing" name="optPing"   id="optPing"/>&nbsp;Ping
			<br/>
			<input type="checkbox" class="optTelnet" name="optTelnet" id="optTelnet"/>&nbsp;Telnet
			<br/>
			<input type="checkbox" class="optPool" name="optPool" id="optPool"/>&nbsp;Validar por Pool
			<br/>
			<% if(request.getParameter("validarEnvio").equals("N")){ %>
			<input type="checkbox" class="optRecebimento" name="optRecebimento" id="optRecebimento"/>&nbsp;Recebimento de E-mail
			<br/>
			<input type="checkbox" class="optSsl" name="optSsl" id="optSsl"/>&nbsp;SSL <!-- Chamado: 86614 - 16/09/2013 - Carlos Nunes -->
			<br/>
			<% } %>
			<% if(request.getParameter("validarEnvio").equals("S")){ %>
				<br/>
				<input type="checkbox" class="optEnvioMail" name="optEnvioMail" id="optEnvioMail"/>&nbsp;Envio de E-mail
				<br/>
				&nbsp;E-mail que enviar� a confirma��o da valida��o de envio.
				<br/>
				&nbsp;<input type="text" class="dsEmailEnvio" name="dsEmailEnvio" id="dsEmailEnvio" maxlength="100" disabled="disabled" style="width: 300px;" onblur="validaEmail(this)"/>
				<br/>
				&nbsp;E-mail que receber� a confirma��o da valida��o de envio.
				<br/>
				&nbsp;<input type="text" class="dsEmail" name="dsEmail" id="dsEmail" maxlength="100" disabled="disabled" style="width: 300px;" onblur="validaEmail(this)"/>				
			<% } %>
		</p>
	</div>
	
	<div style="clear:left;height: 20px;">&nbsp;</div>
</body>

</html:html>