<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
</head>
<script type="text/javascript">
	function iniciarTela()
	{
	    var idAsn1 = parent.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	    
		document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = idAsn1;
		
		parent.carregaAsn2(); 
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela()">
<html:form action="/AdministracaoCsAstbProdutoassuntogfpermPagp.do" styleId="administracaoCsAstbProdutoassuntogfpermPagpForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
  
  <html:select property="csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" styleClass="principalObjForm"  style="width:400px" onchange="parent.carregaAsn2();">
	<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
	<logic:present name="listAsn1">
		<html:options collection="listAsn1" property="idAsn1CdAssuntoNivel1" labelProperty="asn1DsAssuntoNivel1"/>
	</logic:present>
  </html:select> 
</html:form>
</body>
</html>