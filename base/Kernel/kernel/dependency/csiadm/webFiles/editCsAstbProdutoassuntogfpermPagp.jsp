<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<%@page import="br.com.plusoft.fw.app.Application"%>
<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	
function inicio(){
	showError('<%=request.getAttribute("msgerro")%>');
	if (ifrmCmbAsn1.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value > 0){
		carregaManifGfperm();
	}
	validarPermissao();
}	

function validarPermissao(){
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_GRUPOFUNCIONARIO_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_GRUPOFUNCIONARIO_ALTERACAO_CHAVE%>', parent.document.administracaoCsAstbProdutoassuntogfpermPagpForm.imgGravar);
			desabilitaCamposGrupofuncperm();
		}
	</logic:equal>
}

function desabilitaCamposGrupofuncperm(){
	try{
		setPermissaoImageDisable('', document.administracaoCsAstbProdutoassuntogfpermPagpForm.imgAvancar);
		setPermissaoImageDisable('', document.administracaoCsAstbProdutoassuntogfpermPagpForm.imgVoltar);
		desabilitarCampos();	

		lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].disabled=true;
		lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm.todos.disabled=true;

		lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.disabled=true;
		lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.todos.disabled=true;
	}
	catch(e){
		setTimeout("desabilitaCamposGrupofuncperm();", 300);
	}
}


function atualizaListaFuncionarios(bOrigemClick) {
	
	if(bOrigemClick){
		document.administracaoCsAstbProdutoassuntogfpermPagpForm["csCdtbFuncionarioFuncVo.funcNmFuncionario"].value="";
	}
	
	oldAcao = document.administracaoCsAstbProdutoassuntogfpermPagpForm.acao.value;
	oldTela = document.administracaoCsAstbProdutoassuntogfpermPagpForm.tela.value;
	
	document.administracaoCsAstbProdutoassuntogfpermPagpForm.acao.value = '<%= Constantes.ACAO_CONSULTAR %>';
	document.administracaoCsAstbProdutoassuntogfpermPagpForm.tela.value = '<%= MAConstantes.TELA_IFRM_LST_GRUPOFUNCIONARIO_PAGP %>';
	document.administracaoCsAstbProdutoassuntogfpermPagpForm.target = lstFuncionarios.name;
	document.administracaoCsAstbProdutoassuntogfpermPagpForm.submit();
	
	document.administracaoCsAstbProdutoassuntogfpermPagpForm.acao.value = oldAcao;
	document.administracaoCsAstbProdutoassuntogfpermPagpForm.tela.value = oldTela;
}	

function moveToRight() {
	selecionou = 0;
	if (lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].length == 0) {
		alert('<bean:message key="prompt.nao_existem_grupo_funcionarios_na_lista_para_serem_selecionados"/>');
		return false;
	}
	for (i = 0; i < lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].length; i++) {
		if (lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].options[i].selected) { 
			selecionou = 1;
			if (!(verificaExistencia(lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].options[i].value, lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo))) {
				alert('<bean:message key="prompt.o_grupofuncionario"/> ' + lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].options[i].text + ' <bean:message key="prompt.ja_foi_selecionado_para_esta_manifestacao"/>');
			}
			else {
				lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.options[lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.length] = new Option(lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].options[i].text, lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].options[i].value);											
				lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].options[i] = null;
				i--;
			}
		}
	}
	lstFuncionarios.atualizaTotal(lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].length);
	lstFuncionariosGrupo.atualizaTotal(lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.length);
	if (selecionou == 0)
		alert('<bean:message key="prompt.Por_favor_selecione_um_grupo_funcionario"/>');
}

function moveToLeft() {
	selecionou = 0;
	if (lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.length == 0) {
		alert('<bean:message key="prompt.nao_existem_grupos_funcionarios_na_lista_para_serem_selecionados"/>');
		return false;
	}
	for (i = 0; i < lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.length; i++) {
		if (lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.options[i].selected) { 
			selecionou = 1;
			if ((verificaExistencia(lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.options[i].value, lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm']))) {
				lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].options[lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].length] = new Option(lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.options[i].text, lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.options[i].value);											
			}
			lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.options[i] = null;
			i--;
		}
	}
	lstFuncionarios.atualizaTotal(lstFuncionarios.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].length);
	lstFuncionariosGrupo.atualizaTotal(lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.length);
	if (selecionou == 0)
		alert('<bean:message key="prompt.Por_favor_selecione_um_grupo_funcionario"/>');
}

// Verifica se o elemento ja foi copiado
function verificaExistencia(valor, obj) {
	for (j = 0; j < obj.length; j++) {
		if (obj.options[j].value == valor)
			return false;	
	}
	return true;
}

function filtrar() {
	if (event.keyCode == 13)
		atualizaListaFuncionarios(false);
}

function getListaFuncionariosGrupo() {
	var html = "";
	for (i = 0; i < lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.length; i++) {
		html += "<input type=\"hidden\" name=\"idFuncCdFuncionarioGrupo\" value=\"" + lstFuncionariosGrupo.document.administracaoCsAstbProdutoassuntogfpermPagpForm.idFuncCdFuncionarioGrupo.options[i].value + "\">";
	}
	document.getElementById("listaFuncionarios").innerHTML = html;
}

var erroAsn1 = new Number(0);

function carregaAsn1() 
{ 
	try
	{
		var url="AdministracaoCsAstbProdutoassuntogfpermPagp.do?tela=ifrmCmbAsn1&acao=<%=Constantes.ACAO_FITRAR%>";
    	url += "&csAstbProdutoassuntogfpermPagpVo.csCdtbLinhaLinhVo.idLinhCdLinha=" + document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;
     	url += "&csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
    	
     	if(parent.admIframe.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value != document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value)
        {
			parent.admIframe.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = 0;	    	       
	    	parent.admIframe.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = 0;
	    	parent.admIframe.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = 0;
	    	document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = 0;
	    	document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = 0;
	    }

 		ifrmCmbAsn1.location.href = url;
 	}
 	catch(e)
 	{
 		erroAsn1++;
 		
 		if(erroAsn1 < 5)
 		{
 			setTimeout('carregaAsn1()',500);
 		}
 		else
 		{
 			alert('Ocorreu um erro na fun��o carregaAsn1(). Erro: ' + e.description());
 		}
 	}
}

var erroAsn2 = new Number(0);
function carregaAsn2()
{
	 try
	 {
		 var url="AdministracaoCsAstbProdutoassuntogfpermPagp.do?tela=ifrmCmbAsn2&acao=<%=Constantes.ACAO_FITRAR%>";
	     url += "&csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + ifrmCmbAsn1.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	     if(parent.admIframe.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value != document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value)
	     {
	    	parent.admIframe.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = 0;
	    	parent.admIframe.document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = 0;
	    	document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = 0;
	     }

	 	ifrmCmbAsn2.location.href = url;
		 	
		 <%if (!Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
		 	carregaManifGfperm();
		 <%}%>
	}
	catch(e)
 	{
 		erroAsn2++;
 		
 		if(erroAsn2 < 5)
 		{
 			setTimeout('carregaAsn2()',500);
 		}
 		else
 		{
 			alert('Ocorreu um erro na fun��o carregaAsn2(). Erro: ' + e.description());
 		}
 	}
}

var erroManif = new Number(0);
function carregaManifGfperm(){

	try
	{
		var url="";
		
		//atualiza lista de grupos funcionario
		url = "AdministracaoCsAstbProdutoassuntogfpermPagp.do?tela=<%= MAConstantes.TELA_IFRM_LST_GRUPOFUNCIONARIO_PAGP%>";
		url = url + "&acao=<%= Constantes.ACAO_CONSULTAR %>";
	
		lstFuncionarios.location.href = url;
	
		//atualiza lista de grupos j� associados
		url = "AdministracaoCsAstbProdutoassuntogfpermPagp.do?tela=<%= MAConstantes.TELA_IFRM_LST_GRUPOFUNCIONARIO_PRODUTOASSUNTO %>";
		url = url + "&acao=<%= Constantes.ACAO_CONSULTAR %>";
		url = url + "&csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + ifrmCmbAsn1.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
		 	url = url + "&csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + ifrmCmbAsn2.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		 <%}else{%>
		 	url = url + "&csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=1";
		  <%}%>
		 
		lstFuncionariosGrupo.location.href = url;	
	}
	catch(e)
 	{
 		erroManif++;
 		
 		if(erroManif < 5)
 		{
 			setTimeout('carregaManifGfperm()', 500);
 		}
 		else
 		{
 			alert('Ocorreu um erro na fun��o carregaManifGfperm(). Erro: ' + e.description());
 		}
 	}
}

</script>
<body class= "principalBgrPageIFRM" onload="inicio();carregaAsn1();">

<html:form styleId="administracaoCsAstbProdutoassuntogfpermPagpForm" action="/AdministracaoCsAstbProdutoassuntogfpermPagp.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>	
	<html:hidden property="csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
	<html:hidden property="csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
	
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr id='trLinha' style="visibility: hidden;"> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.linha"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%" colspan="2"> 
			<html:select property="csAstbProdutoassuntogfpermPagpVo.csCdtbLinhaLinhVo.idLinhCdLinha" styleClass="principalObjForm"  style="width:400px" onchange="carregaAsn1();">
				<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
				<logic:present name="listLinha">
					<html:options collection="listLinha" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
				</logic:present>
			</html:select>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
         <%if (!Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")){%>
         	<script>document.getElementById('trLinha').style.visibility = 'visible';</script>
        <%}%>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.produto"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%" colspan="2">
			<iframe name="ifrmCmbAsn1" id="ifrmCmbAsn1" src="AdministracaoCsAstbProdutoassuntogfpermPagp.do?tela=ifrmCmbAsn1&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
       
        <tr id='trVariedade' style="visibility: hidden;"> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.variedade"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%" colspan="2"> 
			<iframe name="ifrmCmbAsn2" id="ifrmCmbAsn2" src="AdministracaoCsAstbProdutoassuntogfpermPagp.do?tela=ifrmCmbAsn2&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
         <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
         	<script>document.getElementById('trVariedade').style.visibility = 'visible';</script>
        <%}%>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <!-- html:checkbox value="true" property="inInativo"/></td-->
            
                <td class="principalLabel" width="17%">&nbsp;
                </td>
              </tr>
            </table>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        
        <!-- INICIO ABA DE FUNCIONARIO -->
        <tr>
        	<td colspan="4">
	        	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td height="254">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"	align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
								<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado" id="funcionario" name="funcionario">
										<bean:message key="prompt.grupofunc" />
									</td>
								</tr>
								</table>
							</td>
							<td width="4">
								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
							</td>
						</tr>
						</table>

						<table width="100%" border="0" cellspacing="0" cellpadding="0"	align="center">
						<tr>
							<td valign="top" class="principalBgrQuadro" height="70">
								
								<!-- TABELA DE FORA -->
								<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
								<tr>
									<!-- LISTA DE GRUPO DE FUNCIONARIOS -->
									<td width="48%" height="30">
										<table border="0" cellpadding="0" cellspacing="4" width="100%" height="100%">
										<tr>
											<td class="principalLabel" height="30">&nbsp;</td>	
											<td class="principalLabel" height="30">&nbsp;</td>	
										</tr>
										<tr>
											<td class="principalLabel" colspan="2" valign="top">
												<iframe name="lstFuncionarios" id="lstFuncionarios"	src="AdministracaoCsAstbProdutoassuntogfpermPagp.do?tela=<%= MAConstantes.TELA_IFRM_LST_GRUPOFUNCIONARIO_PAGP%>&acao=<%= Constantes.ACAO_CONSULTAR %>"	width="100%" height="175" scrolling="no" frameborder="0"	marginwidth="0" marginheight="0"></iframe>
											</td>	
										</tr>
										</table>	
									</td>
									<!-- LISTA DE GRUPO DE FUNCIONARIOS -->
									
									<!-- BOTOES DE MOVIMENTACAO -->
									<td valign="top" width="4%" height="100">
										<table cellpadding="0" cellspacing="0" width="100%" height="80%" border="0" align="center">
										<tr><td height="80"></td></tr>
			  							<tr>
			  								<td valign="top" align="center"><img src="webFiles/images/botoes/avancar_unico.gif" name="imgAvancar" width=21 height=18 class=geralCursoHand title="<bean:message key="prompt.adicionaraogrupo"/>" onclick="moveToRight();"></td>
			  							</tr>
			  				  			<tr>
			  								<td valign="top" align="center"><img src="webFiles/images/botoes/voltar_unico.gif" name="imgVoltar" width=21 height=18 class=geralCursoHand title="<bean:message key="prompt.retirardogrupo"/>" onclick="moveToLeft();"></td>
							  			</tr>
			  							</table>
									</td>
									<!-- BOTOES DE MOVIMENTACAO -->
									
									<!-- LISTA DE GRUPO FUNCIONARIOS CADASTRADO -->
									<td height="100">
										<table border="0" cellpadding="0" cellspacing="4" width="100%" height="100%">
										<tr>
											<td class="principalLabel" height="30">
												&nbsp;
											</td>	
										</tr>
										<tr>
											<td class="principalLabel" colspan="2" valign="top">
												<iframe name="lstFuncionariosGrupo" id="lstFuncionariosGrupo" src="AdministracaoCsAstbProdutoassuntogfpermPagp.do?tela=<%= MAConstantes.TELA_IFRM_LST_GRUPOFUNCIONARIO_PRODUTOASSUNTO %>&acao=<%= Constantes.ACAO_CONSULTAR %>&csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm=<bean:write name="administracaoCsAstbProdutoassuntogfpermPagpForm" property="csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"/>"	width="100%" height="175" scrolling="no" frameborder="0"	marginwidth="0" marginheight="0"></iframe>
											</td>	
										</tr>
										</table>	
									</td>
									<!-- LISTA DE GRUPO FUNCIONARIOS CADASTRADO -->
									
								</tr>
								</table>
								<!-- FINAL DA TABELA DE FORA -->
								
							</td>
							<td width="4" height="230"><img	src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>						
						</tr>
						<tr>
							<td width="100%" height="8" valign="top"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
							<td width="4" height="8" valign="top"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
        <!-- FINAL ABA DE FUNCIONARIO -->
        
      </table>

      <div id="listaFuncionarios"></div>
</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			setTimeout("desabilitaCamposGrupofuncperm();confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>'); parent.setConfirm(confirmacao)",500);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_PRODUTOASSUNTOGFPERM_INCLUSAO_CHAVE%>', parent.document.administracaoCsAstbProdutoassuntogfpermPagpForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_PRODUTOASSUNTOGFPERM_INCLUSAO_CHAVE%>')){
				desabilitaCamposGrupofuncperm();
			}else{
				try{
					document.ifrmCmbAsn1.administracaoCsAstbProdutoassuntogfpermPagpForm["csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].disabled= false;
					document.ifrmCmbAsn1.administracaoCsAstbProdutoassuntogfpermPagpForm["csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value= 0;
				}catch(e){}
			}
		
		</script>
</logic:equal>

</html>

<script>
function desabilitarCampos()
{
	document.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbLinhaLinhVo.idLinhCdLinha'].disabled= true;
	document.ifrmCmbAsn1.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].disabled= true;
	document.ifrmCmbAsn2.administracaoCsAstbProdutoassuntogfpermPagpForm['csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].disabled= true;
}

	try{document.ifrmCmbAsn1.administracaoCsAstbProdutoassuntogfpermPagpForm["csAstbProdutoassuntogfpermPagpVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].focus();}
	catch(e){}
</script>
