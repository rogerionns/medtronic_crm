<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>

<script language="JavaScript">
	function excluir(idRegra, idItemRegra, dsCampo, dsCondicao, dsValor){
	
	//	parent.document.administracaoCsCdtbItemRegraItreForm.idRegrCdRegra.value = idRegra;
		parent.document.administracaoCsCdtbItemRegraItreForm.itreNrSequencia.value = idItemRegra;
		
		parent.document.administracaoCsCdtbItemRegraItreForm.itreDsCampo.value = dsCampo;
		parent.document.administracaoCsCdtbItemRegraItreForm.itreDsCondicao.value = dsCondicao;
		parent.document.administracaoCsCdtbItemRegraItreForm.itreDsValor.value = dsValor;
		
		parent.excluir();
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsCdtbItemRegraItreForm" action="/AdministracaoCsCdtbItemRegraItre.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csCdtbItemRegraItreVector" indexId="sequencia"> 
  <tr> 
    <td width="5%"> 
	    <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18" height="18" class="principalLstParMao" title="<bean:message key="prompt.excluir"/>" onclick="excluir('', '<bean:write name="ccttrtVector" property="itreNrSequencia" />', '<bean:write name="ccttrtVector" property="itreDsCampo" />', '<bean:write name="ccttrtVector" property="itreDsCondicao" />', '<bean:write name="ccttrtVector" property="itreDsValor" /> ');"> 
	</td>
    <td class="principalLstPar" width="36%" > 
    	<bean:write name="ccttrtVector" property="itreDsCampo" />
    </td>
    <td class="principalLstPar" width="32%" > 
    	<bean:write name="ccttrtVector" property="itreDsCondicao" /> 
    </td>
    <td class="principalLstPar" align="left" width="30%" > 
        <%//Chamado: 105868 - 17/12/2015 - Carlos Nunes%>
    	<logic:equal name="ccttrtVector" property="itreDsCampo" value="CAIXAPOSTAL">
    		<plusoft:acronym name="ccttrtVector" property="capoDsDescricao" length="25" />
    	</logic:equal>
    	<logic:notEqual name="ccttrtVector" property="itreDsCampo" value="CAIXAPOSTAL">
    		<plusoft:acronym name="ccttrtVector" property="itreDsValor" length="25" />
    	</logic:notEqual>
    </td>
  </tr>
  </logic:iterate>
</table>
<script>

setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_ITEMREGRA_EXCLUSAO_CHAVE%>', editCsCdtbItemRegraItreForm.lixeira);

</script>

</html:form>
</body>
</html>

