<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title>PLUSOFT CRM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script language="JavaScript">

var idTadeCdTpabrangdetalhe = "";

function atualizaPatpDsAbrangencia(){
	for (x = 0;  x < administracaoCsCdtbParamAbrangPaabForm.elements.length;  x++) {
		Campo = administracaoCsCdtbParamAbrangPaabForm.elements[x];
		if(Campo.type == "checkbox" && Campo.checked){
			idTadeCdTpabrangdetalhe += Campo.value + ",";
		}
	}

	idTadeCdTpabrangdetalhe = idTadeCdTpabrangdetalhe.substring(0,idTadeCdTpabrangdetalhe.length-1);
	window.dialogArguments.administracaoCsCdtbParamAbrangPaabForm["csAstbParamtpabrangPatpVo.patpDsAbrangencia"].value = "";
	window.dialogArguments.administracaoCsCdtbParamAbrangPaabForm["csAstbParamtpabrangPatpVo.patpDsAbrangencia"].value = idTadeCdTpabrangdetalhe;
}

function inicio(){
	if(window.dialogArguments.administracaoCsCdtbParamAbrangPaabForm["csAstbParamtpabrangPatpVo.patpDsAbrangencia"].value != ""){
		var patpDsAbrangencia = window.dialogArguments.administracaoCsCdtbParamAbrangPaabForm["csAstbParamtpabrangPatpVo.patpDsAbrangencia"].value;
		var arrayPatpDsAbrangencia = new Array(); 
		
		if(patpDsAbrangencia.indexOf(",") != -1){
			arrayPatpDsAbrangencia = patpDsAbrangencia.split(",");
		}else{
			arrayPatpDsAbrangencia[0] = patpDsAbrangencia;
		}
		
		//fazer for com conteudo do text de baixo, selecionando os devidos checks
		for (i = 0;  i < arrayPatpDsAbrangencia.length;  i++) {
			for (x = 0;  x < administracaoCsCdtbParamAbrangPaabForm.elements.length;  x++) {
				Campo = administracaoCsCdtbParamAbrangPaabForm.elements[x];
				if(Campo.type == "checkbox"){
					if(arrayPatpDsAbrangencia[i] == Campo.value){
						Campo.checked = true;
					}
				}
			}
		}
	}
}

</script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();" onbeforeunload="atualizaPatpDsAbrangencia()">
<html:form styleId="administracaoCsCdtbParamAbrangPaabForm" action="/AdministracaoCsCdtbParamAbrangPaab.do">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr> 
		<td width="1007" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
				<tr> 
					<td class="principalPstQuadro" height="17" width="166"><%= getMessage("prompt.abrangencia", request)%></td>
					<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
					<td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
				</tr> 
			</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top" height="250"> &nbsp;
			<div id="lista" style="position:absolute; width:220px; height:245px; z-index:1; overflow:auto; visibility: visible;"> 
				<table width="96%" border="0" cellspacing="0" cellpadding="0" align="center">
				 <logic:present name="tadeVector"> 	
					<logic:iterate name="tadeVector" id="vo" indexId="index">
						<tr> 
							<td width="5%" class="principalLstPar" align="center"><input type="checkbox" id="check" value="<bean:write name="vo" property="field(tade_ds_tpabrangdetalhe)"/>" /></td>
							<td class="principalLstPar">&nbsp;<bean:write name="vo" property="field(tade_ds_tpabrangdetalhe)"/></td>
						</tr>
					</logic:iterate>
				 </logic:present>
					
				</table>
			</div>
		</td>
		<td width="4" height="134"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr> 
	<tr> 
		<td width="100%"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr> 
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right"> 
	<tr> 
		<td> 
			<img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand">
		</td>
	</tr> 
</table>

</html:form>
</body>
</html>

