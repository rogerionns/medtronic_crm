<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>

<script language="JavaScript">
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbGrupoocorrenciaGrocForm['csCdtbGrupoocorrenciaGrocVo.idGrocCdGrupoocorrencia'].value);
	}
</script>

<script language="Javascript">

nLinha = new Number(0);
estilo = new Number(0);

function incluirManif(){
		if (administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbTpmanifocorrenciaTpocVo.idTpmaCdTpmanifestacao'].value == ""){
			alert('<bean:message key="prompt.Por_favor_escolha_um_item"/>');
			administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbTpmanifocorrenciaTpocVo.idTpmaCdTpmanifestacao'].focus();
			return false;
		}
		addManif(document.administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbTpmanifocorrenciaTpocVo.idTpmaCdTpmanifestacao'][document.administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbTpmanifocorrenciaTpocVo.idTpmaCdTpmanifestacao'].selectedIndex].text, document.administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbTpmanifocorrenciaTpocVo.idTpmaCdTpmanifestacao'].value); 
		administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbTpmanifocorrenciaTpocVo.idTpmaCdTpmanifestacao'].value="";
}

function addManif(cResultado, nResultado) {
	nLinha = nLinha + 1;
	estilo++;

	objEMail = document.administracaoCsCdtbGrupoocorrenciaGrocForm.idManif;

	for (nNode=0;nNode<objEMail.length;nNode++) {
	  if (objEMail[nNode].value == nResultado) {
		  nResultado=0;
		  }
	}
	if (nResultado > 0) { 
		resultado(cResultado, nResultado, nLinha, estilo);
	}else{
		alert('<bean:message key="prompt.Selecionar_um_item_nao_repitido"/>');
	}
}

function resultado(cResultado, nResultado, nLinha, estilo){
		strTxt = "";
		strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr class='intercalaLst" + (estilo-1)%2 + "'> ";
		strTxt += "     	<td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif name=lixeiraResultado width=14 height=14 class=geralCursoHand onclick=removeManif(\"" + nLinha + "\")></td> ";
		strTxt += "       	<input type=\"hidden\" name=\"idManif\" value=\"" + nResultado + "\" > ";
		strTxt += "     	<td class=principalLstPar width=55%> " + cResultado ;
		strTxt += "     	</td> ";
		strTxt += "		</tr> ";
		strTxt += " </table> ";
		document.getElementById("lstResultado").innerHTML += strTxt;
}

function removeManif(nTblExcluir) {
	msg = '<bean:message key="prompt.Deseja_remover_esse_item"/>';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstResultado.removeChild(objIdTbl);
		estilo--;
	}
}

// ************************ ATENDIMENTO *********************************

function incluirAtend(){
		if (administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbPadraoocorrenciaPaocVo.idAtpdCdAtendpadrao'].value == ""){
			alert('<bean:message key="prompt.Por_favor_escolha_um_item"/>');
			administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbPadraoocorrenciaPaocVo.idAtpdCdAtendpadrao'].focus();
			return false;
		}
		addAtend(document.administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbPadraoocorrenciaPaocVo.idAtpdCdAtendpadrao'][document.administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbPadraoocorrenciaPaocVo.idAtpdCdAtendpadrao'].selectedIndex].text, document.administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbPadraoocorrenciaPaocVo.idAtpdCdAtendpadrao'].value); 
		administracaoCsCdtbGrupoocorrenciaGrocForm['csAstbPadraoocorrenciaPaocVo.idAtpdCdAtendpadrao'].value="";
}

function addAtend(cResultado, nResultado) {
	nLinha = nLinha + 1;
	estilo++;

	objEMail = document.administracaoCsCdtbGrupoocorrenciaGrocForm.idAtend;

	for (nNode=0;nNode<objEMail.length;nNode++) {
	  if (objEMail[nNode].value == nResultado) {
		  nResultado=0;
		  }
	}
	if (nResultado > 0) { 
		resultadoAtend(cResultado, nResultado, nLinha, estilo);
	}else{
		alert('<bean:message key="prompt.Selecionar_um_item_nao_repitido"/>');
	}
	
}

function resultadoAtend(cResultado, nResultado, nLinha, estilo){
		strTxt = "";
		strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr class='intercalaLst" + (estilo-1)%2 + "'> ";
		strTxt += "     	<td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif name=lixeiraResultadoAtend width=14 height=14 class=geralCursoHand onclick=removeAtend(\"" + nLinha + "\")></td> ";
		strTxt += "       	<input type=\"hidden\" name=\"idAtend\" value=\"" + nResultado + "\" > ";
		strTxt += "     	<td class=principalLstPar width=55%> " + cResultado ;
		strTxt += "     	</td> ";
		strTxt += "		</tr> ";
		strTxt += " </table> ";
		document.getElementById("lstAtendimento").innerHTML += strTxt;
}

function removeAtend(nTblExcluir) {
	msg = '<bean:message key="prompt.Deseja_remover_esse_item"/>';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstAtendimento.removeChild(objIdTbl);
		estilo--;
	}
}

function desabilitaCamposGrupoocorrencia(){
	administracaoCsCdtbGrupoocorrenciaGrocForm["csCdtbGrupoocorrenciaGrocVo.grocDsGrupoocorrencia"].disabled= true;	
	administracaoCsCdtbGrupoocorrenciaGrocForm.grocDhInativo.disabled= true;
	administracaoCsCdtbGrupoocorrenciaGrocForm["csCdtbGrupoocorrenciaGrocVo.grocNrQtdeocorrencia"].disabled= true;	
	administracaoCsCdtbGrupoocorrenciaGrocForm["csCdtbGrupoocorrenciaGrocVo.grocNrPeriodotempo"].disabled= true;	
	administracaoCsCdtbGrupoocorrenciaGrocForm["csCdtbGrupoocorrenciaGrocVo.grocInAbrangencia"].disabled= true;	
	administracaoCsCdtbGrupoocorrenciaGrocForm["csAstbTpmanifocorrenciaTpocVo.idTpmaCdTpmanifestacao"].disabled= true;	
	administracaoCsCdtbGrupoocorrenciaGrocForm["csAstbPadraoocorrenciaPaocVo.idAtpdCdAtendpadrao"].disabled= true;	
}

</script>
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')" style="overflow: hidden;">

<html:form styleId="administracaoCsCdtbGrupoocorrenciaGrocForm" action="/AdministracaoCsCdtbGrupoocorrenciaGroc.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="dataInativo"/>		

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="csCdtbGrupoocorrenciaGrocVo.idGrocCdGrupoocorrencia" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="csCdtbGrupoocorrenciaGrocVo.grocDsGrupoocorrencia" styleClass="text" maxlength="60" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        
        
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.qtdeOcorrencia"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="csCdtbGrupoocorrenciaGrocVo.grocNrQtdeocorrencia" maxlength="5" styleClass="text" onkeypress="validaCampoNumericoInteiro(this)"/>
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.periodoTempo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="csCdtbGrupoocorrenciaGrocVo.grocNrPeriodotempo" maxlength="5" styleClass="text" onkeypress="validaCampoNumericoInteiro(this)"/>
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr>
          <td width="23%">&nbsp;</td>
          <td colspan="3" class="principalLabel">
          	<html:checkbox value="true" property="csCdtbGrupoocorrenciaGrocVo.grocInAbrangencia"/> <bean:message key="prompt.abrangencia"/>
          </td>
          <td width="12%">&nbsp;</td>
        </tr>
        
	   <!-- Tipo de Manifesta��o -->
		<tr> 
		  <td width="23%" class="principalLabel"> 
			<div align="right"><bean:message key="prompt.tipoManifestacao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
		  </td>
		  <td colspan="3">
		  	<html:select property="csAstbTpmanifocorrenciaTpocVo.idTpmaCdTpmanifestacao" styleClass="principalObjForm" > 
				<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
				<html:options collection="csCdtbTpManifestacaoTpmaVector" property="idTpmaCdTpManifestacao" labelProperty="tpmaDsTpManifestacao"/> 
			</html:select>                              
		  </td>
		  
         <td width="20">
				<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="incluirManif();" >
		  </td>
		</tr>
		<!-- Final Tipo de Manifesta��o -->

		<tr>
			<td width="10%">&nbsp;</td>
			<td colspan="3" class="principalLstCab" width="13%">&nbsp;&nbsp;<bean:message key="prompt.descricao"/></td>
		</tr>

		<tr>
			<td width="10%">&nbsp;</td>
            <td colspan="3" height="45" valign="top"> 
              <div id="lstResultado" style="position:absolute; width:99%; height:45px; z-index:4; visibility: visible; overflow: auto"> 
              	<input type="hidden" name="idManif">
              		
				<!--Inicio Lista ltado -->
				 <logic:present name="listResultadoVector">
				  <logic:iterate id="cnrrVector" name="listResultadoVector">
						<script language="JavaScript">
						  addManif('<bean:write name="cnrrVector" property="tpmaDsTpManifestacao" />',
								  '<bean:write name="cnrrVector" property="idTpmaCdTpManifestacao" />');
						</script>
				  </logic:iterate>
				</logic:present>
			  </div>
            </td>
		</tr>
		
        <tr> 
          <td width="13%">&nbsp;</td>
        </tr>
        
	   <!-- Atendimento PADR�O -->
		<tr> 
		  <td width="23%" class="principalLabel"> 
			<div align="right"><bean:message key="prompt.atendimentoPadrao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
		  </td>
		  <td colspan="3">
		  	<html:select property="csAstbPadraoocorrenciaPaocVo.idAtpdCdAtendpadrao" styleClass="principalObjForm" > 
				<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
				<html:options collection="csCdtbAtendpadraoAtpaVector" property="idAtpdCdAtendpadrao" labelProperty="atpdDsAtendpadrao"/> 
			</html:select>
		  </td>
		  
         <td width="20">
				<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="incluirAtend();" >
		  </td>
		</tr>
		<!-- Atendimento PADR�O -->
        
		<tr>
			<td width="10%">&nbsp;</td>
			<td colspan="3" class="principalLstCab" width="13%">&nbsp;&nbsp;<bean:message key="prompt.descricao"/></td>
		</tr>

		<tr>
			<td width="10%">&nbsp;</td>
            <td colspan="3" height="45" valign="top"> 
              <div id="lstAtendimento" style="position:absolute; width:99%; height:45px; z-index:4; visibility: visible; overflow: auto"> 
              	<input type="hidden" name="idAtend">
              		
				<!--Inicio Lista Atend -->
				 <logic:present name="listAtendVector">
				  <logic:iterate id="atpaVector" name="listAtendVector">
						<script language="JavaScript">
						  addAtend('<bean:write name="atpaVector" property="atpdDsAtendpadrao" />',
								  '<bean:write name="atpaVector" property="idAtpdCdAtendpadrao" />');
						</script>
				  </logic:iterate>
				</logic:present>
			  </div>
            </td>
		</tr>

        <tr> 
          <td width="13%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="100%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="100%"> 
                  <html:checkbox value="true" property="grocDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			administracaoCsCdtbGrupoocorrenciaGrocForm["csCdtbGrupoocorrenciaGrocVo.grocDsGrupoocorrencia"].disabled= true;	
			administracaoCsCdtbGrupoocorrenciaGrocForm.grocDhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_GRUPOOCORRENCIA_INCLUSAO_CHAVE%>', parent.administracaoCsCdtbGrupoocorrenciaGrocForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_GRUPOOCORRENCIA_INCLUSAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_GRUPOOCORRENCIA_EXCLUSAO_CHAVE%>', administracaoCsCdtbGrupoocorrenciaGrocForm.lixeiraResultado);
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_GRUPOOCORRENCIA_EXCLUSAO_CHAVE%>', administracaoCsCdtbGrupoocorrenciaGrocForm.lixeiraResultadoAtend);
				desabilitaCamposGrupoocorrencia();	
			}else{
				administracaoCsCdtbGrupoocorrenciaGrocForm["csCdtbGrupoocorrenciaGrocVo.idGrocCdGrupoocorrencia"].disabled= false;
				administracaoCsCdtbGrupoocorrenciaGrocForm["csCdtbGrupoocorrenciaGrocVo.idGrocCdGrupoocorrencia"].value= '';
				administracaoCsCdtbGrupoocorrenciaGrocForm["csCdtbGrupoocorrenciaGrocVo.idGrocCdGrupoocorrencia"].disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_GRUPOOCORRENCIA_EXCLUSAO_CHAVE%>', administracaoCsCdtbGrupoocorrenciaGrocForm.lixeiraResultado);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_GRUPOOCORRENCIA_EXCLUSAO_CHAVE%>', administracaoCsCdtbGrupoocorrenciaGrocForm.lixeiraResultadoAtend);
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_GRUPOOCORRENCIA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_GRUPOOCORRENCIA_ALTERACAO_CHAVE%>', parent.administracaoCsCdtbGrupoocorrenciaGrocForm.imgGravar);	
				desabilitaCamposGrupoocorrencia();				
			}
		</script>
</logic:equal>

</html>

<script>
	try{administracaoCsCdtbGrupoocorrenciaGrocForm["csCdtbGrupoocorrenciaGrocVo.grocDsGrupoocorrencia"].focus();}
	catch(e){}
</script>