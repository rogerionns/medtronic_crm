<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Grupo Ocorr&ecirc;ncia</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
this.name = "noticia";
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function validarPermissao(){

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
	try{
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_ABRANGENCIA_EXCLUSAO_CHAVE%>', ifrmLstParamtp.administracaoCsCdtbParamAbrangPaabForm.lixeira);	
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_ABRANGENCIA_ALTERACAO_CHAVE%>')){
			administracaoCsCdtbParamAbrangPaabForm["csAstbParamtpabrangPatpVo.patpDsAbrangencia"].disabled=true;
			administracaoCsCdtbParamAbrangPaabForm["csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrQtdeocorrencia"].disabled=true;
			administracaoCsCdtbParamAbrangPaabForm["csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrPeriodotempo"].disabled=true;
			administracaoCsCdtbParamAbrangPaabForm["chkNovoSubGupo"].disabled=true;
			administracaoCsCdtbParamAbrangPaabForm["csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia"].disabled=true;
			administracaoCsCdtbParamAbrangPaabForm["csAstbParamtpabrangPatpVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia"].disabled=true;
		}
	}
	catch(e){
		setTimeout("validarPermissao();",100);
	}
</logic:equal>

<logic:notEqual name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
	administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrQtdeocorrencia'].value = '';
		administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrPeriodotempo'].value = '';
</logic:notEqual>
}


function trataCheckControleEdicao(){
	if (administracaoCsCdtbParamAbrangPaabForm.chkNovoSubGupo.checked==true){
		administracaoCsCdtbParamAbrangPaabForm.idGrocCdGrupoocorrenciaFixo.value = "";
		administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idPaabCdSequencial'].value = "";
		administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrQtdeocorrencia'].value = "0";
		administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrPeriodotempo'].value = "0";
		
		administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia'].disabled = false;
		
		if (ifrmLstParamtp.administracaoCsCdtbParamAbrangPaabForm.optControleEdicao.length == undefined){
			ifrmLstParamtp.administracaoCsCdtbParamAbrangPaabForm.optControleEdicao.checked = false;
		}else{
			for(i=0;i<ifrmLstParamtp.administracaoCsCdtbParamAbrangPaabForm.optControleEdicao.length;i++){
				ifrmLstParamtp.administracaoCsCdtbParamAbrangPaabForm.optControleEdicao[i].checked = false;
			}
		}
	}else{
		administracaoCsCdtbParamAbrangPaabForm.chkNovoSubGupo.checked=true;
	}
		
}

function trataOptControleEdicao(idGrocCdGrupoocorrencia,idPaabCdSequencial,paabNrQtdeocorrencia,paabNrPeriodotempo){

	administracaoCsCdtbParamAbrangPaabForm.idGrocCdGrupoocorrenciaFixo.value = idGrocCdGrupoocorrencia;
	administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia'].value = idGrocCdGrupoocorrencia;
	
	administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idPaabCdSequencial'].value = idPaabCdSequencial;
	administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrQtdeocorrencia'].value = paabNrQtdeocorrencia;
	administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrPeriodotempo'].value = paabNrPeriodotempo;
	
	administracaoCsCdtbParamAbrangPaabForm.chkNovoSubGupo.checked = false;
	administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia'].disabled = true;
}

function carregaConsulta(){

	ifrmLstParamtp.location="AdministracaoCsCdtbParamAbrangPaab.do?tela=ifrmLstCsCdtbParamAbrangPaab&acao=<%=Constantes.ACAO_VISUALIZAR%>&csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia=" + document.getElementById("csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia").value;

}

//Chamado 71936 - Vinicius - Inclus�o do popUp para selecionar as abrangencias (DDD)
function abrirAbrangencias(tipo){
	var url = "AdministracaoCsCdtbParamAbrangPaab.do?tela=<%=MAConstantes.TELA_POPUP_CS_CDTB_PARAMABRANG_PAAB%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csAstbParamtpabrangPatpVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia=<%=MAConstantes.CAMPO_DDD%>";
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:250px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px');
}

//Chamado 71936 - Vinicius - Inclus�o do popUp para selecionar as abrangencias (DDD)
function habilitaBotao(obj){
	if(obj.value == <%=MAConstantes.CAMPO_DDD%>){
		administracaoCsCdtbParamAbrangPaabForm.imgSelectAbrangencia.className = "geralCursoHand";
		administracaoCsCdtbParamAbrangPaabForm.imgSelectAbrangencia.onclick = function(){abrirAbrangencias('<%=MAConstantes.CAMPO_DDD%>');}
	}else{
		administracaoCsCdtbParamAbrangPaabForm.imgSelectAbrangencia.className = "geralImgDisable";
		administracaoCsCdtbParamAbrangPaabForm.imgSelectAbrangencia.onclick = function(){}
	}
}

function inicio(){
	setaAssociacaoMultiEmpresa();
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();validarPermissao();">
<html:form styleId="administracaoCsCdtbParamAbrangPaabForm" action="/AdministracaoCsCdtbParamAbrangPaab.do">

	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idPaabCdSequencial" />
	<html:hidden property="csAstbParamtpabrangPatpVo.idPatpCdSequencial" />
	
	<html:hidden property="idGrocCdGrupoocorrenciaFixo"/>
	<html:hidden property="idTpabCdTipoabrangenciaFixo"/>

      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
		
          <td height="210" valign="top" align="center"> 
            <table width="98%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="20%" align="right"><bean:message key="prompt.grupoOcorrencia"/> 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabel" width="80%"> 
				<html:select property="csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia" onclick="carregaConsulta();" styleClass="principalObjForm" >
					<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
					<html:options collection="grocVector" property="idGrocCdGrupoocorrencia" labelProperty="grocDsGrupoocorrencia"/>
				</html:select>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="2" align="right">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="22%" class="principalLabel"><bean:message key="prompt.tipoAbrangencia"/></td>
                <td width="20%" class="principalLabel"><bean:message key="prompt.abrangencia"/></td>
                <td width="5%" class="principalLabel">&nbsp;</td>
                <td width="17%" class="principalLabel"><bean:message key="prompt.quantidade"/></td>
                <td width="15%" class="principalLabel"><bean:message key="prompt.periodo"/></td>
                <td width="16%" class="principalLabel">&nbsp;</td>
                <td width="5%" class="principalLabel">&nbsp;</td>
              </tr>
              <tr> 
                <td width="22%" class="principalLabel"> 
                  
				<html:select property="csAstbParamtpabrangPatpVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia" styleClass="principalObjForm" onchange="habilitaBotao(this)">
					<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
					<html:options collection="listResultadoVector" property="idTpabCdTipoabrangencia" labelProperty="tpabDsTipoabrangencia"/>
				</html:select>
                  
                  
                </td>
                <td width="20%" class="principalLabel"> 
                  <html:text property="csAstbParamtpabrangPatpVo.patpDsAbrangencia" styleClass="principalObjForm" maxlength="1000" />
                </td>
                
                 <!-- Chamado 71936 - Vinicius - Inclus�o do popUp para selecionar as abrangencias (DDD) -->
                <td width="5%" class="principalLabel">
                	<img src="/plusoft-resources/images/botoes/blist.gif" border="1" id="imgSelectAbrangencia" class="geralImgDisable" title="<bean:message key='prompt.selecioneAbrangecia'/>">
                </td>
                
                <td width="17%" class="principalLabel"> 
                  <html:text property="csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrQtdeocorrencia" maxlength="5" onkeypress="isDigito(this)" styleClass="principalObjForm"/>
                </td>
                <td width="15%" class="principalLabel"> 
                  <html:text property="csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrPeriodotempo" maxlength="5" onkeypress="isDigito(this)" styleClass="principalObjForm"/>
                </td>
                <td width="16%" class="principalLabel"> 
                  <input type="checkbox" name="chkNovoSubGupo" checked onclick="trataCheckControleEdicao()">
                  <bean:message key="prompt.novoSubGrupo"/></td>
                <td width="5%" class="principalLabel"><!--img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand"--></td>
              </tr>
              <tr>
                <td width="22%" class="principalLabel">&nbsp;</td>
                <td width="20%" class="principalLabel">&nbsp;</td>
                <td width="5%" class="principalLabel">&nbsp;</td>
                <td width="17%" class="principalLabel">&nbsp;</td>
                <td width="15%" class="principalLabel">&nbsp;</td>
                <td width="16%" class="principalLabel">&nbsp;</td>
                <td width="5%" class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="260" valign="top">
                	<iframe id=ifrmLstParamtp name="ifrmLstParamtp" src="AdministracaoCsCdtbParamAbrangPaab.do?tela=ifrmLstCsCdtbParamAbrangPaab&acao=<%=Constantes.ACAO_VISUALIZAR%>&csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia=<bean:write name='administracaoCsCdtbParamAbrangPaabForm' property='csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia'/>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      
     <logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_OCORRENCIAMASSIVA_ABRANGENCIA_INCLUSAO_CHAVE%>', administracaoCsCdtbParamAbrangPaabForm.imgGravar, "<bean:message key='prompt.gravar'/>");
		</script>
	</logic:equal>
     
</html:form>      
</body>
</html>

<script>
	try{administracaoCsCdtbParamAbrangPaabForm["csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia"].focus();}
	catch(e){}
</script>