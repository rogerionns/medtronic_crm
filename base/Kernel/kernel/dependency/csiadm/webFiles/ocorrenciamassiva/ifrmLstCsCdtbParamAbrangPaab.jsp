<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
this.name = "noticia";
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function submeteExcluir(idGrocCdGrupoocorrencia,idPaabCdSequencial,idTpabCdTipoabrangencia,idPatpCdSequencial){
	if(parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.patpDsAbrangencia'].value==""){
		if (confirm("<bean:message key='prompt.DesejaExcluirEsteRegistro' />")==true){
			administracaoCsCdtbParamAbrangPaabForm.tela.value = '<%= MAConstantes.TELA_LST_CS_CDTB_PARAMABRANG_PAAB%>';
			administracaoCsCdtbParamAbrangPaabForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
			
			administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia'].value = idGrocCdGrupoocorrencia;
			administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idPaabCdSequencial'].value = idPaabCdSequencial;
			administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia'].value = idTpabCdTipoabrangencia;
			administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.idPatpCdSequencial'].value = idPatpCdSequencial;
			
			administracaoCsCdtbParamAbrangPaabForm.submit();
		}
	}else{
		alert("<bean:message key='prompt.favorGravarCancelarAlteracaoAntesContinuarExclusao'/>");
	}
}


function editar(idGrocCdGrupoocorrencia,idPaabCdSequencial,idPatpCdSequencial,idTpabCdTipoabrangencia,patpDsAbrangencia,paabNrQtdeocorrencia,paabNrPeriodotempo){
	
	parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia'].disabled = true;
	parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia'].disabled = true;

	parent.administracaoCsCdtbParamAbrangPaabForm.chkNovoSubGupo.checked = false;
	parent.administracaoCsCdtbParamAbrangPaabForm.chkNovoSubGupo.disabled = true;
	
	if (administracaoCsCdtbParamAbrangPaabForm.optControleEdicao.length == undefined){
		administracaoCsCdtbParamAbrangPaabForm.optControleEdicao.checked = false;
		administracaoCsCdtbParamAbrangPaabForm.optControleEdicao.disabled = true;
	}else{
		for (i=0;i<administracaoCsCdtbParamAbrangPaabForm.optControleEdicao.length;i++){
			administracaoCsCdtbParamAbrangPaabForm.optControleEdicao[i].checked = false;
			administracaoCsCdtbParamAbrangPaabForm.optControleEdicao[i].disabled = true;
		}
	}
	
	
	parent.administracaoCsCdtbParamAbrangPaabForm.idGrocCdGrupoocorrenciaFixo.value = idGrocCdGrupoocorrencia;
	parent.administracaoCsCdtbParamAbrangPaabForm.idTpabCdTipoabrangenciaFixo.value = idTpabCdTipoabrangencia;

	parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia'].value = idGrocCdGrupoocorrencia;
	parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idPaabCdSequencial'].value = idPaabCdSequencial;
	parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.idPatpCdSequencial'].value = idPatpCdSequencial;
	parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia'].value = idTpabCdTipoabrangencia;
	parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.patpDsAbrangencia'].value = patpDsAbrangencia;
	parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrQtdeocorrencia'].value = paabNrQtdeocorrencia;
	parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.paabNrPeriodotempo'].value = paabNrPeriodotempo;
	parent.administracaoCsCdtbParamAbrangPaabForm.acao.value='<%=Constantes.ACAO_EDITAR %>';
	
	//Chamado 71936 - Vinicius Habilita o bot�o para edi��o
	parent.habilitaBotao(parent.administracaoCsCdtbParamAbrangPaabForm['csAstbParamtpabrangPatpVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia']);
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form styleId="administracaoCsCdtbParamAbrangPaabForm"	action="/AdministracaoCsCdtbParamAbrangPaab.do">

	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<!-- chave para exclus�o-->
	<html:hidden property="csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idGrocCdGrupoocorrencia" />
	<html:hidden property="csAstbParamtpabrangPatpVo.csCdtbParamabrangPaabVo.idPaabCdSequencial" />
	<html:hidden property="csAstbParamtpabrangPatpVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia"/>
	<html:hidden property="csAstbParamtpabrangPatpVo.idPatpCdSequencial" />


<table width="100%" border="0" cellspacing="0" cellpadding="0" bordercolor="#CCCCCC" class="principalBordaQuadro">
  <tr> 
    <td width="5%" align="center" class="principalLstCab">Per&iacute;odo</td>
    <td width="5%" align="center" class="principalLstCab">Qtd.</td>
    <td width="5%" align="center" class="principalLstCab">&nbsp; </td>
    <td width="2%" align="center" class="principalLstCab">&nbsp;</td>
    <td width="18%" class="principalLstCab">Tipo Abrang&ecirc;ncia</td>
    <td width="65%" class="principalLstCab">Abrang&ecirc;ncia</td>
  </tr>
</table>  

<table width="100%" border="0" cellspacing="0" cellpadding="0" bordercolor="#CCCCCC" class="principalBordaQuadro">
	<logic:present name="csCdtbParamabrangPaabVector">
		<logic:iterate id="ccttrtVector" name="csCdtbParamabrangPaabVector" >  	
		  <tr> 
		    <td rowspan='<bean:write name="ccttrtVector" property="paabQtdAssociacao"/>' width="5%" align="center" class="principalBordaQuadro"><span class="principalLabel">&nbsp;<bean:write name="ccttrtVector" property="paabNrPeriodotempo"/></span></td>
		    <td rowspan='<bean:write name="ccttrtVector" property="paabQtdAssociacao"/>' width="5%" align="center" class="principalBordaQuadro"><span class="principalLabel">&nbsp;<bean:write name="ccttrtVector" property="paabNrQtdeocorrencia"/></span></td>
		    <td rowspan='<bean:write name="ccttrtVector" property="paabQtdAssociacao"/>' width="5%" align="center" class="principalBordaQuadro"> 
		      <input type="radio" name="optControleEdicao" value="" onclick="parent.trataOptControleEdicao('<bean:write name="ccttrtVector" property="idGrocCdGrupoocorrencia"/>','<bean:write name="ccttrtVector" property="idPaabCdSequencial"/>','<bean:write name="ccttrtVector" property="paabNrQtdeocorrencia"/>','<bean:write name="ccttrtVector" property="paabNrPeriodotempo"/>')">
		    </td>

				  <logic:iterate id="patpVector" name="ccttrtVector" property="csAstbParamtpabrangPatpVector">  
					<td width="2%" align="center" class="principalBordaQuadro"><img src="webFiles/images/botoes/lixeira.gif" name="lixeira" title="<bean:message key="prompt.excluir" />" onclick="submeteExcluir('<bean:write name="ccttrtVector" property="idGrocCdGrupoocorrencia"/>','<bean:write name="ccttrtVector" property="idPaabCdSequencial"/>','<bean:write name="patpVector" property="csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia"/>','<bean:write name="patpVector" property="idPatpCdSequencial"/>')" width="14" height="14" class="geralCursoHand"></td>
					<td width="18%" class="principalBordaQuadro"><span class="principalLabel"><span class="geralCursoHand" onclick="editar('<bean:write name="ccttrtVector" property="idGrocCdGrupoocorrencia"/>','<bean:write name="ccttrtVector" property="idPaabCdSequencial"/>','<bean:write name="patpVector" property="idPatpCdSequencial"/>','<bean:write name="patpVector" property="csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia"/>','<bean:write name="patpVector" property="patpDsAbrangencia"/>','<bean:write name="ccttrtVector" property="paabNrQtdeocorrencia"/>','<bean:write name="ccttrtVector" property="paabNrPeriodotempo"/>')">&nbsp;<bean:write name="patpVector" property="csDmtbTipoabrangenciaTpabVo.tpabDsTipoabrangencia"/></span></span></td>
					<td width="65%" class="principalBordaQuadro"><span class="principalLabel"><span class="geralCursoHand" onclick="editar('<bean:write name="ccttrtVector" property="idGrocCdGrupoocorrencia"/>','<bean:write name="ccttrtVector" property="idPaabCdSequencial"/>','<bean:write name="patpVector" property="idPatpCdSequencial"/>','<bean:write name="patpVector" property="csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia"/>','<bean:write name="patpVector" property="patpDsAbrangencia"/>','<bean:write name="ccttrtVector" property="paabNrQtdeocorrencia"/>','<bean:write name="ccttrtVector" property="paabNrPeriodotempo"/>')">&nbsp;<script>acronym('<bean:write name="patpVector" property="patpDsAbrangencia"/>', 75);</script></span></span></td>
				  </tr>
				  </logic:iterate>
		 
		 </logic:iterate>
	 </logic:present>
</table>
</html:form>
</body>
</html>