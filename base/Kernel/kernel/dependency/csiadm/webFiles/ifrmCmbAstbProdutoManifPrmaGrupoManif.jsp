<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
	function atualizaTipoManif() {
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
		document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_TIPOMANIF %>';
		document.administracaoCsAstbProdutoManifPrmaForm.target = parent.document.getElementById('ifrmCmbLinhaTipoManif').name;
		document.administracaoCsAstbProdutoManifPrmaForm.submit();
	}
	
	function iniciaTela(){
		if(document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.length == 2){
			document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.selectedIndex = 1;
		}
		atualizaTipoManif();
		parent.validaChkCombo();
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); iniciaTela();">

	<html:form action="/AdministracaoCsAstbProdutoManifPrma.do" styleId="administracaoCsAstbProdutoManifPrmaForm">
		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<html:hidden property="idMatpCdManifTipo" />
		<html:hidden property="idTpmaCdTpManifestacao" />
		<html:hidden property="idAsn1CdAssuntoNivel1" />
		<html:hidden property="idAsn2CdAssuntoNivel2" />
		<html:hidden property="idSugrCdSupergrupo" />
		
		<html:select property="idGrmaCdGrupoManifestacao" styleClass="principalObjForm" onchange="atualizaTipoManif()">
			<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
			<html:options collection="combo" property="idGrmaCdGrupoManifestacao" labelProperty="grmaDsGrupoManifestacao"/>
		</html:select>
	</html:form>
</body>

	<script>
		//Action Center: 15838 - 21/08/2012 - Carlos Nunes
		var contErro = new Number(0);
		
		function verificaFlagProduto()
		{
			try
			{
				if (parent.getFlagProduto() == 1)
				{
					parent.ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.disabled = true;
				}
			}
			catch(e)
			{
				if(contErro <= 10)
				{
					contErro++;
					setTimeout('verificaFlagProduto();', 200);
				}
			}
		}
		
		verificaFlagProduto();
	</script>
</html>