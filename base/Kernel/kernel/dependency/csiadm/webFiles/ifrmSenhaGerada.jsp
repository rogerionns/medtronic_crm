<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
	String senha = new String("");
	if (request.getAttribute("senha") != null) {
		senha = request.getAttribute("senha").toString();
	}
%>

<script>
	function inicio(){
		var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
		wi.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.value="<%=senha%>";
	}
</script>

<html>
<head>
<title>..: <bean:message key="prompt.geracaoSenha"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio()">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.geracaoSenha"/></td>
            <td class="principalQuadroPstVazia" >&nbsp; </td>
            <!-- Chamado item 58 - 26/11/2014 - Leonardo Marquini Facchini-->
            <td width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="100%"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="100%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="100%" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="40%" class="principalLabel">&nbsp;</td>
                        <td width="1%" class="principalLabel">&nbsp;</td>
                        <td width="59%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
	                    <td width="40%" class="principalLabel" align="right"><bean:message key="prompt.senha"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
                        <td width="1%">&nbsp;</td>
                        <td width="59%" class="principalLabelValorFixo">
                        	<input type="text" class="principalLabelValorFixo" value="<%=senha%>" readOnly />
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <!-- Chamado item 58 - 26/11/2014 - Leonardo Marquini Facchini-->
      <td width="4" height="100%" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.sair'/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</body>
</html>