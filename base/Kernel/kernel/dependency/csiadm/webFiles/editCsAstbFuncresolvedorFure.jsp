<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
	<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
	<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
	<script language="JavaScript">
		function MontaLista(){
			lstArqCarga.location = "AdministracaoCsAstbFuncresolvedorFure.do?tela=administracaoLstCsAstbFuncresolvedorFure&acao=<%=Constantes.ACAO_VISUALIZAR%>&csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario=" + document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value;
		}
		
		function desabilitaCamposProcinstrucao(){
			document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].disabled= true;
			document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].disabled= true;
		}
		
		function verificaCampo(){
			if(document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.fureInResolver'].checked){
				document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.fureInVer'].checked = true;
			}else{
				document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.fureInVer'].checked = false;	
			}
		}
	</script>

	<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista();">
		<html:form styleId="administracaoCsAstbFuncresolvedorFureForm" action="/AdministracaoCsAstbFuncresolvedorFure.do">
			<html:hidden property="modo" />
			<html:hidden property="acao" />
			<html:hidden property="tela" />
			<html:hidden property="topicoId" />
			<html:hidden property="csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdSuperior" />

			<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
					<td width="17%" align="right" class="principalLabel">
						<bean:message key="prompt.areaResolvedora"/>
						<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
					<td colspan="2">
						<html:select property="csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario" styleClass="principalObjForm" onchange="MontaLista()">
							<html:option value=""> 
								<bean:message key="prompt.selecione_uma_opcao"/> 
							</html:option> 
							<html:options collection="csCdtbFuncionarioFuncAreaVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>
						</html:select>
					</td>
					<td width="27%">&nbsp;</td>
				</tr>
				<tr> 
					<td width="17%" align="right" class="principalLabel">
						<bean:message key="prompt.funcionarioResolvedor"/> 
						<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
					<td colspan="2">
						<html:select property="csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario" styleClass="principalObjForm">
							<html:option value=""> 
								<bean:message key="prompt.selecione_uma_opcao"/> 
							</html:option> 
							<html:options collection="csCdtbFuncionarioFuncResolvedorVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>
						</html:select>
					</td>
					<td width="27%">&nbsp;</td>
				</tr>
				<tr> 
					<td width="17%">&nbsp;</td>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr> 
					<td width="13%">&nbsp;</td>
					<td width="10%">&nbsp;</td>
					<td class="principalLabel" width="28%"></td>
					<td width="31%" class="principalLabel">
						<bean:message key="prompt.funcionarioVer"/>
						<html:checkbox value="true" property="csAstbFuncresolvedorFureVo.fureInVer"/>
						<bean:message key="prompt.funcionarioResolver"/>
						<html:checkbox value="true" property="csAstbFuncresolvedorFureVo.fureInResolver" onclick="verificaCampo()" />
					</td>
				</tr>
				<tr> 
					<td colspan="4" height="220"> 
						<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr> 
								<td width="6%" class="principalLstCab" height="1">&nbsp;</td>
								<td class="principalLstCab" width="36%"> 
									<bean:message key="prompt.funcionarioResolvedor"/>
								</td>
								<td class="principalLstCab" width="33%"> 
									<bean:message key="prompt.funcionarioVer"/>
								</td>
								<td class="principalLstCab" width="11%"> 
									<bean:message key="prompt.funcionarioResolver"/>
								</td>
								<td class="principalLstCab" width="14%">&nbsp;</td>
							</tr>
							<tr valign="top"> 
								<td id="tdteste" colspan="5" height="180">
									<iframe name="lstArqCarga" id="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</html:form>
	</body>
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script language="JavaScript">
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
			document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].disabled= false;
			document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].disabled= false;
			parent.setConfirm(confirmacao);
		</script>
	</logic:equal>
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script language="JavaScript">
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIORESOLVEDOR_INCLUSAO_CHAVE%>', parent.document.administracaoCsAstbFuncresolvedorFureForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIORESOLVEDOR_INCLUSAO_CHAVE%>')){
				desabilitaCamposProcinstrucao();
			}else{
				document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].disabled= false;
				document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].disabled= false;
			}
		</script>
	</logic:equal>
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script language="JavaScript">
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIORESOLVEDOR_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIORESOLVEDOR_ALTERACAO_CHAVE%>', parent.document.administracaoCsAstbFuncresolvedorFureForm.imgGravar);	
				desabilitaCamposProcinstrucao();
			}
		</script>
	</logic:equal>
	<script language="JavaScript">
		try{document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].focus();}
		catch(e){}
	</script>	
</html>
