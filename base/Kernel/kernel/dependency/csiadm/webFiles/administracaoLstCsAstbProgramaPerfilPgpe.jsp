<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsAstbProgramaPerfilPgpeForm" action="/AdministracaoCsAstbProgramaPerfilPgpe.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <logic:iterate id="capppVector" name="csAstbProgramaPerfilPgpeVector"> 
  <tr> 
    <td class="principalLstParMao" width="2%"> 
	    <img src="webFiles/images/botoes/lixeira18x18.gif" width="18" height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.parent.submeteExcluir('<bean:write name="capppVector" property="csCdtbTpProgramaTppgVo.idTppgCdTipoPrograma" />','<bean:write name="capppVector" property="csCdtbPerfilPerfVo.idPerfCdPerfil" />')"> 
	</td>
    <td class="principalLstPar" width="49%" > 
    	<bean:write name="capppVector" property="csCdtbTpProgramaTppgVo.tppgDsTipoPrograma" /> 
    </td>
    <td class="principalLstPar" width="49%" > 
    	<bean:write name="capppVector" property="csCdtbPerfilPerfVo.perfDsPerfil" /> 
    </td>
  </tr>
  </logic:iterate>
</table>
</html:form>
</body>
</html>