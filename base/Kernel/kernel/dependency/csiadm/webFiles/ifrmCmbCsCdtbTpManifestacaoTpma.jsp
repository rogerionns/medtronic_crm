<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/AdministracaoCsAstbComposicaoComp.do" styleId="administracaoCsAstbComposicaoCompForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
  <html:select property="idTpmaCdTpManifestacao" styleClass="principalObjForm">
    <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
    <html:options collection="csCdtbTpManifestacaoTpmaVector" property="idTpmaCdTpManifestacao" labelProperty="tpmaDsTpManifestacao" />
  </html:select>
</html:form>
</body>
</html>