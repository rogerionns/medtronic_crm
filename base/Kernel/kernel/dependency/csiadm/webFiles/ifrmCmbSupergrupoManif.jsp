<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>
function atualizaGrupo() {
	document.administracaoCsCdtbTpManifestacaoTpmaForm.target = parent.cmbGrupoManifestacao.name;
	document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
	document.administracaoCsCdtbTpManifestacaoTpmaForm.submit();
}

function inicio(){
	if(administracaoCsCdtbTpManifestacaoTpmaForm.idSugrCdSupergrupo.length == 2)
		administracaoCsCdtbTpManifestacaoTpmaForm.idSugrCdSupergrupo.selectedIndex = 1;
		
	atualizaGrupo();
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form action="/AdministracaoCsCdtbTpManifestacaoTpma.do" styleId="administracaoCsCdtbTpManifestacaoTpmaForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="idMatpCdManifTipo" />
	<html:hidden property="idGrmaCdGrupoManifestacao" />
		
	<html:select property="idSugrCdSupergrupo" styleClass="principalObjForm" onchange="atualizaGrupo();">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		<html:options collection="combo" property="idSugrCdSupergrupo" labelProperty="sugrDsSupergrupo"/>
	</html:select>
</html:form>
</body>
</html>
