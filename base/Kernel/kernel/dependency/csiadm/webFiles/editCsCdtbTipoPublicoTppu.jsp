<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function VerificaCampos(){
		if (document.administracaoCsCdtbTipoPublicoTppuForm.tppuCdNaoIdentificado.value == "0" ){
			document.administracaoCsCdtbTipoPublicoTppuForm.tppuCdNaoIdentificado.value = "" ;
		}
	}
	
	function desabilitaCamposTipoPublico(){
		document.administracaoCsCdtbTipoPublicoTppuForm.tppuDsTipoPublico.disabled= true;	
		document.administracaoCsCdtbTipoPublicoTppuForm.tppuDhInativo.disabled= true;
		document.administracaoCsCdtbTipoPublicoTppuForm.tppuCdNaoIdentificado.disabled= true;
	}
	
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbTipoPublicoTppuForm.idTppuCdTipoPublico.value);
	}
	
</script>
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>');VerificaCampos()">

<html:form styleId="administracaoCsCdtbTipoPublicoTppuForm" action="/AdministracaoCsCdtbTipoPublicoTppu.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<input type="hidden" name="limparSessao" value="false"></input>
	
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="9%"> <html:text property="idTppuCdTipoPublico" styleClass="text" disabled="true" /> 
    </td>
    <td width="38%">&nbsp;</td>
    <td width="29%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="tppuDsTipoPublico" styleClass="text" maxlength="60" style="width: 319px;" /> 
    </td>
    <td width="29%">&nbsp;</td>
  </tr>
  <tr>
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.idPessoaNaoIdentificada"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="tppuCdNaoIdentificado" styleClass="text" maxlength="10" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/> </td>
    <td width="29%">&nbsp;</td>
  </tr>
  <tr>
    <td width="24%" align="right" class="principalLabel"></td>
    <td colspan="2" class="principalLabel"> <bean:message key="prompt.pessoa"/><html:checkbox property="tppuInPessoa" /> <bean:message key="prompt.contato"/><html:checkbox property="tppuInContato" /></td>
    <td width="29%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
    <td class="principalLabel" width="38%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="83%"> <html:checkbox value="true" property="tppuDhInativo"/>
            <!-- @@ -->
          </td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/>
            <!-- ## -->
          </td>
        </tr>
      </table>
    </td>
    <td width="29%">&nbsp;</td>
  </tr>
</table>

<div id="divEmpresa" name="divEmpresa" style="visibility:hidden">
	
</div>


</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposTipoPublico();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbTipoPublicoTppuForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICO_INCLUSAO_CHAVE%>')){
				desabilitaCamposTipoPublico();
			}else{
				document.administracaoCsCdtbTipoPublicoTppuForm.idTppuCdTipoPublico.disabled= false;
				document.administracaoCsCdtbTipoPublicoTppuForm.idTppuCdTipoPublico.value= '';
				document.administracaoCsCdtbTipoPublicoTppuForm.idTppuCdTipoPublico.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbTipoPublicoTppuForm.imgGravar);	
				desabilitaCamposTipoPublico();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbTipoPublicoTppuForm.tppuDsTipoPublico.focus();}
	catch(e){}
</script>