<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>
function atualizaProduto() {
	document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRODUTO_MANIF %>';
	document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmCmbProdutoManif.name;
	document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
}
	
function inicio(){
	if(document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.length == 2){
		document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.selectedIndex = 1;
	}
	
	atualizaProduto();
}

//Chamado: 89525 - 15/07/2013 - Carlos Nunes
function atualizaDadosEdicao()
{
	administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.value = "";
	administracaoCsCdtbAtendpadraoAtpaForm.idAsn1CdAssuntonivel1.value = "0";
	administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.value = "0";
	administracaoCsCdtbAtendpadraoAtpaForm.idTpmaCdTpmanifestacao.value = "0";
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form action="/AdministracaoCsCdtbAtendpadraoAtpa.do" styleId="administracaoCsCdtbAtendpadraoAtpaForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="idAsnCdAssuntoNivel" />
	<html:hidden property="idAsn1CdAssuntonivel1" />
	<html:hidden property="idAsn2CdAssuntonivel2" />
	<html:hidden property="idMatpCdManiftipo" />
	<html:hidden property="idGrmaCdGrupomanifestacao" />
	<html:hidden property="idSugrCdSupergrupo" />
	<html:hidden property="idTpmaCdTpmanifestacao" />
	<html:hidden property="prasInProdutoassunto" />

    <!-- Chamado: 89525 - 15/07/2013 - Carlos Nunes -->
	<html:select property="idLinhCdLinha" styleClass="principalObjForm" onchange="atualizaDadosEdicao();atualizaProduto();">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		<html:options collection="combo" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
	</html:select>
</html:form>
</body>
</html>