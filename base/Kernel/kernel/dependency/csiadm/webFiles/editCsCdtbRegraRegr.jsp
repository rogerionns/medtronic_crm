<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
long idAcreCdAcaoRegra = 0;
%>


<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbAcaoRegraAcreVo"%>
<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
 	function desabilitaCampos(){
 		document.administracaoCsCdtbRegraRegrForm.idRegrCdRegra.disabled= true;
		document.administracaoCsCdtbRegraRegrForm.idAsmeCdAssuntoMail.disabled= true;	
		document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegra.disabled= true;
		document.administracaoCsCdtbRegraRegrForm.idDocuCdDocumento.disabled= true;
		document.administracaoCsCdtbRegraRegrForm.regrDsRegra.disabled= true;
		document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.disabled= true;
		document.administracaoCsCdtbRegraRegrForm.regrDhInativo.disabled= true;
	}
	
	function inicio(){
		setaChavePrimaria(administracaoCsCdtbRegraRegrForm.idRegrCdRegra.value);

		if(administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegra.value != "" && administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegra.value != "0")
			administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.value = administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegra.value;
	}
	
	function mudarCombo(){
		administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegra.value = administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.options[administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.selectedIndex].value;
	}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbRegraRegrForm" action="/AdministracaoCsCdtbRegraRegr.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<html:hidden property="idAcreCdAcaoRegra" />

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="16%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idRegrCdRegra" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="28%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="16%" align="right" class="principalLabel"><bean:message key="prompt.assuntoEMail"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
		      	<html:select property="idAsmeCdAssuntoMail" styleClass="principalObjForm" > 
		          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
		          <html:options collection="csCdtbAssuntoMailAsmeVector" property="idAsmeCdAssuntoMail" labelProperty="asmeDsAssuntoMail"/> 
		        </html:select> 
          </td>
          <td width="28%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="16%" align="right" class="principalLabel"><bean:message key="prompt.acaoRegra"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
          		<select name="idAcreCdAcaoRegraAux" class="principalObjForm" onchange="mudarCombo();">
					<option value=""><bean:message key="prompt.selecione_uma_opcao"/></option>
					<logic:iterate name="csCdtbAcaoRegraAcreVector" id="csCdtbAcaoRegraAcreVector">
						<option value="<bean:write name="csCdtbAcaoRegraAcreVector" property="idAcreCdAcaoRegra"/>"
							responder="<bean:write name="csCdtbAcaoRegraAcreVector" property="acreInEnviaResposta"/>"
							encaminhar="<bean:write name="csCdtbAcaoRegraAcreVector" property="acreInEncaminha"/>"
							definirAssunto="<bean:write name="csCdtbAcaoRegraAcreVector" property="acreInDefineAssunto"/>"> 
							
							<bean:write name="csCdtbAcaoRegraAcreVector" property="acreDsAcaoRegra"/>
						</option>
					</logic:iterate>
				</select> 
          </td>
          <td width="28%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="16%" align="right" class="principalLabel"><bean:message key="prompt.documento"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
		      	<html:select property="idDocuCdDocumento" styleClass="principalObjForm" > 
		          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
		          <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/> 
		        </html:select> 
          </td>
          <td width="28%">&nbsp;</td>
        </tr>        
        <tr> 
          <td width="16%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="regrDsRegra" style="width:312px" styleClass="text" maxlength="60" /> 
          </td>
          <td width="28%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="16%" align="right" class="principalLabel"><bean:message key="prompt.eMailParaEncaminhar"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="regrDsEmailEncaminhar" styleClass="text" maxlength="100" /> 
          </td>
          <td width="28%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="16%" align="right" class="principalLabel"><bean:message key="prompt.sequencia"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="regrNrSequencia" styleClass="text" maxlength="3" /> 
          </td>
          <td width="28%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="16%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="16%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="regrDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="28%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbRegraRegrForm.regrDsRegra.disabled= true;	
			document.administracaoCsCdtbRegraRegrForm.regrDhInativo.disabled= true;
			document.administracaoCsCdtbRegraRegrForm.idAsmeCdAssuntoMail.disabled= true;
			document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegra.disabled= true;
			document.administracaoCsCdtbRegraRegrForm.idDocuCdDocumento.disabled= true;
			document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.disabled= true;
			
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_REGRA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbRegraRegrForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_REGRA_INCLUSAO_CHAVE%>')){
				desabilitaCampos();
			}		
			document.administracaoCsCdtbRegraRegrForm.idRegrCdRegra.disabled= false;
			document.administracaoCsCdtbRegraRegrForm.idRegrCdRegra.value= '';
			document.administracaoCsCdtbRegraRegrForm.idRegrCdRegra.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_REGRA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_REGRA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbRegraRegrForm.imgGravar);	
				desabilitaCampos();
			}
			
			<%if(request.getAttribute("csCdtbAcaoRegraAcreVo") != null){
				idAcreCdAcaoRegra = ((CsCdtbAcaoRegraAcreVo)request.getAttribute("csCdtbAcaoRegraAcreVo")).getIdAcreCdAcaoRegra();
			}
			%>
			administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegra.value = <%=idAcreCdAcaoRegra%>;
		</script>
</logic:equal>

</html>