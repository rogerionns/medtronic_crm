<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.vo.*, br.com.plusoft.csi.adm.util.*"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
		<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
		<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
	</head>

	<script language="JavaScript">
	parent.idTpmaSelecionados = "";
	/*	function editar(idEvfuCdEventoFollowUp, evfuDsEventoFollowUp, idFuncCdFuncionario, evfuNrTemporesolucao, evfuInTemporesolucao, evfuDhInativo, idMatpCdManifTipo, idSugrCdSupergrupo, idGrmaCdGrupoManifestacao, idTpmaCdTpManifestacao){
			parent.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp.value = idEvfuCdEventoFollowUp;
			parent.administracaoCsCdtbEventoFollowUpEvfuForm.evfuDsEventoFollowUp.value = evfuDsEventoFollowUp;
			parent.administracaoCsCdtbEventoFollowUpEvfuForm.idFuncCdFuncionario.value = idFuncCdFuncionario;
			parent.administracaoCsCdtbEventoFollowUpEvfuForm.evfuNrTemporesolucao.value = evfuNrTemporesolucao;
			parent.administracaoCsCdtbEventoFollowUpEvfuForm.evfuInTemporesolucao.value = evfuInTemporesolucao;
			parent.administracaoCsCdtbEventoFollowUpEvfuForm.evfuDhInativo.value = evfuDhInativo;
			parent.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value = idMatpCdManifTipo;
			parent.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value = idGrmaCdGrupoManifestacao;
			parent.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = idTpmaCdTpManifestacao;
			parent.changeGrupoManif();
		}*/
	</script>

	<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
		<html:form styleId="administracaoCsCdtbEventoFollowUpEvfuForm" action="/AdministracaoCsCdtbEventoFollowUpEvfu.do">
			<html:hidden property="acao" />
			<html:hidden property="tela" />
			<html:hidden property="idEvfuCdEventoFollowUp" />
			<html:hidden property="idTpmaExcluir" />
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<logic:iterate id="vet" name="csAstbTpmanifeventoTpevVector">
					<tr> 
						<td width="6%"> 
							<input type="checkbox" onclick="parent.adicionarTpma(this.value,this.checked?true:false);" value="<bean:write name="vet" property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />">
						</td>
						<td class="principalLstPar" width="30%">
							<bean:write name="vet" property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.matpDsManifTipo" /> 
						</td>
						<td class="principalLstPar" width="32%">
							<bean:write name="vet" property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.grmaDsGrupoManifestacao" /> 
						</td>
						<td class="principalLstPar" width="32%">
							<bean:write name="vet" property="csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao" /> 
						</td>
					</tr>
				</logic:iterate>
			</table>
		</html:form>
	</body>
</html>