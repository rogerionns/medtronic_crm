<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsAstbArqCargaArcaForm.target = admIframe.name;
	document.administracaoCsAstbArqCargaArcaForm.acao.value ='filtrar';
	document.administracaoCsAstbArqCargaArcaForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsAstbArqCargaArcaForm.filtro.value = '';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsAstbArqCargaArcaForm.target = editIframe.name;
	editIframe.document.administracaoCsAstbArqCargaArcaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_ARQCARGA_ARCA%>';
	editIframe.document.administracaoCsAstbArqCargaArcaForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsAstbArqCargaArcaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo1,codigo2){
	
	tab.document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.value = codigo1;
	tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value = codigo2;
	tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencialAntigo.value = codigo2;
	
	tab.document.administracaoCsAstbArqCargaArcaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_ARQCARGA_ARCA%>';
	tab.document.administracaoCsAstbArqCargaArcaForm.target = editIframe.name;
	tab.document.administracaoCsAstbArqCargaArcaForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsAstbArqCargaArcaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
	
}

function submeteSalvar(){


	/*
	if(tab.document.administracaoCsAstbArqCargaArcaForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_ARQCARGA_ARCA%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/>. ');
	}else 
	if(tab.document.administracaoCsAstbArqCargaArcaForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_ASTB_ARQCARGA_ARCA%>'){
	*/

	if(window.document.all.item("Manifestacao").style.display == "none"){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/>. ');
	}else{
		if(tab.document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.value == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_Lay-out"/>.");
			tab.document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.focus();
			return false;
		}
		if(tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_um_campo_de_Lay-out"/>.");
			tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.focus();
			return false;
		}
		if(tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value == ""){
			alert("<bean:message key="prompt.Por_favor_digite_uma_sequencia"/>.");
			tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.focus();
			return false;
		}
		if (tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value.search(/[^\d]/) != -1) {
			alert ("<bean:message key="prompt.Por_favor_digite_apenas_numeros"/>.");
			tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.focus();
			return false;
		}
		
		if(tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value.length > 0){
			var sequenciaExiste=false;
			var campoExiste=false;
			if (editIframe.lstArqCarga.numLinha > 0){
				if (editIframe.lstArqCarga.numLinha == 1 ){
					if (editIframe.document.administracaoCsAstbArqCargaArcaForm.txtSequencia.value != tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value && editIframe.lstArqCarga.editCsAstbArqCargaArcaForm.txtSequencia.value == tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value)
						sequenciaExiste = true;
					
					if(tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value != 1){
						if (editIframe.document.administracaoCsAstbArqCargaArcaForm.txtIdCalo.value != tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value && editIframe.lstArqCarga.editCsAstbArqCargaArcaForm.txtIdCalo.value == tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value){
							campoExiste = true;
						}
					}			
				}else{
					for (i=0;i<editIframe.lstArqCarga.editCsAstbArqCargaArcaForm.txtSequencia.length;i++) {
						if (editIframe.document.administracaoCsAstbArqCargaArcaForm.txtSequencia.value != tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value && editIframe.lstArqCarga.editCsAstbArqCargaArcaForm.txtSequencia[i].value == tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value)
							sequenciaExiste = true;
					}
					
					if(tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value != 1){
						for (i=0;i<editIframe.lstArqCarga.editCsAstbArqCargaArcaForm.txtIdCalo.length;i++) {
							if (editIframe.document.administracaoCsAstbArqCargaArcaForm.txtIdCalo.value != tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value && editIframe.lstArqCarga.editCsAstbArqCargaArcaForm.txtIdCalo[i].value == tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value){
								campoExiste = true;
							}
						}
					}				
				}
			}
			if (sequenciaExiste){
				alert ("<bean:message key="prompt.Sequencia_ja_em_uso"/>");	
				tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.focus();
				return false;
			}	
			if (campoExiste){
				alert ("<bean:message key="prompt.Campo_ja_em_uso"/>");	
				tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.focus();
				return false;
			}	
			
		}
				
/*		if(tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.value == ""){
			alert("<bean:message key="prompt.Por_favor_digite_o_valor_de_inicio"/>.");
			tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.focus();
			return false;
		}
		if (tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.value.search(/[^\d]/) != -1) {
			alert ("<bean:message key="prompt.Por_favor_digite_apenas_numeros"/>.");
			tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.focus();
			return false;
		}		
		if(tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.value == ""){
			alert("<bean:message key="prompt.Por_favor_digite_o_tamanho"/>.");
			tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.focus();
			return false;
		}
		if (tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.value.search(/[^\d]/) != -1) {
			alert ("<bean:message key="prompt.Por_favor_digite_apenas_numeros"/>.");
			tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.focus();
			return false;
		}	
*/

		tab.document.administracaoCsAstbArqCargaArcaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_ARQCARGA_ARCA%>';
		tab.document.administracaoCsAstbArqCargaArcaForm.target = admIframe.name;
		tab.document.administracaoCsAstbArqCargaArcaForm.submit();
		//cancel();

		setTimeout("editIframe.lstArqCarga.location.reload()", 2500);
		
		tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value = "";
		tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value = ""
		tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.value = "";
		tab.document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.value = "";

	}
}

function submeteExcluir(codigo1,codigo2) {
	
	tab.document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.value = codigo1;
	tab.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value = codigo2;
	tab.document.administracaoCsAstbArqCargaArcaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_ARQCARGA_ARCA%>';
	tab.document.administracaoCsAstbArqCargaArcaForm.target = editIframe.name;
	tab.document.administracaoCsAstbArqCargaArcaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsAstbArqCargaArcaForm.submit();
	
	//////////////////////////////
	//AtivarPasta(editIframe);
	//MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
	//////////////////////////////
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		nIdLaou = editIframe.document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.value
		disableEnable(editIframe.document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial, false);
		disableEnable(editIframe.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial, false);
		editIframe.document.administracaoCsAstbArqCargaArcaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_ARQCARGA_ARCA%>';
		editIframe.document.administracaoCsAstbArqCargaArcaForm.target = admIframe.name;
		editIframe.document.administracaoCsAstbArqCargaArcaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		//disableEnable(editIframe.document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial, false);
		editIframe.document.administracaoCsAstbArqCargaArcaForm.submit();

		/////////////////////////
		//AtivarPasta(admIframe);
		//MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		/////////////////////////
		//editIframe.location = 'AdministracaoCsAstbArqCargaArca.do?tela=editCsAstbArqCargaArca&acao=incluir';
		
		editIframe.document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.value = "";
		editIframe.document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value = ""
		editIframe.document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.value = "";
		editIframe.document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.value = "";
		editIframe.document.administracaoCsAstbArqCargaArcaForm.acao.value = "incluir";
			
	}else{
		editIframe.MontaLista();
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsAstbArqCargaArca.do?tela=editCsAstbArqCargaArca&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsAstbArqCargaArcaForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsAstbArqCargaArcaForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_ARQCARGA_ARCA%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			break;
		case editIframe:
			tab.document.administracaoCsAstbArqCargaArcaForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_ARQCARGA_ARCA%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsAstbArqCargaArcaForm"	action="/AdministracaoCsAstbArqCargaArca.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
										<bean:message key="prompt.procurar"/></td>
                				      <td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);"> 
				                        <bean:message key="prompt.camposLayout"/></td>
								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"> 
                  <div name="Manifestacao" id="Manifestacao"
								style="width: 97%; height: 225px;display: none"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td height="380" valign="top"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsAstbArqCargaArca.do?tela=editCsAstbArqCargaArca&acao=incluir"
										width="98%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
 				 </div>
                  <div name="Destinatario" id="Destinatario"
								style="width: 97%; height: 199px;display: block"> 
                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLabel" colspan="4"><bean:message key="prompt.descricao"/></td>
                        <td class="principalLabel" width="18%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="65%"> 
                          <table border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="25%"> <html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
                              </td>
                              <td width="05%"> &nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()"> 
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" colspan="5">&nbsp;</td>
                      </tr>
                   </table>
                   <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLstCab" width="2%">&nbsp;</td>
                        <td class="principalLstCab" width="12%"><bean:message key="prompt.codigo"/></td>
                        <td class="principalLstCab" width="57%"><bean:message key="prompt.descricao"/></td>
                        <td class="principalLstCab" width="11%">&nbsp;</td>
                        <td class="principalLstCab" align="left" width="18%"><bean:message key="prompt.inativo"/></td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="5" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsAstbArqCargaArca.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
                      </tr>
                    </table>
							</div>

							
                </td>
							<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="submeteSalvar()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPOSDOLAYOUT_INCLUSAO_CHAVE%>', document.administracaoCsAstbArqCargaArcaForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPOSDOLAYOUT_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPOSDOLAYOUT_ALTERACAO_CHAVE%>')){
			document.administracaoCsAstbArqCargaArcaForm.imgGravar.disabled=true;
			document.administracaoCsAstbArqCargaArcaForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsAstbArqCargaArcaForm.imgGravar.title='';
	}
	
	window.top.document.getElementById("divDesabilita").style.visibility = "visible";
	//window.top.document.getElementById("imgLstEmpresas").disabled=true;
	//window.top.document.getElementById("imgLstEmpresas").className = 'geralImgDisable';
	
</script>


</body>
</html>