<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" 
		import="br.com.plusoft.csi.adm.helper.Configuracoes,br.com.plusoft.csi.adm.helper.ConfiguracaoConst" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
	<title>Permissoes</title>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	<script type="text/javascript">
		var permissaoReady = false;
		var objPermissao = new Array();
		
		/**
		Este metodo tem como objetivo verificar se o usuario logado no sistema tem direito a 
		uma determinada	funcionalidade
		*/
		function findPermissao(funcionalidade){
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PERMISSIONAMENTO,request).equals("S") || "true".equals(request.getSession().getAttribute("usuarioplus"))) {%>
			return true;
			<%}%>
		
			var retorno = false;		
			for (var i = 0; i <= objPermissao.length; i++){
				if (objPermissao[i] == funcionalidade){
					retorno = true;
					break;
				}
			}
			
			return retorno;
		}
		
		//Busca todas as permissoes
		function findPermissoesByFuncionalidade(funcionalidade){
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PERMISSIONAMENTO,request).equals("S") || "true".equals(request.getSession().getAttribute("usuarioplus"))) {
				out.println("return true");
			}
			%>
		
			var retorno = "";		
			var permissao = "";
			for (var i = 0; i < objPermissao.length; i++){
				permissao = objPermissao[i];
				if (permissao.substring(0, funcionalidade.length) == funcionalidade){
					//if (retorno != ""){
					//	retorno = retorno + "&";
					//}
					retorno = retorno + "&perm=" + permissao.substring(funcionalidade.length);
					}
				}			
			return retorno;
		}

		
	</script>
</head>

<body onload="window.top.showError('<%=request.getAttribute("msgerro")%>')">

	<html:form action="/AdministracaoPermissionamento.do" styleId="administracaoPermissionamentoForm">
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
	
		<script>
			<logic:present name="permissaoVector">
			<logic:iterate name="permissaoVector" id="permissao" indexId="sequencia">			
			objPermissao.push("<bean:write name="permissao" property="csAstbFunciopeniFupnVo.fupnDsFunciopeni"/>");
			</logic:iterate>
			</logic:present>

			permissaoReady = true;
		</script>
	
	</html:form>
</body>
</html>