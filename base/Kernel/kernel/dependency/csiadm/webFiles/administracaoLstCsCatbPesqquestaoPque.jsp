<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsCatbPesqquestaoPqueForm" action="/AdministracaoCsCatbPesqquestaoPque.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csCatbPesqquestaoPqueVector" indexId="sequencia"> 
  <tr> 
    <td width="3%"  > 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18" height="18" class="principalLstParMao" title="<bean:message key="prompt.excluir"/>"  onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="idPesq" />','<bean:write name="ccttrtVector" property="idQues" />')" > 
    </td>
    <td  class="principalLstParMao" width="66%"  onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPesq" />','<bean:write name="ccttrtVector" property="idQues" />')">
    	&nbsp;
    	<%=acronymChar(((CsCatbPesqquestaoPqueVo)ccttrtVector).getCsCdtbQuestaoQuesVo().getQuesDsQuestao(),50)%>
    </td>
    <td  class="principalLstParMao" width="10%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPesq" />','<bean:write name="ccttrtVector" property="idQues" />')" > 
    	<bean:write name="ccttrtVector" property="pqueNrSequencia" /> 
    </td>
    <td  class="principalLstParMao" width="14%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPesq" />','<bean:write name="ccttrtVector" property="idQues" />')" > 
    <logic:equal name="ccttrtVector" property="pqueInEncerramento" value="true">	
      <bean:message key="prompt.sim"/> </logic:equal> <logic:notEqual name="ccttrtVector" property="pqueInEncerramento" value="true">	
      <bean:message key="prompt.nao"/> </logic:notEqual> 
    </td>
    <td  class="principalLstParMao" width="7%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPesq" />','<bean:write name="ccttrtVector" property="idQues" />')" > 
    	<logic:equal name="ccttrtVector" property="pqueInObrigatorio" value="true">	
      <bean:message key="prompt.sim"/> </logic:equal> <logic:notEqual name="ccttrtVector" property="pqueInObrigatorio" value="true">	
      <bean:message key="prompt.nao"/> </logic:notEqual> 
    </td>    
   
  </tr>
  </logic:iterate>
</table>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISAQUESTAO_EXCLUSAO_CHAVE%>', editCsCatbPesqquestaoPqueForm.lixeira);	
</script>

</html:form>
</body>
</html>