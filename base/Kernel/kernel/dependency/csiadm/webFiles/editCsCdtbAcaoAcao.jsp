<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
	function textCounter(field, countfield, maxlimit) {
		if (field.value.length > maxlimit) 
			field.value = field.value.substring(0, maxlimit);
		else 
			countfield.value = maxlimit - field.value.length;
	}
	
	function desmarcaPesquisa() {
		if (document.administracaoCsCdtbAcaoAcaoForm.acaoInEtiqueta.checked)
			document.administracaoCsCdtbAcaoAcaoForm.idPesqCdPesquisa.value = '';
	}

	function desmarcaEtiqueta() {
		if (document.administracaoCsCdtbAcaoAcaoForm.idPesqCdPesquisa.value != '')
			document.administracaoCsCdtbAcaoAcaoForm.acaoInEtiqueta.checked = false;
	}
</script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbAcaoAcaoForm" action="/AdministracaoCsCdtbAcaoAcao.do">
	<input readonly type="hidden" name="remLen" size="3" maxlength="3" value="4000" > 
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="11%"> <html:text property="idAcaoCdAcao" styleClass="text" disabled="true" /> 
    </td>
    <td width="65%">&nbsp;</td>
    <td width="7%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.pesquisa"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      	<html:select property="idPesqCdPesquisa" styleClass="principalObjForm" onchange="desmarcaEtiqueta()"> 
          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
          <html:options collection="csCdtbPesquisaPesqVector" property="idCodigo" labelProperty="pesqDsPesquisa"/> 
        </html:select> 
	</td>
    <td width="7%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.documento"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      	<html:select property="idDocuCdDocumento" styleClass="principalObjForm" > 
          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
          <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/> 
        </html:select> 
	</td>
    <td width="7%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.material"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      	<html:select property="idMaprCdMaterialPromocional" styleClass="principalObjForm" > 
          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
          <html:options collection="csCdtbMaterialPromoMaprVector" property="idMaprCdMaterialPromocional" labelProperty="maprDsDescricao"/> 
        </html:select> 
	</td>
    <td width="7%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="acaoDsDescricao" styleClass="text" maxlength="60" /> 
    </td>
    <td width="7%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.observacao"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
    	<html:textarea property="acaoDsObservacao" styleClass="text" rows="7" cols="50" onkeypress="textCounter(this.form.acaoDsObservacao,this.form.remLen,4000)" onkeyup="textCounter(this.form.acaoDsObservacao,this.form.remLen,4000)"/>
    </td>
    <td width="7%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel">&nbsp;</td>
    <td colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="4%"> 
            <div align="left"></div>
            <html:checkbox value="true" property="acaoInEtiqueta" onclick="desmarcaPesquisa()"/> </td>
          <td class="principalLabel" width="96%"> <bean:message key="prompt.etiqueta"/></td>
        </tr>
        <tr> 
          <td align="right" width="4%"> 
            <div align="left"></div>
            <html:checkbox value="true" property="acaoInEmail" onclick="desmarcaPesquisa()"/> </td>
          <td class="principalLabel" width="96%"> <bean:message key="prompt.email"/></td>
        </tr>
      </table>
    </td>
    <td width="7%">&nbsp;</td>
  </tr>
  <!--tr> 
    <td width="17%" align="right" class="principalLabel">&nbsp;</td>
    <td colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="4%"> 
            <div align="left"></div>
            <------html:checkbox value="true" property="acaoInRequerAtivo"/> </td>
          <td class="principalLabel" width="96%"><---------bean:message key="prompt.requerAtivo"/></td>
        </tr>
      </table>
    </td>
    <td width="7%">&nbsp;</td>
  </tr-->
  <tr> 
    <td width="17%">&nbsp;</td>
    <td colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="4%"> 
            <div align="left"></div>
            <html:checkbox value="true" property="acaoDhInativo"/> </td>
          <td class="principalLabel" width="96%">&nbsp;<bean:message key="prompt.inativo"/> </td>
        </tr>
      </table>
    </td>
    <td width="7%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td class="principalLabel" width="65%">&nbsp; </td>
    <td width="7%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbAcaoAcaoForm.acaoDsDescricao.disabled= true;	
			document.administracaoCsCdtbAcaoAcaoForm.acaoDhInativo.disabled= true;
			document.administracaoCsCdtbAcaoAcaoForm.idPesqCdPesquisa.disabled= true;
			document.administracaoCsCdtbAcaoAcaoForm.idDocuCdDocumento.disabled= true;
			document.administracaoCsCdtbAcaoAcaoForm.idMaprCdMaterialPromocional.disabled= true;
			document.administracaoCsCdtbAcaoAcaoForm.acaoDsObservacao.disabled= true;
			document.administracaoCsCdtbAcaoAcaoForm.acaoInEtiqueta.disabled= true;
//			document.administracaoCsCdtbAcaoAcaoForm.acaoInRequerAtivo.disabled= true;
									
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			document.administracaoCsCdtbAcaoAcaoForm.idAcaoCdAcao.disabled= false;
			document.administracaoCsCdtbAcaoAcaoForm.idAcaoCdAcao.value= '';
			document.administracaoCsCdtbAcaoAcaoForm.idAcaoCdAcao.disabled= true;
		</script>
</logic:equal>
</html>