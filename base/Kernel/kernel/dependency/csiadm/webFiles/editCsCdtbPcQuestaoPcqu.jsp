<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">

	function VerificaCampos(){
		if (document.administracaoCsCdtbPcQuestaoPcquForm.pcquNrTempoSeg.value == "0" ){
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquNrTempoSeg.value = "" ;
		}
		if (document.administracaoCsCdtbPcQuestaoPcquForm.pcquNrPontos.value == "0" ){
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquNrPontos.value = "" ;
		}
		if (document.administracaoCsCdtbPcQuestaoPcquForm.pcquInDificuldade.value == "0" ){
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquInDificuldade.value = "" ;
		}
		if (document.administracaoCsCdtbPcQuestaoPcquForm.pcquInFrequencia.value == "0" ){
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquInFrequencia.value = "" ;
		}
			
	}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos()">

<html:form styleId="administracaoCsCdtbPcQuestaoPcquForm" action="/AdministracaoCsCdtbPcQuestaoPcqu.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="11%"> <html:text property="idPcquCdPcQuestao" styleClass="text" disabled="true" /> 
    </td>
    <td width="61%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.pesquisa"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idPcpeCdPcpesquisa" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbPcPesquisaPcpeVector" property="idPcpeCdPcpesquisa" labelProperty="pcpeDsPcPesquisa"/> 
      </html:select> </td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.rodada"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idPcroCdPcRodada" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbPcRodadaPcroVector" property="idPcroCdPcRodada" labelProperty="pcroDsRodada"/> 
      </html:select> </td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.questao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><html:textarea property="pcquDsQuestao" onkeypress="textCounter(this.form.pcquDsQuestao,4000)" onkeyup="textCounter(this.form.pcquDsQuestao,4000)"  styleClass="text" rows="5" cols="50" /></td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.resposta"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><html:textarea property="pcquDsResposta" onkeypress="textCounter(this.form.pcquDsResposta,4000)" onkeyup="textCounter(this.form.pcquDsResposta,4000)"  styleClass="text" rows="7" cols="50" /></td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.tempoEmSegundos"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="11%"><html:text property="pcquNrTempoSeg" styleClass="text" maxlength="6" /> 
    </td>
    <td width="61%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.numeroPontos"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="11%"><html:text property="pcquNrPontos" styleClass="text" maxlength="6" /></td>
    <td width="61%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.dificuldade"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="11%"><html:text property="pcquInDificuldade" styleClass="text" maxlength="6" /></td>
    <td width="61%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.frequencia"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="11%"><html:text property="pcquInFrequencia" styleClass="text" maxlength="6" /></td>
    <td width="61%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td class="principalLabel" width="61%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="83%"> <html:checkbox value="true" property="pcquDhInativo"/></td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/></td>
        </tr>
      </table>
    </td>
    <td width="9%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquDsQuestao.disabled= true;	
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquDhInativo.disabled= true;

			document.administracaoCsCdtbPcQuestaoPcquForm.idPcpeCdPcpesquisa.disabled= true;
			document.administracaoCsCdtbPcQuestaoPcquForm.idPcroCdPcRodada.disabled= true;
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquDsResposta.disabled= true;
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquInDificuldade.disabled= true;
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquInFrequencia.disabled= true;
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquNrTempoSeg.disabled= true;
			document.administracaoCsCdtbPcQuestaoPcquForm.pcquNrPontos.disabled= true;
			
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			document.administracaoCsCdtbPcQuestaoPcquForm.idPcquCdPcQuestao.disabled= false;
			document.administracaoCsCdtbPcQuestaoPcquForm.idPcquCdPcQuestao.value= '';
			document.administracaoCsCdtbPcQuestaoPcquForm.idPcquCdPcQuestao.disabled= true;
		</script>
</logic:equal>
</html>