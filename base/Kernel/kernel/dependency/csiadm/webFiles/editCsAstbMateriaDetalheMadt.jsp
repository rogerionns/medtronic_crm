<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>

function carregaClassificacao() {
	document.administracaoCsAstbMateriaDetalheMadtForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsAstbMateriaDetalheMadtForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_CLASSIFICACAO_CLAS%>';
	document.administracaoCsAstbMateriaDetalheMadtForm.target = cmbClassificacao.name;
	document.administracaoCsAstbMateriaDetalheMadtForm.submit();
}

function habilitaCadastro() {
	try{
		cmbDescricao.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsDetalheClass.value = '';
		cmbDescricao.document.administracaoCsAstbMateriaDetalheMadtForm.idDetcCdDetalheClass.value = '';
	}catch(e){}
	document.administracaoCsAstbMateriaDetalheMadtForm.idPessCdPessoa.value = 0;
	document.administracaoCsAstbMateriaDetalheMadtForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsAstbMateriaDetalheMadtForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_DETALHECLASS_DETC%>';
	document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value = "0";
	document.administracaoCsAstbMateriaDetalheMadtForm.detcDsDetalheClass.value = "";
	document.administracaoCsAstbMateriaDetalheMadtForm.detcDsAutor.value = "";
	document.administracaoCsAstbMateriaDetalheMadtForm.target = cmbDescricao.name;
	document.administracaoCsAstbMateriaDetalheMadtForm.submit();
}

function MontaLista(){
	lstArqCarga.location.href= "AdministracaoCsAstbMateriaDetalheMadt.do?tela=administracaoLstCsAstbMateriaDetalheMadt&acao=<%=Constantes.ACAO_VISUALIZAR%>&idMateCdMateria=" + document.administracaoCsAstbMateriaDetalheMadtForm.idMateCdMateria.value;
}

</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista();">
<html:form styleId="administracaoCsAstbMateriaDetalheMadtForm" action="/AdministracaoCsAstbMateriaDetalheMadt.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="idClasCdClassificacao" />
	<html:hidden property="idDetcCdDetalheClass"/>
	<html:hidden property="idPessCdPessoa"/>
	
	<br>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.materia" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      <html:select property="idMateCdMateria" styleClass="principalObjForm" onchange="MontaLista()">
        <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
        <html:options collection="csCdtbMateriaMateVector" property="idMateCdMateria" labelProperty="mateDsMateria" />
      </html:select>
    </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.tipoClassificacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      <html:select property="idTpclCdTpClassificacao" onchange="carregaClassificacao()" styleClass="principalObjForm" > 
        <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
        <html:options collection="csCdtbTpClassificacaoTpclVector" property="idTpclCdTpClassificacao" labelProperty="tpclDsTpClassificacao"/> 
      </html:select> </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.classificacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      <iframe name="cmbClassificacao" src="AdministracaoCsAstbMateriaDetalheMadt.do?acao=<%=Constantes.ACAO_RECUSAR%>&tela=<%=MAConstantes.TELA_CMB_CS_CDTB_CLASSIFICACAO_CLAS%>" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.descricao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
		<html:text property="detcDsDetalheClass"  styleClass="principalObjForm" maxlength="100" />
    </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.autor" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
		<html:text property="detcDsAutor"  styleClass="principalObjForm" maxlength="60" />
    </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="220">
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="4%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="32%">&nbsp;<bean:message key="prompt.tipoClassificacao" /></td>
          <td class="principalLstCab" width="32%">&nbsp;<bean:message key="prompt.classificacao" /></td>
          <td class="principalLstCab" width="32%">&nbsp;<bean:message key="prompt.descricao" /></td>
        </tr>
        <tr valign="top"> 
          <td colspan="4" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
</html>