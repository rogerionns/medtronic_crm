<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
 	function desabilitaCampos(){
 		document.administracaoCsCdtbItemRegraItreForm.itreNrSequencia.disabled= true;
		document.administracaoCsCdtbItemRegraItreForm.idRegrCdRegra.disabled= true;	
		document.administracaoCsCdtbItemRegraItreForm.itreDsCampo.disabled= true;
		document.administracaoCsCdtbItemRegraItreForm.itreDsCondicao.disabled= true;
		document.administracaoCsCdtbItemRegraItreForm.itreDsValor.disabled= true;
		<%//Chamado: 105868 - 17/12/2015 - Carlos Nunes%>
		document.administracaoCsCdtbItemRegraItreForm.capoNrSequencia.disabled=true;
	}	
	
	function carregarLista(){
		lstArqCarga.document.location = "AdministracaoCsCdtbItemRegraItre.do?tela=administracaoLstCsCdtbItemRegraItre&acao=visualizar"+
			"&idRegrCdRegra="+ administracaoCsCdtbItemRegraItreForm.idRegrCdRegra.value;
	}
	
	function excluir(){
		document.administracaoCsCdtbItemRegraItreForm.itreDsCampo.disabled= true;	
		document.administracaoCsCdtbItemRegraItreForm.itreDsCondicao.disabled= true;
		document.administracaoCsCdtbItemRegraItreForm.idRegrCdRegra.disabled= true;
		document.administracaoCsCdtbItemRegraItreForm.itreDsValor.disabled= true;
		document.administracaoCsCdtbItemRegraItreForm.itreNrSequencia.disabled= true;
		<%//Chamado: 105868 - 17/12/2015 - Carlos Nunes%>
		document.administracaoCsCdtbItemRegraItreForm.capoNrSequencia.disabled=true;
		
		confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
		parent.setConfirm(confirmacao);
	}
	
	<%//Chamado: 105868 - 17/12/2015 - Carlos Nunes%>
	function definirAssunto(){
		if(document.administracaoCsCdtbItemRegraItreForm.itreDsCampo.value == "CAIXAPOSTAL"){
			document.getElementById("divCaixaPostal").style.display = 'block';
			document.getElementById("divValor").style.display = 'none';
		}else{
			document.getElementById("divCaixaPostal").style.display = 'none';
			document.getElementById("divValor").style.display = 'block';
		}
	}
	
	<%//Chamado: 105868 - 17/12/2015 - Carlos Nunes%>
	function definirValor(){
		if(document.administracaoCsCdtbItemRegraItreForm.itreDsCampo.value == "CAIXAPOSTAL"){
			document.administracaoCsCdtbItemRegraItreForm.itreDsValor.value = document.administracaoCsCdtbItemRegraItreForm.capoNrSequencia.value;
		}else{
			document.administracaoCsCdtbItemRegraItreForm.itreDsValor.value = "";
			document.administracaoCsCdtbItemRegraItreForm.capoNrSequencia.value = "";
		}
	}
	
</script>

<%//Chamado: 105868 - 17/12/2015 - Carlos Nunes%>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');definirAssunto();carregarLista()">

<html:form styleId="administracaoCsCdtbItemRegraItreForm" action="/AdministracaoCsCdtbItemRegraItre.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="itreNrSequencia" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.regra"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
	      	<html:select property="idRegrCdRegra" styleClass="principalObjForm" onchange="carregarLista();"> 
	          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
	          <html:options collection="csCdtbRegraRegrVector" property="idRegrCdRegra" labelProperty="regrDsRegra"/> 
	        </html:select> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
	      	<html:select property="itreDsCampo" styleClass="principalObjForm" onchange="definirAssunto();definirValor()"> 
	          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
	          <html:option value="ASSUNTO"> <bean:message key="prompt.assuntom"/> </html:option> 
	          <html:option value="MENSAGEM"> <bean:message key="prompt.mensagemm"/> </html:option> 
	          <html:option value="DE"> <bean:message key="prompt.dem"/> </html:option>
	          <html:option value="PARA"> <bean:message key="prompt.param"/> </html:option> 
	          <html:option value="CAIXAPOSTAL"> <bean:message key="prompt.caixaPostalMaiuscula"/> </html:option> 
	        </html:select> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.condicao"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
	      	<html:select property="itreDsCondicao" styleClass="principalObjForm"> 
	          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
	          <html:option value="IGUAL"> <bean:message key="prompt.igualm"/> </html:option> 
	          <html:option value="CONT�M"> <bean:message key="prompt.contemm"/> </html:option> 
	        </html:select> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.valor"/> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <%//Chamado: 105868 - 17/12/2015 - Carlos Nunes%>
          	<div id="divValor">
            	<html:text property="itreDsValor" styleClass="text" maxlength="50" />
            </div>
            <div id="divCaixaPostal">
            	<html:select property="capoNrSequencia" styleClass="principalObjForm" onchange="definirValor()"> 
		          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
		          <html:options collection="csCdtbCaixaPostalCapoVector" property="capoNrSequencia" labelProperty="capoDsUsuarioRec"/> 
		        </html:select> 
            </div> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="5%" class="principalLstCab">&nbsp;</td>
          <td class="principalLstCab" width="35%"> &nbsp;Descri��o</td>
          <td class="principalLstCab" width="32%">Condi��o</td>
          <td class="principalLstCab" width="28%">Valor</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"><iframe id="lstArqCarga" name="lstArqCarga" src="" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_ITEMREGRA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbItemRegraItreForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_ITEMREGRA_INCLUSAO_CHAVE%>')){
				desabilitaCampos();
			}		
			document.administracaoCsCdtbItemRegraItreForm.itreNrSequencia.disabled= false;
			//document.administracaoCsCdtbItemRegraItreForm.itreNrSequencia.value= '';
			document.administracaoCsCdtbItemRegraItreForm.itreNrSequencia.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_ITEMREGRA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_ITEMREGRA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbItemRegraItreForm.imgGravar);	
				desabilitaCampos();
			}
		</script>
</logic:equal>
</html>