<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.identificacao" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language='javascript' src='<bean:message key="prompt.funcoes"/>/TratarDados.js'></script>
<script>
function Buscar(){
	var te = false;
	for (x = 0;  x < document.identificaForm.elements.length;  x++) {
		Campo = document.identificaForm.elements[x];
		if(Campo.type == "text" && Campo.value != "" ){
			te  = true
		}
	}
	if (te==false){
		alert('<bean:message key="prompt.alert.campo.busca" />');
	}else{
		if (document.identificaForm.pessNmPessoa.value.length > 0 && document.identificaForm.pessNmPessoa.value.length < 3) {
			alert("<bean:message key="prompt.O_campo_Nome_precisa_de_no_minimo_3_letras_para_fazer_o_filtro"/>");
		} else {
			document.identificaForm.acao.value= "<%= Constantes.ACAO_CONSULTAR %>";
			msg = true;
			document.all.item('aguarde').style.visibility = 'visible';
			document.identificaForm.submit();
		}
	}
}

function abreMr(id, nome){
	wi = window.dialogArguments;
	wi.abrir(id, nome);
	self.close();
}

function limpar(){
	for (x = 0;  x < document.identificaForm.elements.length;  x++){
		Campo = document.identificaForm.elements[x];
		if(Campo.type == "text" ){
			Campo.value = "";
		}
		if(Campo.type == "radio" && Campo.value == "3"){
			Campo.checked = true;
		}
		if(Campo.type == "checkbox"){
			Campo.checked = false;
		}
	}
	
	habilitaCamposCorp();
	
}

function pressEnter(event) {
    if (event.keyCode == 13) {
    	Buscar();
    }
}

var msg = false;

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';">
<html:form action="/ResultListIdentifica.do" target="lstIdentificacao" styleId="identificaForm">
  <html:hidden property="acao" />
  <input type="hidden" name="mr" value="<%=request.getParameter("mr")%>">
  <input type="hidden" name="corresp" value="<%=request.getParameter("corresp")%>">  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.identificacao" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              
            <table width="99%" border="0" cellspacing="2" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabel" width="44%"> <html:checkbox property="BIncondicional" value="true" ></html:checkbox> 
                  <bean:message key="prompt.buscaincond" /></td>
                <td class="principalLabel" width="4%">&nbsp;</td>
                <td class="principalLabel" width="15%"> 
                  &nbsp;
                </td>
                <td class="principalLabel" width="4%"> 
                  <div align="right">&nbsp;</div>
                </td>
                <td class="principalLabel" width="18%"> 
                  &nbsp;
                </td>
                <td class="principalLabel" width="15%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="2"><bean:message key="prompt.nome" /></td>
                <td class="principalLabel" width="15%"><bean:message key="prompt.codigo" /></td>
                <td class="principalLabel" colspan="3">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="2"> <html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" onkeydown="pressEnter(event)"/> 
                </td>
                <td class="principalLabel" width="15%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" height="19"> <html:text property="idPessCdPessoa" styleClass="principalObjForm" onfocus="SetarEvento(this, 'N')" maxlength="10" onkeydown="pressEnter(event)"/> 
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" colspan="3"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" width="31%"> <html:radio property="tipoChamado" value="1"></html:radio> 
                        <%= getMessage("prompt.chamado", request)%></td>
                      <!--td class="principalLabel" height="19" width="39%"> <html:radio property="tipoChamado" value="2"></html:radio> 
                        <bean:message key="prompt.reclamacao" /></td-->
                      <td class="principalLabel" width="70%"> <html:radio property="tipoChamado" value="3"></html:radio> 
                        <bean:message key="prompt.pessoa" /></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3"><bean:message key="prompt.endereco" /></td>
                <td class="principalLabel" colspan="3"><bean:message key="prompt.login" /></td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3"> <html:text property="peenDsLogradouro" styleClass="principalObjForm" maxlength="40" onkeydown="pressEnter(event)" />
                </td>
                <td class="principalLabel" colspan="3">
                  <html:text property="pessCdInternetId" styleClass="principalObjForm" maxlength="10" onkeydown="pressEnter(event)" />
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3"><bean:message key="prompt.email" /></td>
                <td class="principalLabel" colspan="3"><bean:message key="prompt.telefone" /></td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3"> <html:text property="email" styleClass="principalObjForm" maxlength="60" onkeydown="pressEnter(event)" /> 
                </td>
                <td class="principalLabel" colspan="3"> <html:text property="telefone" styleClass="principalObjForm" maxlength="15" onkeydown="pressEnter(event)" /> 
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="6"> 
                  <table width="100%" border="0" cellspacing="2" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" width="25%"><bean:message key="prompt.cep" /></td>
                      <td class="principalLabel" width="23%"><bean:message key="prompt.cpf" /> / <bean:message key="prompt.cnpj" /></td>
                      <td class="principalLabel" width="21%"><bean:message key="prompt.rg" /> / <bean:message key="prompt.ie" /> </td>
                      <td class="principalLabel" width="31%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" width="25%"> <html:text property="peenDsCep" styleClass="principalObjForm" maxlength="8" onkeydown="pressEnter(event)" /> 
                      </td>
                      <td class="principalLabel" width="23%"> <html:text property="pessDsCgccpf" styleClass="principalObjForm" maxlength="15" onkeydown="pressEnter(event)" /> 
                      </td>
                      <td class="principalLabel" width="21%"> <html:text property="pessDsIerg" styleClass="principalObjForm" maxlength="20" onkeydown="pressEnter(event)" /> 
                      </td>
                      <td class="principalLabel" width="31%" align="right"> 
                          <table border="0" cellspacing="0" cellpadding="4">
                            <tr> 
                              <td class="principalLabelOptChk">
                              	<img src="webFiles/images/botoes/lupa.gif" width="15" height="15" title="<bean:message key="prompt.pesquisar"/>" border="0" onclick="Buscar()" class="geralCursoHand">
                              </td>
                              <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" onclick="limpar();" class="geralCursoHand"></td>
                            </tr>
                          </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3">&nbsp; </td>
                <td class="principalLabel" colspan="3"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr valign="top"> 
                  <td height="220" colspan="7"><iframe name="lstIdentificacao" src="ResultListIdentifica.do" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
              </table>
              
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
  </form>
<div id="aguarde" style="position:absolute; left:300px; top:130px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/aguarde.htm" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
<script>
	document.identificaForm.idPessCdPessoa.value = ""
	document.all.item('aguarde').style.visibility = 'visible';
</script>
</html:form>