<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<OBJECT id=colorChooser CLASSID="clsid:3050f819-98b5-11cf-bb82-00aa00bdce0b" width="0px" height="0px"></OBJECT>

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">

	function chooseColor(){
		showModalDialog("webFiles/colorChooserAssuntoMail.jsp", window, "dialogHeight: 350px; dialogWidth: 400px");
	}

	function atualizarCor(cor){
		administracaoCsCdtbAssuntoMailAsmeForm.asmeDsCor.value = cor;
		document.getElementById("tdCor").bgColor = cor;
	}

	function inicio(){
		//setaAssociacaoMultiEmpresa();
		atualizarCor(administracaoCsCdtbAssuntoMailAsmeForm.asmeDsCor.value);
		setaChavePrimaria(administracaoCsCdtbAssuntoMailAsmeForm.idAsmeCdAssuntoMail.value);
	}

	function VerificaCampos(){
		if (document.administracaoCsCdtbAssuntoMailAsmeForm.asmeNrTempoResolucao.value == "0" ){
			document.administracaoCsCdtbAssuntoMailAsmeForm.asmeNrTempoResolucao.value = "" ;
		}
	}
</script>

<script>
 	function desabilitaCampos(){
 		document.administracaoCsCdtbAssuntoMailAsmeForm.idAsmeCdAssuntoMail.disabled= true;
		document.administracaoCsCdtbAssuntoMailAsmeForm.asmeDsAssuntoMail.disabled= true;	
		document.administracaoCsCdtbAssuntoMailAsmeForm.asmeNrTempoResolucao.disabled= true;
		document.administracaoCsCdtbAssuntoMailAsmeForm.asmeInPrioridade.disabled= true;
		document.administracaoCsCdtbAssuntoMailAsmeForm.asmeDhInativo.disabled= true;
	}	
</script>

<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>');VerificaCampos()">

<html:form styleId="administracaoCsCdtbAssuntoMailAsmeForm" action="/AdministracaoCsCdtbAssuntoMailAsme.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="11%"> <html:text property="idAsmeCdAssuntoMail" styleClass="text" disabled="true" /> 
    </td>
    <td width="32%">&nbsp;</td>
    <td width="38%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="asmeDsAssuntoMail" styleClass="text" maxlength="60" style="width: 300px;" /> 
    </td>
    <td width="38%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%"> 
      <div align="right"><bean:message key="prompt.tempoResolucao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td><html:text property="asmeNrTempoResolucao" styleClass="text" maxlength="5" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/></td>
    <td align="right" class="principalLabel">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    		<tr>
    			<td width="40%">
					<html:select property="asmeInTemporesolucao" styleClass="principalObjForm" > 
			      		<html:option value="D"><bean:message key="prompt.dias"/></html:option> 
			      		<html:option value="H"><bean:message key="prompt.horas"/></html:option> 
			      		<html:option value="M"><bean:message key="prompt.minutos"/></html:option> 
			      	</html:select>
    			</td>
    			<td>&nbsp;</td>
    		</tr>
    	</table>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%"> 
      <div align="right"><bean:message key="prompt.prioridade"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td> <html:select property="asmeInPrioridade" styleClass="principalObjForm" > 
      <html:option value=""></html:option> <html:option value="B"><bean:message key="prompt.baixa"/></html:option> 
      <html:option value="A"><bean:message key="prompt.alta"/></html:option> </html:select> </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%">
    	<div align="right"><bean:message key="prompt.corDaFonte"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td><html:text property="asmeDsCor" styleClass="text" maxlength="7" onclick="chooseColor();"/></td>
    <td name="tdCor" id="tdCor">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td class="principalLabel" width="32%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="83%"> <html:checkbox value="true" property="asmeDhInativo"/> 
          </td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/> </td>
        </tr>
      </table>
    </td>
    <td width="38%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbAssuntoMailAsmeForm.asmeDsAssuntoMail.disabled= true;	
			document.administracaoCsCdtbAssuntoMailAsmeForm.asmeDhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_ASSUNTOEMAIL_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbAssuntoMailAsmeForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_ASSUNTOEMAIL_INCLUSAO_CHAVE%>')){
				desabilitaCampos();
			}
			document.administracaoCsCdtbAssuntoMailAsmeForm.idAsmeCdAssuntoMail.disabled= false;
			document.administracaoCsCdtbAssuntoMailAsmeForm.idAsmeCdAssuntoMail.value= '';
			document.administracaoCsCdtbAssuntoMailAsmeForm.idAsmeCdAssuntoMail.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_ASSUNTOEMAIL_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_ASSUNTOEMAIL_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbAssuntoMailAsmeForm.imgGravar);	
				desabilitaCampos();
			}
		</script>
</logic:equal>

</html>