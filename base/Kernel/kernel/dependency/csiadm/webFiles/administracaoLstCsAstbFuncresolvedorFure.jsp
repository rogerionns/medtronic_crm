<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>

<script>
	function editaFuncResolvedor(idFunc,bVer,bResolver){

		parent.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].value = idFunc;

		if(bVer == 'true'){
			parent.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.fureInVer'].checked = true;
		}else{
			parent.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.fureInVer'].checked = false;
		}

		if(bResolver == 'true'){
			parent.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.fureInResolver'].checked = true;
		}else{
			parent.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.fureInResolver'].checked = false;
		}
		
		
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsAstbFuncresolvedorFureForm" action="/AdministracaoCsAstbFuncresolvedorFure.do"> 
<html:hidden property="tela" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csAstbFuncresolvedorFureVector" indexId="sequencia"> 
  <tr> 
    <td width="5%" > 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18" height="18" class="principalLstParMao" title="<bean:message key="prompt.excluir"/>" onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />','<bean:write name="ccttrtVector" property="csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario" />')"> 
    </td>
    <td  class="principalLstParMao" width="38%" onclick="editaFuncResolvedor('<bean:write name="ccttrtVector" property="csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario" />','<bean:write name="ccttrtVector" property="fureInVer" />','<bean:write name="ccttrtVector" property="fureInResolver" />');">
     <bean:write name="ccttrtVector" property="csCdtbFuncResolvedorFuncVo.funcNmFuncionario" /> 
    </td>
    <td  class="principalLstParMao" width="35%" onclick="editaFuncResolvedor('<bean:write name="ccttrtVector" property="csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario" />','<bean:write name="ccttrtVector" property="fureInVer" />','<bean:write name="ccttrtVector" property="fureInResolver" />');">
  &nbsp;&nbsp;<logic:equal property="fureInVer" name="ccttrtVector" value="false">
				  N
			  </logic:equal>
			  <logic:notEqual property="fureInVer" name="ccttrtVector" value="false">
				  S
			  </logic:notEqual>
    </td>

    <td  class="principalLstParMao" width="10%" onclick="editaFuncResolvedor('<bean:write name="ccttrtVector" property="csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario" />','<bean:write name="ccttrtVector" property="fureInVer" />','<bean:write name="ccttrtVector" property="fureInResolver" />');">
  &nbsp;&nbsp;<logic:equal property="fureInResolver" name="ccttrtVector" value="false">
				  N
			  </logic:equal>
			  <logic:notEqual property="fureInResolver" name="ccttrtVector" value="false">
				  S
			  </logic:notEqual>
    </td>
    
    <td  class="principalLstPar" width="13%">&nbsp;</td>

    
  </tr>
  </logic:iterate>
</table>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIORESOLVEDOR_EXCLUSAO_CHAVE%>', editCsAstbFuncresolvedorFureForm.lixeira);
</script>

</html:form>
</body>
</html>