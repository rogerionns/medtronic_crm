<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head>
	<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css" />
	<script type="text/javascript" src="webFiles/funcoes/variaveis.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
	<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
	<script>
	
	function MontaLista(){
		lstArqCarga.location.href= "AdministracaoCsAstbSuperGrupoManifSugm.do?tela=administracaoLstCsAstbSuperGrupoManifSugm&acao=<%=Constantes.ACAO_VISUALIZAR%>&idSugrCdSupergrupo=" + document.administracaoCsAstbSuperGrupoManifSugmForm.idSugrCdSupergrupo.value;
	}
	
	function desabilitaCamposSuperGrupoManif(){
		document.administracaoCsAstbSuperGrupoManifSugmForm.idSugrCdSupergrupo.disabled = true;
		document.administracaoCsAstbSuperGrupoManifSugmForm.idMatpCdManiftipo.disabled = true;
		document.administracaoCsAstbSuperGrupoManifSugmForm.idGrmaCdGrupoManifestacao.disabled = true;
	}

	function carregaGrupomanifestacao() {
		var ajax = new ConsultaBanco("<%=MAConstantes.ENTITY_CS_ASTB_IDIOMAGRMA_IDGM %>", "ConsultaBanco.do");
		
		ajax.addField("statementName", "select-ativo-by-matp-supergurpo");
		
		ajax.addField("id_empr_cd_empresa", document.administracaoCsAstbSuperGrupoManifSugmForm.idEmpresa.value);
		ajax.addField("id_idio_cd_idioma", document.administracaoCsAstbSuperGrupoManifSugmForm.idIdioma.value);
		ajax.addField("id_matp_cd_maniftipo", document.administracaoCsAstbSuperGrupoManifSugmForm.idMatpCdManiftipo.value);
		ajax.addField("id_sugr_cd_supergrupo", document.administracaoCsAstbSuperGrupoManifSugmForm.idSugrCdSupergrupo.value);
		ajax.addField("id_grma_cd_grupomanifestacao", "0");

		ajax.executarConsulta(
			function(ajax) {
				ajax.popularCombo(document.getElementById("idGrmaCdGrupoManifestacao"), "id_grma_cd_grupomanifestacao", "grma_ds_grupomanifestacao", "", true, false);
			}, true, true);
	}

	
	</script>
</head>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista()">
<html:form styleId="administracaoCsAstbSuperGrupoManifSugmForm" action="/AdministracaoCsAstbSuperGrupoManifSugm.do">
	<html:hidden property="idEmpresa" />
	<html:hidden property="idIdioma" />
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.Supergrupo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idSugrCdSupergrupo" styleClass="principalObjForm" onchange="MontaLista(); carregaGrupomanifestacao();"> 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbSupergrupoSugrVector" property="idSugrCdSupergrupo" labelProperty="sugrDsSupergrupo"/> 
      </html:select> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr>
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.manifestacao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
	    <html:select property="idMatpCdManiftipo" styleClass="principalObjForm" onchange="carregaGrupomanifestacao();" > 
	    	<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
	    	<html:options collection="csCdtbManiftipoMatpVector" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo"/> 
	    </html:select> 
    </td>
    <td width="31%">&nbsp;</td>
  </tr>

  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.grupoManifestacao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
    	<html:select property="idGrmaCdGrupoManifestacao" styleId="idGrmaCdGrupoManifestacao" styleClass="principalObjForm">
			<html:option value=""> <bean:message key="prompt.selecione_uma_opcao" /> </html:option>
			<html:options collection="csCdtbGrupoManifestacaoGrmaVector" property="idGrmaCdGrupoManifestacao" labelProperty="grmaDsGrupoManifestacao" />
		</html:select>
	</td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td class="principalLabel" width="28%"> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="100%"> 
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="5%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="40%"><bean:message key="prompt.manifestacao"/> </td>
          <td class="principalLstCab" width="48%"><bean:message key="prompt.grupoManifestacao"/> </td>
        </tr>
        <tr valign="top"> 
          <td colspan="3" height="98%"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposSuperGrupoManif();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_SEGMENTOGRUPO_INCLUSAO_CHAVE%>', parent.document.administracaoCsAstbSuperGrupoManifSugmForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_SEGMENTOGRUPO_INCLUSAO_CHAVE%>')){
				desabilitaCamposSuperGrupoManif();
			}
			else{
				document.administracaoCsAstbSuperGrupoManifSugmForm.idSugrCdSupergrupo.disabled= false;
				document.administracaoCsAstbSuperGrupoManifSugmForm.idGrmaCdGrupoManifestacao.disabled= false;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_SEGMENTOGRUPO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_SEGMENTOGRUPO_ALTERACAO_CHAVE%>', parent.administracaoCsCdtbAreaAreaForm.imgGravar);	
				desabilitaCamposSuperGrupoManif();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsAstbSuperGrupoManifSugmForm.idSugrCdSupergrupo.focus();}
	catch(e){}
</script>