<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0" leftmargin="0">
<html:form styleId="editCsAstbComposicaoCompForm" action="/AdministracaoCsAstbComposicaoComp.do"> 
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="idAsnCdAssuntoNivel" />
<html:hidden property="idAsn1CdAssuntonivel1" />
<html:hidden property="idAsn2CdAssuntonivel2" />
	
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
  <logic:iterate id="ccttrtVector" name="csAstbComposicaoCompVector" indexId="sequencia"> 
  <tr> 
    <td width="3%" class="principalLstPar">
      <input type="checkbox" name="idCompCdInativos" value="<bean:write name="ccttrtVector" property="dhInativo" /> @<bean:write name="ccttrtVector" property="idCompCdSequencial" />">
    </td>
    <td width="3%" class="principalLstPar">
      <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18" height="18" class="principalLstParMao" title="<bean:message key='prompt.excluir'/>" onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="idCompCdSequencial" />')"> 
    </td>
    <td align="left" class="principalLstParMao" width="10%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idCompCdSequencial" />')"> 
      <bean:write name="ccttrtVector" property="idCompCdSequencial" /> </td>
    <td class="principalLstParMao" align="left" width="35%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idCompCdSequencial" />')"> 
      <script>acronym('<bean:write name="ccttrtVector" property="tpinDsTipoinformacao" />', 20);</script> </td>
	<td class="principalLstParMao" align="left" width="32%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idCompCdSequencial" />')"> 
      <script>acronym('<bean:write name="ccttrtVector" property="toinDsTopicoinformacao" />', 20);</script> </td>
	<td class="principalLstParMao" align="left" width="13%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idCompCdSequencial" />')"> 
      &nbsp;<bean:write name="ccttrtVector" property="dhInativo" /> </td>
  </tr>
  </logic:iterate>
</table>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_EXCLUSAO_CHAVE%>', editCsAstbComposicaoCompForm.lixeira);	
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_ALTERACAO_CHAVE%>', editCsAstbComposicaoCompForm.idCompCdInativos);
	window.top.document.getElementById("LayerAguarde").style.visibility = "hidden";
</script>

</html:form>
</body>
</html>