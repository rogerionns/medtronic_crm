<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html:html>
<head>
<title><bean:message key="prompt.AssociacaoDeFuncionarioXModulo"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>


<script type="text/javascript">
	function inicio() {
		if(document.forms[0].idModuCdModulo.value != '') {
			var modulos = document.forms[0].idModuCdModulo.value.split(';');

			for(var i = 0; i < modulos.length; i++) {
				for(var j = 0; j < document.forms[0].checkModulo.length; j++) {
					if(modulos[i] == document.forms[0].checkModulo[j].value) {
						document.forms[0].checkModulo[j].checked = true;
						break;
					}
				}
			}
		}
	}

	function salvar() {
		if(document.forms[0].checkModulo != null && document.forms[0].checkModulo != undefined) {
			var modulosChecados = '';
			
			if(document.forms[0].checkModulo.length > 1) {
				for(var i = 0; i < document.forms[0].checkModulo.length; i++) {
					if(document.forms[0].checkModulo[i].checked) {
						modulosChecados += document.forms[0].checkModulo[i].value + ';';
					}
				}
			} else {
				if(document.forms[0].checkModulo.checked) {
					modulosChecados = document.forms[0].checkModulo.value + ';';
				}
			}
		}
		
		window.dialogArguments.document.forms[0].idModuCdModulo.value = modulosChecados;
		window.close();
	}

	function marcarDesmarcar() {
		var check = document.forms[0].checkTodos.checked;
		for(var i = 0; i < document.forms[0].checkModulo.length; i++) {
			document.forms[0].checkModulo[i].checked = check;
		}
	}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();">
<html:form styleId="administracaoCsCdtbFuncionarioFuncForm" action="/AdministracaoCsCdtbFuncionarioFunc.do">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="idModuCdModulo"/>
		
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      	<td width="1007" colspan="2"> 
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          		<tr> 
            		<td class="principalPstQuadroGiant" height="17" width="166">&nbsp;<bean:message key="prompt.AssociacaoDeFuncionarioXModulo"/></td>
            		<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            		<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          		</tr>
        	</table>
      	</td>
    </tr>
    <tr> 
      	<td width="100%" align="center" class="principalBgrQuadro" valign="top" height="200"> 
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
		     	<tr>
                 	<td class="EspacoPqn">&nbsp;</td>
              	</tr>
		 		<tr>
		 			<td class="principalLstCab" width="10%">&nbsp;<input type="checkbox" name="checkTodos" class="geralCursoHand" onclick="marcarDesmarcar()" title="<bean:message key="prompt.MarcarTodosDesmarcarTodos" />" /></td>
					<td class="principalLstCab" width="90%">&nbsp;<bean:message key="prompt.modulo"/></td>
		  		</tr>
			</table>				  
			<table width="98%" border="0" cellspacing="0" cellpadding="0" height="200" class="principalBordaQuadro">
				<tr> 
					<td valign="top"> 
						<div id="lstModulos" style="position:absolute; width:715px; height:195px; z-index:4; overflow:auto">
							<table width=100% height="100%" border=0 cellspacing=0 cellpadding=0>
								<logic:present name="csDmtbModuloModuVector">
									<logic:iterate id="modulosVector" name="csDmtbModuloModuVector" indexId="indice">
										<tr>
											<td class="principalLstParMao" width="10%" height="15"><input type="checkbox" name="checkModulo" value="<bean:write name="modulosVector" property="field(id_modu_cd_modulo)" />" /></td>
											<td class="principalLstParMao" width="90%" height="15"><bean:write name="modulosVector" property="field(modu_ds_modulo)" /></td>
										</tr>
									</logic:iterate>
								</logic:present>
							</table>
						</div> 
					</td>					
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalLabel">&nbsp;</td>
				</tr>
			</table>
		</td>
      	<td width="4" height="250"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="250"></td>
	</tr>
	<tr>
		<td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
	<tr> 
		<td> 
			<div align="right"></div>
			<img src="webFiles/images/botoes/confirmaEdicao.gif" width="20" height="20" border="0" title="<bean:message key='prompt.confirm'/>" onClick="salvar()" class="geralCursoHand">
			<img src="webFiles/images/botoes/out.gif" width="20" height="20" border="0" title="<bean:message key='prompt.sair'/>" onClick="javascript:window.close()" class="geralCursoHand">
		</td>
    </tr>
</table>
</html:form>
</body>
</html:html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>