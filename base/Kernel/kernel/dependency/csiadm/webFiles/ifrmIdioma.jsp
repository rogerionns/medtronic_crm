<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, 
				 br.com.plusoft.csi.crm.helper.*,
				 br.com.plusoft.fw.app.Application, 
				 br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<html:html>
<head>

<style>

	ul{
		list-style: none;
		margin-top:5px;
		margin-left:0px;
		padding: 1px;
	}

	li{
		padding: 1px;
		margin-left:0px;
		border-bottom: 1px solid #f4d2ee;
		background-color: #efefef;
	}
	
	ul li a:hover{
		background-color: #efefff;
	}
	
	ul li a:active{
		background-color: #efefff;
	}

	a{
		display: block;
		color: black;
		text-decoration: none;
		text-align: center;
	}

</style>

<title>:::Plusoft 5 - CRM:::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>

<script>
	function inicio() {
		atualizarDetalhe();
	}

	var idioma1 = "";
	
	function abrirLink(obj,idIdioCdIdioma){
		var objs = document.getElementsByName("liMenu");
		for(i=0;i<objs.length;i++){
			objs[i].style.backgroundColor = "#efefef";
		}
		obj.style.backgroundColor = "#efefff";
		
		ifrmDetalheIdioma.document.getElementById("idiomaForm").tela.value = "ifrmDetalheIdioma";
		ifrmDetalheIdioma.document.getElementById("idiomaForm").acao.value = '<%=Constantes.ACAO_CONSULTAR%>';
		ifrmDetalheIdioma.document.getElementById("idiomaForm").idIdioCdIdiomaAnterior.value = ifrmDetalheIdioma.document.getElementById("idiomaForm").idIdioCdIdioma.value;
		ifrmDetalheIdioma.document.getElementById("idiomaForm").idIdioCdIdioma.value = idIdioCdIdioma;
		ifrmDetalheIdioma.document.getElementById("idiomaForm").arquivoXml.value = idiomaForm.arquivoXml.value;
		ifrmDetalheIdioma.document.getElementById("idiomaForm").chavePrimaria.value = idiomaForm.chavePrimaria.value;

		try{
			if(ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[0].type == "text"){
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo1.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[0].value;
			}else{
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo1.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[0].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[1].type == "text"){
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo2.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[1].value;
			}else{
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo2.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[1].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[2].type == "text"){
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo3.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[2].value;
			}else{
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo3.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[2].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[3].type == "text"){
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo4.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[3].value;
			}else{
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo4.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[3].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[4].type == "text"){
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo5.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[4].value;
			}else{
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo5.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[4].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[5].type == "text"){
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo6.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[5].value;
			}else{
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo6.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[5].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[6].type == "text"){
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo7.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[6].value;
			}else{
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo7.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[6].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[7].type == "text"){
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo8.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[7].value;
			}else{
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo8.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[7].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[8].type == "text"){
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo9.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[8].value;
			}else{
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo9.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[8].innerHTML;
			}
		}catch(e){
		}
		
		try{
			if(ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[9].type == "text"){
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo10.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[9].value;
			}else{
				ifrmDetalheIdioma.document.getElementById("idiomaForm").campo10.value = ifrmDetalheIdioma.document.getElementsByName("descricaoCampo")[9].innerHTML;
			}
		}catch(e){
		}
		
		ifrmDetalheIdioma.document.getElementById("idiomaForm").target = ifrmDetalheIdioma.name;
		ifrmDetalheIdioma.document.getElementById("idiomaForm").submit();
	}
	
	function atualizarDetalhe(){
		//nao existe nenhum selecionado provavelmente a tabela de idioma possui apenas um registro
		if (idioma1 == ""){
			alert("<bean:message key='prompt.sistemaEstaConfiguradoGerenciarApenasIdioma' />");
			window.close();
		}
		
		var objs = document.getElementsByName("liMenu");
		if(objs != undefined){
			if (objs.lenght > 0){
				objs[0].style.backgroundColor = "#efefef";
			}
		}
	
		ifrmDetalheIdioma.location = "Idioma.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=ifrmDetalheIdioma" + "&arquivoXml=" + idiomaForm.arquivoXml.value + "&chavePrimaria=" + idiomaForm.chavePrimaria.value + "&idIdioCdIdioma=" + idioma1 + "&hashIdiomaViewState=" + document.getElementById("hashIdiomaViewState").value + "&hashIdiomaNovoViewState=" + document.getElementById("hashIdiomaNovoViewState").value;
	}
	
	function sair(){
		ifrmDetalheIdioma.botaoSairClick();
	}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio()" >
<html:form styleId="idiomaForm" action="/Idioma.do">
	<html:hidden property="tela"/>
	<html:hidden property="acao"/>
	<html:hidden property="arquivoXml"/>
	<html:hidden property="chavePrimaria"/>
	
	<input type="hidden" id="hashIdiomaViewState" name="hashIdiomaViewState" value="<%=request.getAttribute("hashIdiomaViewState")%>"/>
	<input type="hidden" id="hashIdiomaNovoViewState" name="hashIdiomaNovoViewState" value="<%=request.getAttribute("hashIdiomaNovoViewState")%>"/>
	
<div id="camposTemp" style="position:absolute; width:0px; height:0px; visibility:hidden"></div>
	
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      	<td width="1007" colspan="2"> 
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          		<tr> 
            		<td class="principalPstQuadroGiant" height="17" width="166">&nbsp;Idiomas</td>
            		<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            		<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          		</tr>
        	</table>
      	</td>
    </tr>
    <tr> 
      	<td width="100%" align="left" class="principalBgrQuadro" valign="top" height="200"> 
      		<div style="float:left; margin-top:5px; margin-left:5px; width:200px;"> 
				<table class="principalBordaQuadro" width="100%" border="0" cellspacing="0" cellpadding="0">
			 		<tr>
			 			<td class="principalLstCab" width="100%" style="text-align:center">&nbsp;Idiomas Disponíveis</td>
			  		</tr>
			  		<tr height="200px"> 
						<td valign="top">
							<ul class="principalLabel">
								<logic:present name="csCdtbIdiomaIdioVector">
									<% Vector v = (Vector)request.getAttribute("csCdtbIdiomaIdioVector"); 
										for(int i =0; i<v.size();i++){
											Vo vo = (Vo)v.get(i);
										%>
										<li name="liMenu" id="liMenu" onclick="javascript:abrirLink(this,<%=vo.getField("id_idio_cd_idioma")%>)">
											<a href="#a"><%=vo.getField("idio_ds_idioma")%></a>
										</li>
										
										<%if(i==0){%>
											<script>
												idioma1 = <%=vo.getField("id_idio_cd_idioma")%>
											</script>
										<%}%>
									<% } %>
								</logic:present>
							</ul>
						</td>					
					</tr>
					<tr>
						<td class="principalLabel">&nbsp;</td>
					</tr>
				</table>
			</div>
			
			<div style="float:left; margin-top:5px; margin-left:5px; width:500px"> 
				<table class="principalBordaQuadro" width="100%" border="0" cellspacing="0" cellpadding="0">
			 		<tr>
			 			<td id="tdIdioma" class="principalLstCab" width="100%" style="text-align:center">&nbsp;</td>
			  		</tr>
			  		<tr height="200px"> 
						<td valign="top">
							<iframe id="ifrmDetalheIdioma" name="ifrmDetalheIdioma" src="" scrolling="no" style="margin-left:5px; margin-top:5px;" frameborder="0" marginwidth="0" marginheight="0" width="99%" height="99%"></iframe>
						</td>					
					</tr>
					<tr>
						<td class="principalLabel">&nbsp;</td>
					</tr>
				</table>
			</div>			
		</td>		
      	<td width="4" height="250"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="250"></td>
	</tr>
	<tr>
		<td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="4" align="right">
	<tr> 
		<td width="92%" align="left">
			&nbsp;
		</td>
		<td width="8%"> 
			<div align="right"></div>
			<img src="webFiles/images/botoes/confirmaEdicao.gif" width="20" height="20" border="0" title="<bean:message key='prompt.confirm'/>" onClick="ifrmDetalheIdioma.salvar()" class="geralCursoHand">
			<img src="webFiles/images/botoes/out.gif" width="20" height="20" border="0" title="<bean:message key='prompt.sair'/>" onClick="sair();" class="geralCursoHand">
		</td>
    </tr>
</table>

<iframe id=ifrmResult name="ifrmResultEmpresa" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="0" height="0"></iframe>

</html:form>
</body>
</html:html>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>