<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsCdtbGrupoManifestacaoGrmaForm" action="/AdministracaoCsCdtbGrupoManifestacaoGrma.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idGrmaCdGrupoManifestacao"/>
	<html:hidden property="idMatpCdManifTipo"/>

<script>var possuiRegistros=false;</script>

<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csCdtbGrupoManifestacaoGrmaVector" > 
  <script>possuiRegistros=true;</script>
  <tr>     
    <td class="principalLstParMao" width="6%">
       <bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />
    </td>
	<td class="principalLstParMao" align="left" width="40%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />','<bean:write name="ccttrtVector" property="csCdtbManifTipoMatpVo.idMatpCdManifTipo" />')">
       &nbsp;
       <%=acronymChar(((CsCdtbGrupoManifestacaoGrmaVo)ccttrtVector).getGrmaDsGrupoManifestacao().replaceAll("<","&lt;").replaceAll(">","&gt;"),35)%>
    </td>    
    <td class="principalLstParMao" align="left" width="35%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />','<bean:write name="ccttrtVector" property="csCdtbManifTipoMatpVo.idMatpCdManifTipo" />')">
      &nbsp;
      <%=acronymChar(((CsCdtbGrupoManifestacaoGrmaVo)ccttrtVector).getCsCdtbManifTipoMatpVo().getMatpDsManifTipo(),30)%>
    </td>
    <td class="principalLstParMao" align="left" width="21%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />','<bean:write name="ccttrtVector" property="csCdtbManifTipoMatpVo.idMatpCdManifTipo" />')">
      &nbsp;<bean:write name="ccttrtVector" property="grmaDhInativo" /> 
	</td>
    
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr> 
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>