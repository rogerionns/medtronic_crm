<%@ page language="java" import="br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
</head>

<script>
	function carregaVariedade(){
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			parent.document.administracaoCsAstbMateriaProdutoMaprForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
			parent.document.administracaoCsAstbMateriaProdutoMaprForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_ASSUNTONIVEL2_ASN2_MAPR%>';
			parent.document.administracaoCsAstbMateriaProdutoMaprForm.idPrasCdProdutoAssunto.value = administracaoCsAstbMateriaProdutoMaprForm.idPrasCdProdutoAssunto.value;
			parent.document.administracaoCsAstbMateriaProdutoMaprForm.target = parent.cmbVariedade.name;
			parent.document.administracaoCsAstbMateriaProdutoMaprForm.submit();
		<% } %>
	}
</script>


<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/AdministracaoCsAstbMateriaProdutoMapr.do" styleId="administracaoCsAstbMateriaProdutoMaprForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
  <html:select property="idPrasCdProdutoAssunto" styleClass="principalObjForm" onchange="carregaVariedade()">
    <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
    <html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto" />
  </html:select>
</html:form>
</body>
</html>