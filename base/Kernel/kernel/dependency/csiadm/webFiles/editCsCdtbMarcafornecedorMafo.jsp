<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">
	
function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
}

function MM_showHideLayers() {
	var obj, i, p, v, obj, args = MM_showHideLayers.arguments;
	for (i=0; i < (args.length-2); i+=3){
		obj = document.getElementById(args[i]);
		if(obj != null){
			v = args[i + 2];
			v = (v == 'show') ? 'block' : (v = 'hide') ? 'none' : v;
			obj.style.display = v;
		}
	}
}
 
function AtivarPasta(pasta)
{
	switch (pasta)
	{
	case 'DETALHE':
		MM_showHideLayers('DetalheMarca','','show','Email','','hide','Site','','hide','Telefone','','hide')
		SetClassFolder('tdDetalheMarca','principalPstQuadroLinkSelecionadoGrande');
		SetClassFolder('tdEmail','principalPstQuadroLinkNormalGrande');
		SetClassFolder('tdSite','principalPstQuadroLinkNormalGrande');
		SetClassFolder('tdTelefone','principalPstQuadroLinkNormalGrande');
		break;
	
	case 'EMAIL':
		MM_showHideLayers('DetalheMarca','','hide','Email','','show','Site','','hide','Telefone','','hide')
		SetClassFolder('tdDetalheMarca','principalPstQuadroLinkNormalGrande');
		SetClassFolder('tdEmail','principalPstQuadroLinkSelecionadoGrande');
		SetClassFolder('tdSite','principalPstQuadroLinkNormalGrande');
		SetClassFolder('tdTelefone','principalPstQuadroLinkNormalGrande');
		break;
	
	case 'SITE':
		MM_showHideLayers('DetalheMarca','','hide','Email','','hide','Site','','show','Telefone','','hide')
		SetClassFolder('tdDetalheMarca','principalPstQuadroLinkNormalGrande');
		SetClassFolder('tdEmail','principalPstQuadroLinkNormalGrande');
		SetClassFolder('tdSite','principalPstQuadroLinkSelecionadoGrande');
		SetClassFolder('tdTelefone','principalPstQuadroLinkNormalGrande');
	
		break;
	
	case 'TELEFONE':
		MM_showHideLayers('DetalheMarca','','hide','Email','','hide','Site','','hide','Telefone','','show')
		SetClassFolder('tdDetalheMarca','principalPstQuadroLinkNormalGrande');
		SetClassFolder('tdEmail','principalPstQuadroLinkNormalGrande');
		SetClassFolder('tdSite','principalPstQuadroLinkNormalGrande');
		SetClassFolder('tdTelefone','principalPstQuadroLinkSelecionadoGrande');
		break;	
	}
 	eval(stracao);
}

function carregarListaEmails()
{
	codigo = parent.admIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value;
	if(codigo > 0)
	{
		ifrmEmail.document.getElementById("ifrmLstEmail").src = "AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_LST_EMAIL%>&acao=<%= Constantes.ACAO_CONSULTAR%>&idMafoCdMarcaFornecedor=" + codigo;
	}
}

function carregarListaSites()
{
	codigo = parent.admIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value;
	
	if(codigo > 0)
	{
		ifrmSite.document.getElementById("ifrmLstSite").src = "AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_LST_SITE%>&acao=<%= Constantes.ACAO_CONSULTAR%>&idMafoCdMarcaFornecedor=" + codigo;
	}
}

function carregarListaTelefones()
{
	codigo = parent.admIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value;
	
	if(codigo > 0)
	{
		ifrmTelefone.document.getElementById("ifrmLstTelefone").src = "AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_LST_TELEFONE%>&acao=<%= Constantes.ACAO_CONSULTAR%>&idMafoCdMarcaFornecedor=" + codigo;
	}
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value);
}

</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();setTimeout('carregarListaEmails()',500);setTimeout('carregarListaSites()',500); setTimeout('carregarListaTelefones()',500);">

<html:form styleId="administracaoCsCdtbMarcafornecedorMafoForm" action="/AdministracaoCsCdtbMarcafornecedorMafo.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	
	<html:hidden property="dataAtendimento"/>	

	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td class="principalPstQuadroLinkSelecionadoGrande" id="tdDetalheMarca" name="tdDetalheMarca" onClick="AtivarPasta('DETALHE')">
				  <bean:message key="prompt.detalheMarca"/> 
		  </td>
		  <td class="principalPstQuadroLinkNormalGrande" id="tdEmail" name="tdEmail" onClick="AtivarPasta('EMAIL')">
				<bean:message key="prompt.eMail"/>   
		  </td>
		  <td class="principalPstQuadroLinkNormalGrande" id="tdSite" name="tdSite" onClick="AtivarPasta('SITE')">
				<bean:message key="prompt.sites"/> 
		  </td>
		  <td class="principalPstQuadroLinkNormalGrande" id="tdTelefone" name="tdTelefone" onClick="AtivarPasta('TELEFONE')">
				<bean:message key="prompt.telefones"/>
		  </td>		 
		  <td class="principalLabel">&nbsp;</td>
		</tr>
   </table>
   
   <table width="99%" border="0" cellspacing="0" cellpadding="0" height="330" align="center" class="principalBordaQuadro">
    <tr> 
      <td valign="top">        
        <div id="DetalheMarca" style="display: block;">
			<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr> 
				<td width="24%">&nbsp;</td>
				<td colspan="3">&nbsp;</td>
			  </tr>
			  <tr> 
				<td width="24%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> 
				  <!-- ## -->
				  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td width="10%"> <html:text property="idMafoCdMarcaFornecedor" styleClass="text" disabled="true" /> 
				</td>
				<td width="28%">&nbsp;</td>
				<td width="20%">&nbsp;</td>
			  </tr>
			  <tr> 
			  <tr> 
				<td width="24%" align="right" class="principalLabel"><bean:message key="prompt.codigoCorporativo"/> 
				  <!-- ## -->
				  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td width="10%"> <html:text property="mafoDsCorporativo" styleClass="text" maxlength="10"/> 
				</td>
				<td width="28%">&nbsp;</td>
				<td width="20%">&nbsp;</td>
			  </tr>
			  <tr> 
				<td width="24%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/>
				  <!-- ## -->
				  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td colspan="2"> <html:text style="height:20px;width:390px" property="mafoDsMarcaFornecedor" styleClass="principalObjForm"  maxlength="80" /> 
				</td>
				<td width="20%">&nbsp;</td>
			  </tr>
			  <tr> 
				<td width="24%" align="right" class="principalLabel"><bean:message key="prompt.horaAtendimento"/>
				  <!-- ## -->
				  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td colspan="2">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td width="100%"><html:text property="horaAtendimento" styleClass="text" maxlength="60" style="width:390px"/></td>
						</tr>
					</table>
				</td>
				<td width="20%">&nbsp;</td>
			  </tr>
			  <tr> 
				<td width="24%" align="right" class="principalLabel"><bean:message key="prompt.fornecedor"/>
				  <!-- ## -->
				  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td colspan="2" height="20"> 
					<html:select property="csCdtbFornProdutoFoprVo.idFoprCdFornproduto" styleClass="principalObjForm" style="width:345px;">
						<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
						   <logic:present name="csCdtbFornprodutoFoprVector">
								<html:options collection="csCdtbFornprodutoFoprVector" property="idFoprCdFornproduto" labelProperty="foprDsFornproduto"/>
						  </logic:present>
				  </html:select>
				</td>
				<td width="20%">&nbsp;</td>
			  </tr>
			  <tr> 
				<td width="24%" align="right" class="principalLabel"><bean:message key="prompt.observacao"/>
				  <!-- ## -->
				  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td colspan="2" height="20"> 
					<html:textarea property="mafoDsObservacao" styleClass="text" rows="3" onkeyup="textCounter(this, 1000)" onblur="textCounter(this, 1000)" />
				</td>
				<td width="20%">&nbsp;</td>
			  </tr>		  
			  <tr> 
				<td width="24%">&nbsp;</td>
				<td colspan="3">&nbsp;</td>
			  </tr>
			  <tr> 
				<td width="24%">&nbsp;</td>
				<td width="10%"> 
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">        
					<tr> 
					  <td align="right" width="19%"><html:checkbox value="true" property="mafoDhInativo"/> 
					  </td>
					  <td class="principalLabel" width="81%"> 
						<div align="left">&nbsp;<bean:message key="prompt.inativo"/> </div>
					  </td>
					</tr>
				  </table>
				</td>
				<td class="principalLabel" width="28%">&nbsp; </td>
				<td width="20%">&nbsp;</td>
			  </tr>
			</table>
		</div>
		<div id="Email" style="display: none;">
			<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr> 
				<td height="305">
              	<iframe id=ifrmEmail  name="ifrmEmail" src="AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_EMAIL%>&acao=<%= Constantes.ACAO_CONSULTAR%>" width="100%" height="100%" scrolling="No" frameborder="0" marginwidtd="0" marginheight="0" ></iframe>
              	</td>
			  </tr>
			</table>
		</div>
		<div id="Site" style="display: none;">
			<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr> 
				<td height="305">
              	<iframe id=ifrmSite  name="ifrmSite" src="AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_SITE%>&acao=<%= Constantes.ACAO_CONSULTAR%>" width="100%" height="100%" scrolling="No" frameborder="0" marginwidtd="0" marginheight="0" ></iframe>
              	</td>
			  </tr>
			</table>
		</div>
		<div id="Telefone" style="display: none;">
			<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr> 
				<td height="305">
              	<iframe id=ifrmTelefone  name="ifrmTelefone" src="AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_TELEFONE%>&acao=<%= Constantes.ACAO_CONSULTAR%>" width="100%" height="100%" scrolling="No" frameborder="0" marginwidtd="0" marginheight="0" ></iframe>
              	</td>
			  </tr>
			</table>
		</div>
	  </td>
    </tr>
   </table>
</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			administracaoCsCdtbMarcafornecedorMafoForm.mafoDsCorporativo.disabled= true;
			administracaoCsCdtbMarcafornecedorMafoForm.mafoDsMarcaFornecedor.disabled= true;	
			administracaoCsCdtbMarcafornecedorMafoForm.mafoDhInativo.disabled= true;
			administracaoCsCdtbMarcafornecedorMafoForm['csCdtbFornProdutoFoprVo.idFoprCdFornproduto'].disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			/*setPermissaoImageEnable('<!%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_INCLUSAO_CHAVE%>', parent.administracaoCsCdtbMarcafornecedorMafoForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<!%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_INCLUSAO_CHAVE%>')){
				administracaoCsCdtbMarcafornecedorMafoForm.mafoDsMarcaFornecedor.disabled= true;	
				administracaoCsCdtbMarcafornecedorMafoForm['csCdtbFornProdutoFoprVo.idFoprCdFornproduto'].disabled= true;
				administracaoCsCdtbMarcafornecedorMafoForm.mafoDhInativo.disabled= true;
			}			
			administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.disabled= false;
			administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value= '';
			administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.disabled= true;
			*/
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			/*
			if (!getPermissao('<!%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<!%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_ALTERACAO_CHAVE%>', parent.administracaoCsCdtbMarcafornecedorMafoForm.imgGravar);	
	
				administracaoCsCdtbMarcafornecedorMafoForm.mafoDsMarcaFornecedor.disabled= true;
				administracaoCsCdtbMarcafornecedorMafoForm['csCdtbFornProdutoFoprVo.idFoprCdFornproduto'].disabled= true;
				administracaoCsCdtbMarcafornecedorMafoForm.depaDhInativo.disabled= true;
				administracaoCsCdtbMarcafornecedorMafoForm.csCdtbFornProdutoFoprVo.idFoprCdFornproduto.disabled= true;
			} */
		</script>
</logic:equal>

</html>