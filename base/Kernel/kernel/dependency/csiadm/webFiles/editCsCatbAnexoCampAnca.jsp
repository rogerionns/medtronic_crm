<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

	function MontaLista(){
		lstArqCarga.location.href= "AdministracaoCsCatbAnexoCampAnca.do?tela=administracaoLstCsCatbAnexoCampAnca&acao=<%=Constantes.ACAO_VISUALIZAR%>&idCampCdCampanha=" + document.administracaoCsCatbAnexoCampAncaForm.idCampCdCampanha.value;
	}

	function desabilitaCamposAnexoCamp(){
		document.administracaoCsCatbAnexoCampAncaForm.idCampCdCampanha.disabled= true;
		document.administracaoCsCatbAnexoCampAncaForm.idAnmaCdAnexoMail.disabled= true;
	}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista()">
<html:form styleId="administracaoCsCatbAnexoCampAncaForm" action="/AdministracaoCsCatbAnexoCampAnca.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.campanha"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
      <html:select property="idCampCdCampanha" styleClass="principalObjForm" onchange="MontaLista()" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbCampanhaCampVector" property="idCampCdCampanha" labelProperty="campDsCampanha"/> 
      </html:select> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.anexoEMail"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idAnmaCdAnexoMail" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbAnexoMailAnmaVector" property="idAnmaCdAnexoMail" labelProperty="anmaDsAnexoMail"/> 
      </html:select> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="250">
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="4%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="38%"> &nbsp;<bean:message key="prompt.campanha"/> </td>
          <td class="principalLstCab" width="33%"><bean:message key="prompt.anexoEMail"/></td>
          <td class="principalLstCab" width="11%"><bean:message key="prompt.dtInicial"/></td>
          <td class="principalLstCab" width="14%"><bean:message key="prompt.dtFinal"/></td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
			document.administracaoCsCatbAnexoCampAncaForm.idCampCdCampanha.disabled= false;
			document.administracaoCsCatbAnexoCampAncaForm.idAnmaCdAnexoMail.disabled= false;
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_ANEXOCAMPANHA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCatbAnexoCampAncaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_ANEXOCAMPANHA_INCLUSAO_CHAVE%>')){
				desabilitaCamposAnexoCamp();
			}else{
				document.administracaoCsCatbAnexoCampAncaForm.idCampCdCampanha.disabled= false;
				document.administracaoCsCatbAnexoCampAncaForm.idAnmaCdAnexoMail.disabled= false;
			}
	
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_ANEXOCAMPANHA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_ANEXOCAMPANHA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCatbAnexoCampAncaForm.imgGravar);	
				desabilitaCamposAnexoCamp();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCatbAnexoCampAncaForm.idCampCdCampanha.focus();}
	catch(e){}
</script>