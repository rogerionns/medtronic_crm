<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); parent.inicio();">
<html:form styleId="editCsAstbSuperGrupoManifSugmForm" action="/AdministracaoCsAstbSuperGrupoManifSugm.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idSugrCdSupergrupo"/>
	<html:hidden property="idMatpCdManiftipo"/>
	<html:hidden property="idGrmaCdGrupoManifestacao"/>
	


<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<thead>
				<tr>
					<th class="principalLstCab">&nbsp;</th>
					<th align="left" class="principalLstCab">&nbsp;</th>
					<th align="left" class="principalLstCab" width="49%"><bean:message key="prompt.AssuntoSuperGrupo" /></th>
					<th class="principalLstCab" align="left" width="43%"><bean:message key="prompt.inativo" /></th>
				</tr>
			</thead>
			<tbody>
			<logic:iterate id="cagaaVector" name="csAstbSuperGrupoManifSugmVector">
  <script>
			possuiRegistros = true;
		</script>
  <tr > 
    <td width="5%" class="principalLstPar"> 
       <!--img src="webFiles/images/botoes/lixeira18x18.gif" width="18"	height="18" class="geralCursoHand" title="<bean:message key='prompt.excluir'/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="cagaaVector" property="csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" />','<bean:write name="cagaaVector" property="csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />')"--> 
      <bean:write name="cagaaVector" property="csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" /> 
    </td>
    <td class="principalLstParMao" width="3%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" />','<bean:write name="cagaaVector" property="csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />')">&nbsp;</td>
    <td class="principalLstParMao" align="left" width="51%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" />','<bean:write name="cagaaVector" property="csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />')"> 
      <bean:write name="cagaaVector" property="csCdtbSupergrupoSugrVo.sugrDsSupergrupo" /> 
    </td>
	<td class="principalLstParMao" align="left" width="41%" onclick="parent.clearError();parent.submeteFormEdit(<bean:write name="cagaaVector" property="csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" />,<bean:write name="cagaaVector" property="csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />,<bean:write name="cagaaVector" property="csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />)"><bean:write name="cagaaVector" property="csCdtbSupergrupoSugrVo.sugrDhInativo" />
	<logic:empty name="cagaaVector" property="csCdtbSupergrupoSugrVo.sugrDhInativo">&nbsp;</logic:empty> 
    </td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td colspan="4" height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
  </tbody>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>