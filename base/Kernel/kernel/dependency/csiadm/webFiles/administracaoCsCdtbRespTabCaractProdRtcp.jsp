<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsCdtbRespTabCaractProdRtcpForm" action="/AdministracaoCsCdtbRespTabCaractProdRtcp.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="erro" />
	<html:hidden property="topicoId"/>
	<html:hidden property="idRtcpRespTabCaractProd"/>
	<html:hidden property="csCdtbTabelaCaractProdTacpVo.idTacpCdTabelaCaractProd"/>

<script>var possuiRegistros=false;</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="CsCdtbRespTabCaractProdRtcpVector">
  <script>possuiRegistros=true;</script>
  <tr> 
    <td width="3%" class="principalLstPar">
      &nbsp;
    </td>
    <td class="principalLstParMao" width="125px" align="left" onclick="parent.clearError();parent.submeteFormEdit('0','<bean:write name="ccttrtVector" property="idTacpCdTabelaCaractProd" />')">
	&nbsp; <bean:write name="ccttrtVector" property="idTacpCdTabelaCaractProd" />
    </td>
    <td class="principalLstParMao" align="left" width="58%" onclick="parent.clearError();parent.submeteFormEdit('0','<bean:write name="ccttrtVector" property="idTacpCdTabelaCaractProd" />')">
	&nbsp; <bean:write name="ccttrtVector" property="tacpDsTabelaCaractProd" />
	</td>
	<td class="principalLstParMao" align="left" width="27%" onclick="parent.clearError();parent.submeteFormEdit('0','<bean:write name="ccttrtVector" property="idTacpCdTabelaCaractProd" />')">
	&nbsp; <bean:write name="ccttrtVector" property="tacpDhInativo" />
	</td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="left: 193; top: 33; display: none">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>