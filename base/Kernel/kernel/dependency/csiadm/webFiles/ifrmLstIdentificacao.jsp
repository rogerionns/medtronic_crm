<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
//-->

function abre(id, nome){
	parent.abreMr(id, nome);
}

var result = 0;

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';">
<html:form action="/ResultListIdentifica.do" styleId="listForm">
	<html:hidden property="acao" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalLstCab" id="cab01" name="cab01" width="27%">&nbsp;<bean:message key="prompt.nome" /></td>
    <td class="principalLstCab" id="cab02" name="cab02" width="12%"><bean:message key="prompt.telefone" /></td>
    <td class="principalLstCab" id="cab03" name="cab03" width="26%"><bean:message key="prompt.endereco" /></td>
    <td class="principalLstCab" id="cab04" name="cab04" width="12%"><bean:message key="prompt.bairro" /></td>
    <td class="principalLstCab" id="cab05" name="cab05" width="14%"><bean:message key="prompt.cidade" /></td>
    <td class="principalLstCab" id="cab06" name="cab06" width="7%"><bean:message key="prompt.cep" /></td>
    <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
  </tr>
  <tr valign="top"> 
    <td height="200"colspan="7"> 
      <div id="lstIdentificados" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <table class=geralCursoHand width="100%" border="0" cellspacing="0" cellpadding="0">
        <logic:present name="resultado">
		<logic:iterate name="resultado" id="result" indexId="numero">
		  <script>
			result++;
		  </script>
          <tr class="intercalaLst<%=numero.intValue()%2%>" onclick="javascript:abre('<bean:write name="result" property="idPessCdPessoa"/>', '<bean:write name="result" property="pessNmPessoa"/>')"> 
            <td class="principalLstPar" width="290">
            	<ACRONYM style="border: 0" title="<bean:write name="result" property="pessNmPessoa"/>">
	            	<bean:write name="result" property="nomeIdentAbrev"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstPar" width="118">
	            <ACRONYM style="border: 0" title="<bean:write name="result" property="telefoneIdent"/>">
            		<bean:write name="result" property="telefoneIdent"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstPar" width="272">
            	<ACRONYM style="border: 0" title="<bean:write name="result" property="enderecoIdent"/>">
	            	<bean:write name="result" property="enderecoIdentAbrev"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstPar" width="137">
	            <ACRONYM style="border: 0" title="<bean:write name="result" property="bairroIdent"/>">
	            	<bean:write name="result" property="bairroIdentAbrev"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstPar" width="93">
	            <ACRONYM style="border: 0" title="<bean:write name="result" property="cidadeIdent"/>">
            		<bean:write name="result" property="cidadeIdentAbrev"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstPar" width="93">
	            <ACRONYM style="border: 0" title="<bean:write name="result" property="CEPIdent"/>">
           			<bean:write name="result" property="CEPIdent"/>&nbsp;
           		</ACRONYM>
            </td>
          </tr>
		</logic:iterate>
		</logic:present>
		<script>
		  if (parent.msg == true && result == 0)
		    document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="200" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
		</script>
        </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>