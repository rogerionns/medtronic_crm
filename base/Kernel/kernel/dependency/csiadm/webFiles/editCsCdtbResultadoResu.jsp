<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>


<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposResultado(){
		document.administracaoCsCdtbResultadoResuForm.resuDsResultado.disabled= true;	
		document.administracaoCsCdtbResultadoResuForm.resuDhInativo.disabled= true;
		document.administracaoCsCdtbResultadoResuForm.resuInProduto.disabled= true;
		document.administracaoCsCdtbResultadoResuForm.idTprsCdTpResultado.disabled= true;
		document.administracaoCsCdtbResultadoResuForm.resuDsCodtelefonia.disabled= true;
		document.administracaoCsCdtbResultadoResuForm.resuInComissao.disabled= true;
	}
	
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbResultadoResuForm.idResuCdResultado.value);
	}
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbResultadoResuForm" action="/AdministracaoCsCdtbResultadoResu.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>
	<input type="hidden" name="limparSessao" value="false"></input>		

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idResuCdResultado" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.tpresultado"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
    		<html:select property="idTprsCdTpResultado" styleClass="principalObjForm" > 
		    <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csDmtbTpResultadoTprsVector" property="idTprsCdTpResultado" labelProperty="tprsDsTpResultado"/>
	        </html:select>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="resuDsResultado" styleClass="text" maxlength="40" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codTelefonia"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="resuDsCodtelefonia" styleClass="text" maxlength="20" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.camapanha.resultado.informaProduto"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:checkbox value="true" property="resuInProduto"/> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="13%" align="right" class="principalLabel"><plusoft:message key="prompt.comissao.resultado"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:checkbox value="true" property="resuInComissao"/> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>

        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="resuDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposResultado();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_RESULTADO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbResultadoResuForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_RESULTADO_INCLUSAO_CHAVE%>')){
				desabilitaCamposResultado();
			}else{
				document.administracaoCsCdtbResultadoResuForm.idResuCdResultado.disabled= false;
				document.administracaoCsCdtbResultadoResuForm.idResuCdResultado.value= '';
				document.administracaoCsCdtbResultadoResuForm.idResuCdResultado.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_RESULTADO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_RESULTADO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbResultadoResuForm.imgGravar);	
				desabilitaCamposResultado();	
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbResultadoResuForm.idTprsCdTpResultado.focus();}
	catch(e){}
</script>