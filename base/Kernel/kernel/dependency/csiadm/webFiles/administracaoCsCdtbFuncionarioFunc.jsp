<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	

long idEmpresa = empresaVo.getIdEmprCdEmpresa();

String maxRegistros = "";
try{
	maxRegistros = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_LIMITE_LINHAS_RESULTADO,idEmpresa);
}catch(Exception e){}
	
if(maxRegistros == null || maxRegistros.equals("")){
	maxRegistros = "100";
}

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("CsCdtbFuncionarioFuncVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("CsCdtbFuncionarioFuncVector"));
	if (v.size() > 0){
		numRegTotal = ((CsCdtbFuncionarioFuncVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************

%>

<%@page import="java.util.Vector"%>
<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

	function setaDeAte(){

		if(parent.document.editCsCdtbFuncionarioFuncForm.regDe.value == "0" && parent.document.editCsCdtbFuncionarioFuncForm.regAte.value == "0"){			
			
			parent.document.editCsCdtbFuncionarioFuncForm.regDe.value='<%=regDe%>';
			parent.document.editCsCdtbFuncionarioFuncForm.regAte.value='<%=regAte%>';
			parent.initPaginacao();
		}	
	
	}

	function inicio(){
			
		parent.setPaginacao(<%=regDe%>,<%=regAte%>);
		parent.atualizaPaginacao(<%=numRegTotal%>);
			
	}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form styleId="editCsCdtbFuncionarioFuncForm" action="/AdministracaoCsCdtbFuncionarioFunc.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idFuncCdFuncionario"/>

<script>
	var possuiRegistros=false;
	var totalRegistros=0;
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="CsCdtbFuncionarioFuncVector" indexId="sequencia">
  <script>
  	possuiRegistros=true;
  	totalRegistros=<%=sequencia%>;
  </script>
  <tr> 
    <td width="3%" class="principalLstPar"> <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="idFuncCdFuncionario" />')"></td>
    <td class="principalLstParMao" width="8%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFuncCdFuncionario" />')"> 
	  <bean:write name="ccttrtVector" property="idFuncCdFuncionario" /> 
    </td>
    <td class="principalLstParMao" align="left" width="20%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFuncCdFuncionario" />')">
     	<script>acronym('<bean:write name="ccttrtVector" property="funcNmFuncionario" />', 14);</script>  
	</td>
    <td class="principalLstParMao" align="left" width="27%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFuncCdFuncionario" />')">
		&nbsp;
		<% //Nao deve o login do funcionario quando for area resolvedora
		if (((br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo)ccttrtVector).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() != br.com.plusoft.csi.adm.helper.MAConstantes.ID_NIVE_CD_NIVELACESSO_AREARESOLVEDORA) {%>
			<%= acronymChar(((br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo)ccttrtVector).getFuncDsLoginname(),20) %>
		<%} %>		
	</td>
    <td class="principalLstParMao" align="left" width="29%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFuncCdFuncionario" />')">
		<bean:write name="ccttrtVector" property="csCdtbAreaAreaVo.areaDsArea"/>&nbsp;
	</td>
    <td class="principalLstParMao" align="center" width="13%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFuncCdFuncionario" />')"> 
      <div align="left">
      &nbsp; <bean:write name="ccttrtVector" property="funcDhInativo"/></div>
      </td>
  </tr>
  </logic:iterate> 
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIO_EXCLUSAO_CHAVE%>', editCsCdtbFuncionarioFuncForm.lixeira);	
	
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	
	if(divErro.innerHTML == null ){
		
	}else{
		//if(parent.error != undefined && parent.error != null)
			//parent.printError(divErro.innerHTML);
	}
	
<logic:messagesPresent message="false">
    <html:messages id="error" message="false">
        <logic:present name="error">
            alert('<bean:write name="error" filter="false" />');
        </logic:present>
    </html:messages>
</logic:messagesPresent>
</script>
</html:form>
</body>
</html>