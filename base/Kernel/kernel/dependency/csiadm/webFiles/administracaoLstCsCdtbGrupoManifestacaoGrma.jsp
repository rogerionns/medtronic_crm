<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsCdtbGrupoManifestacaoGrmaForm" action="/AdministracaoCsCdtbGrupoManifestacaoGrma.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csCdtbGrupoManifestacaoGrmaVector" indexId="sequencia"> 
  <tr> 
    <td width="3%" class="principalLstParMao"> 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18" height="18" class="principalLstParMao" title="<bean:message key="prompt.excluir"/>" onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />')" > 
    </td>
    <td width="3%" class="principalLstParMao"> 
    	<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMAGRMA_IDGM.xml','<bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />')">
    </td>
    <td width="6%" class="principalLstParMao" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />','<bean:write name="ccttrtVector" property="csCdtbManifTipoMatpVo.idMatpCdManifTipo" />')"  >
       &nbsp;<bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" /> 
    </td>
    <td  width="66%" class="principalLstParMao" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />','<bean:write name="ccttrtVector" property="csCdtbManifTipoMatpVo.idMatpCdManifTipo" />')"  >
       
       <%=acronymChar(((CsCdtbGrupoManifestacaoGrmaVo)ccttrtVector).getGrmaDsGrupoManifestacao().replaceAll("<","&lt;").replaceAll(">","&gt;"),40)%>
    </td>
    <td  width="23%" class="principalLstParMao" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />','<bean:write name="ccttrtVector" property="csCdtbManifTipoMatpVo.idMatpCdManifTipo" />')"  >
       &nbsp;<bean:write name="ccttrtVector" property="grmaDhInativo" /> 
    </td>
  </tr>
  </logic:iterate>
</table>

<script>

setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_GRUPOMANIFESTACAO_EXCLUSAO_CHAVE%>', editCsCdtbGrupoManifestacaoGrmaForm.lixeira);
	
</script>



</html:form>
</body>
</html>