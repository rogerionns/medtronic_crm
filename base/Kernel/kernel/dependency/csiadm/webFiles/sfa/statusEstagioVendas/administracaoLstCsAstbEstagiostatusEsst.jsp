<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsAstbEstagiostatusEsstForm" action="/AdministracaoCsAstbEstagiostatusEsst.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="CsAstbEstagiostatusEsstVector" indexId="sequencia"> 
  <tr> 
    <td width="5%" class="principalLstPar"> 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" width="15" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" 
    		onclick="parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="idEsveCdEstagiovenda" />', '<bean:write name="ccttrtVector" property="idStopCdStatusopor" />')"> 
    	&nbsp;<bean:write name="ccttrtVector" property="stopDsStatusopor" /> 
    </td>
  </tr>
  </logic:iterate>
</table>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISAQUESTAO_EXCLUSAO_CHAVE%>', editCsAstbEstagiostatusEsstForm.lixeira);	
</script>

</html:form>
</body>
</html>