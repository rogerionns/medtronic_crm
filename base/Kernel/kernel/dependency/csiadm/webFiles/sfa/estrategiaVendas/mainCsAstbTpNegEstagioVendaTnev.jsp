<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	administracaoCsAstbTpnegestagiovendaTnevForm.target = admIframe.name;
	administracaoCsAstbTpnegestagiovendaTnevForm.acao.value ='filtrar';
	
	administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.value = ifrmCmbEstagio.document.forms[0].idEsveCdEstagiovenda.value;
	
	administracaoCsAstbTpnegestagiovendaTnevForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value = '';
	administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.value = '';
	carregaEstagio();
	
	
}

function submeteFormIncluir() {
	
	editIframe.administracaoCsAstbTpnegestagiovendaTnevForm.target = editIframe.name;
	editIframe.administracaoCsAstbTpnegestagiovendaTnevForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_TPNEGESTAGIOVENDA_TNEV%>';
	editIframe.administracaoCsAstbTpnegestagiovendaTnevForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.administracaoCsAstbTpnegestagiovendaTnevForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(idTpNeg,idEstVenda){
	tab.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value = idTpNeg;
	//tab.administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.value = idEstVenda;
	
	tab.administracaoCsAstbTpnegestagiovendaTnevForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_TPNEGESTAGIOVENDA_TNEV%>';
	tab.administracaoCsAstbTpnegestagiovendaTnevForm.target = editIframe.name;
	tab.administracaoCsAstbTpnegestagiovendaTnevForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.administracaoCsAstbTpnegestagiovendaTnevForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){

	if(tab.administracaoCsAstbTpnegestagiovendaTnevForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_TPNEGESTAGIOVENDA_TNEV%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.administracaoCsAstbTpnegestagiovendaTnevForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_ASTB_TPNEGESTAGIOVENDA_TNEV%>'){
		
		//calcula porcentagem
		if(!tab.ifrmLstTpnegEstagioVenda.calculaPorcentagem()){
			return false;
		}
		
		tab.administracaoCsAstbTpnegestagiovendaTnevForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_TPNEGESTAGIOVENDA_TNEV%>';
		tab.administracaoCsAstbTpnegestagiovendaTnevForm.target = admIframe.name;
		tab.administracaoCsAstbTpnegestagiovendaTnevForm.submit();
		cancel();
	}
}

function submeteExcluir(codigo) {
	
	tab.administracaoCsAstbTpnegestagiovendaTnevForm.idAreaCdArea.value = codigo;
	tab.administracaoCsAstbTpnegestagiovendaTnevForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_TPNEGESTAGIOVENDA_TNEV%>';
	tab.administracaoCsAstbTpnegestagiovendaTnevForm.target = editIframe.name;
	tab.administracaoCsAstbTpnegestagiovendaTnevForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.administracaoCsAstbTpnegestagiovendaTnevForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.administracaoCsAstbTpnegestagiovendaTnevForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_TPNEGESTAGIOVENDA_TNEV%>';
		editIframe.administracaoCsAstbTpnegestagiovendaTnevForm.target = admIframe.name;
		editIframe.administracaoCsAstbTpnegestagiovendaTnevForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.administracaoCsAstbTpnegestagiovendaTnevForm.idAreaCdArea, false);
		editIframe.administracaoCsAstbTpnegestagiovendaTnevForm.submit();
		disableEnable(editIframe.administracaoCsAstbTpnegestagiovendaTnevForm.idAreaCdArea, true);
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbAreaArea.do?tela=editCsCdtbAreaArea&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsAstbTpnegestagiovendaTnev.do?tela=editCsAstbTpNegEstagioVendaTnev&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsAstbTpnegestagiovendaTnevForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.administracaoCsAstbTpnegestagiovendaTnevForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_TPNEGESTAGIOVENDA_TNEV%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			break;
		case editIframe:
			tab.administracaoCsAstbTpnegestagiovendaTnevForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_TPNEGESTAGIOVENDA_TNEV%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function carregaEstagio(){
	var url = "";
	url = "AdministracaoCsAstbTpnegestagiovendaTnev.do?tela=<%=MAConstantes.TELA_CMB_ESTAGIOVENDA%>";
	url = url +	"&acao=<%=Constantes.ACAO_FITRAR%>";
	url = url +	"&idTpneCdTiponegocio=" + administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value;
	
	ifrmCmbEstagio.location.href = url; 
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsAstbTpnegestagiovendaTnevForm" action="/AdministracaoCsAstbTpnegestagiovendaTnev.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<html:hidden property="idEsveCdEstagiovenda" />

	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="100%">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">
						&nbsp;
					</td>
					<td height="100%" width="4">
						<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top">
				<br>
				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="90%">
				<tr>
					<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="principalPstQuadroLinkSelecionadoGrande" id="tdDestinatario" name="tdDestinatario"	onClick="AtivarPasta(admIframe);">
											<bean:message key="prompt.procurar" />
										</td>
										<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao" name="tdManifestacao" onClick="AtivarPasta(editIframe);">
											<bean:message key="prompt.tpnegEstagioVenda" />
										</td>
									</tr>
								</table>
							</td>
							<td width="4">
								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
							</td>
						</tr>
						</table>
						
						<% // Chamado: 96471 - 20/08/2014 - Daniel Gon�alves - Corre��o tamanho tabela  %>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" height= "95%">
						<tr>				
			                <td valign="top" class="principalBgrQuadro" height="400">
			                	<br>
			                	<div name="Manifestacao" id="Manifestacao" style=" width: 97%; height: 95%; z-index: 6; display: none;"> 
									<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" height="95%">
									<tr>									
				                        <td valign="top"> 
					                    	<!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
    					                    <iframe id=editIframe name="editIframe"	src="AdministracaoCsAstbTpnegestagiovendaTnev.do?tela=editCsAstbTpNegEstagioVendaTnev&acao=incluir" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0">
    					                    </iframe>
    					                </td>
									</tr>
									</table>
								</div>
			                    
			          			<div name="Destinatario" id="Destinatario" style=" width: 97%; height: 95%; z-index: 2; display: block">     
									<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr>
									<td colspan="3" valign="top">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="15%" align="right" class="principalLabel"><bean:message key="prompt.tiponegocio"/>
													<img src="webFiles/images/icones/setaAzul.gif"/>&nbsp;
												</td>
												<td width="55%">
													<html:select property="idTpneCdTiponegocio" styleClass="principalObjForm" onchange="carregaEstagio();">
													  <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
													  <logic:present name="tpnegVector">
													    <html:options collection="tpnegVector" property="idTpneCdTiponegocio" labelProperty="tpneDsTiponegocio" />
													  </logic:present>
													</html:select>
												</td>
												<td width="30%">&nbsp;
												</td>
											</tr>	
										</table>
									</td>
								</tr>
								<tr>
									<td class="espacoPqn" colspan="3">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="3">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="15%" align="right" class="principalLabel"><bean:message key="prompt.estagioVenda"/>
													<img src="webFiles/images/icones/setaAzul.gif"/>&nbsp;
												</td>
												<td width="55%">
													<iframe id="ifrmCmbEstagio" name="ifrmCmbEstagio" src="AdministracaoCsAstbTpnegestagiovendaTnev.do?tela=<%=MAConstantes.TELA_CMB_ESTAGIOVENDA%>&acao=<%=Constantes.ACAO_FITRAR%>" width="100%" height="20px" frameborder="0" marginwidth="1" marginheight="0" scrolling="no"></iframe>
												</td>
												<td width="30%">&nbsp;
													<img src="webFiles/images/botoes/lupa.gif" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
												</td>
											</tr>	
										</table>
									</td>
								</tr>
									<tr>
										<td class="principalLabel" width="15%">
											&nbsp;
										</td>
										<td class="principalLabel" width="64%">
											&nbsp;
										</td>
									</tr>
									</table>
									<% // Chamado: 96471 - 20/08/2014 - Daniel Gon�alves - Corre��o tamanho tabela  %>
									<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" height ="90%" >
										<tr>
											<td colspan="5" class="principalLstCab" valign="middle" style="height:20px">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="principalLstCab" width="3%">&nbsp;</td>
														<td class="principalLstCab" width="20%">&nbsp;<bean:message key="prompt.tiponegocio"/></td>
														<td class="principalLstCab" width="22%"><bean:message key="prompt.estagioVenda"/></td>
														<td class="principalLstCab" width="10%"><bean:message key="prompt.ordem"/></td>
														<td class="principalLstCab" width="17%"><bean:message key="prompt.previsaodias"/></td>
														<td class="principalLstCab" width="13%"><bean:message key="prompt.percentual"/></td>
														<td class="principalLstCab" width="15%"><bean:message key="prompt.inativo"/></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr valign="top">
											<td colspan="5"> 
					                        	<!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
	                          					<iframe id=admIframe name="admIframe" 
	                          						src="AdministracaoCsAstbTpnegestagiovendaTnev.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
	                          						width="100%" height="98%" scrolling="Default" frameborder="0"
	                          						marginwidth="0" marginheight="0">
	                          					</iframe>
	                          				</td>
										</tr>		
									</table>							
								</div>
							</td>
							<td width="4" style="height:100%">
								<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
							</td>
						</tr>
						<tr>
							<td width="100%" height="8" valign="top"><img 
							src="webFiles/images/linhas/horSombra.gif" width="100%" 
							height="4"></td>
							<td width="4" height="8" valign="top"><img 
							src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" 
							height="4"></td>
						</tr>
						</table>		
					</td>
				</tr>
				</table>			
				<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
						<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
						<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
						<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
				</tr>
				</table>					
				
				<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
				</table>				
			</td>					
			<td width="4" height="100%">
				<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
			</td>
		</tr>
		<tr>
			<td width="100%" valign="top"><img 
			src="webFiles/images/linhas/horSombra.gif" width="100%" 
			height="4"></td>
			<td width="4" valign="top"><img 
			src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" 
			height="4"></td>
		</tr>
	</table>
	</body>
	
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGESTAGIOVENDA_INCLUSAO_CHAVE%>', administracaoCsAstbTpnegestagiovendaTnevForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGESTAGIOVENDA_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGESTAGIOVENDA_ALTERACAO_CHAVE%>')){
			administracaoCsAstbTpnegestagiovendaTnevForm.imgGravar.disabled=true;
			administracaoCsAstbTpnegestagiovendaTnevForm.imgGravar.className = 'geralImgDisable';
			administracaoCsAstbTpnegestagiovendaTnevForm.imgGravar.title='';
	   }
</script>

</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>