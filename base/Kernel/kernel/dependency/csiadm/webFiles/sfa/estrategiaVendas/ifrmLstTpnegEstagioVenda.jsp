<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.*"%>

<%
long contReg=0;

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
var nResult=0;

function calculaPorcentagem(){
	var totalPorc= new Number(0);
	
	if (nResult==1){
		if (administracaoCsAstbTpnegestagiovendaTnevForm.dhInativoArray.value == ""){
			totalPorc = new Number(administracaoCsAstbTpnegestagiovendaTnevForm.vrPercentualArray.value);
		}
	}else if (nResult > 1){
		for (i=0;i<administracaoCsAstbTpnegestagiovendaTnevForm.vrPercentualArray.length;i++){
			if (administracaoCsAstbTpnegestagiovendaTnevForm.dhInativoArray[i].value == ""){
				totalPorc =  totalPorc + new Number(administracaoCsAstbTpnegestagiovendaTnevForm.vrPercentualArray[i].value);
			}	
		}
	}else{
		alert ('<bean:message key="prompt.Nenhuma_associacao_encontrada_para_gravacao"/>');
		return false;
	}
	
	if (totalPorc!=100){
		alert ('<bean:message key="prompt.A_soma_das_porcentagens_dos_estagios_nao_pode_ser_diferente_de_100"/>');
		return false;
	}
	
	return true;
}

function submetRemover(nSeq){

	parent.submetEdit(nSeq);
	parent.habilitaCampo(false);

	if (!confirm('<bean:message key="prompt.Deseja_remover_esse_item" />')){
		parent.habilitaCampo(true);
		parent.limpaCampos();
		return false;
	}

	nSeq = nSeq -1;

	if (nResult == 1){
		administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value = administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray.value;
		administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.value = administracaoCsAstbTpnegestagiovendaTnevForm.idEsveArray.value;
	}
	else if (nResult > 1){
		administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value = administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray[nSeq].value;
		administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.value = administracaoCsAstbTpnegestagiovendaTnevForm.idEsveArray[nSeq].value;
	}
	
	administracaoCsAstbTpnegestagiovendaTnevForm.tela.value="<%=MAConstantes.TELA_LST_TPNEGESTAGIOVENDA%>";
	administracaoCsAstbTpnegestagiovendaTnevForm.acao.value="<%=Constantes.ACAO_EXCLUIR%>";
	administracaoCsAstbTpnegestagiovendaTnevForm.submit();
		
}

function iniciaTela(){
	if (administracaoCsAstbTpnegestagiovendaTnevForm.acao.value == '<%=Constantes.ACAO_RECUSAR%>'){
		alert('<bean:message key="prompt.Esta_associacao_esta_em_uso_em_outros_registros" />');
	}else if (administracaoCsAstbTpnegestagiovendaTnevForm.acao.value == '<%=Constantes.ACAO_EXCLUIR%>'){
		parent.habilitaCampo(true);
		parent.limpaCampos();
	}
	
	if (nResult > 1){
		//manter desabilitado o combo de Tipo de neg�cio para que o controle de sess�o funcione corretamente
		parent.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.disabled = true;
	}
	
	
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/AdministracaoCsAstbTpnegestagiovendaTnev.do" styleId="administracaoCsAstbTpnegestagiovendaTnevForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>

<html:hidden property="idTpneCdTiponegocio"/>
<html:hidden property="idEsveCdEstagiovenda"/>
<html:hidden property="tnevNrOrdem"/>
<html:hidden property="tnevNrPrevisao"/>
<html:hidden property="tnevVrPercentual"/>
<html:hidden property="esveDsEstagiovenda"/>
<html:hidden property="tpneDsTiponegocio"/>
<html:hidden property="inInativo"/>
<html:hidden property="tnevDhInativo"/>


<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td height="210" valign="top"> 
      <div id="lstEstagioVenda" style="position:absolute; width:100%; height:205px; z-index:1; visibility: visible; overflow: auto"> 
	    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	       <logic:present name="tnevVector">
		       <logic:iterate id="tnevVector" name="tnevVector" indexId="sequencia">
			       <logic:notEqual name="tnevVector" property="atualizar" value="E" >
			       <%contReg++;%>
			       <script>nResult++;</script>
					<tr>
					    <td class="principalLstPar" width="3%"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="submetRemover(<%=contReg%>);">
						    <input type="hidden" name="idTpneArray" value='<bean:write name="tnevVector" property="idTpneCdTiponegocio"/>'>
						    <input type="hidden" name="idEsveArray" value='<bean:write name="tnevVector" property="idEsveCdEstagiovenda"/>'>
						    <input type="hidden" name="dsEsveArray" value='<bean:write name="tnevVector" property="esveDsEstagiovenda"/>'>
						    <input type="hidden" name="nrOrdemArray" value='<bean:write name="tnevVector" property="tnevNrOrdem"/>'>
						    <input type="hidden" name="nrPrevisaoArray" value='<bean:write name="tnevVector" property="tnevNrPrevisao"/>'>
						    <input type="hidden" name="vrPercentualArray" value='<bean:write name="tnevVector" property="tnevVrPercentual"/>'>
						    <input type="hidden" name="dhInativoArray" value='<bean:write name="tnevVector" property="tnevDhInativo"/>'>
						</td>
						<td class="principalLstPar" width="20%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEdit(<%=contReg%>);">
								<bean:write name="tnevVector" property="tpneDsTiponegocio"/>
							</span>
						</td>
						<td class="principalLstPar" width="24%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEdit(<%=contReg%>);">
								<bean:write name="tnevVector" property="esveDsEstagiovenda"/>
							</span>	
						</td>
						<td class="principalLstPar" width="12%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEdit(<%=contReg%>);">
								<bean:write name="tnevVector" property="tnevNrOrdem"/>
							</span>		
						</td>
						<td class="principalLstPar" width="17%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEdit(<%=contReg%>);">
								<bean:write name="tnevVector" property="tnevNrPrevisao"/>
							</span>		
						</td>
						<td class="principalLstPar" width="13%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEdit(<%=contReg%>);">
								<bean:write name="tnevVector" property="tnevVrPercentual"/>
							</span>		
						</td>
						<td class="principalLstPar" width="11%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEdit(<%=contReg%>);">
								<bean:write name="tnevVector" property="tnevDhInativo"/>
							</span>		
						</td>
					</tr>
			       </logic:notEqual>	
			   </logic:iterate>
		   </logic:present>
		</table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
