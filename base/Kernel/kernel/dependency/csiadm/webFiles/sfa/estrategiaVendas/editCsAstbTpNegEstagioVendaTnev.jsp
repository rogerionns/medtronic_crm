<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

var nResult = 0;

function carregaTpNegEstVenda(){
	parent.submeteFormEdit(administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value);
}

function adicionaRegistro(){
	var idTpne=administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value;
	var idEsve=administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.value;
	var nrOrdem=administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrOrdem.value; 
	var nrPrevisao=administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrPrevisao.value;
	var vrPercentual=administracaoCsAstbTpnegestagiovendaTnevForm.tnevVrPercentual.value;
	var dhInativo="";
	var dsTiponegocio="";
	var dsEstagiovenda="";
	var dsAcao="";
	var testaIgualdade=true;
	
	if (administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.disabled){
		dsAcao='<%=Constantes.ACAO_EDITAR%>'
		testaIgualdade=false;	
	}else
		dsAcao='<%=Constantes.ACAO_INCLUIR%>';
	
	
	if (idTpne==""){
		alert ('<bean:message key="prompt.O_campo_tipo_de_negocio_e_obrigatorio"/>');
		return false;
	}else{
		dsTiponegocio = administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio[administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.selectedIndex].text;
	}

	if (idEsve==""){
		alert ('<bean:message key="prompt.O_campo_estagio_de_venda_e_obrigatorio"/>');
		return false;
	}else{
		dsEstagiovenda = administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda[administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.selectedIndex].text;
	}
			
	if (trim(nrOrdem)=="" || trim(nrOrdem)=="0"){
		alert ('<bean:message key="prompt.O_campo_ordem_e_obrigatorio"/>');
		return false;
	}

	if (trim(nrPrevisao)=="" || trim(nrPrevisao)=="0"){
		alert ('<bean:message key="prompt.O_campo_previsao_em_dias_e_obrigatorio"/>');
		return false;
	}

	if (trim(vrPercentual)=="" || trim(vrPercentual)=="0"){
		alert ('<bean:message key="prompt.O_campo_percentual_e_obrigatorio"/>');
		return false;
	}
	
	if (!novoRegistro(idTpne,idEsve,nrOrdem,testaIgualdade)){
		return false;
	}
	
	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value=administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value;
	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.value=administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.value;
	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrOrdem.value=administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrOrdem.value; 
	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrPrevisao.value=administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrPrevisao.value;
	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.tnevVrPercentual.value=administracaoCsAstbTpnegestagiovendaTnevForm.tnevVrPercentual.value;

	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.tpneDsTiponegocio.value=dsTiponegocio;
	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.esveDsEstagiovenda.value=dsEstagiovenda;
	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.inInativo.value=administracaoCsAstbTpnegestagiovendaTnevForm.inInativo.checked;
	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.tnevDhInativo.value=administracaoCsAstbTpnegestagiovendaTnevForm.tnevDhInativo.value;

	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.tela.value="<%=MAConstantes.TELA_LST_TPNEGESTAGIOVENDA%>";
	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.acao.value = dsAcao;
	ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.submit();
	
	limpaCampos();
}

function limpaCampos(){
	//(A limpeza deste campo � feita no novo. gravar e cancelar)
	//administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value = "";
	//--------------------------------------------------------
	
	if(isEditando){
		administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.remove[administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.selectedIndex];
		isEditando = false;
	}
	
	administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.value = "";

	administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.disabled = false;
	administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.disabled = false;
	
	
	administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrOrdem.value = ""; 
	administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrPrevisao.value = "";
	administracaoCsAstbTpnegestagiovendaTnevForm.tnevVrPercentual.value = "";
	administracaoCsAstbTpnegestagiovendaTnevForm.inInativo.checked = false;
}

function novoRegistro(idTpne,idEsve,nrOrdem,testaIgualdade){
	
	if (ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray  == undefined){
		return true;
	}
	if (ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray.length == undefined){
			if ((testaIgualdade) && (idTpne == ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray.value) && (idEsve == ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idEsveArray.value)){
				alert ('<bean:message key="prompt.Associacao_ja_existente"/>.');
				return false;
			}
			
			if (nrOrdem==ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.nrOrdemArray.value){
				if (testaIgualdade){
					alert ('<bean:message key="prompt.Este_numero_de_orderm_ja_esta_sendo_utilizado"/>.');
					return false;			
				}else if ((idTpne != ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray.value) || (idEsve != ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idEsveArray.value)){
					alert ('<bean:message key="prompt.Este_numero_de_orderm_ja_esta_sendo_utilizado"/>.');
					return false;			
				}	
			}
	}else{
		for (i=0;i<ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray.length;i++){
			if ((testaIgualdade) && (idTpne == ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray[i].value) && (idEsve == ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idEsveArray[i].value)){
				alert ('<bean:message key="prompt.Associacao_ja_existente"/>.');
				return false;
			}
			
			if (nrOrdem==ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.nrOrdemArray[i].value){
				if (testaIgualdade){
					alert ('<bean:message key="prompt.Este_numero_de_orderm_ja_esta_sendo_utilizado"/>.');
					return false;			
				}else if ((idTpne != ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray[i].value) || (idEsve != ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idEsveArray[i].value)){
					alert ('<bean:message key="prompt.Este_numero_de_orderm_ja_esta_sendo_utilizado"/>.');
					return false;			
				}	
			}
		}
	}
	
	return true;
}

var isEditando = false;
function submetEdit(nSeq){
	
	var novoOption = new Option();
	
	var idEstagioVenda = 0;
	var dsEstagioVenda = "";
	
	nSeq = nSeq - 1;

	if (ifrmLstTpnegEstagioVenda.nResult == 1){
		administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray.value;
		idEstagioVenda = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idEsveArray.value;
		dsEstagioVenda = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.dsEsveArray.value;
		administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrOrdem.value = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.nrOrdemArray.value; 
		administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrPrevisao.value = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.nrPrevisaoArray.value;
		administracaoCsAstbTpnegestagiovendaTnevForm.tnevVrPercentual.value = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.vrPercentualArray.value;
		administracaoCsAstbTpnegestagiovendaTnevForm.tnevDhInativo.value = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.dhInativoArray.value;
		
	}else if (ifrmLstTpnegEstagioVenda.nResult > 1){
		administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.value = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idTpneArray[nSeq].value;
		idEstagioVenda = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.idEsveArray[nSeq].value;
		dsEstagioVenda = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.dsEsveArray[nSeq].value;
		administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrOrdem.value = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.nrOrdemArray[nSeq].value; 
		administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrPrevisao.value = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.nrPrevisaoArray[nSeq].value;
		administracaoCsAstbTpnegestagiovendaTnevForm.tnevVrPercentual.value = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.vrPercentualArray[nSeq].value;
		administracaoCsAstbTpnegestagiovendaTnevForm.tnevDhInativo.value = ifrmLstTpnegEstagioVenda.administracaoCsAstbTpnegestagiovendaTnevForm.dhInativoArray[nSeq].value;
	}	

	isEditando = true;
	novoOption.value = idEstagioVenda;
	novoOption.text = dsEstagioVenda;
	administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.add(novoOption);
	administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.selectedIndex = administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.length - 1;
	
	administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.disabled=true;
	administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.disabled=true;

	if (administracaoCsAstbTpnegestagiovendaTnevForm.tnevDhInativo.value != ""){
		administracaoCsAstbTpnegestagiovendaTnevForm.inInativo.checked = true;
	}
	
	
}

function habilitaCampo(bStatus){
	administracaoCsAstbTpnegestagiovendaTnevForm.idTpneCdTiponegocio.disabled=!bStatus;
	administracaoCsAstbTpnegestagiovendaTnevForm.idEsveCdEstagiovenda.disabled=!bStatus;
	administracaoCsAstbTpnegestagiovendaTnevForm.inInativo.disabled = !bStatus;
	administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrOrdem.disabled = !bStatus;
	administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrPrevisao.disabled = !bStatus;
	administracaoCsAstbTpnegestagiovendaTnevForm.tnevVrPercentual.disabled = !bStatus;
}

// -->
</script>
</head>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');" style="overflow: hidden;">
<html:form styleId="administracaoCsAstbTpnegestagiovendaTnevForm" action="/AdministracaoCsAstbTpnegestagiovendaTnev.do">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="tnevDhInativo"/>


<table width="97%" border="0" cellspacing="1" cellpadding="1" align="center">
  <tr> 
    <td width="24%" align="right" class="principalLabel">Tipo de Neg&oacute;cio 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
    <td colspan="2"> 
		<html:select property="idTpneCdTiponegocio" styleClass="principalObjForm" onchange="carregaTpNegEstVenda();">
		  <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		  <logic:present name="tpnegVector">
		    <html:options collection="tpnegVector" property="idTpneCdTiponegocio" labelProperty="tpneDsTiponegocio" />
		  </logic:present>
		</html:select>
    </td>
    <td width="24%"><!-- img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" --></td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel">Est&aacute;gio de Venda 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
    <td colspan="2"> 
		<html:select property="idEsveCdEstagiovenda" styleClass="principalObjForm">
		  <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		  <logic:present name="esveVector">
		    <html:options collection="esveVector" property="idEsveCdestagiovenda" labelProperty="esveDsEstagiovenda" />
		  </logic:present>
		</html:select>
    </td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.ordem" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td width="24%"> 
      <html:text property="tnevNrOrdem" maxlength="4" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
      <script>
      	if (administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrOrdem.value == "0")
      		administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrOrdem.value = "";
      </script>
    </td>
    <td width="28%">&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.previsaodias" />
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
    <td width="24%"> 
      <html:text property="tnevNrPrevisao" maxlength="4" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
      <script>
      	if (administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrPrevisao.value == "0")
      		administracaoCsAstbTpnegestagiovendaTnevForm.tnevNrPrevisao.value = "";
      </script>
    </td>
    <td width="28%">&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.percentual" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td width="24%"> 
      <html:text property="tnevVrPercentual" maxlength="4" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
      <script>
      	if (administracaoCsAstbTpnegestagiovendaTnevForm.tnevVrPercentual.value == "0")
      		administracaoCsAstbTpnegestagiovendaTnevForm.tnevVrPercentual.value = "";
      </script>
    </td>
    <td width="28%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionaRegistro();"></td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel">&nbsp; </td>
    <td width="24%" class="principalLabel"> 
      <html:checkbox property="inInativo" />
      Inativo </td>
    <td width="28%">&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel">&nbsp;</td>
    <td width="24%" class="principalLabel">&nbsp;</td>
    <td width="28%" class="principalLabel">&nbsp;</td>
    <td width="24%" class="principalLabel">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="principalLstCab" width="4%">&nbsp;</td>
		<td class="principalLstCab" width="20%">&nbsp;<bean:message key="prompt.tiponegocio"/></td>
		<td class="principalLstCab" width="22%"><bean:message key="prompt.estagioVenda"/></td>
		<td class="principalLstCab" width="10%"><bean:message key="prompt.ordem"/></td>
		<td class="principalLstCab" width="17%"><bean:message key="prompt.previsaodias"/></td>
		<td class="principalLstCab" width="13%"><bean:message key="prompt.percentual"/></td>
		<td class="principalLstCab" width="14%"><bean:message key="prompt.inativo"/></td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td height="210" valign="top"> 
    	<iframe id="ifrmLstTpnegEstagioVenda" name="ifrmLstTpnegEstagioVenda" src="AdministracaoCsAstbTpnegestagiovendaTnev.do?tela=<%=MAConstantes.TELA_LST_TPNEGESTAGIOVENDA%>&acao=<%=Constantes.ACAO_CONSULTAR%>&idTpneCdTiponegocio=<bean:write name='administracaoCsAstbTpnegestagiovendaTnevForm' property='idTpneCdTiponegocio'/>" width="100%" height="210px" frameborder="0" marginwidth="1" marginheight="0" scrolling="no"></iframe>
    </td>
  </tr>  
</table>
</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			//administracaoCsAstbTpnegestagiovendaTnevForm.areaDsArea.disabled= true;	
			//administracaoCsAstbTpnegestagiovendaTnevForm.areaDhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGESTAGIOVENDA_INCLUSAO_CHAVE%>', parent.administracaoCsAstbTpnegestagiovendaTnevForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGESTAGIOVENDA_INCLUSAO_CHAVE%>')){
				habilitaCampo(false);
			}		
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGESTAGIOVENDA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGESTAGIOVENDA_ALTERACAO_CHAVE%>', parent.administracaoCsAstbTpnegestagiovendaTnevForm.imgGravar);	
				habilitaCampo(false);
			}
		</script>
</logic:equal>
</html>
<script>
	try{administracaoCsAstbTpnegestagiovendaTnevForm.areaDsArea.focus();}
	catch(e){}	
</script>
</html>
