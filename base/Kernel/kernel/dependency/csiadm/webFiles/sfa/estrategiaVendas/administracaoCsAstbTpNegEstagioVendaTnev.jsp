<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsAstbTpnegestagiovendaTnevForm" action="/AdministracaoCsAstbTpnegestagiovendaTnev.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idTpneCdTiponegocio" />
	<html:hidden property="idEsveCdEstagiovenda" />

<script>var possuiRegistros=false;</script>
<!-- Jonathan Costa | FullScreen 12/2013 -->	
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
 
 <logic:iterate id="vetorBean" name="vetorBean" indexId="sequencia">
  <script>possuiRegistros=true;</script>
  <tr>
    <td width="3%" class="principalLstPar">&nbsp;
      <!-- img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="vetorBean" property="idTpneCdTiponegocio" />','<bean:write name="vetorBean" property="idEsveCdEstagiovenda" />')"-->
    </td>
    <td class="principalLstParMao" width="20%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="vetorBean" property="idTpneCdTiponegocio" />','<bean:write name="vetorBean" property="idEsveCdEstagiovenda" />')">
      &nbsp;<bean:write name="vetorBean" property="tpneDsTiponegocio" />
    </td>
    <td id="lstResultado" class="principalLstParMao" width="22%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="vetorBean" property="idTpneCdTiponegocio" />','<bean:write name="vetorBean" property="idEsveCdEstagiovenda" />')">
      &nbsp;<bean:write name="vetorBean" property="esveDsEstagiovenda" />
	</td>
	<td class="principalLstParMao" align="left" width="10%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="vetorBean" property="idTpneCdTiponegocio" />','<bean:write name="vetorBean" property="idEsveCdEstagiovenda" />')">
      &nbsp;<bean:write name="vetorBean" property="tnevNrOrdem" />
	</td>
	<td class="principalLstParMao" align="left" width="17%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="vetorBean" property="idTpneCdTiponegocio" />','<bean:write name="vetorBean" property="idEsveCdEstagiovenda" />')">
      &nbsp;&nbsp;&nbsp;<bean:write name="vetorBean" property="tnevNrPrevisao" />
	</td>
	<td class="principalLstParMao" align="left" width="13%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="vetorBean" property="idTpneCdTiponegocio" />','<bean:write name="vetorBean" property="idEsveCdEstagiovenda" />')">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="vetorBean" property="tnevVrPercentual" />
	</td>
	<td class="principalLstParMao" align="left" width="15%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="vetorBean" property="idTpneCdTiponegocio" />','<bean:write name="vetorBean" property="idEsveCdEstagiovenda" />')">
      &nbsp;&nbsp;&nbsp;<bean:write name="vetorBean" property="tnevDhInativo" />
	</td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td  align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>

<div id="divErro"  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGESTAGIOVENDA_EXCLUSAO_CHAVE%>', administracaoCsAstbTpnegestagiovendaTnevForm.lixeira);	
	
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>