<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
var result=0;
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsAstbTpnegocioprodutoTpnpForm" action="/AdministracaoCsAstbTpnegocioprodutoTpnp.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csAstbTpnegocioprodutoTpnpVector" indexId="sequencia"> 
  <script>
  	result++;
  </script>
  <tr> 
    <td width="80%" class="principalLstPar"> 
    	<img id="imgLixeira" src="webFiles/images/botoes/lixeira18x18.gif" width="15" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" 
    		onclick="parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="idTpneCdTiponegocio" />', '<bean:write name="ccttrtVector" property="idPrsfCdProdutosfa" />')"> 
    	&nbsp;<bean:write name="ccttrtVector" property="prsdDsProdutoSfa" /> 
    </td>
    <td width="20%" class="principalLstPar">
	     &nbsp;<bean:write name="ccttrtVector" property="prsfDhInativo" /> 
    </td>
  </tr>
  </logic:iterate>
</table>

<script>
	//setPermissaoImageDisable('<!--%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGPRODUTO_EXCLUSAO_CHAVE%-->', editCsAstbTpnegocioprodutoTpnpForm.lixeira);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGPRODUTO_EXCLUSAO_CHAVE%>')){
		if (result>1){
			for (i=0;i<window.document.editCsAstbTpnegocioprodutoTpnpForm.imgLixeira.length;i++){
				window.document.editCsAstbTpnegocioprodutoTpnpForm.imgLixeira[i].disabled = true;
				window.document.editCsAstbTpnegocioprodutoTpnpForm.imgLixeira[i].className = "geralImgDisable";
			}
		}else if(result==1){
			window.document.editCsAstbTpnegocioprodutoTpnpForm.imgLixeira.disabled = true;
			window.document.editCsAstbTpnegocioprodutoTpnpForm.imgLixeira.className = "geralImgDisable";
		}
	}
</script>

</html:form>
</body>
</html>