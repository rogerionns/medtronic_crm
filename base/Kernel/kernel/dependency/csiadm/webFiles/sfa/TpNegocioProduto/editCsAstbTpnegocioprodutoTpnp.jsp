<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head>
<script language="JavaScript">
<!--
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

	function VerificaCampos(){
/*		if (administracaoCsAstbTpnegocioprodutoTpnpForm.pqueNrSequencia.value == "0" ){
			administracaoCsAstbTpnegocioprodutoTpnpForm.pqueNrSequencia.value = "" ;
		}
		if (administracaoCsAstbTpnegocioprodutoTpnpForm.pqueNrTamanhodado.value == "0" ){
			administracaoCsAstbTpnegocioprodutoTpnpForm.pqueNrTamanhodado.value = "" ;
		}*/
	}

	function MontaLista(){
		lstArqCarga.location.href= "AdministracaoCsAstbTpnegocioprodutoTpnp.do?tela=administracaoLstCsAstbTpnegocioprodutoTpnp&acao=<%=Constantes.ACAO_VISUALIZAR%>&idTpneCdTiponegocio=" + administracaoCsAstbTpnegocioprodutoTpnpForm.idTpneCdTiponegocio.value;
	}

	function desabilitaCampos(){
		administracaoCsAstbTpnegocioprodutoTpnpForm.idTpneCdTiponegocio.disabled = true;
		administracaoCsAstbTpnegocioprodutoTpnpForm.idPrsfCdProdutosfa.disabled = true;
	}

//-->

	function verificaLimite(limite)
	{
		if(limite != '' && limite.value > 250)
		{
			alert("<bean:message key='prompt.tamanhoLimitePermitido250'/>");
			return false;
		}
	}
	
</script>
</head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos();MontaLista()">

<html:form styleId="administracaoCsAstbTpnegocioprodutoTpnpForm" action="/AdministracaoCsAstbTpnegocioprodutoTpnp.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.tipoNegocio"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
    <html:select property="idTpneCdTiponegocio" styleClass="principalObjForm" onchange="MontaLista()"> 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option>
      <html:options collection="csCdtbTiponegocioTpneVector" property="idTpneCdTiponegocio" labelProperty="tpneDsTiponegocio"/> 
      </html:select> 
      </td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.produto"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
    <html:select property="idPrsfCdProdutosfa" styleClass="principalObjForm"> 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
      <html:options collection="csCdtbProdutosfaPrsfVector" property="idPrsfCdProdutosfa" labelProperty="prsfDsProdutosfa"/> 
      </html:select> 
      </td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%">&nbsp;</td>
    <td colspan="2">&nbsp; </td>
    <td width="14%"> </td>
  </tr>
  <tr> 
    <td colspan="4" height="200"> 
      <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="5%" class="principalLstCab" height="1">&nbsp;&nbsp;</td>
          <td width="73%" colspan="3" class="principalLstCab"><bean:message key="prompt.produto"/></td>
          <td width="22%" class="principalLstCab">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="170"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGPRODUTO_INCLUSAO_CHAVE%>', parent.administracaoCsAstbTpnegocioprodutoTpnpForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGPRODUTO_INCLUSAO_CHAVE%>')){
				desabilitaCampos();
			}else{
			//	administracaoCsAstbTpnegocioprodutoTpnpForm.idPesqCdPesquisa.disabled= false;	
			//	administracaoCsAstbTpnegocioprodutoTpnpForm.idQuesCdQuestao.disabled= false;	
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGPRODUTO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_TPNEGPRODUTO_ALTERACAO_CHAVE%>', parent.administracaoCsAstbTpnegocioprodutoTpnpForm.imgGravar);	
				desabilitaCampos();
			}
		</script>
</logic:equal>

</html>

<script>
	try{administracaoCsAstbTpnegocioprodutoTpnpForm.idPesqCdPesquisa.focus();}
	catch(e){}
</script>