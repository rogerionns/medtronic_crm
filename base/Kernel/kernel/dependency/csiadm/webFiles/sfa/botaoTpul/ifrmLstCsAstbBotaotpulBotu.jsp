<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');" topmargin="0">
<html:form styleId="editCsAstbBotaotpulBotuForm" action="/AdministracaoCsAstbBotaotpulBotu.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csAstbBotaotpulBotuVector" indexId="sequencia"> 
  <tr> 
    <td width="5%" > 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" id="lixeira" name="lixeira" id="lixeira" width="18" height="18" class="principalLstParMao" title="<bean:message key="prompt.excluir"/>" onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="idTpulCdTipopublicolead" />','<bean:write name="ccttrtVector" property="idBotaCdBotao" />')" > 
    </td>
    <td  class="principalLstPar"  >
     <bean:write name="ccttrtVector" property="botaDsBotao" /> 
    </td>
  </tr>
  </logic:iterate>
</table>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICOBOTAO_EXCLUSAO_CHAVE%>', document.getElementById('lixeira'));
</script>

</html:form>
</body>
</html>