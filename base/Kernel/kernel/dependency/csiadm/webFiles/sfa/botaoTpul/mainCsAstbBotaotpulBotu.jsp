<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsAstbBotaotpulBotuForm.target = admIframe.name;
	document.administracaoCsAstbBotaotpulBotuForm.acao.value ='filtrar';
	document.administracaoCsAstbBotaotpulBotuForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsAstbBotaotpulBotuForm.filtro.value = '';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsAstbBotaotpulBotuForm.target = editIframe.name;
	editIframe.document.administracaoCsAstbBotaotpulBotuForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_BOTAOTPUL_BOTU%>';
	editIframe.document.administracaoCsAstbBotaotpulBotuForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsAstbBotaotpulBotuForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo1,codigo2){
	tab.document.administracaoCsAstbBotaotpulBotuForm.idTpulCdTipopublicolead.value = codigo1;
	tab.document.administracaoCsAstbBotaotpulBotuForm.idBotaCdBotao.value = codigo2;
	tab.document.administracaoCsAstbBotaotpulBotuForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_BOTAOTPUL_BOTU%>';
	tab.document.administracaoCsAstbBotaotpulBotuForm.target = editIframe.name;
	tab.document.administracaoCsAstbBotaotpulBotuForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsAstbBotaotpulBotuForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){
	/*
	if(tab.document.administracaoCsAstbBotaotpulBotuForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_BOTAOTPUL_BOTU%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsAstbBotaotpulBotuForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_ASTB_BOTAOTPUL_BOTU%>'){
	*/

	if(window.document.all.item("Manifestacao").style.visibility == "none"){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else{	

		if (tab.document.administracaoCsAstbBotaotpulBotuForm.idTpulCdTipopublicolead.value == "") {
			alert("<bean:message key="prompt.O_campo_tipo_de_publico_e_obrigatorio"/>.");
			tab.document.administracaoCsAstbBotaotpulBotuForm.idTpulCdTipopublicolead.focus();
			return false;
		}

		if (tab.document.administracaoCsAstbBotaotpulBotuForm.idBotaCdBotao.value == "") {
			alert("<bean:message key="prompt.Por_favor_escolha_um_botao"/>.");
			tab.document.administracaoCsAstbBotaotpulBotuForm.idBotaCdBotao.focus();
			return false;
		}

		tab.document.administracaoCsAstbBotaotpulBotuForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_BOTAOTPUL_BOTU%>';
		tab.document.administracaoCsAstbBotaotpulBotuForm.target = admIframe.name;
		disableEnable(tab.document.administracaoCsAstbBotaotpulBotuForm.idTpulCdTipopublicolead, false);
		tab.document.administracaoCsAstbBotaotpulBotuForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
		tab.document.administracaoCsAstbBotaotpulBotuForm.submit();
		//disableEnable(tab.document.administracaoCsAstbBotaotpulBotuForm.idTpulCdTipopublicolead, true);
		//cancel();

		setTimeout("editIframe.ifmlstCsAstbBotaotpulBotu.location.reload()", 2500);
		tab.document.administracaoCsAstbBotaotpulBotuForm.idBotaCdBotao.value = "";
	}
}

function submeteExcluir(codigo1,codigo2) {
	if(confirm("<bean:message key="prompt.Deseja_remover_esse_item"/>")){
		editIframe.document.administracaoCsAstbBotaotpulBotuForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_BOTAOTPUL_BOTU%>';
		editIframe.document.administracaoCsAstbBotaotpulBotuForm.target = admIframe.name;
		editIframe.document.administracaoCsAstbBotaotpulBotuForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		//disableEnable(editIframe.document.administracaoCsAstbBotaotpulBotuForm.idTpulCdTipopublicolead, false);
		
		editIframe.document.administracaoCsAstbBotaotpulBotuForm.idTpulCdTipopublicolead.value = codigo1;
		editIframe.document.administracaoCsAstbBotaotpulBotuForm.idBotaCdBotao.value = codigo2;
		
		editIframe.document.administracaoCsAstbBotaotpulBotuForm.submit();
		//disableEnable(editIframe.document.administracaoCsAstbBotaotpulBotuForm.idTpulCdTipopublicolead, true);

		/////////////////////////////////
		//AtivarPasta(admIframe);
		//MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		//editIframe.location = 'AdministracaoCsAstbBotaotpulBotu.do?tela=CsAstbBotaotpulBotu&acao=incluir';
		/////////////////////////////////
	
		tab.document.administracaoCsAstbBotaotpulBotuForm.idBotaCdBotao.value = "";
		setTimeout("editIframe.ifmlstCsAstbBotaotpulBotu.location.reload()", 2000);
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsAstbBotaotpulBotu.do?tela=editCsAstbBotaotpulBotu&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsAstbBotaotpulBotuForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsAstbBotaotpulBotuForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_BOTAOTPUL_BOTU%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			break;
		case editIframe:
			tab.document.administracaoCsAstbBotaotpulBotuForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_BOTAOTPUL_BOTU%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">

<html:form styleId="administracaoCsAstbBotaotpulBotuForm"	action="/AdministracaoCsAstbBotaotpulBotu.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/><!-- ## --></td>
									<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.tipoDePublico_x_Botao"/><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="width: 97%; height: 225px;display: none"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsAstbBotaotpulBotu.do?tela=editCsAstbBotaotpulBotu&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="width: 97%; height: 199px;display: block"> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td class="principalLabel" width="8%" colspan="2"><bean:message key="prompt.nomeDocumento"/></td>

								</tr>

								<tr>
									
                        <td colspan="3"> 
                          <table border="0" cellspacing="0" cellpadding="0">
									<tr>
									<td width="25%">
									<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
									</td>
									<td width="05%">
									&nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
									</td>
									</tr>	
									</table>
									</td>
								</tr>
								<tr>
									
                        <td class="principalLabel" width="8%">&nbsp;</td>
									
                        <td class="principalLabel" width="49%">&nbsp;</td>
								</tr>
								<tr>
									
                        <td class="principalLstCab" width="8%">&nbsp;<bean:message key="prompt.codigo"/></td>
									
                        <td class="principalLstCab" width="49%"><bean:message key="prompt.tipoPublico"/> </td>
									
                        <td class="principalLstCab" align="left" width="43%"><bean:message key="prompt.botao"/></td>
								</tr>
								
								<tr valign="top">
									
                        <td colspan="3" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsAstbBotaotpulBotu.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                </td>
							<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">

/*	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICOBOTAO_INCLUSAO_CHAVE%>', document.administracaoCsAstbBotaotpulBotuForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICOBOTAO_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICOBOTAO_ALTERACAO_CHAVE%>')){
			document.administracaoCsAstbBotaotpulBotuForm.imgGravar.disabled=true;
			document.administracaoCsAstbBotaotpulBotuForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsAstbBotaotpulBotuForm.imgGravar.title='';
	}
	
	desabilitaListaEmpresas();*/
	
</script>


</body>
</html>
