<%@ page language="java" %>

<%
    String msgErro = (String)request.getAttribute("msgerro");
    if (msgErro == null) {
        msgErro = (String)request.getParameter("msgerro");
    }
    if (msgErro == null || msgErro == "null") {
        msgErro = "N�o foi poss�vel determinar o erro.";
    }
%>

<html>
<head>
<title>..: MENSAGEM DO SISTEMA :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../webFiles/css/global.css" type="text/css">
</head>
<script>
	function resposta(cResp){
		
		if (cResp=='S'){
			window.dialogArguments.limpaConf();
			window.close();
		}else if(cResp=='N'){
			window.close();
		}
	}
</script>
<body class="principalBgrPage" text="#000000">
  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
      <td align="center"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
          <tr> 
            <td width="1007" colspan="2"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalPstQuadro" height="17" width="166">MENSAGEM</td>
                  <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                  <td height="17" width="4"><img src="../webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td class="principalBgrQuadro" valign="top" align="center"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td align="center">
                    <table width="99%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr> 
                              <td> 
                                <div align="center"><font size="6"><b><font face="Arial, Helvetica, sans-serif" size="5">Mensagem 
                                  do Sistema<br>
                                </font></b></font></div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          
                        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" height="60">
                          <tr> 
                            <td class="principalLabel" align="center"><%=msgErro%></td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="center">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="principalLabel" align="center">
                              <table width="200" border="0" cellspacing="0" cellpadding="0">
                                <tr align="center"> 
                                  <td><img src="../webFiles/images/botoes/bt_sim.gif" width="72" height="29" class="geralCursoHand" onclick="resposta('S')"></td>
                                  <td><img src="../webFiles/images/botoes/bt_nao.gif" width="72" height="29" class="geralCursoHand" onclick="resposta('N')"></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          
                        <table border="0" cellspacing="0" cellpadding="0" class="geralCursoHand" width="100%">
                          <tr> 
                              <td> 
                                <div align="right">&nbsp;</div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="4"><img src="../webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
          <tr> 
            <td width="100%"><img src="../webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
            <td width="4"><img src="../webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>