<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbAreaAreaVo"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
  
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String idFuncCdFuncionario = (String)request.getAttribute("idFuncCdFuncionario");

Vector areaOpts = (Vector)request.getAttribute("areaOpts");
%>


<html>
<head>
<title>PLUSOFT CRM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script> 

<script>

function buscaFuncByArea(){
	
	var idArea = document.getElementById('area').value;
	
	document.body.style.cursor = 'wait';
	
	try{
		if(idArea == ''){			
			
			var optFunc = document.getElementById('funcSuperior');
			
			var i = optFunc.options.length;		
			while(i > 0){
				i--;
				optFunc.options[i] = null;
			}
			
			optFunc.options[0] = new Option('<bean:message key="prompt.selecione_uma_opcao"/>', '');
			
			document.body.style.cursor = 'default';
		}
		else{
			var dadospost = { 
					idAreaCdArea : idArea
				};
			
			$.post("/csiadm/GenericAdmFindFuncByArea.do", dadospost, function(ret) {	
				
				document.body.style.cursor = 'default';
				
				if(ret.msgerro!=undefined) {
					alert("<bean:message key='prompt.erro_carregar_funcionarios' /> "+ret.msgerro);
					return false;						
				} else if(ret.func!=null && ret.func!=undefined && ret.func!='') {
					
					var optFunc = document.getElementById('funcSuperior');
					
					for(var x = 0; x < ret.func.length; x++){
						var idFunc = ret.func[x][0];
						var nmFunc = ret.func[x][1];
						
						if(idFunc != <%=idFuncCdFuncionario%>){
							optFunc.options[optFunc.options.length] = new Option(nmFunc, idFunc);
						}
					}
				}
				
			}, "json");
		}
	}
	catch(x){		
		alert("Erro em findFuncByArea()"+ x.description);		
	}
}

function confirmarSuperior(){
	var idFunc = document.getElementById('funcSuperior').value;
	if(idFunc == ''){
		alert('<bean:message key='prompt.Por_favor_selecione_um_funcionario' />');		
	}
	else{
		window.returnValue = idFunc;
		window.close();
	}
}
</script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">


<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr> 
		<td width="1007" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
				<tr> 
					<td class="principalPstQuadro" height="17" width="166"><%= getMessage("prompt.superior", request)%></td>
					<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
					<td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
				</tr> 
			</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro">
			&nbsp;
		</td>
		<td width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top">
			<span class="principalLabel popupFuncSuperior">
				<bean:message key="prompt.area"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
			</span>
			<select name="area" id="area" onchange="buscaFuncByArea()" class="principalObjForm popupFuncSuperior">
				<option value=""><bean:message key="prompt.selecione_uma_opcao"/></option>
				<%
				for(Iterator i = areaOpts.iterator(); i.hasNext();){
					CsCdtbAreaAreaVo areaVo = (CsCdtbAreaAreaVo)i.next();
				%>
					<option value="<%=areaVo.getIdAreaCdArea() %>"><%=areaVo.getAreaDsArea() %></option>
				<%
				}
				%>
			</select>
		</td>
		<td width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr>
		<td class="principalBgrQuadro">
			&nbsp;
		</td>
		<td width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top">
			<span class="principalLabel popupFuncSuperior">
				<bean:message key="prompt.funcionario"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
			</span>
			<select name="funcSuperior" id="funcSuperior" class="principalObjForm popupFuncSuperior">
				<option value=""><bean:message key="prompt.selecione_uma_opcao"/></option>
			</select>
		</td>
		<td width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr> 
	<tr>
		<td class="principalBgrQuadro">
			&nbsp;
		</td>
		<td width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr> 
		<td width="100%"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr> 
</table>
<table border="0" cellspacing="0" cellpadding="4"> 
	<tr> 
		<td class="popupFuncSuperior"> 
			<img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="window.close()" class="geralCursoHand">
		 
			<img src="/plusoft-resources/images/botoes/bt_confirmar2.gif" width="25" height="25" border="0" alt="Confirmar" onClick="confirmarSuperior()" class="geralCursoHand" style="margin-left:260px;">
			<span class="principalLabel" style="margin:5px 0 0 2px; cursor: pointer;" onClick="confirmarSuperior()">
				<bean:message key="prompt.confirm"/>
			</span>
		</td>
	</tr> 
</table>


</body>
</html>

