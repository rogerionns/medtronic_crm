<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
//Chamado: 86446 - 21/01/2013
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>

<head></head>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<link rel="stylesheet" href="webFiles/caixapostal/caixapostal.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
 	function desabilitaCampos(){
 		document.administracaoCsCdtbCaixaPostalCapoForm.capoNrSequencia.disabled= true;
		document.administracaoCsCdtbCaixaPostalCapoForm.idServCdServico.disabled= true;	
		//document.administracaoCsCdtbCaixaPostalCapoForm.capoDsServidor.disabled= true;
		//document.administracaoCsCdtbCaixaPostalCapoForm.capoDsBancoDados.disabled= true;
		document.administracaoCsCdtbCaixaPostalCapoForm.capoDhInativo.disabled= true;
		document.administracaoCsCdtbCaixaPostalCapoForm.idAsmeCdAssuntoMail.disabled= true;
	}	
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbCaixaPostalCapoForm" action="/AdministracaoCsCdtbCaixaPostalCapo.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<br>
	 <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="capoNrSequencia" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="26%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.servico"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
		      	<html:select property="idServCdServico" styleClass="principalObjForm" > 
		          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
		          <html:options collection="CsCdtbServicoServVector" property="idServCdServico" labelProperty="servDsServico"/> 
		        </html:select>   
          </td>
          <td width="26%">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.assuntoEMail"/>
          	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
          </td>
          <td colspan="2">
		      	<html:select property="idAsmeCdAssuntoMail" styleClass="principalObjForm" > 
		          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
		          <html:options collection="csCdtbAssuntoMailAsmeVector" property="idAsmeCdAssuntoMail" labelProperty="asmeDsAssuntoMail"/> 
		        </html:select> 
          </td>
          <td>&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="4" class="espacoPqn">&nbsp;</td>
        </tr>
        
        <tr>
        	<td colspan="4">
        		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        			<tr>
        				<td width="50%">
        					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalPstQuadroLinkNormal"><bean:message key="prompt.capo.recebimento"/></td>
									<td class="principalLabel">&nbsp;</td>
								</tr>
							</table>
							<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
								<tr>
						        	<td colspan="2" class="espacoPqn">&nbsp;</td>
						        </tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.servidor"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsServidorRec" styleClass="text" maxlength="100" style="width:250px"/></td> <!--Chamado: 85185 - 31/10/2012 - Carlos Nunes -->
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.porta"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsPortaRec" styleClass="text" maxlength="10" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.protocolo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsProtocoloRec" styleClass="text" maxlength="10" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.usuario"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsUsuarioRec" styleClass="text" maxlength="100" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.senha"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:password property="capoDsSenhaRec" styleClass="text" maxlength="100" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.pool"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsPoolRec" styleClass="text" maxlength="100" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.outros.parametros"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:textarea property="capoDsBancodadosRec" onkeypress="textCounter(this,'2000')" onblur="textCounter(this,'2000')" styleClass="text" rows="2" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.apagar.email"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:checkbox property="capoInApagaremail"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.debug"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:checkbox property="capoInDebugRec"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.ssl"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:checkbox property="capoInSslRec"/></td>
								</tr>
								<tr>
									<td class="btnreceber" width="30%" align="left" title="<plusoft:message key='prompt.validar.servidor'/>"><img src="webFiles/Menu/images/mvert/bt_Agente.gif"></td>
									<td>&nbsp;</td>
								</tr>
								
							</table>
							
        				</td>
        				<td width="50%">
        				
        					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalPstQuadroLinkNormal"><bean:message key="prompt.capo.envio"/></td>
									<td class="principalLabel">&nbsp;</td>
								</tr>
							</table>
							<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
								<tr>
						        	<td colspan="2" class="espacoPqn">&nbsp;</td>
						        </tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.servidor"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsServidorEnv" styleClass="text" maxlength="100" style="width:250px"/></td> <!--Chamado: 85185 - 31/10/2012 - Carlos Nunes -->
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.porta"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsPortaEnv" styleClass="text" maxlength="10" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.protocolo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsProtocoloEnv" styleClass="text" maxlength="10" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.usuario"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsUsuarioEnv" styleClass="text" maxlength="100" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.senha"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:password property="capoDsSenhaEnv" styleClass="text" maxlength="100" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.pool"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsPoolEnv" styleClass="text" maxlength="100" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.outros.parametros"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:textarea property="capoDsBancodadosEnv" onkeypress="textCounter(this,'2000')" onblur="textCounter(this,'2000')" styleClass="text" rows="2" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.email.resposta"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:text property="capoDsEmailresposta" styleClass="text" maxlength="255" style="width:250px"/></td>
								</tr>
								<tr>
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.debug"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:checkbox property="capoInDebugEnv"/></td>
								</tr>
								<tr style="display:none">
									<td class="principalLabel" width="30%" align="right"><plusoft:message key="prompt.capo.ssl"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
									<td><html:checkbox property="capoInSslEnv"/></td>
								</tr>
								<tr>
									<td class="btnenviar" width="30%" align="left" title="<plusoft:message key='prompt.validar.servidor'/>"><img src="webFiles/Menu/images/mvert/bt_Agente.gif"></td>
									<td>&nbsp;</td>
								</tr>

							</table>
							
        				</td>
        			</tr>
        		</table>
        	
        	
        	
        	</td>
        </tr>
        
        <tr> 
          <td width="18%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="capoDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="26%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>

<script>
if('<%=request.getAttribute("gravou")%>' == 'true' || '<%=request.getAttribute("excluiu")%>' == 'true'){
	parent.cancel();
}
</script>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			//document.administracaoCsCdtbCaixaPostalCapoForm.capoDsServidor.disabled= true;	
			document.administracaoCsCdtbCaixaPostalCapoForm.capoDhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_CAIXAPOSTAL_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbCaixaPostalCapoForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_CAIXAPOSTAL_INCLUSAO_CHAVE%>')){
				desabilitaCampos();
			}
			/*
			document.administracaoCsCdtbCaixaPostalCapoForm.idServCdServico.disabled= false;
			document.administracaoCsCdtbCaixaPostalCapoForm.idServCdServico.value= '';
			document.administracaoCsCdtbCaixaPostalCapoForm.idServCdServico.disabled= true;
			*/
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_CAIXAPOSTAL_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_CAIXAPOSTAL_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbCaixaPostalCapoForm.imgGravar);	
				desabilitaCampos();
			}
		</script>
</logic:equal>
</html>

<script>
var essa_acao_poder_demorar = '<plusoft:message key="prompt.confirm.essa_acao_poder_demorar" />';
var resultadoValidacao = '<bean:message key="prompt.resultado.validacao" />';
var opcaoValidacao = '<bean:message key="prompt.opcao.validacao" />';
</script>

<!-- Chamado: 86446 - 21/01/2013 -->
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
<script language="JavaScript" src="webFiles/caixapostal/caixapostal.js"></script>