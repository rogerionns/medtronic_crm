<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

function atualizaCmbRespTab(){
	var url="";
	
	url = "AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%=MAConstantes.TELA_CMB_RESPTABCARACT_PRAS%>";
	url = url + "&acao=incluir";
	url = url + "&csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd=" + administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd'].value;
	
	parent.cmbRespTabIframe.location.href = url;
	
}


</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');atualizaCmbRespTab();">

<html:form styleId="administracaoCsCdtbProdutoAssuntoPrasForm" action="/AdministracaoCsCdtbProdutoAssuntoPras.do">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
		
	<html:select property="csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd" styleClass="principalObjForm" onchange="atualizaCmbRespTab();">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		<html:options collection="linhaCaractVector" property="csCdtbCaracteristicaProdCaprVo.idCaprCdCaracteristicaprod" labelProperty="csCdtbCaracteristicaProdCaprVo.caprDsCaracteristicaprod"/>
	</html:select>

</html:form>
</body>
</html>