<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
</head>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>

<script>
function atualizaPesquisa() {
	oldAcao = document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value;
	oldTela = document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value;
	oldTarget = document.administracaoCsCdtbProdutoAssuntoPrasForm.target;
	document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
	document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_PESQ %>';
	document.administracaoCsCdtbProdutoAssuntoPrasForm.target = ifrmCmbPesq.name;
	document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();
	document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value = oldAcao;
	document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = oldTela;
	document.administracaoCsCdtbProdutoAssuntoPrasForm.target = oldTarget;
}

function verificaProduto(){

		if(document.administracaoCsCdtbProdutoAssuntoPrasForm.prasInProdutoAssunto.checked == false){
			document.administracaoCsCdtbProdutoAssuntoPrasForm.codProd.disabled= true;	
			document.administracaoCsCdtbProdutoAssuntoPrasForm.contaProd.disabled= true;	
			document.administracaoCsCdtbProdutoAssuntoPrasForm.prasVlUnitario.disabled= true;	
			document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDhLancamento.disabled= true;	
			document.administracaoCsCdtbProdutoAssuntoPrasForm.prasInAcessorio.disabled= true;	
			document.administracaoCsCdtbProdutoAssuntoPrasForm.idTpprCdTpproduto.disabled= true;																			
			document.administracaoCsCdtbProdutoAssuntoPrasForm.receptiva.disabled= true;
			
			<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
            	document.administracaoCsCdtbProdutoAssuntoPrasForm.ativa.disabled= true;
			<%} %>
			
			document.administracaoCsCdtbProdutoAssuntoPrasForm.questionario.disabled= true;
			ifrmCmbPesq.document.administracaoCsCdtbProdutoAssuntoPrasForm.idPesqCdPesquisa.disabled= true;	
			
		}
		else{
			document.administracaoCsCdtbProdutoAssuntoPrasForm.codProd.disabled= false;	
			document.administracaoCsCdtbProdutoAssuntoPrasForm.contaProd.disabled= false;	
			document.administracaoCsCdtbProdutoAssuntoPrasForm.prasVlUnitario.disabled= false;	
			document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDhLancamento.disabled= false;	
			document.administracaoCsCdtbProdutoAssuntoPrasForm.prasInAcessorio.disabled= false;	
			document.administracaoCsCdtbProdutoAssuntoPrasForm.idTpprCdTpproduto.disabled= false;
			document.administracaoCsCdtbProdutoAssuntoPrasForm.receptiva.disabled= false;
			
			<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
            	document.administracaoCsCdtbProdutoAssuntoPrasForm.ativa.disabled= false;
			<%} %>
			
			document.administracaoCsCdtbProdutoAssuntoPrasForm.questionario.disabled= false;
			ifrmCmbPesq.document.administracaoCsCdtbProdutoAssuntoPrasForm.idPesqCdPesquisa.disabled= false;			
		}
}	

function VerificaCalendario(){
	if(document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDhLancamento.disabled==false){
		show_calendar('administracaoCsCdtbProdutoAssuntoPrasForm.prasDhLancamento');
	}
}

function inicio(){
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">		
			desabilitaCamposProdutoAssunto();	
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);		
	</logic:equal>

	showError('<%=request.getAttribute("msgerro")%>');
	verificaPermissao();
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
		document.administracaoCsCdtbProdutoAssuntoPrasForm.idLinhCdLinha.value=1;
	<%}%>
	
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value + ";" + administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value);
	
/*	document.getElementById("caractIframe").src = "AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%=MAConstantes.TELA_CARACTERISTICA_PRAS%>&acao=incluir"+
		"&idAsn1CdAssuntonivel1="+ administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value +
		"&idAsn2CdAssuntonivel2="; administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value;*/

	//Action Center 
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">		
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.disabled= true;
		ifrmCmbAssuntoNivel2Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.disabled= true;

		ifrmCmbAssuntoNivel1Pras.desabilitarBotaoBuscaProd();	
		ifrmCmbAssuntoNivel2Pras.desabilitarBotaoBuscaProd();
	<%}%>
	</logic:equal>
}

function verificaPermissao(){
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_PRODUTOASSUNTO_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_PRODUTOASSUNTO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbProdutoAssuntoPrasForm.imgGravar);		
			desabilitaCamposProdutoAssunto();
		}
	</logic:equal>
}

function desabilitaCamposProdutoAssunto(){
	try{
		document.administracaoCsCdtbProdutoAssuntoPrasForm.idLinhCdLinha.disabled= true;	
		document.administracaoCsCdtbProdutoAssuntoPrasForm.idAreaCdArea.disabled= true;	
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDsProdutoAssunto.disabled= true;	
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasInProdutoAssunto.disabled= true;	
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasInInformacao.disabled= true;																			
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasInManifestacao.disabled= true;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDsLink.disabled= true;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasInDescontinuado.disabled= true;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDhInativo.disabled= true;
		ifrmCmbPesq.document.administracaoCsCdtbProdutoAssuntoPrasForm.idPesqCdPesquisa.disabled= true;	
		document.administracaoCsCdtbProdutoAssuntoPrasForm.codProd.disabled= true;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.contaProd.disabled= true;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasVlUnitario.disabled= true;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDhLancamento.disabled= true;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.receptiva.disabled= true;
		
		<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
         <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
           	document.administracaoCsCdtbProdutoAssuntoPrasForm.ativa.disabled= true;
		<%} %>
		
		document.administracaoCsCdtbProdutoAssuntoPrasForm.questionario.disabled= true;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.idTpprCdTpproduto.disabled= true;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasInAcessorio.disabled= true;
		administracaoCsCdtbProdutoAssuntoPrasForm.idMafoCdMarcaFornecedor.disabled= true;
		
		ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.disabled= true;
		ifrmCmbAssuntoNivel2Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.disabled= true;
		//administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.disabled= true;
		//administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.disabled= true;

	}
	catch(e){
		setTimeout("desabilitaCamposProdutoAssunto();",300);			
	}
}

function setFunction(cRetorno, campo){
	if(campo=="document.administracaoCsCdtbProdutoAssuntoPrasForm.prasTxObservacao"){
		document.getElementById("divConteudoDocumento").innerHTML = cRetorno;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.prasTxObservacao.value=cRetorno;
	}
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 


function AtivarPasta(pasta){
  try {
	//tab = pasta;
	switch (pasta){
		case 1:
			MM_showHideLayers('layerCaracteristica','','show','layerProduto','','hide','layerImagem','','hide','layerObservacao','','hide');
			SetClassFolder('tdCaracteristica','principalPstQuadroLinkSelecionadoGrande');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormalGrande');
			SetClassFolder('tdObservacao','principalPstQuadroLinkNormalGrande');
			SetClassFolder('tdImagem','principalPstQuadroLinkNormalGrande');			
			break;
		case 2:
			MM_showHideLayers('layerProduto','','show','layerCaracteristica','','hide','layerImagem','','hide','layerObservacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionadoGrande');
			SetClassFolder('tdCaracteristica','principalPstQuadroLinkNormalGrande');	
			SetClassFolder('tdObservacao','principalPstQuadroLinkNormalGrande');
			SetClassFolder('tdImagem','principalPstQuadroLinkNormalGrande');			
			//geralImgHidden
			break;
		case 3:
			MM_showHideLayers('layerImagem','','show','layerProduto','','hide','layerCaracteristica','','hide','layerObservacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormalGrande');
			SetClassFolder('tdCaracteristica','principalPstQuadroLinkNormalGrande');
			SetClassFolder('tdObservacao','principalPstQuadroLinkNormalGrande');
			SetClassFolder('tdImagem','principalPstQuadroLinkSelecionadoGrande');			
			break;
		case 4:
			MM_showHideLayers('layerObservacao','','show','layerImagem','','hide','layerProduto','','hide','layerCaracteristica','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormalGrande');
			SetClassFolder('tdCaracteristica','principalPstQuadroLinkNormalGrande');
			SetClassFolder('tdObservacao','principalPstQuadroLinkSelecionadoGrande');
			SetClassFolder('tdImagem','principalPstQuadroLinkNormalGrande');			
			break;			
	}
	//eval(stracao);
  }catch(e){
	//alert (e.description);
  }
}

function montaDadosCaracteristica(){
	var str = "";
	var objWin = caractIframe.lstCaractIframe;
	var objForm = objWin.document.administracaoCsCdtbProdutoAssuntoPrasForm;
	
	if (objWin.result > 0){
		if (objWin.result == 1){
			str = str + "<input type=\"hidden\" name=\"idPrcaCdProdutoCaract\" value=\"" + objForm.idPrcaCdProdutoCaract.value + "\">";
			str = str + "<input type=\"hidden\" name=\"idCaprCdCaracteristicaProd\" value=\"" + objForm.idCaprCdCaracteristicaProd.value + "\">";
			str = str + "<input type=\"hidden\" name=\"idRtcpRespTabCaractProd\" value=\"" + objForm.idRtcpRespTabCaractProd.value + "\">";
		}else{
			for (i=0;i<objForm.idCaprCdCaracteristicaProd.length;i++){
				str = str + "<input type=\"hidden\" name=\"idPrcaCdProdutoCaract\" value=\"" + objForm.idPrcaCdProdutoCaract[i].value + "\">";
				str = str + "<input type=\"hidden\" name=\"idCaprCdCaracteristicaProd\" value=\"" + objForm.idCaprCdCaracteristicaProd[i].value + "\">";
				str = str + "<input type=\"hidden\" name=\"idRtcpRespTabCaractProd\" value=\"" + objForm.idRtcpRespTabCaractProd[i].value + "\">";
			}
		}
		
		window.document.getElementById("layerCategoria").innerHTML = str;
	}			
}

function carregarCmbVariedade(){
	ifrmCmbAssuntoNivel2Pras.document.location = "AdministracaoCsCdtbProdutoAssuntoPras.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_ASSUNTONIVEL2_PRAS %>&idAsn2CdAssuntoNivel2=<bean:write name='baseForm' property='idAsn2CdAssuntoNivel2' />";
}

function carregarCmbProduto(){
	ifrmCmbAssuntoNivel1Pras.document.location = "AdministracaoCsCdtbProdutoAssuntoPras.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_ASSUNTONIVEL1_PRAS %>&idAsn1CdAssuntoNivel1=<bean:write name='baseForm' property='idAsn1CdAssuntoNivel1' />";
}

</script>
<body class= "principalBgrPageIFRM" onload="inicio();">
<html:form styleId="administracaoCsCdtbProdutoAssuntoPrasForm" action="/AdministracaoCsCdtbProdutoAssuntoPras.do"> 
<html:hidden property="modo" /> 
<html:hidden property="acao" /> 
<html:hidden property="tela" /> 
<html:hidden property="erro" /> 
<html:hidden property="topicoId" /> 
<html:hidden property="CDataInativo" />
<html:hidden property="inativarComp"/>
<html:hidden property="idPesqCdPesquisa"/>

<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("N")) {%>
	<html:hidden property="idAsn1CdAssuntoNivel1"/>
	<html:hidden property="idAsn2CdAssuntoNivel2" value="1"/>
<%}
else{%>
	<html:hidden property="idAsn1CdAssuntoNivel1"/>
	<html:hidden property="idAsn2CdAssuntoNivel2"/>
<%} %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="principalLabel">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0" class="principalLabel">
        <tr> 
          <td width="23%"> 
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
            	<div align="right" style="visibility: hidden">
            <%}else{%>
            	<div align="right">
            <%}%>
            	<%= getMessage("prompt.linha", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
            </div>
          </td>
          <td colspan="3">
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
            	<div style="visibility: hidden">
            <%}else{%>
            	<div>
            <%}%>
	       	<html:select property="idLinhCdLinha" styleClass="principalObjForm" onchange="caractIframe.atualizaCmbCaracteristica();"> 
	            <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
	            <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/> 
	        </html:select>                              
	        </div>
          </td>
          <td width="12%">&nbsp;</td>
        </tr>
      	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		<tr> 
          <td width="23%"> 
            <div align="right"><%= getMessage("prompt.assuntoNivel1", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td colspan="3">
			<iframe id="ifrmCmbAssuntoNivel1Pras" name="ifrmCmbAssuntoNivel1Pras" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
          	<script>
          		carregarCmbProduto();
          	</script>                         
          </td>
          <td width="12%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="23%"> 
            <div align="right"><%= getMessage("prompt.assuntoNivel2", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td colspan="3">
          	<iframe id="ifrmCmbAssuntoNivel2Pras" name="ifrmCmbAssuntoNivel2Pras" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
          	<script>
          		carregarCmbVariedade();
          	</script>
          </td>
          <td width="12%">&nbsp;</td>
        </tr>
        <%}%>
        <tr> 
          <td width="23%"> 
            <div align="right"><bean:message key="prompt.area"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td colspan="3">
	       	<html:select property="idAreaCdArea" styleClass="principalObjForm" > 
	            <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
	            <html:options collection="csCdtbAreaAreaVector" property="idAreaCdArea" labelProperty="areaDsArea"/> 
	        </html:select>            
          </td>
          <td width="12%">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="23%"> 
            <div align="right"><bean:message key="prompt.marca"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td colspan="3">
	       	<html:select property="idMafoCdMarcaFornecedor" styleClass="principalObjForm" > 
	            <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
	            <html:options collection="csCdtbMarcafornecedorMafoVector" property="idMafoCdMarcaFornecedor" labelProperty="mafoDsMarcaFornecedor"/> 
	        </html:select>            
          </td>
          <td width="12%">&nbsp;</td>
        </tr>
        
        <tr id="tdVariedade"> 
          <td width="23%"> 
            <div align="right"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td colspan="3">
          	<html:text property="prasDsProdutoAssunto" styleClass="text" maxlength="60" /> 	
          </td>
          <td width="12%">&nbsp;</td>
        </tr>
        
        <script>
        	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
        		document.getElementById("tdVariedade").style.display = "none";
        	<%}%>
        </script>
        
        <tr> 
          <td width="23%"> 
            <div align="right"><bean:message key="prompt.link"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td colspan="3"> 
          	<html:text property="prasDsLink" styleClass="text" maxlength="200" /> 	
          </td>
          <td width="12%">&nbsp;</td> 
        </tr>
        <tr> 
          <td width="23%"></td>
          <td>
          	<html:checkbox value="true" property="prasInInformacao"/> <bean:message key="prompt.informacao"/>
          </td>
          <td width="12%">&nbsp;</td>
          <td>
          	<html:checkbox value="true" property="prasInManifestacao"/> <bean:message key="prompt.manifestacaofixo"/>
          </td>
          <td width="12%">&nbsp;</td>
          
        </tr>
        <tr> 
          <td width="23%"></td>
          <td>
          	<html:checkbox value="true" property="prasInProdutoAssunto" onclick=""/> <bean:message key="prompt.produto"/>
          </td>
          <td width="12%">&nbsp;</td>
          <td>
          	<% // 91850 - 04/11/2013 - Jaider Alba %>
          	<html:checkbox value="true" property="prasInWeb"/> <bean:message key="prompt.adm.web"/>
          </td>
          <td width="12%">&nbsp;</td>

        </tr>
        <tr> 
          <td width="23%"> 
            <div align="right"></div>
          </td>
          <td >
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
<!-- Pedido do ZECA colocar flag CONSUMIDOR -->
          	<html:checkbox value="true" property="prasInDescontinuado"/> <bean:message key="prompt.descontinuado"/>
<%}else{%>
			<div style="visibility: hidden">
				<html:checkbox value="true" property="prasInDescontinuado"/> <bean:message key="prompt.descontinuado"/>
			</div>
<%}%>
          </td>
          <td width="12%">&nbsp;</td>
          <td>
          	<html:checkbox value="true" property="prasDhInativo"/> <bean:message key="prompt.inativo"/>
          </td>
          <td width="12%">&nbsp;</td>
        </tr>
        <tr>
          <td width="23%" class="principalLabel">&nbsp;</td>
          <td colspan="3" class="principalLabel">&nbsp;</td>
          <td width="12%" class="principalLabel">&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="5">
       			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td valign="top" colspan="2" align="center">
							<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								
								<tr>
									<td class="principalPstQuadroLinkVazio">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalPstQuadroLinkSelecionadoGrande" id="tdDestinatario" name="tdDestinatario" onClick="AtivarPasta(2);">
													<bean:message key="prompt.produto"/>
												</td>
												<td class="principalPstQuadroLinkNormalGrande" id="tdObservacao" name="tdObservacao" onClick="AtivarPasta(4);">
													<bean:message key="prompt.infAdicionais"/>
												</td>
												<td class="principalPstQuadroLinkNormalGrande" id="tdCaracteristica" name="tdCaracteristica" onClick="AtivarPasta(1);">
													<bean:message key="prompt.caracteristicaProduto"/>
												</td>
												<logic:equal name="baseForm" property="acao" value="<%=Constantes.ACAO_EDITAR%>">
													<td class="principalPstQuadroLinkNormalGrande" id="tdImagem" name="tdImagem" onClick="AtivarPasta(3);">
														<bean:message key="prompt.imagens"/>
													</td>
												</logic:equal>	
											</tr>
										</table>
									</td>
									<td width="4" colspan="4">
										<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
									</td>
								</tr>
								<tr>
									<td height="130" class="principalBgrQuadro" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<div id="layerCaracteristica" style="position:absolute; width:99%; height:100px; z-index:1; visibility: hidden">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td valign="top">
																<iframe id=caractIframe name="caractIframe" src="" width="640px" height="100px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
																<!-- AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%=MAConstantes.TELA_CARACTERISTICA_PRAS%>&acao=incluir -->
																<script language="JavaScript">
																	var url;
																	url = "AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%=MAConstantes.TELA_CARACTERISTICA_PRAS%>&acao=incluir"+
																		"&idAsn1CdAssuntonivel1="+ administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value +
																		"&idAsn2CdAssuntonivel2="+ administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value;
																	
																	caractIframe.document.location = url;
																//	document.getElementById("caractIframe").src = url;
																</script>
															<td>
														<tr>
													</table>	
												</div> 
												<div id="layerImagem" style="position:absolute; width:99%; height:100px; z-index:1; visibility: hidden">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td><iframe id=imagemIframe name="imagemIframe" src="AdministracaoCsCdtbProdutoImagemPrim.do?tela=<%=MAConstantes.TELA_IMAGEM_PRAS%>" width="635px" height="100px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe><td>											
														<tr>
													</table>	
												</div>
												<div id="layerObservacao" style="position:absolute; width:635px; height:100px; z-index:1; visibility: hidden">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
													<td class="principalLabel">&nbsp;</td>
													</tr>
													</table>
													<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left:15px; ">
														<tr>
															<td class="principalLabel" > 
																<html:hidden property="prasTxObservacao"/>
																<div id="divConteudoDocumento" style="position:relative; overflow: auto; width: 570px; height: 80px; background-color: #FFFFFF; border: 1px none #000000; display: block;"> 
															        <script>
															        document.getElementById("divConteudoDocumento").innerHTML = "";
															    	document.write(document.administracaoCsCdtbProdutoAssuntoPrasForm.prasTxObservacao.value);
															       </script>
      															</div>
															<td>
															<td width="5%"> 
      															<img src="webFiles/images/botoes/bt_CriarCarta.gif" name="imgEditor" width="25" height="22" class="geralCursoHand" border="0" title="<bean:message key="prompt.tituloCorresp"/>" onClick="MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=document.administracaoCsCdtbProdutoAssuntoPrasForm.prasTxObservacao&carta=true','Documento','width=850,height=494,top=0,left=0')"> 
   															</td>											
														</tr>
														<tr>
															<td class="principalLabel" colspan="2"> 
																<%= getMessage("prompt.produtoAssuntoCompleto", request)%> 
															<td>																									
														</tr>
														<tr>
															<td class="principalLabel" colspan="2"> 
																<html:text property="prasDsProdutoAssuntoCompl" styleClass="text" maxlength="255" /> 
															<td>																									
														</tr>
													</table>	
												</div> 
												<div id="layerProduto" style="position:absolute; width:635px; height:100px; z-index:1; visibility: visible"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
												   <tr> 
													  <td width="15%" class="principalLabel"> 
														<div align="right"><%=getMessage("prompt.Cod_Prod", request) %> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
													  </td>
													  <td width="23%"> 
														  <html:text property="codProd" styleClass="text" maxlength="20" /> 	
													  </td>
													  <td width="19%" class="principalLabel"> 
														<div align="right"><%=getMessage("prompt.ContaProd", request) %><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
													  </td>
													  <td> 
														  <table width="100%" border="0" cellspacing="2" cellpadding="0" > 
															  <tr>
																  <td width="100%"><html:text property="contaProd" styleClass="text" maxlength="20" /></td>
															  </tr>
														  </table> 	
													  </td>
													  <td width="12%">&nbsp;</td>
													</tr>
													<tr> 
													  <td width="15%" class="principalLabel"> 
														<div align="right"><bean:message key="prompt.valorUnitario"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
													  </td>
													  <td width="23%">
														  <html:text property="prasVlUnitario" styleClass="text" maxlength="10" onkeydown="return isDigitoVirgula(event);" onblur="numberValidate(this, 2, '.', ',', event);"/> 	
													  </td>
													  <td width="23%" class="principalLabel"> 
														<div align="right"><bean:message key="prompt.dataLancamento"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
													  </td>
													  <td>
														  <table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
															  <td width="90%"><html:text property="prasDhLancamento" styleClass="text" maxlength="10" onblur="verificaData(this)" onkeydown="return validaDigito(this, event)" />  
															  </td>
															  <td width="10%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" onclick="VerificaCalendario()" class="principalLstParMao" ></td>
															</tr>
														  </table>    
													  </td>
													  
													  <td>&nbsp;</td>
													</tr>
													<!-- Pesquisa -->    
													<tr> 
													  <td width="15%" class="principalLabel" valign="bottom" align="right"> 
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
														 <tr> 
														  <td align="right" class="principalLabel">
															<bean:message key="prompt.pesquisa"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
														 </tr>
														 <tr> 
														  <td height="4px"></td>
														 </tr>
														</table>
													  </td>
													  <td colspan="3"> 
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
														 <tr> 
														  <td align="right" width="2%">
															<html:checkbox value="true" property="receptiva" onclick="atualizaPesquisa();"/> 
														  </td>
														  <td class="principalLabel" width="10%">
															<div align="left">&nbsp;<bean:message key="prompt.receptiva"/></div>
														  </td>
														  <td class="principalLabel" width="2%" align="right">
															<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
															<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
																<html:checkbox value="true" property="ativa" onclick="atualizaPesquisa();"/>  
															<%}%>
														  </td>
														  <td class="principalLabel" width="8%" align="left">
														  	<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
															<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
																&nbsp;<bean:message key="prompt.ativa"/>
															<%}%>
														  </td>
														  <td class="principalLabel" width="2%" align="right">
															<html:checkbox value="true" property="questionario" onclick="atualizaPesquisa();"/> 
														  </td>
														  <td class="principalLabel" width="10%" align="left">
															&nbsp;<bean:message key="prompt.questionario"/>
														  </td>
														 </tr>
													  </table>
													  <iframe id=ifrmCmbPesq name="ifrmCmbPesq" src="AdministracaoCsCdtbProdutoAssuntoPras.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRMA_PESQ %>&idPesqCdPesquisa=<bean:write name='administracaoCsCdtbProdutoAssuntoPrasForm' property='idPesqCdPesquisa'/>&receptiva=<bean:write name='administracaoCsCdtbProdutoAssuntoPrasForm' property='receptiva'/>&ativa=<bean:write name='administracaoCsCdtbProdutoAssuntoPrasForm' property='ativa'/>&questionario=<bean:write name='administracaoCsCdtbProdutoAssuntoPrasForm' property='questionario'/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
													</td>
												   <td width="12%">&nbsp;</td>
												  </tr>
													<!-- Final da pesquisa -->
												
													<!-- Tipo de Produto -->
													<tr> 
													  <td width="15%" class="principalLabel"> 
														<div align="right"><bean:message key="prompt.tipoProduto"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
													  </td>
													  <td colspan="3">
														   <html:select property="idTpprCdTpproduto" styleClass="principalObjForm" > 
															<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
															<html:options collection="csCdtbTpprTpprodutoVector" property="idTpprCdTpproduto" labelProperty="tpprDsTpproduto"/> 
														</html:select>                              
													  </td>
													  <td width="12%">&nbsp;</td>
													</tr>
													<!-- Final Tipo de Produto -->
												
													<tr>
													  <td width="15%">&nbsp;</td>
													  <td colspan="3" class="principalLabel">
														  <html:checkbox value="true" property="prasInAcessorio"/> <bean:message key="prompt.acessorio"/>
													  </td>
													  <td width="12%">&nbsp;</td>
													</tr>
												</table>
												</div>
											</td>
										</tr>
									</table>
									</td>
									<td width="12%" class="principalBgrQuadro" align="right">&nbsp;</td>
									
								</tr>
								<tr>
									<td width="100%" height="5"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
									<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
								</tr>
							</table>
						</td>
					</tr>		
			   </table>
		  </td>
		</tr>
</table>

<div id="layerCategoria"></div>

</html:form> 
</body>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_PRODUTOASSUNTO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbProdutoAssuntoPrasForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_PRODUTOASSUNTO_INCLUSAO_CHAVE%>')){
				desabilitaCamposProdutoAssunto();
			}
			/*
			document.administracaoCsCdtbProdutoAssuntoPrasForm.idTpmaCdTpManifestacao.disabled= false;
			document.administracaoCsCdtbProdutoAssuntoPrasForm.idTpmaCdTpManifestacao.value= '';
			document.administracaoCsCdtbProdutoAssuntoPrasForm.idTpmaCdTpManifestacao.disabled= true;
			*/	
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbProdutoAssuntoPrasForm.idLinhCdLinha.focus();}
	catch(e){}
</script>