<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<OBJECT id=colorChooser CLASSID="clsid:3050f819-98b5-11cf-bb82-00aa00bdce0b" width="0px" height="0px"></OBJECT>

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

	function chooseColor(){
		showModalDialog("webFiles/colorChooser.jsp", window, "dialogHeight: 350px; dialogWidth: 400px");
	}
	
	function atualizarCor(cor){
		administracaoCsCdtbClassifmaniClmaForm.clmaDsCor.value = cor;
		document.getElementById("tdCor").bgColor = cor;
	}

	function desabilitaCamposClassfmani(){
		document.administracaoCsCdtbClassifmaniClmaForm.clmaDsClassifmanif.disabled= true;	
		document.administracaoCsCdtbClassifmaniClmaForm.clmaNrSequencia.disabled= true;
		document.administracaoCsCdtbClassifmaniClmaForm.clmaDhInativo.disabled= true;
		document.administracaoCsCdtbClassifmaniClmaForm.clmaDsCor.disabled= true;	
		document.administracaoCsCdtbClassifmaniClmaForm.clmaCdCorporativo.disabled= true;	
	}

	function inicio(){
		setaAssociacaoMultiEmpresa();
		atualizarCor(administracaoCsCdtbClassifmaniClmaForm.clmaDsCor.value);
		setaChavePrimaria(administracaoCsCdtbClassifmaniClmaForm.idClmaCdClassifmanif.value);
	}
	
</script>
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbClassifmaniClmaForm" action="/AdministracaoCsCdtbClassifmaniClma.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>		
	<input type="hidden" name="limparSessao" value="false"></input>
	
	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="12%"> 
            <html:text property="idClmaCdClassifmanif" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigoCorporativo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="12%"> 
            <html:text property="clmaCdCorporativo" styleClass="text" maxlength="15" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="29%">&nbsp;</td>
        </tr>        
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="clmaDsClassifmanif" styleClass="text" style="width:312px" maxlength="60" /> 
          </td>
          <td width="29%">&nbsp;</td>
        </tr>

        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.cadcor"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="1"> 
            <html:text property="clmaDsCor" styleClass="text" onclick="chooseColor();" maxlength="7" /> 
          </td>
          <td colspan="1" width="12%" name="tdCor" id="tdCor">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.sequencia"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="clmaNrSequencia" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" styleClass="text" maxlength="7" />
            <script>
            document.all('clmaNrSequencia').value = (document.all('clmaNrSequencia').value == '0'?'':document.all('clmaNrSequencia').value);
            </script>
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="12%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="clmaDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposClassfmani();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CLASSIFICACAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbClassifmaniClmaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CLASSIFICACAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposClassfmani();
			}else{
				document.administracaoCsCdtbClassifmaniClmaForm.idClmaCdClassifmanif.disabled= false;
				document.administracaoCsCdtbClassifmaniClmaForm.idClmaCdClassifmanif.value= '';
				document.administracaoCsCdtbClassifmaniClmaForm.clmaNrSequencia.disabled= false;
				document.administracaoCsCdtbClassifmaniClmaForm.idClmaCdClassifmanif.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CLASSIFICACAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CLASSIFICACAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbClassifmaniClmaForm.imgGravar);	
				desabilitaCamposClassfmani();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbClassifmaniClmaForm.clmaDsClassifmanif.focus();}
	catch(e){}
</script>