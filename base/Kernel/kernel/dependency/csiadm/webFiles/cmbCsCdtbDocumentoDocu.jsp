<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>
	
	<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
	<script language="JavaScript">
		function acaoCmbDocumento(){
			parent.document.administracaoCsCatbDocumentoAnexoDoanForm.idDocuCdDocumento.value = administracaoCsCatbDocumentoAnexoDoanForm.idDocuCdDocumento.value;
			parent.MontaLista();
		}
	</script>

	<body class="principalBgrPageIFRM" topmargin=0 leftmargin=0 bottommargin=0 rightmargin=0 style="overflow: hidden;">
		<html:form styleId="administracaoCsCatbDocumentoAnexoDoanForm" action="/AdministracaoCsCatbDocumentoAnexoDoan.do">
			<html:hidden property="acao" />
			<html:hidden property="tela" />
			
			<html:select property="idDocuCdDocumento" styleClass="principalObjForm" onchange="acaoCmbDocumento();"> 
				<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/> 
			</html:select> 
		</html:form>
	</body>
</html>