<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

function MontaLista(){
	lstArqCarga.location.href= "AdministracaoCsAstbFuncareavendaFuav.do?tela=administracaoLstCsAstbFuncareavendaFuav&acao=<%=Constantes.ACAO_VISUALIZAR%>&csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario=" + administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value;
}

function desabilitaCamposProcinstrucao(){
	administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].disabled= true;
	administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].disabled= true;
}

function verificaCampo(){
	if(administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInResponsavel'].checked){
		administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInEditar'].checked = true;
		administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInVer'].checked = true;
		administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInEditar'].disabled = true;
		administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInVer'].disabled = true;
	}
	else{
		administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInEditar'].disabled = false;
		administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInVer'].disabled = false;
		
		if(administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInEditar'].checked){
			administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInVer'].checked = true;
		}else{
			administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInVer'].checked = false;	
		}
	}
}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista()">
<html:form styleId="administracaoCsAstbFuncareavendaFuavForm" action="/AdministracaoCsAstbFuncareavendaFuav.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.EquipeDeVendas"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><html:select property="csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario" styleClass="principalObjForm" onchange="MontaLista()"> 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbFuncionarioFuncAreaVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/> 
      </html:select> </td>
    <td width="27%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.funcionarioVendedor"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><html:select property="csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario" styleClass="principalObjForm"  > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbFuncionarioFuncVendedorVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/> 
      </html:select> </td>
    <td width="27%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td colspan="2" class="principalLabel">
 	    <bean:message key="prompt.funcionarioVer"/> 
	   	<html:checkbox value="true" property="csAstbFuncareavendaFuavVo.fuavInVer"/>&nbsp;&nbsp;
        <bean:message key="prompt.funcionarioEditar"/>
    	<html:checkbox value="true" property="csAstbFuncareavendaFuavVo.fuavInEditar" onclick="verificaCampo()" />&nbsp;&nbsp;
 	    <b><bean:message key="prompt.responsavel"/></b> 
	   	<html:checkbox value="true" property="csAstbFuncareavendaFuavVo.fuavInResponsavel" onclick="verificaCampo()"/>
    </td>
  </tr>
  <tr> 
    <td colspan="4" height="220"> 
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="5%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="50%"> <bean:message key="prompt.funcionarioVendedor"/></td>
          <td class="principalLstCab" width="10%" align="center"> <bean:message key="prompt.funcionarioVer"/> </td>
          <td class="principalLstCab" width="10%" align="center"> <bean:message key="prompt.funcionarioEditar"/></td>
          <td class="principalLstCab" width="10%" align="center"> <bean:message key="prompt.responsavel"/></td>
          <td class="principalLstCab" width="15%">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="6" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
			administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].disabled= false;
			administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].disabled= false;
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_FUNCIONARIOVENDEDOR_INCLUSAO_CHAVE%>', parent.administracaoCsAstbFuncareavendaFuavForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_FUNCIONARIOVENDEDOR_INCLUSAO_CHAVE%>')){
				desabilitaCamposProcinstrucao();
			}else{
				administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].disabled= false;
				administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].disabled= false;
			}
			
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_FUNCIONARIOVENDEDOR_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_FUNCIONARIOVENDEDOR_ALTERACAO_CHAVE%>', parent.administracaoCsAstbFuncareavendaFuavForm.imgGravar);	
				desabilitaCamposProcinstrucao();
			}
			
		</script>
</logic:equal>

</html>

<script>
	try{administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].focus();}
	catch(e){}
</script>