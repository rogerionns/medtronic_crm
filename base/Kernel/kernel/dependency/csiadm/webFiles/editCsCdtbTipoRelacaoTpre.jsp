<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposTipoRelacao(){
		document.administracaoCsCdtbTipoRelacaoTpreForm.idTpreCdTipoRelacao.disabled= true;
		document.administracaoCsCdtbTipoRelacaoTpreForm.tpreDsTipoRelacao.disabled= true;	
		document.administracaoCsCdtbTipoRelacaoTpreForm.tpreDhInativo.disabled= true;
	}
	
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbTipoRelacaoTpreForm.idTpreCdTipoRelacao.value);
	}
	
</script>

<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbTipoRelacaoTpreForm" action="/AdministracaoCsCdtbTipoRelacaoTpre.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<input type="hidden" name="limparSessao" value="false"></input>
		<!--Jonathan | Adequa��o para o IE 10-->
	<br>
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idTpreCdTipoRelacao" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="30%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="tpreDsTipoRelacao" styleClass="text" maxlength="60" style="width:300px" /> 
          </td>
          <td width="30%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="19%" align="right" class="principalLabel"><bean:message key="prompt.tipoRelacaoInversa"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:select property="idTpreCdTipoRelacaoInversa" styleClass="principalObjForm" style="width:300px"> 
            	<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
            	<html:options collection="csCdtbTipoRelacaoTpreInversaVector" property="idTpreCdTipoRelacao" labelProperty="tpreDsTipoRelacao"/> 
        	</html:select> 
          </td>
          <td width="30%">&nbsp;</td>
        </tr> 
        <tr> 
          <td width="19%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="19%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="tpreDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="30%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposTipoRelacao();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_TIPODERELACAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbTipoRelacaoTpreForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_TIPODERELACAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposTipoRelacao();
			}
			document.administracaoCsCdtbTipoRelacaoTpreForm.idTpreCdTipoRelacao.disabled= false;
			document.administracaoCsCdtbTipoRelacaoTpreForm.idTpreCdTipoRelacao.value= '';
			document.administracaoCsCdtbTipoRelacaoTpreForm.idTpreCdTipoRelacao.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_TIPODERELACAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_TIPODERELACAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbTipoRelacaoTpreForm.imgGravar);	
				desabilitaCamposTipoRelacao();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbTipoRelacaoTpreForm.tpreDsTipoRelacao.focus();}
	catch(e){}
</script>
