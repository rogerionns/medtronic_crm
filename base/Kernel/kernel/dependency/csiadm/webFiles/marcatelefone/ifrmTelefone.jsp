<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>
</head>

<script>
function submeteExcluirTelefone(idVirtualTelefone, idMafoCdMarcaFornecedor,idTemaCdTelefonemarca) {
	if(confirm('<bean:message key="prompt.Deseja_remover_esse_item" />'))
	{
		ifrmLstTelefone.location.href = "AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_LST_TELEFONE%>&acao=<%= Constantes.ACAO_EXCLUIR%>&idMafoCdMarcaFornecedor=" + idMafoCdMarcaFornecedor + "&idTemaCdTelefonemarca=" + idTemaCdTelefonemarca + "&idVirtualTelefone=" + idVirtualTelefone;
	}
}

	
function submeteFormEditTelefone(idVirtualTelefone, idMafoCdMarcaFornecedor, idTemaCdTelefonemarca, idTpcoCdTpcomunicacao, temaDsDdd, temaDsTelefone, temaDsRamal, temaDsObservacao) {
	
	administracaoCsCdtbMarcafornecedorMafoForm.idVirtualTelefone.value = idVirtualTelefone;
	administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value = idMafoCdMarcaFornecedor;
	administracaoCsCdtbMarcafornecedorMafoForm.idTemaCdTelefonemarca.value = idTemaCdTelefonemarca;
	administracaoCsCdtbMarcafornecedorMafoForm.idTpcoCdTpcomunicacao.value = idTpcoCdTpcomunicacao;
	administracaoCsCdtbMarcafornecedorMafoForm.temaDsDdd.value = temaDsDdd;
	administracaoCsCdtbMarcafornecedorMafoForm.temaDsTelefone.value = temaDsTelefone;
	administracaoCsCdtbMarcafornecedorMafoForm.temaDsRamal.value = temaDsRamal;
	administracaoCsCdtbMarcafornecedorMafoForm.temaDsObservacao.value = temaDsObservacao;
	
	ifrmLstTelefone.location.href = "AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_LST_TELEFONE%>&acao=<%= Constantes.ACAO_EDITAR%>&idMafoCdMarcaFornecedor=" + idMafoCdMarcaFornecedor + "&idTemaCdTelefonemarca=" + idTemaCdTelefonemarca + "&idTpcoCdTpcomunicacao=" + idTpcoCdTpcomunicacao + "&idVirtualTelefone=" + idVirtualTelefone + "&temaDsDdd=" + temaDsDdd + "&temaDsTelefone=" + temaDsTelefone + "&temaDsRamal=" + temaDsRamal + "&temaDsObservacao=" + temaDsObservacao;
}

function limparTela()
{
	administracaoCsCdtbMarcafornecedorMafoForm.idTpcoCdTpcomunicacao.value = "";
	administracaoCsCdtbMarcafornecedorMafoForm.temaDsDdd.value = "";
	administracaoCsCdtbMarcafornecedorMafoForm.temaDsTelefone.value = "";
	administracaoCsCdtbMarcafornecedorMafoForm.temaDsRamal.value = "";
	administracaoCsCdtbMarcafornecedorMafoForm.temaDsObservacao.value = "";					
}


function submeteIncluirTelefone() {
	
	idTpcoCdTpcomunicacao = administracaoCsCdtbMarcafornecedorMafoForm.idTpcoCdTpcomunicacao.value;
	temaDsDdd = administracaoCsCdtbMarcafornecedorMafoForm.temaDsDdd.value;
	temaDsTelefone = administracaoCsCdtbMarcafornecedorMafoForm.temaDsTelefone.value;
	temaDsRamal = administracaoCsCdtbMarcafornecedorMafoForm.temaDsRamal.value;
	temaDsObservacao = administracaoCsCdtbMarcafornecedorMafoForm.temaDsObservacao.value;			
	tpcoDsTpcomunicacao = administracaoCsCdtbMarcafornecedorMafoForm.idTpcoCdTpcomunicacao.options[administracaoCsCdtbMarcafornecedorMafoForm.idTpcoCdTpcomunicacao.selectedIndex].text;		
	administracaoCsCdtbMarcafornecedorMafoForm.tpcoDsTpcomunicacao.value = tpcoDsTpcomunicacao;
	
	
	if(trim(idTpcoCdTpcomunicacao)!= "")
	{
		if(trim(temaDsTelefone)!= "")
		{
			idMafoCdMarcaFornecedor = parent.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value;
			idVirtualTelefone = ifrmLstTelefone.contTelefones;
			idTemaCdTelefonemarca = administracaoCsCdtbMarcafornecedorMafoForm.idTemaCdTelefonemarca.value;	
			
			simaInPrincipal = "";
			
			if(idVirtualTelefone == 0)
			{					
				idVirtualTelefone = 1;	
			}else
				idVirtualTelefone++;
			
			url  = "AdministracaoCsCdtbMarcafornecedorMafo.do";
			url += "?acao=<%= Constantes.ACAO_INCLUIR%>";
			url += "&tela=<%= MAConstantes.IFRM_LST_TELEFONE%>";
			
			url += "&idMafoCdMarcaFornecedor=" + idMafoCdMarcaFornecedor;
			url += "&idTemaCdTelefonemarca=" + idTemaCdTelefonemarca;
			url += "&idVirtualTelefone=" + idVirtualTelefone;
			url += "&idTpcoCdTpcomunicacao=" + idTpcoCdTpcomunicacao;
			url += "&temaDsDdd=" + temaDsDdd;
			url += "&temaDsTelefone=" + temaDsTelefone;
			url += "&temaDsRamal=" + temaDsRamal;
			url += "&tpcoDsTpcomunicacao=" + tpcoDsTpcomunicacao;
			url += "&temaDsObservacao=" + temaDsObservacao;
			
			if(simaInPrincipal != null)
			{
				url += "&simaInPrincipal=" + simaInPrincipal;
			}
			
			ifrmLstTelefone.document.location.href = url;
			
			limparTela();
		}
		else
		{
		//	alert('<bean:message key="prompt.alert.E_necessario_preencher_telefone" />');
			alert('<bean:message key="prompt.alert.E_necessario_preencher_os_campos_telefone_e_tipo" />');
		}
	}
	else
	{
	//	alert('<bean:message key="prompt.alert.E_necessario_selecionar_tipo" />');
		alert('<bean:message key="prompt.alert.E_necessario_preencher_os_campos_telefone_e_tipo" />');
	}
}

</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
	
<html:form action="/AdministracaoCsCdtbMarcafornecedorMafo.do" styleId="administracaoCsCdtbMarcafornecedorMafoForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	
	<html:hidden property="idMafoCdMarcaFornecedor" />
	<html:hidden property="idTemaCdTelefonemarca" />
	<html:hidden property="idVirtualTelefone" />
	<html:hidden property="tpcoDsTpcomunicacao" />	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr> 
             <td>&nbsp;</td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">  
			<tr>                     
             <td width="100%"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                 	<td width="12%" class="principalLabel" align="right">
             		  <bean:message key="prompt.tipo"/> 
             		  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> &nbsp;
             		</td>
                 	<td class="principalLabel">
                      	<html:select property="idTpcoCdTpcomunicacao" styleClass="principalObjForm" style="width:340"> 
                         	<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option> 
                         	<logic:present name="csDmtbTpcomunicacaoTpcoVector"> 
                         		<html:options collection="csDmtbTpcomunicacaoTpcoVector" property="idTpcoCdTpcomunicacao" labelProperty="tpcoDsTpcomunicacao"/> 
                         	</logic:present> 
                         </html:select> 
                    </td>
                 </tr>
               </table>
              </td>
             </tr>
         	<tr>                     
             <td width="100%">  
             	<table width="100%" border="0" cellspacing="0" cellpadding="0">             
                 <tr> 
                 	<td width="12%" class="principalLabel" align="right">
             			<bean:message key="prompt.telefone"/> 
             		  	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> &nbsp;
             		</td>                 	
                    <td width="5" class="principalLabelValorFixo" align="left">
                    	&nbsp;(XX&nbsp;
                    </td>
                    <td width="4%" class="principalLabelValorFixo"> 
                      	<html:text property="temaDsDdd" maxlength="3" styleClass="text" style="text-align:center" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
                    </td>
                    <td width="18" class="principalLabelValorFixo">
                    	&nbsp;)&nbsp;-&nbsp;
                    </td>
                    <td width="20%" class="principalLabelValorFixo"> 
                      	<html:text property="temaDsTelefone" maxlength="20" styleClass="text" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
                    </td>
                    <td class="principalLabel" width="10%" align="right"> 
                      	<bean:message key="prompt.ramal"/> 
                      	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> &nbsp;
                    </td>  
                    <td width="10%" class="principalLabelValorFixo"> 
                      	<html:text property="temaDsRamal" maxlength="10" styleClass="text" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
                    </td>                                       
                    <td width="6%" align="right">
					  
				    </td>
				    <td width="32%"></td>
                 </tr>                 
               </table>
               
             </td>
             <td width="38%"></td>
           </tr>
           <tr>                     
             <td width="100%"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                 	<td width="12%" class="principalLabel" align="right">
             		  <bean:message key="prompt.observacao"/>
             		  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> &nbsp;
             		</td>
                 	<td class="principalLabel">
                      	<html:text property="temaDsObservacao" maxlength="400" styleClass="text" style=" width : 345px;"/>
                    </td>
                    <td width="6%" align="right">
					  <img src="webFiles/images/botoes/setaDown.gif" title="<bean:message key="prompt.confirma"/>" width="21" height="18" class="geralCursoHand" onclick="submeteIncluirTelefone();">
				    </td>
				    <td width="32%"></td>
                 </tr>
               </table>
              </td>
             </tr>
           <tr> 
             <td colspan="4" class="principalLabel" align="right">&nbsp; </td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
           <tr> 
           	 <td width="4%" class="principalLstCab">&nbsp;</td>
             <td width="20%" class="principalLstCab"><bean:message key="prompt.tipo"/></td>
             <td width="54%" class="principalLstCab"><bean:message key="prompt.telefone"/></td>
             <td width="22%" class="principalLstCab"><bean:message key="prompt.ramal"/></td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="160">
           <tr> 
             <td valign="top"> 
                 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                   <tr> 
                     <td height="160">
                     	<iframe id=ifrmLstTelefone  name="ifrmLstTelefone" src="AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_LST_TELEFONE%>&acao=none" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidtd="0" marginheight="0" ></iframe>
                     </td>                     
                   </tr>
                 </table>
             </td>
           </tr>
         </table>
</html:form>
</body>
</html>