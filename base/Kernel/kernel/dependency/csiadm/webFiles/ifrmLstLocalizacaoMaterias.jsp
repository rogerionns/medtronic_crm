<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmLstManifTerceiros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
nLinha = new Number(0);

function incluirItem(cLocal, dLocal) {
	
	nLinha = nLinha + 1;
	
	strTxt = "";
    strTxt += "<div id=\"lstLocal" + nLinha + "\" style='width:100%; height:0px; z-index:1; overflow: auto'>";
    strTxt += "<table id=\"" + nLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cLocal + "'>";
    strTxt += "    <input type='hidden' name='descricao' value='" + dLocal + "'>";
    if (cLocal == "0")
	    strTxt += "    <td class='principalLstParMao' width='2%'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"excluirItem('" + nLinha + "')\"></td>";
	else
	    strTxt += "    <td class='principalLstParMao' width='2%'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"excluirItemBD('" + nLinha + "', '" + cLocal + "')\"></td>";
    strTxt += "    <td class='principalLstPar' width='98%'>" + dLocal + "&nbsp;</td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    strTxt += "</div>";

	document.getElementsByName("lstLocal").innerHTML += strTxt;
}

function excluirItem(nTblExcluir) {
	msg = '<bean:message key="prompt.Deseja_remover_esse_item" />';
	if (confirm(msg)) {
		objIdTbl = lstLocal.getElementById("lstLocal" + nTblExcluir);
		lstLocal.removeChild(objIdTbl);
	}
}

function excluirItemBD(nTblExcluir, cLocal) {
	msg = '<bean:message key="prompt.Deseja_remover_esse_item" />';
	if (confirm(msg)) {
		objIdTbl = lstLocal.all.item("lstLocal" + nTblExcluir);
		lstLocal.removeChild(objIdTbl);
		localExcluidos.value += cLocal + ";";
	}
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<input type="hidden" name="localExcluidos">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top"> 
    <td class="principalLstPar" colspan="3" height="100%">
	  <div id="lstLocal" style="position:absolute; width:100%; height:0px; z-index:1">
		  <logic:present name="csCdtbLocalMateriaLomaVector">
			  <logic:iterate name="csCdtbLocalMateriaLomaVector" id="cclmlVector">
		        <script language="JavaScript">
				  incluirItem('<bean:write name="cclmlVector" property="idLomaCdLocalMateria" />',
							  '<bean:write name="cclmlVector" property="lomaDsLocalMateria" />',
							  true);
		        </script>
			  </logic:iterate>
		  </logic:present>
	  </div>
    </td>
  </tr>
</table>
</body>
</html>