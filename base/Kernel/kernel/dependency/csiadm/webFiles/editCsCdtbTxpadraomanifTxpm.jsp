<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script>
	function desabilitaCamposTxpadraomanif(){
		document.administracaoCsCdtbTxpadraomanifTxpmForm.txpmDsTxpadraomanif.disabled= true;	
		document.administracaoCsCdtbTxpadraomanifTxpmForm.txpmDhInativo.disabled= true;
		document.administracaoCsCdtbTxpadraomanifTxpmForm.txpmTxTexto.disabled= true;
	}
	
	function inicio(){
		setaChavePrimaria(administracaoCsCdtbTxpadraomanifTxpmForm.idTxpmCdTxpadraomanif.value);
	}	
</script>
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsCdtbTxpadraomanifTxpmForm" action="/AdministracaoCsCdtbTxpadraomanifTxpm.do">
	<input readonly type=hidden name=remLen size=3 maxlength=3 value="4000"/> 
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
  <tr> 
    <td width="14%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><img src="webFiles/images/icones/setaAzul.gif" width="7px" height="7px"></td>
    <td width="100px"><html:text property="idTxpmCdTxpadraomanif" styleClass="text" disabled="true" /></td>
    <td>&nbsp;</td>
    <td width="32%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="14%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="txpmDsTxpadraomanif" style="width:408px" styleClass="text" maxlength="60" /> 
    </td>
    <td width="32%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="14%"> 
      <div align="right"><bean:message key="prompt.observacao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="2"> <html:textarea property="txpmTxTexto" style="width:478px" styleClass="text" rows="10" onkeypress="textCounter(this.form.txpmTxTexto,2000)" onkeyup="textCounter(this.form.txpmTxTexto,2000)" /> 
    </td>
     <td width="32%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="14%">&nbsp;</td>
    <td width="13%" colspan="2">&nbsp;</td>
    <td width="32%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="14%">&nbsp;</td>
    <td colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <!-- 88642 - 03/09/2013 - Jaider Alba -->
          <td align="left" width="66%" class="principalLabel">
          	<html:checkbox value="true" property="txpmInManifest"/> <bean:message key="prompt.manifestacao"/> <br />
          	<html:checkbox value="true" property="txpmInFollowup"/> <bean:message key="prompt.followup"/>
          </td>
                     
          <td align="right" width="17%"> <html:checkbox value="true" property="txpmDhInativo"/></td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/></td>
        </tr>
      </table>
    </td>
    <td width="32%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposTxpadraomanif();					
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TEXTOPADRAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbTxpadraomanifTxpmForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TEXTOPADRAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposTxpadraomanif();
			}else{
				document.administracaoCsCdtbTxpadraomanifTxpmForm.idTxpmCdTxpadraomanif.disabled= false;
				document.administracaoCsCdtbTxpadraomanifTxpmForm.idTxpmCdTxpadraomanif.value= '';
				document.administracaoCsCdtbTxpadraomanifTxpmForm.idTxpmCdTxpadraomanif.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TEXTOPADRAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TEXTOPADRAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbTxpadraomanifTxpmForm.imgGravar);	
				desabilitaCamposTxpadraomanif();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbTxpadraomanifTxpmForm.txpmDsTxpadraomanif.focus();}
	catch(e){}
</script>