<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	
	long idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposEventoFollowUp(){
		document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuDsEventoFollowUp.disabled= true;
		document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuDhInativo.disabled= true;	
		document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuInContaLigacao.disabled= true;
	}	
	
	function changeGrupoManif(){
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
		ifrmCmbSuperGrupomanifestacao.document.location = "AdministracaoCsCdtbEventoFollowUpEvfu.do"+
			"?tela=<%=MAConstantes.TELA_CMB_SUPERGRUPOMANIFESTACAO%>"+
			"&acao=<%=Constantes.ACAO_CONSULTAR%>"+
			"&idEmprCdEmpresa=<%=idEmprCdEmpresa%>"+
			"&csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo="+ administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value +
			"&csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao="+ administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value +
			"&csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao="+ administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
	<%} else {%>
		ifrmCmbGrupomanifestacao.document.location = "AdministracaoCsCdtbEventoFollowUpEvfu.do"+
			"?tela=<%=MAConstantes.TELA_CMB_GRUPOMANIFESTACAO%>"+
			"&acao=<%=Constantes.ACAO_CONSULTAR%>"+
			"&idEmprCdEmpresa=<%=idEmprCdEmpresa%>"+
			"&csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo="+ administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value +
			"&csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao="+ administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value +
			"&csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao="+ administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
	<%}%>
	}
	
	//Checkboxes todos os grupos / tipos
	function checkTodos(x){
		if (!administracaoCsCdtbEventoFollowUpEvfuForm.todosGrupos.checked && !administracaoCsCdtbEventoFollowUpEvfuForm.todosTipos.checked 
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
			&& !administracaoCsCdtbEventoFollowUpEvfuForm.todosSuperGrupos.checked 
<%}%>
			) {
			ifrmCmbTpmanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].selectedIndex = 0;

			ifrmCmbGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].disabled = false;
			ifrmCmbTpmanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].disabled = false;

<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
			ifrmCmbSuperGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["idSugrCdSupergrupo"].disabled = false;
<%} else {%>
			ifrmCmbGrupomanifestacao.submeteForm();
<%}%>
		}
		else if(x == 0){
			administracaoCsCdtbEventoFollowUpEvfuForm.todosGrupos.checked = false;
			administracaoCsCdtbEventoFollowUpEvfuForm.todosTipos.checked = false;
			ifrmCmbSuperGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["idSugrCdSupergrupo"].selectedIndex = 0;
			ifrmCmbGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].selectedIndex = 0;
			ifrmCmbTpmanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].selectedIndex = 0;

			ifrmCmbSuperGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["idSugrCdSupergrupo"].disabled = true;
			ifrmCmbGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].disabled = true;
			ifrmCmbTpmanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].disabled = true;

			ifrmCmbSuperGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm.submit();
		}
		else if(x == 1){
			administracaoCsCdtbEventoFollowUpEvfuForm.todosTipos.checked = false;
			ifrmCmbGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].selectedIndex = 0;
			ifrmCmbTpmanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].selectedIndex = 0;
			
			ifrmCmbGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].disabled = true;
			ifrmCmbTpmanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].disabled = true;

			ifrmCmbGrupomanifestacao.submeteForm();
		}
		else if(x == 2){
			administracaoCsCdtbEventoFollowUpEvfuForm.todosGrupos.checked = false;
			ifrmCmbTpmanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].selectedIndex = 0;
			
			ifrmCmbGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].disabled = false;
			ifrmCmbTpmanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].disabled = true;
			
		}
	}
	
	//Funcao para marcar / desmarcar todos os checks da lista
	function marcaDesmarca(marcar){
		var objs = lstTpmanifestacaoEvfu.document.getElementsByTagName("INPUT");
		for(i = 0; i < objs.length; i++){
			if(objs[i].type == "checkbox"){
				objs[i].checked = marcar;
				adicionarTpma(objs[i].value, marcar);
			}
		}
	}
	
	//Excluir os tipos de manifestacao selecionados
	function excluirTpma(){
		var idTpmaSel_Enviar;
		if(idTpmaSelecionados == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_um_tipo_de_manifestacao"/>");
		}
		else if(confirm("<bean:message key="prompt.alert.temCertezaQueDesejaExcluirSelecionados"/>")){
			idTpmaSel_Enviar = idTpmaSelecionados;
			if (idTpmaSel_Enviar.substr(idTpmaSel_Enviar.length-1,1)==",")
				idTpmaSel_Enviar = idTpmaSel_Enviar.substr(0,idTpmaSel_Enviar.length-1);
			
			lstTpmanifestacaoEvfu.document.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp.value = administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp.value;
			lstTpmanifestacaoEvfu.document.administracaoCsCdtbEventoFollowUpEvfuForm.idTpmaExcluir.value = idTpmaSel_Enviar;
			lstTpmanifestacaoEvfu.document.administracaoCsCdtbEventoFollowUpEvfuForm.acao.value = "<%=Constantes.ACAO_EXCLUIR%>";
			lstTpmanifestacaoEvfu.document.administracaoCsCdtbEventoFollowUpEvfuForm.submit();
		}
	}
	
	var idTpmaSelecionados = "";
	function adicionarTpma(idTpma, adicionar){

		if(adicionar){
			if(idTpmaSelecionados.indexOf("," + idTpma + ",") == -1) {
				if (idTpmaSelecionados.substr(idTpmaSelecionados.length-1,1)==",")
					idTpmaSelecionados = idTpmaSelecionados.substr(0,idTpmaSelecionados.length-1);
				idTpmaSelecionados += ","+ idTpma+",";
			}

		}
		else{
			if(idTpmaSelecionados.indexOf(","+ idTpma) > -1)
				idTpmaSelecionados = idTpmaSelecionados.substring(0, idTpmaSelecionados.indexOf(","+ idTpma)) +
					idTpmaSelecionados.substring(idTpmaSelecionados.indexOf(","+ idTpma) + idTpma.length + 1, idTpmaSelecionados.length);
		}
	}
	
	function iniciaTela(){
		setaChavePrimaria(administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp.value);
		lstTpmanifestacaoEvfu.document.location = "AdministracaoCsCdtbEventoFollowUpEvfu.do"+
			"?tela=<%=MAConstantes.TELA_LST_TPMANIFESTACAOEVFU%>"+
			"&acao=<%=Constantes.ACAO_CONSULTAR%>"+
			"&idEvfuCdEventoFollowUp="+ administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp.value;
	}
</script>

<body class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();" style="overflow: hidden;">

<html:form styleId="administracaoCsCdtbEventoFollowUpEvfuForm" action="/AdministracaoCsCdtbEventoFollowUpEvfu.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<html:hidden property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
	<html:hidden property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />
	<html:hidden property="idSugrCdSupergrupo" />
	
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
		<td width="15%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="10%"> <html:text property="idEvfuCdEventoFollowUp" styleClass="text" disabled="true" /> </td>
		<td width="22%">&nbsp;</td> <!-- //DECRETO -->
		<td width="35%">&nbsp;</td>
	</tr>
	<tr> 
		<td align="right" class="principalLabel"><bean:message key="prompt.descricao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td colspan="2"> <html:text property="evfuDsEventoFollowUp" styleClass="text" maxlength="60" style="width: 94%" /> </td>
		<!-- //DECRETO -->
		<td class="principalLabel">&nbsp;
			<html:checkbox value="true" property="evfuInTextoManif"/>&nbsp;
			Adicionar Evento a Texto da Manifestação
		</td>
	</tr>
	<tr>
		<td align="right" class="principalLabel"><bean:message key="prompt.destinatario"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td colspan="2"> 
			<html:select property="idFuncCdFuncionario" styleClass="principalObjForm" style="width: 94%"> 
				<html:option value="">
					<bean:message key="prompt.selecione_uma_opcao"/></html:option>
				<html:options collection="CsCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>
			</html:select>
		</td>
		<td class="principalLabel">&nbsp;
			<html:checkbox value="true" property="evfuInContaLigacao"/>&nbsp;
			<bean:message key="prompt.contarLigacao"/>
		</td>
	</tr>
	<tr>
		<td width="20%" class="principalLabel" style="text-align: right"><bean:message key="prompt.previsaoResolucao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="20%" class="principalLabel"> 
			<table width="100%">
				<tr>
					<td width="50%">
						<html:text property="evfuNrTemporesolucao" styleClass="text" maxlength="3" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" />
					</td>
					<td width="50%">
						<html:select property="evfuInTemporesolucao" styleClass="principalObjForm" > 
	            			<html:option value=""></html:option> <html:option value="D"><bean:message key="prompt.dias"/></html:option> 
	           				<html:option value="H"><bean:message key="prompt.horas"/></html:option>
	            			<html:option value="M"><bean:message key="prompt.minutos"/></html:option> 
	            		</html:select> 
	            	</td>
	            </tr>
			</table>
		</td>
		<td class="principalLabel">&nbsp;</td>
		<td class="principalLabel" style="text-align: left">&nbsp;
			<html:checkbox value="true" property="evfuInConcluir"/>&nbsp;
			<bean:message key="prompt.concluir_automatico"/>
		</td>
	</tr>	
	<tr>
	<!-- 88642 - 03/09/2013 - Jaider Alba -->
		<td align="right" class="principalProdManif" style="text-align: right;">
			<bean:message key="prompt.textopadrao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
		</td>
		<td colspan="2" class="principalProdManif" style="text-align: left" > 
			<html:select property="idTxpmCdTxpadraomanif" styleClass="principalObjForm" style="width: 94%"> 
				<html:option value="">
					<bean:message key="prompt.selecione_uma_opcao"/></html:option>
				<html:options collection="csCdtbTxpadraomanifTxpmVector" property="idTxpmCdTxpadraomanif" labelProperty="txpmDsTxpadraomanif"/>
			</html:select>
		</td>
		<td class="principalProdManif" style="text-align:left;">&nbsp;
			<html:checkbox value="true" property="evfuDhInativo"/>&nbsp;
			<bean:message key="prompt.inativo"/>
		</td>
	</tr>
	
	<tr>
		<td align="right" class="principalLabel"><%= getMessage("prompt.manifestacao", request)%><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td colspan="2"> 
			<html:select property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" styleClass="principalObjForm" style="width: 94%" onchange="changeGrupoManif();">
				<html:option value="">
					<bean:message key="prompt.selecione_uma_opcao"/></html:option>
				<html:options collection="CsCdtbManiftipoMatpVector" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo"/>
			</html:select>
		</td>
		<td class="principalLabel">&nbsp;
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
			<html:checkbox property="todosSuperGrupos" styleId="ckTodosSuperGrupos" onclick="checkTodos(0);"/>&nbsp;
			<bean:message key="prompt.todos_supergrupos_manifestacao"/>
<%} else { %>
			<html:checkbox property="todosGrupos" styleId="ckTodosGrupos" onclick="checkTodos(1);"/>&nbsp;
			<bean:message key="prompt.todos_grupos_manifestacao"/>
<%}%>
		</td>
	</tr>
	
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
	<tr>
		<td align="right" class="principalLabel"><bean:message key="prompt.Supergrupo"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td colspan="2"> 
			<iframe id="ifrmCmbSuperGrupomanifestacao" name="ifrmCmbSuperGrupomanifestacao" src="" height="20" width="100%" frameborder="0" style="width: 100%"></iframe>
		</td>
		<td class="principalLabel">&nbsp;
			<html:checkbox property="todosGrupos" styleId="ckTodosGrupos" onclick="checkTodos(1);"/>&nbsp;
			<bean:message key="prompt.todos_grupos_manifestacao"/>
		</td>
	</tr>
<%} %>

	<tr>
		<td align="right" class="principalLabel"><%= getMessage("prompt.grupomanif", request)%><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td colspan="2"> 
			<iframe id="ifrmCmbGrupomanifestacao" name="ifrmCmbGrupomanifestacao" src="" height="20" width="100%" frameborder="0"></iframe>
			<script>changeGrupoManif();</script>
		</td>
		<td class="principalLabel">&nbsp;
			<html:checkbox property="todosTipos" styleId="ckTodosTipos" onclick="checkTodos(2);"/>&nbsp;
			<bean:message key="prompt.todos_tipos_grupo"/>
		</td>
	</tr>
	<tr>
		<td class="principalProdManif" style="text-align: right"><%= getMessage("prompt.tipomanif", request)%><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td colspan="2" class="principalProdManif">
			<iframe id="ifrmCmbTpmanifestacao" name="ifrmCmbTpmanifestacao" src="" height="20" width="100%" frameborder="0"></iframe>
		</td>
		<td class="principalLabel">&nbsp;
			<% // 91850 - 05/11/2013 - Jaider Alba %>
			<html:checkbox property="evfuInWeb" />&nbsp;
			<bean:message key="prompt.adm.web"/>
		</td>
	</tr>
	<tr> 
		<td>&nbsp;</td>
		<td colspan="3">&nbsp;</td>
	</tr>
</table>
<div id="divLstTpfu">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td width="18">&nbsp;</td>
			<td><img src="webFiles/images/icones/bt_selecionar_nao.gif" onclick="marcaDesmarca(false);" width="16" height="16" class="geralCursoHand" title="<bean:message key="prompt.desmarcarTodos" />"></td>
			<td><img src="webFiles/images/icones/bt_selecionar_sim.gif" onclick="marcaDesmarca(true);" width="16" height="16" class="geralCursoHand" title="<bean:message key="prompt.marcarTodos" />"></td>
		</tr>
	</table>
	<table width="720" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td width="6%" class="principalLstCab">&nbsp;</td>
			<td class="principalLstCab" width="30%" align="left"><%= getMessage("prompt.manifestacao", request)%></td>
			<td class="principalLstCab" width="32%" align="left"><%= getMessage("prompt.grupomanif", request)%></td>
			<td class="principalLstCab" width="32%" align="left"><%= getMessage("prompt.tipomanif", request)%></td>
		</tr>
		<tr>
			<td colspan=4>
				<iframe id="lstTpmanifestacaoEvfu" name="lstTpmanifestacaoEvfu" src="" width="720" height="135" frameborder="0"></iframe>
			</td>
		</tr>
	</table>
</div>    

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposEventoFollowUp();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_EVENTOFOLLOWUP_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbEventoFollowUpEvfuForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_EVENTOFOLLOWUP_INCLUSAO_CHAVE%>')){
				desabilitaCamposEventoFollowUp();
			}
			document.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp.disabled= false;
			document.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp.value= '';
			document.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp.disabled= true;
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_EVENTOFOLLOWUP_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_EVENTOFOLLOWUP_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbEventoFollowUpEvfuForm.imgGravar);	
				desabilitaCamposEventoFollowUp();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuDsEventoFollowUp.focus();}
	catch(e){}
</script>