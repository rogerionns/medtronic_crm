<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="org.apache.commons.beanutils.BeanUtils"%>
<%@page import="java.util.Vector"%>
<%@page import="org.apache.struts.validator.DynaValidatorForm"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>




<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/cobranca/js/pt/TratarDados.js"></script>
<script language="JavaScript">
	var countRegistros = new Number(0);
	var houveAlteracao = false;
	var possuiDelimitador = false;
	var ultimoRegistroSelecionado = new Number(-1);
	var atualRegistroSelecionado = new Number(-1);
	
	function excluir(indice) {
		var jaEstaGravadoNoBD = false;
		
		if(confirm('Deseja excluir este registro?')) {
			if(countRegistros > 1) {
				if(document.forms[0].lico_nr_sequencia[indice].value == '') {
					jaEstaGravadoNoBD = false;
				} else {
					jaEstaGravadoNoBD = true;
				}
			} else {
				if(document.forms[0].lico_nr_sequencia.value == '') {
					jaEstaGravadoNoBD = false;
				} else {
					jaEstaGravadoNoBD = true;
				}
			}

			if(jaEstaGravadoNoBD) {
				if(indice > 0) {
					document.forms[0].lico_nr_sequencia_exclusao.value = document.forms[0].lico_nr_sequencia[indice].value;
				} else {
					document.forms[0].lico_nr_sequencia_exclusao.value = document.forms[0].lico_nr_sequencia.value;
				}
				
				document.forms[0].id_laou_cd_sequencial.value = parent.document.forms[0].id_laou_cd_sequencial.value; 
				document.forms[0].target = this.name = 'listaLayout';
				document.forms[0].action = 'ExcluirRegistro.do';
				document.forms[0].submit();
			} else {
				excluiRegistroNaTela(indice);
			}
		}
	}
	
	function registroAlterado(indice) {
		houveAlteracao = true;
		pintaLinha(indice);
	}
	
	function pintaLinha(indice) {		
		var corMarcada = "#B0C4DE";
		var corNaoMarcada = "#FFFFFF";
		atualRegistroSelecionado = indice;

		// [ Trakinas ] Todos os .sytle dos combos foram comentados pois da vers�o 7 para cima ao setar o style ele perdia o clique do combo.
		
		if(countRegistros > 1) {
			if(ultimoRegistroSelecionado >= 0) {
				document.forms[0].lico_ds_nome[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_ds_descricao[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_ds_regex[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_nr_posicao[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_nr_ci[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_nr_cf[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_ds_tabeladesttemp[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_ds_campodesttemp[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				//document.forms[0].lico_ds_tabeladest[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				//document.forms[0].lico_ds_campodest[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				//document.forms[0].lico_in_identificadorreg[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_ds_identificadorreg[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				//document.forms[0].lico_nr_sequenciapai[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_ds_depara[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_ds_hardcoded[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				//document.forms[0].lico_ds_nextcode[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				//document.forms[0].lico_nr_sequenciafk[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				//document.forms[0].lico_ds_tipocampo[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_nr_tamanho[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				//document.forms[0].lico_in_obrigatorio[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_ds_formato[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_ds_agrupadorregistro[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				//document.forms[0].lico_in_pesquisa[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
				document.forms[0].lico_nr_digito[ultimoRegistroSelecionado].style.backgroundColor = corNaoMarcada;
			}
			document.forms[0].lico_ds_nome[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_descricao[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_regex[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_nr_posicao[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_nr_ci[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_nr_cf[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_tabeladesttemp[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_campodesttemp[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			//document.forms[0].lico_ds_tabeladest[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			//document.forms[0].lico_ds_campodest[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			//document.forms[0].lico_in_identificadorreg[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_identificadorreg[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			//document.forms[0].lico_nr_sequenciapai[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_depara[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_hardcoded[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			//document.forms[0].lico_ds_nextcode[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			//document.forms[0].lico_nr_sequenciafk[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			//document.forms[0].lico_ds_tipocampo[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_nr_tamanho[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			//document.forms[0].lico_in_obrigatorio[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_formato[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_agrupadorregistro[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			//document.forms[0].lico_in_pesquisa[atualRegistroSelecionado].style.backgroundColor = corMarcada;
			document.forms[0].lico_nr_digito[atualRegistroSelecionado].style.backgroundColor = corMarcada;
		} else {
			document.forms[0].lico_ds_nome.style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_descricao.style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_regex.style.backgroundColor = corMarcada;
			document.forms[0].lico_nr_posicao.style.backgroundColor = corMarcada;
			document.forms[0].lico_nr_ci.style.backgroundColor = corMarcada;
			document.forms[0].lico_nr_cf.style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_tabeladesttemp.style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_campodesttemp.style.backgroundColor = corMarcada;
			//document.forms[0].lico_ds_tabeladest.style.backgroundColor = corMarcada;
			//document.forms[0].lico_ds_campodest.style.backgroundColor = corMarcada;
			//document.forms[0].lico_in_identificadorreg.style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_identificadorreg.style.backgroundColor = corMarcada;
			//document.forms[0].lico_nr_sequenciapai.style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_depara.style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_hardcoded.style.backgroundColor = corMarcada;
			//document.forms[0].lico_ds_nextcode.style.backgroundColor = corMarcada;
			//document.forms[0].lico_nr_sequenciafk.style.backgroundColor = corMarcada;
			//document.forms[0].lico_ds_tipocampo.style.backgroundColor = corMarcada;
			document.forms[0].lico_nr_tamanho.style.backgroundColor = corMarcada;
			//document.forms[0].lico_in_obrigatorio.style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_formato.style.backgroundColor = corMarcada;
			document.forms[0].lico_ds_agrupadorregistro.style.backgroundColor = corMarcada;
			//document.forms[0].lico_in_pesquisa.style.backgroundColor = corMarcada;
			document.forms[0].lico_nr_digito.style.backgroundColor = corMarcada;
		}
		
		ultimoRegistroSelecionado = atualRegistroSelecionado;
	}
	
	function excluiRegistroNaTela(indice) {
		objIdTbl = document.getElementById("divRegistro" + indice);
		lstRegistro.removeChild(objIdTbl);
	}
	
	function novo() {
//		parent.document.all.item('aguarde').style.visibility = 'visible';
		showInProgress();
		
		//Habilitando todos os campos antes do submit
		habilitaTodosElementosDoForm();
	
		document.forms[0].id_laou_cd_sequencial.value = parent.document.forms[0].id_laou_cd_sequencial.value; 
		document.forms[0].target = this.name = 'listaLayout';
		document.forms[0].action = 'IncluirNovoRegistro.do';
		document.forms[0].submit();
	}

	// Chamado: 98422 - 29/12/2014 - Marcos Donato	
	function showInProgress(){
		window.top.document.all.item('LayerAguarde').style.visibility = 'visible';
	}
	// Chamado: 98422 - 29/12/2014 - Marcos Donato	
	function hideInProgress(){
		window.top.document.all.item('LayerAguarde').style.visibility = 'hidden';
	}
	
	function salvar() {	
		if(parent.document.forms[0].id_laou_cd_sequencial.value != '') {
			if(confirm('Deseja gravar o(s) registro(s)?')) {
//				parent.document.all.item('aguarde').style.visibility = 'visible';
				showInProgress();
			
				if(countRegistros > 1) {
					for(i = 0; i < document.forms[0].lico_in_identificadorreg.length; i++) {
						if(document.forms[0].lico_in_identificadorreg[i].value == '') {
							alert('O campo Possui Identificador � obrigat�rio!');
							document.forms[0].lico_in_identificadorreg[i].focus();
//							parent.document.all.item('aguarde').style.visibility = 'hidden';
							hideInProgress();
							return false;
						}
						
						if(document.forms[0].lico_ds_tipocampo[i].value == '') {
							alert('O campo Tipo do Campo � obrigat�rio!');
							document.forms[0].lico_ds_tipocampo[i].focus();
//							parent.document.all.item('aguarde').style.visibility = 'hidden';
							hideInProgress();
							return false;
						}
						
						if(document.forms[0].lico_in_obrigatorio[i].value == '') {
							alert('O campo Obrigat�rio � obrigat�rio!');
							document.forms[0].lico_in_obrigatorio[i].focus();
//							parent.document.all.item('aguarde').style.visibility = 'hidden';
							hideInProgress();
							return false;
						}
						
						if(document.forms[0].lico_ds_tipocampo[i].value == 'DATETIME') {
							if(document.forms[0].lico_ds_formato[i].value == '') {
								alert('O campo Formato do Campo � obrigat�rio!');
								document.forms[0].lico_ds_formato[i].focus();
//								parent.document.all.item('aguarde').style.visibility = 'hidden';
								hideInProgress();
								return false;
							}
						}
						
						if(document.forms[0].lico_ds_tipocampo[i].value == 'DOUBLE') {
							//if(document.forms[0].lico_nr_digito[i].value == '' || document.forms[0].lico_nr_digito[i].value == '0') {
							if(document.forms[0].lico_nr_digito[i].value == '') {
								alert('O campo Casas Decimais � obrigat�rio!');
								document.forms[0].lico_nr_digito[i].focus();
//								parent.document.all.item('aguarde').style.visibility = 'hidden';
								hideInProgress();
								return false;
							}
						}
						
						if(document.forms[0].lico_ds_agrupadorregistro[i].value == '') {
							alert('O campo Agrupador Registro � obrigat�rio!');
							document.forms[0].lico_ds_agrupadorregistro[i].focus();
//							parent.document.all.item('aguarde').style.visibility = 'hidden';
							hideInProgress();
							return false;
						}
					}
				} else {
					if(document.forms[0].lico_in_identificadorreg.value == '') {
						alert('O campo Possui Identificador � obrigat�rio!');
						document.forms[0].lico_in_identificadorreg.focus();
//						parent.document.all.item('aguarde').style.visibility = 'hidden';
						hideInProgress();
						return false;
					}
					
					if(document.forms[0].lico_ds_tipocampo.value == '') {
						alert('O campo Tipo do Campo � obrigat�rio!');
						document.forms[0].lico_ds_tipocampo.focus();
//						parent.document.all.item('aguarde').style.visibility = 'hidden';
						hideInProgress();
						return false;
					}
					
					if(document.forms[0].lico_in_obrigatorio.value == '') {
						alert('O campo Obrigat�rio � obrigat�rio!');
						document.forms[0].lico_in_obrigatorio.focus();
//						parent.document.all.item('aguarde').style.visibility = 'hidden';
						hideInProgress();
						return false;
					}
					
					if(document.forms[0].lico_ds_tipocampo.value == 'DATETIME') {
						if(document.forms[0].lico_ds_formato.value == '') {
							alert('O campo Formato do Campo � obrigat�rio!');
							document.forms[0].lico_ds_formato.focus();
//							parent.document.all.item('aguarde').style.visibility = 'hidden';
							hideInProgress();
							return false;
						}
					}
					
					if(document.forms[0].lico_ds_tipocampo.value == 'DOUBLE') {
						if(document.forms[0].lico_nr_digito.value == '' || document.forms[0].lico_nr_digito.value == '0') {
							alert('O campo Casas Decimais � obrigat�rio!');
							document.forms[0].lico_nr_digito.focus();
//							parent.document.all.item('aguarde').style.visibility = 'hidden';
							hideInProgress();
							return false;
						}
					}
					
					if(document.forms[0].lico_ds_agrupadorregistro.value == '') {
						alert('O campo Agrupador Registro � obrigat�rio!');
						document.forms[0].lico_ds_agrupadorregistro.focus();
//						parent.document.all.item('aguarde').style.visibility = 'hidden';
						hideInProgress();
						return false;
					}
				}
				
				
				//Habilitando todos os campos antes do submit
				habilitaTodosElementosDoForm();	
							
				document.forms[0].id_laou_cd_sequencial.value = parent.document.forms[0].id_laou_cd_sequencial.value; 
				document.forms[0].target = this.name = 'listaLayout';
				document.forms[0].action = 'GravarRegistro.do';
				document.forms[0].submit();
			}
		} else {
			alert('O campo Layout � obrigat�rio!');
			parent.document.forms[0].id_laou_cd_sequencial.focus();
			return false; 
		}
	}
	
	function inicio() {
		parent.document.forms[0].caminhoArquivo.value = '';	
		parent.document.forms[0].delimitador.value = '';
		parent.document.forms[0].possuiCabecalho.checked = false;
		
		parent.carregaHistorico();
		
		//Verificando os layouts
		if(parent.document.forms[0].id_laou_cd_sequencial.value != '0') {
			for(i = 0; i < parent.layoutArray.length; i++) {
				if(parent.document.forms[0].id_laou_cd_sequencial.value == parent.layoutArray[i][0]) {
					if(parent.layoutArray[i][1] == 'S') {
						possuiDelimitador = true;
					}
					break;
				}
			}
		}
		
		if(countRegistros > 1) {
			for(i = 0; i < document.forms[0].lico_nr_sequencia.length; i++) {
				if(possuiDelimitador) {
					document.forms[0].lico_nr_ci[i].disabled = true;
					document.forms[0].lico_nr_cf[i].disabled = true;
				} else {
					document.forms[0].lico_nr_posicao[i].disabled = true;
				}
			}
		} else if(countRegistros == 1){
			if(possuiDelimitador) {
				document.forms[0].lico_nr_ci.disabled = true;
				document.forms[0].lico_nr_cf.disabled = true;
			} else {
				document.forms[0].lico_nr_posicao.disabled = true;
			}
		}
		
//		parent.document.all.item('aguarde').style.visibility = 'hidden';
		hideInProgress();
	}
	
	function verificaTipoCampo(indice) {
		if(countRegistros > 1) {
			if(document.forms[0].lico_ds_tipocampo[indice].value == 'DOUBLE') {
  				document.forms[0].lico_nr_digito[indice].disabled = false;
  			} else {
  				document.forms[0].lico_nr_digito[indice].value = '';
  				document.forms[0].lico_nr_digito[indice].disabled = true;
  			}
		} else {
			if(document.forms[0].lico_ds_tipocampo.value == 'DOUBLE') {
  				document.forms[0].lico_nr_digito.disabled = false;
  			} else {
  				document.forms[0].lico_nr_digito.value = '';
  				document.forms[0].lico_nr_digito.disabled = true;
  			}
		}
	}
	
	function habilitaTodosElementosDoForm() {
		//Habilitando todos os campos antes do submit
		for(i = 0; i < document.forms[0].length; i++) {
			document.forms[0].elements[i].disabled = false;
		}
	}
</script>

</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="CarregarCamposLayout.do" styleId="layoutImportacaoForm"> 
<input type="hidden" name="id_laou_cd_sequencial" />
<input type="hidden" name="lico_nr_sequencia_exclusao" />
	<div id="tituloLista" style="scroll: no; overflow: auto; width: 100%; z-index: 1; position: relative;height: 100%" class="LandscapeDiv">
		<!-- <table width="3385px" border="0" cellspacing="0" cellpadding="0"> -->
		<!--<table width="3285px" border="0" cellspacing="0" cellpadding="0"> -->
			<table width="2885px" border="0" cellspacing="0" cellpadding="0" >
				<thead style="height: 20px;">
					<tr>
						<th class="principalLstCab" width="20px" align="center">&nbsp;</th>
						<th class="principalLstCab" width="150px" align="center">&nbsp;<bean:message
								key="prompt.NomeDoCampo" /></th>
						<th class="principalLstCab" width="150px" align="center"><bean:message
								key="prompt.DescricaoDoCampo" /></th>
						<!-- <td class="principalLstCab" width="100px" align="center"><bean:message key="prompt.Regex" /></th> -->
						<th class="principalLstCab" width="100px" align="center"><bean:message
								key="prompt.sequencia" /></th>
						<th class="principalLstCab" width="100px" align="center"><bean:message
								key="prompt.PosicaoInicial" /></th>
						<th class="principalLstCab" width="100px" align="center"><bean:message
								key="prompt.PosicaoFinal" /></th>
						<th class="principalLstCab" width="200px" align="center"><bean:message
								key="prompt.NomeTabelaTemporaria" /></th>
						<th class="principalLstCab" width="215px" align="center"><bean:message
								key="prompt.NomeCampoTabelaTemporaria" /></th>
						<!-- <td class="principalLstCab" width="200px" align="center"><bean:message key="prompt.NomeTabelaDestino" /></th>
	        	<th class="principalLstCab" width="200px" align="center"><bean:message key="prompt.NomeCampoTabelaDestino" /></th> -->
						<th class="principalLstCab" width="150px" align="center"><bean:message
								key="prompt.PossuiIdentificador" /></th>
						<th class="principalLstCab" width="100px" align="center"><bean:message
								key="prompt.Identificador" /></th>
						<th class="principalLstCab" width="150px" align="center"><bean:message
								key="prompt.SequenciaPai" /></th>
						<th class="principalLstCab" width="200px" align="center"><bean:message
								key="prompt.DePara" /></th>
						<th class="principalLstCab" width="100px" align="center"><bean:message
								key="prompt.HardCode" /></th>
						<th class="principalLstCab" width="100px" align="center"><bean:message
								key="prompt.NextCode" /></th>
						<th class="principalLstCab" width="150px" align="center"><bean:message
								key="prompt.SequenciaFK" /></th>
						<th class="principalLstCab" width="150px" align="center"><bean:message
								key="prompt.TipoCampo" /></th>
						<th class="principalLstCab" width="150px" align="center"><bean:message
								key="prompt.TamanhoCampo" /></th>
						<th class="principalLstCab" width="100px" align="center"><bean:message
								key="prompt.Obrigatorio" /></th>
						<th class="principalLstCab" width="150px" align="center"><bean:message
								key="prompt.FormatoCampo" /></th>
						<th class="principalLstCab" width="150px" align="center"><bean:message
								key="prompt.AgrupadorRegistro" /></th>
						<th class="principalLstCab" width="90px" align="center"><bean:message
								key="prompt.Pesquisa" /></th>
						<th class="principalLstCab" width="100px" align="center"><bean:message
								key="prompt.CasasDecimais" /></th>
						<th class="principalLstCab" width="10px" align="center">&nbsp;</th>
					</tr>
				</thead>
				<tbody id="lstRegistro" class="LandscapeDiv">
					<logic:present name="layoutVector">
					<logic:iterate name="layoutVector" id="layoutVector" indexId="indice">
					<!-- <table width="3285px" border="0" cellspacing="0" cellpadding="0"> -->
						<tr onclick="registroAlterado(<%=indice.intValue()%>)" id="divRegistro<%=indice.intValue()%>" name="divRegistro<%=indice.intValue()%>"> 
							<input type="hidden" name="lico_nr_sequencia" value="<bean:write name="layoutVector" property="field(lico_nr_sequencia)" />" />
							<input type="hidden" name="lico_ds_valoriteracao" value="<bean:write name="layoutVector" property="field(lico_ds_valoriteracao)" />" />
		
							<td class="intercalaLst<%=indice.intValue()%2%>" width="20px" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluiCampo<%=indice.intValue()%>" id="btExcluiCampo<%=indice.intValue()%>" class="geralCursoHand" alt="<bean:message key="prompt.excluir"/>"  onclick="excluir('<%=indice.intValue()%>');" /></td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="150px">
				        		<input type="text" name="lico_ds_nome" style="width: 150px" maxlength="255" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_nome)" />" />
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="150px">
				        		<input type="text" name="lico_ds_descricao" style="width: 150px" maxlength="255" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_descricao)" />" />
				        	</td>
				        	<!-- <td class="intercalaLst<%=indice.intValue()%2%>" width="100px">
				        		<input type="text" name="lico_ds_regex" style="width: 100px" maxlength="255" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_regex)" />"/>
				        	</td> -->
				        	<input type="hidden" name="lico_ds_regex" value="<bean:write name="layoutVector" property="field(lico_ds_regex)" />"/>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="100px">
				        		<input type="text" name="lico_nr_posicao" style="width: 100px" maxlength="10" class="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);" value="<bean:write name="layoutVector" property="field(lico_nr_posicao)" />"/>
							</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="100px">
				        		<input type="text" name="lico_nr_ci" style="width: 100px" maxlength="10" class="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);" value="<bean:write name="layoutVector" property="field(lico_nr_ci)" />"/>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="100px">
				        		<input type="text" name="lico_nr_cf" style="width: 100px" maxlength="10" class="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);" value="<bean:write name="layoutVector" property="field(lico_nr_cf)" />"/>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="200px">
				        		<input type="text" name="lico_ds_tabeladesttemp" style="width: 200px" maxlength="255" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_tabeladesttemp)" />"/>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="215px">
				        		<input type="text" name="lico_ds_campodesttemp" style="width: 215px" maxlength="255" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_campodesttemp)" />"/>
				        	</td>
				        	<%--<td class="intercalaLst<%=indice.intValue()%2%>" width="200px">
				        		<input type="text" name="lico_ds_tabeladest" style="width: 200px" maxlength="255" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_tabeladest)" />"/>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="200px">
				        		<input type="text" name="lico_ds_campodest" style="width: 200px" maxlength="255" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_campodest)" />"/>
				        	</td>--%>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="150px">
				        		<html:select property="lico_in_identificadorreg" styleClass="principalObjForm" style="width: 150px">
				        			<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
				        			<option value="S" <%=((Vo)layoutVector).getFieldAsString("lico_in_identificadorreg").equalsIgnoreCase("S")?"selected":"" %>>SIM</option>
				        			<option value="N" <%=((Vo)layoutVector).getFieldAsString("lico_in_identificadorreg").equalsIgnoreCase("N")?"selected":"" %>>N�O</option>
				        		</html:select>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="100px">
				        		<input type="text" name="lico_ds_identificadorreg" style="width: 100px" maxlength="255" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_identificadorreg)" />"/>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="150px">
				        		<html:select property="lico_nr_sequenciapai" styleClass="principalObjForm" style="width: 150px">
									<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
									<logic:present name="identificadorPaiVector">
										<logic:iterate name="identificadorPaiVector" id="identificadorPai">
											<option value="<%=((Vo)identificadorPai).getFieldAsString("lico_nr_sequencia") %>" <%=((Vo)layoutVector).getFieldAsString("lico_nr_sequenciapai").equalsIgnoreCase(((Vo)identificadorPai).getFieldAsString("lico_nr_sequencia"))?"selected":"" %>><bean:write name="identificadorPai" property="field(lico_ds_nome)" /></option>
										</logic:iterate>
									</logic:present>
				        		</html:select>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="200px">
				        		<input type="text" name="lico_ds_depara" style="width: 200px" maxlength="4000" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_depara)" />"/>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="100px">
				        		<input type="text" name="lico_ds_hardcoded" style="width: 100px" maxlength="255" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_hardcoded)" />"/>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="100px">
				        		<html:select property="lico_ds_nextcode" styleClass="principalObjForm" style="width: 100px">
									<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
				        			<option value="S" <%=((Vo)layoutVector).getFieldAsString("lico_ds_nextcode").equalsIgnoreCase("S")?"selected":"" %>>SIM</option>
				        			<option value="N" <%=((Vo)layoutVector).getFieldAsString("lico_ds_nextcode").equalsIgnoreCase("N")?"selected":"" %>>N�O</option>
				        		</html:select>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="150px">
				        		<html:select property="lico_nr_sequenciafk" styleClass="principalObjForm" style="width: 150px">
									<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
									<logic:present name="sequenciaFkVector">
										<logic:iterate name="sequenciaFkVector" id="sequenciaFk">
											<option value="<%=((Vo)sequenciaFk).getFieldAsString("lico_nr_sequencia") %>" <%=((Vo)layoutVector).getFieldAsString("lico_nr_sequenciafk").equalsIgnoreCase(((Vo)sequenciaFk).getFieldAsString("lico_nr_sequencia"))?"selected":"" %>><bean:write name="sequenciaFk" property="field(lico_ds_nome)" /></option>
										</logic:iterate>
									</logic:present>
				        		</html:select>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" indice="<%=indice %>" width="150px">
				        		<html:select property="lico_ds_tipocampo" styleClass="principalObjForm" onchange="verificaTipoCampo(parent.getAttribute('indice'));">
				        			<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
				        			<option value="DATETIME" <%=((Vo)layoutVector).getFieldAsString("lico_ds_tipocampo").equalsIgnoreCase("DATETIME")?"selected":"" %>>DATETIME</option>
									<option value="DOUBLE" <%=((Vo)layoutVector).getFieldAsString("lico_ds_tipocampo").equalsIgnoreCase("DOUBLE")?"selected":"" %>>DOUBLE</option>
									<option value="INT" <%=((Vo)layoutVector).getFieldAsString("lico_ds_tipocampo").equalsIgnoreCase("INT")?"selected":"" %>>INT</option>
									<option value="LONG" <%=((Vo)layoutVector).getFieldAsString("lico_ds_tipocampo").equalsIgnoreCase("LONG")?"selected":"" %>>LONG</option>
									<option value="STRING" <%=((Vo)layoutVector).getFieldAsString("lico_ds_tipocampo").equalsIgnoreCase("STRING")?"selected":"" %>>STRING</option>
				        		</html:select>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="150px">
				        		<input type="text" name="lico_nr_tamanho" style="width: 150px" maxlength="10" class="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);" value="<bean:write name="layoutVector" property="field(lico_nr_tamanho)" />"/>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="100px">
				        		<html:select property="lico_in_obrigatorio" styleClass="principalObjForm" style="width: 100px" value="">
									<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
				        			<option value="S" <%=((Vo)layoutVector).getFieldAsString("lico_in_obrigatorio").equalsIgnoreCase("S")?"selected":"" %>>SIM</option>
				        			<option value="N" <%=((Vo)layoutVector).getFieldAsString("lico_in_obrigatorio").equalsIgnoreCase("N")?"selected":"" %>>N�O</option>
				        		</html:select>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="150px">
				        		<input type="text" name="lico_ds_formato" style="width: 150px" style="width: px" maxlength="255" class="principalObjForm" value="<bean:write name="layoutVector" property="field(lico_ds_formato)" />"/>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="150px">
				        		<input type="text" name="lico_ds_agrupadorregistro" style="width: 150px" maxlength="10" class="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);" value="<bean:write name="layoutVector" property="field(lico_ds_agrupadorregistro)" />"/>
				        	</td>
				        	<td class="intercalaLst<%=indice.intValue()%2%>" width="90px">
				        		<html:select property="lico_in_pesquisa" styleClass="principalObjForm" style="width: 90px">
									<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
				        			<option value="S" <%=((Vo)layoutVector).getFieldAsString("lico_in_pesquisa").equalsIgnoreCase("S")?"selected":"" %>>SIM</option>
				        			<option value="N" <%=((Vo)layoutVector).getFieldAsString("lico_in_pesquisa").equalsIgnoreCase("N")?"selected":"" %>>N�O</option>
				        		</html:select>
				        	</td>
				        	<td colspan="2" class="intercalaLst<%=indice.intValue()%2%>" width="110px">
				        		<input type="text" name="lico_nr_digito" style="width: 110px" maxlength="10" class="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);" value="<bean:write name="layoutVector" property="field(lico_nr_digito)" />"/>

				        	<div name="noPrint<%=indice.intValue()%>" id="noPrint<%=indice.intValue()%>">
				        	<table id="tiraTable<%=indice.intValue()%>"">
				        	<script>
				            
				        	countRegistros++;
					        	
				        	</script>
				        	</table>
				        	</div>

				        	</td>
				        	
						</tr>
						</logic:iterate>
					</logic:present>
				</tbody>
			</table>
		</div> 
</html:form>

<script>	
	var temPermissaoExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LAYOUT_IMP_COBRANCA_EXCLUSAO_CHAVE%>');
	
	//Verifica permiss�o de excluir
	if (!temPermissaoExcluir){
		if(countRegistros == 1) {
			setPermissaoImageDisable(false, document.layoutImportacaoForm.btExcluiCampo0);
		} else if(countRegistros > 1) {
			for(i = 0; i < countRegistros; i++) {
				eval('setPermissaoImageDisable(false, document.layoutImportacaoForm.btExcluiCampo'+i+');');
			}
		}
	}
</script>
</body>
</html>