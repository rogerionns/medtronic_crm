var countRegistro = new Number(0);
var temPermissaoIncluir = false;
var temPermissaoAlterar = false;
var temPermissaoExcluir = false;

function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[empresaCobrancaForm].userAction.value = "init";
	document.forms[empresaCobrancaForm].submit();		
}

function pesquisar(){	
	var texto= document.forms[0].elements["emcoDsEmpresaPesquisar"].value;
	var maiusculas=texto.toUpperCase();
	document.forms[0].elements["emcoDsEmpresaPesquisar"].value = maiusculas;
	
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();	
	habilitarCampos()
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();
	document.forms[0].elements["alterando"].value ="criar";	
}

function excluir(idEmpresaCobranca){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';		
		document.forms[0].elements["idEmpresaCobranca"].value =idEmpresaCobranca;		
		document.forms[0].userAction.value = "excluir";		
		document.forms[0].submit();		
	}	
}

function alterar(idEmpresaCobranca, dsEmpresa, dsCnpj, dsEndereco, dsNumero, dsBairro, dsCep, dsUf, dsCidade, dsContato, dsTelefone, dsEmail, emcoDhInativo){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
		
	document.forms[0].elements["idEmpresaCobranca"].value = idEmpresaCobranca;
	document.forms[0].elements["dsEmpresa"].value = dsEmpresa;
	document.forms[0].elements["dsCnpj"].value = dsCnpj;
	document.forms[0].elements["dsEndereco"].value = dsEndereco;
	document.forms[0].elements["dsNumero"].value = dsNumero;
	document.forms[0].elements["dsBairro"].value = dsBairro;
	document.forms[0].elements["dsCep"].value = dsCep;
	document.forms[0].elements["dsUf"].value = dsUf;
	document.forms[0].elements["dsCidade"].value = dsCidade;
	document.forms[0].elements["dsContato"].value = dsContato;
	document.forms[0].elements["dsTelefone"].value = dsTelefone;
	document.forms[0].elements["dsEmail"].value = dsEmail;	
	document.forms[0].elements["emcoDhInativo"].value =emcoDhInativo; 
	
	document.forms[0].userAction.value = "alterar";
	document.forms[0].elements["alterando"].value= "altera";
	document.forms[0].submit();
}

function bloqueiaTodosCamposEditar(){
	document.forms[0].elements["emcoDsEmpresa"].disabled="true";
	document.forms[0].elements["emcoDsCnpj"].value="";	
	document.forms[0].elements["emcoDsCnpj"].disabled="true";	
	document.forms[0].elements["emcoDsEndereco"].disabled="true";
	document.forms[0].elements["emcoDsNumero"].value="";
	document.forms[0].elements["emcoDsNumero"].disabled="true";
	document.forms[0].elements["emcoDsBairro"].disabled="true";
	document.forms[0].elements["emcoDsCep"].value="";
	document.forms[0].elements["emcoDsCep"].disabled="true";
	document.forms[0].elements["emcoDsUf"].disabled="true";
	document.forms[0].elements["emcoDsCidade"].disabled="true";
	document.forms[0].elements["emcoDsContato"].disabled="true";
	document.forms[0].elements["emcoDsTelefone"].disabled="true";
	document.forms[0].elements["emcoDsEmail"].disabled="true";
	document.forms[0].elements["inativo"].disabled="true";
}

function salvar(){
	if (document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado'){
		alert("� necess�rio estar incluindo ou editando um item para poder salv�-lo ");
		return;
	}
	if(trim(document.forms[0].elements["emcoDsEmpresa"].value) == ""){
		alert("Por favor digite o nome da Empresa");
		return;
	}
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	//CHECANDO O INATIVO
	if(document.forms[0].elements["inativo"].checked){
		document.forms[0].elements["check"].value = 'INATIVO';
	}else{
		document.forms[0].elements["check"].value = 'ATIVO';
	}
	if(podeGravarAssociacao()==false){
		return false;
	}
	
	document.forms[0].elements["idEmcoCdEmpreco"].disabled = false;

	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;
}

function verificaCampos(tipoAba, habilitaCampos){
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
		limparCampos();
	}
}

function pesquisarcep(){
	var tamanho = document.forms[0].elements["emcoDsCep"].value
	if(tamanho.length < 8){
		alert("Digite um CEP primeiro, por favor.");
		return;
	}

	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "pesquisarcep";
	document.forms[0].submit();
}

function limparCampos(){	
	document.forms[0].elements["emcoDsEmpresa"].value = "";
	document.forms[0].elements["emcoDsCnpj"].value = "";
	document.forms[0].elements["emcoDsEndereco"].value = "";
	document.forms[0].elements["emcoDsNumero"].value = "";
	document.forms[0].elements["emcoDsBairro"].value = "";
	document.forms[0].elements["emcoDsCep"].value = "";
	document.forms[0].elements["emcoDsUf"].value = "";
	document.forms[0].elements["emcoDsCidade"].value = "";
	document.forms[0].elements["emcoDsContato"].value = "";
	document.forms[0].elements["emcoDsTelefone"].value = "";
	document.forms[0].elements["emcoDsEmail"].value = "";
	document.forms[0].elements["inativo"].checked = true 
	
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();	
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function fecharAguarde(){
	
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function verificar(){
	if(document.forms[0].elements["compara"].value  == "N"){
		alert("CEP n�o localizado");
		return;
	}
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	setaListaBloqueia();
	setaIdiomaBloqueia();
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
	setaListaHabilita();
 	setaIdiomaHabilita();			
	break;
}
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(document.forms[0].elements["idEmpresaCobranca"].value);
}

function habilitarCampos(){
	empresaCobrancaForm.emcoDsEmpresa.disabled = false;
	empresaCobrancaForm.emcoDsCnpj.disabled = false;
	empresaCobrancaForm.emcoDsEndereco.disabled=false;
	empresaCobrancaForm.emcoDsNumero.disabled = false;
	empresaCobrancaForm.emcoDsBairro.disabled = false;
	empresaCobrancaForm.emcoDsCep.disabled = false;
	empresaCobrancaForm.emcoDsUf.disabled = false;
	empresaCobrancaForm.emcoDsCidade.disabled = false;
	empresaCobrancaForm.emcoDsContato.disabled = false;
	empresaCobrancaForm.emcoDsTelefone.disabled = false;
	empresaCobrancaForm.emcoDsEmail.disabled = false;
	empresaCobrancaForm.inativo.disabled = false;
}

function desabilitaCamposEmpresaCobranca(){
	empresaCobrancaForm.emcoDsEmpresa.disabled = true;
	empresaCobrancaForm.emcoDsCnpj.disabled = true;
	empresaCobrancaForm.emcoDsEndereco.disabled=true;
	empresaCobrancaForm.emcoDsNumero.disabled = true;
	empresaCobrancaForm.emcoDsBairro.disabled = true;
	empresaCobrancaForm.emcoDsCep.disabled = true;
	empresaCobrancaForm.emcoDsUf.disabled = true;
	empresaCobrancaForm.emcoDsCidade.disabled = true;
	empresaCobrancaForm.emcoDsContato.disabled = true;
	empresaCobrancaForm.emcoDsTelefone.disabled = true;
	empresaCobrancaForm.emcoDsEmail.disabled = true;
	empresaCobrancaForm.inativo.disabled = true;
	setPermissaoImageDisable(false, document.empresaCobrancaForm.imgBuscarCep);
}

function validaEmail(email)
{
	if (email.value.search(/\S/) != -1) {
			regExp = /[A-Za-z0-9_]+@[A-Za-z0-9_-]{2,}\.[A-Za-z]{2,}/
			if (email.value.length < 7 || email.value.search(regExp) == -1){
				alert('Por favor preencha corretamente o seu e-mail!');
			    email.focus();
			    return false;
			}						
		}
		num1 = email.value.indexOf("@");
		num2 = email.value.lastIndexOf("@");
		if (num1 != num2){
		    alert('Por favor preencha corretamente o seu e-mail!');
		    email.focus();
			return false;
		}
}

function trim(cStr){
	if (typeof(cStr) != "undefined"){
		var re = /^\s+/
		cStr = cStr.replace (re, "")
		re = /\s+$/
		cStr = cStr.replace (re, "")
		return cStr
	}
	else
		return ""
}