var countRegistro = new Number(0);
var countRegistrosGrupoParc = new Number(0);
var qtdadeRegistrosComissao = new Number(0);
var qtdadeRegistrosEntrada = new Number(0);
var qtdadeRegistrosParcela = new Number(0);
var qtdadeRegistrosRegraNegociacao = new Number(0);
var temPermissaoIncluir = false;
var temPermissaoAlterar = false;
var temPermissaoExcluir = false;
var editando = false;

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide', 'um','','hide','dois','','hide','tres','','hide','quatro','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdGrupoRegra','principalPstQuadroLinkNormalMaior');	
	setaListaBloqueia();
	setaIdiomaBloqueia();
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show', 'um','','show','dois','','hide','tres','','hide','quatro','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdGrupoRegra','principalPstQuadroLinkSelecionadoMaior');
	SetClassFolder('tdUm','principalPstQuadroLinkSelecionadoMaior');
	SetClassFolder('tdDois','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdTres','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdQuatro','principalPstQuadroLinkNormalMaior');
	setaListaHabilita();
 	setaIdiomaHabilita();			
	break;
	
case 'UM':
	MM_showHideLayers('um','','show','dois','','hide','tres','','hide','quatro','','hide')
	SetClassFolder('tdUm','principalPstQuadroLinkSelecionadoMaior');
	SetClassFolder('tdDois','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdTres','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdQuatro','principalPstQuadroLinkNormalMaior');
	break;

case 'DOIS':
	MM_showHideLayers('um','','hide','dois','','show','tres','','hide','quatro','','hide')
	SetClassFolder('tdUm','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdDois','principalPstQuadroLinkSelecionadoMaior');
	SetClassFolder('tdTres','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdQuatro','principalPstQuadroLinkNormalMaior');
	break;

case 'TRES':
	MM_showHideLayers('um','','hide','dois','','hide','tres','','show','quatro','','hide')
	SetClassFolder('tdUm','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdDois','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdTres','principalPstQuadroLinkSelecionadoMaior');
	SetClassFolder('tdQuatro','principalPstQuadroLinkNormalMaior');
	break;
	
case 'QUATRO':
	MM_showHideLayers('um','','hide','dois','','hide','tres','','hide','quatro','','show')
	SetClassFolder('tdUm','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdDois','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdTres','principalPstQuadroLinkNormalMaior');
	SetClassFolder('tdQuatro','principalPstQuadroLinkSelecionadoMaior');
	break;	
}
	parent.document.all.item('LayerAguarde').style.display = 'none';
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}

function inicio() {
	fecharAguarde();	
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(document.forms[0].elements["id_grre_cd_gruporene"].value);
	
	carregaLstGrupoRegrasNegociacao();
}

function novo() {
	parent.document.all.item('LayerAguarde').style.display = 'block';
	limparCampos();	
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();
	
	parent.setTimeout("window.top.ifrmConteudo.AtivarPasta('editar');", 500);
}

function limparCampos() {	
	cadastroGrupoRegrasNegociacaoForm.grre_ds_gruporene.value = "";
	//cadastroGrupoRegrasNegociacaoForm.grreVlValordescpagvista.value = "";
	cadastroGrupoRegrasNegociacaoForm.grre_ds_comandosql.value = "";
}

function cancelar() {
	parent.document.all.item('LayerAguarde').style.display = 'block';	
	limparCampos();	
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function fecharAguarde() {
	parent.document.all.item('LayerAguarde').style.display = 'none';
}

function habilitarCampos() {
	cadastroGrupoRegrasNegociacaoForm.grre_ds_gruporene.disabled = false;
	//cadastroGrupoRegrasNegociacaoForm.grreVlValordescpagvista.disabled = false;
	cadastroGrupoRegrasNegociacaoForm.inativo.disabled = false;
	cadastroGrupoRegrasNegociacaoForm.grre_ds_comandosql.disabled = false;
	
	habilitaDesabilitaTodosElementosDoForm(false);
}

function desabilitaCampos() {
	cadastroGrupoRegrasNegociacaoForm.grre_ds_gruporene.disabled = true;
	//cadastroGrupoRegrasNegociacaoForm.grreVlValordescpagvista.disabled = true;
	cadastroGrupoRegrasNegociacaoForm.inativo.disabled = true;
	cadastroGrupoRegrasNegociacaoForm.grre_ds_comandosql.disabled = true;
	
	habilitaDesabilitaTodosElementosDoForm(true);
}

function habilitaDesabilitaTodosElementosDoForm(valor) {
	for(i = 0; i < document.forms[0].length; i++) {
		if(document.forms[0].elements[i].name=="id_grre_cd_gruporene") continue;
		document.forms[0].elements[i].disabled = valor;				
	}
}

function habilitaDesabilitaImagensAbaRegrasNegociacao() {
	//Habilita/Desabilita as imagens da aba de Regra de Negociacao
	//setPermissaoImageDisable(!temPermissaoAlterar, $('imgAdicionarRegraNegociacao'));

	for(i = 0; i < qtdadeRegistrosRegraNegociacao; i++) {
		habilitaDesabilitaImagem($('btExcluirRegraNegociacao'+i), temPermissaoExcluir);
		habilitaDesabilitaImagem($('btDetalhesRegraNegociacao'+i), temPermissaoAlterar);
	}
}

function habilitaDesabilitaImagensAbaParcelamento() {
	//Habilita/Desabilita as imagens da aba de Parcelamento
	//setPermissaoImageDisable(temPermissaoAlterar, $('imgAdicionarParcela'));
	
	for(i = 0; i < qtdadeRegistrosParcela; i++) {
		habilitaDesabilitaImagem($('btExcluirParcela'+i), temPermissaoExcluir);
		habilitaDesabilitaImagem($('btDetalhesParcela'+i), temPermissaoAlterar);
		habilitaDesabilitaImagem($('btDescontoParcela'+i), temPermissaoAlterar);
		
	}
}

function habilitaDesabilitaImagensAbaEntrada() {
	//Habilita/Desabilita as imagens da aba de Entrada
	//setPermissaoImageDisable(!temPermissaoAlterar, $('imgAdicionarEntrada'));
	
	for(i = 0; i < qtdadeRegistrosEntrada; i++) {
		habilitaDesabilitaImagem($('btExcluirEntrada'+i), temPermissaoExcluir);
	}
}

function habilitaDesabilitaImagensAbaComissao() {	
	//Habilita/Desabilita as imagens da aba de Comiss�o	
	//setPermissaoImageDisable(!temPermissaoAlterar, $('imgAdicionarComissao'));
	
	for(i = 0; i < qtdadeRegistrosComissao; i++) {
		habilitaDesabilitaImagem($('btExcluirComissao'+i), temPermissaoExcluir);
	}
}

function salvarGrupoRegrasNegociacao() {
	if (document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado') {
		alert('� necess�rio incluir ou editar um item para poder gravar');
		return false;
	}
	
	if(document.forms[0].grre_ds_gruporene.value == '') {
		alert('O campo descri��o � obrigat�rio ');
		document.forms[0].grre_ds_gruporene.focus();
		return false;
	}
	
	for(var i = 0; i < qtdadeRegistrosRegraNegociacao; i++) {
		if(document.forms[0].id_cane_cd_camponegonegoarray[i].value=='') {
			alert('� necess�rio selecionar o Campo de Regra para as Regras de Negocia��o.');
			return false;
		}
	}
	
	if(podeGravarAssociacao() == false){
		return false;
	}
	
	var ajax = new ConsultaBanco("", "SalvarGrupoRegrasNegociacao.do");
	
	if(document.forms[0].inativo.checked) {
		ajax.addField("inativo", 'S');
	} else {
		ajax.addField("inativo", 'N');
		document.forms[0].grre_dh_inativo.value = '';
	}	
	
	ajax.addField("id_grre_cd_gruporene", document.forms[0].id_grre_cd_gruporene.value);
	ajax.addField("grre_ds_gruporene", document.forms[0].grre_ds_gruporene.value);
	ajax.addField("grre_ds_comandosql", document.forms[0].grre_ds_comandosql.value);
	ajax.addField("grre_dh_inativo", document.forms[0].grre_dh_inativo.value);
	
	//Seta os campos da aba Regras de Negocia��o
	setaCamposRegraNegociacao(ajax);
	
	//Seta os campos da aba Parcelamento
	setaCamposParcela(ajax);
	
	//Seta os campos da aba Entrada
	setaCamposEntrada(ajax);
	
	//Seta os campos da aba Comiss�o
	setaCamposComissao(ajax);
		
	parent.document.all.item('LayerAguarde').style.display = 'block';
	
	ajax.executarConsulta(retornoSalvarGrupoRegrasNegociacao, true, true);
}

function retornoSalvarGrupoRegrasNegociacao(ajax) {
	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}

	var rs = ajax.getRecordset();

	if(rs == null || rs.getSize() == 0) {
		parent.document.all.item('LayerAguarde').style.display = 'none';	
		return;
	}

	while(rs.next()) {
		if(rs.get('sucesso') != null && rs.get('sucesso') == 'true') {
			carregaLstGrupoRegrasNegociacao();		
		}
	}
}

function carregaLstGrupoRegrasNegociacao() {
	var ajax = new ConsultaBanco("", "CarregaLstGrupoRegrasNegociacao.do");
	
	ajax.addField("filtro", document.forms[0].filtro.value);
	
	parent.document.all.item('LayerAguarde').style.display = 'block';
	
	ajax.executarConsulta(listarGrupoRegrasNegociacao, true, true);
}

function listarGrupoRegrasNegociacao(ajax) {
	removeAllNonPrototipeRows("rowGrupoRegrasNegociacao", "tableGrupoRegrasNegociacao");

	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}

	var rs = ajax.getRecordset();

	if(rs == null || rs.getSize() == 0) {
		parent.document.all.item('LayerAguarde').style.display = 'none';
		document.all.item('nenhumGrupoRegrasNegociacao').style.display = "block";
		
		countRegistro = new Number(0);
		
		return;
	}

	document.all.item('nenhumGrupoRegrasNegociacao').style.display = "none";

	while(rs.next()) {
		cloneNode("rowGrupoRegrasNegociacao", { idSuffix:"" + (rs.getCurr()) });
		
		$("rowGrupoRegrasNegociacao" + (rs.getCurr())).indice = rs.getCurr();
		$("rowGrupoRegrasNegociacao" + (rs.getCurr())).id_grre_cd_gruporene = rs.get('id_grre_cd_gruporene');

		$("tdCodigo" + (rs.getCurr())).innerHTML = getAcronym(rs.get('id_grre_cd_gruporene'), 10) + '&nbsp;';
		$("tdDescricao" + (rs.getCurr())).innerHTML = getAcronym(rs.get('grre_ds_gruporene'), 50) + '&nbsp;';
		$("tdInativo" + (rs.getCurr())).innerHTML = getAcronym(rs.get('grre_dh_inativo'), 10) + '&nbsp;';
	
		countRegistro++;

		$("rowGrupoRegrasNegociacao" + (rs.getCurr())).style.display = "block";
	}

	AtivarPasta('pesquisar');
	
	parent.document.all.item('LayerAguarde').style.display = 'none';
}

function excluirGrupoRegrasNegociacao(id_grre_cd_gruporene) {
	if(confirm('Confirma exclus�o ?')) {
		var ajax = new ConsultaBanco("", "ExcluirGrupoRegrasNegociacao.do");
		
		ajax.addField("id_grre_cd_gruporene", id_grre_cd_gruporene);
		
		parent.document.all.item('LayerAguarde').style.display = 'block';
		
		ajax.executarConsulta(retornoExcluirGrupoRegrasNegociacao, true, true);
	}
}

function retornoExcluirGrupoRegrasNegociacao(ajax) {
	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}

	var rs = ajax.getRecordset();

	if(rs == null || rs.getSize() == 0) {
		parent.document.all.item('LayerAguarde').style.display = 'none';
		return;
	}
	
	if(rs.next()) {
		if(rs.get('sucesso') != null && rs.get('sucesso') == 'true') {
			carregaLstGrupoRegrasNegociacao();		
		}		
	}	
}

function editarGrupoRegrasNegociacao(id_grre_cd_gruporene) {
	var ajax = new ConsultaBanco("", "EditarGrupoRegrasNegociacao.do");
		
	ajax.addField("id_grre_cd_gruporene", id_grre_cd_gruporene);
		
	parent.document.all.item('LayerAguarde').style.display = 'block';
		
	ajax.executarConsulta(retornoEditarGrupoRegrasNegociacao, true, true);
}

function retornoEditarGrupoRegrasNegociacao(ajax) {
	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}

	var rs = ajax.getRecordset();

	if(rs == null || rs.getSize() == 0) {
		parent.document.all.item('LayerAguarde').style.display = 'none';
		return;
	}
	
	if(rs.next()) {
		editando = true;
		
		document.forms[0].id_grre_cd_gruporene.value = rs.get('id_grre_cd_gruporene');
		document.forms[0].grre_ds_gruporene.value = rs.get('grre_ds_gruporene');
		document.forms[0].grre_ds_comandosql.value = rs.get('grre_ds_comandosql');
		document.forms[0].grre_dh_inativo.value = rs.get('grre_dh_inativo');
		
		if(rs.get('grre_dh_inativo') != null && rs.get('grre_dh_inativo') != '') {
			document.forms[0].inativo.checked = true;
		}
		
		if(document.forms[0].id_grre_cd_gruporene.value != '' && document.forms[0].id_grre_cd_gruporene.value != '0') {
			carregaLstRegraNegociacao();
			carregaLstComissao();
			carregaLstEntrada();
			carregaLstParcela();
		}
		
		editando = false;
	}
		
	AtivarPasta('editar');
}

function limpaCamposFaixaComissao(indice) {
	document.forms[0].grco_vl_faixainicialarray[indice].value = '';
	document.forms[0].grco_vl_faixafinalarray[indice].value = '';
}

function setaCamposComissao(ajax) {
	ajax.addField("comissaoViewState", document.forms[0].comissaoViewState.value);
	ajax.addField("tipoVState", "comissaoViewState");
	
	for(var i = 0; i < qtdadeRegistrosComissao; i++) {
		ajax.addField("tipoOperacaoGrco", document.forms[0].tipoOperacaoGrco[i].value);
		ajax.addField("id_grco_cd_gruporenecomissaarray", document.forms[0].id_grco_cd_gruporenecomissaarray[i].value);
		ajax.addField("id_cane_cd_camponegoarray", document.forms[0].id_cane_cd_camponegoarray[i].value);
		ajax.addField("grco_ds_gruporenecomissaarray", document.forms[0].grco_ds_gruporenecomissaarray[i].value);
		ajax.addField("grco_vl_faixainicialarray", document.forms[0].grco_vl_faixainicialarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("grco_vl_faixafinalarray", document.forms[0].grco_vl_faixafinalarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("grco_in_tpcomoperadoraarray", document.forms[0].grco_in_tpcomoperadoraarray[i].value);
		ajax.addField("grco_vl_comoperadoraarray", document.forms[0].grco_vl_comoperadoraarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("grco_in_tpcomoperadorarray", document.forms[0].grco_in_tpcomoperadorarray[i].value);
		ajax.addField("grco_vl_comoperadorarray", document.forms[0].grco_vl_comoperadorarray[i].value.replace(".","").replace(".","").replace(".",""));
	}
}

function carregaLstComissao() {
	if(editando) {
		var ajax = new ConsultaBanco("", "CarregaLstComissao.do");	
		
		ajax.addField("id_grre_cd_gruporene", document.forms[0].id_grre_cd_gruporene.value);
			
		ajax.executarConsulta(retornoPopulaLinhasComissao, true, true);
	}
}

function adicionarLinhaComissao() {
	var ajax = new ConsultaBanco("", "AdicionarLinha.do");	
	
	ajax.addField("adicionaLinha", "true");
	
	setaCamposComissao(ajax);
	
	parent.document.all.item('LayerAguarde').style.display = 'block';
	
	ajax.executarConsulta(retornoPopulaLinhasComissao, true, true);
}

function excluirLinhaComissao(indice) {
	if(confirm('O registro ser� exclu�do permanentemente!\nDeseja Continuar?')) {
		var ajax = new ConsultaBanco("", "ExcluirLinha.do");	
		
		ajax.addField("comissaoViewState", document.forms[0].comissaoViewState.value);
		ajax.addField("tipoVState", "comissaoViewState");
		ajax.addField("indice", indice);

		parent.document.all.item('LayerAguarde').style.display = 'block';
		
		ajax.executarConsulta(retornoPopulaLinhasComissao, true, true);
	}
}

function retornoPopulaLinhasComissao(ajax) {
	removeAllNonPrototipeRows("rowComissao", "tableComissao");

	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}
	
	document.forms[0].comissaoViewState.value = ajax.getViewState();
	
	var rs = ajax.getRecordset();

	qtdadeRegistrosComissao = new Number(0);
	
	if(rs == null || rs.getSize() == 0) {
		parent.document.all.item('LayerAguarde').style.display = 'none';		
		return;
	}
	
	while(rs.next()) {	
		cloneNode("rowComissao", { idSuffix:"" + (rs.getCurr()) });

		$("rowComissao" + (rs.getCurr())).indice = rs.getCurr();

		//if(rs.get('id_cane_cd_camponego') != null && rs.get('id_cane_cd_camponego') != '') {
			document.forms[0].id_grco_cd_gruporenecomissaarray[rs.getCurr()].value = rs.get('id_grco_cd_gruporenecomissa') != null ? rs.get('id_grco_cd_gruporenecomissa') : '';
			document.forms[0].tipoOperacaoGrco[rs.getCurr()].value = rs.get('tipooperacaogrco') != null ? rs.get('tipooperacaogrco') : '';
			document.forms[0].id_cane_cd_camponegoarray[rs.getCurr()].value = rs.get('id_cane_cd_camponego') != null ? rs.get('id_cane_cd_camponego') : '';
			document.forms[0].grco_ds_gruporenecomissaarray[rs.getCurr()].value = rs.get('grco_ds_gruporenecomissa') != null ? rs.get('grco_ds_gruporenecomissa') : '';			
			
			if(rs.get('grco_vl_faixainicial') != null && rs.get('grco_vl_faixainicial') != '') {
				document.forms[0].grco_vl_faixainicialarray[rs.getCurr()].value = rs.get('grco_vl_faixainicial');	
			} else if(rs.get('grco_nr_faixainicial') != null && rs.get('grco_nr_faixainicial') != '') {
				document.forms[0].grco_vl_faixainicialarray[rs.getCurr()].value = rs.get('grco_nr_faixainicial');	
			} else if(rs.get('grco_dh_faixainicial') != null && rs.get('grco_dh_faixainicial') != '') {
				document.forms[0].grco_vl_faixainicialarray[rs.getCurr()].value = rs.get('grco_dh_faixainicial');	
			}
			
			if(rs.get('grco_vl_faixafinal') != null && rs.get('grco_vl_faixafinal') != '') {
				document.forms[0].grco_vl_faixafinalarray[rs.getCurr()].value = rs.get('grco_vl_faixafinal');	
			} else if(rs.get('grco_nr_faixafinal') != null && rs.get('grco_nr_faixafinal') != '') {
				document.forms[0].grco_vl_faixafinalarray[rs.getCurr()].value = rs.get('grco_nr_faixafinal');	
			} else if(rs.get('grco_dh_faixafinal') != null && rs.get('grco_dh_faixafinal') != '') {
				document.forms[0].grco_vl_faixafinalarray[rs.getCurr()].value = rs.get('grco_dh_faixafinal');	
			}			
			
			document.forms[0].grco_in_tpcomoperadoraarray[rs.getCurr()].value = rs.get('grco_in_tpcomoperadora') != null ? rs.get('grco_in_tpcomoperadora') : '';
			document.forms[0].grco_vl_comoperadoraarray[rs.getCurr()].value = rs.get('grco_vl_comoperadora') != null ? rs.get('grco_vl_comoperadora') : '';
			document.forms[0].grco_in_tpcomoperadorarray[rs.getCurr()].value = rs.get('grco_in_tpcomoperador') != null ? rs.get('grco_in_tpcomoperador') : '';
			document.forms[0].grco_vl_comoperadorarray[rs.getCurr()].value = rs.get('grco_vl_comoperador') != null ? rs.get('grco_vl_comoperador') : '';
		//}
						
		qtdadeRegistrosComissao++;

		$("rowComissao" + (rs.getCurr())).style.display = "table-row";
	}
	
	habilitaDesabilitaImagensAbaComissao();

	parent.document.all.item('LayerAguarde').style.display = 'none';
}

function carregaLstParcela() {
	if(editando) {
		var ajax = new ConsultaBanco("", "CarregaLstParcela.do");	
		
		ajax.addField("id_grre_cd_gruporene", document.forms[0].id_grre_cd_gruporene.value);
			
		ajax.executarConsulta(retornoPopulaLinhasParcela, true, true);
	}
}

function adicionarLinhaParcela() {
	var ajax = new ConsultaBanco("", "AdicionarLinha.do");	
	
	ajax.addField("adicionaLinha", "true");
	
	setaCamposParcela(ajax);
	
	parent.document.all.item('LayerAguarde').style.display = 'block';
	
	ajax.executarConsulta(retornoPopulaLinhasParcela, true, true);
}

function excluirLinhaParcela(indice) {
	if(confirm('O registro ser� exclu�do permanentemente!\nDeseja Continuar?')) {
		var ajax = new ConsultaBanco("", "ExcluirLinha.do");	
		
		ajax.addField("parcelamentoViewState", document.forms[0].parcelamentoViewState.value);
		ajax.addField("tipoVState", "parcelamentoViewState");
		ajax.addField("indice", indice);

		parent.document.all.item('LayerAguarde').style.display = 'block';
		
		ajax.executarConsulta(retornoPopulaLinhasParcela, true, true);
	}
}

function retornoPopulaLinhasParcela(ajax) {
	removeAllNonPrototipeRows("rowParcela", "tableParcela");

	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}
	
	document.forms[0].parcelamentoViewState.value = ajax.getViewState();
	
	var rs = ajax.getRecordset();

	qtdadeRegistrosParcela = new Number(0);
	
	if(rs == null || rs.getSize() == 0) {
		parent.document.all.item('LayerAguarde').style.display = 'none';		
		return;
	}
	
	while(rs.next()) {
		cloneNode("rowParcela", { idSuffix:"" + (rs.getCurr()) });

		$("rowParcela" + (rs.getCurr())).indice = rs.getCurr();
		$("rowParcela" + (rs.getCurr())).id_grnp_cd_gruporeneparcela = rs.get('id_grnp_cd_gruporeneparcela');
		
		document.forms[0].id_grnp_cd_gruporeneparcelaarray[rs.getCurr()].value = rs.get('id_grnp_cd_gruporeneparcela') != null ? rs.get('id_grnp_cd_gruporeneparcela') : '';
		
		document.forms[0].id_cane_cd_camponegoparcarray[rs.getCurr()].value = rs.get('id_cane_cd_camponego') != null ? rs.get('id_cane_cd_camponego') : '';
		
		document.forms[0].tipoOperacaoGrpn[rs.getCurr()].value = rs.get('tipooperacaogrpn') != null ? rs.get('tipooperacaogrpn') : '';
					
		if(rs.get('grnp_vl_faixainicial') != null && rs.get('grnp_vl_faixainicial') != '') {
			document.forms[0].grnp_vl_faixainicialarray[rs.getCurr()].value = rs.get('grnp_vl_faixainicial');	
		} else if(rs.get('grnp_nr_faixainicial') != null && rs.get('grnp_nr_faixainicial') != '') {
			document.forms[0].grnp_vl_faixainicialarray[rs.getCurr()].value = rs.get('grnp_nr_faixainicial');	
		} else if(rs.get('grnp_dh_faixainicial') != null && rs.get('grnp_dh_faixainicial') != '') {
			document.forms[0].grnp_vl_faixainicialarray[rs.getCurr()].value = rs.get('grnp_dh_faixainicial');	
		}
		
		if(rs.get('grnp_vl_faixafinal') != null && rs.get('grnp_vl_faixafinal') != '') {
			document.forms[0].grnp_vl_faixafinalarray[rs.getCurr()].value = rs.get('grnp_vl_faixafinal');	
		} else if(rs.get('grnp_nr_faixafinal') != null && rs.get('grnp_nr_faixafinal') != '') {
			document.forms[0].grnp_vl_faixafinalarray[rs.getCurr()].value = rs.get('grnp_nr_faixafinal');	
		} else if(rs.get('grnp_dh_faixafinal') != null && rs.get('grnp_dh_faixafinal') != '') {
			document.forms[0].grnp_vl_faixafinalarray[rs.getCurr()].value = rs.get('grnp_dh_faixafinal');	
		}
		
		document.forms[0].grnp_nr_parcelamentoinicialarray[rs.getCurr()].value = rs.get('grnp_nr_parcelamentoinicial') != null ? rs.get('grnp_nr_parcelamentoinicial') : ''; 
		document.forms[0].grnp_nr_parcelamentofinalarray[rs.getCurr()].value = rs.get('grnp_nr_parcelamentofinal') != null ? rs.get('grnp_nr_parcelamentofinal') : '';
		document.forms[0].grnp_nr_diaspripgtoarray[rs.getCurr()].value = rs.get('grnp_nr_diaspripgto') != null ? rs.get('grnp_nr_diaspripgto') : '0';
		
		document.forms[0].grnp_in_boletagemarray[rs.getCurr()].checked = rs.get('grnp_in_boletagem') != null ? rs.get('grnp_in_boletagem') == "S" ?true:false:false;
		
		
		/*document.forms[0].id_tpco_cd_tpdescontoarray[rs.getCurr()].value = rs.get('id_tpco_cd_tpdesconto') != null ? rs.get('id_tpco_cd_tpdesconto') : '';
		document.forms[0].grnp_vl_descinicialarray[rs.getCurr()].value = rs.get('grnp_vl_descinicial') != null ? rs.get('grnp_vl_descinicial') : ''; 
		document.forms[0].grnp_vl_descfinalarray[rs.getCurr()].value = rs.get('grnp_vl_descfinal') != null ? rs.get('grnp_vl_descfinal') : '';
		document.forms[0].id_tpco_cd_tpdescontoesparray[rs.getCurr()].value = rs.get('id_tpco_cd_tpdescontoesp') != null ? rs.get('id_tpco_cd_tpdescontoesp') : '';
		document.forms[0].grnp_vl_descespinicialarray[rs.getCurr()].value = rs.get('grnp_vl_descespinicial') != null ? rs.get('grnp_vl_descespinicial') : '';
		document.forms[0].grnp_vl_descespfinalarray[rs.getCurr()].value = rs.get('grnp_vl_descespfinal') != null ? rs.get('grnp_vl_descespfinal') : '';
		document.forms[0].grpn_in_descparcelaarray[rs.getCurr()].value = rs.get('grpn_in_descparcela') != null ? rs.get('grpn_in_descparcela') : '';
		*/		
			
		if(!rs.get('id_grnp_cd_gruporeneparcela')) {
			habilitaDesabilitaImagem($('btExcluirParcela'+rs.getCurr()), true);
			habilitaDesabilitaImagem($('btDetalhesParcela'+rs.getCurr()), false);
			habilitaDesabilitaImagem($('btDescontoParcela'+rs.getCurr()), false);
			
		} else {
			habilitaDesabilitaImagem($('btExcluirParcela'+rs.getCurr()), temPermissaoExcluir);
			habilitaDesabilitaImagem($('btDetalhesParcela'+rs.getCurr()), temPermissaoAlterar);
			habilitaDesabilitaImagem($('btDescontoParcela'+rs.getCurr()), temPermissaoAlterar);

		}
		
		/*
		habilitaDesabilitaTpDescontoParcela(document.forms[0].id_tpco_cd_tpdescontoarray[rs.getCurr()].value, rs.getCurr(), '1');
		habilitaDesabilitaTpDescontoParcela(document.forms[0].id_tpco_cd_tpdescontoesparray[rs.getCurr()].value, rs.getCurr(), '2');
		*/
						
		qtdadeRegistrosParcela++;

		$("rowParcela" + (rs.getCurr())).style.display = "table-row";
	}
	
	//habilitaDesabilitaImagensAbaParcelamento();

	parent.document.all.item('LayerAguarde').style.display = 'none';
}

function habilitaDesabilitaImagem(obj, habilita) {
	if(habilita) {
		obj.className = 'geralCursoHand';
		obj.disabled = false;
	} else {
		obj.className = 'geralImgDisable';
		obj.disabled = true;
	}
}

function limpaCamposFaixaParcela(indice) {
	document.forms[0].grnp_vl_faixainicialarray[indice].value = '';
	document.forms[0].grnp_vl_faixafinalarray[indice].value = '';
}

function setaCamposParcela(ajax) {
	ajax.addField("parcelamentoViewState", document.forms[0].parcelamentoViewState.value);
	ajax.addField("tipoVState", "parcelamentoViewState");
	
	for(var i = 0; i < qtdadeRegistrosParcela; i++) {
		ajax.addField("tipoOperacaoGrpn", document.forms[0].tipoOperacaoGrpn[i].value);
		ajax.addField("id_grnp_cd_gruporeneparcelaarray", document.forms[0].id_grnp_cd_gruporeneparcelaarray[i].value);
		ajax.addField("id_cane_cd_camponegoparcarray", document.forms[0].id_cane_cd_camponegoparcarray[i].value);
		ajax.addField("grnp_vl_faixainicialarray", document.forms[0].grnp_vl_faixainicialarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("grnp_vl_faixafinalarray", document.forms[0].grnp_vl_faixafinalarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("grnp_nr_parcelamentoinicialarray", document.forms[0].grnp_nr_parcelamentoinicialarray[i].value); 
		ajax.addField("grnp_nr_parcelamentofinalarray", document.forms[0].grnp_nr_parcelamentofinalarray[i].value);
		ajax.addField("grnp_nr_diaspripgtoarray", document.forms[0].grnp_nr_diaspripgtoarray[i].value);
		
		if(document.forms[0].grnp_in_boletagemarray[i].checked)
			ajax.addField("grnp_in_boletagemarray", "S");
		else
			ajax.addField("grnp_in_boletagemarray", "N");
		
		/*
		ajax.addField("id_tpco_cd_tpdescontoarray", document.forms[0].id_tpco_cd_tpdescontoarray[i].value);
		ajax.addField("grnp_vl_descinicialarray", document.forms[0].grnp_vl_descinicialarray[i].value.replace(".","").replace(".","").replace(".","")); 
		ajax.addField("grnp_vl_descfinalarray", document.forms[0].grnp_vl_descfinalarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("id_tpco_cd_tpdescontoesparray", document.forms[0].id_tpco_cd_tpdescontoesparray[i].value);
		ajax.addField("grnp_vl_descespinicialarray", document.forms[0].grnp_vl_descespinicialarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("grnp_vl_descespfinalarray", document.forms[0].grnp_vl_descespfinalarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("grpn_in_descparcelaarray", document.forms[0].grpn_in_descparcelaarray[i].value);
		*/
	}
}
/*
function habilitaDesabilitaTpDescontoParcela(valor, indice, tipo) {
	var habilita = false;
	
	if(valor == '') {
		habilita = true;
	}
	
	if(tipo == '1') {
		document.forms[0].grnp_vl_descinicialarray[indice].disabled = habilita;
		document.forms[0].grnp_vl_descfinalarray[indice].disabled = habilita;
		
		if(habilita) {
			document.forms[0].grnp_vl_descinicialarray[indice].value = '';
			document.forms[0].grnp_vl_descfinalarray[indice].value = '';
		}		
	} else if(tipo == '2') {
		document.forms[0].grnp_vl_descespinicialarray[indice].disabled = habilita;
		document.forms[0].grnp_vl_descespfinalarray[indice].disabled = habilita;
		
		if(habilita) {
			document.forms[0].grnp_vl_descespinicialarray[indice].value = '';
			document.forms[0].grnp_vl_descespfinalarray[indice].value = '';
		}		
	}
}
*/
function abrirDetalhesParcela(idGrnpCdGruporeneparcela) {
	if(idGrnpCdGruporeneparcela != '') {
		showModalDialog('AbrirPopupTaxas.do?id_grnp_cd_gruporeneparcela=' + idGrnpCdGruporeneparcela, window, 'help:no;scroll:no;Status:NO;dialogWidth:710px;dialogHeight:310px,dialogTop:0px,dialogLeft:650px');
	}
}

function abrirDescontoParcela(idGrnpCdGruporeneparcela) {
	if(idGrnpCdGruporeneparcela != '') {
		showModalDialog('AbrirPopupDesconto.do?id_grnp_cd_gruporeneparcela=' + idGrnpCdGruporeneparcela, window, 'help:no;scroll:no;Status:NO;dialogWidth:710px;dialogHeight:310px,dialogTop:0px,dialogLeft:650px');
	}
}


function limpaCamposFaixaEntrada(indice) {
	document.forms[0].gren_vl_faixainicialarray[indice].value = '';
	document.forms[0].gren_vl_faixafinalarray[indice].value = '';
}

function setaCamposEntrada(ajax) {
	ajax.addField("entradaViewState", document.forms[0].entradaViewState.value);
	ajax.addField("tipoVState", "entradaViewState");
	
	for(var i = 0; i < qtdadeRegistrosEntrada; i++) {
		ajax.addField("tipoOperacaoGren", document.forms[0].tipoOperacaoGren[i].value);
		ajax.addField("id_gren_cd_gruporeneentradaarray", document.forms[0].id_gren_cd_gruporeneentradaarray[i].value);
		ajax.addField("id_cane_cd_camponegoentrarray", document.forms[0].id_cane_cd_camponegoentrarray[i].value);
		ajax.addField("gren_ds_obsarray", document.forms[0].gren_ds_obsarray[i].value);
		ajax.addField("gren_vl_faixainicialarray", document.forms[0].gren_vl_faixainicialarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("gren_vl_faixafinalarray", document.forms[0].gren_vl_faixafinalarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("gren_in_tpentradaarray", document.forms[0].gren_in_tpentradaarray[i].value);
		ajax.addField("gren_vl_valorminimoarray", document.forms[0].gren_vl_valorminimoarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("gren_vl_valormaximoarray", document.forms[0].gren_vl_valormaximoarray[i].value.replace(".","").replace(".","").replace(".",""));
	}
}

function carregaLstEntrada() {
	if(editando) {
		var ajax = new ConsultaBanco("", "CarregaLstEntrada.do");	
		
		ajax.addField("id_grre_cd_gruporene", document.forms[0].id_grre_cd_gruporene.value);
			
		ajax.executarConsulta(retornoPopulaLinhasEntrada, true, true);
	}
}

function adicionarLinhaEntrada() {
	var ajax = new ConsultaBanco("", "AdicionarLinha.do");	
	
	ajax.addField("adicionaLinha", "true");
	
	setaCamposEntrada(ajax);
	
	parent.document.all.item('LayerAguarde').style.display = 'block';
	
	ajax.executarConsulta(retornoPopulaLinhasEntrada, true, true);
}

function excluirLinhaEntrada(indice) {
	if(confirm('O registro ser� exclu�do permanentemente!\nDeseja Continuar?')) {
		var ajax = new ConsultaBanco("", "ExcluirLinha.do");	
		
		ajax.addField("entradaViewState", document.forms[0].entradaViewState.value);
		ajax.addField("tipoVState", "entradaViewState");
		ajax.addField("indice", indice);

		parent.document.all.item('LayerAguarde').style.display = 'block';
		
		ajax.executarConsulta(retornoPopulaLinhasEntrada, true, true);
	}
}

function retornoPopulaLinhasEntrada(ajax) {
	removeAllNonPrototipeRows("rowEntrada", "tableEntrada");

	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}
	
	document.forms[0].entradaViewState.value = ajax.getViewState();
	
	var rs = ajax.getRecordset();

	qtdadeRegistrosEntrada = new Number(0);
	
	if(rs == null || rs.getSize() == 0) {
		parent.document.all.item('LayerAguarde').style.display = 'none';		
		return;
	}
	
	while(rs.next()) {	
		cloneNode("rowEntrada", { idSuffix:"" + (rs.getCurr()) });

		$("rowEntrada" + (rs.getCurr())).indice = rs.getCurr();

		//if(rs.get('id_gren_cd_gruporeneentrada') != null && rs.get('id_gren_cd_gruporeneentrada') != '') {
			document.forms[0].id_gren_cd_gruporeneentradaarray[rs.getCurr()].value = rs.get('id_gren_cd_gruporeneentrada') != null ? rs.get('id_gren_cd_gruporeneentrada') : '';
			document.forms[0].tipoOperacaoGren[rs.getCurr()].value = rs.get('tipooperacaogren') != null ? rs.get('tipooperacaogren') : '';;
			document.forms[0].id_cane_cd_camponegoentrarray[rs.getCurr()].value = rs.get('id_cane_cd_camponego') != null ? rs.get('id_cane_cd_camponego') : '';;
			document.forms[0].gren_ds_obsarray[rs.getCurr()].value = rs.get('gren_ds_obs') != null ? rs.get('gren_ds_obs') : '';;			
			
			if(rs.get('gren_vl_faixainicial') != null && rs.get('gren_vl_faixainicial') != '') {
				document.forms[0].gren_vl_faixainicialarray[rs.getCurr()].value = rs.get('gren_vl_faixainicial');	
			} else if(rs.get('gren_nr_faixainicial') != null && rs.get('gren_nr_faixainicial') != '') {
				document.forms[0].gren_vl_faixainicialarray[rs.getCurr()].value = rs.get('gren_nr_faixainicial');	
			} else if(rs.get('gren_dh_faixainicial') != null && rs.get('gren_dh_faixainicial') != '') {
				document.forms[0].gren_vl_faixainicialarray[rs.getCurr()].value = rs.get('gren_dh_faixainicial');	
			}
			
			if(rs.get('gren_vl_faixafinal') != null && rs.get('gren_vl_faixafinal') != '') {
				document.forms[0].gren_vl_faixafinalarray[rs.getCurr()].value = rs.get('gren_vl_faixafinal');	
			} else if(rs.get('gren_nr_faixafinal') != null && rs.get('gren_nr_faixafinal') != '') {
				document.forms[0].gren_vl_faixafinalarray[rs.getCurr()].value = rs.get('gren_nr_faixafinal');	
			} else if(rs.get('gren_dh_faixafinal') != null && rs.get('gren_dh_faixafinal') != '') {
				document.forms[0].gren_vl_faixafinalarray[rs.getCurr()].value = rs.get('gren_dh_faixafinal');	
			}
			
			document.forms[0].gren_in_tpentradaarray[rs.getCurr()].value = rs.get('gren_in_tpentrada') != null ? rs.get('gren_in_tpentrada') : '';;
			document.forms[0].gren_vl_valorminimoarray[rs.getCurr()].value = rs.get('gren_vl_valorminimo') != null ? rs.get('gren_vl_valorminimo') : '';;
			document.forms[0].gren_vl_valormaximoarray[rs.getCurr()].value = rs.get('gren_vl_valormaximo') != null ? rs.get('gren_vl_valormaximo') : '';;
		//}
						
		qtdadeRegistrosEntrada++;

		$("rowEntrada" + (rs.getCurr())).style.display = "table-row";
	}
	
	habilitaDesabilitaImagensAbaEntrada();

	parent.document.all.item('LayerAguarde').style.display = 'none';
}

function carregaLstRegraNegociacao() {
	if(editando) {
		var ajax = new ConsultaBanco("", "CarregaLstRegraNegociacao.do");	
		
		ajax.addField("id_grre_cd_gruporene", document.forms[0].id_grre_cd_gruporene.value);
			
		ajax.executarConsulta(retornoPopulaLinhasRegraNegociacao, true, true);
	}
}

function adicionarLinhaRegraNegociacao() {
	var ajax = new ConsultaBanco("", "AdicionarLinha.do");	
	
	ajax.addField("adicionaLinha", "true");
	
	setaCamposRegraNegociacao(ajax);
	
	parent.document.all.item('LayerAguarde').style.display = 'block';
	
	ajax.executarConsulta(retornoPopulaLinhasRegraNegociacao, true, true);
}

function excluirLinhaRegraNegociacao(indice) {
	if(confirm('O registro ser� exclu�do permanentemente!\nDeseja Continuar?')) {
		var ajax = new ConsultaBanco("", "ExcluirLinha.do");	
		
		ajax.addField("regrasNegociacaoViewState", document.forms[0].regrasNegociacaoViewState.value);
		ajax.addField("tipoVState", "regrasNegociacaoViewState");
		ajax.addField("indice", indice);

		parent.document.all.item('LayerAguarde').style.display = 'block';
		
		ajax.executarConsulta(retornoPopulaLinhasRegraNegociacao, true, true);
	}
}

function retornoPopulaLinhasRegraNegociacao(ajax) {
	removeAllNonPrototipeRows("rowRegraNegociacao", "tableRegraNegociacao");

	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}
	
	document.forms[0].regrasNegociacaoViewState.value = ajax.getViewState();
	
	var rs = ajax.getRecordset();
	
	qtdadeRegistrosRegraNegociacao = new Number(0);
	
	if(rs == null || rs.getSize() == 0) {
		parent.document.all.item('LayerAguarde').style.display = 'none';		
		return;
	}
	
	while(rs.next()) {
		cloneNode("rowRegraNegociacao", { idSuffix:"" + (rs.getCurr()) });

		$("rowRegraNegociacao" + (rs.getCurr())).indice = rs.getCurr();
		$("rowRegraNegociacao" + (rs.getCurr())).id_rene_cd_regnegociacao = rs.get('id_rene_cd_regnegociacao');
		
		document.forms[0].id_rene_cd_regnegociacaoarray[rs.getCurr()].value = rs.get('id_rene_cd_regnegociacao') != null ? rs.get('id_rene_cd_regnegociacao') : '';
		
		document.forms[0].id_cane_cd_camponegonegoarray[rs.getCurr()].value = rs.get('id_cane_cd_camponego') != null ? rs.get('id_cane_cd_camponego') : '';
		
		document.forms[0].tipoOperacaoRene[rs.getCurr()].value = rs.get('tipooperacaorene') != null ? rs.get('tipooperacaorene') : '';
					
		if(rs.get('rene_vl_faixainicial') != null && rs.get('rene_vl_faixainicial') != '') {
			document.forms[0].rene_vl_faixainicialarray[rs.getCurr()].value = rs.get('rene_vl_faixainicial');	
		} else if(rs.get('rene_nr_faixainicial') != null && rs.get('rene_nr_faixainicial') != '') {
			document.forms[0].rene_vl_faixainicialarray[rs.getCurr()].value = rs.get('rene_nr_faixainicial');	
		} else if(rs.get('rene_dh_faixainicial') != null && rs.get('rene_dh_faixainicial') != '') {
			document.forms[0].rene_vl_faixainicialarray[rs.getCurr()].value = rs.get('rene_dh_faixainicial');	
		}
		
		if(rs.get('rene_vl_faixafinal') != null && rs.get('rene_vl_faixafinal') != '') {
			document.forms[0].rene_vl_faixafinalarray[rs.getCurr()].value = rs.get('rene_vl_faixafinal');	
		} else if(rs.get('rene_nr_faixafinal') != null && rs.get('rene_nr_faixafinal') != '') {
			document.forms[0].rene_vl_faixafinalarray[rs.getCurr()].value = rs.get('rene_nr_faixafinal');	
		} else if(rs.get('rene_dh_faixafinal') != null && rs.get('rene_dh_faixafinal') != '') {
			document.forms[0].rene_vl_faixafinalarray[rs.getCurr()].value = rs.get('rene_dh_faixafinal');	
		}
		
		

		if (!rs.get('id_rene_cd_regnegociacao')) {
			habilitaDesabilitaImagem($('btDetalhesRegraNegociacao'+rs.getCurr()), false);
			habilitaDesabilitaImagem($('btExcluirRegraNegociacao'+rs.getCurr()), true);
		} else {
			habilitaDesabilitaImagem($('btExcluirRegraNegociacao'+rs.getCurr()), temPermissaoExcluir);
			habilitaDesabilitaImagem($('btDetalhesRegraNegociacao'+rs.getCurr()), temPermissaoAlterar);
		}
		
		
								
		qtdadeRegistrosRegraNegociacao++;

		$("rowRegraNegociacao" + (rs.getCurr())).style.display = "table-row";
	}
	
	//habilitaDesabilitaImagensAbaRegrasNegociacao();
	

	parent.document.all.item('LayerAguarde').style.display = 'none';
	
}

function limpaCamposFaixaRegraNegociacao(indice) {
	document.forms[0].rene_vl_faixainicialarray[indice].value = '';
	document.forms[0].rene_vl_faixafinalarray[indice].value = '';
}

function setaCamposRegraNegociacao(ajax) {
	
	ajax.addField("regrasNegociacaoViewState", document.forms[0].regrasNegociacaoViewState.value);
	ajax.addField("tipoVState", "regrasNegociacaoViewState");
	
	for(var i = 0; i < qtdadeRegistrosRegraNegociacao; i++) {
		ajax.addField("tipoOperacaoRene", document.forms[0].tipoOperacaoRene[i].value);
		ajax.addField("id_rene_cd_regnegociacaoarray", document.forms[0].id_rene_cd_regnegociacaoarray[i].value);
		ajax.addField("id_cane_cd_camponegonegoarray", document.forms[0].id_cane_cd_camponegonegoarray[i].value);
		ajax.addField("rene_vl_faixainicialarray", document.forms[0].rene_vl_faixainicialarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("rene_vl_faixafinalarray", document.forms[0].rene_vl_faixafinalarray[i].value.replace(".","").replace(".","").replace(".",""));
	}
}

function abrirDetalhesRegraNegociacao(idReneCdRegnegociacao) {
	if(idReneCdRegnegociacao != '') {
		showModalDialog('AbrirPopupTaxasRene.do?userAction=' + document.forms[0].userAction.value + '&id_rene_cd_regnegociacao=' + idReneCdRegnegociacao, window, 'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:310px,dialogTop:0px,dialogLeft:650px');
	}
}

function getCaneInTipo(valor) {
	var caneInTipoArray = valor.split(';');
	return caneInTipoArray[1];
}

function verificaTipoCampoOnKeyDown(obj, evento, tipo) {
	tipo = getCaneInTipo(tipo);

	if(tipo == 'V') {
		return isDigitoVirgula(evento);
	} else if(tipo == 'N') {
		return isDigito(evento);
	} else if(tipo == 'D') {
		return validaDigito(obj, evento);
	}
	
	return true;
}

function verificaTipoCampoOnBlur(obj, evento, tipo) {
	tipo = getCaneInTipo(tipo);
		
	if(tipo == 'V') {
		//chamado 81229 - Vinicius - inclusao do sinal de menos (-) e libera��o para usar numeros negativos
		//obj.value = obj.value.replace(',','.').replace(',','.').replace(',','.');
		//return numberValidate(obj, 2, '.', ',', evento);
		return numberValidateWithSignal(obj, 2, '.', ',', evento);
	} else if(tipo == 'D') {
		return verificaData(obj);
	}
	
	return true;
}