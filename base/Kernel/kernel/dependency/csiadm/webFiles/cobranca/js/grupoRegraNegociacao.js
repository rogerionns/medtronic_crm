var countRegistro = new Number(0);
var countRegistrosGrupoParc = new Number(0);
var temPermissaoIncluir = false;
var temPermissaoAlterar = false;
var temPermissaoExcluir = false;

function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[grupoRegraNegociacaoForm].userAction.value = "init";
	document.forms[grupoRegraNegociacaoForm].submit();		
}

function pesquisar(){	
	var texto= document.forms[0].elements["grreDsGruporenePesquisar"].value;
	var maiusculas=texto.toUpperCase();
	document.forms[0].elements["grreDsGruporenePesquisar"].value = maiusculas;
	
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();	
	habilitarCampos()
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();
	document.forms[0].elements["alterando"].value = "criar";	
}

function excluir(idGrreCdGruporene){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';		
		document.forms[0].elements["idGrreCdGruporene"].value = idGrreCdGruporene;
		document.forms[0].elements["idGrreCdGruporene"].disabled = false;
		document.forms[0].userAction.value = "excluir";		
		document.forms[0].submit();		
	}	
}

function alterar(idGrreCdGruporene, grreDsGruporene, grreVlValordescpagvista, grreDhInativo, grreDsComandoSQL){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].elements["idGrreCdGruporene"].value = idGrreCdGruporene;
	document.forms[0].elements["grreDsGruporene"].value = grreDsGruporene;
	document.forms[0].elements["grreVlValordescpagvista"].value = grreVlValordescpagvista;
	document.forms[0].elements["grreDhInativo"].value = grreDhInativo;
	
	document.forms[0].elements["grreDsComandosql"].value = grreDsComandoSQL;
	
	document.forms[0].elements["idGrreCdGruporene"].disabled = false;
	
	document.forms[0].userAction.value = "alterar";
	document.forms[0].elements["alterando"].value = "altera";
	document.forms[0].submit();
}

function salvar(){
	if (document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado'){
		alert("� necess�rio estar incluindo ou editando um item para poder salv�-lo ");
		return false;
	}
	
	if(document.forms[0].elements["grreDsGruporene"].value == "") {
		alert("Por favor digite a Descri��o!");
		return false;
	}
	
	if(podeGravarAssociacao()==false){
		return false;
	}
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	//CHECANDO O INATIVO
	if(document.forms[0].elements["inativo"].checked){
		document.forms[0].elements["inativo"].value = 'INATIVO';
	}else{
		document.forms[0].elements["inativo"].value = 'ATIVO';
	}
		
	document.forms[0].elements["idGrreCdGruporene"].disabled = false;

	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;
}

function verificaCampos(tipoAba, habilitaCampos){
	if(tipoAba == 'editar' && habilitaCampos == 'desabilita' ){
		limparCampos();
	}
}

function limparCampos(){	
	grupoRegraNegociacaoForm.grreDsGruporene.value = "";
	grupoRegraNegociacaoForm.grreVlValordescpagvista.value = "";
	grupoRegraNegociacaoForm.grreDsComandosql.value = "";
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();	
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function fecharAguarde(){
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	setaListaBloqueia();
	setaIdiomaBloqueia();
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
	setaListaHabilita();
 	setaIdiomaHabilita();			
	break;
}
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function inicio(){
	fecharAguarde();	
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(document.forms[0].elements["idGrreCdGruporene"].value);
}

function habilitarCampos(){
	grupoRegraNegociacaoForm.grreDsGruporene.disabled = false;
	grupoRegraNegociacaoForm.grreVlValordescpagvista.disabled = false;
	grupoRegraNegociacaoForm.inativo.disabled = false;
	grupoRegraNegociacaoForm.grreDsComandosql.disabled = false;
}

function desabilitaCampos(){
	grupoRegraNegociacaoForm.grreDsGruporene.disabled = true;
	grupoRegraNegociacaoForm.grreVlValordescpagvista.disabled = true;
	grupoRegraNegociacaoForm.inativo.disabled = true;
	grupoRegraNegociacaoForm.grreDsComandosql.disabled = true;
}

function adicionarLinhaEmBranco() {
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	//Habilitando todos os campos antes do submit
	habilitaTodosElementosDoForm();
 
	document.forms[0].userAction.value = "adicionarLinhaEmBranco";
	document.forms[0].submit();
}

function habilitaTodosElementosDoForm() {
	//Habilitando todos os campos antes do submit
	for(i = 0; i < document.forms[0].length; i++) {
		document.forms[0].elements[i].disabled = false;				
	}
}

function excluirRegistroGrupoParcela(indice) {
	var jaEstaGravadoNoBD = false;
	
	if(confirm('Deseja excluir este registro?')) {
		try {
			if(document.forms[0].idGrnpCdGruporeneparcela[indice].value == '') {
				jaEstaGravadoNoBD = false;
			} else {
				jaEstaGravadoNoBD = true;
			}
		} catch(e) {
			if(document.forms[0].idGrnpCdGruporeneparcela.value == '') {
				jaEstaGravadoNoBD = false;
			} else {
				jaEstaGravadoNoBD = true;
			}
		}
	
		if(jaEstaGravadoNoBD) {
			if(countRegistrosGrupoParc > 1) {
				document.forms[0].idGrnpCdGruporeneparcelaExclusao.value = document.forms[0].idGrnpCdGruporeneparcela[indice].value;
			} else {
				document.forms[0].idGrnpCdGruporeneparcelaExclusao.value = document.forms[0].idGrnpCdGruporeneparcela.value;
			}
			
			document.forms[0].elements["idGrreCdGruporene"].disabled = false;
			document.forms[0].userAction.value = "excluirRegistroGrupoParcela";
			document.forms[0].submit();
		} else {
			excluiRegistroNaTela(indice);
		}
	}
}

function excluiRegistroNaTela(indice) {
	objIdTbl = document.getElementById("divRegistro" + indice);
	lstRegistro.removeChild(objIdTbl);
	countRegistrosGrupoParc--;
}