var countRegistro = new Number(0);
var temPermissaoIncluir = false;
var temPermissaoAlterar = false;
var temPermissaoExcluir = false;

function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();		
}

function limparCampos(){
	document.forms[0].elements["idEstrCdEstrategia"].value = "";
	document.forms[0].elements["agendar"].value = "";
	document.forms[0].elements["estrDsEstrategia"].value = "";
	document.forms[0].elements["estrDhValidadede"].value = "";
	document.forms[0].elements["estrDhValidadeate"].value = "";
	//document.forms[0].elements["idAssunto"].value = "";
	document.forms[0].elements["checkado"].value = "";
	/*document.forms[0].elements["filtInCampo"].value = "";
	document.forms[0].elements["filtInOperador"].value = "";
	document.forms[0].elements["filtDsValor"].value = "";
	document.forms[0].elements["filtInJuncao"][0].value = "";
	document.forms[0].elements["filtInJuncao"][1].value = "";
	document.forms[0].elements["filtInSeparador"][0].value = "";
	document.forms[0].elements["filtInSeparador"][1].value = "";*/
}

function verificaCampos(tipoAba, habilitaCampos){	
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
		limparCampos();
	}
}

function fecharAguarde(){
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function criarEstrategia(){
	/*if(document.forms[0].elements["filtInCampo"].value == 0){
		alert("Por favor escolha um campo");
		return;
	}
	if(document.forms[0].elements["filtInOperador"].value == 0){
		alert("Por favor escolha uma opera��o");
		return;
	}
	if(document.forms[0].elements["filtDsValor"].value == ""){
		alert("Por favor digite um valor");
		return;
	}
	if(document.forms[0].elements["filtInJuncao"].value == ""){
		alert("Por favor digite um valor");
		return;
	}
	if(document.forms[0].elements["filtInSeparador"].value == ""){
		alert("Por favor digite um valor");
		return;
	}
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	var filtInCampo = document.forms[0].elements["filtInCampo"].value;	
	var filtInOperador = document.forms[0].elements["filtInOperador"].value;
	var filtDsValor = document.forms[0].elements["filtDsValor"].value;
	var filtInJuncao = document.forms[0].elements["filtInJuncao"].value;
	var filtInSeparador = document.forms[0].elements["filtInSeparador"].value;
	
	
	filtInJuncao = "";
	
	if(document.forms[0].elements["filtInJuncao"][0].checked){
		filtInJuncao = "E"; 
	}else if(document.forms[0].elements["filtInJuncao"][1].checked){
		filtInJuncao = "OU";
	}
	filtInSeparador = "";
	if(document.forms[0].elements["filtInSeparador"][0].checked){
		filtInSeparador = "(";
	}else if(document.forms[0].elements["filtInSeparador"][1].checked){
		filtInSeparador = ")";
	}

	var selIndex = document.forms[0].filtInCampo.selectedIndex;
	document.forms[0].elements["descricaoCampo"].value = document.forms[0].filtInCampo.options[selIndex].text;
	var descricaoCampo = document.forms[0].elements["descricaoCampo"].value
	
	var selIndex1 = document.forms[0].filtInOperador.selectedIndex;
	document.forms[0].elements["descricaoOperacao"].value = document.forms[0].filtInOperador.options[selIndex1].text;
	var descricaoOperacao = document.forms[0].elements["descricaoOperacao"].value;
	window.ifCriteriosSelecao.location.href = "estrategias.do?userAction=criarEstrategia&filtInCampo=" +filtInCampo+ "&descricaoCampo=+" +descricaoCampo+ "&filtInOperador=" +filtInOperador+ "&descricaoOperacao=" +descricaoOperacao+ "&filtDsValor=" +filtDsValor+ "&filtInJuncao=" +filtInJuncao+ "&filtInSeparador=" +filtInSeparador;
	*/
}

function limpaFiltro(){
	//parent.ifCriteriosSelecao.location.href = "estrategias.do?userAction=ifCriteriosSelecao";
}

function excluirCriterioSelecao(idFiltro){
	parent.parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	parent.document.forms[0].elements["idFiltro"].value =idFiltro;
	parent.document.forms[0].userAction.value = "excluirCriterioSelecao";
	parent.document.forms[0].submit();
}

function bloqueiarTodosCampos(){
	document.forms[0].elements["idEstrCdEstrategia"].disabled="true";
	document.forms[0].elements["agendar"].disabled="true";
	document.forms[0].elements["estrDsEstrategia"].disabled="true";
	document.forms[0].elements["estrDhValidadede"].disabled="true";
	document.forms[0].elements["estrDhValidadeate"].disabled="true";
	//document.forms[0].elements["idAssunto"].disabled="true";
	document.forms[0].elements["checkado"].disabled="true";
	/*document.forms[0].elements["filtInCampo"].disabled="true";
	document.forms[0].elements["filtInOperador"].disabled="true";
	document.forms[0].elements["filtDsValor"].disabled="true";
	document.forms[0].elements["filtInJuncao"][0].disabled=true;
	document.forms[0].elements["filtInJuncao"][1].disabled=true;
	document.forms[0].elements["filtInSeparador"][0].disabled=true;
	document.forms[0].elements["filtInSeparador"][1].disabled=true;*/
}

function popupVerCriterios(){
	window.open("estrategias.do?userAction=popupVerCriterios",'x',',0,help=no; scroll=no; Status=NO; width=447px; height=338px; Top=100px; left=200px');
	//window.popupVerCriterios.location.href = "estrategias.do?userAction=popupVerCriterios&filtInCampo=" +filtInCampo+ "&descricaoCampo=" +descricaoCampo+ "&filtInOperador=" +filtInOperador+ "&descricaoOperacao=" +descricaoOperacao+ "&filtDsValor=" +filtDsValor+ "&filtInJuncao=" +filtInJuncao+ "&filtInSeparador=" +filtInSeparador;
}

//function carregaIfCriteriosSelecao(){
//	window.ifCriteriosSelecao.location.href = "estrategias.do?userAction=carregaIfCriteriosSelecao";
//}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);



function SetClassFolderParent(pasta, estilo) {
 stracao = "parent.document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 
function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)	{
	switch (pasta)	{
		case 'pesquisar':
			MM_showHideLayers('Procurar','','show','Estrategias','','hide','criterios','','hide','cenarios','','hide','inclusao','','hide');
			SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
			setaListaBloqueia();
			setaIdiomaBloqueia();
			break;
		case 'editar':
			MM_showHideLayers('Procurar','','hide','Estrategias','','show','criterios','','show');
			SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
			SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
			setaListaHabilita();
 			setaIdiomaHabilita();		
			break;
	}
	//eval(stracao);
}

function AtivarPasta2(pasta)
	{
		switch (pasta)
		{
		case 'CRITERIOS':
			MM_showHideLayers('criterios','','show','cenarios','','hide','inclusao','','hide');
			SetClassFolder('tdCriterios','principalPstQuadroLinkSelecionadoGrd');
			SetClassFolder('tdCenarios','principalPstQuadroLinkNormalGrd');	
			SetClassFolder('tdInclusao','principalPstQuadroLinkNormalGrd');	
			break;
		
		case 'CENARIOS':
			MM_showHideLayers('criterios','','hide','cenarios','','show','inclusao','','hide');
			SetClassFolder('tdCriterios','principalPstQuadroLinkNormalGrd');
			SetClassFolder('tdCenarios','principalPstQuadroLinkSelecionadoGrd');
			SetClassFolder('tdInclusao','principalPstQuadroLinkNormalGrd');			
			break;
		
		case 'INCLUSAO':
			MM_showHideLayers('criterios','','hide','cenarios','','hide','inclusao','','show');
			SetClassFolder('tdCriterios','principalPstQuadroLinkNormalGrd');
			SetClassFolder('tdCenarios','principalPstQuadroLinkNormalGrd');
			SetClassFolder('tdInclusao','principalPstQuadroLinkSelecionadoGrd');			
			break;

	}
	eval(stracao);
}



function AtivarPastaParent(pasta)
	{
		switch (pasta)
		{
		case 'CRITERIOS':
			MM_showHideLayers('criterios','','show','cenarios','','hide','inclusao','','hide');
			SetClassFolderParent('tdCriterios','principalPstQuadroLinkSelecionadoGrd');
			SetClassFolderParent('tdCenarios','principalPstQuadroLinkNormalGrd');	
			SetClassFolderParent('tdInclusao','principalPstQuadroLinkNormalGrd');	
			break;
		
		case 'CENARIOS':
			MM_showHideLayers('criterios','','hide','cenarios','','show','inclusao','','hide');
			SetClassFolderParent('tdCriterios','principalPstQuadroLinkNormalGrd');
			SetClassFolderParent('tdCenarios','principalPstQuadroLinkSelecionadoGrd');
			SetClassFolderParent('tdInclusao','principalPstQuadroLinkNormalGrd');			
			break;
		
		case 'INCLUSAO':
			MM_showHideLayers('criterios','','hide','cenarios','','hide','inclusao','','show');
			SetClassFolderParent('tdCriterios','principalPstQuadroLinkNormalGrd');
			SetClassFolderParent('tdCenarios','principalPstQuadroLinkNormalGrd');
			SetClassFolderParent('tdInclusao','principalPstQuadroLinkSelecionadoGrd');			
			break;

	}
	eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function habilitarCampos(){
	//estrategiasForm.idEstrCdEstrategia.disabled = false;
	estrategiasForm.estrDsEstrategia.disabled = false;
	estrategiasForm.estrDhValidadede.disabled=false;
	estrategiasForm.estrDhValidadeate.disabled = false;

	/*estrategiasForm.filtDsValor.disabled = false;
	estrategiasForm.filtInCampo.disabled = false;
	estrategiasForm.filtInOperador.disabled = false;*/
	//estrategiasForm.idAssunto.disabled = false;
	estrategiasForm.checkado.disabled = false;
}

function desabilitaCamposEstrategia(){
	estrategiasForm.idEstrCdEstrategia.disabled = true;
	estrategiasForm.estrDsEstrategia.disabled = true;
	estrategiasForm.estrDhValidadede.disabled=true;
	estrategiasForm.estrDhValidadeate.disabled = true;
	/*estrategiasForm.filtDsValor.disabled = true;
	estrategiasForm.filtInCampo.disabled = true;
	estrategiasForm.filtInOperador.disabled = true;*/
	//estrategiasForm.idAssunto.disabled = true;
	estrategiasForm.checkado.disabled = true;
	setPermissaoImageDisable(false, document.forms[0].imgExecutarEstrategia);
	setPermissaoImageDisable(false, document.forms[0].imgFecharCenario);
}

function atualizaCmbVisao() {
	parent.ifrmCmbVisao.location.href = 'estrategias.do?userAction=carregaCmbVisao';
}

function excluirCriterioSimulado(idCriterio) {
	if(confirm('Confirma a exclus�o?')){
		parent.ifCriteriosSimulados.location.href = 'estrategias.do?userAction=excluirCriterioSimulado&idCriterio='+idCriterio;
	}
}

function pressEnter(ev) {
    if (ev.keyCode == 13) {
    	pesquisar();
    }
}