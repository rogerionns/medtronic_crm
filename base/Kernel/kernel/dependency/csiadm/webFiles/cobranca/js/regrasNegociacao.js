var countRegistro = new Number(0);
var temPermissaoIncluir = false;
var temPermissaoAlterar = false;
var temPermissaoExcluir = false;

function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	bloqueiaTodosCamposEditar();
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();		
}

function pesquisar(){	
	var texto= document.forms[0].elements["reneDsRegnegociacaoDescricao"].value;
	var maiusculas=texto.toUpperCase();
	document.forms[0].elements["reneDsRegnegociacaoDescricao"].value = maiusculas;
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();	
	habilitaCampos();
	document.forms[0].userAction.value = "novo";
	document.forms[0].elements["alterando"].value ="criar";
	document.forms[0].submit();
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();	
	bloqueiaTodosCamposEditar();
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function excluir(idNegociacao){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';		
		document.forms[0].elements["idNegociacao"].value =idNegociacao;		
		document.forms[0].userAction.value = "excluir";		
		document.forms[0].submit();		
	}	
}

function alterar(idNegociacao, dsRegnegociacao, nrDiaspripag,nrMinparcela,nrMaxparcela,vlJurosparcelamento,nrTaxaadm,vlMindesconto,vlMaxdesconto,vlJurosdiasatraso,nrMulta,vlCustoboleto,inImpressaoboleto,
inIsencaojuros,inIsencaomulta,dhInativo,inPermissaoprimpag,inPermissaoparce,inPermissaodesconto,idInfoboleto,nrDiaspripagmax,vlEntrada,vlPercentualentrada,inPermissaoentrada,nrIof,InTipoCobrancaJuros,InTipoCobrancaMulta,vlSeguro,tpArredondamento,
nrDiasinicio,nrDiasfim,nrSequencia,dsCampocalculo,vlRemunoperador,inTpremunoperador,vlRemunoperadora,inTpremunoperadora,idGrreCdGruporene,dsFormula){																					
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].elements["idNegociacao"].value = idNegociacao;
	document.forms[0].elements["dsRegnegociacao"].value = dsRegnegociacao;
	document.forms[0].elements["nrDiaspripag"].value = nrDiaspripag ;
	document.forms[0].elements["nrMinparcela"].value = nrMinparcela;
	document.forms[0].elements["nrMaxparcela"].value = nrMaxparcela;
	document.forms[0].elements["vlJurosparcelamento"].value = vlJurosparcelamento;
	document.forms[0].elements["nrTaxaadm"].value = nrTaxaadm;
	document.forms[0].elements["vlMindesconto"].value = vlMindesconto;
	document.forms[0].elements["vlMaxdesconto"].value = vlMaxdesconto;
	document.forms[0].elements["vlJurosdiasatraso"].value = vlJurosdiasatraso;
	document.forms[0].elements["nrMulta"].value = nrMulta;
	document.forms[0].elements["vlCustoboleto"].value = vlCustoboleto;
	document.forms[0].elements["inImpressaoboleto"].value = inImpressaoboleto;
	document.forms[0].elements["inIsencaojuros"].value = inIsencaojuros;
	document.forms[0].elements["inIsencaomulta"].value = inIsencaomulta;
	document.forms[0].elements["dhInativo"].value = dhInativo;
	document.forms[0].elements["inPermissaoprimpag"].value = inPermissaoprimpag;
	document.forms[0].elements["inPermissaoparce"].value = inPermissaoparce;	
	document.forms[0].elements["inPermissaodesconto"].value = inPermissaodesconto;
	document.forms[0].elements["idInfoboleto"].value = idInfoboleto;
	document.forms[0].elements["nrDiaspripagmax"].value = nrDiaspripagmax;
	document.forms[0].elements["vlEntrada"].value = vlEntrada;
	document.forms[0].elements["vlPercentualentrada"].value = vlPercentualentrada;
	document.forms[0].elements["inPermissaoentrada"].value = inPermissaoentrada;
	document.forms[0].elements["nrIof"].value = nrIof;
	document.forms[0].elements["InTipoCobrancaJuros"].value = InTipoCobrancaJuros;
	document.forms[0].elements["InTipoCobrancaMulta"].value = InTipoCobrancaMulta;
	document.forms[0].elements["vlSeguro"].value = vlSeguro;
	document.forms[0].elements["tpArredondamento"].value = tpArredondamento;
	document.forms[0].elements["nrDiasinicio"].value = nrDiasinicio;
	document.forms[0].elements["nrDiasfim"].value = nrDiasfim;
	document.forms[0].elements["nrSequencia"].value = nrSequencia;
	document.forms[0].elements["dsCampocalculo"].value = dsCampocalculo;
	document.forms[0].elements["vlRemunoperador"].value = vlRemunoperador;
	document.forms[0].elements["inTpremunoperador"].value = inTpremunoperador;
	document.forms[0].elements["vlRemunoperadora"].value = vlRemunoperadora;
	document.forms[0].elements["inTpremunoperadora"].value = inTpremunoperadora;
	document.forms[0].elements["idGrreCdGruporene"].value = idGrreCdGruporene;
	document.forms[0].elements["dsFormula"].value = dsFormula;

	document.forms[0].userAction.value = "alterar";
	document.forms[0].elements["alterando"].value= "altera";
	document.forms[0].submit();	
}

function salvar(){
	if (document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado'){
		alert("� necess�rio estar incluindo ou editando um item para poder salv�-lo!");
		return;
	}
	
	if(document.forms[0].elements["reneNrSequencia"].value == ""){
		alert("Por favor digite a sequ�ncia para a Regra de Negocia��o!");
		return;
	}
		
	if(document.forms[0].elements["reneDsRegnegociacao"].value == ""){
		alert("Por favor digite a descri��o da Regra de Negocia��o!");
		return;
	}else if(document.forms[0].elements["idGrreCdGruporene"].value == ""){
		alert("Por favor selecione o Grupo Regra de Negocia��o!");
		return;
	}else if(document.forms[0].elements["reneNrDiasinicio"].value == ""){
		alert("Por favor digite a quantidade de dias em atraso m�nima para esta regra!");
		return;
	}else if(document.forms[0].elements["reneNrDiasfim"].value == ""){
		alert("Por favor digite a quantidade de dias em atraso m�xima para esta regra!");
		return;
	}else if(document.forms[0].elements["reneNrDiaspripag"].value == ""){
		alert("Por favor digite a quantidade M�nima de dias para primeiro pagamento!");
		return;
	}else if(document.forms[0].elements["reneNrDiaspripagmax"].value == ""){
		alert("Por favor digite a quantidade M�xima de dias para primeiro pagamento!");
		return;
	}else if(document.forms[0].elements["cmbDsCampocalculo"].value == "0"){
		alert("Por favor selecione a data base para atraso!");
		return;
	}else if(document.forms[0].elements["reneNrMinparcela"].value == ""){
		alert("Por favor digite a Quantidade M�nima de parcelas!");
		return;
	}else if(document.forms[0].elements["reneNrMaxparcela"].value == ""){
		alert("Por favor digite a Quantidade M�xima de parcelas!");
		return;
	}else if((document.forms[0].elements["reneVlPercentualentrada"].value == "")&&(document.forms[0].elements["reneVlEntrada"].value == "")){
		alert("Por favor digite uma forma de Entrada!");
		return;
	}else if((document.forms[0].elements["reneVlPercentualentrada"].value != "")&&(document.forms[0].elements["reneVlEntrada"].value != "")){
		alert("Por favor digite apenas uma forma de Entrada!");
		return;
	}else if(document.forms[0].elements["reneNrMulta"].value == ""){
		alert("Por favor digite a Multa do contrato por atraso!");
		return;
	}else if(!document.forms[0].elements["reneInTipoCobrancaMulta"][0].checked && !document.forms[0].elements["reneInTipoCobrancaMulta"][1].checked){
		alert("Por favor selecione uma forma de Cobran�a de Multa!");
		return; 
	}else if(document.forms[0].elements["reneVlJurosdiasatraso"].value == ""){
		alert("Por favor digite o Juro de mora di�ria do Contrato!");
		return;
	}else if(!document.forms[0].elements["reneInTipoCobrancaJuros"][0].checked && !document.forms[0].elements["reneInTipoCobrancaJuros"][1].checked){
		alert("Por favor selecione uma forma de Cobran�a de Juros!");
		return; 
	}else if(document.forms[0].elements["reneVlJurosparcelamento"].value == ""){
		alert("Por favor digite uma quantidade de % para Parcelamento!");
		return;
	}else if(document.forms[0].elements["reneVlCustoboleto"].value == ""){
		alert("Por favor digite o custo de emiss�o do boleto!");
		return;
	}else if(document.forms[0].elements["reneVlMindesconto"].value == ""){
		alert("Por favor digite o valor m�nimo de desconto!");
		return;
	}else if(document.forms[0].elements["reneVlMaxdesconto"].value == ""){
		alert("Por favor digite o valor m�ximo de desconto!");
		return;
	}else if(document.forms[0].elements["reneVlSeguro"].value == ""){
		alert("Por favor digite o valor da taxa de seguro!");
		return;
	}else if(document.forms[0].elements["reneNrIof"].value == ""){
		alert("Por favor digite o valor do IOF!");
		return;
	}else if(document.forms[0].elements["reneNrTaxaadm"].value == ""){
		alert("Por favor digite um valor para as taxas administrativas!");
		return; 
	}else if(document.forms[0].elements["idInboCdInfoboleto"].value == ""){
		alert("Por favor selecione uma Informa��es para boleto!");
		return; 
	}else if(!document.forms[0].elements["reneInImpressaoboleto"][0].checked && !document.forms[0].elements["reneInImpressaoboleto"][1].checked){
		alert("Por favor selecione uma op��o para impress�o de boleto!");
		return; 
	}else if(!document.forms[0].elements["reneInIsencaojuros"][0].checked && !document.forms[0].elements["reneInIsencaojuros"][1].checked){
		alert("Por favor selecione uma op��o isen��o de juros!");
		return; 
	}else if(!document.forms[0].elements["reneInIsencaomulta"][0].checked && !document.forms[0].elements["reneInIsencaomulta"][1].checked){
		alert("Por favor selecione uma op��o isen��o de multa!");
		return; 	
	}else if((document.forms[0].elements["verInPermissaoprimpag"].checked == "")&&(document.forms[0].elements["altInPermissaoprimpag"].checked == "")){
		alert("Por favor selecione uma op��o para o operador em quantidade de dias!");
		return; 
	}else if((document.forms[0].elements["verInPermissaoparce"].checked == "")&&(document.forms[0].elements["altInPermissaoparce"].checked == "")){
		alert("Por favor selecione uma op��o para o operador em valor fixo de entrada!");
		return;
	//}else if((document.forms[0].elements["verInPermissaoentrada"].checked == "")&&(document.forms[0].elements["altInPermissaoentrada"].checked == "")){
	//	alert("Por favor selecione as duas op��es para o operador em valor fixo de entrada!");
	//	return; 
	}else if((document.forms[0].elements["verInPermissaodesconto"].checked == "")&&(document.forms[0].elements["altInPermissaodesconto"].checked == "")){
		alert("Por favor selecione uma op��o para o operador em valor fixo de entrada!");
		return;		 
	}
	
	//CHECANDO O INATIVO
	if(document.forms[0].elements["checkado"].checked){
		document.forms[0].elements["check"].value = 'INATIVO';
	}else{
		document.forms[0].elements["check"].value = 'ATIVO';
	}
	if(podeGravarAssociacao()==false){
		return false;
	}
	
	if(document.forms[0].elements["cmbDsCampocalculo"].value != ""){
		regrasNegociacaoForm.reneDsCampocalculo.value = document.forms[0].elements["cmbDsCampocalculo"].value;
  	}
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;
	
	
}

function verificaCampos(tipoAba, habilitaCampos){	
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
		limparCampos();
	}
}

function fecharAguarde(){
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	setaListaBloqueia();
	setaIdiomaBloqueia();
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
	setaListaHabilita();
 	setaIdiomaHabilita();			
	break;

}
 //eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
function inicio(){
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(document.forms[0].elements["idReneCdRegnegociacao"].value);
}

function bloqueiaTodosCamposEditar(){
	document.forms[0].elements["idReneCdRegnegociacao"].disabled=true;
	document.forms[0].elements["reneDsRegnegociacao"].disabled=true;
	document.forms[0].elements["reneNrDiaspripag"].disabled=true;
	document.forms[0].elements["reneNrMinparcela"].disabled=true;
	document.forms[0].elements["reneNrMaxparcela"].disabled=true;
	document.forms[0].elements["reneVlJurosparcelamento"].disabled=true;
	document.forms[0].elements["reneNrTaxaadm"].disabled=true;
	document.forms[0].elements["reneVlMindesconto"].disabled=true;
	document.forms[0].elements["reneVlMaxdesconto"].disabled=true;
	document.forms[0].elements["reneVlJurosdiasatraso"].disabled=true;
	document.forms[0].elements["reneNrMulta"].disabled=true;
	document.forms[0].elements["reneVlCustoboleto"].disabled=true;
	document.forms[0].elements["reneInImpressaoboleto"][0].disabled=true;
	document.forms[0].elements["reneInImpressaoboleto"][1].disabled=true;
	document.forms[0].elements["reneInIsencaojuros"][0].disabled=true;
	document.forms[0].elements["reneInIsencaojuros"][1].disabled=true;
	document.forms[0].elements["reneInIsencaomulta"][0].disabled=true;
	document.forms[0].elements["reneInIsencaomulta"][1].disabled=true;
	document.forms[0].elements["checkado"].disabled=true;
	document.forms[0].elements["verInPermissaoprimpag"].disabled=true;
	document.forms[0].elements["altInPermissaoprimpag"].disabled=true;
	document.forms[0].elements["verInPermissaoparce"].disabled=true;
	document.forms[0].elements["altInPermissaoparce"].disabled=true;
	document.forms[0].elements["verInPermissaodesconto"].disabled=true;
	document.forms[0].elements["altInPermissaodesconto"].disabled=true;
	document.forms[0].elements["idInboCdInfoboleto"].disabled=true;
	document.forms[0].elements["reneNrDiaspripagmax"].disabled=true;
	document.forms[0].elements["reneVlEntrada"].disabled=true;
	document.forms[0].elements["reneVlPercentualentrada"].disabled=true;
	document.forms[0].elements["verInPermissaoentrada"].disabled=true;
	document.forms[0].elements["altInPermissaoentrada"].disabled=true;
	document.forms[0].elements["reneNrIof"].disabled=true;
	document.forms[0].elements["reneInTipoCobrancaJuros"][0].disabled=true;
	document.forms[0].elements["reneInTipoCobrancaJuros"][1].disabled=true;
	document.forms[0].elements["reneInTipoCobrancaMulta"][0].disabled=true;
	document.forms[0].elements["reneInTipoCobrancaMulta"][1].disabled=true;
	document.forms[0].elements["reneVlSeguro"].disabled=true;
	document.forms[0].elements["reneInTparredondamento"][0].disabled=true;
	document.forms[0].elements["reneInTparredondamento"][1].disabled=true;
	document.forms[0].elements["reneInTparredondamento"][2].disabled=true;
	document.forms[0].elements["reneNrDiasinicio"].disabled=true;
	document.forms[0].elements["reneNrDiasfim"].disabled=true;
	document.forms[0].elements["reneNrSequencia"].disabled=true;
	document.forms[0].elements["reneDsCampocalculo"].disabled=true;
	document.forms[0].elements["reneVlRemunoperador"].disabled=true;
	document.forms[0].elements["reneInTpremunoperador"][0].disabled=true;
	document.forms[0].elements["reneInTpremunoperador"][1].disabled=true;
	document.forms[0].elements["reneVlRemunoperadora"].disabled=true;
	document.forms[0].elements["reneInTpremunoperadora"][0].disabled=true;
	document.forms[0].elements["reneInTpremunoperadora"][1].disabled=true;
	document.forms[0].elements["reneDsFormula"].disabled=true;
	regrasNegociacaoForm.idGrreCdGruporene.disabled=true;
}


function limparCampos(){	
	//document.forms[0].elements["idReneCdRegnegociacao"].value = "";
	document.forms[0].elements["idReneCdRegnegociacao"].value = "";
	document.forms[0].elements["reneDsRegnegociacao"].value = "";
	document.forms[0].elements["reneNrDiaspripag"].value = "";
	document.forms[0].elements["reneNrMinparcela"].value = "";
	document.forms[0].elements["reneNrMaxparcela"].value = "";
	document.forms[0].elements["reneVlJurosparcelamento"].value = "";
	document.forms[0].elements["reneNrTaxaadm"].value = "";
	document.forms[0].elements["reneVlMindesconto"].value = "";
	document.forms[0].elements["reneVlMaxdesconto"].value = "";
	document.forms[0].elements["reneVlJurosdiasatraso"].value = "";
	document.forms[0].elements["reneNrMulta"].value = "";
	document.forms[0].elements["reneVlCustoboleto"].value = "";
	//document.forms[0].elements["reneInImpressaoboleto"].checked = "";
	document.forms[0].elements["reneInImpressaoboleto"][0].checked=false;
	document.forms[0].elements["reneInImpressaoboleto"][1].checked=false;
	document.forms[0].elements["reneInIsencaojuros"][0].checked=false;
	document.forms[0].elements["reneInIsencaojuros"][1].checked=false;
	document.forms[0].elements["reneInIsencaomulta"][0].checked=false;
	document.forms[0].elements["reneInIsencaomulta"][1].checked=false;
	//document.forms[0].elements["checkado"][0].checked=false;
	document.forms[0].elements["verInPermissaoprimpag"].checked = "";
	document.forms[0].elements["altInPermissaoprimpag"].checked = "";
	document.forms[0].elements["verInPermissaoparce"].checked = "";
	document.forms[0].elements["altInPermissaoparce"].checked = "";
	document.forms[0].elements["altInPermissaodesconto"].checked="";
	//document.forms[0].elements["verInPermissaodesconto"].value="";
	document.forms[0].elements["verInPermissaodesconto"].checked ="";
	document.forms[0].elements["idInboCdInfoboleto"].value = "";
	document.forms[0].elements["reneNrDiaspripagmax"].value = "";
	document.forms[0].elements["reneVlEntrada"].value = "";
	document.forms[0].elements["reneVlPercentualentrada"].value = "";
	document.forms[0].elements["verInPermissaoentrada"].checked = "";
	document.forms[0].elements["altInPermissaoentrada"].checked = "";
	document.forms[0].elements["reneNrIof"].value = "";
	document.forms[0].elements["reneVlSeguro"].value = "";
	//document.forms[0].elements["check"][0].checked=false;
	document.forms[0].elements["reneInTipoCobrancaJuros"][0].checked=false;
	document.forms[0].elements["reneInTipoCobrancaJuros"][1].checked=false;
	document.forms[0].elements["reneInTipoCobrancaMulta"][0].checked=false;
	document.forms[0].elements["reneInTipoCobrancaMulta"][1].checked=false;
	document.forms[0].elements["reneInTparredondamento"][0].checked=false;
	document.forms[0].elements["reneInTparredondamento"][1].checked=false;
	document.forms[0].elements["reneInTparredondamento"][2].checked=false;
	document.forms[0].elements["reneNrDiasinicio"].value = "";
	document.forms[0].elements["reneNrDiasfim"].value = "";
	document.forms[0].elements["reneNrSequencia"].value = "";
	document.forms[0].elements["reneDsCampocalculo"].value = "";
	document.forms[0].elements["reneVlRemunoperador"].value = "";
	document.forms[0].elements["reneInTpremunoperador"][0].checked=false;
	document.forms[0].elements["reneInTpremunoperador"][1].checked=false;
	document.forms[0].elements["reneVlRemunoperadora"].value = "";
	document.forms[0].elements["reneInTpremunoperadora"][0].checked=false;
	document.forms[0].elements["reneInTpremunoperadora"][1].checked=false;
	document.forms[0].elements["reneDsFormula"].value="";

	regrasNegociacaoForm.idGrreCdGruporene.value = "";
}

function habilitaCampos(){
	//document.forms[0].elements["idReneCdRegnegociacao"].disabled=false;
	document.forms[0].elements["reneDsRegnegociacao"].disabled=false;
	document.forms[0].elements["reneNrDiaspripag"].disabled=false;
	document.forms[0].elements["reneNrMinparcela"].disabled=false;
	document.forms[0].elements["reneNrMaxparcela"].disabled=false;
	document.forms[0].elements["reneVlJurosparcelamento"].disabled=false;
	document.forms[0].elements["reneNrTaxaadm"].disabled=false;
	document.forms[0].elements["reneVlMindesconto"].disabled=false;
	document.forms[0].elements["reneVlMaxdesconto"].disabled=false;
	document.forms[0].elements["reneVlJurosdiasatraso"].disabled=false;
	document.forms[0].elements["reneNrMulta"].disabled=false;
	document.forms[0].elements["reneVlCustoboleto"].disabled=false;
	document.forms[0].elements["reneInImpressaoboleto"][0].disabled=false;
	document.forms[0].elements["reneInImpressaoboleto"][1].disabled=false;
	document.forms[0].elements["reneInIsencaojuros"][0].disabled=false;
	document.forms[0].elements["reneInIsencaojuros"][1].disabled=false;
	document.forms[0].elements["reneInIsencaomulta"][0].disabled=false;
	document.forms[0].elements["reneInIsencaomulta"][1].disabled=false;
	document.forms[0].elements["checkado"].disabled=false;
	document.forms[0].elements["verInPermissaoprimpag"].disabled=false;
	document.forms[0].elements["altInPermissaoprimpag"].disabled=false;
	document.forms[0].elements["verInPermissaoparce"].disabled=false;
	document.forms[0].elements["altInPermissaoparce"].disabled=false;
	document.forms[0].elements["verInPermissaodesconto"].disabled=false;
	document.forms[0].elements["altInPermissaodesconto"].disabled=false;	
	document.forms[0].elements["idInboCdInfoboleto"].disabled=false;
	document.forms[0].elements["reneNrDiaspripagmax"].disabled=false;
	document.forms[0].elements["reneVlEntrada"].disabled=false;
	document.forms[0].elements["reneVlPercentualentrada"].disabled=false;
	document.forms[0].elements["verInPermissaoentrada"].disabled=false;
	document.forms[0].elements["altInPermissaoentrada"].disabled=false;
	document.forms[0].elements["reneNrIof"].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaJuros"][0].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaJuros"][1].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaMulta"][0].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaMulta"][1].disabled=false;
	document.forms[0].elements["reneVlSeguro"].disabled=false;
	document.forms[0].elements["reneInTparredondamento"][0].disabled=false;
	document.forms[0].elements["reneInTparredondamento"][1].disabled=false;
	document.forms[0].elements["reneInTparredondamento"][2].disabled=false;
	document.forms[0].elements["reneNrDiasinicio"].disabled=false;
	document.forms[0].elements["reneNrDiasfim"].disabled=false;
	document.forms[0].elements["reneNrSequencia"].disabled=false;
	document.forms[0].elements["reneDsCampocalculo"].disabled=false;
	document.forms[0].elements["reneVlRemunoperador"].disabled=false;
	document.forms[0].elements["reneInTpremunoperador"][0].disabled=false;
	document.forms[0].elements["reneInTpremunoperador"][1].disabled=false;
	document.forms[0].elements["reneVlRemunoperadora"].disabled=false;
	document.forms[0].elements["reneInTpremunoperadora"][0].disabled=false;
	document.forms[0].elements["reneInTpremunoperadora"][1].disabled=false;
	document.forms[0].elements["reneDsFormula"].disabled=false;

	regrasNegociacaoForm.idGrreCdGruporene.disabled = false;
}

function habilitarCampos(){
	//document.forms[0].elements["idReneCdRegnegociacao"].disabled=false;
	document.forms[0].elements["reneDsRegnegociacao"].disabled=false;
	document.forms[0].elements["reneNrDiaspripag"].disabled=false;
	document.forms[0].elements["reneNrMinparcela"].disabled=false;
	document.forms[0].elements["reneNrMaxparcela"].disabled=false;
	document.forms[0].elements["reneVlJurosparcelamento"].disabled=false;
	document.forms[0].elements["reneNrTaxaadm"].disabled=false;
	document.forms[0].elements["reneVlMindesconto"].disabled=false;
	document.forms[0].elements["reneVlMaxdesconto"].disabled=false;
	document.forms[0].elements["reneVlJurosdiasatraso"].disabled=false;
	document.forms[0].elements["reneNrMulta"].disabled=false;
	document.forms[0].elements["reneVlCustoboleto"].disabled=false;
	document.forms[0].elements["reneInImpressaoboleto"][0].disabled=false;
	document.forms[0].elements["reneInImpressaoboleto"][1].disabled=false;
	document.forms[0].elements["reneInIsencaojuros"][0].disabled=false;
	document.forms[0].elements["reneInIsencaojuros"][1].disabled=false;
	document.forms[0].elements["reneInIsencaomulta"][0].disabled=false;
	document.forms[0].elements["reneInIsencaomulta"][1].disabled=false;
	document.forms[0].elements["altInPermissaoprimpag"].disabled=false;
	document.forms[0].elements["verInPermissaoparce"].disabled=false;
	document.forms[0].elements["altInPermissaoparce"].disabled=false;
	document.forms[0].elements["verInPermissaodesconto"].disabled=false;
	document.forms[0].elements["altInPermissaodesconto"].disabled=false;	
	document.forms[0].elements["idInboCdInfoboleto"].disabled=false;
	document.forms[0].elements["reneNrDiaspripagmax"].disabled=false;
	document.forms[0].elements["reneVlEntrada"].disabled=false;
	document.forms[0].elements["reneVlPercentualentrada"].disabled=false;
	document.forms[0].elements["verInPermissaoentrada"].disabled=false;
	document.forms[0].elements["altInPermissaoentrada"].disabled=false;
	document.forms[0].elements["reneNrIof"].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaJuros"][0].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaJuros"][1].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaMulta"][0].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaMulta"][1].disabled=false;
	document.forms[0].elements["reneVlSeguro"].disabled=false;
	document.forms[0].elements["reneInTparredondamento"][0].disabled=false;
	document.forms[0].elements["reneInTparredondamento"][1].disabled=false;
	document.forms[0].elements["reneInTparredondamento"][2].disabled=false;
	document.forms[0].elements["reneNrDiasinicio"].disabled=false;
	document.forms[0].elements["reneNrDiasfim"].disabled=false;
	document.forms[0].elements["reneNrSequencia"].disabled=false;
	document.forms[0].elements["reneDsCampocalculo"].disabled=false;
	document.forms[0].elements["reneVlRemunoperador"].disabled=false;
	document.forms[0].elements["reneInTpremunoperador"][0].disabled=false;
	document.forms[0].elements["reneInTpremunoperador"][1].disabled=false;
	document.forms[0].elements["reneVlRemunoperadora"].disabled=false;
	document.forms[0].elements["reneInTpremunoperadora"][0].disabled=false;
	document.forms[0].elements["reneInTpremunoperadora"][1].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaMulta"][0].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaMulta"][1].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaJuros"][0].disabled=false;
	document.forms[0].elements["reneInTipoCobrancaJuros"][1].disabled=false;
	document.forms[0].elements["reneInImpressaoboleto"][0].disabled=false;
	document.forms[0].elements["reneInImpressaoboleto"][1].disabled=false;
	document.forms[0].elements["reneInIsencaojuros"][0].disabled=false;
	document.forms[0].elements["reneInIsencaojuros"][1].disabled=false;
	document.forms[0].elements["reneInIsencaomulta"][0].disabled=false;
	document.forms[0].elements["reneInIsencaomulta"][1].disabled=false;
	document.forms[0].elements["reneInTpremunoperador"][0].disabled=false;
	document.forms[0].elements["reneInTpremunoperador"][1].disabled=false;
	document.forms[0].elements["reneDsFormula"].disabled=false;

	regrasNegociacaoForm.cmbDsCampocalculo.disabled = false;
	regrasNegociacaoForm.idGrreCdGruporene.disabled = false;
}

function desabilitaCamposRegraNegociacao(){
	regrasNegociacaoForm.idReneCdRegnegociacao.disabled = true;
	regrasNegociacaoForm.reneDsRegnegociacaoDescricao.disabled = true;
	regrasNegociacaoForm.reneDsRegnegociacao.disabled = true;
	regrasNegociacaoForm.reneNrDiaspripag.disabled = true;
	regrasNegociacaoForm.reneNrMinparcela.disabled = true;
	regrasNegociacaoForm.reneNrMaxparcela.disabled = true;
	regrasNegociacaoForm.reneVlJurosparcelamento.disabled = true;
	regrasNegociacaoForm.reneNrTaxaadm.disabled = true;
	regrasNegociacaoForm.reneVlMindesconto.disabled = true;
	regrasNegociacaoForm.reneVlMaxdesconto.disabled = true;
	regrasNegociacaoForm.reneVlJurosdiasatraso.disabled = true;
	regrasNegociacaoForm.reneNrMulta.disabled = true;
	regrasNegociacaoForm.reneVlCustoboleto.disabled = true;
	regrasNegociacaoForm.reneInImpressaoboleto.disabled = true;
	regrasNegociacaoForm.reneInIsencaojuros.disabled = true;
	regrasNegociacaoForm.reneInIsencaomulta.disabled = true;
	regrasNegociacaoForm.verInPermissaoprimpag.disabled = true;
	regrasNegociacaoForm.altInPermissaoprimpag.disabled = true;
	regrasNegociacaoForm.verInPermissaoparce.disabled = true;
	regrasNegociacaoForm.altInPermissaoparce.disabled = true;
	regrasNegociacaoForm.verInPermissaodesconto.disabled = true;
	regrasNegociacaoForm.altInPermissaodesconto.disabled = true;
	regrasNegociacaoForm.idInboCdInfoboleto.disabled = true;
	regrasNegociacaoForm.reneNrDiaspripagmax.disabled = true;
	regrasNegociacaoForm.reneVlEntrada.disabled = true;
	regrasNegociacaoForm.reneVlPercentualentrada.disabled = true;
	regrasNegociacaoForm.verInPermissaoentrada.disabled = true;
	regrasNegociacaoForm.altInPermissaoentrada.disabled = true;
	regrasNegociacaoForm.reneNrIof.disabled = true;
	regrasNegociacaoForm.reneInTipoCobrancaJuros.disabled = true;
	//alert(regrasNegociacaoForm.reneInTipoCobrancaMulta)
	regrasNegociacaoForm.reneInTipoCobrancaMulta.disabled = true;
	regrasNegociacaoForm.reneVlSeguro.disabled = true;
	regrasNegociacaoForm.idNegociacao.disabled = true;
	regrasNegociacaoForm.check.disabled = true;
	regrasNegociacaoForm.idInfoboleto.disabled = true;
	regrasNegociacaoForm.checkado.disabled = true;
	regrasNegociacaoForm.cmbDsCampocalculo.disabled = true;
	document.forms[0].elements["reneInTparredondamento"][0].disabled=true;
	document.forms[0].elements["reneInTparredondamento"][1].disabled=true;
	document.forms[0].elements["reneInTparredondamento"][2].disabled=true;
	document.forms[0].elements["reneNrDiasinicio"].disabled=true;
	document.forms[0].elements["reneNrDiasfim"].disabled=true;
	document.forms[0].elements["reneNrSequencia"].disabled=true;
	document.forms[0].elements["reneDsCampocalculo"].disabled=true;
	document.forms[0].elements["reneVlRemunoperador"].disabled=true;
	document.forms[0].elements["reneInTpremunoperador"][0].disabled=true;
	document.forms[0].elements["reneInTpremunoperador"][1].disabled=true;
	document.forms[0].elements["reneVlRemunoperadora"].disabled=true;
	document.forms[0].elements["reneInTpremunoperadora"][0].disabled=true;
	document.forms[0].elements["reneInTpremunoperadora"][1].disabled=true;
	document.forms[0].elements["reneInTipoCobrancaMulta"][0].disabled=true;
	document.forms[0].elements["reneInTipoCobrancaMulta"][1].disabled=true;
	document.forms[0].elements["reneInTipoCobrancaJuros"][0].disabled=true;
	document.forms[0].elements["reneInTipoCobrancaJuros"][1].disabled=true;
	document.forms[0].elements["reneInImpressaoboleto"][0].disabled=true;
	document.forms[0].elements["reneInImpressaoboleto"][1].disabled=true;
	document.forms[0].elements["reneInIsencaojuros"][0].disabled=true;
	document.forms[0].elements["reneInIsencaojuros"][1].disabled=true;
	document.forms[0].elements["reneInIsencaomulta"][0].disabled=true;
	document.forms[0].elements["reneInIsencaomulta"][1].disabled=true;
	document.forms[0].elements["reneInTpremunoperador"][0].disabled=true;
	document.forms[0].elements["reneInTpremunoperador"][1].disabled=true;
	document.forms[0].elements["reneDsFormula"].disabled=true;

	regrasNegociacaoForm.idGrreCdGruporene.disabled = true;
}