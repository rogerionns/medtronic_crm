var countRegistro = new Number(0);
var temPermissaoAlterar = false;

function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();		
}

function pesquisar(){
	
	var texto= document.forms[0].elements["teadDsTelaadicionalDescricao"].value;
	var maiusculas=texto.toUpperCase();
	document.forms[0].elements["teadDsTelaadicionalDescricao"].value = maiusculas;
		
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();
	habilitarCampos();
	document.forms[0].elements["alterando"].value ="criar";		
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();
}

function excluirLista(idCampo){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';		
		document.forms[0].elements["idCampo"].value =idCampo;		
		document.forms[0].userAction.value = "excluirLista";		
		document.forms[0].submit();		
	}	
}

function excluir(idTela){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';		
		document.forms[0].elements["idTela"].value =idTela;		
		document.forms[0].userAction.value = "excluir";		
		document.forms[0].submit();
	}	
}

function bloqueiaTodosCamposEditar(){
	document.forms[0].elements["idTeadCdTelaadicional"].disabled="true";
	document.forms[0].elements["teadDsTelaadicional"].disabled="true";
	document.forms[0].elements["caadDsGrupo"].disabled="true";
	document.forms[0].elements["caadDsCampo"].disabled="true";
	document.forms[0].elements["caadNrPosicao"].disabled="true";
	document.forms[0].elements["caadNrTamanho"].disabled="true";
	document.forms[0].elements["checkado"].disabled="true";
}

function alterar(idTela,idCampoAdicional, inativo){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].elements["idTela"].value = idTela;
	document.forms[0].elements["idCampoAdicional"].value = idCampoAdicional;
	document.forms[0].elements["dataInativa"].value = inativo;

	document.forms[0].userAction.value = "alterar";
	document.forms[0].elements["alterando"].value= "altera";
	document.forms[0].submit();
}

function salvar(){
	if(document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado'){
		alert("� necess�rio estar incluindo ou editando um item para poder salv�-lo ");
		return;
	}
	if(document.forms[0].elements["teadDsTelaadicional"].value <= 0){
		alert("Por favor digite um nome para a tela");
		return;
	}else if(document.forms[0].elements["caadDsGrupo"].value <= 0){
		alert("Por favor digite um grupo");
		return;
	}else if(document.forms[0].elements["caadDsCampo"].value <= 0){
		alert("Por favor digite um campos");
		return;
	}else if(document.forms[0].elements["caadNrPosicao"].value <= 0){
		alert("Por favor digite o tamanho do registro");
		return;
	}else if(document.forms[0].elements["caadNrTamanho"].value <= 0){
		alert("Por favor digite o tamanho");
		return;
	}
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	//CHECANDO O INATIVO 
	if(document.forms[0].elements["checkado"].checked){
		document.forms[0].elements["check"].value = 'INATIVO';
	}else{
		document.forms[0].elements["check"].value = 'ATIVO';
	}
	if(podeGravarAssociacao()==false){
		return false;
	}
		
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;

}

function verificaCampos(tipoAba, habilitaCampos){	
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
		limparCampos();
	}
}

function limparCampos(){	
	document.forms[0].elements["idTeadCdTelaadicional"].value = "";
	document.forms[0].elements["teadDsTelaadicional"].value = "";
	document.forms[0].elements["caadDsGrupo"].value = "";
	document.forms[0].elements["caadDsCampo"].value = "";
	document.forms[0].elements["caadNrPosicao"].value = "";
	document.forms[0].elements["caadNrTamanho"].value = "";
	document.forms[0].elements["checkado"].checked = false;
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();	
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function fecharAguarde(){
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function adicionaLista(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "adicionaLista";
	document.forms[0].submit();	
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	setaListaBloqueia();
	setaIdiomaBloqueia();
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
	setaListaHabilita();
 	setaIdiomaHabilita();			
	break;
}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	//setaChavePrimaria(document.forms[0].elements["cbCdtbTelaadiconalTead"].value);
}
function habilitarCampos(){
	//cadastroTelaForm.idTeadCdTelaadicional.disabled = false;
	cadastroTelaForm.teadDsTelaadicional.disabled = false;
	cadastroTelaForm.caadDsGrupo.disabled = false;
	cadastroTelaForm.caadDsCampo.disabled = false;
	cadastroTelaForm.caadDsGrupo.disabled = false;
	cadastroTelaForm.caadNrPosicao.disabled = false;
	cadastroTelaForm.caadNrTamanho.disabled = false;
	
}
function desabilitaCamposCadastroTela(){
	cadastroTelaForm.idTeadCdTelaadicional.disabled = true;
	cadastroTelaForm.teadDsTelaadicional.disabled = true;
	cadastroTelaForm.caadDsGrupo.disabled = true;
	cadastroTelaForm.caadDsCampo.disabled = true;
	cadastroTelaForm.caadDsGrupo.disabled = true;
	cadastroTelaForm.caadNrPosicao.disabled = true;
	cadastroTelaForm.caadNrTamanho.disabled = true;
}	