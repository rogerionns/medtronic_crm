var countRegistro = new Number(0);
var temPermissaoIncluir = false;
var temPermissaoAlterar = false;
var temPermissaoExcluir = false;

function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();		
}

function pesquisar(){
	
	var texto= document.forms[0].elements["metaPesquisar"].value;
	var maiusculas=texto.toUpperCase();
	document.forms[0].elements["metaPesquisar"].value = maiusculas;
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();
	habilitarCampos();
	document.forms[0].elements["alterando"].value = "criar";
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();		
}


function atualizaCombos(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].elements["alterando"].value = "criar";
	document.forms[0].userAction.value = "atualizaCombos";
	document.forms[0].submit();	
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();	
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function excluir(idMeta){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
		document.forms[0].elements["idMeta"].value =idMeta;
		document.forms[0].userAction.value = "excluir";	
		document.forms[0].submit();		
	}	
}

function excluirLista(idListaResultado){
	if(confirm('Confirma exclus�o ?')){
		parent.parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
		parent.document.forms[0].elements["idListaResultado"].value = idListaResultado;
		parent.document.forms[0].userAction.value = "excluirLista";	
		parent.document.forms[0].submit();
	}	
}

function alterar(idMeta, idArea, idFuncionario, idCampanha, idCampanhaSub, metaContatos, metaRecuperado, metaNegociado, inativo){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	document.forms[0].elements["idMeta"].value = idMeta;
	document.forms[0].elements["idArea"].value = idArea;
	document.forms[0].elements["idFuncionario"].value = idFuncionario;
	document.forms[0].elements["idCampanha"].value = idCampanha;
	document.forms[0].elements["idCampanhaSub"].value = idCampanhaSub;
	document.forms[0].elements["metaContatos"].value = metaContatos;
	document.forms[0].elements["metaRecuperado"].value = metaRecuperado;
	document.forms[0].elements["metaNegociado"].value = metaNegociado;
	document.forms[0].elements["dataInativa"].value = inativo;

	document.forms[0].elements["alterando"].value= "altera";
	document.forms[0].userAction.value = "alterar";
	document.forms[0].submit();
}

function adicionaLista(){
	
	//ESCOLHER UM Resultado
	
	if (document.forms[0].elements["idCampCdCampanhaCombo"].selectedIndex <= 0){
		alert("Por favor selecione uma campanha");
		return false;
	}
	
	if (document.forms[0].elements["idPublCdPublicoCombo"].selectedIndex <= 0){
		alert("Por favor selecione uma sub-campanha");
		return false;
	}
	
	if (document.forms[0].elements["idComboResultado"].selectedIndex <= 0){
		alert("Por favor selecione um resultado");
		return false;
	}
	
	if (document.forms[0].elements["mereNrQtde"].value == ""){
		alert("Por favor digite uma quantidade");
		return false;
	}

	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	var idMeta = document.forms[0].elements["idMeta"].value ;
	var idListaResultado = document.forms[0].elements["idListaResultado"].value ;	
	var idCampCdCampanhaCombo = document.forms[0].elements["idCampCdCampanhaCombo"].value ;	
	var idPublCdPublicoCombo = document.forms[0].elements["idPublCdPublicoCombo"].value ;
	var idComboResultado = document.forms[0].elements["idComboResultado"].value ;
	var mereNrQtde = document.forms[0].elements["mereNrQtde"].value ;

	var selectIndexCampanha = document.forms[0].idCampCdCampanhaCombo.selectedIndex;
	descCampanha = document.forms[0].idCampCdCampanhaCombo.options[selectIndexCampanha].text;

	var selectIndexCampanhaSubCampanha = document.forms[0].idPublCdPublicoCombo.selectedIndex;
	campanhaSub = document.forms[0].idPublCdPublicoCombo.options[selectIndexCampanhaSubCampanha].text;
	
	var selectIndexResultado = document.forms[0].idComboResultado.selectedIndex;
	resultadoCombo = document.forms[0].idComboResultado.options[selectIndexResultado].text;
			
	window.cadastroMetasLista.location.href = "cadastroMetas.do?userAction=adicionaLista&idCampCdCampanhaCombo=" + idCampCdCampanhaCombo + "&idPublCdPublicoCombo=" + idPublCdPublicoCombo + "&idComboResultado=" + idComboResultado + "&mereNrQtde=" + mereNrQtde + "&descCampanha=" + descCampanha + "&campanhaSub=" + campanhaSub + "&resultadoCombo=" + resultadoCombo + "&idMeta=" + idMeta + "&idListaResultado=" + idListaResultado;
}

function limparCampos(){
	document.forms[0].elements["idMeta"].value = "";	
	document.forms[0].elements["idMetaCdMeta"].value = "";
	document.forms[0].elements["idAreaCdAreaCombo"].value = "";
	document.forms[0].elements["idFuncCdFuncionarioCombo"].value = "";
	document.forms[0].elements["idCampCdCampanhaCombo"].value = "";
	document.forms[0].elements["idPublCdPublicoCombo"].value = "";
	document.forms[0].elements["metaNrContatos"].value = "";
	document.forms[0].elements["metaVlRecuperado"].value = "";
	document.forms[0].elements["metaVlNegociado"].value = "";
	document.forms[0].elements["inativo"].value = "";
	document.forms[0].elements["idComboResultado"].value = "";
	document.forms[0].elements["mereNrQtde"].value = "";
}

function fecharAguarde(){
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function verificaCampos(tipoAba, habilitaCampos){	
	if((tipoAba == 'editar') && (habilitaCampos== 'desabilita')){
		limparCampos();
	}
}

function bloqueiaTodosCamposEditar(){
	document.forms[0].elements["idMetaCdMeta"].disabled="true";
	document.forms[0].elements["idAreaCdAreaCombo"].disabled="true";
	document.forms[0].elements["idFuncCdFuncionarioCombo"].disabled="true";
	document.forms[0].elements["idCampCdCampanhaCombo"].disabled="true";
	document.forms[0].elements["idPublCdPublicoCombo"].disabled="true";
	document.forms[0].elements["metaNrContatos"].disabled="true";
	document.forms[0].elements["metaVlRecuperado"].disabled="true";
	document.forms[0].elements["metaVlNegociado"].disabled="true";
	document.forms[0].elements["inativo"].disabled="true";
	document.forms[0].elements["idComboResultado"].disabled="true";
	document.forms[0].elements["mereNrQtde"].disabled="true";
	
	document.getElementById("cadastroMetasLista").src = "pages/cadastroMetasListaLimpa.jsp";
}

function carregaCadastroMetasLista(idMeta){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible'; 
	document.cadastroMetasLista.location =  "cadastroMetas.do?userAction=carregaCadastroMetasLista&idMeta=" + idMeta;
}

function salvar(){
	if (document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado'){
		alert("� necess�rio estar incluindo ou editando um item para poder salv�-lo ");
		return;
	}
	if(document.forms[0].elements["idAreaCdAreaCombo"].value <= 0){
		alert("Por favor selecione uma �rea");
		return;
	}else if(document.forms[0].elements["idFuncCdFuncionarioCombo"].value <= 0){
		alert("Por favor selecione um funcion�rio");
		return;
	}else if(document.forms[0].elements["idCampCdCampanhaCombo"].value <= 0){
		alert("Por favor selecione uma campanha");
		return;
	}else if(document.forms[0].elements["idPublCdPublicoCombo"].value <= 0){
		alert("Por favor selecione uma sub-campanha");
		return;
	}
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	//CHECANDO O INATIVO;
	if(document.forms[0].elements["inativo"].checked){
		document.forms[0].elements["check"].value = 'INATIVO';
	}else{
		document.forms[0].elements["check"].value = 'ATIVO';
	}
	if(podeGravarAssociacao()==false){
		return false;
	}
	
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	setaListaBloqueia();

	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
	setaListaHabilita();
 		
	break;

}
 //eval(stracao);
}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	//setaChavePrimaria(document.forms[0].elements["idMetaCdMeta"].value);
}

function habilitarCampos(){	
	//cadastroMetasForm.idMetaCdMeta.disabled = false;
	cadastroMetasForm.idAreaCdAreaCombo.disabled = false;
	cadastroMetasForm.idFuncCdFuncionarioCombo.disabled = false;
	cadastroMetasForm.idCampCdCampanhaCombo.disabled = false;
	cadastroMetasForm.idPublCdPublicoCombo.disabled = false;
	cadastroMetasForm.metaNrContatos.disabled = false;
	cadastroMetasForm.metaVlRecuperado.disabled = false;
	cadastroMetasForm.metaVlNegociado.disabled = false;
	cadastroMetasForm.metaNrContatos.disabled = false;
	cadastroMetasForm.idComboResultado.disabled = false;
	cadastroMetasForm.mereNrQtde.disabled = false;
	cadastroMetasForm.inativo.disabled = false;
}

function desabilitaCamposMeta(){
	cadastroMetasForm.idMetaCdMeta.disabled = true;
	cadastroMetasForm.idAreaCdAreaCombo.disabled = true;
	cadastroMetasForm.idFuncCdFuncionarioCombo.disabled = true;
	cadastroMetasForm.idCampCdCampanhaCombo.disabled = true;
	cadastroMetasForm.idPublCdPublicoCombo.disabled = true;
	cadastroMetasForm.metaNrContatos.disabled = true;
	cadastroMetasForm.metaVlRecuperado.disabled = true;
	cadastroMetasForm.metaVlNegociado.disabled = true;
	cadastroMetasForm.metaNrContatos.disabled = true;
	cadastroMetasForm.idComboResultado.disabled = true;
	cadastroMetasForm.mereNrQtde.disabled = true;
	cadastroMetasForm.inativo.disabled = true;
	setPermissaoImageDisable(false, document.cadastroMetasForm.imgAdicionaMeta);
}	