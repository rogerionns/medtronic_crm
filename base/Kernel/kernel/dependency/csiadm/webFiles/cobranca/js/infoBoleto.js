var countRegistro = new Number(0);
var temPermissaoIncluir = false;
var temPermissaoAlterar = false;
var temPermissaoExcluir = false;

function init(){
	parent.document.all.item('LayerAguarde').style.display = 'block';	
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();		
}

function pesquisar(){
	var texto= document.forms[0].elements["inboDsDescricao"].value;
	var maiusculas=texto.toUpperCase();
	document.forms[0].elements["inboDsDescricao"].value = maiusculas;
	
	parent.document.all.item('LayerAguarde').style.display = 'block';	
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function novo(){
	parent.document.all.item('LayerAguarde').style.display = 'block';
	limparCampos();
	habilitarCampos();
	document.forms[0].elements["alterando"].value= "criar";
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();
}

//function alterar(idInfoBoleto, dsCnpj, dsNome, inBanco, dsConta, dsAgencia, dsNossoNumero, dsLocalPagamento1, dsInstrucoes, dsPathImagen, dhInativo, dsDigitoagencia, dsDigitoconta, dsLocalpagamento2, dsCarteira, dsAceite, dsEspecie, dsQtdmoeda, dsMoeda, dsDescricoes, dsNumdocumento, dsCedente, dsDigitoCedente){
function alterar(idInfoBoleto, dsCnpj, dsNome, inBanco, dsConta, dsAgencia, dsNossoNumero, dsLocalPagamento1, dsInstrucoes, dsPathImagen, dhInativo, dsDigitoagencia, dsDigitoconta, dsLocalpagamento2, dsCarteira, dsAceite, dsEspecie, dsQtdmoeda, dsMoeda, dsDescricoes, dsNumdocumento, dsCedente, dsNumconvenio, dsCodoperacao, dsCodfornecidoagencia, inboDsMoeda, inboDsTamnossonumero, idTpboCdTipoboleto, index){
	
	parent.document.all.item('LayerAguarde').style.display = 'block';
	document.forms[0].elements["alterando"].value= "altera";
	document.forms[0].elements["idInfoBoleto"].value = idInfoBoleto;
	document.forms[0].elements["idInboCdInfoBoleto"].value = idInfoBoleto;
	document.forms[0].elements["dsCnpj"].value = dsCnpj;
	document.forms[0].elements["dsNome"].value = dsNome;
	document.forms[0].elements["inBanco"].value = inBanco;
	document.forms[0].elements["dsConta"].value = dsConta;
	document.forms[0].elements["dsAgencia"].value = dsAgencia;
	document.forms[0].elements["dsNossoNumero"].value = dsNossoNumero;
	document.forms[0].elements["dsLocalPagamento1"].value = dsLocalPagamento1;
	document.forms[0].elements["dsInstrucoes"].value = replaceAll(document.forms[0].inboDsInst[index].value, "QBRLNH", "\r\n");
	document.forms[0].elements["dsPathImagen"].value = dsPathImagen;
	document.forms[0].elements["dhInativo"].value = dhInativo;
	document.forms[0].elements["dsDigitoagencia"].value = dsDigitoagencia;
	document.forms[0].elements["dsDigitoconta"].value = dsDigitoconta;
	document.forms[0].elements["dsLocalpagamento2"].value = dsLocalpagamento2;
	document.forms[0].elements["dsCarteira"].value = dsCarteira;
	document.forms[0].elements["dsAceite"].value = dsAceite;
	document.forms[0].elements["dsEspecie"].value = dsEspecie;
	document.forms[0].elements["dsQtdmoeda"].value = dsQtdmoeda;
	document.forms[0].elements["dsMoeda"].value = dsMoeda;
	document.forms[0].elements["dsDescricoes"].value = document.forms[0].inboDsDesc[index].value;
	document.forms[0].elements["dsNumdocumento"].value = dsNumdocumento;
	document.forms[0].elements["dsCedente"].value = dsCedente;
	//document.forms[0].elements["dsDigitoCedente"].value = dsDigitoCedente;
	document.forms[0].elements["dsNumconvenio"].value = dsNumconvenio;
	document.forms[0].elements["dsCodoperacao"].value = dsCodoperacao;
	document.forms[0].elements["dsCodfornecidoagencia"].value = dsCodfornecidoagencia;
	
	document.forms[0].elements["ds_Moeda"].value = inboDsMoeda;
	document.forms[0].elements["ds_Tamnossonumero"].value = inboDsTamnossonumero;
	
	document.forms[0].elements["idTpboCdTipoboleto"].value = idTpboCdTipoboleto;
	
			
	document.forms[0].userAction.value = "alterar";
	document.forms[0].submit();
}

function bloqueiaTodosCamposEditar(){
	document.forms[0].inboDsCnpj.disabled="true";
	document.forms[0].inboDsNome.disabled="true";	
	document.forms[0].idPcoCdBancoBoleto.disabled="true";
	document.forms[0].inboDsAgencia.disabled="true";
	document.forms[0].inboDsConta.disabled="true";
	document.forms[0].inboDsNossoNumero.disabled="true";
	document.forms[0].inboDsLocalPagamento1.disabled="true";
	document.forms[0].inboDsInstrucoes.disabled="true";
	document.forms[0].inboDsPathImagen.disabled="true";
	document.forms[0].inativo.disabled="true";
	document.forms[0].inboNrDigitoconta.disabled="true";
	document.forms[0].inboNrDigitoagencia.disabled="true";
	document.forms[0].inboDsLocalPagamento2.disabled="true";
	document.forms[0].inboDsCarteira.disabled="true";
	document.forms[0].inboDsAceite.disabled="true";
	document.forms[0].inboDsEspecie.disabled="true";
	document.forms[0].inboNrQtdmoeda.disabled="true";
	document.forms[0].inboVlMoeda.disabled="true";
	document.forms[0].inboDsDescricoes.disabled="true";
	document.forms[0].inboNrNumdocumento.disabled="true";
	document.forms[0].inboDsCedente.disabled="true";
	document.forms[0].inboNrNumconvenio.disabled="true";
	document.forms[0].inboNrCodoperacao.disabled="true";
	document.forms[0].inboNrCodfornecidoagencia.disabled="true";
	
	document.forms[0].inboDsMoeda.disabled="true";
	document.forms[0].inboDsTamnossonumero.disabled="true";
	document.forms[0].idTpboCdTipoboleto.disabled="true";
	//document.forms[0].inboNrDigitocedente.disabled="true";
}

function excluir(idInfoBoleto){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.display = 'block';
		document.forms[0].elements["idInfoBoleto"].value = idInfoBoleto;
		document.forms[0].userAction.value = "excluir";
		document.forms[0].submit();
	}	
}

function salvar(){
	if (document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado'){
		alert("� necess�rio estar incluindo ou editando um item para poder salv�-lo!");
		return;
	}
	
	if(document.forms[0].elements["inboDsCnpj"].value == ""){
		alert("Por favor digite o n�mero do CNPJ!");
		return;
	}

	if(document.forms[0].elements["inboDsNome"].value == ""){
		alert("Por favor digite o nome!");
		return;
	}

	if(document.forms[0].elements["idPcoCdBancoBoleto"].value == ""){
		alert("Por favor selecione um banco!");
		return;
	} else if(document.forms[0].elements["idPcoCdBancoBoleto"].value == "0"){
		if(document.forms[0].elements["inboNrNumconvenio"].value == "") {
			alert("Por favor selecione um N�mero Conv�nio!");
			return;
		}
	} else if(document.forms[0].elements["idPcoCdBancoBoleto"].value == "4"){
		if(document.forms[0].elements["inboNrCodoperacao"].value == "") {
			alert("Por favor selecione um C�digo Opera��o!");
			return;
		}
		
		if(document.forms[0].elements["inboNrCodfornecidoagencia"].value == "") {
			alert("Por favor selecione um C�digo Fornecido Ag�ncia!");
			return;
		}
	} 
		
	if(document.forms[0].elements["inboDsTamnossonumero"].value == ""){
		alert("Por favor digite o tamanho do nosso n�mero!");
		return;
	}
	
	if(document.forms[0].elements["inboDsAgencia"].value == ""){
		alert("Por favor digite o n�mero da ag�ncia!");
		return;
	}
	
	if(document.forms[0].elements["inboDsConta"].value == ""){
		alert("Por favor digite o n�mero da conta!");
		return;
	}
	
	if(document.forms[0].elements["inboDsLocalPagamento1"].value == ""){
		alert("Por favor digite o local de pagamento!");
		return;
	}
	
	if(document.forms[0].elements["inboDsInstrucoes"].value == ""){
		alert("Por favor digite alguma instru��o!");
		return;
	}
		
	if(document.forms[0].elements["inativo"].checked){
		document.forms[0].elements["check"].value = 'INATIVO';
	}else{
		document.forms[0].elements["check"].value = 'ATIVO';
	}
	
	if(podeGravarAssociacao()==false){
		return false;
	}
	
	//Verifica se existe algum nosso numero marcado com principal
	var temRangePrincipalSelecionado = false;
	if(coutLstRange > 1) {						
		for(i = 0; i < document.forms[0].checkInPrincipalArray.length; i++) {
			if(document.forms[0].checkInPrincipalArray[i].checked) {
				temRangePrincipalSelecionado = true;
			}			
		}
	} else if(coutLstRange == 1) {
		if(document.forms[0].checkInPrincipalArray.checked) {
			temRangePrincipalSelecionado = true;
		}
	} 
	
	if(!temRangePrincipalSelecionado) {
		AtivarPasta('NOSSONUMERO');
		alert("Por favor informe o nosso n�mero que deve ser usado!");
		return false;
	}
	
	//Verifica se o campo sequencia do range do nosso numero foi preenchido
	if(coutLstRange > 1) {						
		for(i = 0; i < document.forms[0].checkInPrincipalArray.length; i++) {
			if(document.forms[0].bonnNrSequenciaArray[i].value == '') {
				AtivarPasta('NOSSONUMERO');
				alert("Por favor informe a sequ�ncia do range!");
				document.forms[0].bonnNrSequenciaArray[i].focus();
				return false;
			}			
		}
	} else if(coutLstRange == 1) {
		if(document.forms[0].bonnNrSequenciaArray.value == '') {
			AtivarPasta('NOSSONUMERO');
			alert("Por favor informe a sequ�ncia do range!");
			document.forms[0].bonnNrSequenciaArray.focus();
			return false;
		}
	} 
	
	parent.document.all.item('LayerAguarde').style.display = 'block';
	
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
}

function detalhe(){
	parent.document.all.item('LayerAguarde').style.display = 'block';
	document.forms[0].userAction.value = "detalhe";
	document.forms[0].submit();		
}

function verificaCampos(tipoAba, habilitaCampos){
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
		limparCampos();
	}	
}

function limparCampos(){
	document.forms[0].elements["inboDsCnpj"].value = "";
	document.forms[0].elements["inboDsNome"].value = "";
	document.forms[0].elements["idPcoCdBancoBoleto"].value = "";
	document.forms[0].elements["inboDsAgencia"].value = "";
	document.forms[0].elements["inboDsConta"].value = "";
	document.forms[0].elements["inboDsNossoNumero"].value = "";
	document.forms[0].elements["inboDsLocalPagamento1"].value = "";
	document.forms[0].elements["inboDsInstrucoes"].value = "";
	document.forms[0].elements["inboDsPathImagen"].value = "";
	//document.forms[0].elements["inboDhInativo"].value = "N" ;
	document.forms[0].inboNrDigitoconta.value = "";
	document.forms[0].inboNrDigitoagencia.value = "";
	document.forms[0].inboDsLocalPagamento2.value = "";
	document.forms[0].inboDsCarteira.value = "";
	document.forms[0].inboDsAceite.value = "";
	document.forms[0].inboDsEspecie.value = "";
	document.forms[0].inboNrQtdmoeda.value = "";
	document.forms[0].inboVlMoeda.value = "";
	document.forms[0].inboDsDescricoes.value = "";
	document.forms[0].inboNrNumdocumento.value = "";
	document.forms[0].inboDsCedente.value = "";
	document.forms[0].inboNrNumconvenio.value = "";
	document.forms[0].inboNrCodoperacao.value = "";
	document.forms[0].inboNrCodfornecidoagencia.value = "";
	document.forms[0].inboDsMoeda.value = "";
	document.forms[0].inboDsTamnossonumero.value = "";

	document.forms[0].idTpboCdTipoboleto.value = "";
	//document.forms[0].inboNrDigitocedente.value = "";
}
	
function cancelar(){
	parent.document.all.item('LayerAguarde').style.display = 'block';	
	limparCampos();
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();
}

function isMaxLength(obj, mlength) {
	//var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
	if (obj.getAttribute && obj.value.length>mlength){
		obj.value=obj.value.substring(0,mlength)
	}
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta){
switch (pasta){

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','InfoBoleto','','hide','dadosboleto','','hide','nossonumero','','hide','lstRange','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdInfoBoleto','principalPstQuadroLinkNormalMaior');	
	SetClassFolder('tdDadosBoleto','principalPstQuadroLinkNormal');
	SetClassFolder('tdNossoNumero','principalPstQuadroLinkNormal');
	setaListaBloqueia();
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','InfoBoleto','','show','dadosboleto','','show','nossonumero','','hide','lstRange','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdInfoBoleto','principalPstQuadroLinkSelecionadoMaior');
	SetClassFolder('tdDadosBoleto','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdNossoNumero','principalPstQuadroLinkNormal');
	setaListaHabilita();
	break;
	
case 'DADOSBOLETO':
	MM_showHideLayers('dadosboleto','','show','nossonumero','','hide','lstRange','','hide');
	SetClassFolder('tdDadosBoleto','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdNossoNumero','principalPstQuadroLinkNormal');
	break;
	
case 'NOSSONUMERO':
	MM_showHideLayers('dadosboleto','','hide','nossonumero','','show','lstRange','','show');
	SetClassFolder('tdDadosBoleto','principalPstQuadroLinkNormal');
	SetClassFolder('tdNossoNumero','principalPstQuadroLinkSelecionado');
	break;
} 
 parent.document.all.item('LayerAguarde').style.display = 'none';
}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}

function habilitarCampos(){
	//infoBoletoForm.idInboCdInfoBoleto.disabled = true;
	//infoBoletoForm.inboDsCnpj.disabled = false;
	infoBoletoForm.inboDsNome.disabled = false;
	infoBoletoForm.idPcoCdBancoBoleto.disabled = false;
	infoBoletoForm.inboDsAgencia.disabled = false;
	infoBoletoForm.inboDsConta.disabled = false;
	infoBoletoForm.inboDsNossoNumero.disabled = false;
	infoBoletoForm.inboDsLocalPagamento1.disabled = false;
	infoBoletoForm.inboDsInstrucoes.disabled = false;
	infoBoletoForm.inboDsPathImagen.disabled = false;
	infoBoletoForm.inboNrDigitoconta.disabled = false;
	infoBoletoForm.inboNrDigitoagencia.disabled = false;
	infoBoletoForm.inboDsLocalPagamento2.disabled = false;
	infoBoletoForm.inboDsCarteira.disabled = false;
	infoBoletoForm.inboDsAceite.disabled = false;
	infoBoletoForm.inboDsEspecie.disabled = false;
	infoBoletoForm.inboNrQtdmoeda.disabled = false;
	infoBoletoForm.inboVlMoeda.disabled = false;
	infoBoletoForm.inboDsDescricoes.disabled = false;
	infoBoletoForm.inboNrNumdocumento.disabled = false;
	infoBoletoForm.inboDsCedente.disabled = false;
	infoBoletoForm.inboNrNumconvenio.disabled = false;
	infoBoletoForm.inboNrCodoperacao.disabled = false;
	infoBoletoForm.inboNrCodfornecidoagencia.disabled = false;
	infoBoletoForm.inativo.disabled = false;
	infoBoletoForm.inboDsMoeda.disabled=false;
	infoBoletoForm.inboDsTamnossonumero.disabled=false;
	infoBoletoForm.idTpboCdTipoboleto.disabled=false;
	//infoBoletoForm.inboNrDigitocedente.disabled = false;	
}

function desabilitaCamposInfoBoletos(){
	//infoBoletoForm.idInboCdInfoBoleto.disabled = true;
	infoBoletoForm.inboDsCnpj.disabled = true;
	infoBoletoForm.inboDsNome.disabled = true;
	infoBoletoForm.idPcoCdBancoBoleto.disabled = true;
	infoBoletoForm.inboDsAgencia.disabled = true;
	infoBoletoForm.inboDsConta.disabled = true;
	infoBoletoForm.inboDsNossoNumero.disabled = true;
	infoBoletoForm.inboDsLocalPagamento1.disabled = true;
	infoBoletoForm.inboDsInstrucoes.disabled = true;
	infoBoletoForm.inboDsPathImagen.disabled = true;
	infoBoletoForm.inboNrDigitoconta.disabled = true;
	infoBoletoForm.inboNrDigitoagencia.disabled = true;
	infoBoletoForm.inboDsLocalPagamento2.disabled = true;
	infoBoletoForm.inboDsCarteira.disabled = true;
	infoBoletoForm.inboDsAceite.disabled = true;
	infoBoletoForm.inboDsEspecie.disabled = true;
	infoBoletoForm.inboNrQtdmoeda.disabled = true;
	infoBoletoForm.inboVlMoeda.disabled = true;
	infoBoletoForm.inboDsDescricoes.disabled = true;
	infoBoletoForm.inboNrNumdocumento.disabled = true;
	infoBoletoForm.inboDsCedente.disabled = true;
	infoBoletoForm.inboNrNumconvenio.disabled = true;
	infoBoletoForm.inboNrCodoperacao.disabled = true;
	infoBoletoForm.inboNrCodfornecidoagencia.disabled = true;
	infoBoletoForm.bonnNrRangeinicial.disabled = true;
	infoBoletoForm.bonnNrRangefinal.disabled = true;
	infoBoletoForm.inboDsMoeda.disabled=true;
	infoBoletoForm.inboDsTamnossonumero.disabled=true;
	infoBoletoForm.idTpboCdTipoboleto.disabled=true;
	//infoBoletoForm.bonnInPrincipal.disabled = true;
	//infoBoletoForm.inboNrDigitocedente.disabled = true;
	setPermissaoImageDisable(false, document.infoBoletoForm.imgVisualizarBoleto);
	setPermissaoImageDisable(false, document.infoBoletoForm.imgCarta);
	setPermissaoImageDisable(false, document.infoBoletoForm.imgAddRange);
}