	var countRegistro = new Number(0);
var temPermissaoIncluir = false;
var temPermissaoAlterar = false;
var temPermissaoExcluir = false;

function init(){
	parent.document.all.item('LayerAguarde').style.display = 'block';	
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();		
}

function pesquisar(){
	var texto= document.forms[0].elements["descricaoAcao"].value;
	var maiusculas=texto.toUpperCase();
	document.forms[0].elements["descricaoAcao"].value = maiusculas;

	parent.document.all.item('LayerAguarde').style.display = 'block';	
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();	
}

function alterar(idAcao, tipo, empresa, acaoDsAcao, acaoDsCusto, idPesquisa, idDocumento, anexo, idManifestacao, accoDhInativo, idCampCdCampanha, idPublCdPublico, accoTxMensagem, idImcoCdImportacaocobr, accoNrIniciovigencia, accoNrFimvigencia, accoNrDiaslimiteexpiracao, idDocuCdSms, idGrdoCdSms, idIpchCdImplementchat, idGrdoCdCarta, idTpacCdTpacaocob, boletagem){
	parent.document.all.item('LayerAguarde').style.display = 'block';
	document.forms[0].elements["idAcao"].value = idAcao;
	document.forms[0].elements["inTipo"].value = idTpacCdTpacaocob;
	document.forms[0].elements["inEmpresa"].value = empresa;
	document.forms[0].elements["descricao"].value = acaoDsAcao;
	document.forms[0].elements["custoDescricao"].value = acaoDsCusto;
	document.forms[0].elements["inPesquisarRealizar"].value = idPesquisa;
	document.forms[0].elements["anexar"].value = anexo;
	document.forms[0].elements["cartaEnviar"].value = idDocumento;
	document.forms[0].elements["idManifesto"].value = idManifestacao;
	document.forms[0].elements["accoDhInativo"].value = accoDhInativo; 
	document.forms[0].elements["idCampCdCampanha"].value = idCampCdCampanha;
	document.forms[0].elements["idPublCdPublico"].value = idPublCdPublico;
	//document.forms[0].elements["accoTxMensagem"].value = accoTxMensagem;
	document.forms[0].elements["idImcoCdImportacaocobr"].value = idImcoCdImportacaocobr;
	document.forms[0].elements["accoNrIniciovigencia"].value = accoNrIniciovigencia;
	document.forms[0].elements["accoNrFimvigencia"].value = accoNrFimvigencia;
	document.forms[0].elements["accoNrDiaslimiteexpiracao"].value = accoNrDiaslimiteexpiracao;	
	document.forms[0].elements["idDocuCdSms"].value = idDocuCdSms;
	document.forms[0].elements["idGrdoCdSms"].value = idGrdoCdSms;
	document.forms[0].elements["idIpchCdImplementchat"].value = idIpchCdImplementchat;
	document.forms[0].elements["idGrdoCdCarta"].value = idGrdoCdCarta;
	
	if(boletagem=='S'){
		document.forms[0].elements["accoInBoletagem"].checked = true;
	}else{
		document.forms[0].elements["accoInBoletagem"].checked = false;
	}
	
	document.forms[0].userAction.value = "alterar";
	document.forms[0].submit();
}

function bloqueiaTodosCamposEditar(){
	document.forms[0].elements["idAcaoCdAcao"].disabled="true";
	document.forms[0].elements["acaoInTipo"].disabled="true";	
	document.forms[0].elements["acaoDsAcao"].disabled="true";

}

function novo(){
	parent.document.all.item('LayerAguarde').style.display = 'block';
	limparCampos();	
	habilitarCampos();
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();
}

function excluir(idAcao){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.display = 'block';
		document.forms[0].elements["idAcao"].value = idAcao;
		document.forms[0].userAction.value = "excluir";
		document.forms[0].submit();
	}	
}

function salvar(){
	
	if(document.forms[0].elements["acaoDsAcao"].value <= 0){
		alert("O campo Descri��o � obrigat�rio!");
		return;
	}
	
	/*if(document.forms[0].elements["accoNrIniciovigencia"].value == ''){
		alert("O campo In�cio da Vig�ncia � obrigat�rio!");
		return;
	}
	
	if(document.forms[0].elements["accoNrFimvigencia"].value == ''){
		alert("O campo Fim da Vig�ncia � obrigat�rio!");
		return;
	}*/
	

	/*if(parseInt(document.forms[0].elements["accoNrIniciovigencia"].value) > parseInt(document.forms[0].elements["accoNrFimvigencia"].value)){
		alert("O in�cio da vig�ncia n�o pode ser maior que o final da vig�ncia");
		return false;
	}*/
		
	if(document.forms[0].elements["accoNrDiaslimiteexpiracao"].value == ''){
		document.forms[0].elements["accoNrDiaslimiteexpiracao"].value == "0";
		//alert("O campo Dias Limite de Expira��o � obrigat�rio!");
		//return;
	}
	
	//Envio de carta simples, com boleto, impressao de carta simples ou impressao de carta com etiqueta
	if(document.forms[0].elements["acaoInTipo"].value == '2' || document.forms[0].elements["acaoInTipo"].value == '3' || 
	   document.forms[0].elements["acaoInTipo"].value == '9' || document.forms[0].elements["acaoInTipo"].value == '10' ) {
		if(document.forms[0].elements["cartaEnviar"].value == '') {
			alert("O campo Carta a Enviar � obrigat�rio!");
			return false;
		}
	}
	
	//Envio por SMS
	if(document.forms[0].elements["acaoInTipo"].value == '4') {
		if(document.forms[0].elements["idIpch"].value == '') {
			alert("O campo Classe para implementa��o � obrigat�rio!");
			return false;
		}
		
		if(document.forms[0].elements["idDocuCdDocumento"].value == '') {
			alert("O campo Documento � obrigat�rio!");
			return false;
		}
	}
	
	//Exporta��o, Exporta��o de Boleto
	if(document.forms[0].elements["acaoInTipo"].value == '6' || document.forms[0].elements["acaoInTipo"].value == '7') {
		if(document.forms[0].elements["idImcoCdImportacaocobr"].value == '') {
			alert("O campo Exporta��o Cobran�a � obrigat�rio!");
			//document.forms[0].elements["idImcoCdImportacaocobr"].focus();
			return false;
		}
	}
	
	//CHECANDO O INATIVO
	if(document.forms[0].elements["inativo"].checked){
		document.forms[0].elements["check"].value = 'INATIVO';
	}else{
		document.forms[0].elements["check"].value = 'ATIVO';
	}
	
	if(document.forms[0].elements["acaoInTipo"].value == ""){
		alert("Por favor selecione um tipo de a��o!");
		return;
	}else if(document.forms[0].elements["acaoDsAcao"].value == ""){
		alert("Por favor digite uma descri��o!");
		return;
	}else if(document.forms[0].elements["acaoDsCusto"].value == ""){
		alert("Por favor digite valor para o custo!");
		return false;
	}

	if(podeGravarAssociacao()==false){
		return false;
	}
	
	document.forms[0].elements["idPublCdPublico"].value = ifrmCmbSubCampanha.document.forms[0].elements["idPublCdPublico"].value;
	
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	
	
	parent.document.all.item('LayerAguarde').style.display = 'block';
}

function detalhe(){
	parent.document.all.item('LayerAguarde').style.display = 'block';
	document.forms[0].userAction.value = "detalhe";
	document.forms[0].submit();		
}

function verificaCampos(tipoAba, habilitaCampos){	
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
		limparCampos();
	}
}

function limparCampos(){	
	document.forms[0].elements["idAcaoCdAcao"].value = "";
	document.forms[0].elements["acaoInTipo"].value = "";
	document.forms[0].elements["acaoDsAcao"].value = "";
	document.forms[0].elements["acaoDsCusto"].value = "";
	document.forms[0].elements["idCampCdCampanha"].value = "";
	document.forms[0].elements["idPublCdPublico"].value = "";
	document.forms[0].elements["cartaEnviar"].value = "";
	//document.forms[0].elements["accoTxMensagem"].value = "";
	document.forms[0].elements["idImcoCdImportacaocobr"].value = "";
	//document.forms[0].elements["idManifestacaoAutomatica"].value = "";
	//document.forms[0].elements["pesquisarRealizar"].value = "";
	document.forms[0].elements["empresa"].value = "";
	document.forms[0].elements["acaoInTipo"].disabled = true ;
	document.forms[0].elements["acaoDsAcao"].disabled = true ;
	document.forms[0].elements["acaoDsCusto"].disabled = true ;	
	document.forms[0].elements["accoNrIniciovigencia"].value = "";
	document.forms[0].elements["accoNrFimvigencia"].value = "";
	document.forms[0].elements["accoNrDiaslimiteexpiracao"].value = "";
	document.forms[0].elements["idDocuCdDocumento"].value = "";
	document.forms[0].elements["idDocuCdSms"].value = "";
	document.forms[0].elements["idGrdoCdGrupodocumento"].value = "";
	document.forms[0].elements["idGrdoCdSms"].value = "";
	document.forms[0].elements["idIpchCdImplementchat"].value = "";
	document.forms[0].elements["idIpch"].value = "";
	document.forms[0].elements["idGrdoCdCarta"].value = "";
	document.forms[0].elements["idGrdoCdGrupodocumentoCarta"].value = "";
	document.forms[0].elements["idDocuCdDocumentoCarta"].value = "";
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.display = 'block';	
	limparCampos();	
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide','campanha','','hide','correspondencia','','hide','sms','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	SetClassFolder('tdCampanha','principalPstQuadroLinkNormal');
	SetClassFolder('tdCorrespondencia','principalPstQuadroLinkNormal');
	SetClassFolder('tdSMS','principalPstQuadroLinkNormal');
	setaListaBloqueia();
	setaIdiomaBloqueia();
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show','campanha','','show','correspondencia','','hide','sms','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
	SetClassFolder('tdCampanha','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdCorrespondencia','principalPstQuadroLinkNormal');
	SetClassFolder('tdSMS','principalPstQuadroLinkNormal');
	setaListaHabilita();
 	setaIdiomaHabilita();
	break;
	
case 'CAMPANHA':
	MM_showHideLayers('campanha','','show','correspondencia','','hide','sms','','hide');
	SetClassFolder('tdCampanha','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdCorrespondencia','principalPstQuadroLinkNormal');
	SetClassFolder('tdSMS','principalPstQuadroLinkNormal');
	break;
	
case 'CORRESPONDENCIA':
	MM_showHideLayers('campanha','','hide','correspondencia','','show','sms','','hide');
	SetClassFolder('tdCampanha','principalPstQuadroLinkNormal');
	SetClassFolder('tdCorrespondencia','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdSMS','principalPstQuadroLinkNormal');
	break;
	
case 'SMS':
	MM_showHideLayers('campanha','','hide','correspondencia','','hide','sms','','show');
	SetClassFolder('tdCampanha','principalPstQuadroLinkNormal');
	SetClassFolder('tdCorrespondencia','principalPstQuadroLinkNormal');
	SetClassFolder('tdSMS','principalPstQuadroLinkSelecionado');
	break;	
		
} 
 parent.document.all.item('LayerAguarde').style.display = 'none';
}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(document.forms[0].elements["idAcaoCdAcao"].value);
	
	if(document.forms[0].idPublCdPublico.value != ''){
		carregaCmbSubCampanha();
	}
}

function habilitarCampos(){
	//acaoCobrancaForm.idAcaoCdAcao.disabled = false;
	acaoCobrancaForm.acaoDsAcao.disabled = false;
	acaoCobrancaForm.empresa.disabled = false;
	acaoCobrancaForm.acaoInTipo.disabled = false;
	acaoCobrancaForm.acaoDsCusto.disabled = false;
	//acaoCobrancaForm.pesquisarRealizar.disabled = false;
	acaoCobrancaForm.cartaEnviar.disabled = false;
	//acaoCobrancaForm.idManifestacaoAutomatica.disabled = false;
	//acaoCobrancaForm.arquivoAnexar.disabled = false;
	acaoCobrancaForm.idCampCdCampanha.disabled = false;
	acaoCobrancaForm.idPublCdPublico.disabled = false;
	//acaoCobrancaForm.accoTxMensagem.disabled = false;
	acaoCobrancaForm.idImcoCdImportacaocobr.disabled = false;
	acaoCobrancaForm.accoNrIniciovigencia.disabled = false;
	acaoCobrancaForm.accoNrFimvigencia.disabled = false;
	acaoCobrancaForm.accoNrDiaslimiteexpiracao.disabled = false;
	acaoCobrancaForm.inativo.disabled = false;
	acaoCobrancaForm.idDocuCdSms.disabled = false;
	acaoCobrancaForm.idGrdoCdSms.disabled = false;
	document.forms[0].elements["idDocuCdDocumento"].disabled = false;
	document.forms[0].elements["idGrdoCdGrupodocumento"].disabled = false;
	document.forms[0].elements["idIpch"].disabled = false;
	document.forms[0].elements["idGrdoCdCarta"].disabled = false;
	document.forms[0].elements["idGrdoCdGrupodocumentoCarta"].disabled = false;
	document.forms[0].elements["idDocuCdDocumentoCarta"].disabled = false;
}

function desabilitaCamposAcoesCobranca(){
	//acaoCobrancaForm.idAcaoCdAcao.disabled = true;
	acaoCobrancaForm.acaoDsAcao.disabled = true;
	acaoCobrancaForm.empresa.disabled = true;
	acaoCobrancaForm.acaoInTipo.disabled = true;
	acaoCobrancaForm.acaoDsCusto.disabled = true;
	//acaoCobrancaForm.pesquisarRealizar.disabled = true;
	acaoCobrancaForm.cartaEnviar.disabled = true;
	//acaoCobrancaForm.idManifestacaoAutomatica.disabled = true;
	//acaoCobrancaForm.arquivoAnexar.disabled = true;
	acaoCobrancaForm.idCampCdCampanha.disabled = true;
	acaoCobrancaForm.idPublCdPublico.disabled = true;
	//acaoCobrancaForm.accoTxMensagem.disabled = true;
	acaoCobrancaForm.idImcoCdImportacaocobr.disabled = true;
	acaoCobrancaForm.accoNrIniciovigencia.disabled = true;
	acaoCobrancaForm.accoNrFimvigencia.disabled = true;
	acaoCobrancaForm.accoNrDiaslimiteexpiracao.disabled = true;
	acaoCobrancaForm.inativo.disabled = true;
	acaoCobrancaForm.idDocuCdSms.disabled = true;
	acaoCobrancaForm.idGrdoCdSms.disabled = true;
	document.forms[0].elements["idDocuCdDocumento"].disabled = true;
	document.forms[0].elements["idGrdoCdGrupodocumento"].disabled = true;
	document.forms[0].elements["idIpch"].disabled = true;
	document.forms[0].elements["idGrdoCdCarta"].disabled = true;
	document.forms[0].elements["idGrdoCdGrupodocumentoCarta"].disabled = true;
	document.forms[0].elements["idDocuCdDocumentoCarta"].disabled = true;
}	

function verificaTipoAcao() {
}


function incluirCampoEspecial(){
	if(acaoCobrancaForm.campoEspecial.value != ''){
		//acaoCobrancaForm.accoTxMensagem.value = acaoCobrancaForm.accoTxMensagem.value + acaoCobrancaForm.campoEspecial.value;
	}
}

function carregaDocu(){
	var dados = {
		idGrdoCdGrupodocumento : $("#idGrdoCdGrupodocumento").val()
	};
	
	 $.post("/csiadm/acaoCobranca.do?userAction=carregaCmbDocu", dados, function(ret) {
		$("#idDocuCdDocumento").loadoptionaguarde();
		$("#idDocuCdDocumento").loadoptions(ret, "id_docu_cd_documento","docu_ds_documento", acaoCobrancaForm.idDocuCdSms.value);
		carregaIdDocu();
	});
}

function carregaIdDocu(){
	acaoCobrancaForm.idDocuCdSms.value = document.forms[0].elements["idDocuCdDocumento"].value;
}

function carregaDocuCarta(){
	var dados = {
		idGrdoCdGrupodocumento : $("#idGrdoCdGrupodocumentoCarta").val()
	};
	
	 $.post("/csiadm/acaoCobranca.do?userAction=carregaCmbDocu", dados, function(ret) {
		$("#idDocuCdDocumentoCarta").loadoptionaguarde();
		$("#idDocuCdDocumentoCarta").loadoptions(ret, "id_docu_cd_documento","docu_ds_documento", acaoCobrancaForm.cartaEnviar.value);
		carregaIdDocuCarta();
	});
}

function carregaIdDocuCarta(){
	acaoCobrancaForm.cartaEnviar.value = document.forms[0].elements["idDocuCdDocumentoCarta"].value;
}

function setImplement(){
	acaoCobrancaForm.idIpchCdImplementchat.value = document.forms[0].elements["idIpch"].value;
}

