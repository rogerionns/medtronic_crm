<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/cobranca/js/pt/TratarDados.js"></script>
<script language="JavaScript">
	function rollbackLayout(idLayout, idIdentificador) {
		if(confirm('Deseja efetuar o rollback para esta vers�o do layout?')) {
			parent.document.forms[0].id_laou_cd_sequencial.value = idLayout;
			parent.document.forms[0].hlco_nr_identificador.value = idIdentificador;
			parent.document.forms[0].lico_nr_sequencia_rollback.value = '';
			document.forms[0].target = this.name = 'rollback';
			parent.document.forms[0].action = 'RollbackLayout.do';
			parent.document.forms[0].submit();
		}
	}

	function visualizarDetalhesCamposLayout(idLayout, idIdentificador) {
		showModalDialog('VisualizarHistoricoCamposLayout.do?id_laou_cd_sequencial=' + idLayout + '&hlco_nr_identificador=' + idIdentificador, window, 'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:400px,dialogTop:100px,dialogLeft:200px');
	}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="CarregarCamposLayout.do" styleId="layoutImportacaoForm"> 	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td class="principalLstCab" width="5%">&nbsp;</td>
        	<td class="principalLstCab" width="5%">&nbsp;</td>
			<td class="principalLstCab" width="20%">&nbsp;<bean:message key="prompt.data" /></td>
        	<td class="principalLstCab" width="50%">&nbsp;<bean:message key="prompt.funcionario" /></td>
        	<td class="principalLstCab" width="20%">&nbsp;<bean:message key="prompt.Operacao" /></td>
		</tr>
	</table> 
	<div id="lstRegistroHistorico" style="overflow: hidden; height: 470x; width: 100%; z-index: 4; position: absolute">
		<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
			<logic:present name="historicoVector">
				<logic:iterate name="historicoVector" id="historicoVector" indexId="indice">
					<tr> 
						<td class="principalLstParMao" width="5%">&nbsp;<img src="webFiles/images/icones/historico.gif" name="imgRollback" id="imgRollback" width="22" height="22" class="geralCursoHand" alt="<bean:message key="prompt.Rollback"/>"  onclick="rollbackLayout('<bean:write name="historicoVector" property="field(id_laou_cd_sequencial)" />', '<bean:write name="historicoVector" property="field(hlco_nr_identificador)" />');" /></td>
			        	<td class="principalLstParMao" width="5%">&nbsp;<img src="webFiles/images/botoes/lupa.gif" name="imgVisualizar" id="imgVisualizar" class="geralCursoHand" alt="<bean:message key="prompt.VisualizarCamposDoLayout"/>"  onclick="visualizarDetalhesCamposLayout('<bean:write name="historicoVector" property="field(id_laou_cd_sequencial)" />', '<bean:write name="historicoVector" property="field(hlco_nr_identificador)" />');" /></td>
						<td class="principalLstParMao" width="20%">&nbsp;<bean:write name="historicoVector" property="field(hlco_dh_historicolayout)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html" /> </td>
			        	<td class="principalLstParMao" width="50%">&nbsp;&nbsp;&nbsp;<bean:write name="historicoVector" property="field(func_nm_funcionario)" /></td>
			        	<td class="principalLstParMao" width="20%">&nbsp;
			        		<logic:equal name="historicoVector" property="field(hlco_in_tipooperacao)" value="A">
			        			<bean:message key="prompt.ALTERACAO" />
			        		</logic:equal>
			        		<logic:equal name="historicoVector" property="field(hlco_in_tipooperacao)" value="I">
			        			<bean:message key="prompt.INCLUSAO" />
			        		</logic:equal>
			        		<logic:equal name="historicoVector" property="field(hlco_in_tipooperacao)" value="R">
			        			<bean:message key="prompt.ROLLBACK" />
			        		</logic:equal>
			        		<logic:equal name="historicoVector" property="field(hlco_in_tipooperacao)" value="AI">
			        			<bean:message key="prompt.ALTERACAO/INCLUSAO" />
			        		</logic:equal>
			        	</td>
					</tr>
				</logic:iterate>
			</logic:present>  
		</table>
	</div>
</html:form>
</body>

<script>
	var temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LAYOUT_IMP_COBRANCA_ALTERACAO_CHAVE%>');
	
	//Verifica permiss�o de alterar
	if (!temPermissaoAlterar){
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LAYOUT_IMP_COBRANCA_ALTERACAO_CHAVE%>', document.layoutImportacaoForm.imgVisualizar);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LAYOUT_IMP_COBRANCA_ALTERACAO_CHAVE %>', document.layoutImportacaoForm.imgRollback);	
	}
</script>
</html>