<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.detalhes"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language="javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script language="javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script type="text/javascript">
var qtdadeRegistrosDesconto = new Number(0);

function isDigitoSemicolon(event) {
	var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;
	return (tk==191 || isDigito(event));
}

function isValPerc(event) {
	if(event.srcElement.value.indexOf(";") == -1) {
		var tds = event.srcElement.parentNode.parentNode.childNodes;
		for(var i=0; i<tds.length; i++) {
			if(tds[i].className.indexOf("valqueb") > -1) {
				tds[i].style.display = "none";
			} else if(tds[i].className.indexOf("valperc") > -1) {
				tds[i].style.display = "";

				if(tds[i].getElementsByTagName("input")[0].name=="pade_vl_inicial") {
					tds[i].getElementsByTagName("input")[0].focus();
					tds[i].getElementsByTagName("input")[0].value = event.srcElement.value;
					event.srcElement.value = "";
				}
				
			}
		}
	}
}

function isValQueb(event) {
	var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;
	if(tk==191) {
		var tds = event.srcElement.parentNode.parentNode.childNodes;
		for(var i=0; i<tds.length; i++) {
			if(tds[i].className.indexOf("valperc") > -1) {
				tds[i].style.display = "none";
			} else if(tds[i].className.indexOf("valqueb") > -1) {
				tds[i].style.display = "";
				tds[i].getElementsByTagName("input")[0].focus();
				tds[i].getElementsByTagName("input")[0].value = event.srcElement.value+";";
			}
		}
		return false;
	} 
	
	return isDigitoVirgula(event);
}
function inicio() {
	showError('<%=request.getAttribute("msgerro")%>');
	
	carregaLstDesconto();
	
	document.all.item('aguarde').style.visibility = 'hidden';
}

function setaCamposDesconto(ajax) {
	ajax.addField("descontoViewState", document.forms[0].descontoViewState.value);
	ajax.addField("id_grnp_cd_gruporeneparcela", document.forms[0].id_grnp_cd_gruporeneparcela.value);
	ajax.addField("tipoVState", "descontoViewState");
	
	for(var i = 0; i < qtdadeRegistrosDesconto; i++) {
		ajax.addField("id_pade_cd_sequencial", document.forms[0].id_pade_cd_sequencial[i].value);
		ajax.addField("id_tpco_cd_tpdesconto", document.forms[0].id_tpco_cd_tpdesconto[i].value);
		ajax.addField("pade_ds_descricao", document.forms[0].pade_ds_descricao[i].value);
		ajax.addField("pade_ds_formula", document.forms[0].pade_ds_formula[i].value);

		if(document.forms[0].pade_ds_valqueb[i].value=="") {
			ajax.addField("pade_vl_inicial", document.forms[0].pade_vl_inicial[i].value.replace(".","").replace(".","").replace(".",""));
			ajax.addField("pade_vl_final", document.forms[0].pade_vl_final[i].value.replace(".","").replace(".","").replace(".",""));
			ajax.addField("pade_ds_valqueb", "");
		} else {
			ajax.addField("pade_vl_inicial", "");
			ajax.addField("pade_vl_final", "");
			ajax.addField("pade_ds_valqueb", document.forms[0].pade_ds_valqueb[i].value.replace(".","").replace(".","").replace(",","."));
		}
		
		ajax.addField("pade_in_parcela", document.forms[0].pade_in_parcela[i].checked ? "S" : "N");
		ajax.addField("pade_in_especial", document.forms[0].pade_in_especial[i].checked ? "S" : "N");
	}
}

function adicionarLinhaDesconto() {
	var ajax = new ConsultaBanco("", "AdicionarLinha.do");	
	
	ajax.addField("adicionaLinha", "true");
	
	setaCamposDesconto(ajax);
	
	document.all.item('aguarde').style.visibility = 'visible';
	
	ajax.executarConsulta(retornoPopulaLinhasDesconto, true, true);
}

function excluirLinhaDesconto(indice) {
	if(confirm('Deseja remover este item?')) {
		var ajax = new ConsultaBanco("", "ExcluirLinha.do");	
		
		setaCamposDesconto(ajax);
		ajax.addField("indice", indice);

		document.all.item('aguarde').style.visibility = 'visible';
		
		ajax.executarConsulta(retornoPopulaLinhasDesconto, true, true);
	}
}

function carregaLstDesconto() {
	var ajax = new ConsultaBanco("", "CarregaLstDesconto.do");	
		
	ajax.addField("id_grnp_cd_gruporeneparcela", document.forms[0].id_grnp_cd_gruporeneparcela.value);
			
	ajax.executarConsulta(retornoPopulaLinhasDesconto, true, true);
}

function retornoPopulaLinhasDesconto(ajax) {
	removeAllNonPrototipeRows("rowDesconto", "tableDesconto");

	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}
	
	document.forms[0].descontoViewState.value = ajax.getViewState();
	
	rs = ajax.getRecordset();

	qtdadeRegistrosDesconto = new Number(0);
	
	if(rs == null || rs.getSize() == 0) {
		document.all.item('aguarde').style.visibility = 'hidden';		
		return;
	}
	
	while(rs.next()) {
		cloneNode("rowDesconto", { idSuffix:"" + (rs.getCurr()) });

		$("rowDesconto" + (rs.getCurr())).indice = rs.getCurr();
		
		document.forms[0].id_pade_cd_sequencial[rs.getCurr()].value = rs.get('id_pade_cd_sequencial') != null ? rs.get('id_pade_cd_sequencial') : '';
		document.forms[0].id_tpco_cd_tpdesconto[rs.getCurr()].value = rs.get('id_tpco_cd_tpdesconto') != null ? rs.get('id_tpco_cd_tpdesconto') : '';
		//pade_nr_sequencia
		document.forms[0].pade_ds_descricao[rs.getCurr()].value = rs.get('pade_ds_descricao') != null ? rs.get('pade_ds_descricao') : '';
		document.forms[0].pade_ds_formula[rs.getCurr()].value = rs.get('pade_ds_formula') != null ? rs.get('pade_ds_formula') : '';
		document.forms[0].pade_in_parcela[rs.getCurr()].checked = rs.get('pade_in_parcela') == 'S';

		if(rs.get('pade_ds_valqueb') != null && rs.get('pade_ds_valqueb') != "") {
			document.forms[0].pade_ds_valqueb[rs.getCurr()].parentNode.style.display="";
			document.forms[0].pade_vl_inicial[rs.getCurr()].parentNode.style.display="none";
			document.forms[0].pade_vl_final[rs.getCurr()].parentNode.style.display="none";

			document.forms[0].pade_ds_valqueb[rs.getCurr()].value = rs.get('pade_ds_valqueb') != null ? rs.get('pade_ds_valqueb') : '';
		} else {
			document.forms[0].pade_ds_valqueb[rs.getCurr()].parentNode.style.display="none";
			document.forms[0].pade_vl_inicial[rs.getCurr()].parentNode.style.display="";
			document.forms[0].pade_vl_final[rs.getCurr()].parentNode.style.display="";

			document.forms[0].pade_vl_inicial[rs.getCurr()].value = rs.get('pade_vl_inicial') != null ? rs.get('pade_vl_inicial').replace(".", ",") : '';
			document.forms[0].pade_vl_final[rs.getCurr()].value = rs.get('pade_vl_final') != null ? rs.get('pade_vl_final').replace(".", ",") : '';
		}			
		
		
		document.forms[0].pade_in_especial[rs.getCurr()].checked = rs.get('pade_in_especial') == 'S';
		
		habilitaDesabilitaCampos(rs.getCurr());
		
		qtdadeRegistrosDesconto++;

		$("rowDesconto" + (rs.getCurr())).style.display = "block";
	}

	document.all.item('aguarde').style.visibility = 'hidden';
}

function gravar() {
	for(var i = 0; i < qtdadeRegistrosDesconto; i++) {
		if(document.forms[0].pade_ds_descricao[i].value == '') {
			alert('O campo legenda � obrigat�rio!');
			document.forms[0].pade_ds_descricao[i].focus();
			return false;
		}
		if(document.forms[0].pade_ds_formula[i].value == '') {
			alert('O campo f�rmula � obrigat�rio!');
			document.forms[0].pade_ds_formula[i].focus();
			return false;
		}

		if(document.forms[0].pade_ds_valqueb[i].value == '') {
			if(document.forms[0].pade_vl_inicial[i].value == '') {
				alert('O campo valor inicial � obrigat�rio!');
				document.forms[0].pade_ds_formula[i].focus();
				return false;
			}
			if(document.forms[0].pade_vl_final[i].value == '') {
				alert('O campo valor final � obrigat�rio!');
				document.forms[0].pade_ds_formula[i].focus();
				return false;
			}
		}
		
	}	
	
	var ajax = new ConsultaBanco("", "GravarPopupDesconto.do");
	
	setaCamposDesconto(ajax);
	
	document.all.item('aguarde').style.visibility = 'visible';
	
	ajax.executarConsulta(retornoGravarDesconto, true, true);
}

function retornoGravarDesconto(ajax) {
	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}

	rs = ajax.getRecordset();

	if(rs == null || rs.getSize() == 0) {
		document.all.item('aguarde').style.visibility = 'hidden';	
		return;
	}

	while(rs.next()) {
		if(rs.get('sucesso') != null && rs.get('sucesso') == 'true') {
			carregaLstDesconto();		
		}
	}
}


function habilitaDesabilitaCampos(indice) {	
	if(document.forms[0].id_tpco_cd_tpdesconto[indice].value != '') {
		var i = document.forms[0].id_tpco_cd_tpdesconto[indice].selectedIndex;
		document.forms[0].pade_ds_formula[indice].readOnly = false;
		document.forms[0].pade_in_parcela[indice].disabled = false;


		document.forms[0].pade_ds_formula[indice].value = document.forms[0].id_tpco_cd_tpdesconto[indice].options[i].formula;
		document.forms[0].pade_in_parcela[indice].checked = (document.forms[0].id_tpco_cd_tpdesconto[indice].options[i].inparc=="S");
		
		document.forms[0].pade_ds_formula[indice].readOnly = true;
		document.forms[0].pade_in_parcela[indice].disabled = true;
		
		
		
	} else {
		document.forms[0].pade_ds_formula[indice].readOnly = false;
		document.forms[0].pade_in_parcela[indice].disabled = false;

		document.forms[0].pade_ds_formula[indice].value = "";
	}	
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio()">
<html:form action="/AbrirPopupDesconto" styleId="cadastroGrupoRegrasNegociacaoForm">
<html:hidden property="descontoViewState"/>
<html:hidden property="id_grnp_cd_gruporeneparcela"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.detalhes"/></td>
            <td class="principalQuadroPstVazia" >&nbsp; </td>
            <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="100%"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="100%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="100%" valign="top">
                  	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                    <div id="lstDesconto" style="overflow: auto; height: 183px; width: 100%; z-index: 21; ">
						<table border="0" width="98%" cellspacing="0" cellpadding="0" id="tableDesconto">
							<tr> 
								<td class="principalLstCab" style="width: 20px;" align="center">&nbsp;</td>
					        	<td class="principalLstCab">Tipo de Desconto</td>
					        	<td class="principalLstCab">Legenda</td>
					        	<td class="principalLstCab" style="width: 110px;"><bean:message key="prompt.formula" /></td>
					        	<td class="principalLstCab" title="Aplicar desconto na parcela?">Parc</td>
					        	<td class="principalLstCab valperc" style="width: 110px;"><bean:message key="prompt.valor" /> Inicial</td>
					        	<td class="principalLstCab valperc" style="width: 110px;"><bean:message key="prompt.valor" /> Final</td>
					        	<td class="principalLstCab" style="width: 20px;" title="Desconto Especial">Esp</td>
					        	<td class="principalLstCab" style="width: 3px;">&nbsp;</td>
							</tr>
	                    	<tr id="rowDesconto" style="display: none" >
								<td class="principalLstPar" align="center">
									<input type="hidden" name="id_pade_cd_sequencial" />
									<img src="webFiles/images/botoes/lixeira.gif" id="btExcluirDesconto" class="geralCursoHand" alt="<bean:message key="prompt.excluir"/>" onclick="excluirLinhaDesconto(this.parentNode.parentNode.indice);" />
								</td>
					        	<td class="principalLstPar">
				        			<select name="id_tpco_cd_tpdesconto" class="principalObjForm" onchange="habilitaDesabilitaCampos(this.parentNode.parentNode.indice)">
					        			<option value=""><bean:message key="prompt.selecione_uma_opcao" /></option>
					        			
					        			<logic:present name="tpcoVector">
					        				<logic:iterate name="tpcoVector" id="tpco">
					        					<option value="<bean:write name='tpco' property='field(id_tpco_cd_tpdesconto)' />" formula="<bean:write name="tpco" property="field(tpco_ds_formula)" />" inparc="<bean:write name="tpco" property="field(tpco_in_descparcela)" />"><bean:write name="tpco" property="field(tpco_ds_tpdesconto)" /></option>
					        				</logic:iterate>
	                              		</logic:present>
					        		</select>
					        	</td>
					        	<td class="principalLstPar">
					        		<input type="text" name="pade_ds_descricao" class="principalObjForm" maxlength="100" />
					        	</td>
					        	<td class="principalLstPar">
					        		<input type="text" name="pade_ds_formula" style="width: 105px;" class="principalObjForm" maxlength="4000" />
					        	</td>
					        	<td class="principalLstPar">
					        		<input type="checkbox" name="pade_in_parcela" value="S" />
					        	</td>
					        	<td class="principalLstPar valperc">
					        		<input type="text" name="pade_vl_inicial" class="principalObjForm" maxlength="10" onkeydown="return isValQueb(event);" onblur="return numberValidate(this, 2, '.', ',', event);"/>
								</td>
					        	<td class="principalLstPar valperc">
					        		<input type="text" name="pade_vl_final" class="principalObjForm" maxlength="10" onkeydown="return isValQueb(event);" onblur="return numberValidate(this, 2, '.', ',', event);"/>
					        	</td>
					        	<td class="principalLstPar valqueb" style="width: 220px; display: none;" colspan="2" >
					        		<input type="text" name="pade_ds_valqueb" class="principalObjForm" maxlength="100" onkeydown="return isDigitoSemicolon(event);" onkeyup="isValPerc(event);" />
					        	</td>
					        	<td class="principalLstPar">
					        		<input type="checkbox" name="pade_in_especial" value="S" />
					        	</td>
					        	<td class="principalLstPar">&nbsp;</td>
							</tr>
	                  	 </table>
                  	 </div>
                  	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
		            	<tr> 
		                	<td class="espacoPqn">&nbsp;</td>
		              	</tr>
		            </table>
                    <table  width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    	<tr>
                    		<td>&nbsp;</td>
                    		<td width="25" align="center">
                    			<img src="webFiles/images/botoes/mais.png" alt="Adicionar" name="imgAdicionarDesconto" id="imgAdicionarDesconto" class="geralCursoHand" onclick="adicionarLinhaDesconto()"/>&nbsp;
                    		</td>
                    		<td width="25" align="center">
		                      	<img src="webFiles/images/botoes/gravar.gif" alt="Gravar" name="imgGravar" id="imgGravar" class="geralCursoHand" onclick="gravar()"/>
		                    </td>
                    	</tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
  <div id="aguarde" style="position:absolute; left:210px; top:60px; width:199px; height:148px; z-index:10; visibility: visible"> 
	  <div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
  </div>
</html:form>
</body>
</html>