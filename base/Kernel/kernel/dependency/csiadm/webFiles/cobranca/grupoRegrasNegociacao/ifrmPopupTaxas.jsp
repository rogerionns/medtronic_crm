<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.detalhes"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language="javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script language="javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script type="text/javascript">
var qtdadeRegistrosTaxa = new Number(0);

function inicio() {
	showError('<%=request.getAttribute("msgerro")%>');
	
	carregaLstTaxa();
	
	document.all.item('aguarde').style.visibility = 'hidden';
}

function setaCamposTaxa(ajax) {
	ajax.addField("taxasViewState", document.forms[0].taxasViewState.value);
	ajax.addField("id_grnp_cd_gruporeneparcela", document.forms[0].id_grnp_cd_gruporeneparcela.value);
	ajax.addField("tipoVState", "taxasViewState");
	
	for(var i = 0; i < qtdadeRegistrosTaxa; i++) {
		ajax.addField("id_patx_cd_sequencialarray", document.forms[0].id_patx_cd_sequencialarray[i].value);
		ajax.addField("id_tppx_cd_tptaxasarray", document.forms[0].id_tppx_cd_tptaxasarray[i].value);
		ajax.addField("patx_nr_sequenciaarray", document.forms[0].patx_nr_sequenciaarray[i].value);
		ajax.addField("patx_vl_valorarray", document.forms[0].patx_vl_valorarray[i].value.replace(".","").replace(".","").replace(".",""));
		ajax.addField("patx_ds_formulaarray", document.forms[0].patx_ds_formulaarray[i].value);
		ajax.addField("patx_ds_observacao", document.forms[0].patx_ds_observacao[i].value);
	}
}

function adicionarLinhaTaxa() {
	var ajax = new ConsultaBanco("", "AdicionarLinha.do");	
	
	ajax.addField("adicionaLinha", "true");
	
	setaCamposTaxa(ajax);
	
	document.all.item('aguarde').style.visibility = 'visible';
	
	ajax.executarConsulta(retornoPopulaLinhasTaxa, true, true);
}

function excluirLinhaTaxa(indice) {
	if(confirm('Deseja remover este item?')) {
		var ajax = new ConsultaBanco("", "ExcluirLinha.do");	
		
		setaCamposTaxa(ajax);
		ajax.addField("indice", indice);

		document.all.item('aguarde').style.visibility = 'visible';
		
		ajax.executarConsulta(retornoPopulaLinhasTaxa, true, true);
	}
}

function carregaLstTaxa() {
	var ajax = new ConsultaBanco("", "CarregaLstTaxa.do");	
		
	ajax.addField("id_grnp_cd_gruporeneparcela", document.forms[0].id_grnp_cd_gruporeneparcela.value);
			
	ajax.executarConsulta(retornoPopulaLinhasTaxa, true, true);
}

function retornoPopulaLinhasTaxa(ajax) {
	removeAllNonPrototipeRows("rowTaxa", "tableTaxa");

	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}
	
	document.forms[0].taxasViewState.value = ajax.getViewState();
	
	rs = ajax.getRecordset();

	qtdadeRegistrosTaxa = new Number(0);
	
	if(rs == null || rs.getSize() == 0) {
		document.all.item('aguarde').style.visibility = 'hidden';		
		return;
	}
	
	while(rs.next()) {
		cloneNode("rowTaxa", { idSuffix:"" + (rs.getCurr()) });

		$("rowTaxa" + (rs.getCurr())).indice = rs.getCurr();
		
		document.forms[0].id_tppx_cd_tptaxasarray[rs.getCurr()].value = rs.get('id_tppx_cd_tptaxas') != null ? rs.get('id_tppx_cd_tptaxas') : '';
		document.forms[0].patx_nr_sequenciaarray[rs.getCurr()].value = rs.get('patx_nr_sequencia') != null ? rs.get('patx_nr_sequencia') : '';
		document.forms[0].patx_vl_valorarray[rs.getCurr()].value = rs.get('patx_vl_valor') != null ? rs.get('patx_vl_valor') : '';
		document.forms[0].patx_ds_formulaarray[rs.getCurr()].value = rs.get('patx_ds_formula') != null ? rs.get('patx_ds_formula') : '';
		document.forms[0].patx_ds_observacao[rs.getCurr()].value = rs.get('patx_ds_observacao') != null ? rs.get('patx_ds_observacao') : '';
		
		habilitaDesabilitaCampos(rs.getCurr());
		
		qtdadeRegistrosTaxa++;

		$("rowTaxa" + (rs.getCurr())).style.display = "block";
	}

	document.all.item('aguarde').style.visibility = 'hidden';
}

function gravar() {
	for(var i = 0; i < qtdadeRegistrosTaxa; i++) {
		if(document.forms[0].patx_nr_sequenciaarray[i].value == '') {
			alert('O campo Sequ�ncia � obrigat�rio!');
			document.forms[0].patx_nr_sequenciaarray[i].focus();
			return false;
		}

		if(document.forms[0].patx_ds_observacao[i].value == '') {
			if(!confirm('Deseja mesmo gravar sem definir uma legenda para a taxa ' + document.forms[0].patx_nr_sequenciaarray[i].value + '?')) {
				document.forms[0].patx_ds_observacao[i].focus();
				return false;
			}
		}
		
		if($('btFormula'+i).disabled && document.forms[0].id_tppx_cd_tptaxasarray[i].value == '') {
			alert('O campo Tipo Taxa � obrigat�rio!');
			document.forms[0].id_tppx_cd_tptaxasarray[i].focus();
			return false;
		} else if(!$('btFormula'+i).disabled && document.forms[0].patx_ds_formulaarray[i].value == '') {
			alert('O campo F�rmula � obrigat�rio!');
			document.forms[0].patx_ds_formulaarray[i].focus();
			return false;
		} 
	}	
	
	var ajax = new ConsultaBanco("", "GravarPopupTaxas.do");
	
	setaCamposTaxa(ajax);
	
	document.all.item('aguarde').style.visibility = 'visible';
	
	ajax.executarConsulta(retornoGravarTaxa, true, true);
}

function retornoGravarTaxa(ajax) {
	if(ajax.getMessage() != '') {
		alert(ajax.getMessage());
	}

	rs = ajax.getRecordset();

	if(rs == null || rs.getSize() == 0) {
		document.all.item('aguarde').style.visibility = 'hidden';	
		return;
	}

	while(rs.next()) {
		if(rs.get('sucesso') != null && rs.get('sucesso') == 'true') {
			carregaLstTaxa();		
		}
	}
}

function abrirPopupFormula(indice) {
	showModalDialog('AbrirPopupFormula.do?indice=' + indice + '&nomeCampo=patx_ds_formulaarray', window, 'help:no;scroll:yes;Status:NO;dialogWidth:620px;dialogHeight:210px,dialogTop:0px,dialogLeft:200px');
}

function habilitaDesabilitaCampos(indice) {	
	if(document.forms[0].id_tppx_cd_tptaxasarray[indice].value != '') {
		document.forms[0].patx_ds_formulaarray[indice].disabled = true;
		document.forms[0].patx_ds_formulaarray[indice].value = '';
		
		$('btFormula'+indice).className = 'geralImgDisable';
		$('btFormula'+indice).disabled = true;
	} else {
		document.forms[0].patx_ds_formulaarray[indice].disabled = false;
		
		$('btFormula'+indice).className = 'geralCursoHand';
		$('btFormula'+indice).disabled = false;
	}	
}

function verificaSequencia(indice) {
	var sequencia = document.forms[0].patx_nr_sequenciaarray[indice].value;
	var count = new Number(0);
	
	for(var i = 0; i < document.forms[0].patx_nr_sequenciaarray.length; i++) {
		if(document.forms[0].patx_nr_sequenciaarray[i].value != '' && (sequencia == document.forms[0].patx_nr_sequenciaarray[i].value)) {
			count++;
		}
	}
	
	if(count > 1) {
		alert('A seq�ncia informada j� est� sendo utilizada!');
		document.forms[0].patx_nr_sequenciaarray[indice].value = '';
		document.forms[0].patx_nr_sequenciaarray[indice].focus();
	}
}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio()">
<html:form action="/AbrirPopupTaxas" styleId="cadastroGrupoRegrasNegociacaoForm">
<html:hidden property="taxasViewState"/>
<html:hidden property="id_grnp_cd_gruporeneparcela"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.detalhes"/></td>
            <td class="principalQuadroPstVazia" >&nbsp; </td>
            <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="100%"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="100%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="100%" valign="top">
                  	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
					<table border="0" width="98%" cellspacing="0" cellpadding="0">
						<tr> 
							<td class="principalLstCab" width="5%" align="center">&nbsp;</td>
				        	<td class="principalLstCab" width="27%"><bean:message key="prompt.TipoTaxa" /></td>
				        	<td class="principalLstCab" width="4%" title="<bean:message key="prompt.sequencia" />">Seq</td>
				        	<td class="principalLstCab" width="14%">Legenda</td>
				        	<td class="principalLstCab" width="14%"><bean:message key="prompt.valor" /></td>
				        	<td class="principalLstCab" width="32%"><bean:message key="prompt.formula" /></td>
				        	<td class="principalLstCab" width="4%">&nbsp;</td>
						</tr>
					</table>
					<table width="100%" height="185px" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
                    	<tr>
                        	<td valign="top">
	                            <div id="lstTaxa" style="overflow-Y:scroll; height: 183px; width: 100%; z-index: 21; position: absolute;">
	                            	<table border="0" width="100%" cellspacing="0" cellpadding="0" id="tableTaxa">
										<tr id="rowTaxa" style="display: none" indice="">
											<input type="hidden" name="id_patx_cd_sequencialarray" />
											
											<td class="principalLstPar" width="5%" align="center">&nbsp;<img src="webFiles/images/botoes/lixeira.gif" id="btExcluirTaxa" class="geralCursoHand" alt="<bean:message key="prompt.excluir"/>" onclick="excluirLinhaTaxa(this.parentNode.parentNode.indice);" /></td>
								        	<td class="principalLstPar" width="27%">
								        		<select name="id_tppx_cd_tptaxasarray" class="principalObjForm" onchange="habilitaDesabilitaCampos(this.parentNode.parentNode.indice)">
								        			<option value=""><bean:message key="prompt.selecione_uma_opcao" /></option>
								        			
								        			<logic:present name="tppxVector">
								        				<logic:iterate name="tppxVector" id="tppxAuxVector">
								        					<option value="<bean:write name='tppxAuxVector' property='field(id_tppx_cd_tptaxas)' />"><bean:write name="tppxAuxVector" property="field(tptx_ds_tptaxas)" /></option>
								        				</logic:iterate>
				                              		</logic:present>
								        		</select>
								        	</td>
								        	<td class="principalLstPar" width="4%">
								        		<input type="text" name="patx_nr_sequenciaarray" style="width: 25px;" class="principalObjForm" maxlength="1" onkeydown="return isDigitoVirgula(event);" onblur="verificaSequencia(this.parentNode.parentNode.indice)"/>
								        	</td>
								        	<td class="principalLstPar" width="14%">
								        		<input type="text" name="patx_ds_observacao" style="width: 90px;" class="principalObjForm" maxlength="100"/>
								        	</td>
								        	<td class="principalLstPar" width="14%">
								        		<input type="text" name="patx_vl_valorarray" style="width: 90px;" class="principalObjForm" maxlength="10" onkeydown="return isDigitoVirgula(event);" onblur="return numberValidate(this, 2, '.', ',', event);"/>
								        	</td>
								        	<td class="principalLstPar" width="32%">
								        		<input type="text" name="patx_ds_formulaarray" style="width: 200px;" class="principalObjForm" maxlength="4000"/>
								        	</td>	
								        	<td class="principalLstPar" width="4%">
								        		<img src="webFiles/images/icones/funcao.gif" width="24" id="btFormula" height="24" class="geralCursoHand" title="<bean:message key="prompt.formula"/>" onclick="abrirPopupFormula(this.parentNode.parentNode.indice)"/>
								        	</td>								        	
										</tr>
									</table> 
	                            </div>
							</td>
                         	</tr>
                  	 </table>
                  	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
		            	<tr> 
		                	<td class="espacoPqn">&nbsp;</td>
		              	</tr>
		            </table>
                    <table  width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    	<tr>
                    		<td>&nbsp;</td>
                    		<td width="25" align="center">
                    			<img src="webFiles/images/botoes/mais.png" alt="Adicionar" name="imgAdicionarTaxa" id="imgAdicionarTaxa" class="geralCursoHand" onclick="adicionarLinhaTaxa()"/>&nbsp;
                    		</td>
                    		<td width="25" align="center">
		                      	<img src="webFiles/images/botoes/gravar.gif" alt="Gravar" name="imgGravar" id="imgGravar" class="geralCursoHand" onclick="gravar()"/>
		                    </td>
                    	</tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
  <div id="aguarde" style="position:absolute; left:210px; top:60px; width:199px; height:148px; z-index:10; visibility: visible"> 
	  <div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
  </div>
</html:form>
</body>
</html>