<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	String nomeCampo = request.getParameter("nomeCampo");
%>

<html>
<head>
<title>..: <bean:message key="prompt.formula"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script type="text/javascript">

var wi = window.dialogArguments;

function inicio() {
	showError('<%=request.getAttribute("msgerro")%>');
	
	document.forms[0].patx_ds_formula.value = eval('wi.document.forms[0].<%=nomeCampo%>[document.forms[0].indice.value].value');
}

function setaFormula() {
	eval('wi.document.forms[0].<%=nomeCampo%>[document.forms[0].indice.value].value = document.forms[0].patx_ds_formula.value');
	window.close();
}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio()">
<html:form action="/AbrirPopupFormula" styleId="cadastroGrupoRegrasNegociacaoForm">
<html:hidden property="indice"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.formula"/></td>
            <td class="principalQuadroPstVazia" >&nbsp; </td>
            <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="100%"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="100%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="100%" valign="top">
                  	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
					<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
		              <tr> 
		                <td width="15%" class="principalLabel" align="right" valign="top">&nbsp;</td>
		                <td width="71%" class="principalLabel"><bean:message key="prompt.descricao"/></td>
		                <td width="14%" class="principalLabel">&nbsp;</td>
		              </tr>
		              <tr> 
		                <td width="15%" class="principalLabel" align="right" valign="top">&nbsp;</td>
		                <td width="71%" class="principalLabel">
		                	<html:text property="patx_ds_formula" styleClass="principalObjForm" maxlength="4000" style="width:510px; height: 105px"/> 
		                </td>
		                <td width="14%" class="principalLabel"><img src="webFiles/images/botoes/setaDown.gif" alt="<bean:message key="prompt.Adicionar"/>" class="geralCursoHand" onclick="setaFormula()"/></td>
		              </tr>
		            </table>
                  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		            	<tr> 
		                	<td class="espacoPqn">&nbsp;</td>
		              	</tr>
		            </table>
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		            	<tr> 
		                	<td class="espacoPqn">&nbsp;</td>
		              	</tr>
		            </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</html:form>
</body>
</html>