<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");

	String fileIncludeIdioma = "../../../webFiles/includes/idioma.jsp";
	String fileInclude = "../../../webFiles/includes/multiempresa.jsp";
%>

<jsp:include page='<%=fileIncludeIdioma%>' flush="true" />
<jsp:include page='<%=fileInclude%>' flush="true" />

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript"
	SRC="webFiles/cobranca/js/grupoRegrasNegociacao.js"></script>
<script language="JavaScript"
	src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript"
	src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language="javascript"
	src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script language="javascript"
	src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>

<link rel="stylesheet" href="webFiles/cobranca/css/global.css"
	type="text/css">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css"
	type="text/css">
<script type="text/javascript">
	function pressEnter(ev) {
    	if (ev.keyCode == 13) {
    		carregaLstGrupoRegrasNegociacao();
    	}
	}
</script>
</head>

<body class="principalBgrPage" text="#000000"
	onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
	<html:form action="/AbrirGrupoRegrasNegociacao"
		styleId="cadastroGrupoRegrasNegociacaoForm">
		<html:hidden property="userAction" />
		<html:hidden property="grre_dh_inativo" />
		<html:hidden property="comissaoViewState" />
		<html:hidden property="parcelamentoViewState" />
		<html:hidden property="entradaViewState" />
		<html:hidden property="regrasNegociacaoViewState" />

		<table width="99%" border="0" cellspacing="0" cellpadding="0"
			height="1">
			<tr>
				<td width="1007" colspan="2">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="principalPstQuadro" height="17" width="166"><bean:message
									key="prompt.GrupoRegraNegociacao" /></td>
							<td class="principalQuadroPstVazia" height="17">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="400">
											<div align="center"></div>
										</td>
									</tr>
								</table>
							</td>
							<td height="17" width="4"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="principalBgrQuadro" valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						height="100%">
						<tr>
							<td valign="top" height="500">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>&nbsp;</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0"
									align="center">
									<tr>
										<td class="principalPstQuadroLinkSelecionado"
											name="tdProcurar" id="tdProcurar"
											onClick="AtivarPasta('pesquisar');"><bean:message
												key="prompt.procurar" /></td>
										<td class="principalPstQuadroLinkNormalMaior"
											name="tdGrupoRegra" id="tdGrupoRegra"
											onClick="AtivarPasta('editar');"><bean:message
												key="prompt.GrupoRegraNegociacao" /></td>
										<td class="principalLabel">&nbsp;</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0"
									align="center" class="principalBordaQuadro">
									<tr>
										<td height="480" valign="top">
											<div id="Procurar"
												style="width: 99%; height: 460px; display: block;">
												<table width="95%" border="0" cellspacing="0"
													cellpadding="0" align="center">
													<tr>
														<td colspan="2">&nbsp;</td>
													</tr>
													<tr>
														<td colspan="2" class="principalLabel"><bean:message
																key="prompt.Descricao" /></td>
													</tr>
													<tr>
														<td width="56%"><html:text property="filtro"
																name="cadastroGrupoRegrasNegociacaoForm"
																styleClass="principalObjForm" maxlength="60"
																onkeyup="pressEnter(event)" /></td>
														<td width="44%"><img
															src="webFiles/images/botoes/setaDown.gif" width="21"
															height="18" class="geralCursoHand"
															onclick="carregaLstGrupoRegrasNegociacao();"
															alt="Pesquisar"></td>
													</tr>
													<tr>
														<td width="56%">&nbsp;</td>
														<td width="44%">&nbsp;</td>
													</tr>
													<tr>
														<td colspan="2">
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td class="principalLstCab" width="5%">&nbsp;</td>
																	<td class="principalLstCab" width="5%">&nbsp;</td>
																	<td class="principalLstCab" width="10%"><bean:message
																			key="prompt.codigo" /></td>
																	<td class="principalLstCab" width="60%"><bean:message
																			key="prompt.Descricao" /></td>
																	<td class="principalLstCab" width="20%"><bean:message
																			key="prompt.inativo" /></td>
																</tr>
															</table>
															<div style="width: 97%; height: 330px; overflow: auto">
																<table width="97%" border="0" cellspacing="0"
																	cellpadding="0" id="tableGrupoRegrasNegociacao">
																	<tr class="geralCursoHand"
																		id="rowGrupoRegrasNegociacao" style="display: none"
																		indice="" id_grre_cd_gruporene="">
																		<td class="principalLstPar" width="5%" align="center"><img
																			src="webFiles/images/botoes/lixeira.gif"
																			name="btExcluir" id="btExcluir"
																			onclick="excluirGrupoRegrasNegociacao(this.parentNode.parentNode.id_grre_cd_gruporene);"
																			width="14" height="14" class="geralCursoHand"
																			alt="Excluir" /></td>
																		<td class="principalLstPar" width="5%" align="center"
																			onclick="abrirModalIdioma('CB_ASTB_IDIOMAGRUPORENE_IMRN.xml', this.parentNode.parentNode.id_grre_cd_gruporene);"><img
																			src="webFiles/images/botoes/GloboAuOn.gif" width="18"
																			height="18" class="geralCursoHand" /></td>
																		<td class="principalLstPar" width="10%" id="tdCodigo"
																			onclick="editarGrupoRegrasNegociacao(this.parentNode.id_grre_cd_gruporene)">&nbsp;</td>
																		<td class="principalLstPar" width="60%"
																			id="tdDescricao"
																			onclick="editarGrupoRegrasNegociacao(this.parentNode.id_grre_cd_gruporene)">&nbsp;</td>
																		<td class="principalLstPar" width="20%" id="tdInativo"
																			onclick="editarGrupoRegrasNegociacao(this.parentNode.id_grre_cd_gruporene)">&nbsp;</td>
																	</tr>
																	<tr id="nenhumGrupoRegrasNegociacao"
																		style="display: block;">
																		<td width="100%" height="329" align="center"
																			class="principalLstPar" colspan="4"><br> <b>Nenhum
																				registro encontrado!</b></td>
																	</tr>
																</table>
															</div>
														</td>
													</tr>
												</table>
											</div>
											<div id="Estrategias"
												style="width: 99%; height: 390px; display: none;">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td>&nbsp;</td>
													</tr>
												</table>
												<table width="98%" border="0" cellspacing="0"
													cellpadding="0" align="center">
													<tr>
														<td class="principalLabel" align="right" height="28"
															width="26%"><bean:message key="prompt.codigo" /> <img
															src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7"></td>
														<td colspan="2" class="principalLabel">
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td width="20%"><html:text
																			property="id_grre_cd_gruporene"
																			name="cadastroGrupoRegrasNegociacaoForm"
																			styleClass="principalObjForm" disabled="true" /></td>
																	<td width="80%">&nbsp;</td>
																</tr>
															</table>
														</td>
														<td width="4%" class="principalLabel">&nbsp;</td>
													</tr>
												</table>
												<table width="98%" border="0" cellspacing="0"
													cellpadding="0" align="center">
													<tr>
														<td class="principalLabel" align="right" height="28"
															width="26%"><bean:message key="prompt.Descricao" />
															<img src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7"></td>
														<td colspan="2" class="principalLabel">
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td width="73%"><html:text
																			property="grre_ds_gruporene" maxlength="60"
																			name="cadastroGrupoRegrasNegociacaoForm"
																			styleClass="principalObjForm" /></td>
																	<td width="27%">&nbsp;</td>
																</tr>
															</table>
														</td>
														<td width="4%" class="principalLabel">&nbsp;</td>
													</tr>
													<tr>
														<td class="principalLabel" align="right" height="28"
															width="26%">&nbsp;<bean:message
																key="prompt.comandoSql" /> <img
															src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7">
														</td>
														<td colspan="2" class="principalLabel">
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td width="73%"><html:textarea
																			property="grre_ds_comandosql"
																			styleClass="principalObjForm"
																			onkeyup="textCounter(this, 2000)"
																			onblur="textCounter(this, 2000)"></html:textarea></td>
																	<td width="27%" class="principalLabel">&nbsp;</td>
																</tr>
															</table>
														</td>
														<td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
													</tr>
													<tr>
														<td class="principalLabel" align="right" height="28"
															width="26%">&nbsp;</td>
														<td colspan="2" class="principalLabel" align="right">
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td width="61%">&nbsp;</td>
																	<td width="30%" class="principalLabel"><logic:equal
																			value="" name="cadastroGrupoRegrasNegociacaoForm"
																			property="grre_dh_inativo">
																			<input type="checkbox" name="inativo" value="ativo" />
																		</logic:equal> <logic:notEqual value=""
																			name="cadastroGrupoRegrasNegociacaoForm"
																			property="grre_dh_inativo">
																			<input type="checkbox" name="inativo" value="inativo"
																				checked="checked" />
																		</logic:notEqual> <bean:message key="prompt.inativo" /></td>
																	<td width="9%" align="right">&nbsp;</td>
																</tr>
															</table>
														</td>
														<td width="4%" class="principalLabel" colspan="2"></td>
													</tr>
												</table>
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td class="EspacoPequeno">&nbsp;</td>
													</tr>
												</table>
												<table width="700px" border="0" cellspacing="0"
													cellpadding="0" align="center">
													<tr>
														<td id="tdUm"
															class="principalPstQuadroLinkSelecionadoMaior"
															onclick="AtivarPasta('UM');"><bean:message
																key="prompt.regrasNegociacao" /></td>
														<td id="tdDois" class="principalPstQuadroLinkNormalMaior"
															onclick="AtivarPasta('DOIS');"><bean:message
																key="prompt.Parcelamento" /></td>
														<td id="tdTres" class="principalPstQuadroLinkNormalMaior"
															onclick="AtivarPasta('TRES');"><bean:message
																key="prompt.Entrada" /></td>
														<td id="tdQuatro"
															class="principalPstQuadroLinkNormalMaior"
															onclick="AtivarPasta('QUATRO');"><bean:message
																key="prompt.comissao" /></td>
													</tr>
												</table>
												<table width="700px" border="0" cellspacing="0"
													cellpadding="0" class="principalBordaQuadro" align="center"
													height="300">
													<tr>
														<td valign="top">
															<div id="um"
																style="width: 700px; height: 100%; overflow: hidden;"
																align="center">
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0" class="EspacoPequeno">
																	<tr>
																		<td>&nbsp;</td>
																	</tr>
																</table>
																<div style="width: 690px; height: 270px; overflow: auto;margin-left: 3px"
																	id="lstRegraNegociacao" class="principalBordaQuadro">
																	<table border="0"
																		id="tableRegraNegociacao" cellspacing="0"
																		cellpadding="0" align="center">
																		<thead>
																			<tr>
																				<th class="principalLstCab" width="30px"
																					align="center">&nbsp;</th>
																				<th class="principalLstCab" width="30px"
																					align="center">&nbsp;</th>
																				<th class="principalLstCab" width="300px"><bean:message
																						key="prompt.CampoDeRegra" /></th>
																				<th class="principalLstCab" width="240px">&nbsp;&nbsp;&nbsp;<bean:message
																						key="prompt.FaixaInicial" /></th>
																				<th class="principalLstCab" width="240px">&nbsp;&nbsp;&nbsp;<bean:message
																						key="prompt.FaixaFinal" /></th>
																				<th class="principalLstCab" width="70px"></th>
																			</tr>
																		</thead>
																		<tbody class="principalBordaQuadro">
																			<tr id="rowRegraNegociacao" style="display: none"
																				indice="" id_rene_cd_regnegociacao="">
																				<input type="hidden"
																					name="id_rene_cd_regnegociacaoarray" />
																				<input type="hidden" name="tipoOperacaoRene" />

																				<td class="principalLstPar" align="center">&nbsp;<img
																					src="webFiles/images/botoes/lixeira.gif"
																					id="btExcluirRegraNegociacao"
																					class="geralCursoHand"
																					alt="<bean:message key="prompt.excluir"/>"
																					onclick="excluirLinhaRegraNegociacao(this.parentNode.parentNode.indice);" /></td>
																				<td class="principalLstPar" align="center">&nbsp;<img
																					src="webFiles/images/botoes/Alterar.gif" width="16"
																					height="14" id="btDetalhesRegraNegociacao"
																					class="geralCursoHand"
																					alt="<bean:message key="prompt.detalhes"/>"
																					onclick="abrirDetalhesRegraNegociacao(this.parentNode.parentNode.id_rene_cd_regnegociacao);" /></td>
																				<td class="principalLstPar"><select
																					name="id_cane_cd_camponegonegoarray"
																					class="principalObjForm"
																					onchange="limpaCamposFaixaRegraNegociacao(this.parentNode.parentNode.indice)">
																						<option value=""><bean:message
																								key="prompt.selecione_uma_opcao" /></option>

																						<logic:present name="caneVector">
																							<logic:iterate name="caneVector"
																								id="caneAuxVector">
																								<option
																									value="<bean:write name='caneAuxVector' property='field(id_tipo_cane)' />"><bean:write
																										name="caneAuxVector"
																										property="field(cane_ds_camponego)" /></option>
																							</logic:iterate>
																						</logic:present>
																				</select></td>
																				<td class="principalLstPar"><input type="text"
																					name="rene_vl_faixainicialarray"
																					class="principalObjForm" maxlength="10"
																					onkeydown="return verificaTipoCampoOnKeyDown(this, event, document.forms[0].id_cane_cd_camponegonegoarray[this.parentNode.parentNode.indice].value);"
																					onblur="return verificaTipoCampoOnBlur(this, event, document.forms[0].id_cane_cd_camponegonegoarray[this.parentNode.parentNode.indice].value);" />
																				</td>
																				<td class="principalLstPar"><input type="text"
																					name="rene_vl_faixafinalarray"
																					class="principalObjForm" maxlength="10"
																					onkeydown="return verificaTipoCampoOnKeyDown(this, event, document.forms[0].id_cane_cd_camponegonegoarray[this.parentNode.parentNode.indice].value);"
																					onblur="return verificaTipoCampoOnBlur(this, event, document.forms[0].id_cane_cd_camponegonegoarray[this.parentNode.parentNode.indice].value);" />
																				</td>
																				<td class="principalLstPar" width="180px">&nbsp;</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td class="EspacoPequeno">&nbsp;</td>
																	</tr>
																</table>
																<table width="98%" border="0" cellspacing="0"
																	cellpadding="0" align="center">
																	<tr>
																		<td align="right"><img
																			src="webFiles/images/botoes/mais.png" alt="Adicionar"
																			name="imgAdicionarRegraNegociacao"
																			id="imgAdicionarRegraNegociacao"
																			class="geralCursoHand"
																			onclick="adicionarLinhaRegraNegociacao();" />&nbsp;</td>
																	</tr>
																</table>
															</div>
															<div id="dois"
																style="width: 700px; height: 100%; overflow: hidden;"
																align="center"
																style="width: 690px; height: 290px; display: none">
																<table width="690px" border="0" cellspacing="0"
																	cellpadding="0" class="EspacoPequeno">
																	<tr>
																		<td>&nbsp;</td>
																	</tr>
																</table>
																<div style="width: 690px; height: 270px; overflow: auto;margin-left: 3px"
																	id="lstParcela" class="principalBordaQuadro">
																	<table border="0" cellspacing="0" cellpadding="0"
																		align="center" id="tableParcela" >
																		<thead>
																			<tr>
																				<th class="principalLstCab" width="30px"
																					align="center">&nbsp;
																				</th>
																				<th class="principalLstCab" width="30px"
																					align="center">&nbsp;
																				</th>
																				<th class="principalLstCab" width="30px"
																					align="center">&nbsp;
																				</th>
																				<th class="principalLstCab" width="200px"><bean:message
																						key="prompt.CampoDeRegra" />
																				</th>
																				<th class="principalLstCab" width="90px"><bean:message
																						key="prompt.FaixaInicial" />
																				</th>
																				<th class="principalLstCab" width="90px">&nbsp;<bean:message
																						key="prompt.FaixaFinal" />
																				</th>
																				<th class="principalLstCab" width="85px"
																					title="<bean:message key="prompt.ParcelaInicial" />">&nbsp;&nbsp;&nbsp;Inicial

																				
																				</th>
																				<th class="principalLstCab" width="85px"
																					title="<bean:message key="prompt.ParcelaFinal" />">&nbsp;&nbsp;Final

																				
																				</th>
																				<th class="principalLstCab" width="85px"
																					align="center"><bean:message
																						key="prompt.Boletagem" />
																				</th>
																				<th class="principalLstCab" width="70px"
																					title="Limite de prazo(dias) para a primeira parcela/entrada.">Dias
																					Ent.
																				</th>

																				<!--
										        	<td class="principalLstCab" width="200px"><bean : message key="prompt.TipoDeDesconto" /></td>
										        	<td class="principalLstCab" width="120px"><bean : message key="prompt.DescontoInicial" /></td>
										        	<td class="principalLstCab" width="120px"><bean : message key="prompt.DescontoFinal" /></td>
										        	<td class="principalLstCab" width="200px"><bean : message key="prompt.TipoDeDescontoEspecial" /></td>
										        	<td class="principalLstCab" width="150px"><bean : message key="prompt.TipoDeDescontoInicial" /></td>
										        	<td class="principalLstCab" width="150px"><bean : message key="prompt.TipoDeDescontoFinal" /></td>
										        	<td class="principalLstCab" width="200px"><bean : message key="prompt.AplicaDescontoNaParcela" /></td>
										        	<td class="principalLstCab" width="20px"></td>
										        	-->
																			</tr>
																		</thead>
																		<tbody>
																			<tr id="rowParcela" style="display: none" indice=""
																				id_grnp_cd_gruporeneparcela="">
																				<input type="hidden"
																					name="id_grnp_cd_gruporeneparcelaarray" />
																				<input type="hidden" name="tipoOperacaoGrpn" />

																				<td class="principalLstPar" align="center">&nbsp;<img
																					width="16" src="webFiles/images/botoes/lixeira.gif"
																					id="btExcluirParcela" class="geralCursoHand"
																					alt="<bean:message key="prompt.excluir"/>"
																					onclick="excluirLinhaParcela(this.parentNode.parentNode.indice);" /></td>
																				<td class="principalLstPar" align="center">&nbsp;<img
																					src="webFiles/images/botoes/Alterar.gif" width="16"
																					height="14" id="btDetalhesParcela"
																					class="geralCursoHand"
																					alt="<bean:message key="prompt.detalhes"/>"
																					onclick="abrirDetalhesParcela(this.parentNode.parentNode.id_grnp_cd_gruporeneparcela);" /></td>
																				<td class="principalLstPar" align="center">&nbsp;<img
																					width="16"
																					src="/plusoft-resources/images/lite/icones/desconto.gif"
																					id="btDescontoParcela" class="geralCursoHand"
																					alt="<bean:message key="prompt.TipoDeDesconto"/>"
																					onclick="abrirDescontoParcela(this.parentNode.parentNode.id_grnp_cd_gruporeneparcela);" /></td>
																				<td class="principalLstPar"><select
																					name="id_cane_cd_camponegoparcarray"
																					class="principalObjForm"
																					onchange="limpaCamposFaixaParcela(this.parentNode.parentNode.indice)">
																						<option value=""><bean:message
																								key="prompt.selecione_uma_opcao" /></option>

																						<logic:present name="caneVector">
																							<logic:iterate name="caneVector"
																								id="caneAuxVector">
																								<option
																									value="<bean:write name='caneAuxVector' property='field(id_tipo_cane)' />"><bean:write
																										name="caneAuxVector"
																										property="field(cane_ds_camponego)" /></option>
																							</logic:iterate>
																						</logic:present>
																				</select></td>
																				<td class="principalLstPar"><input type="text"
																					name="grnp_vl_faixainicialarray"
																					class="principalObjForm" maxlength="10"
																					onkeydown="return verificaTipoCampoOnKeyDown(this, event, document.forms[0].id_cane_cd_camponegoparcarray[this.parentNode.parentNode.indice].value);"
																					onblur="return verificaTipoCampoOnBlur(this, event, document.forms[0].id_cane_cd_camponegoparcarray[this.parentNode.parentNode.indice].value);" />
																				</td>
																				<td class="principalLstPar"><input type="text"
																					name="grnp_vl_faixafinalarray"
																					class="principalObjForm" maxlength="10"
																					onkeydown="return verificaTipoCampoOnKeyDown(this, event, document.forms[0].id_cane_cd_camponegoparcarray[this.parentNode.parentNode.indice].value);"
																					onblur="return verificaTipoCampoOnBlur(this, event, document.forms[0].id_cane_cd_camponegoparcarray[this.parentNode.parentNode.indice].value);" />
																				</td>
																				<td class="principalLstPar"><input type="text"
																					name="grnp_nr_parcelamentoinicialarray"
																					class="principalObjForm" maxlength="10"
																					onkeydown="return isDigito(event);" /></td>
																				<td class="principalLstPar"><input type="text"
																					name="grnp_nr_parcelamentofinalarray"
																					class="principalObjForm" maxlength="10"
																					onkeydown="return isDigito(event);" /></td>
																				<td class="principalLstPar" align="center"><input
																					type="checkbox" name="grnp_in_boletagemarray"
																					value="S" /></td>
																				<td class="principalLstPar"><input type="text"
																					name="grnp_nr_diaspripgtoarray"
																					class="principalObjForm" maxlength="3"
																					onkeydown="return isDigito(event);" /></td>

																				<!--
										        	<td class="principalLstPar" width="200px">
										        		<select name="id_tpco_cd_tpdescontoarray" class="principalObjForm" onchange="habilitaDesabilitaTpDescontoParcela(this.value, this.parentNode.parentNode.indice, '1')">
										        			<option value=""><bean:message key="prompt.selecione_uma_opcao" /></option>
										        			
										        			<logic : present name="tpcoVector">
										        				<logic : iterate name="tpcoVector" id="tpcoAuxVector">
										        					<option value="<bean : write name='tpcoAuxVector' property='field(id_tpco_cd_tpdesconto)' />"><bean : write name="tpcoAuxVector" property="field(tpco_ds_tpdesconto)" /></option>
										        				</logic : iterate>
						                              		</logic : present>
										        		</select>
										        	</td>
										        	<td class="principalLstPar" width="120px">
										        		<input type="text" name="grnp_vl_descinicialarray" class="principalObjForm" maxlength="10" onkeydown="return isDigitoVirgula(event);" onblur="return numberValidate(this, 2, '.', ',', event);"/>
										        	</td>
										        	
										        	<td class="principalLstPar" width="120px">
										        		<input type="text" name="grnp_vl_descfinalarray" class="principalObjForm" maxlength="10" onkeydown="return isDigitoVirgula(event);" onblur="return numberValidate(this, 2, '.', ',', event);"/>
										        	</td>
										        	
										        	<td class="principalLstPar" width="200px">
										        		<select name="id_tpco_cd_tpdescontoesparray" class="principalObjForm" onchange="habilitaDesabilitaTpDescontoParcela(this.value, this.parentNode.parentNode.indice, '2')">
										        			<option value=""><bean : message key="prompt.selecione_uma_opcao" /></option>
										        			
										        			<logic : present name="tpcoVector">
										        				<logic : iterate name="tpcoVector" id="tpcoAuxVector">
										        					<option value="<bean : write name='tpcoAuxVector' property='field(id_tpco_cd_tpdesconto)' />"><bean : write name="tpcoAuxVector" property="field(tpco_ds_tpdesconto)" /></option>
										        				</logic : iterate>
						                              		</logic : present>
										        		</select>
										        	</td>
										        	<td class="principalLstPar" width="150px">
										        		<input type="text" name="grnp_vl_descespinicialarray" class="principalObjForm" maxlength="10" onkeydown="return isDigitoVirgula(event);" onblur="return numberValidate(this, 2, '.', ',', event);"/>
										        	</td>
										        	
										        	<td class="principalLstPar" width="150px">
										        		<input type="text" name="grnp_vl_descespfinalarray" class="principalObjForm" maxlength="10" onkeydown="return isDigitoVirgula(event);" onblur="return numberValidate(this, 2, '.', ',', event);"/>
										        	</td>
										        	<td class="principalLstPar" width="200px">
										        		<html:select property="grpn_in_descparcelaarray" styleClass="principalObjForm">
						                              		<html:option value=""><bean : message key="prompt.selecione_uma_opcao" /></html:option>
						                              		<html:option value="S"><bean : message key="prompt.sim" /></html:option>
						                              		<html:option value="N"><bean : message key="prompt.nao" /></html:option>
						                              	</html:select>
										        	</td -->
																			</tr>
																		</tbody>
																	</table>
																</div>
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td class="EspacoPequeno">&nbsp;</td>
																	</tr>
																</table>
																<table width="98%" border="0" cellspacing="0"
																	cellpadding="0" align="center">
																	<tr>
																		<td align="right"><img
																			src="webFiles/images/botoes/mais.png" alt="Adicionar"
																			name="imgAdicionarParcela" id="imgAdicionarParcela"
																			class="geralCursoHand"
																			onclick="adicionarLinhaParcela();" />&nbsp;</td>
																	</tr>
																</table>
															</div>
															<div id="tres"
																style="width: 700px; height: 290px; display: none">
																<table width="690px" border="0" cellspacing="0"
																	cellpadding="0" class="EspacoPequeno">
																	<tr>
																		<td>&nbsp;</td>
																	</tr>
																</table>
																<div style="width: 690px; height: 270px; overflow: auto;margin-left: 3px"
																	id="lstEntrada" class="principalBordaQuadro">
																	<table width="1190px" border="0" id="tableEntrada"
																		cellspacing="0" cellpadding="0" align="center">
																		<thead>
																			<th class="principalLstCab" width="55px"
																				align="center">&nbsp;</th>
																			<th class="principalLstCab" width="210px"><bean:message
																					key="prompt.CampoDeRegra" /></th>
																			<th class="principalLstCab" width="110px"><bean:message
																					key="prompt.FaixaInicial" /></th>
																			<th class="principalLstCab" width="110px"><bean:message
																					key="prompt.FaixaFinal" /></th>
																			<th class="principalLstCab" width="170px"><bean:message
																					key="prompt.observacao" />
																			</td>
																			<th class="principalLstCab" width="200px"><bean:message
																					key="prompt.TipoDeEntrada" /></th>
																			<th class="principalLstCab" width="150px"><bean:message
																					key="prompt.ValorMinimoDeEntrada" /></th>
																			<th class="principalLstCab" width="150px"><bean:message
																					key="prompt.ValorMaximoDeEntrada" />
																			</td>
																			<th class="principalLstCab" width="20px"></th>
																		</thead>
																		<tbody>
																			<tr id="rowEntrada" style="display: none" indice="">
																				<input type="hidden"
																					name="id_gren_cd_gruporeneentradaarray" />
																				<input type="hidden" name="tipoOperacaoGren" />

																				<td class="principalLstPar" align="center">&nbsp;<img
																					src="webFiles/images/botoes/lixeira.gif"
																					id="btExcluirEntrada" class="geralCursoHand"
																					alt="<bean:message key="prompt.excluir"/>"
																					onclick="excluirLinhaEntrada(this.parentNode.parentNode.indice);" /></td>
																				<td class="principalLstPar"><select
																					name="id_cane_cd_camponegoentrarray"
																					class="principalObjForm"
																					onchange="limpaCamposFaixaEntrada(this.parentNode.parentNode.indice)">
																						<option value=""><bean:message
																								key="prompt.selecione_uma_opcao" /></option>

																						<logic:present name="caneVector">
																							<logic:iterate name="caneVector"
																								id="caneAuxVector">
																								<option
																									value="<bean:write name='caneAuxVector' property='field(id_tipo_cane)' />"><bean:write
																										name="caneAuxVector"
																										property="field(cane_ds_camponego)" /></option>
																							</logic:iterate>
																						</logic:present>
																				</select></td>
																				<td class="principalLstPar"><input type="text"
																					name="gren_vl_faixainicialarray"
																					class="principalObjForm" maxlength="10"
																					onkeydown="return verificaTipoCampoOnKeyDown(this, event, document.forms[0].id_cane_cd_camponegoentrarray[this.parentNode.parentNode.indice].value);"
																					onblur="return verificaTipoCampoOnBlur(this, event, document.forms[0].id_cane_cd_camponegoentrarray[this.parentNode.parentNode.indice].value);" />
																				</td>
																				<td class="principalLstPar"><input type="text"
																					name="gren_vl_faixafinalarray"
																					class="principalObjForm" maxlength="10"
																					onkeydown="return verificaTipoCampoOnKeyDown(this, event, document.forms[0].id_cane_cd_camponegoentrarray[this.parentNode.parentNode.indice].value);"
																					onblur="return verificaTipoCampoOnBlur(this, event, document.forms[0].id_cane_cd_camponegoentrarray[this.parentNode.parentNode.indice].value);" />
																				</td>
																				<td class="principalLstPar"><input type="text"
																					name="gren_ds_obsarray" class="principalObjForm"
																					maxlength="100" /></td>
																				<td class="principalLstPar"><html:select
																						property="gren_in_tpentradaarray"
																						styleClass="principalObjForm">
																						<html:option value="">
																							<bean:message key="prompt.selecione_uma_opcao" />
																						</html:option>
																						<html:option value="P">
																							<bean:message key="prompt.percentual" />
																						</html:option>
																						<html:option value="F">
																							<bean:message key="prompt.fixo" />
																						</html:option>
																					</html:select></td>
																				<td class="principalLstPar"><input type="text"
																					name=gren_vl_valorminimoarray
																					class="principalObjForm" maxlength="10"
																					onkeydown="return isDigitoVirgula(event);"
																					onblur="return numberValidate(this, 2, '.', ',', event);" />
																				</td>
																				<td class="principalLstPar"><input type="text"
																					name=gren_vl_valormaximoarray
																					class="principalObjForm" maxlength="10"
																					onkeydown="return isDigitoVirgula(event);"
																					onblur="return numberValidate(this, 2, '.', ',', event);" />
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td class="EspacoPequeno">&nbsp;</td>
																	</tr>
																</table>
																<table width="98%" border="0" cellspacing="0"
																	cellpadding="0" align="center">
																	<tr>
																		<td align="right"><img
																			src="webFiles/images/botoes/mais.png" alt="Adicionar"
																			name="imgAdicionarEntrada" id="imgAdicionarEntrada"
																			class="geralCursoHand"
																			onclick="adicionarLinhaEntrada();" />&nbsp;</td>
																	</tr>
																</table>
															</div>
															<div id="quatro"
																style="width: 100%; height: 290px; display: none">
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0" class="EspacoPequeno">
																	<tr>
																		<td>&nbsp;</td>
																	</tr>
																</table>
																<div id="lstComissao" class="principalBordaQuadro"
																	style="overflow: auto; width: 690px; height: 270px; display: block;margin-left: 3px">
																	<table width="1430px" border="0" id="tableComissao"
																		cellspacing="0" cellpadding="0" align="center">
																		<thead>
																			<tr>
																				<th class="principalLstCab" width="30px"
																					align="center">&nbsp;</th>
																				<th class="principalLstCab" width="200px"><bean:message
																						key="prompt.CampoDeRegra" /></th>
																				<th class="principalLstCab" width="100px"><bean:message
																						key="prompt.FaixaInicial" /></th>
																				<th class="principalLstCab" width="100px"><bean:message
																						key="prompt.FaixaFinal" /></th>
																				<th class="principalLstCab" width="150px"><bean:message
																						key="prompt.observacao" /></th>
																				<th class="principalLstCab" width="200px"><bean:message
																						key="prompt.TipoDeComissaoDaOperadora" /></th>
																				<th class="principalLstCab" width="200px"><bean:message
																						key="prompt.ValorDeComissaoDaOperadora" /></th>
																				<th class="principalLstCab" width="200px"><bean:message
																						key="prompt.TipoDeComissaoDoOperador" /></th>
																				<td class="principalLstCab" width="200px"><bean:message
																						key="prompt.ValorDeComissaoDoOperador" />
																				</th>
																				<th class="principalLstCab" width="20px"></th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr id="rowComissao" style="display: none"
																							indice="">
																							<input type="hidden"
																								name="id_grco_cd_gruporenecomissaarray" />
																							<input type="hidden" name="tipoOperacaoGrco" />

																							<td class="principalLstPar" width="30px"
																								align="center">&nbsp;<img
																								src="webFiles/images/botoes/lixeira.gif"
																								id="btExcluirComissao" class="geralCursoHand"
																								alt="<bean:message key="prompt.excluir"/>"
																								onclick="excluirLinhaComissao(this.parentNode.parentNode.indice);" /></td>
																							<td class="principalLstPar" width="200px"><select
																								name="id_cane_cd_camponegoarray"
																								class="principalObjForm"
																								onchange="limpaCamposFaixaComissao(this.parentNode.parentNode.indice)">
																									<option value=""><bean:message
																											key="prompt.selecione_uma_opcao" /></option>

																									<logic:present name="caneVector">
																										<logic:iterate name="caneVector"
																											id="caneAuxVector">
																											<option
																												value="<bean:write name='caneAuxVector' property='field(id_tipo_cane)' />"><bean:write
																													name="caneAuxVector"
																													property="field(cane_ds_camponego)" /></option>
																										</logic:iterate>
																									</logic:present>
																							</select></td>
																							<td class="principalLstPar" width="100px"><input
																								type="text" name="grco_vl_faixainicialarray"
																								class="principalObjForm" maxlength="10"
																								onkeydown="return verificaTipoCampoOnKeyDown(this, event, document.forms[0].id_cane_cd_camponegoarray[this.parentNode.parentNode.indice].value);"
																								onblur="return verificaTipoCampoOnBlur(this, event, document.forms[0].id_cane_cd_camponegoarray[this.parentNode.parentNode.indice].value);" />
																							</td>
																							<td class="principalLstPar" width="100px"><input
																								type="text" name="grco_vl_faixafinalarray"
																								class="principalObjForm" maxlength="10"
																								onkeydown="return verificaTipoCampoOnKeyDown(this, event, document.forms[0].id_cane_cd_camponegoarray[this.parentNode.parentNode.indice].value);"
																								onblur="return verificaTipoCampoOnBlur(this, event, document.forms[0].id_cane_cd_camponegoarray[this.parentNode.parentNode.indice].value);" />
																							</td>
																							<td class="principalLstPar" width="150px"><input
																								type="text" name="grco_ds_gruporenecomissaarray"
																								class="principalObjForm" maxlength="100" /></td>
																							<td class="principalLstPar" width="200px"><html:select
																									property="grco_in_tpcomoperadoraarray"
																									styleClass="principalObjForm">
																									<html:option value="">
																										<bean:message key="prompt.selecione_uma_opcao" />
																									</html:option>
																									<html:option value="P">
																										<bean:message key="prompt.percentual" />
																									</html:option>
																									<html:option value="F">
																										<bean:message key="prompt.fixo" />
																									</html:option>
																								</html:select></td>
																							<td class="principalLstPar" width="200px"><input
																								type="text" name="grco_vl_comoperadoraarray"
																								class="principalObjForm" maxlength="10"
																								onkeydown="return isDigitoVirgula(event);"
																								onblur="return numberValidate(this, 2, '.', ',', event);" />
																							</td>
																							<td class="principalLstPar" width="200px"><html:select
																									property="grco_in_tpcomoperadorarray"
																									styleClass="principalObjForm">
																									<html:option value="">
																										<bean:message key="prompt.selecione_uma_opcao" />
																									</html:option>
																									<html:option value="P">
																										<bean:message key="prompt.percentual" />
																									</html:option>
																									<html:option value="F">
																										<bean:message key="prompt.fixo" />
																									</html:option>
																								</html:select></td>
																							<td class="principalLstPar" width="200px"><input
																								type="text" name="grco_vl_comoperadorarray"
																								class="principalObjForm" maxlength="10"
																								onkeydown="return isDigitoVirgula(event);"
																								onblur="return numberValidate(this, 2, '.', ',', event);" />
																							</td>
																						</tr>
																		</tbody>
																	</table>
																</div>
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td class="EspacoPequeno">&nbsp;</td>
																	</tr>
																</table>
																<table width="98%" border="0" cellspacing="0"
																	cellpadding="0" align="center">
																	<tr>
																		<td align="right"><img
																			src="webFiles/images/botoes/mais.png" alt="Adicionar"
																			name="imgAdicionarComissao" id="imgAdicionarComissao"
																			class="geralCursoHand"
																			onclick="adicionarLinhaComissao();" />&nbsp;</td>
																	</tr>
																</table>
															</div>
														</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="EspacoPequeno">&nbsp;</td>
									</tr>
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td>&nbsp;</td>
													<td width="25" align="center"><img
														src="webFiles/images/botoes/new.gif" alt="Novo"
														name="imgNovo" id="imgNovo" class="geralCursoHand"
														onclick="novo();" /></td>
													<td width="25" align="center"><img
														src="webFiles/images/botoes/gravar.gif" alt="Gravar"
														name="imgGravar" id="imgGravar" class="geralCursoHand"
														onclick="salvarGrupoRegrasNegociacao();" /></td>
													<td width="25" align="center"><img
														src="webFiles/images/botoes/cancelar.gif" alt="Cancelar"
														name="imgCancelar" id="imgCancelar" class="geralCursoHand"
														onclick="cancelar()" /></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td class="EspacoPequeno">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td width="4" height="550"><img
					src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
			<tr>
				<td width="100%"><img
					src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img
					src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
					height="4"></td>
			</tr>
		</table>
	</html:form>
</body>

<script>
	temPermissaoIncluir = getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_INCLUSAO_CHAVE%>');
	temPermissaoAlterar = getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>');
	temPermissaoExcluir = getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_EXCLUSAO_CHAVE%>');
	
	function verificaPermissaoExcluir() {
		if (!temPermissaoExcluir){
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.cadastroGrupoRegrasNegociacaoForm.btExcluir'+i+');');
			}
		}
	}
	
	verificaPermissaoExcluir();
</script>

<logic:equal name="cadastroGrupoRegrasNegociacaoForm"
	property="userAction" value="init">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.cadastroGrupoRegrasNegociacaoForm.imgNovo);
			setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.cadastroGrupoRegrasNegociacaoForm.imgGravar);
			desabilitaCampos();
		}
	</script>
</logic:equal>
<logic:equal name="cadastroGrupoRegrasNegociacaoForm"
	property="userAction" value="alterar">
	<script>
		if(!temPermissaoAlterar) {
			setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.cadastroGrupoRegrasNegociacaoForm.imgNovo);
			setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.cadastroGrupoRegrasNegociacaoForm.imgGravar);
			desabilitaCampos();
		}
	</script>
</logic:equal>
<logic:equal name="cadastroGrupoRegrasNegociacaoForm"
	property="userAction" value="cancelar">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.cadastroGrupoRegrasNegociacaoForm.imgNovo);
			setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>
		',
					document.cadastroGrupoRegrasNegociacaoForm.imgGravar);
			desabilitaCampos();
		}
	</script>
</logic:equal>


<script>
	habilitaListaEmpresas();
	setaArquivoXml("CB_ASTB_IDIOMAGRUPORENE_IMRN.xml");
	habilitaTelaIdioma();
</script>
</html>