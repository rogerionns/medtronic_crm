<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="ValidarLayout.do" styleId="layoutImportacaoForm">  	
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr> 
        <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.ValidacaoLayout" /></td>
        <td class="principalQuadroPstVazia" height="17"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td width="400"> 
                <div align="center"></div>
              </td>
            </tr>
          </table>
        </td>
        <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="540px">
        <tr>
	        <td valign="top"> 
	        	<html:textarea property="retornoValidacaoLayout" rows="34" cols="96" readonly="true"></html:textarea>
	        </td>
        </tr>
      </table>
    </td>
  	<td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>