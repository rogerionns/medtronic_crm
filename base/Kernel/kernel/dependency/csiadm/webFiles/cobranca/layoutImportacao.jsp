<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/includes/data.js"></script>
<script language="JavaScript">
	var layoutArray = new Array();
	var countLayout = new Number(0);

	function inicio() {
		if(document.forms[0].id_laou_cd_sequencial.value != '') {
			carregaRegistros();
		}
		
		if(document.forms[0].mensagem.value != '') {
			alert(document.forms[0].mensagem.value);
		}
	}

	<!--
	function MM_reloadPage(init) {  //reloads the window if Nav4 resized
	  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
	    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
	  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
	}
	MM_reloadPage(true);
	// -->
	
	function SetClassFolder(pasta, estilo) {
	 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
	 eval(stracao);
	  } 
	
	
	function AtivarPasta(pasta)
	{
	switch (pasta)
	{
	case 'PROCURAR':
		MM_showHideLayers('layout','','show','historico','','hide')
		SetClassFolder('tdLayout','principalPstQuadroLinkSelecionadoGrande');
		SetClassFolder('tdHistorico','principalPstQuadroLinkNormalGrande');
		break;
	
	case 'CONTEUDO':
		MM_showHideLayers('layout','','hide','historico','','show')
		SetClassFolder('tdLayout','principalPstQuadroLinkNormalGrande');
		SetClassFolder('tdHistorico','principalPstQuadroLinkSelecionadoGrande');
		break;
	}
	 eval(stracao);
	}
	
	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
	    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
	    obj.display=v; }
	}

	function carregaRegistros() {
//		document.all.item('aguarde').style.display = 'block';
		ifrmCamposLayout.location.href = 'CarregarCamposLayout.do?id_laou_cd_sequencial=' + document.forms[0].id_laou_cd_sequencial.value;
	}
	
	function carregaHistorico() {
		ifrmHistoricoLayout.location.href = 'CarregarHistoricoLayout.do?id_laou_cd_sequencial=' + document.forms[0].id_laou_cd_sequencial.value;
	}
		
	function cancelar() {
		if(confirm('<bean:message key="prompt.DesejaCancelar" />')) {
			document.forms[0].id_laou_cd_sequencial.value = '';
			document.forms[0].submit();
		}
	}
	
	function validarLayout() {	
		if(ifrmCamposLayout.countRegistros >= 1) {
			if(document.forms[0].caminhoArquivo.value == '') {
				alert('<bean:message key="prompt.OCampoArquivoEObrigatorio" />');
				document.forms[0].caminhoArquivo.focus();
				return false;
			}
			
			var possuiCabecalho = document.forms[0].possuiCabecalho.checked ? 'S' : 'N';
			
			showModalDialog('ValidarLayout.do?id_laou_cd_sequencial=' + document.forms[0].id_laou_cd_sequencial.value + '&caminhoArquivo=' + document.forms[0].caminhoArquivo.value + '&possuiCabecalho=' + possuiCabecalho + '&delimitador=' + document.forms[0].delimitador.value, 0, 'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:605px,dialogTop:100px,dialogLeft:200px');
		} else {
			alert('<bean:message key="prompt.OLayoutSelecionadoNaoPossuiNenhumCampoCadastrado" />');
		}
	}	

	function duplicar(){
		if(document.forms[0].id_laou_cd_sequencial.value > 0) {
			showModalDialog('AbreDuplicarLayout.do', window, 'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:260px,dialogTop:100px,dialogLeft:200px');
		} else {
			alert('Selecionar o Layout a ser duplicado!');
		}
	}
	
	function desabilitaCamposLayout() {
		//document.forms[0].id_laou_cd_sequencial.disabled = true;
		document.forms[0].caminhoArquivo.disabled = true;
		document.forms[0].possuiCabecalho.disabled = true;
		document.forms[0].delimitador.disabled = true;
	}

	function imprimir(){

		var prtContent = ifrmCamposLayout.tituloLista;
		var prtContentII = ifrmCamposLayout.lstRegistro;
		divPrint.innerHTML = prtContentII.innerHTML;
				                              		
		for(i = 0;i < document.all.item("lico_nr_sequencia").length;i++){
			objIdTbl = document.all.item("tiraTable" + [i]);
			document.getElementById("noPrint" + [i]).removeChild(objIdTbl);
		}
		var WinPrint = window.open('','','left=0,top=0,width=1,height=1,t oolbar=0,scrollbars=0,status=0');
		WinPrint.document.write("Layout : " + document.forms[0].id_laou_cd_sequencial[document.forms[0].id_laou_cd_sequencial.selectedIndex].innerText);
		WinPrint.document.write(prtContent.innerHTML);
		WinPrint.document.write(divPrint.innerHTML);
		WinPrint.document.close();
		WinPrint.focus();
		WinPrint.print();
		WinPrint.close();
	}
</script>
</head>
<body class="principalBgrPage" text="#000000" onload="inicio()">
<html:form action="AbrirTelaLayoutImportacao" styleId="layoutImportacaoForm">
<html:hidden property="hlco_nr_identificador"/>
<html:hidden property="lico_nr_sequencia_rollback"/>
<html:hidden property="mensagem"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLabel" height="30">&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0"	align="center">
        <tr> 
          <td height="254"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				   <tr>
					  <td class="principalPstQuadroLinkVazio">
						 <table border="0" cellspacing="0" cellpadding="0">
						    <tr>
							   <td class="principalPstQuadroLinkSelecionadoGrande" id="tdLayout" name="tdLayout" onClick="AtivarPasta('PROCURAR');">
									<bean:message key="prompt.LayoutImportacao"/>
                               </td>
							   <td class="principalPstQuadroLinkNormalGrande" id="tdHistorico" name="tdHistorico"	onClick="AtivarPasta('CONTEUDO');">
									<bean:message key="prompt.HistoricoLayoutImp"/>
                               </td>
							</tr>
					     </table>
					  </td>
					  <td width="4"><img src="/webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
				   </tr>
				</table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td valign="top" class="principalBgrQuadro" align="center"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="490">
                    <tr> 
                      <td valign="top"> 
                        <div id="layout" style="width:99%; height:395px;display: block">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                            <tr> 
	                              <td width="20%" class="EspacoPequeno">&nbsp;</td>
	                              <td width="60%" class="EspacoPequeno">&nbsp;</td>
	                              <td width="20%" class="EspacoPequeno">&nbsp;</td>
	                            </tr>
	                            <tr> 
	                              	<td width="20%" class="principalLabel" align="right"><bean:message key="prompt.layout"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	                              	<td width="60%">
		                              	<html:select property="id_laou_cd_sequencial" name="layoutImportacaoForm" styleClass="principalObjForm" onchange="carregaRegistros();">
											<html:option value=""><bean:message key="prompt.selecione_uma_opcao"/></html:option>
											<logic:present name="csCdtbLayoutLaouVector">
												<html:options collection="csCdtbLayoutLaouVector" property="idLaouCdSequencial" labelProperty="laouDsLayout"/>
											</logic:present>
										</html:select>
									</td>
									<td width="20%">&nbsp;</td>
	                            </tr>
	                        </table>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr> 
	                              <td width="2%" class="EspacoPequeno">&nbsp;</td>
	                              <td width="96%" class="EspacoPequeno">&nbsp;</td>
	                              <td width="2%" class="EspacoPequeno">&nbsp;</td>
	                            </tr>
								<tr>
									<td width="2%">&nbsp;</td>
	                            	<td width="96%">
	                            		<iframe id="ifrmCamposLayout" name="ifrmCamposLayout" src="CarregarCamposLayout.do" width="99%" height="395px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	                            	</td>
	                            	<td width="2%">&nbsp;</td>
	                            </tr>
                      		</table>
                       		<table width="100%" border="0" cellspacing="0" cellpadding="0">
                       			<tr> 
                              		<td class="espacoPqn">&nbsp;</td>
                            	</tr>
	                        </table>
	                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="8%" class="principalLabel" align="right"><bean:message key="prompt.arquivo"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	                            	<td width="48%">
	                            		<html:file property="caminhoArquivo" styleClass="principalObjForm" />
	                            	</td>
	                            	<td width="18%" class="principalLabel" align="right"><bean:message key="prompt.arquivoComCabecalho"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	                            	<td width="6%">
	                            		<html:checkbox property="possuiCabecalho" value="S" />
	                            	</td>
	                            	<td width="80px" class="principalLabel" align="right"><bean:message key="prompt.delimitado"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	                            	<td width="2%">
	                            		<html:text property="delimitador" styleClass="principalObjForm" maxlength="1"></html:text>
	                            	</td>
	                            	<td width="3%">&nbsp;</td>
	                            	<td width="4%"><img src="webFiles/images/icones/agendamentos01.gif" name="imgValidaLayout" id="imgValidaLayout" alt="<bean:message key="prompt.ValidarLayout" />" class="geralCursoHand" onclick="validarLayout()"></td>
	                            	<td width="2%">&nbsp;</td>
	                            </tr>
                      		</table> 
                        </div>
                        <div id="historico" style="width:100%; height:485px;display: none">
                        	<iframe id="ifrmHistoricoLayout" name="ifrmHistoricoLayout" src="CarregarHistoricoLayout.do" width="100%" height="485px" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0"></iframe> 
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
              </tr>
              <tr> 
                <td width="100%" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%"	height="4"></td>
                <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="100%" height="4"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
        </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
			<td width="25" align="left">
				<img src="webFiles/images/botoes/Acao_Programa.gif" alt="Copiar Layout" name="imgDuplicar" id="imgDuplicar" width="20" height="20" class="geralCursoHand" onclick="duplicar();">
			</td>
			<td width="25" align="left">
				<img src="webFiles/images/icones/impressora.gif" alt="Imprimir Layout" name="imgImprimir" id="imgImprimir" width="20" height="20" class="geralCursoHand" onclick="imprimir();">
			</td>
			<td width="25" align="center">
				<img src="webFiles/images/botoes/new.gif" alt="<bean:message key="prompt.novo"/>" name="imgNovo" id="imgNovo" class="geralCursoHand" onclick="ifrmCamposLayout.novo();">
			</td>
			<td width="25" align="center">
				<img src="webFiles/images/botoes/gravar.gif" alt="<bean:message key="prompt.gravar"/>" name="imgGravar" id="imgGravar" width="20" height="20" class="geralCursoHand" onclick="ifrmCamposLayout.salvar();">
			</td>
			<td width="25" align="center">
				<img src="webFiles/images/botoes/cancelar.gif" alt="<bean:message key="prompt.cancelar"/>" name="imgCancelar" id="imgCancelar" width="20" height="20" class="geralCursoHand" onclick="cancelar();">
			</td>
		</tr>
      </table>
    </td>
    <td width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
  </tr>
  <tr> 
    <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<div id="aguarde" style="left:300px; top:200px; width:199px; height:148px;display: none"> 
  <div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

<div id="divPrint" style="left:0px; top:0px; width:0px; height:0px;display: none"> 
</div>

</html:form>
</body>

<script>
	var temPermissaoIncluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LAYOUT_IMP_COBRANCA_INCLUSAO_CHAVE%>');
	var temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LAYOUT_IMP_COBRANCA_ALTERACAO_CHAVE%>');
	
	//Verifica permiss�o de alterare de gravar
	//if (!temPermissaoAlterar || !temPermissaoIncluir){
	if (!temPermissaoIncluir){
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LAYOUT_IMP_COBRANCA_ALTERACAO_CHAVE%>', document.layoutImportacaoForm.imgNovo);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LAYOUT_IMP_COBRANCA_ALTERACAO_CHAVE %>', document.layoutImportacaoForm.imgGravar);	
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LAYOUT_IMP_COBRANCA_ALTERACAO_CHAVE %>', document.layoutImportacaoForm.imgValidaLayout);
		desabilitaCamposLayout();
	}
	
	<logic:present name="csCdtbLayoutLaouVector">
		<logic:iterate name="csCdtbLayoutLaouVector" id="layoutVector">
			layoutArray[countLayout] = new Array();
			layoutArray[countLayout][0] = '<bean:write name="layoutVector" property="idLaouCdSequencial" />';
			layoutArray[countLayout][1] = '<bean:write name="layoutVector" property="laouInDelimitado" />';
			countLayout++;
		</logic:iterate>
	</logic:present>	
</script>
</html>