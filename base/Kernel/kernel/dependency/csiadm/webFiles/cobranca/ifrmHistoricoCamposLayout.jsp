<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/cobranca/js/pt/TratarDados.js"></script>
<title><bean:message key="prompt.CamposLayout" /></title>
<script language="JavaScript">
	function rollbackLayout(idLayout, idIdentificador, idSequencia) {
		if(confirm('Deseja efetuar o rollback deste campo?')) {
			var wi = window.dialogArguments;
			wi.parent.document.forms[0].id_laou_cd_sequencial.value = idLayout;
			wi.parent.document.forms[0].hlco_nr_identificador.value = idIdentificador;
			wi.parent.document.forms[0].lico_nr_sequencia_rollback.value = idSequencia;
			document.forms[0].target = this.name = 'rollback';
			wi.parent.document.forms[0].action = 'RollbackLayout.do';
			wi.parent.document.forms[0].submit();
			window.close();
		}
	}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="VisualizarHistoricoCamposLayout.do" styleId="layoutImportacaoForm"> 
	<div id="tituloLista" style="scroll: no; overflow: hidden; width: 100%; z-index: 1; position: relative">
		<!-- <table width="3385px" border="0" cellspacing="0" cellpadding="0"> -->
		<!-- <table width="3285px" border="0" cellspacing="0" cellpadding="0"> -->
		<table width="2885px" border="0" cellspacing="0" cellpadding="0">
			<tr> 
				<td class="principalLstCab" width="25px" align="center">&nbsp;</td>
	        	<td class="principalLstCab" width="145px">&nbsp;<bean:message key="prompt.NomeDoCampo" /></td>
	        	<td class="principalLstCab" width="150px"><bean:message key="prompt.DescricaoDoCampo" /></td>
	        	<!--  <td class="principalLstCab" width="100px"><bean:message key="prompt.Regex" /></td>-->
	        	<td class="principalLstCab" width="100px" align="center"><bean:message key="prompt.sequencia" /></td>
	        	<td class="principalLstCab" width="100px" align="center"><bean:message key="prompt.PosicaoInicial" /></td>
	        	<td class="principalLstCab" width="100px" align="center"><bean:message key="prompt.PosicaoFinal" /></td>
	        	<td class="principalLstCab" width="200px"><bean:message key="prompt.NomeTabelaTemporaria" /></td>
	        	<td class="principalLstCab" width="215px"><bean:message key="prompt.NomeCampoTabelaTemporaria" /></td>
	        	<!-- <td class="principalLstCab" width="200px"><bean:message key="prompt.NomeTabelaDestino" /></td>
	        	<td class="principalLstCab" width="200px"><bean:message key="prompt.NomeCampoTabelaDestino" /></td> -->
	        	<td class="principalLstCab" width="150px" align="center"><bean:message key="prompt.PossuiIdentificador" /></td>
	        	<td class="principalLstCab" width="100px" align="center"><bean:message key="prompt.Identificador" /></td>
	        	<td class="principalLstCab" width="150px"><bean:message key="prompt.SequenciaPai" /></td>
	        	<td class="principalLstCab" width="200px"><bean:message key="prompt.DePara" /></td>
	        	<td class="principalLstCab" width="100px"><bean:message key="prompt.HardCode" /></td>
	        	<td class="principalLstCab" width="100px" align="center"><bean:message key="prompt.NextCode" /></td>
	        	<td class="principalLstCab" width="150px"><bean:message key="prompt.SequenciaFK" /></td>
	        	<td class="principalLstCab" width="150px" align="center"><bean:message key="prompt.TipoCampo" /></td>
	        	<td class="principalLstCab" width="150px" align="center"><bean:message key="prompt.TamanhoCampo" /></td>
	        	<td class="principalLstCab" width="100px" align="center"><bean:message key="prompt.Obrigatorio" /></td>
	        	<td class="principalLstCab" width="150px"><bean:message key="prompt.FormatoCampo" /></td>
	        	<td class="principalLstCab" width="150px" align="center"><bean:message key="prompt.AgrupadorRegistro" /></td>
	        	<td class="principalLstCab" width="90px" align="center"><bean:message key="prompt.Pesquisa" /></td>
	        	<td class="principalLstCab" width="100px" align="center"><bean:message key="prompt.CasasDecimais" /></td>
	        	<td class="principalLstCab" width="10px" align="center">&nbsp;</td>
			</tr>
		</table> 
	</div> 
	<div id="lstRegistro" style="overflow: scroll; height: 350px; width: 100%; z-index: 2; position: absolute" onScroll="tituloLista.scrollLeft=this.scrollLeft;"> 
		<logic:present name="historicoVector">
			<logic:iterate name="historicoVector" id="historicoVector" indexId="indice">
				<!-- <table id="table<%=indice.intValue()%>" width="3385px" border="0" cellspacing="0" cellpadding="0"> -->
				<table id="table<%=indice.intValue()%>" width="2885px" border="0" cellspacing="0" cellpadding="0">
					<tr>					
						<td class="intercalaLst0" width="25px" align="center"><img src="webFiles/images/icones/historico.gif" width="20" height="20" class="geralCursoHand" alt="<bean:message key="prompt.Rollback"/>" onclick="rollbackLayout('<bean:write name="historicoVector" property="field(id_laou_cd_sequencial)" />', '<bean:write name="historicoVector" property="field(hlco_nr_identificador)" />', '<bean:write name="historicoVector" property="field(lico_nr_sequencia)" />')" /></td>
			        	<td class="intercalaLst0" width="145px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_ds_nome)" />', 22);</script>
			        	</td>
			        	<td class="intercalaLst0" width="150px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_ds_descricao)" />', 24);</script>
			        	</td>
			        	<!-- <td class="intercalaLst0" width="100px">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_ds_regex)" />
			        	</td> -->
			        	<td class="intercalaLst0" width="100px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_nr_posicao)" />
						</td>
			        	<td class="intercalaLst0" width="100px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_nr_ci)" />
			        	</td>
			        	<td class="intercalaLst0" width="100px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_nr_cf)" />
			        	</td>
			        	<td class="intercalaLst0" width="200px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_ds_tabeladesttemp)" />', 30);</script>
			        	</td>
			        	<td class="intercalaLst0" width="215px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_ds_campodesttemp)" />', 30);</script>
			        	</td>
			        	<!-- <td class="intercalaLst0" width="200px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_ds_tabeladest)" />', 30);</script>
			        	</td>
			        	<td class="intercalaLst0" width="200px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_ds_campodest)" />', 30);</script>
			        	</td> -->
			        	<td class="intercalaLst0" width="150px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_in_identificadorreg)" />
			        	</td>
			        	<td class="intercalaLst0" width="100px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_ds_identificadorreg)" />
			        	</td>
			        	<td class="intercalaLst0" width="150px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_nr_sequenciapai)" />', 22);</script>
			        	</td>
			        	<td class="intercalaLst0" width="200px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_ds_depara)" />', 30);</script>
			        	</td>
			        	<td class="intercalaLst0" width="100px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_ds_hardcoded)" />', 12);</script>
			        	</td>
			        	<td class="intercalaLst0" width="100px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_ds_nextcode)" />
			        	</td>
			        	<td class="intercalaLst0" width="150px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_nr_sequenciafk)" />', 22);</script>
			        	</td>
			        	<td class="intercalaLst0" width="150px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_ds_tipocampo)" />
			        	</td>
			        	<td class="intercalaLst0" width="150px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_nr_tamanho)" />
			        	</td>
			        	<td class="intercalaLst0" width="100px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_in_obrigatorio)" />
			        	</td>
			        	<td class="intercalaLst0" width="150px">
			        		&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(lico_ds_formato)" />', 22);</script>
			        	</td>
			        	<td class="intercalaLst0" width="150px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_ds_agrupadorregistro)" />
			        	</td>
			        	<td class="intercalaLst0" width="90px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_in_pesquisa)" />
			        	</td>
			        	<td colspan="2" class="intercalaLst0" width="110px" align="center">
			        		&nbsp;<bean:write name="historicoVector" property="field(lico_nr_digito)" />
			        	</td>
					</tr>
				</table>
			</logic:iterate>
		</logic:present>  
	</div> 
	<div id="divBtSair" style="height: 20px; width: 100%; top: 367px; z-index: 3; position: absolute">	
		<table border="0" cellspacing="0" cellpadding="4" align="right">
		  <tr> 
		    <td> 
		      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
		  </tr>
		</table>
	</div>
</html:form>
</body>
</html>