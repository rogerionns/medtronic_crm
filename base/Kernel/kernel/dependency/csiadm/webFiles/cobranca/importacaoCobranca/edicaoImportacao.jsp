<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>

<% 
String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../../../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<link rel="stylesheet" href="webFiles/cobranca/css/global.css" type="text/css">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<script language="JavaScript" src="../csicrm/webFiles/funcoes/date-picker.js"></script>
<script type="text/javascript" src="../csicrm/webFiles/javascripts/funcoesMozilla.js"></script>
<script type="text/javascript" src="../csicrm/webFiles/javascripts/TratarDados.js"></script>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/validadata.js"></SCRIPT>
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>    
<script language="JavaScript">
<!--
function validaHora(obj){
	if (obj.value.length > 0){
		return verificaHora(obj);
	}
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'CONTEUDO':
	MM_showHideLayers('conteudo','','show','adicional','','hide','origem','','hide','sql','','hide')
	SetClassFolder('tdConteudo','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdAdicional','principalPstQuadroLinkNormal');
	SetClassFolder('tdOrigem','principalPstQuadroLinkNormal');
	SetClassFolder('tdSql','principalPstQuadroLinkNormal');
	break;
	
case 'ADICIONAL':
	MM_showHideLayers('conteudo','','hide','adicional','','show','origem','','hide','sql','','hide')
	SetClassFolder('tdConteudo','principalPstQuadroLinkNormal');
	SetClassFolder('tdAdicional','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdOrigem','principalPstQuadroLinkNormal');
	SetClassFolder('tdSql','principalPstQuadroLinkNormal');
	break;

case 'ORIGEM':
	MM_showHideLayers('conteudo','','hide','adicional','','hide','origem','','show','sql','','hide')
	SetClassFolder('tdConteudo','principalPstQuadroLinkNormal');
	SetClassFolder('tdAdicional','principalPstQuadroLinkNormal');
	SetClassFolder('tdOrigem','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdSql','principalPstQuadroLinkNormal');
	break;

case 'SQL':
	MM_showHideLayers('conteudo','','hide','adicional','','hide','origem','','hide','sql','','show')
	SetClassFolder('tdConteudo','principalPstQuadroLinkNormal');
	SetClassFolder('tdAdicional','principalPstQuadroLinkNormal');
	SetClassFolder('tdOrigem','principalPstQuadroLinkNormal');
	SetClassFolder('tdSql','principalPstQuadroLinkSelecionado');
	break;
	
}
 eval(stracao);
}

function AtivarPastaImp(pasta)
{
switch (pasta)
{
case 'ARQUIVO':
	MM_showHideLayers2('arquivo','','show','plusinfo','','hide')
	break;

case 'PLUSINFO':
	MM_showHideLayers2('arquivo','','hide','plusinfo','','show')
	break;
	
}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}

function MM_showHideLayers2() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers2.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}

function alterarTipoCampanha(){
	document.forms[0].action = 'AbrirEdicaoAgendaCampanha.do';
	document.forms[0].submit();	
}

function inicio(){
	<logic:present name="gravacaoOK">
		parent.document.forms[0].submit();
	</logic:present>	
	
	showError('<%=request.getAttribute("msgerro")%>');

	changeTipo();

	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(document.forms[0].id_imco_cd_importacaocobr.value);
	
	carregaCmbFuncionario();

	//desabilita combo de Resulta
	//document.forms[0].id_resu_cd_resultado.disabled = true;
}

function desabilitaCamposArea(){
	document.forms[0].id_imco_cd_importacaocobr.disabled = true;
	document.forms[0].imco_in_tipo[0].disabled = true;
	document.forms[0].imco_in_tipo[1].disabled = true;
	document.forms[0].imco_ds_importacaocobranca.disabled = true;
	document.forms[0].imco_nr_sequencia.disabled = true;
	document.forms[0].id_orig_cd_origem.disabled = true;
	document.forms[0].imco_ds_diretorioerro.disabled = true;
	document.forms[0].imco_ds_emailparanotificacao.disabled = true;
	document.forms[0].imco_ds_emailccnotificacao.disabled = true;
	document.forms[0].inativo.disabled = true;
	document.forms[0].baixa.disabled = true;
	document.forms[0].id_laou_cd_sequencial.disabled = true;
	document.forms[0].imco_ds_diretorioimportacao.disabled = true;
	document.forms[0].imco_ds_diretoriobackup.disabled = true;
	document.forms[0].imco_ds_separador.disabled = true;
	document.forms[0].imco_in_cabecalho.disabled = true;
	document.forms[0].imco_in_duplicado.disabled = true;
	document.forms[0].imco_dh_execucaode.disabled = true;
	document.forms[0].imco_dh_execucaoate.disabled = true;
	document.forms[0].imco_ds_horaexecucao.disabled = true;
	document.forms[0].imco_in_diario.disabled = true;
	document.forms[0].imco_in_segunda.disabled = true;
	document.forms[0].imco_in_terca.disabled = true;
	document.forms[0].imco_in_quarta.disabled = true;
	document.forms[0].imco_in_quinta.disabled = true;
	document.forms[0].imco_in_sexta.disabled = true;
	document.forms[0].imco_in_sabado.disabled = true;
	document.forms[0].imco_in_domingo.disabled = true;
	document.forms[0].imco_dh_atualizacao.disabled = true;
	document.forms[0].imco_dh_cadastro.disabled = true;
	document.forms[0].id_area_cd_area.disabled = true;
	document.forms[0].id_resu_cd_resultado.disabled = true;
	ifrmCmbFuncionarioResultado.document.forms[0].id_func_cd_resultado.disabled = true;
	document.forms[0].imco_ds_origemdados.disabled = true;
	document.forms[0].imco_ds_usuario.disabled = true; 
	document.forms[0].imco_ds_senha.disabled = true; 
	document.forms[0].imco_ds_servidor.disabled = true;
	document.forms[0].imco_ds_sqlinicio.disabled = true; 
	document.forms[0].imco_ds_sqlregistro.disabled = true; 
	document.forms[0].imco_ds_sqlfim.disabled = true;

	
	
	
}

function executarImportacao(){
	if(document.forms[0].id_imco_cd_importacaocobr.value != '') {
		document.forms[0].mensagem.value = '';
		ifrmAutoRefresh.location.href = 'ExecutarImportacao.do?id_laou_cd_sequencial=' + document.forms[0].id_laou_cd_sequencial.value + '&imco_ds_diretorioimportacao=' + document.forms[0].imco_ds_diretorioimportacao.value + '&id_imco_cd_importacaocobr=' + document.forms[0].id_imco_cd_importacaocobr.value;
		window.open('/csiadm/cobranca/importacao/status.do?idImcoCdImportacaocobr=' + document.forms[0].id_imco_cd_importacaocobr.value, 'statusImportacao', 'width=600,height=300,left=80,top=80,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1', true);
	}else{
		alert('� necess�rio gravar o registro primeiro!');
	}
}

function changeTipo(){
	if(document.forms[0].imco_in_tipo[1].checked){
		document.getElementById('tipoExportacao').style.display = '';
		document.getElementById('tipoImportacao').style.display = 'none';
	}else{
		document.getElementById('tipoExportacao').style.display = 'none';
		document.getElementById('tipoImportacao').style.display = '';
	}	
}

function carregaCmbFuncionario() {
	if(document.forms[0].id_area_cd_area.value != '') {
		ifrmCmbFuncionarioResultado.location.href = 'CarregaCmbFuncionarioResultado.do?id_area_cd_area=' + document.forms[0].id_area_cd_area.value + '&id_func_cd_resultado=' + document.forms[0].id_func_cd_resultado.value;
	}
}

function habilitaDesabilitaCampos(valor) {
	document.forms[0].imco_in_tipo[0].disabled = valor;
	document.forms[0].imco_in_tipo[1].disabled = valor;
	document.forms[0].imco_ds_importacaocobranca.disabled = valor;
	document.forms[0].imco_nr_sequencia.disabled = valor;
	document.forms[0].id_orig_cd_origem.disabled = valor;
	document.forms[0].id_laou_cd_sequencial.disabled = valor;
	document.forms[0].imco_ds_diretorioerro.disabled = valor;
	document.forms[0].imco_ds_diretorioimportacao.disabled = valor;
	document.forms[0].imco_ds_diretoriobackup.disabled = valor;
	document.forms[0].imco_ds_separador.disabled = valor;
	document.forms[0].imco_in_cabecalho.disabled = valor;
	document.forms[0].imco_in_duplicado.disabled = valor;
	document.forms[0].id_visa_cd_visao.disabled = valor;
	document.forms[0].imco_ds_expheader.disabled = valor;
	document.forms[0].imco_ds_expfooter.disabled = valor;
	document.forms[0].imco_ds_expnomearquivo.disabled = valor;
	document.forms[0].imco_ds_titulo.disabled = valor;
	document.forms[0].imco_ds_emailparanotificacao.disabled = valor;
	document.forms[0].imco_ds_emailccnotificacao.disabled = valor;
	document.forms[0].id_resu_cd_resultado.disabled = valor;
	document.forms[0].id_area_cd_area.disabled = valor;
	document.forms[0].baixa.disabled = valor;
	document.forms[0].inativo.disabled = valor;
	document.forms[0].imco_dh_execucaode.disabled = valor;
	document.forms[0].imco_dh_execucaoate.disabled = valor;
	document.forms[0].imco_ds_horaexecucao.disabled = valor;
	document.forms[0].imco_in_diario.disabled = valor;
	document.forms[0].imco_in_segunda.disabled = valor;
	document.forms[0].imco_in_terca.disabled = valor;
	document.forms[0].imco_in_quarta.disabled = valor;
	document.forms[0].imco_in_quinta.disabled = valor;
	document.forms[0].imco_in_sexta.disabled = valor;
	document.forms[0].imco_in_sabado.disabled = valor;
	document.forms[0].imco_in_domingo.disabled = valor;
	desabilitaCmbFuncionarioResultado();
	setPermissaoImageDisable(false, document.forms[0].imgExecutarImportacao);
	document.forms[0].imco_ds_origemdados.disabled = valor;
	document.forms[0].imco_ds_usuario.disabled = valor; 
	document.forms[0].imco_ds_senha.disabled = valor; 
	document.forms[0].imco_ds_servidor.disabled = valor;
	document.forms[0].imco_ds_sqlinicio.disabled = valor; 
	document.forms[0].imco_ds_sqlregistro.disabled = valor; 
	document.forms[0].imco_ds_sqlfim.disabled = valor;
	
	 
}

function desabilitaCmbFuncionarioResultado() {
	try {
		ifrmCmbFuncionarioResultado.document.forms[0].id_func_cd_resultado.disabled = true;
	} catch(e) {
		setTimeout('desabilitaCmbFuncionarioResultado()', 300);
	}	
}

function habilitaCombo(){
	if(document.forms[0].baixa.checked==true){
		document.forms[0].id_resu_cd_resultado.disabled = false;
	}else if(document.forms[0].baixa.checked==false){
		document.forms[0].id_resu_cd_resultado.value = '';
		document.forms[0].id_resu_cd_resultado.disabled = true;
	}	
}

function validaPeriodoData() {
	var dataDe = document.forms[0].elements["imco_dh_execucaode"].value;
	var dataAte = document.forms[0].elements["imco_dh_execucaoate"].value;
	
	if(validaDataMaiorQueOutra(dataDe, dataAte) < 0) {
		alert('O campo execu��o a partir at� deve ser maior ou igual ao campo execu��o a partir de!');
		document.forms[0].elements["imco_dh_execucaoate"].focus();
		return false;
	}
}

function validaFTP(){

	var Url = "";
	var strTipo = "";
	
	if(document.forms[0].imco_ds_origemdados.value!='' && document.forms[0].imco_ds_origemdados.value == 'F'){

		if(document.forms[0].imco_in_tipo[0].checked == true){
			strTipo = "I";
		}else if(document.forms[0].imco_in_tipo[1].checked == true){
			strTipo = "E";
		}
		
		if(document.forms[0].imco_ds_usuario.value==null && document.forms[0].imco_ds_usuario.value==''){
			alert("� obrigat�rio o usu�rio.");
			return false;
		}

		if(document.forms[0].imco_ds_senha.value==null && document.forms[0].imco_ds_senha.value==''){
			alert("� obrigat�rio a senha.");
			return false;
		}

		if(document.forms[0].imco_ds_servidor.value==null && document.forms[0].imco_ds_servidor.value==''){
			alert("� obrigat�rio o endere�o do servidor.");
			return false;
		}
		
		Url = Url + "connectFTP.do?";
		Url = Url + "dsUser=" + document.forms[0].imco_ds_usuario.value;
		Url = Url + "&dsPassword=" + document.forms[0].imco_ds_senha.value;
		Url = Url + "&dsServer=" + document.forms[0].imco_ds_servidor.value;
		Url = Url + "&dsDestino=" + document.forms[0].imco_ds_diretorioimportacao.value;
		Url = Url + "&dsDirFTP=" + document.forms[0].imco_ds_diretorioftp.value;
		Url = Url + "&dsTipo=" + strTipo;

		ifrmValidaFTP.location.href = Url
			
	}else{
		alert('A conex�o s� � feita quando a Origem � FTP');
		return false;
	}
	
	

	  
}

</script>
</head>
<html:form action="SalvarImportacaoCobranca">
<html:hidden property="imco_dh_inativo"/>
<html:hidden property="imco_in_baixa"/>
<html:hidden property="id_func_cd_resultado"/>
<html:hidden property="userAction"/>
<html:hidden property="mensagem"/>

<body class="principalBgrPageIFRM" text="#000000" onload="inicio()">
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td>&nbsp; </td>
	     </tr>
	   </table>
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">C&oacute;digo 
	         <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="I">
	           <tr> 
	             <td width="22%"> 
	               <html:text property="id_imco_cd_importacaocobr" styleClass="principalObjForm" readonly="true"></html:text>
	             </td>
	             <td width="78%">
	               <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                 <tr> 
	                   <td class="principalLabel" align="center" width="5%"> 
	                     <html:radio property="imco_in_tipo" value="I" onclick="changeTipo();"></html:radio>
	                   </td>
	                   <td class="principalLabel" width="17%"><bean:message key="prompt.importacao"/></td>
	                   <td class="principalLabel" align="center" width="5%"> 
	                     <html:radio property="imco_in_tipo" value="E" onclick="changeTipo();"></html:radio>
	                   </td>
	                   <td class="principalLabel" width="73%"><bean:message key="prompt.exportacao"/></td>
	                 </tr>
	               </table>
	             </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>	   
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.descricao"/> 
	         <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
					<html:text property="imco_ds_importacaocobranca" styleClass="principalObjForm" maxlength="255"></html:text>	               
	             </td>
	             <td width="26%">&nbsp; </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>	   
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.sequencia"/> 
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
           </td>
           <td class="principalLabel" width="78%">
           	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
					<html:text property="imco_nr_sequencia" styleClass="principalObjForm" maxlength="10" onfocus="SetarEvento(this, 'N')"></html:text>	               
	             </td>
	             <td width="26%">&nbsp; </td>
	           </tr>
	         </table>
           </td>
         </tr>
       </table>	   
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.origem"/> 
	         <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
	               	<html:select property="id_orig_cd_origem" styleClass="principalObjForm">
						<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/></html:option>
						<logic:present name="origVector" >
							<html:options collection="origVector" property="idOrigCdOrigem" labelProperty="origDsOrigem" />
						</logic:present>
					</html:select>	
	             </td>
	             <td width="26%">&nbsp;</td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>	 
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.layout"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
            </td>
            <td class="principalLabel" width="78%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="74%"> 
                    <html:select property="id_laou_cd_sequencial" styleClass="principalObjForm">
						<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/></html:option>
						<logic:present name="laouVector" >
							<html:options collection="laouVector" property="idLaouCdSequencial" labelProperty="laouDsLayout" />
						</logic:present>
					</html:select>	
					<input type="hidden" name="laouInDelimitado" value="N">
					<logic:present name="laouVector" >
						<logic:iterate name="laouVector" id="laou">
							<input type="hidden" name="laouInDelimitado" value="<bean:write name="laou" property="laouInDelimitado"/>">
						</logic:iterate>
					</logic:present>																		
                  </td>
                  <td width="26%">&nbsp; </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>		
	   									        
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.separadorCampos"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
            </td>
            <td class="principalLabel" width="78%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="8%" class="principalLabel"> 
                    <html:text property="imco_ds_separador" styleClass="principalObjForm" maxlength="1"></html:text>
                  </td>
                  <td width="92%" class="principalLabel"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalLabel" align="center" width="5%"> 
                          <html:checkbox property="imco_in_cabecalho" value="S"></html:checkbox>
                        </td>
                        <td class="principalLabel" width="29%"><bean:message key="prompt.arquivoComCabecalho"/></td>
                        <td class="principalLabel" align="center" width="3%"> 
                          <html:checkbox property="imco_in_duplicado" value="S"></html:checkbox>
                        </td>
                        <td class="principalLabel" width="63%"><bean:message key="prompt.atualizarCadastro"/></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>        
	   <div id="tipoExportacao" style="display: none">
		   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr> 
	            <td class="principalLabel" width="22%" align="right">
					<bean:message key="prompt.visaoPlusinfo"/>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	            </td>
	            <td class="principalLabel" width="78%"> 
	              <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <tr> 
	                  <td width="74%"> 
	                    <html:select property="id_visa_cd_visao" styleClass="principalObjForm">
							<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/></html:option>
							<logic:present name="visaVector" >
								<html:options collection="visaVector" property="field(id_visa_cd_visao)" labelProperty="field(visa_ds_visao)" />
							</logic:present>
						</html:select>	
	                  </td>
	                  <td width="26%">&nbsp;</td>
	                </tr>
	              </table>
	            </td>
	          </tr>
	        </table>	
		   <table width="100%" border="0" cellspacing="0" cellpadding="0">
		     <tr> 
		       <td class="principalLabel" width="22%" align="right">
					<bean:message key="prompt.header"/>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
		       </td>
		       <td class="principalLabel" width="78%"> 
		         <table width="100%" border="0" cellspacing="0" cellpadding="0">
		           <tr> 
		             <td width="74%"> 
		               <html:text property="imco_ds_expheader" styleClass="principalObjForm" style="width: 435px;"></html:text>
		             </td>
		             <td width="26%">
		               &nbsp;
		             </td>
		           </tr>
		         </table>
		       </td>
		     </tr>
		   </table>	
		   <table width="100%" border="0" cellspacing="0" cellpadding="0">
		     <tr> 
		       <td class="principalLabel" width="22%" align="right">
					<bean:message key="prompt.footer"/>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
		       </td>
		       <td class="principalLabel" width="78%"> 
		         <table width="100%" border="0" cellspacing="0" cellpadding="0">
		           <tr> 
		             <td width="74%"> 
		               <html:text property="imco_ds_expfooter" styleClass="principalObjForm" style="width: 435px;"></html:text>
		             </td>
		             <td width="26%">
		               &nbsp;
		             </td>
		           </tr>
		         </table>
		       </td>
		     </tr>
		   </table>	
		   <table width="100%" border="0" cellspacing="0" cellpadding="0">
		     <tr> 
		       <td class="principalLabel" width="22%" align="right">
					<bean:message key="prompt.nomeArquivo"/>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
		       </td>
		       <td class="principalLabel" width="78%"> 
		         <table width="100%" border="0" cellspacing="0" cellpadding="0">
		           <tr> 
		             <td width="74%"> 
		               <html:text property="imco_ds_expnomearquivo" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
		             </td>
		             <td width="26%">
		               &nbsp;
		             </td>
		           </tr>
		         </table>
		       </td>
		     </tr>
		   </table>	
	   </div>
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.assuntoEmailNotificacao"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
	               <html:text property="imco_ds_titulo" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
	             </td>
	             <td width="26%">
	               &nbsp;
	             </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.emailNotificacao"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
	               <html:text property="imco_ds_emailparanotificacao" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
	             </td>
	             <td width="26%">
	               &nbsp;
	             </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>	
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.emailCcNotificacao"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
	               <html:text property="imco_ds_emailccnotificacao" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
	             </td>
	             <td width="26%">
	               &nbsp;
	             </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>
	   <div id="tipoImportacao" style="display: none">
	   	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.resultado"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
	               	  <html:select property="id_resu_cd_resultado" styleClass="principalObjForm">
						<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
					  	<logic:present name="resultadoVector">
					  		<html:options collection="resultadoVector" property="idResuCdResultado" labelProperty="resuDsResultado" />  
					  	</logic:present>
					  </html:select>
	             </td>
	             <td width="26%">
	               &nbsp;
	             </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.area"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
	             	  <html:select property="id_area_cd_area" styleClass="principalObjForm" onchange="carregaCmbFuncionario()">
						<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
					  	<logic:present name="areaVector">
					  		<html:options collection="areaVector" property="idAreaCdArea" labelProperty="areaDsArea" />  
					  	</logic:present>
					  </html:select>
	             </td>
	             <td width="26%">
	               &nbsp;
	             </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.FuncionarioResultado"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	       	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="100%"> 
	             	  <iframe id="ifrmCmbFuncionarioResultado" name="ifrmCmbFuncionarioResultado" src="CarregaCmbFuncionarioResultado.do" width="100%" height="25px" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	             </td>
	             
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>
	   </div>
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">
				Implementa��o
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	       	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
	             	<html:select property="id_icim_cd_imcoimpl" styleClass="principalObjForm">
					  	<logic:present name="icimVector">
					  		<html:options collection="icimVector" property="field(id_icim_cd_imcoimpl)" labelProperty="field(icim_ds_imcoimpl)" />  
					  	</logic:present>
					  </html:select>
	             </td>
	             <td width="26%">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                 <tr> 
		                   <td class="principalLabel" align="right" width="20%"> 
		                     	<html:checkbox property="baixa" value="true" onclick="habilitaCombo();"/>							
		                   </td>
		                   <td class="principalLabel" width="80%"><bean:message key="prompt.ArquivoDeBaixa"/></td>
		                 </tr>
		                 <tr> 
		                 <td class="principalLabel" align="right" > 
		                   	<html:checkbox property="inativo" value="true"/>							
		                 </td>
		                 <td class="principalLabel" ><bean:message key="prompt.inativo"/></td>
	                 	 </tr>
	               </table>
	             </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>
        
	   <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100">
	     <tr>
	       <td valign="top">
				<!-- INICIO ABAS -->
					<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
					  <tr> 
					    <td class="principalPstQuadroLinkSelecionado" name="tdConteudo" id="tdConteudo" onclick="AtivarPasta('CONTEUDO')"><bean:message key="prompt.periodicidade"/></td>
					    <td class="principalPstQuadroLinkNormal" name="tdOrigem" id="tdOrigem" onclick="AtivarPasta('ORIGEM')"><bean:message key="prompt.origem"/></td>
					    <td class="principalPstQuadroLinkNormal" name="tdSql" id="tdSql" onclick="AtivarPasta('SQL')">SQL</td>
					    <td class="principalPstQuadroLinkNormal" name="tdAdicional" id="tdAdicional" onclick="AtivarPasta('ADICIONAL')"><bean:message key="prompt.resultado"/></td>
					    <td >&nbsp;</td>
					    <td class="principalLabel">&nbsp;</td>
					  </tr>
					</table>
					<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro" height="190">
					  <tr>
					    <td valign="top">					      
					      <div id="conteudo" style="width:100%;display: block;"> 
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td>&nbsp;</td>
					          </tr>
					        </table>
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.execucaoAPartirDe"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					            </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="27%"> 
					                    <table width="92%" border="0" cellspacing="0" cellpadding="0">
					                      <tr> 
					                        <td width="89%"> 
					                          <html:text property="imco_dh_execucaode" styleClass="principalObjForm" maxlength="10" onkeydown="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''"></html:text>
					                        </td>
					                        <td width="11%" align="center"> <img src="webFiles/images/botoes/calendar.gif" title="Calend�rio" width="16" height="15" class="geralCursoHand" onclick="show_calendar('forms[0].imco_dh_execucaode')"></td>
					                      </tr>
					                    </table>
					                  </td>
					                  <td width="73%"> 
					                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                      <tr> 
					                        <td width="15%" class="principalLabel" align="right"><bean:message key="prompt.ate"/> 
					                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td class="principalLabel" width="27%"> 
					                          <table width="92%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="89%"> 
					                                <html:text property="imco_dh_execucaoate" styleClass="principalObjForm" maxlength="10" onkeydown="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):'';validaPeriodoData();"></html:text>
					                              </td>
					                              <td width="11%" align="center"> <img src="webFiles/images/botoes/calendar.gif" title="Calend�rio" width="16" height="15" class="geralCursoHand" onclick="show_calendar('forms[0].imco_dh_execucaoate')"></td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="58%">&nbsp; </td>
					                      </tr>
					                    </table>
					                  </td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>					        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.horario"/> 
					              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					            </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="22%" class="principalLabel"> 
					                    <html:text property="imco_ds_horaexecucao" styleClass="principalObjForm" maxlength="5" onkeydown="return validaDigitoHora(this, event)" onblur="validaHora(this)"></html:text>
					                  </td>
					                  <td width="78%" class="principalLabel">&nbsp;</td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>					        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.umaVezAoDia"/> 
					              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="77%" class="principalLabel"> 
					                    <html:checkbox property="imco_in_diario" value="S"></html:checkbox>
					                  </td>
					                  <td width="23%" class="principalLabel">&nbsp;</td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>					        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.diaSemana"/> 
					              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					            </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="77%" class="principalLabel"> 
					                    <html:checkbox property="imco_in_segunda" value="S"></html:checkbox>
					                    <bean:message key="prompt.segunda"/>
					                    <html:checkbox property="imco_in_terca" value="S"></html:checkbox>
					                    <bean:message key="prompt.terca"/>
					                    <html:checkbox property="imco_in_quarta" value="S"></html:checkbox>
					                    <bean:message key="prompt.quarta"/> 
					                    <html:checkbox property="imco_in_quinta" value="S"></html:checkbox>
					                    <bean:message key="prompt.quinta"/>
					                    <html:checkbox property="imco_in_sexta" value="S"></html:checkbox>
					                    <bean:message key="prompt.sexta"/>
					                    <html:checkbox property="imco_in_sabado" value="S"></html:checkbox>
					                    <bean:message key="prompt.sabado"/>
					                    <html:checkbox property="imco_in_domingo" value="S"></html:checkbox>
					                    <bean:message key="prompt.domingo"/></td>
					                  <td width="23%" class="principalLabel">&nbsp;</td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right">
					            	&nbsp;					              
					            </td>
					            <td class="principalLabel" width="78%" align="right"> 
					              <img src="webFiles/images/botoes/Acao_Programa.gif" name="imgExecutarImportacao" id="imgExecutarImportacao" width="34" height="34" title="Executar Agora" class="geralCursoHand" onclick="executarImportacao();">
					            </td>
					          </tr>
					        </table>
					        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
					          <tr> 
					            <td>&nbsp;</td>
					          </tr>
					        </table>
					      </div>
					      <div id="origem" style="width:100%;display: none"> 
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td>&nbsp;</td>
					          </tr>
					        </table>
					        				        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.origem"/> 
					              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
					            </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="40%" class="principalLabel"> 
										<html:select property="imco_ds_origemdados" styleClass="principalObjForm">
											<html:option value=""><bean:message key="prompt.selecione_uma_opcao"/></html:option>
											<html:option value="F">FTP</html:option>
											<html:option value="L">LOCAL</html:option>
											<html:option value="S">SERVIDOR</html:option>
										</html:select>
					                  </td>
					                  <td width="60%" class="principalLabel">&nbsp; <img src="webFiles/images/botoes/GloboAuOn.gif" name="btConnectFTP" id="btConnectFTP" width="34" height="34" title="Download" class="geralCursoHand" onclick="validaFTP();"></td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
						     <tr> 
						       <td class="principalLabel" width="22%" align="right">
									<bean:message key="prompt.diretorioErro"/>
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
						       </td>
						       <td class="principalLabel" width="78%"> 
						         <table width="100%" border="0" cellspacing="0" cellpadding="0">
						           <tr> 
						             <td width="74%"> 
						               <html:text property="imco_ds_diretorioerro" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
						             </td>
						             <td width="26%">&nbsp; </td>
						           </tr>
						         </table>
						       </td>
						     </tr>
						   </table>	
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right">
									<bean:message key="prompt.diretorioArquivo"/>
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
					            </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="74%"> 
					                    <html:text property="imco_ds_diretorioimportacao" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
					                  </td>
					                  <td width="26%">&nbsp; </td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>										        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right">
									<bean:message key="prompt.diretorioBackup"/>
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
					            </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="74%"> 
					                    <html:text property="imco_ds_diretoriobackup" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
					                  </td>
					                  <td width="26%">&nbsp; </td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right">
									<bean:message key="prompt.diretorioFTP"/>
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
					            </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="74%"> 
					                    <html:text property="imco_ds_diretorioftp" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
					                  </td>
					                  <td width="26%">&nbsp; </td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>		
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="100%" align="right" colspan="7">
					            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
					            		<td width="13%"></td>
					            		<td class="principalLabel" width="7%" align="right"><bean:message key="prompt.usuario"/>
					            			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
					            		</td>
					                    <td width="13%"> 
					                          <html:text property="imco_ds_usuario" styleClass="principalObjForm" maxlength="20"></html:text>
					                    </td>
					                    <td width="7%" class="principalLabel" align="right"><bean:message key="prompt.senha"/> 
					                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
					                    </td>
		                                <td width="13%"> 
		                              	      <html:password property="imco_ds_senha" styleClass="principalObjForm" maxlength="20"></html:password>
					                    </td>
					                    <td width="9%" class="principalLabel" align="right"><bean:message key="prompt.servidor"/> 
					                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
					                    </td>
					                    <td width="15%"> 
					                    	  <html:text property="imco_ds_servidor" styleClass="principalObjForm" maxlength="20"></html:text>
					                    </td>
					                    <td width="16%"></td>
					                </table>
					            </td>
					          </tr>
					        </table>						        
					      </div>
					      
					      <div id="sql" style="width:100%;display: none"> 
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td>&nbsp;</td>
					          </tr>
					        </table>
					        				        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
					          <tr> 
					            <td class="principalLabel" width="22%" align="right">
					            	<bean:message key="prompt.sqlinicio"/>
					              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
					            </td>
					            <td class="principalLabel" width="100%"> 
									<html:textarea property="imco_ds_sqlinicio" styleClass="principalObjForm"></html:textarea>
					            </td>
					          </tr>
					        </table>
					        	
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right">
									<bean:message key="prompt.sqlRegistro"/>
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
					            </td>
					            <td class="principalLabel" width="100%"> 
					           		<html:textarea property="imco_ds_sqlregistro" styleClass="principalObjForm"></html:textarea>
					            </td>
					          </tr>
					        </table>		
					        								        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right">
									<bean:message key="prompt.sqlFim"/>
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
					            </td>
					            <td class="principalLabel" width="100%"> 
				                    <html:textarea property="imco_ds_sqlfim" styleClass="principalObjForm"></html:textarea>
					            </td>
					          </tr>
					        </table>						        
					      </div>
					      
					      <div id="adicional" style="width:100%;display: none"> 
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td>&nbsp;</td>
					          </tr>
					        </table>
					        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					          <tr> 
					            <td class="principalLstCab" width="50%">&nbsp;<bean:message key="prompt.nomeArquivo"/></td>
					            <td class="principalLstCab" align="center" width="30%"><bean:message key="prompt.dataProcessamento"/></td>
					            <td class="principalLstCab" align="center" width="20%"><bean:message key="prompt.erro"/></td>
					          </tr>
					        </table>
					        <div style="overflow:auto;height: 130px;overflow-y: scroll">
					        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					          <logic:present name="historicoExec">
								<logic:iterate id="hiac" name="historicoExec">
								  <tr onClick="showModalDialog('AbrirDetalhesImportacaoCob.do?id_imco_cd_importacaocobr=<bean:write name="hiac" property="field(id_imco_cd_importacaocobr)"/>&hiac_nr_sequencia=<bean:write name="hiac" property="field(himc_nr_sequencia)"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:690px;dialogHeight:465px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand"> 
						            <td class="principalLstPar" width="50%">&nbsp;<script>acronym('<bean:write name="hiac" property="field(himc_nm_arquivo)"/>',60)</script></td>
						            <td class="principalLstPar" width="35%" align="center"><bean:write name="hiac" property="field(himc_dh_processamento)" format="dd/MM/yyyy HH:mm:ss"/></td>
						            <td class="principalLstPar" width="15%" align="center">
										<logic:equal name="hiac" property="field(himc_in_houveerro)" value="S">
											<bean:message key="prompt.sim"/>
										</logic:equal>
										<logic:equal name="hiac" property="field(himc_in_houveerro)" value="N">
											<bean:message key="prompt.nao"/>
										</logic:equal>
									</td>
						          </tr>
						       </logic:iterate>					          	  					          
					          </logic:present>								  
					        </table>
					        </div>
					       
					        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">					          
					        </table>					        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="15%" align="right"><bean:message key="prompt.cadastradoEm"/> 
					              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
					            <td class="principalLabel" width="85%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="15%" class="principalLabel"> 
					                    <html:text property="imco_dh_cadastro" styleClass="principalObjForm" maxlength="10" readonly="true"></html:text>
					                  </td>
					                  <td width="85%" class="principalLabel"> 
					                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                      <tr> 
					                        <td class="principalLabel" align="right" width="24%"><bean:message key="prompt.atualizadoEm"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td class="principalLabel" width="76%"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="25%"> 
					                                <html:text property="imco_dh_atualizacao" styleClass="principalObjForm" maxlength="10"  readonly="true"></html:text>
					                              </td>
					                              <td width="75%"> 
					                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                  <tr> 
					                                    <td class="principalLabel" align="right" width="61%"><bean:message key="prompt.ultimoProc"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                                    </td>
					                                    <td class="principalLabel" width="39%"> 
					                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                        <tr> 
					                                          <td width="64%"> 
																<logic:present name="ultimoProcessamento">
																	<input type="text" name="textfield32224" class="principalObjForm" value="<bean:write name="ultimoProcessamento" property="field(himc_dh_processamento)" format="dd/MM/yyyy HH:mm:ss"/>"  readonly="true">
																</logic:present>
					                                            <logic:notPresent name="ultimoProcessamento">
																	<input type="text" name="textfield32224" class="principalObjForm" value=""  readonly="true">
																</logic:notPresent>
					                                          </td>
					                                          <td width="36%">&nbsp;</td>
					                                        </tr>
					                                      </table>
					                                    </td>
					                                  </tr>
					                                </table>
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                      </tr>
					                    </table>
					                  </td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>
					      </div>
					    </td>
					  </tr>
					</table>	
				<!-- FIM ABAS -->
		   </td>
	     </tr>
	   </table>
	   <iframe id="ifrmAutoRefresh" name="ifrmAutoRefresh" src="" width="0%" height="0%" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	   <iframe id="ifrmValidaFTP" name="ifrmValidaFTP" src="" width="0%" height="0%" frameborder="0" marginwidth="0" marginheight="0"></iframe>
</html:form>
</body>

<script>
 	if(document.forms[0].id_imco_cd_importacaocobr.value == '' || document.forms[0].id_imco_cd_importacaocobr.value == 0){
		
	}else{
					
	}
	
	parent.verificaPermissaoExcluir();
</script>

<logic:equal name="importacaoCobrancaForm" property="userAction" value="alterar">
	<script>
		if(!parent.temPermissaoAlterar) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_IMPORTACAO_ALTERACAO_CHAVE%>', parent.document.importacaoCobrancaForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_IMPORTACAO_ALTERACAO_CHAVE%>', parent.document.importacaoCobrancaForm.imgGravar);
			habilitaDesabilitaCampos(true);
		}
	</script>
</logic:equal>
<logic:equal name="importacaoCobrancaForm" property="userAction" value="cancelar">
	<script>
		if(!parent.temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_IMPORTACAO_ALTERACAO_CHAVE%>', parent.document.importacaoCobrancaForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_IMPORTACAO_ALTERACAO_CHAVE%>', parent.document.importacaoCobrancaForm.imgGravar);
			habilitaDesabilitaCampos(true);
		}	
	</script>
</logic:equal>
</html>
