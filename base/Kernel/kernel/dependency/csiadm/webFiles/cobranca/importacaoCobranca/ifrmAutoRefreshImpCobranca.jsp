<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>-- Detalhes da Execu��o --</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/estilos.css" type="text/css">
<script type="text/javascript" src="../csicrm/webFiles/javascripts/funcoesMozilla.js"></script>
<link rel="stylesheet" href="webFiles/cobranca/css/global.css" type="text/css">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
						    
<script>	
	function inicio() {

		var win = window.dialogArguments;
		if(win.document.forms[0].mensagem.value != '') {
			document.getElementById("divAguarde").style.visibility = 'hidden';
			alert(win.document.forms[0].mensagem.value);
		}


		if(document.forms[0].himc_nr_totallinhasarquivoAux.value!=''){
			if(document.forms[0].himc_nr_totallinhasarquivoAux.value == document.forms[0].himc_nr_totallinhasarquivo.value){
				document.getElementById("divAguarde").style.visibility = 'hidden';
				alert('Execu��o Finalizada!');
				loadTela(win);
			}else{
				setTimeout('autoRefresh()', 4000);
			}
		}else{
			setTimeout('autoRefresh()', 4000);
		}

		//if(document.forms[0].himc_nr_totallinhasarquivoAux.value != '' && (document.forms[0].himc_nr_totallinhasarquivoAux.value == document.forms[0].himc_nr_totallinhasarquivo.value)) {
		//	document.getElementById("divAguarde").style.visibility = 'hidden';
		//	alert('Execu��o Finalizada!');
		//} else {
		//	setTimeout('autoRefresh()', 3000);
		//}
	}
		
	function autoRefresh() {
		document.forms[0].target = this.name = "autoRefreshImpCobranca";
		document.forms[0].himc_nr_totallinhasarquivoAux.value = document.forms[0].himc_nr_totallinhasarquivo.value;
		document.forms[0].submit();
	}	

	function loadTela(tela){
		tela.document.forms[0].action = 'AbrirEdicaoImportacaoCobranca.do';
		tela.document.forms[0].userAction.value = 'alterar';
		tela.document.forms[0].inativo.value;
		tela.document.forms[0].submit();
	}
</script>
</head>

<body class="framePadrao" onload="inicio();">
	<html:form action="AutoRefreshImpCobranca" styleId="importacaoCobrancaForm">
		<html:hidden property="id_imco_cd_importacaocobr" />
		<html:hidden property="himc_nr_totallinhasarquivoAux" />
		
		<div id="bordaPrincipal">
			<div class="titulo">
				Detalhes da Execu��o
			</div>
			
			<div class="borda" style=" height : 250px;">	
				<div style="float: left; width : 90%px; height : 200px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
	       					<td class="principalLabel" width="35%" align="right">
	       						Processando o arquivo <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
	       					</td>
	       					<td width="65%">
	       						&nbsp;<html:text property="himc_nm_arquivo" styleClass="principalLabelFundo" style="width: 370px" readonly="true"></html:text>
	       					</td>
	       				</tr>
	       				<tr> 
	       					<td class="principalLabel" width="35%" align="right">
	       						Registros processados do arquivo <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
	       					</td>
	       					<td width="65%">
	       						&nbsp;<html:text property="himc_nr_totallinhasarquivo" styleClass="principalLabelFundo" style="width: 370px" readonly="true"></html:text>
	       					</td>
	       				</tr>
	     				<!-- <tr> 
	       					<td class="principalLabel" width="35%" align="right">
	       						Total de registros processados <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
	       					</td>
	       					<td width="65%">
	       						&nbsp;<html:text property="totalLinhasProcessadas" styleClass="principalLabelFundo" style="width: 370px" readonly="true"></html:text>
	       					</td>
	       				</tr> -->
	       			</table>
				</div>
			</div>
			<div style="clear: left; float: right">
				<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" class="geralCursoHand" onClick="javascript:window.close()">
			</div>
		</div>
		<div id="divAguarde" style="position:absolute; left:190px; top:95px; width:199px; height:148px; z-index:10; visibility: visible"> 
		  	<div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
		</div>
	</html:form>
</body>
</html>