<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/estilos.css" type="text/css">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
</head>

<%
	String detalhe = "";

	if(request != null) {		
		if(request.getAttribute("detalhe") != null) {
			detalhe = (String) request.getAttribute("detalhe");
		}
	}
%>

<script>
	function onLoad(){			
		if('<%=detalhe%>' != '') {
			var detalheErro = '<%=detalhe%>';

			while (detalheErro.indexOf("<espaco>") > -1)
				detalheErro = detalheErro.replace('<espaco>', ' ');
				
			while (detalheErro.indexOf("<br>") > -1)
				detalheErro = detalheErro.replace('<br>', '\n');
				
			document.forms[0].texto.value = detalheErro;
		} else {
			document.forms[0].texto.value = window.dialogArguments;
		}
	}
</script>

<body class="framePadrao" onload="onLoad()">
<html:form action="DetalharDescricao" styleId="importacaoCobrancaForm">
	<div id="bordaPrincipal">
		<div class="titulo">
			Detalhe
		</div>
		
		<div class="borda" style=" height : 400px;">	
		
			<div style="float: left; width : 90%px; height : 400px;">
				<li class="seta">
					<textarea rows="1" name="texto" rows="1" style="height : 360px; width : 700px;" readonly="true">
					</textarea>						
				</li>									
			</div>	
			
		</div>
		<div style="clear: left; float: right">
			<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" class="geralCursoHand" onClick="javascript:window.close()">
		</div>
	</div>
</html:form>
</body>
</html>