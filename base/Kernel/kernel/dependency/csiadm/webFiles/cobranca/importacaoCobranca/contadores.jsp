<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.fw.entity.Vo"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>

<script language="JavaScript">
	function inicio() {
		parent.document.forms[0].mensagem.value = document.forms[0].mensagem.value;
	}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="principalBgrPageIFRM" onload="inicio()">
<html:form action="ExecutarImportacao" styleId="importacaoCobrancaForm">
<html:hidden property="mensagem"/>
</html:form>
</body>
</html>