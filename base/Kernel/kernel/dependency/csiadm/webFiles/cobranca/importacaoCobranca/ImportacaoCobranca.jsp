<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../../../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>
<link rel="stylesheet" href="webFiles/cobranca/css/global.css" type="text/css">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/includes/data.js"></script>
<script language="JavaScript">
var countRegistro = new Number(0);
var temPermissaoIncluir = false;
var temPermissaoAlterar = false;
var temPermissaoExcluir = false;

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'PROCURAR':
	MM_showHideLayers('procurar','','show','conteudo','','hide')
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdConteudo','principalPstQuadroLinkNormalMAIOR');
	setaListaBloqueia();
	setaIdiomaBloqueia();
	break;

case 'CONTEUDO':
	MM_showHideLayers('procurar','','hide','conteudo','','show')
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdConteudo','principalPstQuadroLinkSelecionadoMAIOR');
	setaListaHabilita();
	setaIdiomaHabilita();
	break;
}
 eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}

//Est� funcao tira os espacos em branco da string
function trim(str){
	return str.replace(/^\s+|\s+$/g,"");
}

function gravar(){
	if(ifrmEdit.document.forms[0].laouInDelimitado.length != undefined ){
		if(ifrmEdit.document.forms[0].laouInDelimitado[ifrmEdit.document.forms[0].id_laou_cd_sequencial.selectedIndex].value == 'S'){
			if(trim(ifrmEdit.document.forms[0].imco_ds_separador.value) == ''){
				alert('<bean:message key="prompt.separadorCampos"/> <bean:message key="prompt.obrigatorio"/>');
				return;
			}
		}	
	}
	
	if(trim(ifrmEdit.document.forms[0].imco_ds_importacaocobranca.value)==''){
		alert('<bean:message key="prompt.OCampo"/> <bean:message key="prompt.descricao"/> <bean:message key="prompt.obrigatorio"/>!');
		return;
	}

	if(trim(ifrmEdit.document.forms[0].imco_nr_sequencia.value)==''){
		alert('<bean:message key="prompt.OCampo"/> <bean:message key="prompt.sequencia"/> <bean:message key="prompt.obrigatorio"/>!');
		return;
	}
	
	if(trim(ifrmEdit.document.forms[0].id_orig_cd_origem.value)==''){
		alert('<bean:message key="prompt.OCampo"/> <bean:message key="prompt.origem"/> <bean:message key="prompt.obrigatorio"/>!');
		return;
	}
		
	if(trim(ifrmEdit.document.forms[0].imco_ds_diretorioerro.value)==''){
		alert('<bean:message key="prompt.diretorioErro"/> <bean:message key="prompt.obrigatorio"/>!');
		return;
	}

	if(trim(ifrmEdit.document.forms[0].imco_ds_emailparanotificacao.value)==''){
		alert('<bean:message key="prompt.OCampo"/> <bean:message key="prompt.emailNotificacao"/> <bean:message key="prompt.obrigatorio"/>!');
		return;
	}

	if(!ifrmEdit.document.forms[0].imco_in_diario.checked
			&& !ifrmEdit.document.forms[0].imco_in_segunda.checked
			&& !ifrmEdit.document.forms[0].imco_in_terca.checked
			&& !ifrmEdit.document.forms[0].imco_in_quarta.checked
			&& !ifrmEdit.document.forms[0].imco_in_quinta.checked
			&& !ifrmEdit.document.forms[0].imco_in_sexta.checked
			&& !ifrmEdit.document.forms[0].imco_in_sabado.checked
			&& !ifrmEdit.document.forms[0].imco_in_domingo.checked){
		alert('<bean:message key="prompt.OCampo"/> <bean:message key="prompt.periodicidade"/> <bean:message key="prompt.obrigatorio"/>!');
		return;
	}
	
	if(ifrmEdit.document.forms[0].baixa.checked) {
		if(trim(ifrmEdit.document.forms[0].id_resu_cd_resultado.value)=='') {
			alert('<bean:message key="prompt.OCampo"/> <bean:message key="prompt.resultado"/> <bean:message key="prompt.obrigatorio"/>!');
			return;
		}
		
		if(trim(ifrmEdit.document.forms[0].id_area_cd_area.value)=='') {
			alert('<bean:message key="prompt.OCampo"/> <bean:message key="prompt.area"/> <bean:message key="prompt.obrigatorio"/>!');
			return;
		}
		
		if(trim(ifrmEdit.ifrmCmbFuncionarioResultado.document.forms[0].id_func_cd_resultado.value)=='') {
			alert('<bean:message key="prompt.OCampo"/> <bean:message key="prompt.FuncionarioResultado"/> <bean:message key="prompt.obrigatorio"/>!');
			return;
		}
	}
	
	if(date(ifrmEdit.document.forms[0].imco_dh_execucaode.value, ifrmEdit.document.forms[0].imco_dh_execucaoate.value)){
	 	alert('A Data At� deve ser maior ou igual a data <bean:message key="prompt.execucaoAPartirDe"/>!');
	 	return;
	 }

	if(podeGravarAssociacao()==false){
		return false;
	}
	
	ifrmEdit.document.forms[0].id_func_cd_resultado.value = ifrmEdit.ifrmCmbFuncionarioResultado.document.forms[0].id_func_cd_resultado.value;
	
	ifrmEdit.document.forms[0].submit();
}

function editar(idImco){
	try{
		ifrmEdit.document.forms[0].id_imco_cd_importacaocobr.value = idImco;
		ifrmEdit.document.forms[0].action = 'AbrirEdicaoImportacaoCobranca.do';
		ifrmEdit.document.forms[0].userAction.value = 'alterar';
		ifrmEdit.document.forms[0].inativo.value;
		ifrmEdit.document.forms[0].submit();
		AtivarPasta('CONTEUDO');
	}catch(e){
		setTimeout('editar(' + idImco + ');', 300);
	}
}

function remover(idImco){
	if(!confirm("Deseja remover esta Importacao de Cobran�a?")){
		return false;
	}
	document.forms[0].id_imco_cd_importacaocobr.value = idImco;
	document.forms[0].action = 'RemoverImportacaoCobranca.do';
	document.forms[0].submit();
}

function novo(){
	ifrmEdit.document.forms[0].id_imco_cd_importacaocobr.value = '';
	ifrmEdit.document.forms[0].action = 'CancelarImportacaoCobranca.do';
	ifrmEdit.document.forms[0].submit();
	AtivarPasta('CONTEUDO');
}

function cancelar(){
	document.forms[0].reset();
	document.forms[0].submit();
	AtivarPasta('PROCURAR');
}
</script>
</head>

<body class="principalBgrPage" text="#000000">
<html:form action="AbrirTelaImportacaoCobranca" styleId="importimportacaoCobrancaForm">
<html:hidden property="id_imco_cd_importacaocobr"/>
<html:hidden property="userAction"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLabel" height="30">&nbsp;</td>
        </tr>
      </table>
      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td height="254"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				   <tr>
					  <td class="principalPstQuadroLinkVazio">
						 <table border="0" cellspacing="0" cellpadding="0">
						    <tr>
							   <td class="principalPstQuadroLinkSelecionado" id="tdProcurar" name="tdProcurar" onClick="AtivarPasta('PROCURAR');">
									<bean:message key="prompt.procurar"/>
                               </td>
							   <td class="principalPstQuadroLinkNormalMaior" id="tdConteudo" name="tdConteudo"	onClick="AtivarPasta('CONTEUDO');">
									<bean:message key="prompt.importacaoCobranca"/>
                               </td>
							</tr>
					     </table>
					  </td>
					  <td width="4"><img
						  src="/webFiles/images/separadores/pxTranp.gif" width="1"
					      height="1"></td>
				   </tr>
				</table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td valign="top" class="principalBgrQuadro" align="center"> 
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" height="450">
                    <tr> 
                      <td valign="top"> 
                        <div id="procurar" style="width:99%; height:450px;display: block"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
                            <tr> 
                              <td>&nbsp; </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="50%" class="principalLabel" height="13"><bean:message key="prompt.descricao"/></td>
                              <td class="principalLabel" height="13">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="50%" class="principalLabel"> 
								<html:text property="imco_ds_importacaocobranca" styleClass="principalObjForm"></html:text>                                
                              </td>
                              <td class="principalLabel"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="Consulta" onclick="submit();"> 
                              </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td height="65" valign="top"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" class="principalLstCab">&nbsp;</td>
                                    <td width="25" class="principalLstCab">&nbsp;</td>
                                    <td width="40" class="principalLstCab" align="center"><bean:message key="prompt.codigo"/></td>
                                    <td width="295" class="principalLstCab">&nbsp;<bean:message key="prompt.descricao"/></td>                                    
                                    <td width="65" class="principalLstCab" align="center"><bean:message key="prompt.inativo"/></td>
                                  </tr>
                                </table>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="350">
                                  <tr> 
                                    <td valign="top"> 
                                      <div id="lstProcurar" style="width:100%; height:350;overflow-y: scroll"> 
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
											<logic:present name="imcoVector">
												<bean:size name="imcoVector" id="qtd"/>
												<logic:iterate id="importacao" name="imcoVector" indexId="indice">
													 <tr> 
			                                            <td class="principalLstParMao" width="20" align="center"><img name="imgExcluir" src="webFiles/images/botoes/lixeira.gif" width="14" height="14" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" class="geralCursoHand" title="<bean:message key='prompt.excluir'/>" onclick="remover('<bean:write name="importacao" property="field(id_imco_cd_importacaocobr)"/>')"></td>
	   	                                                <td class="principalLstParMao" width="25">&nbsp;<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMAIMCO_IDIM.xml','<bean:write name="importacao" property="field(id_imco_cd_importacaocobr)"/>')"></td>
			                                            <td class="principalLstParMao" width="45" onClick="editar('<bean:write name="importacao" property="field(id_imco_cd_importacaocobr)"/>');">&nbsp;&nbsp;<bean:write name="importacao" property="field(id_imco_cd_importacaocobr)"/></td>
			                                            <td class="principalLstParMao" width="325" onClick="editar('<bean:write name="importacao" property="field(id_imco_cd_importacaocobr)"/>');">&nbsp;<bean:write name="importacao" property="field(imco_ds_importacaocobranca)"/></td>			                                            
			                                            <td class="principalLstParMao" width="8" onClick="editar('<bean:write name="importacao" property="field(id_imco_cd_importacaocobr)"/>');">&nbsp;<bean:write name="importacao" property="field(imco_dh_inativo)" format="dd/MM/yyyy"/></td>
			                                          </tr>	
			                                          <script>
			                                          	countRegistro++;
			                                          </script>											
												</logic:iterate>
												<logic:equal name="qtd" value="0">
													<tr id="nenhumRegistro" ><td height="260" width="800" align="center"  class="principalLstPar"><br><b><bean:message key="lista.nenhum.registro.encontrado"/> </b></td></tr>
												</logic:equal>											
											</logic:present>
											<logic:notPresent name="imcoVector">
												<tr> 
		                                            <td class="principalLstParMao" width="100%" align="center"><bean:message key="prompt.nenhumregistro"/></td>
		                                        </tr>
											</logic:notPresent>
                                        </table>
                                      </div>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <div id="conteudo" style="width:99%; height:450px;display: none"> 
                          <iframe id="ifrmEdit" name="ifrmEdit" src="AbrirEdicaoImportacaoCobranca.do" width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
              </tr>
              <tr> 
                <td width="100%" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%"	height="4"></td>
                <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="100%" height="4"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
        </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr align="center"> 
			<td width="20"><img name="imgNovo" id="imgNovo" src="webFiles/images/botoes/new.gif" width="14" height="16" class="geralCursoHand" onclick="novo();" title="Novo"></td>
			<td width="20"><img name="imgGravar" id="imgGravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="gravar();" title="Gravar"></td>
			<td width="20"><img name="imgCancelar" id="imgCancelar" src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="cancelar();" title="Cancelar"></td>
        </tr>
      </table>
    </td>
    <td width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
  </tr>
  <tr> 
    <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
</html:form>
</body>

<script>
	var temPermissaoIncluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_IMPORTACAO_INCLUSAO_CHAVE%>');
	var temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_IMPORTACAO_ALTERACAO_CHAVE%>');
	var temPermissaoExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_IMPORTACAO_EXCLUSAO_CHAVE%>');
		
	function verificaPermissaoExcluir() {
		if (!temPermissaoExcluir){		
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.forms[0].btExcluir'+i+');');
			}
		}
	}
	
	verificaPermissaoExcluir();
</script>

<logic:equal name="importacaoCobrancaForm" property="userAction" value="init">
	<script>
		if(!temPermissaoIncluir) {
			alert('init');
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_IMPORTACAO_ALTERACAO_CHAVE%>', document.importacaoCobrancaForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_IMPORTACAO_ALTERACAO_CHAVE%>', document.importacaoCobrancaForm.imgGravar);
			//desabilitaCamposAcoesCobranca();
		}
	</script>
</logic:equal>

<script>
	habilitaListaEmpresas();
	
	setaArquivoXml("CS_ASTB_IDIOMAIMCO_IDIM.xml");
	habilitaTelaIdioma();
</script>
</html>