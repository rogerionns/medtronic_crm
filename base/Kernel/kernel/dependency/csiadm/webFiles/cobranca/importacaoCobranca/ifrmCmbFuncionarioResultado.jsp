<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>

<body class="principalBgrPageIFRM" text="#000000">
<html:form action="/CarregaCmbFuncionarioResultado.do" styleId="importacaoCobrancaForm">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="76%">
				<html:select property="id_func_cd_resultado" styleClass="principalObjForm">
					<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
					<logic:present name="funcVector">
						<html:options collection="funcVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario" />
					</logic:present>
				</html:select>
			</td>
			<td width="24%">&nbsp;</td>
		</tr>
	</table>
</html:form>
</body>
</html>
