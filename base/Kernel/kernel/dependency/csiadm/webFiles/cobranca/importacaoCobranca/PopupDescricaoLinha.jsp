<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="br.com.plusoft.fw.entity.Vo"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/estilos.css" type="text/css">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
</head>

<%
	Vo imcoVo = null;
	String detalhe = "";

	if(request != null) {
		if(request.getAttribute("imcoVo") != null) {
			imcoVo = (Vo) request.getAttribute("imcoVo");
		}
		
		if(request.getAttribute("detalhe") != null) {
			detalhe = (String) request.getAttribute("detalhe");
		}
	}
%>

<script>
	function onLoad() {
		var linhaErro = window.dialogArguments;
		
		<% if(imcoVo != null) { %>		
			if(document.forms[0].tipoDetalhe.value == '2') {
				var ultimoValorRegua = new Number(0);
				var reguaNumeros = '';
				var regua = '';
				var imagem = '';
				var detalhe = '<%=detalhe%>';
				var arrayDetalhe = detalhe.split("<br>");
				var nomeCampos = arrayDetalhe[0];
				var valorCampos = arrayDetalhe[1];
				
				if(nomeCampos.indexOf(";") > -1 && valorCampos.indexOf(";") > -1) {
					var nomeCamposArray = nomeCampos.split(";");
					var valorCamposArray = valorCampos.split(";");
					var valorCamposSemEspacosArray = valorCampos.split(";");
					var conteudo = '';
					var separador = '<%=imcoVo.getFieldAsString("imco_ds_separador")%>';
					
					while (nomeCampos.indexOf("<espaco>") > -1)
						nomeCampos = nomeCampos.replace('<espaco>', '&nbsp;');
						
					while (valorCampos.indexOf("<espaco>") > -1)
						valorCampos = valorCampos.replace('<espaco>', '&nbsp;');
										
					//Armazenando os nomes dos campos		
					nomeCampos = '';
					for(i = 0; i < nomeCamposArray.length; i++) {				
						while (valorCamposSemEspacosArray[i].indexOf("<espaco>") > -1)
							valorCamposSemEspacosArray[i] = valorCamposSemEspacosArray[i].replace('<espaco>', ' ');
					
						if(nomeCamposArray[i].length > valorCamposArray[i].length) {
							nomeCamposArray[i] = acronymLst(nomeCamposArray[i], (valorCamposArray[i].length - 3));
						} else if(valorCamposSemEspacosArray[i].length > nomeCamposArray[i].length) {
							var diferenca = valorCamposSemEspacosArray[i].length - nomeCamposArray[i].length;
							nomeCamposArray[i] += preencheEspacoBranco(diferenca, "&nbsp;");
						}
						
						if(separador)
							nomeCamposArray[i] += "&nbsp;";
						
						nomeCampos += nomeCamposArray[i];
					}
								
					if(separador == '') {	// Se for importação sem separdor	
						//Armazenando os valores dos campos
						while (valorCampos.indexOf(";") > -1)
							valorCampos = valorCampos.replace(';', '');
														
						var qtdade = new Number(1000);		
						for(countLinha = 0; countLinha < qtdade; countLinha++) {
							if((countLinha % 10) == 0) {
								reguaNumeros += countLinha + preencheEspacoBranco(10 - new String(countLinha).length, "&nbsp;");
								regua += ".";
								imagem += "<img class='barraReguaGrd'/>";
								ultimoValorRegua = countLinha;
							} else if((countLinha % 10) == 5) {
								regua += ".";
								imagem += "<img class='barraReguaGrd'/>";
							} else if((countLinha + 1) == qtdade) {
								reguaNumeros += ultimoValorRegua + 10;
								regua += ".";
								imagem += "<img class='barraReguaPqn'/>";
							} else {
								regua += ".";
								imagem += "<img class='barraReguaPqn'/>";
							}
						}
						imagem += "<img class='barraReguaGrd'/>";
					} else { // Se for importação com separdor
						var espacos = ''; 
						for(i = 0; i < nomeCamposArray.length - 1; i++) {
							espacos = preencheEspacoBranco(valorCamposSemEspacosArray[i].length - new String(i).length, "&nbsp;");
							reguaNumeros += i + espacos + "&nbsp;";
							imagem += "<img class='barraReguaGrd'/>" + espacos + "&nbsp;";
						}
					}
					
					conteudo += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
					conteudo += '	<tr>';
					conteudo += '		<td>';
					conteudo += '		<font face="Courier New" size="2">';
					conteudo += reguaNumeros;
					conteudo += '		</font>';
					conteudo += '		</td>';
					conteudo += '	</tr>';
					conteudo += '</table>';
					/*conteudo += '<table width="3000px" border="0" cellspacing="0" cellpadding="0">';
					conteudo += '	<tr>';
					conteudo += '		<td>';
					conteudo += '		<font face="Courier New" size="2">';
					conteudo += regua;
					conteudo += '		</font>';
					conteudo += '		</td>';
					conteudo += '	</tr>';
					conteudo += '</table>';*/
					conteudo += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
					conteudo += '	<tr>';
					conteudo += '		<td>';
					conteudo += '		<font face="Courier New" size="2">';
					conteudo += imagem;
					conteudo += '		</font>';
					conteudo += '		</td>';
					conteudo += '	</tr>';
					conteudo += '</table>';
					conteudo += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';			
					conteudo += '	<tr>';
					conteudo += '		<td>';
					conteudo += '		<font face="Courier New" size="2" color="red">';
					conteudo += nomeCampos;
					conteudo += '		</font>';
					conteudo += '		</td>';
					conteudo += '	</tr>';
					conteudo += '</table>';
					/*conteudo += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';			
					conteudo += '	<tr>';
					conteudo += '		<td>';
					conteudo += '		<font face="Courier New" size="2">';
					conteudo += valorCampos;
					conteudo += '		</font>';
					conteudo += '		</td>';
					conteudo += '	</tr>';
					conteudo += '</table>';*/
					
					document.getElementById("divRegua").innerHTML = conteudo;
					document.getElementById("divDetalhe").innerHTML = valorCampos;
				}
			} else { 
				document.getElementById("divRegua").innerHTML = linhaErro;
			}
		<% } %>
		
		if(document.forms[0].sucesso.value == 'true') {
			alert('Importação realizada com sucesso!');
		}
	}
							
	function preencheEspacoBranco(quantidade, caractere){
		var retorno = "";
		for(j = 0; j < quantidade; j++){
			retorno = retorno + caractere;	
		}	
		return retorno;
	}
	
	function salvar() {
		if(confirm('Deseja importar novamente esta linha?')) {
			document.forms[0].linhaAlterada.value = document.getElementById("divDetalhe").innerHTML;
			document.forms[0].target = this.name = 'salvar';
			document.forms[0].action = 'ProcessaLinhaAlterada.do';
			document.forms[0].submit();
		}
	}
</script>

<body class="framePadrao" onload="onLoad()">
<html:form action="DetalharDescricao" styleId="importacaoCobrancaForm">
<html:hidden property="id_imco_cd_importacaocobr"/>
<html:hidden property="hiac_nr_sequencia"/>
<html:hidden property="linhaAlterada"/>
<html:hidden property="tipoDetalhe"/>
<html:hidden property="sucesso"/>
	<div id="bordaPrincipal">
		<div class="titulo">Detalhe</div>
		<div class="borda">
			<div style="height: 55px;">
				<div id="divRegua" style="scroll: no; overflow: hidden; height: 55px; width: 785px; position: absolute; z-index: 2;" ></div> 	
			</div>
			<div style="height: 40px;" onClick="document.getElementById('divDetalhe').contentEditable='true';">
				<div id="divDetalhe" class="divCourier" style="overflow: auto; height: 40px; width: 785px; position: absolute; z-index: 3;" onScroll="divRegua.scrollLeft=this.scrollLeft;" onClick="this.contentEditable='true';"></div>
			</div>
			<div style="height: 265px;">
				&nbsp;
			</div>
			<div style="height: 20px;" align="right">
				<img src="webFiles/images/botoes/gravar.gif" border="0" alt="Salvar Alterações" class="geralCursoHand" onClick="salvar()">
			</div>
			<div style="clear: left; float: right">
				<br>
				<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" class="geralCursoHand" onClick="javascript:window.close()">
			</div>
		</div>
	</div>
</html:form>
</body>
</html>