<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="contenttab">

	<table>
		<tr class="label">
			<td style="width: 300px;"><plusoft:message key="prompt.estrategia.tab.acao.acao"/></td>
			<td style="width: 210px;"><plusoft:message key="prompt.estrategia.tab.acao.empresaCobranca"/></td>
			<td colspan="4" style="width: 220px;"><plusoft:message key="prompt.estrategia.tab.acao.grupoRegrasNegociacao"/></td>
		</tr>
		<tr>
			<td>
				<select class="idAccoCdAcaocob">
					<option value="">-- Selecione uma op��o--</option>
					<logic:present name="cbCdtbAcaocobAccoVector"><logic:iterate id="accoVo" name="cbCdtbAcaocobAccoVector">
					<option tipo="<bean:write name="accoVo" property="field(id_tpac_cd_tpacaocob)" />" value="<bean:write name="accoVo" property="field(id_acco_cd_acaocob)" />"><bean:write name="accoVo" property="field(acco_ds_acaocob)" /></option>
					</logic:iterate></logic:present>
				</select>
			</td>
			<td>
				<select class="idEmcoCdEmprecob">
					<option value="">-- Selecione uma op��o--</option>
					<logic:present name="cbCdtbEmprecobEmcoVector"><logic:iterate id="emcoVo" name="cbCdtbEmprecobEmcoVector">
					<option value="<bean:write name="emcoVo" property="field(id_emco_cd_emprecob)" />"><bean:write name="emcoVo" property="field(emco_ds_empresa)" /></option>
					</logic:iterate></logic:present>
				</select>
			</td>
			<td colspan="4">
				<select class="idReneCdRegnegociacao">
					<option value="">-- Selecione uma op��o--</option>
					<logic:present name="cbCdtbRegnegociacaoReneVector"><logic:iterate id="reneVo" name="cbCdtbRegnegociacaoReneVector">
					<option value="<bean:write name="reneVo" property="field(id_grre_cd_gruporene)" />"><bean:write name="reneVo" property="field(grre_ds_gruporene)" /></option>
					</logic:iterate></logic:present>
				</select>
			</td>
		</tr>
		<tr>
			<td><plusoft:message key="prompt.estrategia.tab.acao.campanha"/></td>
			<td><plusoft:message key="prompt.estrategia.tab.acao.subcampanha"/></td>
			<td style="width:60px"><plusoft:message key="prompt.estrategia.tab.acao.diasUteis"/></td>
			<td style="width:60px"><plusoft:message key="prompt.estrategia.tab.acao.sequencia"/></td>
			<td style="width:100px">&nbsp;</td>
			<td class="imagem">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<select class="idCampCdCampanha" disabled="true">
					<option value="">-- Selecione uma op��o--</option>
					<logic:present name="csCdtbCampanhaCampVector"><logic:iterate id="campVo" name="csCdtbCampanhaCampVector">
					<option dist="<bean:write name="campVo" property="field(camp_in_distribuicao)" />" value="<bean:write name="campVo" property="field(id_camp_cd_campanha)" />"><bean:write name="campVo" property="field(camp_ds_campanha)" /></option>
					</logic:iterate></logic:present>
				</select>
			</td>
			<td>
				<select class="idPublCdPublico" disabled="true">
					<option value="">-- Selecione uma op��o--</option>
				</select>
			</td>
			<td><input type="text" class="text left dias" id="diasuteis" maxlength="3" /></td>
			<td><input type="text" class="text left sequencia" id="sequencia" maxlength="2" /></td>
			<td><input type="checkbox" id="exec_auto" disabled="true"/> <plusoft:message key="prompt.estrategia.tab.acao.execAutomatica" /></td>
			<td><a href="#" class="incluiracao left" title="<plusoft:message key="prompt.estrategia.listar" />"><img src="/plusoft-resources/images/botoes/setaDown.gif" /></a></td>
		</tr>
	</table>
	
	<br>
	
	<div class="list borda">
	<table>
			<thead>
			
				<tr>
					<td class="imageCab">&nbsp;</td>
					<td>&nbsp;<plusoft:message key="prompt.estrategia.tab.acao.acao"/></td>
					<td class="fix140">&nbsp;<plusoft:message key="prompt.estrategia.tab.acao.empresaCobranca"/></td>
					<td>&nbsp;<plusoft:message key="prompt.estrategia.tab.acao.grupoRegrasNegociacao"/></td>
					<td class="fix60Center">&nbsp;<plusoft:message key="prompt.estrategia.tab.acao.diasUteis"/></td>
					<td class="fix40Center">&nbsp;<plusoft:message key="prompt.estrategia.tab.acao.sequencia"/></td>
					<td class="fix60Center">&nbsp;<plusoft:message key="prompt.estrategia.tab.acao.execAutomatica"/></td>
				</tr>
		
		
			</thead>
		</table>
		
	<div class="scrolllist scrolltableacoescob">
		<table id="tableacco">
			<tbody>
				<logic:present name="acoesViewState">
					<logic:iterate id="acco" name="acoesViewState" indexId="idx">
						<tr indx="<bean:write name="idx" />" class="geralCursoHand">
							
							<td idacao="<bean:write name="acco" property="field(id_accr_cd_acaocriterio)" />" class="image"><a href="#" class="lixeira" title="Excluir"></a></td>
							
							<td idacao="<bean:write name="acco" property="field(id_accr_cd_acaocriterio)" />" class="clickable"><bean:write name="acco" property="acronymHTML(acco_ds_acaocob, 22)" filter="html" /></td>
							<td idacao="<bean:write name="acco" property="field(id_accr_cd_acaocriterio)" />" class="clickable fix140"><bean:write name="acco" property="acronymHTML(emco_ds_empresa, 18)" filter="html" /></td>
							<td idacao="<bean:write name="acco" property="field(id_accr_cd_acaocriterio)" />" class="clickable"><bean:write name="acco" property="acronymHTML(grre_ds_gruporene, 18)" filter="html" /></td>
							<td idacao="<bean:write name="acco" property="field(id_accr_cd_acaocriterio)" />" class="clickable fix60Center"><bean:write name="acco" property="field(accr_nr_dias)" /></td>
							<td idacao="<bean:write name="acco" property="field(id_accr_cd_acaocriterio)" />" class="clickable fix40Center"><bean:write name="acco" property="field(accr_nr_sequencia)" /></td>
							<td idacao="<bean:write name="acco" property="field(id_accr_cd_acaocriterio)" />" class="clickable fix60Center"><bean:write name="acco" property="field(accr_in_execauto)" /></td>
						</tr>
						</logic:iterate>
					</logic:present>
					<!-- tr class="nenhumregistroacoes" height="70px">
						<td>
							<div align="center" class="principalLabel" style="position:absolute; width:90%; height:80px; z-index:7; ">
								<b><plusoft:message key="lista.nenhum.registro.encontrado"/></b> 
						 	</div>
				 		</td>
				 	</tr -->
			</tbody>
		</table>
	</div>
	</div>
	
	<input type="hidden" name="mensagemacao" class="mensagemacao" value='<%=request.getAttribute("mensagem")%>'/>
	
	<logic:present name="msgerro">
		<script type="text/javascript">
			$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
		</script>
	</logic:present>
	
	<html:form styleId="acoesForm">
		<html:hidden property="acoesViewState" styleId="acoesViewState" />
		<html:hidden property="acoesExclusaoViewState" styleId="acoesExclusaoViewState" />
	</html:form>
	
</div>


