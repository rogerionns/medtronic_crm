<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="content">

	<div class="formtab">
		<table>
			<tr>
				<td class="formlabel">C�digo</td>
				<td class="seta"></td>
				<td><input type="text" class="codigo text" id="idEstrCdEstrategia" readonly="true" /></td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td class="formlabel"><label for="estrDsEstrategia">Descri��o</label></td>
				<td class="seta"></td>
				<td colspan="4"><input type="text" class="text" id="estrDsEstrategia" /></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="formlabel"><label for="estrNrPrioridade">Prioridade</label></td>
				<td class="seta"></td>
				<td><input type="text" class="text numeric" id="estrNrPrioridade" maxlength="2" /></td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td class="formlabel">Validade: De</label></td>
				<td class="seta"></td>
				<td style="width:150px"><input type="text" class="text numeric left" id="estrDhValidadede" maxlength="10" /></td>
				<td style="width:30px" align="center">At�</label></td>
				<td class="seta" style="width: 10px;"></td>
				<td style="width:150px"><input type="text" class="text numeric left" id="estrDhValidadeate" maxlength="10" /></td>
				<td style="width:300px">&nbsp;</td>
			</tr>
			<tr>
				<td class="formlabel"><label for="estrInInativo">Inativo</label></td>
				<td class="seta"></td>
				<td><input type="checkbox" class="left" id="estrInInativo"/></td>
				<td colspan="4">&nbsp;</td>
			</tr>
		</table>
	</div>
	<div class="right divbtnexec">
		<a href="#" class="execacao" title="<plusoft:message key="prompt.estrategia.execucao" />"><img src="/plusoft-resources/images/botoes/Acao.gif" width="26" height="26" /></a>
	</div>
	<div class="right divbtnhist" style="width: 40px">
		<a href="#" class="btnhistorico" title="<plusoft:message key="prompt.estrategia.historico" />"><img src="/plusoft-resources/images/botoes/Msg_Historico.gif" /></a>
	</div>
	
	
	<br>
	
	<div class="tabframeEdit">
		<div class="tabtabs">
			<ul>
				<li id="criterios"><plusoft:message key="prompt.estrategia.tab.criterios" /></li>
				<li id="acao"><plusoft:message key="prompt.estrategia.tab.acoes" /></li>
				<li id="periodicidade"><plusoft:message key="prompt.estrategia.tab.periodicidade" /></li>
				<li id="notificacao"><plusoft:message key="prompt.estrategia.tab.notificacao" /></li>
			</ul>
		</div>
		
		
		<div class="tabcontent">
			<div id="criteriostab">
				<div class="criterios"><div class="content"></div></div>
			</div>
			
			<div id="acaotab">
				<div class="acoes"><div class="content"></div></div>
			</div>
			
			<div id="periodicidadetab">
				<div class="periodicidade"><div class="content"></div></div>
			</div>
			
			<div id="notificacaotab">
				<div class="notificacao"><div class="content"></div></div>
			</div>
		</div>
	</div>
	
	<div class="popupdet noscroll" style="display: none; overflow: hidden;">
		<div class="frameEspec lblDetExecucao" style="overflow: hidden;"></div>
	</div>
	
	<div class="popuphistorico" style="display: none; ">
		<div class="listahistorico"></div>
	</div>
	
	<logic:present name="msgerro">
		<script type="text/javascript">
			$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
		</script>
	</logic:present>
	
	
</div>


