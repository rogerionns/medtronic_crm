<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>

<title><plusoft:message key="prompt.estrategia.titleTelaEstrategia" /></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">

<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<body class="tela">

<div class="tabframe frame shadow hecrframe">
	<div class="title"><plusoft:message key="prompt.estrategia.tab.criterios" /></div>
	<br>	
	<div class="list">
		<table>
			<thead>
			
				<tr>
					<td>&nbsp;Criterio</td>
					<td class="fix120Center">&nbsp;<plusoft:message key="prompt.estrategia.dataExec"/></td>
					<td class="fix60Center"><plusoft:message key="prompt.estrategia.duracao"/></td>
					<td class="fix60Center"><plusoft:message key="prompt.estrategia.qtd"/></td>
					<td class="fix80Right"><plusoft:message key="prompt.valor"/></td>
					<td class="image">&nbsp;</td>
					<td class="image">&nbsp;</td>
				</tr>
		
		
			</thead>
		</table>
	
		<div class="scrolllist scrolltable hecrlist">
			<table>
				<tbody>
					<logic:present name="hecrArray">
						<logic:iterate id="hist" name="hecrArray">
								<tr>
									<td><bean:write name="hist" property="acronymHTML(crit_ds_nome, 30)" filter="html" /></td>
									<td class="fix120Center"><bean:write name="hist" property="field(hecr_dh_inicial)" formatKey="prompt.cobranca.dateformat"/></td>
									<td class="fix60Center"><bean:write name="hist" property="field(duracao)" />(s)</td>
									<td class="fix60Center"><bean:write name="hist" property="field(hecr_nr_registros)" /></td>
									<td class="fix80Right"><bean:write name="hist" property="field(hecr_vl_valor)" format="##,###,##0.00" /></td>
									<td class="image"><a href="#" sql="<bean:write name="hist" property="field(hecr_tx_sql)" />" class="criterioselecao" title="<plusoft:message key="prompt.estrategia.criterios.criterioSelecao" />"><img src="/plusoft-resources/images/botoes/lupa.gif" /></a></td>
									<td class="image">
									<logic:notEmpty name="hist" property="field(hecr_ds_erroquery)">
										<a href="#" erro="<bean:write name="hist" property="field(hecr_ds_erroquery)" />" class="criterioerro" title="<plusoft:message key="prompt.estrategia.criterios.erro.query" />"><img src="/plusoft-resources/images/icones/exclamacao.gif" /></a>
									</logic:notEmpty>
									</td>
								</tr>
						</logic:iterate>
					</logic:present>
				</tbody>
			</table>
		</div>
	</div>
</div>			
<br>
<div class="tabframe frame shadow heacframe">
	<div class="title"><plusoft:message key="prompt.estrategia.tab.acoes" /></div>
	<br>	
	<div class="list">
		<table>
			<thead>
			
				<tr>
					<td>&nbsp;<plusoft:message key="prompt.estrategia.tab.acao.acao"/></td>
					<td class="fix110Center"><plusoft:message key="prompt.estrategia.dataExec"/></td>
					<td class="fix40Center"><plusoft:message key="prompt.estrategia.duracao"/></td>
					<td class="fix40Center"><plusoft:message key="prompt.estrategia.qtd"/></td>
					<td class="fix60Center"><plusoft:message key="prompt.estrategia.qtd.exec"/></td>
					<td class="fix120"><plusoft:message key="prompt.empresa"/></td>
					<td class="fix120"><plusoft:message key="prompt.campanha"/></td>
					<td class="fix120"><plusoft:message key="prompt.subcampanha"/></td>
				</tr>
		
		
			</thead>
		</table>
	
		<div class="scrolllist scrolltable heaclist">
			<table>
				<tbody>
					<logic:present name="heacArray">
						<logic:iterate id="hist" name="heacArray">
								<tr class="geralCursoHand">
									<td><bean:write name="hist" property="acronymHTML(acco_ds_acaocob, 22)" filter="html"/></td>
									<td class="fix110Center"><bean:write name="hist" property="field(heac_dh_inicial)" formatKey="prompt.cobranca.dateformat"/></td>
									<td class="fix40Center"><bean:write name="hist" property="field(duracao)" />(s)</td>
									<td class="fix40Center"><bean:write name="hist" property="field(heac_nr_registros)" /></td>
									<td class="fix60Center"><bean:write name="hist" property="field(heac_nr_executados)" /></td>
									<td class="fix120"><bean:write name="hist" property="acronymHTML(emco_ds_empresa, 16)" filter="html"/></td>
									<td class="fix120"><bean:write name="hist" property="acronymHTML(camp_ds_campanha, 16)" filter="html" /></td>
									<td class="fix120"><bean:write name="hist" property="acronymHTML(publ_ds_publico, 16)" filter="html" /></td>
								</tr>
						</logic:iterate>
					</logic:present>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="popup" style="display: none;">
	<div class="frame comandosql"></div>
</div>
<div class="right margin">
	<a href="#" class="clickable btnsair" title="<plusoft:message key="prompt.sair" />"><img src="/plusoft-resources/images/botoes/out.gif" /></a>
</div>

	<link rel="stylesheet" href="/csiadm/webFiles/cobranca/estrategia/estrategia.css">
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>

<script>
		
$(".hecrlist").find(".criterioselecao").bind("click", function(event) {
	event.preventDefault();
	$(".comandosql").html($(this).attr("sql"));
	abrirpopup(".popup", "250", "400", "Comando SQL");
});

$(".hecrlist").find(".criterioerro").bind("click", function(event) {
	event.preventDefault();
	$(".comandosql").html($(this).attr("erro"));
	abrirpopup(".popup", "250", "400", "Erro Query");
});

$(".btnsair").bind("click", function(event) {
	event.preventDefault();
	window.close();
});

	var abrirpopup = function(popup, altura, largura, title) {
		$(popup).dialog({
			height: altura,
			width: largura,
			modal: true,
			resizable: true,
			title: title,
			buttons: {
				Ok : function() {
					$( this ).dialog( "close" );
				}
			},
			open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
	    	close: function(event, ui){
		    	$('body').css('overflow','auto'); 
		    } 
		});
	}

</script>			

</body>
</html:html>