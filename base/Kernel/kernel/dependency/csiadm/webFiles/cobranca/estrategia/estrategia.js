/**
 * Extens�es de estrategias
 * 
 * @author vinicius
 * @since 21/02/2012
 */
var largura = 0;
var novo = false;
var finalizada = false;

var estrategia = (function($) {
	var idestrcdestrategia = "";
	
	return {

		//Metodo que adiciona os bot�es espec na tela
		addButton : function(src, id, title, width) {
			largura += new Number(width);
			var botao = "";
			
			botao += "<div id=\"child\" style=\"float: left; width: "+ width +"px;\">";
			botao += "<span class=\""+ id +"\" id=\"" + id + "\" style=\"cursor:pointer\"> <img src=\"" + src + "\" title=\"" + title + "\" />";
			botao += title;
			botao += "</span>"
			botao += "</div>";
			
			document.getElementById("btnsEspec").insertAdjacentHTML("BeforeEnd", botao);
			$('.btnsEspe').css("width", largura);
		},
		
		onload_estrategia : function(){
			
			$(".telaestrategias").show();
			
			$(".criterios").load("/csiadm/Estrategia.do?userAction=criterio", {id:idestrcdestrategia}, function(text, status, xhr) {
				if(status=="ajaxError") {
					alert(text);
				}
				estrategia.criterios.onload_criterio();
			});
			
			$(".acoes").load("/csiadm/Estrategia.do?userAction=acoes", {id:idestrcdestrategia}, function(text, status, xhr) {
				if(status=="ajaxError") {
					alert(text);
				}
				estrategia.acoes.onload_acaolist();
			});
			
			$(".periodicidade").load("/csiadm/EstrategiaPeriodicidade.do", null, function(text, status, xhr) {
				if(status=="ajaxError") {
					alert(text);
				}
				estrategia.periodicidade.onload_periodicidade();
			});
			
			$(".notificacao").load("/csiadm/EstrategiaNotificacao.do", null, function(text, status, xhr) {
				if(status=="ajaxError") {
					alert(text);
				}
			});
			
			$("#estrDhValidadede").datepicker({
				onSelect : estrategia.validadata	
			});

			$("#estrDhValidadeate").datepicker({
				onSelect : estrategia.validadata
			});
			
			$(".tabframeEdit").inittabs();
			
			if(novo){
				$("#estrategias").click();
				novo = false;
			}
			
			$("#estrNrPrioridade").number();
			$(".execacao").addClass("disabled").bind("click", function(){});
			$(".btnhistorico").addClass("disabled").bind("click", function(){});
			//$(".divbtnhist").hide();
			
		},
		
		//Fun��o para validar se a data inicial � maior que a final
		validadata : function(){
			
			if($("#estrDhValidadede").val() != ""  && $("#estrDhValidadede").val() != "" ){
				var days = window.top.date($("#estrDhValidadede").val(), $("#estrDhValidadeate").val());
				if (days) {
		        	alert("Data DE n�o pode ser maior que a data AT�.");
		        	$("#estrDhValidadeate").val("");
		            return false;
		        } else {
		            return true;
		        }
			}
		},
		
		abrirpopup : function(popup, altura, largura, title) {
			$(popup).dialog({
				height: altura,
				width: largura,
				modal: true,
				resizable: true,
				title: title,
				buttons: {
					Ok : function() {
						$( this ).dialog( "close" );
					}
				},
				open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
		    	close: function(event, ui){
			    	$('body').css('overflow','auto'); 
			    } 
			});
		},
		
		//Fun��o para validar os campos no momento da grava��o
		validagravacao : function(){
			var temCriterio = false;
			var temAcoes = false;
			
			if($("#estrDsEstrategia").val() == ""){
				alert(descricao);
				return false;
			}
		
			if($("#estrNrPrioridade").val() == ""){
				alert(prioridade);
				return false;
			}
			
			if($("#estrDhValidadede").val() == "" || $("#estrDhValidadeate").val() == ""){
				alert(dtValidade);
				return false;
			}
			
			$(".criterios").find(".lixeira").each(function() {
				temCriterio = true;
			});
			
			$(".acoes").find(".lixeira").each(function() {
				temAcoes = true;
			});
			
			if(!temCriterio){
				alert(msgCriterios);
				return false;
			}
			
			if(!temAcoes){
				alert(msgAcoes);
				return false;
			}
				
			if(!document.getElementById("chkControleManual").checked){
				if($("#chkUmaVez").val() == ""){
					alert(umavez);
					return false;
				}
				
				if(!document.getElementById("chkSeg").checked && !document.getElementById("chkTer").checked
						&& !document.getElementById("chkQua").checked && !document.getElementById("chkQui").checked
						&& !document.getElementById("chkSex").checked && !document.getElementById("chkSab").checked && !document.getElementById("chkDom").checked){
					alert(diaSemana);
					return false;
				}
			}
		
			return true;
			
		},
		
		criterios : {
			
			//Fun��o para inserir as fun��es dos objetos da aba de criterios quando carregada
			onload_criterio : function(){
				
				if($(".criterios tbody .lixeira").length > 0) {
					//se tiver registros esconde o div de registros nao encontrado
					$(".nenhumregistrocriterio").hide();
				}
					
					
				$(".listarvisao").bind("click", function (event){
					event.preventDefault();
					event.stopPropagation();
					
					if($(".idVisaCdVisao").val() == ''){
						alert("Por favor selecione uma vis�o");
						return;
					}
					
					$(".listacriterios").loadaguarde();
					
					var url = "/csiadm/Estrategia.do?userAction=criterio";
					var data = {
							visa : $(".idVisaCdVisao").val(),
							criteriosViewState : document.getElementById("criteriosForm").criteriosViewState.value,
							criteriosExclusaoViewState : document.getElementById("criteriosForm").criteriosExclusaoViewState.value,
							totaisCriterioViewState : document.getElementById("criteriosForm").totaisCriterioViewState.value
							
						};
					
					$(".criterios").show();
					$(".criterios").loadaguarde();
					
					$(".criterios").loadaguarde();
					$(".criterios").load(url, data, function(text, status, xhr) {
						if(status=="ajaxError") {
							alert(text);
						}
						
						estrategia.criterios.onload_criterio();
						
					});
					
				});
				
				$(".criterios").find(".criterioselecao").unbind();
				$(".criterios").find(".criterioselecao").bind("click", function(event) {
					event.preventDefault();
					event.stopPropagation();
					
					$(".comandosql").html($(this).attr("sql")); 
					estrategia.abrirpopup(".popup", "250", "400", "Comando SQL");
				});
				
				$(".criterios").find(".refresh").unbind();
				$(".criterios").find(".refresh").bind("click", function(event) {
					event.preventDefault();
					event.stopPropagation();
					
					var idcriterio = $(this).parent().attr("idcrit");
					var idvisao = $(this).parent().attr("idvisa");
					var idestr = $(this).parent().attr("idestr");
					
					$.post('/csiadm/Estrategia.do?userAction=atualizaNrCriterios', { idcritcdcriterio : idcriterio, idvisacdvisao : idvisao }, function(ret) {
						
						if(ret.msgerro != '' && ret.msgerro != undefined){
							alert(ret.msgerro);
						}
						
						$(".criterios").load("/csiadm/Estrategia.do?userAction=criterio", {id:idestr}, function(text, status, xhr) {
							if(status=="ajaxError") {
								alert(text);
							}
							estrategia.criterios.onload_criterio();
						});
						
					});
				});
				
				$(".criterios").find(".lixeira").bind("click", function(event) {
					event.preventDefault();
					event.stopPropagation();
					
					var linhacrit = $(this).parent().parent().attr("indx");
					var idcriterio = $(this).parent().attr("idcrit");
					$.modal.showconfirm(msgExcluir, { buttons: [
         				{ label : "Sim", click : function(){
         						$(".criterios").load("/csiadm/Estrategia.do?userAction=criterio", { linha:linhacrit, idcrit:idcriterio, criteriosViewState: document.getElementById("criteriosForm").criteriosViewState.value, totaisCriterioViewState: document.getElementById("criteriosForm").totaisCriterioViewState.value, criteriosExclusaoViewState: document.getElementById("criteriosForm").criteriosExclusaoViewState.value }, estrategia.criterios.onload_criterio);
         						$.modal.close();
         					}
         				},
         				{ label : "N�o", click : $.modal.close }
         			] });
				});
				
				
				$(".criterios").find(".mensagemCamposObrigatorios").each(function() {
					if($(this).val() != ""){
						alert($(this).val());
					}
				});
				
				$(".atualizarvisao").bind("click", function (event){
					event.preventDefault();
					event.stopPropagation();
					
					$(".idVisaCdVisao").jsonoptions("/csiadm/Estrategia.do?userAction=reloadcmbvisao", "id_visa_cd_visao", "visa_ds_visao", null);
				});
				
				
				if($(".mensagemcriterio").val() != ""){
					alert($(".mensagemcriterio").val());
				}
				
			}
			
	
		},
	
		acoes : {
			
			//Fun��o para inserir as fun��es dos objetos da aba de a��es quando carregada
			onload_acaolist : function(){
				
				if($(".acoes tbody .lixeira").length > 0) {
					//se tiver registros esconde o div de registros nao encontrado
					$(".nenhumregistroacoes").hide();
				}
				
				$(".incluiracao").bind("click", function(event) {
					event.preventDefault();
					event.stopPropagation();

					if($(".idAccoCdAcaocob").val() == ""){
						alert(msgCmdAcoes);
						return false;
					}
					
					if($(".idEmcoCdEmprecob").val() == ""){
						alert(msgCmdEmpresa);
						return false;
					}
					
					var tipo = $(".idAccoCdAcaocob option:selected").attr("tipo")

					if(tipo == '1'){
						if($(".idReneCdRegnegociacao").val() == ""){
							alert(msgCmdGrupo);
							return false;
						}
					}
					
					if(tipo == '1'){
						if($(".idCampCdCampanha").val() == ""){
							alert(msgCampanha);
							return false;
						}


						var dist = $(".idCampCdCampanha option:selected").attr("dist")
						
						if(dist != "S" && $(".idPublCdPublico").val() == ""){
							alert(msgSubCampanha);
							return false;
						} 
					}
					
					
					var txtCamp = "";
					var txtPubl = "";
					var exec_auto = "N";
					
					if($(".idCampCdCampanha").val() != "")
						txtCamp = $(".idCampCdCampanha option:selected").text();
					
					var txtCamp = "";
					if($(".idPublCdPublico").val() != "")
						txtPubl = $(".idPublCdPublico option:selected").text();
					
					if($("#diasuteis").val() == ""){
						alert(msgDias);
						return false;
					}
					
					if($("#sequencia").val() == ""){
						alert(msgSequencia);
						return false;
					}
					
					if(document.getElementById("exec_auto").checked)
						exec_auto = "S";
					
					var data = { 
							acao:"incluir",
							idacao:$(".idAccoCdAcaocob").val(),
							dsacao:$(".idAccoCdAcaocob option:selected").text(),
							
							idempr:$(".idEmcoCdEmprecob").val(),
							dsempr:$(".idEmcoCdEmprecob option:selected").text(),
							
							idrene:$(".idReneCdRegnegociacao").val(),
							dsrene:$(".idReneCdRegnegociacao option:selected").text(),
							
							idcamp:$(".idCampCdCampanha").val(),
							dscamp:txtCamp,
							
							idpubl:$(".idPublCdPublico").val(),
							dspubl:txtPubl,
							
							exec_auto:exec_auto,
							
							diasuteis:$("#diasuteis").val(),
							sequencia:$("#sequencia").val(),
							acoesViewState : document.getElementById("acoesForm").acoesViewState.value,
							acoesExclusaoViewState : document.getElementById("acoesForm").acoesExclusaoViewState.value
							
					};

					$(".acoes").load("/csiadm/Estrategia.do?userAction=acoes", data, estrategia.acoes.onload_acaolist);
				});
				

				$(".idCampCdCampanha").attr("disabled", true);
				$(".idPublCdPublico").attr("disabled", true);
				
				$(".idAccoCdAcaocob").bind("change", function(event) {
					if($(".idAccoCdAcaocob option:selected").attr("tipo") == "1"){
						$("#exec_auto").attr("disabled", false);
						$(".idCampCdCampanha").attr("disabled", false);
						$(".idCampCdCampanha").bind("change", function(event) {
							if($(".idCampCdCampanha option:selected").attr("dist") != "S"){
								$(".idPublCdPublico").attr("disabled", false);
								
								$(".idPublCdPublico").jsonoptions("/plusoft-eai/generic/consulta-banco", "id_publ_cd_publico", "publ_ds_publico", {	
									"entity" : "br/com/plusoft/csi/adm/dao/xml/CS_CDTB_PUBLICO_PUBL.xml",
									"statement" : "select-by-id-camp",
									"id_camp_cd_campanha" : $(this).val(),
									"id_idio_cd_idioma": idIdioma,
									"type":"json"
								});
								
							}else{
								$(".idPublCdPublico").attr("disabled", true);
								$(".idPublCdPublico").val("");
							}
						});
					}else if($(".idAccoCdAcaocob option:selected").attr("tipo") == "3"){
						$("#exec_auto").attr("disabled", false);
					}else if($(".idAccoCdAcaocob option:selected").attr("tipo") == "4"){
						$("#exec_auto").attr("disabled", false);
					}else{
						$(".idCampCdCampanha").attr("disabled", true);
						$(".idCampCdCampanha").val("");
						$(".idPublCdPublico").val("");
						$("#exec_auto").attr("disabled", true);
					}
					
				});
				
				$(".acoes").find(".lixeira").bind("click", function(event) {
					event.preventDefault();
					event.stopPropagation();
					
					var linhaacao = $(this).parent().parent().attr("indx");
					var idacao = $(this).parent().attr("idacao");
					$.modal.showconfirm(msgExcluir, { buttons: [
         				{ label : "Sim", click : function(){
         						$(".acoes").load("/csiadm/Estrategia.do?userAction=acoes", { acao:"excluir", linha:linhaacao, idacao: idacao, acoesViewState: document.getElementById("acoesForm").acoesViewState.value, acoesExclusaoViewState: document.getElementById("acoesForm").acoesExclusaoViewState.value }, estrategia.acoes.onload_acaolist);
         						$.modal.close();
         					}
         				},
         				{ label : "N�o", click : $.modal.close }
         			] });
					
				});
				
				if($(".mensagemacao").val() != ""){
					alert($(".mensagemacao").val());
				}
				
			}
			
		},
		
		periodicidade : {
			
			//Fun��o para inserir as fun��es dos objetos da aba de periodicidade quando carregada
			onload_periodicidade : function(){
				
				/*$("#dhde").datepicker({
					onSelect : estrategia.periodicidade.validadata	
				});*/
	
				$("#dhate").datepicker({
					onSelect : estrategia.periodicidade.validadata
				});
				
				$("#chkControleManual").bind("click", function() {
					if(document.getElementById("chkControleManual").checked){
						//limpar todos os valores
						$("#horarioexecucao").val("");
						$("#chkUmaVez").attr("disabled", true);
						$("#horarioexecucao").attr("disabled", true);
						$("#chkSeg").attr("disabled", true);
						$("#chkTer").attr("disabled", true);
						$("#chkQua").attr("disabled", true);
						$("#chkQui").attr("disabled", true);
						$("#chkSex").attr("disabled", true);
						$("#chkSab").attr("disabled", true);
						$("#chkDom").attr("disabled", true);
						
						$("#chkUmaVez").attr("checked", false);
						$("#chkSeg").attr("checked", false);
						$("#chkTer").attr("checked", false);
						$("#chkQua").attr("checked", false);
						$("#chkQui").attr("checked", false);
						$("#chkSex").attr("checked", false);
						$("#chkSab").attr("checked", false);
						$("#chkDom").attr("checked", false);
						
					}else{
						$("#chkUmaVez").attr("disabled", false);
						$("#horarioexecucao").attr("disabled", false);
						$("#chkSeg").attr("disabled", false);
						$("#chkTer").attr("disabled", false);
						$("#chkQua").attr("disabled", false);
						$("#chkQui").attr("disabled", false);
						$("#chkSex").attr("disabled", false);
						$("#chkSab").attr("disabled", false);
						$("#chkDom").attr("disabled", false);
						
					}
				});

				$("#chkUmaVez").bind("click", function() {
					if(document.getElementById("chkUmaVez").checked){
						$("#horarioexecucao").attr("disabled", false);
					}else{
						$("#horarioexecucao").attr("disabled", true);
						$("#horarioexecucao").val("");
						
					}
				});
				
			}
			
		},
		
		verificaExecucao : function(idestrcdestrategia){
			
			if(!finalizada){
				$.post("/csiadm/Estrategia.do?userAction=verificaEstrategia", { id : idestrcdestrategia }, function(ret) {
					var texto = "";
					if(ret.msgerro) {
						alert(ret.msgerro);
						return;
					}
					
					if(ret.mensagem != undefined){
						$(".execacao").addClass("disabled").bind("click", function(){});
						
						estrategia.abrirpopup(".popupdet", "350", "450", "Detalhe da execu��o");
						$(".popupdet").loadaguarde();
						
						texto = ret.mensagem;
						texto += '<br>';
						texto += '<br>';
						if(ret.qtdestrategia != undefined){
							texto += ret.qtdestrategia;
						}
						if(ret.qtdacoes != undefined){
							texto += '<br>';
							texto += ret.qtdacoes;
						}
						$(".lblDetExecucao").html(texto);
						
					}else{
						$(".execacao").removeClass("disabled").bind("click", function(event){
							event.preventDefault();
							event.stopPropagation();
							
							$.modal.showconfirm(msgConfirmaExecucao, { buttons: [
								{ label : "Sim", click : function(event) {
									
									estrategia.abrirpopup(".popupdet", "350", "450", "Detalhe da execu��o");
									$(".popupdet").loadaguarde();
									
									$.post("/csiadm/Estrategia.do?userAction=inseriracaoexecucao", { id : $("#idEstrCdEstrategia").val() }, function(ret) {
										finalizada = true;
										if(ret.mensagem != undefined){
											alert(ret.mensagem);
											$(".popupdet").find(".loadaguarde").remove();
										}else{
											alert(ret.msgerro);
											$(".popupdet").find(".loadaguarde").remove();
										}
										
										if(ret.detalhes != undefined){
											texto += '<br>';
											texto += '<br>';
											texto += ret.detalhes;
											$(".lblDetExecucao").html(texto);
										}
										
										$(".popupdet").find(".loadaguarde").remove();
				    				});
									$.modal.close();
								}
								},
								{ label : "N�o", click : $.modal.close }
								] });
							
						});
					}
				});
				
				setTimeout("estrategia.verificaExecucao("+idestrcdestrategia+");",10000);
			}
		}
	}
	
	
}(jQuery));


/**
 * Fun��es executadas ao carregar a tela para controlar o fluxo da tela e a��es dos bot�es
 */
$(document).ready(function() {

	$(".tabframe").inittabs();
	
	var onload_estrlist = function() {

		$("#filtro").bind("keydown", function(event) {
			if(event.which==13) {
				event.preventDefault();
				event.stopPropagation();
				
				$("#filtrar").click();
				return false;
			}
		});
		
		/*
		 * Bot�o Filtar
		 */
		$("#filtrar").bind("click", function(event) {
			event.preventDefault();
			event.stopPropagation();
			
			$("#estrlist").loadaguarde();
			$(".listestrategias").load("/csiadm/Estrategia.do?userAction=buscar", { 
				filtro: $("#filtro").val()
			},onload_estrlist);
		});

		/*
		 * Bot�es excluir da lista na aba Procurar
		 */
		if($("#estrlist tbody .lixeira").length > 0) {
			
			//se tiver registros esconde o div de registros nao encontrado
			$(".nenhumregistro").hide();
			
			$("#estrlist tbody .lixeira").bind("click", function(event) {
				event.preventDefault();
				event.stopPropagation();
				var idestr = $(this).parent().attr("estr");
				$("#estrlist").loadaguarde();
				$.modal.showconfirm(msgExcluir, { buttons: [
     				{ label : "Sim", click : function(){
     					
     						$.post("/csiadm/Estrategia.do?userAction=remover", { id : idestr }, function(ret) {
	     						if(ret.msgerro) {
	     							alert(ret.msgerro);
	     							return;
	     						}else{
	     							window.top.ifrmConteudo.location.href = "/csiadm/Estrategia.do?userAction=init";
	     						}
     						});
     						$.modal.close();
     					}
     				},
     				{ label : "N�o", click : $.modal.close }
     			] });
				
			});
		} 
		/*else {
			$("#estrlist tbody .lixeira").fadeTo('fast', 0.4);
			$("#estrlist tbody .lixeira").attr('disabled', true);
		}*/

		/*
		 * Clique de cada item da lista da aba procurar
		 */
		$(".estrlist tbody tr td").bind("click", function(event) {
			event.preventDefault();
			event.stopPropagation();
			
			$(".divBotoes").show();
			
			var data = { 
					id: $(this).attr("estr")
			};

			$.post("/csiadm/Estrategia.do?userAction=edit", data, function(ret) {

				if(ret.msgerro) {
					alert(ret.msgerro);
					return;
				}

				idestrcdestrategia = ret.id_estr_cd_estrategia;
				estrategia.verificaExecucao(idestrcdestrategia);
				
				$("#idEstrCdEstrategia").val(ret.id_estr_cd_estrategia);
				$("#estrDsEstrategia").val(ret.estr_ds_estrategia);
				$("#estrDhValidadede").val(ret.estr_dh_validadede);
				$("#estrDhValidadeate").val(ret.estr_dh_validadeate);
				$("#estrNrPrioridade").val(ret.estr_nr_sequencia);
				
				$("#notificacaoAssunto").val(ret.estr_ds_notassunto);
				$("#notificacaoPara").val(ret.estr_ds_notpara);
				$("#notificacaoCC").val(ret.estr_ds_notcc);
				
				if(ret.estr_dh_inativo != null && ret.estr_dh_inativo != ""){
					$("#estrInInativo").attr("checked", true);
				}else{
					$("#estrInInativo").removeAttr("checked", false);
				}
				
				//carregar os dados da aba periodicidade
				if(ret.estr_in_contmanual == "S"){
					$("#chkControleManual").attr("selected", true);
					$("#chkControleManual").attr("checked", true);
					
					//desabilitar os campos
					$("#chkUmaVez").attr("disabled", true);
					$("#horarioexecucao").attr("disabled", true);
					$("#chkSeg").attr("disabled", true);
					$("#chkTer").attr("disabled", true);
					$("#chkQua").attr("disabled", true);
					$("#chkQui").attr("disabled", true);
					$("#chkSex").attr("disabled", true);
					$("#chkSab").attr("disabled", true);
					$("#chkDom").attr("disabled", true);
					
				}else{
					$("#chkControleManual").attr("selected", false);
					$("#chkControleManual").attr("checked", false);
					
					$("#chkUmaVez").attr("disabled", false);
					$("#horarioexecucao").attr("disabled", false);
					$("#chkSeg").attr("disabled", false);
					$("#chkTer").attr("disabled", false);
					$("#chkQua").attr("disabled", false);
					$("#chkQui").attr("disabled", false);
					$("#chkSex").attr("disabled", false);
					$("#chkSab").attr("disabled", false);
					$("#chkDom").attr("disabled", false);
					
				}
				
				//$("#dhde").val(ret.estr_dh_execucaode);
				//$("#dhate").val(ret.estr_dh_execucaoate);
				$("#horarioexecucao").val(ret.estr_ds_horaexecucao);
				
				if(ret.estr_in_diario == "S"){
					$("#chkUmaVez").attr("selected", true);
					$("#chkUmaVez").attr("checked", true);
					$("#horarioexecucao").attr("disabled", false);
				}else{
					$("#chkUmaVez").attr("selected", false);
					$("#horarioexecucao").attr("disabled", true);
				}
				
				if(ret.estr_in_segunda == "S"){
					$("#chkSeg").attr("selected", true);
					$("#chkSeg").attr("checked", true);
				}else{
					$("#chkSeg").attr("selected", false);
					$("#chkSeg").attr("checked", false);
				}
				
				if(ret.estr_in_terca == "S"){
					$("#chkTer").attr("selected", true);
					$("#chkTer").attr("checked", true);
				}else{
					$("#chkTer").attr("selected", false);
					$("#chkTer").attr("checked", false);
				}
				
				if(ret.estr_in_quarta == "S"){
					$("#chkQua").attr("selected", true);
					$("#chkQua").attr("checked", true);
				}else{
					$("#chkQua").attr("selected", false);
					$("#chkQua").attr("checked", false);
				}
				
				if(ret.estr_in_quinta == "S"){
					$("#chkQui").attr("selected", true);
					$("#chkQui").attr("checked", true);
				}else{
					$("#chkQui").attr("selected", false);
					$("#chkQui").attr("checked", false);
				}
				
				if(ret.estr_in_sexta == "S"){
					$("#chkSex").attr("selected", true);
					$("#chkSex").attr("checked", true);
				}else{
					$("#chkSex").attr("selected", false);
					$("#chkSex").attr("checked", false);
				}
				
				if(ret.estr_in_sabado == "S"){
					$("#chkSab").attr("selected", true);
					$("#chkSab").attr("checked", true);
				}else{
					$("#chkSab").attr("selected", false);
					$("#chkSab").attr("checked", false);
				}
				
				if(ret.estr_in_domingo == "S"){
					$("#chkDom").attr("selected", true);
					$("#chkDom").attr("checked", true);
				}else{
					$("#chkDom").attr("selected", false);
					$("#chkDom").attr("checked", false);
				}
				
				$("#estrategias").click();
				$("#criterios").click();
				
				$(".criterios").load("/csiadm/Estrategia.do?userAction=criterio", {id:idestrcdestrategia}, function(text, status, xhr) {
					if(status=="ajaxError") {
						alert(text);
					}
					estrategia.criterios.onload_criterio();
				});
				
				$(".acoes").load("/csiadm/Estrategia.do?userAction=acoes", {id:idestrcdestrategia}, function(text, status, xhr) {
					if(status=="ajaxError") {
						alert(text);
					}
					estrategia.acoes.onload_acaolist();
				});
				
				
				$(".btnhistorico").removeClass("disabled").bind("click", function(event){
					event.preventDefault();
					event.stopPropagation();
					
					//estrategia.abrirpopup(".popuphistorico", "450", "780", "Hist�rico da execu��o");
					window.top.showModalDialog("/csiadm/Estrategia.do?userAction=historico&id="+$("#idEstrCdEstrategia").val(), window, 'help:no;scroll:no;Status:NO;dialogWidth:780px;dialogHeight:320px;dialogTop:150px;dialogLeft:150px');
					
					/*$(".listahistorico").load("/csiadm/Estrategia.do?userAction=historico", {id : $("#idEstrCdEstrategia").val()}, function(text, status, xhr) {
						if(status=="ajaxError") {
							alert(text);
						}
						
						
						 * Clique de cada item da lista de historico
						 
						$(".histlist tbody tr td").bind("click", function(event) {
							event.preventDefault();
							var idEstr = $(this).attr("estr");
							var data = { 
									id: idEstr
							};
							
							$(".popuphistorico").dialog( "close" );
							
							$.modal.show("/csiadm/Estrategia.do?userAction=dethistorico", data, "Hist�rico da execu��o", { width:810, height: 550, buttons: [{ label : "Sair", click : function(){ $.modal.close();estrategia.abrirpopup(".popuphistorico", "450", "780", "Hist�rico da execu��o");} }] }, function (){
								
								$(".heaclist").find(".criterioselecao").bind("click", function(event) {
									event.preventDefault();
									$(".comandosql").html($(this).attr("sql")); 
									estrategia.abrirpopup(".popup", "250", "400", "Comando SQL");
								});
								
							});
							
						});
						
					});*/
				});
				
				
				//chama metodo que verifica se a estrategia esta sendo executada no momento
				
				
				
			});
		});

		$("#filtro").focus();
	};
	
	$(".cancelar").bind("click", function(event) {
		event.preventDefault();
		event.stopPropagation();
		
		$.modal.showconfirm(msgCancelar, { width:200, height: 150, buttons: [
			{ label : "Sim", click : function(event) {
										window.top.ifrmConteudo.location.href = "/csiadm/Estrategia.do?userAction=init";
									 }
			},
			{ label : "N�o", click : $.modal.close }
		] });
	
	});
	
	$(".novo").bind("click", function(event) {
		event.preventDefault();
		event.stopPropagation();
		
		$(".telaestrategias").load("/csiadm/EstrategiaEdit.do", null, function(text, status, xhr) {
			if(status=="ajaxError") {
				alert(text);
			}
			novo = true;
			estrategia.onload_estrategia();
		});
	});
	
	$(".gravar").bind("click", function(event) {
		event.preventDefault();
		event.stopPropagation();
		
		if(!estrategia.validagravacao())
			return false;
		
		var contmanual = "N";
		var umavezdia = "N";
		var inativo = "N";
		var seg = "N";
		var ter = "N";
		var qua = "N";
		var qui = "N";
		var sex = "N";
		var sab = "N";
		var dom = "N";
		
		if(document.getElementById("chkControleManual").checked)
			contmanual = "S";
		
		if(document.getElementById("estrInInativo").checked)
			inativo = "S";
		
		if(document.getElementById("chkUmaVez").checked)
			umavezdia = "S";
		
		if(document.getElementById("chkSeg").checked)
			seg = "S";
		
		if(document.getElementById("chkTer").checked)
			ter = "S";
		
		if(document.getElementById("chkQua").checked)
			qua = "S";
		
		if(document.getElementById("chkQui").checked)
			qui = "S";
		
		if(document.getElementById("chkSex").checked)
			sex = "S";
		
		if(document.getElementById("chkSab").checked)
			sab = "S";
		
		if(document.getElementById("chkDom").checked)
			dom = "S";
			
		var data = { 
				id: $("#idEstrCdEstrategia").val(),
				dsestrategia:$("#estrDsEstrategia").val(),
				sequencia:$("#estrNrPrioridade").val(),
				estrdhde:$("#estrDhValidadede").val(),
				estrdhate:$("#estrDhValidadeate").val(),
				inativo:inativo,
				dsnotassunto:$("#notificacaoAssunto").val(),
				dsnotpara:$("#notificacaoPara").val(),
				dsnotcc:$("#notificacaoCC").val(),
				horaexec:$("#horarioexecucao").val(),
				contmanual:contmanual,
				umavezdia:umavezdia,
				seg:seg,
				ter:ter,
				qua:qua,
				qui:qui,
				sex:sex,
				sab:sab,
				dom:dom,
				acoesViewState : document.getElementById("acoesForm").acoesViewState.value,
				acoesExclusaoViewState : document.getElementById("acoesForm").acoesExclusaoViewState.value,
				criteriosViewState : document.getElementById("criteriosForm").criteriosViewState.value,
				criteriosExclusaoViewState : document.getElementById("criteriosForm").criteriosExclusaoViewState.value
		};
		
		$.post("/csiadm/Estrategia.do?userAction=gravar", data, function(ret) {
			if(ret.status=="ajaxError"){
				alert(ret.msgerro);
			}else{
				alert(msgSucesso);
				window.top.ifrmConteudo.location.href = "/csiadm/Estrategia.do?userAction=init";
			}
		});
	});
	
	//adiciona a fun��o de scroll dos bot�es
	$("#setaLeft").bind("click", function(event) {
		document.getElementById("divBotoes").scrollLeft -= 50;
	});
	
	$("#setaRight").bind("click", function(event) {
		document.getElementById("divBotoes").scrollLeft += 50;
	});

	
	//exemplo de bot�o extra
	//estrategia.addButton("/plusoft-resources/images/botoes/agendaCalendario.gif", "btnAteciparAgenda", "Antecipa Agendamentos", "180");
	
	
	$('ul li a').each(function () {
		$(this).bind("click", function(){
			
			if($(this).attr("href")=="#procurartab"){
				$(".divBotoes").hide();
				$(".divSetaDir").hide();
			}else if($(this).attr("href")=="#estrategiastab"){
				$(".divBotoes").show();
				$(".divSetaDir").show();
			}
			
		})
        
    });

	onload_estrlist();
	estrategia.onload_estrategia();
	
	
});