<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>

<title><plusoft:message key="prompt.estrategia.msg.detalhes_execucao" /></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">

<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<body class="tela">

<div class="tabframe frame shadow">
	<div class="title"><plusoft:message key="prompt.estrategia.msg.detalhes_execucao" /></div>
	
	<div class="list">
		<table>
			<thead>
			
				<tr>
					<td class="fix100Center">&nbsp;<plusoft:message key="prompt.estrategia.dataExec"/></td>
					<td class="fix60Center"><plusoft:message key="prompt.estrategia.duracao"/></td>
					<td class="fix60Center"><plusoft:message key="prompt.estrategia.qtd"/></td>
					<td class="fix80Right"><plusoft:message key="prompt.estrategia.valor"/></td>
					<td class="fix90Center"><plusoft:message key="prompt.estrategia.execmanual"/></td>
					<td><plusoft:message key="prompt.estrategia.funcionario"/></td>
				</tr>
		
		
			</thead>
		</table>

		<div class="scrolllist scrolltable histlist">
			<table>
				<tbody>
					<logic:present name="histArray">
						<logic:iterate id="hist" name="histArray">
								<tr class="clickable">
									<td class="fix100Center" hist="<bean:write name="hist" property="field(id_hiex_cd_histexecucao)" />" estr="<bean:write name="hist" property="field(id_estr_cd_estrategia)" />"><bean:write name="hist" property="field(hiex_dh_inicio)" formatKey="prompt.cobranca.dateformat" /></td>
									<td class="fix60Center" hist="<bean:write name="hist" property="field(id_hiex_cd_histexecucao)" />" estr="<bean:write name="hist" property="field(id_estr_cd_estrategia)" />"><bean:write name="hist" property="field(duracao)" />(s)</td>
									<td class="fix60Center" hist="<bean:write name="hist" property="field(id_hiex_cd_histexecucao)" />" estr="<bean:write name="hist" property="field(id_estr_cd_estrategia)" />"><bean:write name="hist" property="field(qtd)" /></td>
									<td class="fix80Right" hist="<bean:write name="hist" property="field(id_hiex_cd_histexecucao)" />" estr="<bean:write name="hist" property="field(id_estr_cd_estrategia)" />"><bean:write name="hist" property="field(valor)" format="##,###,##0.00" /></td>
									<td class="fix90Center" hist="<bean:write name="hist" property="field(id_hiex_cd_histexecucao)" />" estr="<bean:write name="hist" property="field(id_estr_cd_estrategia)" />"><bean:write name="hist" property="field(hiex_in_execmanual)" /></td>
									<td hist="<bean:write name="hist" property="field(id_hiex_cd_histexecucao)" />" estr="<bean:write name="hist" property="field(id_estr_cd_estrategia)" />"><bean:write name="hist" property="acronymHTML(func_nm_funcionario, 15)" filter="html" /></td>
								</tr>
						</logic:iterate>
					</logic:present>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="right margin">
	<a href="#" class="clickable btnsair" title="<plusoft:message key="prompt.sair" />"><img src="/plusoft-resources/images/botoes/out.gif" /></a>
</div>
	<link rel="stylesheet" href="/csiadm/webFiles/cobranca/estrategia/estrategia.css">
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>

<script>

$(".histlist tbody tr td").bind("click", function(event) {
	event.preventDefault();
	var idEstr = $(this).attr("estr");
	var idHist = $(this).attr("hist");
	
	
	window.dialogArguments.top.showModalDialog("/csiadm/Estrategia.do?userAction=dethistorico&id="+idEstr+"&idHist="+idHist, window, 'help:no;scroll:no;Status:NO;dialogWidth:810px;dialogHeight:510px;dialogTop:150px;dialogLeft:150px');
	
});


$(".btnsair").bind("click", function(event) {
	event.preventDefault();
	window.close();
});
	
</script>

</body>
</html:html>