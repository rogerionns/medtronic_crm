<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="contenttabperiodicidade formtab">

	<table class="tableperiodicidade" style="width:600px;">
		<tr>
			<td class="formlabel">Controle Manual</td>
			<td class="seta"></td>
			<td><input type="checkbox" id="chkControleManual"/></td>
		</tr>
		<!-- <tr>
			<td colspan="3">
				<table>
					<tr>
						<td>Execu��o a partir: De</label></td>
						<td class="seta"></td>
						<td><input type="text" class="text date left" id="dhde" maxlength="10" /></td>
						<td width="35px" align="center">At�</td>
						<td class="seta"></td>
						<td><input type="text" class="text date left" id="dhate" maxlength="10" /></td>
					</tr>
				</table>
			</td>
		</tr> -->
		<tr>
			<td class="formlabel">Uma vez ao dia</td>
			<td class="seta"></td>
			<td><input type="checkbox" id="chkUmaVez" /></td>
		</tr>
		<tr>
			<td class="formlabel">Hor�rio</td>
			<td class="seta"></td>
			<td><input type="text" class="numeric text" id="horarioexecucao" disabled="true"/></td>
		</tr>
		<tr>
			<td class="formlabel">Dia da semana</td>
			<td class="seta"></td>
			<td ><input type="checkbox" id="chkSeg" /> Segunda &nbsp;&nbsp; <input type="checkbox" id="chkTer" /> Ter�a &nbsp;&nbsp; <input type="checkbox" id="chkQua" /> Quarta &nbsp;&nbsp; 
			<input type="checkbox" id="chkQui" /> Quinta &nbsp;&nbsp; <input type="checkbox" id="chkSex" /> Sexta &nbsp;&nbsp; <input type="checkbox" id="chkSab" /> S�bado &nbsp;&nbsp; <input type="checkbox" id="chkDom" /> Domingo</td>
		</tr>
	</table>
	
	<logic:present name="msgerro">
		<script type="text/javascript">
			$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
		</script>
	</logic:present>
	
</div>


