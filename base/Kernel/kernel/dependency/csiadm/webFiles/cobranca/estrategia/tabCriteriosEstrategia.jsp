<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="contenttab">
	<table>
		<tr>
			<td style="width:40px"><label for="estrNrPrioridade">Vis�o</label></td>
			<td class="seta"></td>
			<td style="width:250px">
				<select class="idVisaCdVisao">
					<option value="">-- Selecione uma op��o--</option>
					<logic:present name="csCdtbVisaoVisaVector"><logic:iterate id="visaoVo" name="csCdtbVisaoVisaVector">
					<option value="<bean:write name="visaoVo" property="field(id_visa_cd_visao)" />"><bean:write name="visaoVo" property="field(visa_ds_visao)" /></option>
					</logic:iterate></logic:present>
				</select>
			</td>
			<td colspan="3">
				<a href="#" class="atualizarvisao left" title="<plusoft:message key="prompt.estrategia.atualizarvisao" />"><img src="/plusoft-resources/images/icones/historico.gif" /></a>
				<a href="#" class="listarvisao left" title="<plusoft:message key="prompt.estrategia.listar" />"><img src="/plusoft-resources/images/botoes/setaDown.gif" /></a>
			</td>
		</tr>
	</table>
	
	<div class="list borda listacriterios">
		<table>
			<thead>
			
				<tr>
					<td class="imageCab">&nbsp;</td>
					<td>&nbsp;<plusoft:message key="prompt.estrategia.tab.criterios.criterios"/></td>
					<td class="fix200Center"><plusoft:message key="prompt.estrategia.tab.criterios.registrosAlocados"/></td>
					<td class="valor"><plusoft:message key="prompt.estrategia.tab.criterios.valorCobranca"/></td>
					<td class="fix40Center">&nbsp;</td>
					<td class="fix60Center">&nbsp;</td>
				</tr>
		
		
			</thead>
		</table>
	
		<logic:present name="criteriosViewState">
			
			<div class="scrolllist scrolltablecriterios">
				<table id="tablecriterios">
					<tbody>
						<logic:iterate id="criterios" name="criteriosViewState" indexId="i">
							<logic:notEmpty name="criterios" property="field(crit_ds_nome)">
								<tr indx="<bean:write name="i" />" class="geralCursoHand">
									
									<td idcrit="<bean:write name="criterios" property="field(id_crit_cd_criterio)" />" class="image"><a href="#" class="lixeira" title="Excluir"></a></td>
									<td><bean:write name="criterios" property="field(crit_ds_nome)" /></td>
									<td class="fix200Center"><bean:write name="criterios" property="field(crit_nr_registros)" /></td>
									<td class="valor"><bean:write name="criterios" property="field(crit_vl_valor)" format="##,###,##0.00" /></td>
									<td class="fix40Center"><a href="#" sql="<bean:write name="criterios" property="field(crit_ds_comandosql)" />" class="criterioselecao" title="<plusoft:message key="prompt.estrategia.criterios.criterioSelecao" />"><img src="/plusoft-resources/images/botoes/lupa.gif" /></a></td>
									<td class="fix60Center" idestr="<bean:write name="criterios" property="field(id_estr_cd_estrategia)" />" idvisa="<bean:write name="criterios" property="field(id_visa_cd_visao)" />" idcrit="<bean:write name="criterios" property="field(id_crit_cd_criterio)" />"><a href="#" class="refresh" style="float:left" title="Atualizar Valor"></a></td>
								</tr>
							</logic:notEmpty>
							<input type="hidden" name="mensagemCamposObrigatorios" class="mensagemCamposObrigatorios" value="<bean:write name="criterios" property="field(possuiTodosCamposObrigatorios)"/>" />
						</logic:iterate>
						<!--tr class="nenhumregistrocriterio" height="120px">
							<td>
								 <div align="center" class="principalLabel" style="position:absolute; width:90%; height:10px; z-index:7; ">
									<b><plusoft:message key="lista.nenhum.registro.encontrado"/></b> 
								 </div>
						 	</td>
						 </tr-->
					</tbody>
				</table>
			</div>
		
		</logic:present>
	</div>
	
	<logic:present name="totaisCriterioViewState">
		<logic:iterate id="totaisCriterio" name="totaisCriterioViewState">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td align="right"><b><plusoft:message key="estrategiaForm.total"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></b></td>
					<td width="10%" align="left"><b>&nbsp;<bean:write name="totaisCriterio" property="field(totalRegistro)"/></b></td>
					<td width="20%" align="right"><b>R$ <bean:write name="totaisCriterio" property="field(totalValor)" format="##,###,##0.00"/></b></td>
					<td width="10%"  align="center">&nbsp;</td>
				</tr>
			</table>
		</logic:iterate>
	</logic:present>
	
	
	<div class="popup" style="display: none;">
		<div class="frame comandosql"></div>
	</div>
	

	<input type="hidden" name="mensagemcriterio" class="mensagemcriterio" value='<%=request.getAttribute("mensagem")%>'/>	
	<logic:present name="msgerro">
		<script type="text/javascript">
			$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
		</script>
	</logic:present>
	
	<html:form styleId="criteriosForm">
		<html:hidden property="criteriosViewState" styleId="criteriosViewState" />
		<html:hidden property="totaisCriterioViewState" />
		<html:hidden property="criteriosExclusaoViewState" />
	</html:form>
	
</div>


