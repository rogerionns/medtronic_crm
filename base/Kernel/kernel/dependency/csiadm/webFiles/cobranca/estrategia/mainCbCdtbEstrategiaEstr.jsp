<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>

<title><plusoft:message key="prompt.estrategia.titleTelaEstrategia" /></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">

<link rel="stylesheet" href="/plusoft-resources/css/global.css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<%
	long idIdioCdIdioma = 0;
	CsCdtbFuncionarioFuncVo ccffVo = request.getSession() != null && request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null ? (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo") : null;
	
	if(ccffVo != null) {
		idIdioCdIdioma = ccffVo.getIdIdioCdIdioma(); 
	}
%>

<script>
	var idIdioma = '<%=idIdioCdIdioma%>';

	var msgExcluir = "<plusoft:message key='prompt.Deseja_excluir_esse_registro' />";
	
	var msgCancelar = "<plusoft:message key='prompt.DesejaCancelar' />";
	var msgSucesso =  "<plusoft:message key='prompt.dados_gravados_com_sucesso' />";
	
	var descricao = "<plusoft:message key='prompt.estrategia.msg.descricao' />";
	var dtValidade =  "<plusoft:message key='prompt.estrategia.msg.dtValidade' />";
	var prioridade =  "<plusoft:message key='prompt.estrategia.msg.prioridade' />";
	
	var dtExecucao =  "<plusoft:message key='prompt.estrategia.msg.periodicidade.execucao' />";
	var horario =  "<plusoft:message key='prompt.estrategia.msg.periodicidade.horario' />";
	var umavez =  "<plusoft:message key='prompt.estrategia.msg.periodicidade.umavez' />";
	var diaSemana =  "<plusoft:message key='prompt.estrategia.msg.periodicidade.dia' />";
	var contmanual =  "<plusoft:message key='prompt.estrategia.msg.periodicidade.contmanual' />";
	
	var msgCriterios = "<plusoft:message key='prompt.estrategia.msg.criterios' />";
	var msgAcoes = "<plusoft:message key='prompt.estrategia.msg.acoes' />";
	
	var msgCmdAcoes = "<plusoft:message key='prompt.estrategia.msg.acoes.acoes' />";
	var msgCmdEmpresa = "<plusoft:message key='prompt.estrategia.msg.acoes.empresa' />";
	var msgCmdGrupo = "<plusoft:message key='prompt.estrategia.msg.acoes.gruponegociacao' />";
	var msgDias = "<plusoft:message key='prompt.estrategia.msg.acoes.dias' />";
	var msgSequencia = "<plusoft:message key='prompt.estrategia.msg.acoes.sequencia' />";
	
	var msgCampanha = "<plusoft:message key='prompt.estrategia.msg.acoes.campanha' />";
	var msgSubCampanha = "<plusoft:message key='prompt.estrategia.msg.acoes.subcampanha' />";
	var msgGrupoNegociacao = "<plusoft:message key='prompt.estrategia.msg.acoes.gruponegociacao' />";
	
	var msgConfirmaExecucao = "<plusoft:message key='prompt.estrategia.msg.confirma_execucao_estrategia' />";
	var msgTTRegistroProc = "<plusoft:message key='prompt.estrategia.msg.total_regitros_processados' />";
	
	var tituloHistorico = "<plusoft:message key='prompt.estrategia.msg.detalhes_execucao' />";
	
	if(<%=request.getAttribute("msgerro")%> != null && '<%=request.getAttribute("msgerro")%>' != ""){
		alert('<%=request.getAttribute("msgerro")%>');
	}
</script>

</head>

<body class="margin topmargin noscroll">
	
	<div class="tabframe frame shadow">
		<div class="title"><plusoft:message key="prompt.estrategia.titleTelaEstrategia" /></div>
		<br>	
		<div class="tabtabs">
			<ul>
				<li id="procurar" class="tabproc"><div class="txtproc"><plusoft:message key="prompt.estrategia.tab.procurar" /></div></li>
				<li id="estrategias" class="tabestr"><div class="txtestr"><plusoft:message key="prompt.estrategia.tab.estrategias" /></div></li>
			</ul>
		</div>

		<div class="tabcontent">
			<div id="procurartab">
				<div class="listestrategias">
					<jsp:include page="administracaoCbCdtbEstrategiaEstr.jsp" />
				</div>
			</div>
			
			<div id="estrategiastab">
				<div class="telaestrategias">
					<jsp:include page="/EstrategiaEdit.do" flush="true"/>
				</div>
			</div>
		</div>
		
		<div class="divBotoes" id="divBotoes">
			<div class="btnsEspe" id="btnsEspec">
			</div>
		</div>
		
		<div class="divSetaDir" style="float: left; margin: 0;">
			<img id="setaLeft" src="/plusoft-resources/images/botoes/esq.gif" style="float:left; cursor: pointer;">
			<img id="setaRight" src="/plusoft-resources/images/botoes/dir.gif" style="float:left; cursor: pointer;">
		</div>	

		<div class="btnDiv" style="float: right; margin: 10px;">
			<a href="#" title="Novo" class="margin left novo"></a>
			<a href="#" title="Gravar" class="margin left gravar"></a>
			<a href="#" title="Cancelar" class="margin left cancelar"></a>
		</div>
		<br>
		<br>
		<br>
	</div>
	
	<link rel="stylesheet" href="/csiadm/webFiles/cobranca/estrategia/estrategia.css">
	<logic:present name="estrategiaCSS"><link rel="stylesheet" href="<bean:write name='estrategiaCSS' />/estrategia.css"></logic:present>

	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	
	<script type="text/javascript" src="/csiadm/webFiles/cobranca/estrategia/estrategia.js"></script>
	<logic:present name="estrategiaJS"><script type="text/javascript" src="<bean:write name='estrategiaJS' />estrategia.js"></script></logic:present>
	

</body>
</html:html>
