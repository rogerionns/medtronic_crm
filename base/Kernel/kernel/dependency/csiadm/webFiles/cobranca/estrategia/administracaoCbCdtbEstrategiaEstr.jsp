<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<div class="formrow">

	<table width="650px">
		<thead>
		<tr>
			<td><label for="estrDsEstrategia"><plusoft:message key="prompt.procurar"/></label></td>
			<td width="40px">&nbsp;</td>
		</tr>
		</thead>
		<tr>
			<td>
				<input type="text" class="text" id="filtro" maxlength="1000" />
			</td>
			<td width="40px"><a href="#" class="setadown" id="filtrar" title="Filtrar"></a></td>
		</tr>
	</table>
</div>
<hr/>

<div class="list margin" id="estrlist" name="estrlist">
	<table>
		<thead>
			<tr>
				<td class="sph">&nbsp;</td>
				<td class="fix60"><plusoft:message key="prompt.cod"/></td>
				<td><plusoft:message key="prompt.descricao"/></td>
				<td class="fix60Center"><plusoft:message key="prompt.prioridade"/></td>
				<td class="fix200Center"><plusoft:message key="prompt.inativo"/></td>
			</tr>
		</thead>
	</table>
	
	<div class="scrolllist estrlist">
		<table><tbody>
			<logic:present name="estrlist"><logic:iterate name="estrlist" id="estr">
			<tr class="clickable">
				<td estr="<bean:write name="estr" property="field(id_estr_cd_estrategia)" />" class="image"><a href="#" class="lixeira" title="Excluir"></a></td>
				<td estr="<bean:write name="estr" property="field(id_estr_cd_estrategia)" />" class="fix60"><bean:write name="estr" property="field(id_estr_cd_estrategia)" /></td>
				<td estr="<bean:write name="estr" property="field(id_estr_cd_estrategia)" />"><bean:write name="estr" property="field(estr_ds_estrategia)" /></td>
				<td estr="<bean:write name="estr" property="field(id_estr_cd_estrategia)" />" class="fix60Center"><bean:write name="estr" property="field(estr_nr_sequencia)" /></td>
				<td estr="<bean:write name="estr" property="field(id_estr_cd_estrategia)" />" class="fix200Center"><bean:write name="estr" property="field(estr_dh_inativo)" formatKey="prompt.cobranca.datetimeformat" /></td>
			</tr>
			</logic:iterate></logic:present>
			 <div class="nenhumregistro" align="center" class="principalLabel" style="position:absolute; width:90%; top:300px; z-index:9; ">
				<b><bean:message key="lista.nenhum.registro.encontrado"/></b> 
			 </div>
		</tbody></table>
	</div>
</div>
