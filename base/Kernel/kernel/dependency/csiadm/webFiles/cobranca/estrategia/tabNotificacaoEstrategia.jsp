<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="contenttab formtab">

	<table class="tablenotificacao">
		<tr>
			<td class="formlabel">Assunto</td>
			<td class="seta"></td>
			<td><input type="text" class="campoDescricao" maxlength="200" id="notificacaoAssunto"/></td>
		</tr>
		<tr>
			<td class="formlabel">Para</td>
			<td class="seta"></td>
			<td><input type="text" class="campoDescricao" id="notificacaoPara"/></td>
		</tr>
		<tr>
			<td class="formlabel">CC</td>
			<td class="seta"></td>
			<td><input type="text" class="campoDescricao" id="notificacaoCC"/></td>
		</tr>
	</table>
	
	<logic:present name="msgerro">
		<script type="text/javascript">
			$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
		</script>
	</logic:present>
	
</div>


