<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/cobranca/js/pt/TratarDados.js"></script>
<script language="JavaScript" src="../plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script language="JavaScript" src="../plusoft-resources/javascripts/consultaBanco.js"></script>
<script language="JavaScript">

	var mesmoBanco = 'S';
	
	function duplicar() {
		if(document.forms[0].id_empr_cd_empresa_nova.value == "" && document.forms[0].id_empr_cd_empresa_nova.value < 1){
			alert("Selecionar a Empresa de Destino!");
			return false;
		}

		if(document.forms[0].id_laou_cd_sequencial.value == "" && document.forms[0].id_laou_cd_sequencial.value < 1){
			alert("Selecionar o Layout a ser copiado!");
			return false;

		}
		
		var wi = (window.dialogArguments?window.dialogArguments:window.opener);
		
		document.forms[0].id_pai_cd_sequencial.value = wi.document.forms[0].id_laou_cd_sequencial.value; 
		document.forms[0].target = this.name = 'duplicaLayout';
		document.forms[0].action = 'DuplicarLayout.do?mesmo_banco=' + mesmoBanco;
		document.getElementById('aguarde').style.visibility = 'visible';
		document.forms[0].submit();
	}

	function chamaComboEmpresa(bco){
		mesmoBanco = bco;
		var ajax = new ConsultaBanco("","CarregaComboEmpresa.do");	
		ajax.addField("mesmo_banco",mesmoBanco);
		ajax.executarConsulta(carregaComboEmpresa, false, true);
	}

	function carregaComboEmpresa(ajax){
		ajax.popularCombo(document.forms[0].id_empr_cd_empresa_nova, "id_empr_cd_empresa", "empr_ds_empresa", "", false, "");
	}
	
	function chamaComboLayout(id){
		var ajax = new ConsultaBanco("","CarregaComboLayout.do");	
		ajax.addField("id_empr_cd_empresa_nova",id);
		ajax.addField("mesmo_banco",mesmoBanco);	
		ajax.executarConsulta(carregaComboLayout, true, true);
	}

	function carregaComboLayout(ajax){
		ajax.popularCombo(document.getElementById("id_laou_cd_sequencial"), "id_laou_cd_sequencial", "laou_ds_layout", "", false, "");
	}
	function inicio(){
		document.forms[0].target = this.name = 'duplicaLayout';
		document.forms[0].action = '';
		document.all.item('aguarde').style.visibility = 'hidden';

	}
	
</script>
</head>
<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" scroll="no">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro">&nbsp;Copiar Layout</td>
          <td class="principalQuadroPstVazia">&nbsp;</td>
          <td heigt="1" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" width="100%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td valign="top">&nbsp;<html:form action="AbreDuplicarLayout.do" styleId="layoutImportacaoForm"> 
<input type="hidden" name="id_pai_cd_sequencial" />

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="41%" class="principalLabel" align="right">Mesmo Banco 
                  de Dados ? <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="5%" class="principalLabel">Sim </td>
                <td width="9%" class="principalLabel"> 
                  <input type="radio" name="optBanco" value="0" onclick="chamaComboEmpresa('S')"></input> 
                </td>
                <td width="5%" class="principalLabel">N�o </td>
                <td width="9%" class="principalLabel"> 
                  <input type="radio" name="optBanco" value="1" onclick="chamaComboEmpresa('N')"></input> 
                </td>
                <td width="31%" class="principalLabel"> ( plusoftcrm/depara )</td>
              </tr>
            </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
	            <td width="41%" class="EspacoPequeno">&nbsp;</td>
	            <td width="44%" class="EspacoPequeno">&nbsp;</td>
	            <td width="15%" class="EspacoPequeno">&nbsp;</td>
	</tr>
	<tr> 
		        <td width="41%" class="principalLabel" align="right">Empresa <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
		        <td width="44%"> <html:select property="id_empr_cd_empresa_nova" styleClass="principalObjForm" onchange="chamaComboLayout(this.value)"> 
                  </html:select> </td>
		        <td width="15%">&nbsp;</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
	            <td width="41%" class="EspacoPequeno">&nbsp;</td>
	            <td width="44%" class="EspacoPequeno">&nbsp;</td>
	            <td width="15%" class="EspacoPequeno">&nbsp;</td>
	</tr>
	<tr> 
		        <td width="41%" class="principalLabel" align="right">Copiar p/ o  
                  <bean:message key="prompt.layout"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
		        <td width="44%"> <html:select property="id_laou_cd_sequencial" styleId="id_laou_cd_sequencial" styleClass="principalObjForm"> 
                  </html:select> </td>
		        <td width="15%">&nbsp;</td>
	</tr>
</table>
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
			<td width="25" align="center">
				<img src="webFiles/images/botoes/Acao_Programa.gif" alt="Duplicar Layout" name="imgDuplicar" id="imgDuplicar" width="20" height="20" class="geralCursoHand" onclick="duplicar();">
			</td>

		</tr>
      </table>

</html:form>
</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td height="1" width="4px"><img src="webFiles/images/linhas/VertSombra.gif" height="100%" width="4"></td>
  </tr>
  <tr> 
    <td height="4"><img height="4" width="100%" src="webFiles/images/linhas/horSombra.gif"></td>
    <td height="4" width="4px"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="96%">&nbsp;</td>
    <td width="4%" align="center"><img src="webFiles/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" title="Sair" onclick="javascript:window.close();"></td>
  </tr>
</table>
<div id="aguarde" style="position:absolute; left:90px; top:30px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>