<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 
String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/parametroRemuneracao.js"></script>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/date-picker.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
</head>

<body class="principalBgrPage" text="#000000" onload="fecharAguarde();showError('<%=request.getAttribute("msgerro")%>');AtivarPasta('<bean:write name="parametrosRemuneracaoForm" property="flagAba"/>');verificaCampos('<bean:write name="parametrosRemuneracaoForm" property="flagAba"/>','<bean:write name="parametrosRemuneracaoForm" property="flagHabilitaCampos"/>');inicio();">
<html:form action="/parametroRemuneracao" method="post">
<html:hidden property="userAction"/>
<html:hidden property="alterando"/>
<html:hidden property="idPareCdParamremunera"/>

<input type="hidden" name="idParamremunera" value=""/>
<input type="hidden" name="idEmpresaCobranca" value=""/>
<input type="hidden" name="idEmcoEmpreco" value=""/>
<input type="hidden" name="emcoEmpresa" value=""/>
<input type="hidden" name="pareInativo" value=""/>
<input type="hidden" name="assuntoNivel1" value=""/>
<input type="hidden" name="assuntoNivel2" value=""/>
<input type="hidden" name="pareValidadede" value=""/>
<input type="hidden" name="pareValidadeate" value=""/>
<input type="hidden" name="pareDiasatrasode" value=""/>
<input type="hidden" name="pareDiasatrasoate" value=""/>
<input type="hidden" name="pareNegociadode" value=""/>
<input type="hidden" name="pareNegociadoate" value=""/>
<input type="hidden" name="idFopgFormapagto" value=""/>
<input type="hidden" name="pareParcelasde" value=""/>
<input type="hidden" name="pareParcelasate" value=""/>
<input type="hidden" name="paraPercentmetade" value=""/>
<input type="hidden" name="paraPercentmetaate" value=""/>
<input type="hidden" name="idCondCondicao" value=""/>
<input type="hidden" name="paraCalculosobre" value=""/>
<input type="hidden" name="paraMulta" value=""/>
<input type="hidden" name="paraJuros" value=""/>
<input type="hidden" name="paraMora" value=""/>
<input type="hidden" name="parePercenthonorarios" value=""/>
<input type="hidden" name="parePercentdespendido" value=""/>
<input type="hidden" name="paraFixo" value=""/>
<input type="hidden" name="checkMora" value=""/>
<input type="hidden" name="checkMulta" value=""/>
<input type="hidden" name="checkJuros" value=""/>
<input type="hidden" name="checkInativo" value=""/>

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroGrd" height="17" width="166"><bean:message key="parametrosRemuneracao.title.descricao"/></td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="540"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado" name="tdProcurar" id="tdProcurar" onClick="AtivarPasta('pesquisar');"><bean:message key="parametrosRemuneracao.janela.procurar"/></td>
                <td class="principalPstQuadroLinkNormalMaior" name="tdEstrategias" id="tdEstrategias" onClick="AtivarPasta('editar');"><bean:message key="parametrosRemuneracao.janela.parametrosRemunerecao"/></td>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr> 
                <td height="440" valign="top"> 
                  <div id="Procurar" style="position:absolute; width:99%; height:380px; z-index:1; visibility: visible;"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="principalLabel"><bean:message key="parametrosRemuneracaoForm.descricao"/></td>
                      </tr>
                      <tr> 
                        <td width="56%"> 
                          <html:text property="paramremuneraParePesquisa" name="parametrosRemuneracaoForm" styleClass="principalObjForm" maxlength="60"/>
                        </td>
                        <td width="44%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="pesquisar();" alt="Pesquisar"></td>
                      </tr>
                      <tr> 
                        <td width="56%">&nbsp;</td>
                        <td width="44%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2"> 
                        <div style="height: 300px; overflow: auto;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLstCab" width="2%">&nbsp;</td>
                              <td class="principalLstCab" width="36%"><bean:message key="parametrosRemuneracao.lista.empresaCobranca"/></td>
                              <td class="principalLstCab" width="22%"><bean:message key="parametrosRemuneracao.lista.produto"/></td>
                              <td class="principalLstCab" width="19%"><bean:message key="parametrosRemuneracao.lista.validade"/></td>
                              <td class="principalLstCab" width="21%"><bean:message key="parametrosRemuneracao.lista.inativo"/></td>
                            </tr>
                            <logic:notEmpty name="parametroRemuneracao">
                            	<logic:iterate name="parametroRemuneracao" id="parametroRemuneracao" indexId="indice">
                            		 <tr class="geralCursoHand"> 
                            			  
                            			 <td class="principalLstPar" width="4%" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" onclick="excluir('<bean:write name="parametroRemuneracao" property="field(ID_PARE_CD_PARAMREMUNERA)"/>');" width="14" height="14" class="geralCursoHand" alt="Excluir"/></td>
                            			 
                            			 <td class="principalLstPar" width="36%" onclick="alterar('<bean:write name="parametroRemuneracao" property="field(ID_PARE_CD_PARAMREMUNERA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_EMCO_CD_EMPRECOB)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(EMCO_DS_EMPRESA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_INATIVO)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_ASN1_ASSUNTONIVEL1_ASN1)"/>',
		                              														      '<bean:write name="parametroRemuneracao" property="field(ID_ASN2_ASSUNTONIVEL2_ASN2)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_VALIDADEDE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_VALIDADEATE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_DIASATRASODE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_DIASATRASOATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_NEGOCIADODE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_NEGOCIADOATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_FOPG_CD_FORMAPAGTO)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_PARCELASDE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_PARCELASATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_PERCENTMETADE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_PERCENTMETAATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_COND_CD_CONDICAO)"/>',
                          			 													  		  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_CALCULOSOBRE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_MULTA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_JUROS)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_MORA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_PERCENTHONORARIOS)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_PERCENTDESPENDIDO)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_FIXO)"/>');">&nbsp;<bean:write name="parametroRemuneracao" property="field(EMCO_DS_EMPRESA)"/></td>
                            			 														  
                            			 <td class="principalLstPar" width="22%" onclick="alterar('<bean:write name="parametroRemuneracao" property="field(ID_PARE_CD_PARAMREMUNERA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_EMCO_CD_EMPRECOB)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(EMCO_DS_EMPRESA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_INATIVO)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_ASN1_ASSUNTONIVEL1_ASN1)"/>',
		                              														   	  '<bean:write name="parametroRemuneracao" property="field(ID_ASN2_ASSUNTONIVEL2_ASN2)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_VALIDADEDE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_VALIDADEATE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_DIASATRASODE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_DIASATRASOATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_NEGOCIADODE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_NEGOCIADOATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_FOPG_CD_FORMAPAGTO)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_PARCELASDE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_PARCELASATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_PERCENTMETADE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_PERCENTMETAATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_COND_CD_CONDICAO)"/>',
                          			 													  		  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_CALCULOSOBRE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_MULTA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_JUROS)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_MORA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_PERCENTHONORARIOS)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_PERCENTDESPENDIDO)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_FIXO)"/>');">&nbsp;<bean:write name="parametroRemuneracao" property="field(PRAS_DS_PRODUTOASSUNTO)"/></td>
                            			 														  
										<td class="principalLstPar" width="19%" onclick="alterar( '<bean:write name="parametroRemuneracao" property="field(ID_PARE_CD_PARAMREMUNERA)"/>',
																								  '<bean:write name="parametroRemuneracao" property="field(ID_EMCO_CD_EMPRECOB)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(EMCO_DS_EMPRESA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_INATIVO)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_ASN1_ASSUNTONIVEL1_ASN1)"/>',
		                              														   	  '<bean:write name="parametroRemuneracao" property="field(ID_ASN2_ASSUNTONIVEL2_ASN2)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_VALIDADEDE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_VALIDADEATE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_DIASATRASODE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_DIASATRASOATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_NEGOCIADODE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_NEGOCIADOATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_FOPG_CD_FORMAPAGTO)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_PARCELASDE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_PARCELASATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_PERCENTMETADE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_PERCENTMETAATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_COND_CD_CONDICAO)"/>',
                          			 													  		  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_CALCULOSOBRE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_MULTA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_JUROS)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_MORA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_PERCENTHONORARIOS)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_PERCENTDESPENDIDO)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_FIXO)"/>');">&nbsp;<bean:write name="parametroRemuneracao" property="field(PARE_DH_VALIDADEDE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html" />&nbsp;-&nbsp;<bean:write name="parametroRemuneracao" property="field(PARE_DH_VALIDADEATE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html" /></td>
                            			 														  
										<td class="principalLstPar" width="21%" onclick="alterar( '<bean:write name="parametroRemuneracao" property="field(ID_PARE_CD_PARAMREMUNERA)"/>',
																								  '<bean:write name="parametroRemuneracao" property="field(ID_EMCO_CD_EMPRECOB)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(EMCO_DS_EMPRESA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_INATIVO)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_ASN1_ASSUNTONIVEL1_ASN1)"/>',
		                              														   	  '<bean:write name="parametroRemuneracao" property="field(ID_ASN2_ASSUNTONIVEL2_ASN2)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_VALIDADEDE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_DH_VALIDADEATE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_DIASATRASODE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_DIASATRASOATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_NEGOCIADODE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_NEGOCIADOATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_FOPG_CD_FORMAPAGTO)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_PARCELASDE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_NR_PARCELASATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_PERCENTMETADE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_PERCENTMETAATE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(ID_COND_CD_CONDICAO)"/>',
                          			 													  		  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_CALCULOSOBRE)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_MULTA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_JUROS)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_IN_MORA)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_PERCENTHONORARIOS)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARE_VL_PERCENTDESPENDIDO)"/>',
                            			 														  '<bean:write name="parametroRemuneracao" property="field(PARA_VL_FIXO)"/>');">&nbsp;<bean:write name="parametroRemuneracao" property="field(EMCO_DH_INATIVO)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html" /></td>
                            			 														                              			 														                              			 														  														  
                            		</tr>
                            		  <script>
		                            	countRegistro++;
		                            </script>
                            	</logic:iterate>
                            </logic:notEmpty>
							<logic:empty name="parametroRemuneracao">
								<tr> 
									<td align="center" colspan="7" > 
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
											<tr id="nenhumRegistro" ><td height="260" width="800" align="center"  class="principalLstPar"><br><b><bean:message key="lista.nenhum.registro.encontrado"/> </b></td></tr>
										</table>
									&nbsp;</td>
								</tr>
							</logic:empty>                            
                          </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                    
                  </div>
                  <div id="Estrategias" style="position:absolute; width:99%; height:399px; z-index:2; visibility: hidden;"> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLabel" align="right" width="26%">&nbsp;</td>
                        <td width="8%" class="principalLabel">&nbsp;</td>
                        <td class="principalLabel" colspan="3">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="parametrosRemuneracaoForm.empresaCobranca"/>
                        	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                                <html:select property="idEmcoCdEmpreco" name="parametrosRemuneracaoForm" styleClass="principalObjForm">
                            		<html:option value="0">-- Selecione uma Op��o --</html:option>
                            		<logic:notEmpty name="cbCdtbEmprecobEmco">
                            			<bean:define name="cbCdtbEmprecobEmco" id="empresaVO" />
                            			<html:options collection="empresaVO"  property="field(ID_EMCO_CD_EMPRECOB)" labelProperty="field(EMCO_DS_EMPRESA)"/>
                            		</logic:notEmpty>
                            </html:select>	
                              </td>
                              <td width="27%" class="principalLabel" align="center"> 
                               <logic:equal value="" name="parametrosRemuneracaoForm" property="pareDhInativo">
								<input type="checkbox" name="checkado" value="ativo"/>                              	                              	
							</logic:equal>
							<logic:notEqual value="" name="parametrosRemuneracaoForm" property="pareDhInativo">                                
								<input type="checkbox" name="checkado" value="inativo" checked="checked"/>                              	
							</logic:notEqual> 	                              	          
							<bean:message key="parametrosRemuneracaoForm.inativo"/>
							</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                    </table>
					<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" class="EspacoPequeno">
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
                      <tr> 
                        <td width="1007" colspan="2"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalPstQuadro" height="17" width="166"><bean:message key="parametrosRemuneracao.lista.criterio"/></td> 
                                
                              <td class="principalQuadroPstVazia" height="17"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="400"> 
                                      <div align="center"></div>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td class="principalBgrQuadro" valign="top">
                          <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr> 
                              <td width="21%" align="right" class="principalLabel" height="35"><bean:message key="parametrosRemuneracaoForm.produto"/> 
                                <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="35%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="63%"> 
                                      <html:select property="idAssunto" name="parametrosRemuneracaoForm" styleClass="principalObjForm">
										<html:option value="0">-- Selecione uma Op��o --</html:option>
										<logic:notEmpty name="csCdtbProdutoAssuntoPrasVector">
											<bean:define name="csCdtbProdutoAssuntoPrasVector" id="produtoVO"/>
											<html:options collection="produtoVO" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/>
										</logic:notEmpty>
									</html:select>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="31%"> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
								  <tr> 
									<td width="23%" align="right" class="principalLabel" height="34"><bean:message key="parametrosRemuneracaoForm.validadede"/>
									   <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
									</td>
									<td width="77%"> 
									  <table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
										  <td class="principalLabel" width="15%" ><img src="webFiles/images/botoes/calendar.gif" align="right" width="16" height="15"  title="Calend�rio" onclick="show_calendar('parametrosRemuneracaoForm.pareDhValidadede')"; class="principalLstParMao"/>
											<html:text property="pareDhValidadede" maxlength="10" name="parametrosRemuneracaoForm" styleClass="principalObjForm"/>
										  </td>
										  <td width="12%" class="principalLabel" align="right"><bean:message key="parametrosRemuneracaoForm.validadeate"/> 
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
										  </td>
										  <td class="principalLabel" width="15%"><img src="webFiles/images/botoes/calendar.gif" align="right" width="16" height="15"  title="Calend�rio" onclick="show_calendar('parametrosRemuneracaoForm.pareDhValidadeate')"; class="principalLstParMao"/>
											<html:text property="pareDhValidadeate" maxlength="10" name="parametrosRemuneracaoForm" styleClass="principalObjForm"/>
										  </td>
										</tr>
									  </table>
									</td>
								  </tr>
								</table>
							 </td>
                            </tr>
                            <tr> 
                              <td width="21%" align="right" class="principalLabel" height="35"><bean:message key="parametrosRemuneracaoForm.diasdeatrasode"/> 
                              	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="35%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalLabel" width="39%"> 
                                      <html:text property="pareNrDiasatrasode" maxlength="5" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false;"/>
                                    </td>
                                    <td width="23%" class="principalLabel" align="right"><bean:message key="parametrosRemuneracaoForm.diasdeatrasoate" /> 
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="38%"> 
                                      <html:text property="pareNrDiasatrasoate" maxlength="5" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false;"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="44%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="21%" align="right" class="principalLabel" height="35"><bean:message key="parametrosRemuneracaoForm.valornegociadode"/>
                                <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="35%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalLabel" width="39%"> 
                                      <html:text property="pareVlNegociadode"  maxlength="15" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return(formataDecimal(this,',',event,9))"/>
                                    </td>
                                    <td width="23%" class="principalLabel" align="right"><bean:message key="parametrosRemuneracaoForm.valornegociadoate"/> 
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="38%"> 
                                      <html:text property="pareVlNegociadoate" maxlength="15" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return(formataDecimal(this,',',event,9))"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="44%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="21%" align="right" class="principalLabel" height="35"><bean:message key="parametrosRemuneracaoForm.formapagamento"/> 
                                 <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="35%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="68%"> 
                                      <html:select property="idFopgCdFormapagto" name="parametrosRemuneracaoForm" styleClass="principalObjForm">
                              			<html:option value="0">-- Selecione uma Op��o --</html:option>
                       					<logic:notEmpty name="cbDmtbFormapagtoFopg">
                       						<bean:define name="cbDmtbFormapagtoFopg" id="formapagtoVO"/>
                       						<html:options collection="formapagtoVO" property="field(ID_FOPG_CD_FORMAPAGTO)" labelProperty="field(FOPG_DS_DESCRICAO)"/>
                       					</logic:notEmpty>
                               		</html:select>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="44%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="21%" align="right" class="principalLabel" height="35"><bean:message key="parametrosRemuneracaoForm.quantidadeParcelasDe"/> 
                                <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="35%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalLabel" width="39%"> 
                                      <html:text property="pareNrParcelasde" maxlength="5" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false;"/>
                                    </td>
                                    <td width="23%" class="principalLabel" align="right"><bean:message key="parametrosRemuneracaoForm.quantidadeParcelasAte" /> 
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="38%"> 
                                      <html:text property="pareNrParcelasate" maxlength="5" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false;"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="44%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="21%" align="right" class="principalLabel" height="35"><bean:message key="parametrosRemuneracaoForm.metaRecuperacaoDe"/> 
                              	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="35%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalLabel" width="39%"> 
                                     <html:text property="paraVlPercentmetade" maxlength="5" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return(formataDecimal(this,',',event,9))"/> 
                                    </td>
                                    <td width="23%" class="principalLabel" align="right"><bean:message key="parametrosRemuneracaoForm.metaRecuperacaoAte"/>
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="38%"> 
                                      <html:text property="paraVlPercentmetaate" maxlength="10" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return(formataDecimal(this,',',event,9))"/>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="44%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="21%" align="right" class="principalLabel" height="35"><bean:message key="parametrosRemuneracaoForm.condicoes"/> 
                                <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="35%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="68%"> 
                                      <html:select property="idCondCdCondicao" name="parametrosRemuneracaoForm" styleClass="principalObjForm">
                              			<html:option value="0">-- Selecione uma Op��o --</html:option>
                       					<logic:notEmpty name="cbDmtbCondicaoCond">
                       						<bean:define name="cbDmtbCondicaoCond" id="condicaoVO"/>
                       						<html:options collection="condicaoVO" property="field(ID_COND_CD_CONDICAO)" labelProperty="field(COND_DS_CONDICAO)"/>
                       					</logic:notEmpty>
                               		</html:select>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="44%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                      </tr>
                      <tr> 
                        <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                        <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" class="EspacoPequeno">
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td width="50%">
                          <table width="98%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"><bean:message key="parametrosRemuneracaoForm.lista.calcularSobre"/></td>
                                    <td class="principalQuadroPstVazia" height="17"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="400"> 
                                            <div align="center"></div>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top"> 
                                <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                                  <tr> 
                                    <td width="42%" class="principalLabel"> 
                                      <html:radio property="paraInCalculosobre" value="P" onclick="selecionaCalculoSobre();"/>
                                      <bean:message key="parametrosRemuneracaoForm.principal"/></td>
                                    <td width="58%" class="principalLabel"> 
                                      <html:radio property="paraInCalculosobre" value="N" onclick="selecionaCalculoSobre();"/>
                                      <bean:message key="parametrosRemuneracaoForm.totalNegociado"/></td>
                                  </tr>
                                  <tr> 
                                    <td width="42%" class="principalLabel">&nbsp; </td>
                                    <td width="58%" class="principalLabel">
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="15%" class="principalLabel" align="right"><img src="webFiles/images/icones/plus.gif" width="13" height="16"></td>
                                          <td width="85%" class="principalLabel"> 
                                            <logic:notEqual value="S" name="parametrosRemuneracaoForm" property="paraInMora">
												<input type="checkbox" name="mora" value="ativo"/>                              	                              	
											</logic:notEqual>
											<logic:equal value="S" name="parametrosRemuneracaoForm" property="paraInMora">                                
												<input type="checkbox" name="mora" value="inativo" checked="checked"/>                              	
											</logic:equal> 
                                            <bean:message key="parametrosRemuneracaoForm.mora"/></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr> 
                                    <td width="42%" class="principalLabel">
                                      <html:radio property="paraInCalculosobre" value="R" onclick="selecionaCalculoSobre();"/>
                                      <bean:message key="parametrosRemuneracaoForm.parcelaRecuperada"/></td>
                                    <td width="58%" class="principalLabel">
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="15%" class="principalLabel" align="right"><img src="webFiles/images/icones/plus.gif" width="13" height="16"></td>
                                          <td width="85%" class="principalLabel"> 
                                           <logic:notEqual value="S" name="parametrosRemuneracaoForm" property="paraInMulta">
			                              		<input type="checkbox" name="multa" value="ativo"/>                              	                              	
			                              	</logic:notEqual>
			                              	<logic:equal value="S" name="parametrosRemuneracaoForm" property="paraInMulta">                                
			                              		<input type="checkbox" name="multa" value="inativo" checked="checked"/>                              	
			                              	</logic:equal> 
                                            <bean:message key="parametrosRemuneracaoForm.multa"/></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr> 
                                    <td width="42%" class="principalLabel">&nbsp;</td>
                                    <td width="58%" class="principalLabel">
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="15%" class="principalLabel" align="right"><img src="webFiles/images/icones/plus.gif" width="13" height="16"></td>
                                          <td width="85%" class="principalLabel"> 
                                            <logic:notEqual value="S" name="parametrosRemuneracaoForm" property="paraInJuros">
			                              		<input type="checkbox" name="juros" value="ativo"/>                              	                              	
			                              	</logic:notEqual>
			                              	<logic:equal value="S" name="parametrosRemuneracaoForm" property="paraInJuros">                                
			                              		<input type="checkbox" name="juros" value="inativo" checked="checked"/>                              	
			                              	</logic:equal>
                                            <bean:message key="parametrosRemuneracaoForm.juros"/></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                        <td width="50%" valign="top"> 
                          <table width="98%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166">&nbsp;<bean:message key="parametrosRemuneracaoForm.lista.valoresRemuneracao"/></td> 
                                    <td class="principalQuadroPstVazia" height="17"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="400"> 
                                            <div align="center"></div>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top"> 
                                <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                                  <tr> 
                                    <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="parametrosRemuneracaoForm.percentual"/> 
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="61%">
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td class="principalLabel" width="28%"> 
                                            <html:text property="pareVlPercenthonorarios" maxlength="5" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return(formataDecimal(this,',',event,9))"/>
                                          </td>
                                          <td width="40%" class="principalLabel" align="right"><bean:message key="parametrosRemuneracaoForm.despendio"/>
                                          		<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                          </td>
                                          <td width="32%"> 
                                             <html:text property="pareVlPercentdespendido" maxlength="5" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return(formataDecimal(this,',',event,9))"/>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td width="13%">&nbsp;</td>
                                  </tr>
                                  <tr> 
                                    <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="parametrosRemuneracaoForm.valorFixo"/> 
                                    	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="61%"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td class="principalLabel" width="28%"> 
                                            <html:text property="paraVlFixo" maxlength="10" name="parametrosRemuneracaoForm" styleClass="principalObjForm" onkeypress="return(formataDecimal(this,',',event,9))"/>
                                          </td>
                                          <td width="40%" class="principalLabel" align="right">&nbsp;</td>
                                          <td width="32%">&nbsp; </td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td width="13%" align="center">&nbsp;</td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                      <td width="25" align="center">
                      	<a title="<bean:message key="botoes.incluir"/>" onclick="novo();">
                      		<html:image src="webFiles/images/botoes/new.gif" />
                      	</a>
                      </td>
                      <td width="25" align="center">
                      	<img src="webFiles/images/botoes/gravar.gif" class="geralCursoHand" onclick="salvar();"/>
                      </td>
                      <td width="25" align="center">
                      	<a title="<bean:message key="botoes.cancelar"/>" onclick="cancelar();">
                      		<html:image src="webFiles/images/botoes/cancelar.gif"/></a>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
 </html:form>
</body>

<script>
	temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PARAMETROS_ALTERACAO_CHAVE%>');
</script>
<logic:equal name="parametrosRemuneracaoForm" property="userAction" value="init">
	<script>
		//Verifica permiss�o de gravar
		//setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PARAMETROS_INCLUSAO_CHAVE%>', document.parametrosRemuneracaoForm.imgGravar, "<bean:message key='prompt.gravar'/>");
		
		//if(!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PARAMETROS_INCLUSAO_CHAVE%>')) {
		if (!temPermissaoAlterar){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PARAMETROS_ALTERACAO_CHAVE %>', document.parametrosRemuneracaoForm.imgGravar);
			desabilitaCamposParametros();
			document.parametrosRemuneracaoForm.idPareCdParamremunera.disabled= false;
			document.parametrosRemuneracaoForm.idPareCdParamremunera.value= '';
			document.parametrosRemuneracaoForm.idPareCdParamremunera.disabled= true;			
		}
			
		//Verifica permiss�o de alterar
		if (!temPermissaoAlterar){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PARAMETROS_ALTERACAO_CHAVE%>', document.parametrosRemuneracaoForm.imgNovo);	
			desabilitaCamposParametros();
		} else{
		habilitarCampos();
		}	
		//Verifica permiss�o de excluir
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PARAMETROS_EXCLUSAO_CHAVE%>')){
			//desabilitaCamposParametros();
			
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.parametrosRemuneracaoForm.btExcluir'+i+');');
			}
		}
	</script>
</logic:equal>
<logic:equal name="parametrosRemuneracaoForm" property="userAction" value="alterar">
	<script>
		//Verifica permiss�o de alterar
		if (!temPermissaoAlterar){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PARAMETROS_ALTERACAO_CHAVE%>', document.parametrosRemuneracaoForm.imgNovo);	
			desabilitaCamposParametros();
		} else{
 			habilitarCampos();
		}	
		if (!temPermissaoAlterar){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PARAMETROS_ALTERACAO_CHAVE%>', document.parametrosRemuneracaoForm.imgGravar);
			desabilitaCamposParametros();
		} else{
 			habilitarCampos();
		}	
	
	</script>
</logic:equal>	
<script>
	habilitaListaEmpresas();
</script>
</html>
