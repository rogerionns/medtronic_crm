<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
String fileIncludeIdioma="../../../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/cadastroTela.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
</head>

<body class="principalBgrPage" text="#000000" onload="fecharAguarde();showError('<%=request.getAttribute("msgerro")%>');AtivarPasta('<bean:write name="cadastroTelaForm" property="flagAba"/>');verificaCampos('<bean:write name="cadastroTelaForm" property="flagAba"/>','<bean:write name="cadastroTelaForm" property="flagHabilitaCampos"/>');inicio();">
<html:form action="/cadastroTela" method="post">
<html:hidden property="userAction"/>
<html:hidden property="alterando"/>
<html:hidden property="idTela"/>
<html:hidden property="idCampo"/>

<input type="hidden" name="idTelaAdicional" value=""/>
<input type="hidden" name="idCampoAdicional" value=""/>
<input type="hidden" name="dataInativa" value=""/>
<input type="hidden" name="dsGrupo" value=""/>
<input type="hidden" name="dsCampo" value=""/>
<input type="hidden" name="nrPosicao" value=""/>
<input type="hidden" name="nrTamanho" value=""/>
<input type="hidden" name="check" value=""/> 
<input type="hidden" name="inativo" value=""/>



  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="cadastroTela.title.cadastroTela"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="500"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado" name="tdProcurar" id="tdProcurar" onClick="AtivarPasta('pesquisar');"><bean:message key="cadastroTelaForm.aba.procurar"/></td>
                <td class="principalPstQuadroLinkNormalMaior" name="tdEstrategias" id="tdEstrategias" onClick="limparCampos();AtivarPasta('editar');"><bean:message key="cadastroTelaForm.aba.cadastroTela"/></td>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr> 
                <td height="400" valign="top"> 
                  <div id="Procurar" style="position:absolute; width:99%; height:380px; z-index:1; visibility: hidden;"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="principalLabel"><bean:message key="cadastroTelaForm.descricao"/></td>
                      </tr>
                      <tr> 
                        <td width="56%"> 
                          <html:text property="teadDsTelaadicionalDescricao" name="cadastroTelaForm" styleClass="principalObjForm" maxlength="60"/>	
                        </td>
                        <td width="44%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="pesquisar();" alt="Pesquisar"></td>
                      </tr>
                      <tr> 
                        <td width="56%">&nbsp;</td>
                        <td width="44%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2"> 
                        <div style="height:300px; overflow: auto;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLstCab" width="4%">&nbsp;</td>
                              <td class="principalLstCab" width="4%">&nbsp;</td>
                              <td class="principalLstCab" align="center" width="10%"><bean:message key="cadastroTelaForm.lista.codigo"/></td>
                              <td class="principalLstCab" width="58%"><bean:message key="cadastroTelaForm.lista.nomeTela"/></td>
                              <td class="principalLstCab" width="28%"><bean:message key="cadastroTelaForm.lista.inativo"/></td>
                            </tr>
                            <logic:notEmpty name="cbCdtbTelaadiconalTead">
                            	<logic:iterate name="cbCdtbTelaadiconalTead" id="cbCdtbTelaadiconalTead" indexId="indice">
                            		<tr class="geralCursoHand">
                            			                            			
                            			<td class="principalLstPar" width="4%" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" onclick="excluir('<bean:write name="cbCdtbTelaadiconalTead" property="field(ID_TEAD_CD_TELAADICIONAL)"/>');" width="14" height="14" class="geralCursoHand" alt="Excluir"/></td>
                            			
                            			<td class="principalLstPar" width="4%" align="center" onclick="abrirModalIdioma('CS_ASTB_IDIOMTELAADICONAL_IDAD.xml','<bean:write name="cbCdtbTelaadiconalTead" property="field(ID_TEAD_CD_TELAADICIONAL)"/>');"><img src="webFiles/images/botoes/GloboAuOn.gif" width="18" height="18" class="geralCursoHand"/></td>
                            			
                            			<td class="principalLstPar" align="center" width="10%"onclick="alterar('<bean:write name="cbCdtbTelaadiconalTead" property="field(ID_TEAD_CD_TELAADICIONAL)"/>',
                            																	'<bean:write name="cbCdtbTelaadiconalTead" property="field(TEAD_DS_TELAADICIONAL)"/>',
                            																	'<bean:write name="cbCdtbTelaadiconalTead" property="field(TEAD_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE"filter="html"/>');">&nbsp;<bean:write name="cbCdtbTelaadiconalTead" property="field(ID_TEAD_CD_TELAADICIONAL)"/></td>
                            																	
										<td class="principalLstPar" width="58%"onclick="alterar('<bean:write name="cbCdtbTelaadiconalTead" property="field(ID_TEAD_CD_TELAADICIONAL)"/>',
                            																	'<bean:write name="cbCdtbTelaadiconalTead" property="field(TEAD_DS_TELAADICIONAL)"/>',
                            																	'<bean:write name="cbCdtbTelaadiconalTead" property="field(TEAD_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE"filter="html"/>');">&nbsp;<bean:write name="cbCdtbTelaadiconalTead" property="field(TEAD_DS_TELAADICIONAL)"/></td>
                            													
                            			<td class="principalLstPar" width="28%"onclick="alterar('<bean:write name="cbCdtbTelaadiconalTead" property="field(ID_TEAD_CD_TELAADICIONAL)"/>',
                            																	'<bean:write name="cbCdtbTelaadiconalTead" property="field(TEAD_DS_TELAADICIONAL)"/>',
                            																	'<bean:write name="cbCdtbTelaadiconalTead" property="field(TEAD_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE"filter="html"/>');">&nbsp;<bean:write name="cbCdtbTelaadiconalTead" property="field(TEAD_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE"filter="html"/></td>
                            																	                            																	
                            		</tr>
                            		<script>
		                            	countRegistro++;
		                            </script>
                            	</logic:iterate>
                            </logic:notEmpty>
							<logic:empty name="cbCdtbTelaadiconalTead">
								<tr> 
									<td align="center" colspan="7" > 
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
											<tr id="nenhumRegistro" ><td height="260" width="800" align="center"  class="principalLstPar"><br><b><bean:message key="lista.nenhum.registro.encontrado"/> </b></td></tr>
										</table>
									&nbsp;</td>
								</tr>
							</logic:empty>
                          </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
                  
                  <div id="Estrategias" style="position:absolute; width:99%; height:390px; z-index:2; visibility: visible; "> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%">&nbsp;</td>
                        <td width="8%" class="principalLabel">&nbsp;</td>
                        <td class="principalLabel" colspan="3">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%" ><bean:message key="cadastroTelaForm.codigo"/>
                           <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="8%" class="principalLabel"> 
                          <html:text property="idTeadCdTelaadicional" name="cadastroTelaForm" styleClass="principalObjForm" maxlength="10" disabled="true"/>	
                        </td>
                        <td class="principalLabel" colspan="3">&nbsp; </td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="cadastroTelaForm.nomeTela"/> 
                           <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                                <html:text property="teadDsTelaadicional" name="cadastroTelaForm" styleClass="principalObjForm" maxlength="30"/>
                              </td>
                              <td width="27%" class="principalLabel" align="center">&nbsp; 
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="cadastroTelaForm.grupo"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                                <html:text property="caadDsGrupo" name="cadastroTelaForm" styleClass="principalObjForm" maxlength="30"/>
                              </td>
                              <td width="27%" class="principalLabel" align="center">&nbsp; 
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="cadastroTelaForm.campos"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                                <html:text property="caadDsCampo" name="cadastroTelaForm" styleClass="principalObjForm" maxlength="30"/>
                              </td>
                              <td width="27%" class="principalLabel" align="center">&nbsp; 
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="cadastroTelaForm.posicaoRegistro"/> 
                          	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="12%"> 
                                <html:text property="caadNrPosicao" name="cadastroTelaForm" styleClass="principalObjForm" maxlength="10" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false;"/>
                              </td>
                              <td width="88%" class="principalLabel" align="center">&nbsp; 
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="cadastroTelaForm.tamanho"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="12%"> 
                                <html:text property="caadNrTamanho" name="cadastroTelaForm" styleClass="principalObjForm" maxlength="30" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false;"/>
                              </td>
                              <td width="61" align="right">	
		                      		<img src="webFiles/images/botoes/setaDown.gif" border="falser" onclick="adicionaLista();"  class="geralCursoHand" alt="Adicionar"/>
		                      		
		                      </td>
                              <td width="27%" class="principalLabel" align="center"> 
                              
                              	<logic:equal value="" name="cadastroTelaForm" property="teadDhInativo">
                              		<input type="checkbox" name="checkado" value="ativo"/>                              	                              	
                              	</logic:equal>
                              	<logic:notEqual value="" name="cadastroTelaForm" property="teadDhInativo">                                
                              		<input type="checkbox" name="checkado" value="inativo" checked="checked"/>                              	
                              	</logic:notEqual> 	       
                              	
                              	                       	                                     	          
                              	<bean:message key="cadastroTelaForm.inativo"/>
                             </tr>
                          </table>
                        </td>
                        
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                    </table>
					<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td>&nbsp; </td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td align="right" class="EspacoPequeno" width="4%">&nbsp;</td>
                        <td class="EspacoPequeno" align="right" width="89%">&nbsp;</td>
                        <td width="7%" class="EspacoPequeno">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="4%" align="right" class="principalLabel" height="25">&nbsp;</td>
                        <td align="right" class="principalLabel" height="25" width="89%"> 
                        <div style="height:130px; width:750px; overflow: auto;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="4%"  class="principalLstCab">&nbsp; </td>
                              <td width="32%" class="principalLstCab">&nbsp;&nbsp;<bean:message key="cadastroTelaForm.lista.grupo"/></td>
                              <td width="22%" class="principalLstCab"><bean:message key="cadasdtroTelaForm.lista.campos"/></td>
                              <td width="22%" class="principalLstCab"><bean:message key="cadasdtroTelaForm.lista.posicaoRegistro"/></td>
                              <td width="20%" class="principalLstCab"><bean:message key="cadasdtroTelaForm.lista.tamanho"/></td>
                            </tr>
                            <logic:notEmpty name="cbCdtbCampoadicionalCaad">
                            	<logic:iterate name="cbCdtbCampoadicionalCaad" id="cbCdtbCampoadicionalCaad">
                            		<logic:notEqual name="cbCdtbCampoadicionalCaad" property="field(excluido)" value="true" >  
	                            		<tr class="geralCursoHand">															
	                            			<td class="principalLstPar" width="4%" onclick="excluirLista('<bean:write name="cbCdtbCampoadicionalCaad" property="field(ID_CAAD_CD_CAMPOADICIONAL)"/>');"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" alt="Excluir"></td>
	                            			<td class="principalLstPar" width="32">&nbsp;&nbsp;<bean:write name="cbCdtbCampoadicionalCaad" property="field(CAAD_DS_GRUPO)"/>&nbsp;</td>
	                            			<td class="principalLstPar" width="22"><bean:write name="cbCdtbCampoadicionalCaad" property="field(CAAD_DS_CAMPO)"/>&nbsp;</td>
	                            			<td class="principalLstPar" width="22"><bean:write name="cbCdtbCampoadicionalCaad" property="field(CAAD_NR_POSICAO))"/>&nbsp;</td>
	                            			<td class="principalLstPar" width="20"><bean:write name="cbCdtbCampoadicionalCaad" property="field(CAAD_NR_TAMANHO)"/>&nbsp;</td>
	                            		</tr>	
	                            	</logic:notEqual>
                            	</logic:iterate>
                            </logic:notEmpty>
                          </table>
                          </div>
                        </td>
                        <td width="7%"></td>
                      </tr>
                       <tr> 
     				     <td width="317%" ><img src="webFiles/images/icones/agendamentos02.gif" width="22" height="23" align="right" class="geralCursoHand" onClick="showModalDialog('cadastroTela.do?userAction=informacoesAdicionais',window,'help:no;scroll:yes;Status:NO;dialogWidth:540px;dialogHeight:305px,dialogTop:0px,dialogLeft:200px');" alt="Simular Tela"></td>
                      </tr>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
		                <td> 
		                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <tr> 
		                      <td>&nbsp;</td>
		                      <td width="25" align="center">
		                      	<a title="<bean:message key="botoes.incluir"/>" onclick="novo();">
		                      		<html:image src="webFiles/images/botoes/new.gif" />
		                      	</a>
		                      </td>
		                      <td width="25" align="center">
		                      	<img src="webFiles/images/botoes/gravar.gif" class="geralCursoHand" onclick="salvar();"/>
		                      </td>
		                      <td width="25" align="center">
		                      	<a title="<bean:message key="botoes.cancelar"/>" onclick="cancelar();">
		                      		<html:image src="webFiles/images/botoes/cancelar.gif"/></a>
		                      </td>
		                    </tr>
		                  </table>
		                </td>
		              </tr>
	              <tr> 
	                <td class="EspacoPequeno">&nbsp;</td>
	              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form> 
</body>

<script>
	temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_TELA_ALTERACAO_CHAVE%>');
</script>
<logic:equal name="cadastroTelaForm" property="userAction" value="init">
	<script>
		//Verifica permiss�o de gravar
		//setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_TELA_INCLUSAO_CHAVE%>', document.cadastroTelaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
		
		//if(!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_TELA_INCLUSAO_CHAVE%>')) {
		if (!temPermissaoAlterar){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_TELA_ALTERACAO_CHAVE%>', document.cadastroTelaForm.imgGravar);
			desabilitaCamposCadastroTela();
			document.cadastroTelaForm.idTeadCdTelaadicional.disabled= false;
			document.cadastroTelaForm.idTeadCdTelaadicional.value= '';
			document.cadastroTelaForm.idTeadCdTelaadicional.disabled= true;			
		} else {
			habilitarCampos();
		}
			
		//Verifica permiss�o de alterar
		temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_TELA_ALTERACAO_CHAVE%>');
		if (!temPermissaoAlterar){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_TELA_ALTERACAO_CHAVE%>', document.cadastroTelaForm.imgNovo);	
			desabilitaCamposCadastroTela();
		} else {
			habilitarCampos();
		}
		
		//Verifica permiss�o de excluir
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_TELA_EXCLUSAO_CHAVE%>')){
			//desabilitaCamposCadastroTela();
			
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.cadastroTelaForm.btExcluir'+i+');');
			}
		}
	</script>
</logic:equal>
<logic:equal name="cadastroTelaForm" property="userAction" value="alterar">
	<script>
		//Verifica permiss�o de alterar
		if (!temPermissaoAlterar){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_TELA_ALTERACAO_CHAVE%>', document.cadastroTelaForm.imgNovo);	
			desabilitaCamposCadastroTela();
		} else {
			habilitarCampos();
		}	
		if (!temPermissaoAlterar){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_TELA_ALTERACAO_CHAVE%>', document.cadastroTelaForm.imgGravar);
			desabilitaCamposCadastroTela();
		} else {
			habilitarCampos();
		}
	</script>
</logic:equal>
<script>
	habilitaListaEmpresas();
	setaArquivoXml("CS_ASTB_IDIOMTELAADICONAL_IDAD.xml");
	habilitaTelaIdioma();
</script>
</html>