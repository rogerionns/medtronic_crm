<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<% 

response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String fileIncludeIdioma="../../../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<script type="text/javascript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script type="text/javascript" src="webFiles/cobranca/js/estrategias.js"></SCRIPT>
<script type="text/javascript" src="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
<script type="text/javascript" src="webFiles/funcoes/pt/date-picker.js"></SCRIPT>
<script type="text/javascript" src="webFiles/funcoes/pt/validadata.js"></script>
<script type="text/javascript" src="webFiles/funcoes/variaveis.js"></SCRIPT>
<script type="text/javascript">
var jaAtualizouUmaVez = false;

function inicio(){
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(document.forms[0].elements["idEstrCdEstrategia"].value);

	if(document.forms[0].flagHabilitaAba.value != '' && document.forms[0].flagAba.value == 'editar' && ifCenariosSimulados.countCenarios > 0)
		AtivarPasta2(document.forms[0].flagHabilitaAba.value);
		
	if(document.forms[0].idSelecionado.value != '') {
		ifInclusao.document.forms[0].idSelecionado.value = document.forms[0].idSelecionado.value;
	}
}

function pesquisar(){	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();	
	habilitarCampos();
	document.forms[0].elements["alterando"].value ="criar";
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();
} 

function alterar(idEstrategia, estrEstrategia, estrValidadede, estrValidadeate, assuntoNivel1, assuntoNivel2, estrInativo, estrAgendar){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].elements["idEstrategia"].value = idEstrategia;
	document.forms[0].elements["estrEstrategia"].value = estrEstrategia;
	document.forms[0].elements["estrValidadede"].value = estrValidadede;
	document.forms[0].elements["estrValidadeate"].value = estrValidadeate;
	document.forms[0].elements["assuntoNivel1"].value = assuntoNivel1;
	document.forms[0].elements["assuntoNivel2"].value = assuntoNivel2;
	document.forms[0].elements["estrInativo"].value = estrInativo;
	document.forms[0].elements["estrAgendar"].value = estrAgendar;
	
	document.forms[0].elements["alterando"].value ="altera";
	document.forms[0].userAction.value = "alterar";
	document.forms[0].submit();
}

function excluirEstrategia (idEstrategia){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
		document.forms[0].elements["idEstrategia"].value =idEstrategia;	
		document.forms[0].userAction.value = "excluirEstrategia";		
		document.forms[0].submit();
	}
}

function validaPeriodoData() {
	if(document.forms[0].elements["estrDhValidadede"].value == ''){
		alert("O campo validade de � obrigat�rio!");
		document.forms[0].elements["estrDhValidadede"].focus();
		return false;
	} else {
		var dataDe = document.forms[0].elements["estrDhValidadede"].value;
		var dataAte = document.forms[0].elements["estrDhValidadeate"].value;
		
		if(validaDataMaiorQueOutra(dataDe, dataAte) < 0) {
			alert('O campo validade at� deve ser maior ou igual ao campo validade de!');
			document.forms[0].elements["estrDhValidadeate"].focus();
			return false;
		}
	}
	return true;
}

function salvar(){
	if(document.forms[0].elements["estrDsEstrategia"].value == ''){
		alert("O campo estrat�gia � obrigat�rio!");
		document.forms[0].elements["estrDsEstrategia"].focus();
		return;
	}
	
	if(document.forms[0].elements["estrDhValidadede"].value == ''){
		alert("O campo validade de � obrigat�rio!");
		document.forms[0].elements["estrDhValidadede"].focus();
		return;
	}
	
	if(document.forms[0].elements["estrDhValidadeate"].value == ''){
		alert("O campo validade at� � obrigat�rio!");
		document.forms[0].elements["estrDhValidadeate"].focus();
		return;
	}
	
	
	if(!validaPeriodoData()) {
		return false;
	}		

	/*if(document.forms[0].elements["idAssunto"].value =='0'){
		alert("O campo produto � obrigat�rio!");
		document.forms[0].elements["idAssunto"].focus();
		return;
	}*/
	
	if(ifCenariosSimulados.countCenarios > 0 && ifCenariosSimulados.cenarioSelecionado == 9999) {
		alert('� necess�rio selecionar um cen�rio!');
		AtivarPasta2('CENARIOS');
		return;
	} else {
		document.forms[0].idSelecionado.value = ifCenariosSimulados.cenarioSelecionado;
	}
		
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	//CHECANDO O INATIVO
	if(document.forms[0].elements["agendar"].checked){
		document.forms[0].elements["checkAgendar"].value = 'S';
	}else{
		document.forms[0].elements["checkAgendar"].value = 'N';
	}
	
	//CHECANDO O AGENDAR
	if(document.forms[0].elements["checkado"].checked){
		document.forms[0].elements["check"].value = 'INATIVO';
	}else{
		document.forms[0].elements["check"].value = 'ATIVO';
	}
	
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	
	return false;
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}
	
function fecharCenario(){
	if (confirm("Confirma o fechamento do cen�rio?")){
		if(ifCriteriosSimulados.mensagemCamposObrigatorios == '') {
			window.ifCenariosSimulados.location.href = "estrategias.do?userAction=ifCenariosSimulados&idEstrCdEstrategia=" + document.forms[0].elements["idEstrCdEstrategia"].value;
			AtivarPasta2('CENARIOS');
		} else {
			alert(ifCriteriosSimulados.mensagemCamposObrigatorios)
		}
	}
}	

function executarEstrategia() {
	if(confirm('Confirma a execu��o da estrat�gia?')) {
		if(document.forms[0].idEstrategia.value == '' || document.forms[0].idEstrategia.value == '0') {
			alert('� necess�rio editar ou salvar o registro para executar a estrat�gia!');
			return false;
		} else {
			document.forms[0].mensagem.value = '';
			ifrmExecutaEstrategia.location.href = 'estrategias.do?userAction=executarEstrategia&idEstrategia=' + document.forms[0].idEstrategia.value;
			showModalDialog('estrategias.do?userAction=autoRefreshEstrategia&idEstrategia=' + document.forms[0].idEstrategia.value, window, 'help:no;scroll:no;Status:NO;dialogWidth:450px;dialogHeight:320px,dialogTop:100px,dialogLeft:650px');
		}
	}
}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="fecharAguarde();showError('<%=request.getAttribute("msgerro")%>');AtivarPasta('<bean:write name="estrategiasForm" property="flagAba"/>');verificaCampos('<bean:write name="estrategiasForm" property="flagAba"/>','<bean:write name="estrategiasForm" property="flagHabilitaCampos"/>');inicio();">

<html:form action="/estrategias" method="post">
<html:hidden property="userAction"/>
<html:hidden property="idEstrategia"/>
<html:hidden property="idFiltro"/>
<html:hidden property="idCritCdCriterio"/>
<html:hidden property="alterando"/>
<html:hidden property="idSelecionado"/>
<html:hidden property="flagAba"/>
<html:hidden property="flagHabilitaAba"/>
<html:hidden property="mensagem"/>
<html:hidden property="jaAtualizouUmaVez"/>
<input type="hidden" name="idEstrEstrategia" value=""/>
<input type="hidden" name="estrEstrategia" value=""/>
<input type="hidden" name="estrValidadede" value=""/>
<input type="hidden" name="estrValidadeate" value=""/>
<input type="hidden" name="assuntoNivel1" value=""/>
<input type="hidden" name="assuntoNivel2" value=""/>
<input type="hidden" name="estrInativo" value=""/>
<input type="hidden" name="estrAgendar" value=""/>
<input type="hidden" name="filtOperador" value=""/>
<input type="hidden" name="filtCampo" value=""/>
<input type="hidden" name="filtValor" value=""/>
<input type="hidden" name="radioE" value=""/>
<input type="hidden" name="radioOU" value=""/>
<input type="hidden" name="descricaoCampo" value=""/>
<input type="hidden" name="descricaoOperacao" value=""/>
<input type="hidden" name="check" value=""/>
<input type="hidden" name="checkAgendar" value=""/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="estrategia.title.descricao"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="570"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado" name="tdProcurar" id="tdProcurar" onClick="AtivarPasta('pesquisar');"><bean:message key="estrategia.janela.procurar"/></td>
                <td class="principalPstQuadroLinkNormalMaior" name="tdEstrategias" id="tdEstrategias" onClick="AtivarPasta('editar');"><bean:message key="estrategia.janela.estrategias"/></td>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr> 
                <td height="490" valign="top"> 
                  <div id="Procurar" style="position:absolute; width:99%; height:350px; z-index:1; visibility: visible"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="principalLabel"><bean:message key="estrategiaForm.descricao"/></td>
                      </tr>
                      <tr> 
                        <td width="56%"> 
                          <html:text property="descricaoEstrategia" name="estrategiasForm" styleClass="principalObjForm" maxlength="60" onkeyup="pressEnter(event)" />
                        </td>
                        <td width="44%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="pesquisar();"></td>
                      </tr>
                      <tr> 
                        <td width="56%">&nbsp;</td>
                        <td width="44%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2"> 
                        	<div style=" height:400px; overflow: auto;">
                          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr> 
		                              <td class="principalLstCab" width="3%">&nbsp;</td>
		                              <td class="principalLstCab" width="3%">&nbsp;</td>
		                              <td class="principalLstCab" width="8%"><bean:message key="estrategia.lista.codigo"/></td>
		                              <td class="principalLstCab" width="64%"><bean:message key="estrategia.lista.estrategia"/></td>
		                              <td class="principalLstCab" width="22%"><bean:message key="estrategia.lista.inativo"/></td>
		                            </tr>
		                            <logic:notEmpty name="cbCdtbEstrategiaEstr">
		                            	<logic:iterate name="cbCdtbEstrategiaEstr" id="cbCdtbEstrategiaEstr" indexId="indice">
				                            <tr class="geralCursoHand"> 
				                              <td class="principalLstPar" width="3%" onclick="excluirEstrategia('<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ESTR_CD_ESTRATEGIA)"/>');">&nbsp;<img src="webFiles/images/botoes/lixeira18x18.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" width="14" height="14" class="geralCursoHand" alt="Excluir"></td>
				                              <td class="principalLstPar" width="3%" align="center" onclick="abrirModalIdioma('CS_ASTB_IDIOMAESTRATEGIA_IDTR.xml','<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ESTR_CD_ESTRATEGIA)"/>');"><img src="webFiles/images/botoes/GloboAuOn.gif" width="18" height="18" class="geralCursoHand" alt="Idiomas"/></td>
				                              <td class="principalLstPar" width="8%" onclick="alterar('<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ESTR_CD_ESTRATEGIA)"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DS_ESTRATEGIA)"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DH_VALIDADEDE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
		                              														   						  '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DH_VALIDADEATE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ASN1_ASSUNTONIVEL1_ASN1)"/>',
		                              														   						  '<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ASN2_ASSUNTONIVEL2_ASN2)"/>',
		                              														   						  '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DH_INATIVO)"format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_IN_AGENDAR)"/>');" >&nbsp;<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ESTR_CD_ESTRATEGIA)"/></td>
				                              
				                              <td class="principalLstPar" width="64%" onclick="alterar('<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ESTR_CD_ESTRATEGIA)"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DS_ESTRATEGIA)"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DH_VALIDADEDE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
		                              														   						  '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DH_VALIDADEATE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ASN1_ASSUNTONIVEL1_ASN1)"/>',
		                              														   						  '<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ASN2_ASSUNTONIVEL2_ASN2)"/>',
		                              														   						  '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_IN_AGENDAR)"/>');" >&nbsp;<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DS_ESTRATEGIA)"/></td>
				                              
	                                          <td class="principalLstPar" width="22%" onclick="alterar('<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ESTR_CD_ESTRATEGIA)"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DS_ESTRATEGIA)"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DH_VALIDADEDE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
		                              														   						  '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DH_VALIDADEATE)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ASN1_ASSUNTONIVEL1_ASN1)"/>',
		                              														   						  '<bean:write name="cbCdtbEstrategiaEstr" property="field(ID_ASN2_ASSUNTONIVEL2_ASN2)"/>',
		                              														   						  '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>',
																						                              '<bean:write name="cbCdtbEstrategiaEstr" property="field(ESTR_IN_AGENDAR)"/>');" >&nbsp;<bean:write name="cbCdtbEstrategiaEstr" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE"  property="field(ESTR_DH_INATIVO)" filter="html"/></td>
				                            </tr>
				                             <script>
				                            	countRegistro++;
				                            </script>
		                            	</logic:iterate>
		                            </logic:notEmpty>
		                            <logic:empty name="cbCdtbEstrategiaEstr">
										<tr> 
											<td align="center" colspan="7" > 
												<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
													<tr id="nenhumRegistro" ><td height="260" width="800" align="center"  class="principalLstPar"><br><b><bean:message key="lista.nenhum.registro.encontrado"/> </b></td></tr>
												</table>
											&nbsp;</td>
										</tr>
									</logic:empty>	
                          </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
                  
                  <div id="Estrategias" style="position:absolute; width:99%; height:415px; z-index:2; visibility: hidden;"> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="EspacoPequeno" align="right" width="19%">&nbsp;</td>
                        <td width="8%" class="EspacoPequeno">&nbsp;</td>
                        <td class="EspacoPequeno" colspan="3">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="19%"><bean:message key="estrategiaForm.codigo"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="8%" class="principalLabel"> 
                          <html:text property="idEstrCdEstrategia" name="estrategiasForm" disabled="true" maxlength="10" styleClass="principalObjForm"/>
                        </td>
                        <td class="principalLabel" colspan="3"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="43%" class="EspacoPequeno">&nbsp;</td>
                              <td width="32%" class="EspacoPequeno">&nbsp;</td>
                              <td width="25%" class="EspacoPequeno">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="43%">&nbsp;</td>
                              <td width="32%" class="principalLabel" valign="bottom"><bean:message key="estrategiaForm.validade"/></td>
                              <td width="25%" class="principalLabel" valign="bottom"> 
                                <div id="esconde" style="visibility: hidden;">
                                <logic:notEqual value="S" name="estrategiasForm" property="estrInAgendar">
                              		<input type="checkbox" name="agendar" value="ativo"/>                              	                              	
                              	</logic:notEqual>	                              	          
                              	<logic:equal value="S" name="estrategiasForm" property="estrInAgendar">                                
	                              	<input type="checkbox" name="agendar" checked="checked" value="inativo"/>                              	
    	                        </logic:equal>
                                <bean:message key="estrategiaForm.agendar"/></td>
                                </div>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="19%"><bean:message key="estrategiaForm.estrategia"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="50%"> 
                                <html:text name="estrategiasForm" property="estrDsEstrategia" styleClass="principalObjForm" maxlength="30"/>
                              </td>
                              <td width="50%" class="principalLabel"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalLabel" align="right" width="13%"><bean:message key="estrategiaForm.de"/> 
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td class="principalLabel" width="37%"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="73%"> 
                                            <html:text property="estrDhValidadede"  maxlength="10" name="estrategiasForm" styleClass="principalObjForm" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                                          </td>
                                          <td width="27%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('estrategiasForm.estrDhValidadede')"; class="principalLstParMao"></td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td class="principalLabel" align="right" width="14%"><bean:message key="estrategiaForm.ate"/> 
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td class="principalLabel" width="36%"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="73%"> 
                                            <html:text property="estrDhValidadeate"  maxlength="10" name="estrategiasForm" styleClass="principalObjForm" onkeydown="return validaDigito(this, event)" onblur="verificaData(this);validaPeriodoData()"/>
                                          </td>
                                          <td width="27%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('estrategiasForm.estrDhValidadeate')"; class="principalLstParMao"></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <!-- <tr> 
                        <td class="principalLabel" align="right" height="28" width="19%"><bean:message key="estrategiaForm.produto"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                                <html:select property="idAssunto" name="estrategiasForm" styleClass="principalObjForm">
										<html:option value="0">-- Selecione uma op��o --</html:option>
										<logic:notEmpty name="csCdtbProdutoAssuntoPrasVector">
											<bean:define name="csCdtbProdutoAssuntoPrasVector" id="produtoVO"/>
											<html:options collection="produtoVO" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/>
										</logic:notEmpty>
									</html:select>
                              </td>
                              <td width="27%" class="principalLabel" align="center"> 
                                <logic:equal value="" name="estrategiasForm" property="estrDhInativo">
                              		<input type="checkbox" name="checkado" value="ativo"/>                              	                              	
                              </logic:equal>	                              	          
                              <logic:notEqual value="" name="estrategiasForm" property="estrDhInativo">                                
                              	<input type="checkbox" name="checkado" checked="checked" value="inativo"/>                              	
                              </logic:notEqual>                  	                     
                                <bean:message key="estrategiaForm.inativo"/>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>-->
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td>
                          <iframe id="ifrmCmbVisao" name="ifrmCmbVisao" src="estrategias.do?userAction=carregaCmbVisao" width="100%" height="25px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                        </td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                      	<td width="94%" class="principalLabel" align="right"> 
	                        <logic:equal value="" name="estrategiasForm" property="estrDhInativo">
	                      		<input type="checkbox" name="checkado" value="ativo"/>                              	                              	
	                      	</logic:equal>	                              	          
	                      	<logic:notEqual value="" name="estrategiasForm" property="estrDhInativo">                                
	                      		<input type="checkbox" name="checkado" checked="checked" value="inativo"/>                              	
	                      	</logic:notEqual>                  	                     
	                        <bean:message key="estrategiaForm.inativo"/>
	                  	</td>
	                  	<td width="6%">&nbsp;</td>
                      </tr>
                    </table>
                    <!-- <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td width="19%" align="right" class="principalLabel" height="28"><bean:message key="estrategiaForm.campo"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="56%"> 
                          	<html:select property="filtInCampo" name="estrategiasForm" styleClass="principalObjForm">
                      			<html:option value="0">-- Selecione um Campo --</html:option>
               					<logic:notEmpty name="findCbCdtbCampofiltroCafi">
               						<bean:define name="findCbCdtbCampofiltroCafi" id="campoVo"/>
               						<html:options collection="campoVo" property="field(ID_CAFI_CD_CAMPOFILTRO)" labelProperty="field(CAFI_DS_CAMPO)"/>
               					</logic:notEmpty>
                            </html:select>
                        </td>
                        <td width="25%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="19%" align="right" class="principalLabel" height="28"><bean:message key="estrategiaForm.operacao"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="56%"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLabel" width="50%"> 
                                <html:select property="filtInOperador" name="estrategiasForm" styleClass="principalObjForm">
                              			<html:option value="0">-- Selecione uma Opera��o --</html:option>
                       					<logic:notEmpty name="findCbDmtbOperacaoOper">
                       						<bean:define name="findCbDmtbOperacaoOper" id="operacaoVO"/>
                       						<html:options collection="operacaoVO" property="field(ID_OPER_CD_OPERACAO)" labelProperty="field(OPER_DS_OPERACAO)"/>
                       					</logic:notEmpty>
                               		</html:select>
                              </td>
                              <td width="10%" class="principalLabel" align="right">&nbsp;</td>
                              <td width="22%" class="principalLabel">&nbsp; </td>
                              <td width="36%" class="principalLabel" align="center">&nbsp; 
                              </td>
                              <td width="12%" class="principalLabel" align="right">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="25%">&nbsp; </td>
                      </tr>
                      <tr>
                        <td width="19%" align="right" class="principalLabel" height="28"><bean:message key="estrategiaForm.valor"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="56%">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLabel" width="20%"> 
                                <html:text property="filtDsValor" name="estrategiasForm" maxlength="15" styleClass="principalObjForm"/>
                              </td>
                              <td width="10%" class="principalLabel" align="right">&nbsp;</td>
                              <td width="22%" class="principalLabel">
                              	E<html:radio property="filtInJuncao" value="E"/>
                                OU<html:radio property="filtInJuncao" value="O"/>
                                Nenhum<html:radio property="filtInJuncao" value="N"/>
                              </td>
                              <td width="36%" class="principalLabel" align="center">
                                &quot;(&quot;<html:radio property="filtInSeparador" value="("/>
                                &quot;)&quot;<html:radio property="filtInSeparador" value=")"/>
                                Nenhum<html:radio property="filtInSeparador" value="N"/>
                              </td>
                              <td width="12%" class="principalLabel" align="right"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="criarEstrategia();" alt="Listar" > 
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="25%">&nbsp; </td>
                      </tr>
                    </table>-->
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td align="right" width="4%">&nbsp;</td>
                        <td align="right" width="89%">&nbsp;</td>
                        <td width="7%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="4%" align="right" class="principalLabel">&nbsp;</td>
                        <td align="right" class="principalLabel" height="90" width="89%" valign="top"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalPstQuadroLinkSelecionadoGrd" name="tdCriterios" id="tdCriterios" onClick="AtivarPasta2('CRITERIOS');"><bean:message key="estrategiaForm.criterioSimulado"/></td>
                              <td class="principalPstQuadroLinkNormalGrd" name="tdCenarios" id="tdCenarios" onClick="AtivarPasta2('CENARIOS');"><bean:message key="estrategiaForm.lista.criteriosSimulados"/></td>
                              <td class="principalPstQuadroLinkNormalGrd" name="tdInclusao" id="tdInclusao" onClick="AtivarPasta2('INCLUSAO');"><bean:message key="estrategiaForm.lista.inclusaoAcoes"/></td>
                              <td class="principalLabel">&nbsp;</td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="310" class="principalBordaQuadro">
                            <tr> 
                              <td valign="top"> 
                                <div id="criterios" style="position:absolute; width:100%; height:305px; z-index:1; visibility: hidden;">
                                  <iframe id="ifCriteriosSimulados" name="ifCriteriosSimulados" src="estrategias.do?userAction=ifCriteriosSimulados&idMeta=<bean:write property="idFiltCdFiltro" name="estrategiasForm"/>" width="100%" height="305px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                                  <div id="Layer1" style="position:absolute; left:640px; top:280px; width:68px; height:27px; z-index:4"> 
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr align="center"> 
												<td>&nbsp;</td>
												<td><img src="webFiles/images/icones/agendamentos01.gif" name="imgFecharCenario" id="imgFecharCenario" width="22" height="23" alt="Fechar Cen&aacute;rio" class="geralCursoHand" onClick="fecharCenario()"></td>
											</tr>
										</table>
								  </div>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td>&nbsp;</td>
                                    </tr>
                                  </table>
                                </div>
                                
                                <div id="cenarios" style="position:absolute; width:100%; height:305px; z-index:2; visibility: hidden;">
                                  <iframe id="ifCenariosSimulados" name="ifCenariosSimulados" src="estrategias.do?userAction=ifCenariosSimulados&idMeta=<bean:write property="idFiltCdFiltro" name="estrategiasForm"/>" width="100%" height="305px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td>&nbsp;</td>
                                    </tr>
                                  </table>
                                </div>
                                
                                <div id="inclusao" style="position:absolute; width:100%; height:305px; z-index:3; visibility: hidden;"> 
                                  <iframe id="ifInclusao" name="ifInclusao" src="estrategias.do?userAction=carregaifInclusao&idMeta=<bean:write property="idFiltCdFiltro" name="estrategiasForm"/>" width="100%" height="305px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td align="right">&nbsp; </td>
                                    </tr>
                                  </table>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="7%" align="center" valign="bottom">&nbsp;</td>
                      </tr>
                    </table>
                    <div id="executaEstrategia" style="position:absolute; left:682px; top:463px; width:68px; height:27px; z-index:7"> 
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr align="center"> 
								<td>&nbsp;</td>
								<td><img src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" name="imgExecutarEstrategia" id="imgExecutarEstrategia" width="25" height="25" alt="Executar Estrat�gia" class="geralCursoHand" onclick="executarEstrategia()"></td>
							</tr>
						</table>
                    </div>
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/new.gif" alt="Novo" name="imgNovo" id="imgNovo" class="geralCursoHand" onclick="novo();"/>
                      </td>
                      <td width="25" align="center">
                      	<img src="webFiles/images/botoes/gravar.gif" alt="Gravar" name="imgGravar" id="imgGravar" class="geralCursoHand" onclick="salvar();"/>
                      </td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/cancelar.gif" alt="Cancelar" name="imgCancelar" id="imgCancelar" class="geralCursoHand" onclick="cancelar()"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <iframe id="ifrmExecutaEstrategia" name="ifrmExecutaEstrategia" src="" width="0%" height="0%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
 </html:form>
</body>

<script>
	temPermissaoIncluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ESTRATEGIAS_INCLUSAO_CHAVE%>');
	temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ESTRATEGIAS_ALTERACAO_CHAVE%>');
	temPermissaoExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ESTRATEGIAS_EXCLUSAO_CHAVE%>');
		
	function verificaPermissaoExcluir() {
		if (!temPermissaoExcluir){		
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.estrategiasForm.btExcluir'+i+');');
			}
		}
	}
	
	verificaPermissaoExcluir();
</script>

<logic:equal name="estrategiasForm" property="userAction" value="init">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ESTRATEGIAS_ALTERACAO_CHAVE%>', document.estrategiasForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ESTRATEGIAS_ALTERACAO_CHAVE%>', document.estrategiasForm.imgGravar);
			desabilitaCamposEstrategia();
		}
	</script>
</logic:equal>
<logic:equal name="estrategiasForm" property="userAction" value="alterar">
	<script>
		if(!temPermissaoAlterar) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ESTRATEGIAS_ALTERACAO_CHAVE%>', document.estrategiasForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ESTRATEGIAS_ALTERACAO_CHAVE%>', document.estrategiasForm.imgGravar);
			desabilitaCamposEstrategia();
		}
	</script>
</logic:equal>
<logic:equal name="estrategiasForm" property="userAction" value="cancelar">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ESTRATEGIAS_ALTERACAO_CHAVE%>', document.estrategiasForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ESTRATEGIAS_ALTERACAO_CHAVE%>', document.estrategiasForm.imgGravar);
			desabilitaCamposEstrategia();
		}	
	</script>
</logic:equal>

<script>
	habilitaListaEmpresas();
	setaArquivoXml("CS_ASTB_IDIOMAESTRATEGIA_IDTR.xml");
	habilitaTelaIdioma();
</script>
</html>
