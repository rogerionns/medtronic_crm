<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 

response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<SCRIPT TYPE="text/javascript" SRC="webFiles/cobranca/js/infoBoleto.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" SRC="webFiles/cobranca/js/pt/util.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" SRC="webFiles/funcoes/number.js"></SCRIPT>
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">

<script type="text/javascript">

var modalWin;

function MM_openBrWindow(theURL,winName,features) { //v2.0
	modalWin = window.open(theURL,winName,features);
	if (modalWin!=null && !modalWin.closed){
		//self.blur();
		modalWin.focus();
	}
}

function setFunction(cRetorno, campo){
	document.getElementById("divDescricoes").innerHTML = cRetorno;
	document.forms[0].inboDsDescricoes.value=cRetorno;
}

	function inicio(){
		//setaAssociacaoMultiEmpresa();
		
		//setaChavePrimaria(document.forms[0].elements["infoBoleto"].value);
		
		if(document.forms[0].inboDsAceite.value == 'S') {
			document.forms[0].inboDsAceite.checked = true;
		} else {
			document.forms[0].inboDsAceite.checked = false;
		}
	}
	
	function checkAceite() {
		if(document.forms[0].inboDsAceite.checked) {
			document.forms[0].inboDsAceite.value = 'S';
		} else {
			document.forms[0].inboDsAceite.value = 'N';
		}
	}
	
	function visualizaBoletoAux() {		      
		if(document.forms[0].inboDsNossoNumero.value == '') {
			document.getElementById("trVisualizarBoleto").style.display = 'table-row';
			alert('Por favor digite o Nosso N�mero!');
			document.forms[0].inboDsNossoNumero.focus();
			return false;
		}		
		
		if(document.forms[0].inboNrNumdocumento.value == '') {
			document.getElementById("trVisualizarBoleto").style.display = 'table-row';
			alert('Por favor digite o N� Documento!');
			document.forms[0].inboNrNumdocumento.focus();
			return false;
		}
		
		visualizarBoleto();
	}
	
	function visualizarBoleto() {	
		//var url = 'infoBoleto.do?userAction=visualizarBoleto';
		var url = 'webFiles/cobranca/pages/gerarBoleto.jsp';
		url += '?banco=' + document.forms[0].idPcoCdBancoBoleto.value;
		url += '&agencia=' + document.forms[0].inboDsAgencia.value;
		url += '&dvAgencia=' + document.forms[0].inboNrDigitoagencia.value;
		url += '&cc=' + document.forms[0].inboDsConta.value;
		url += '&dvCc=' + document.forms[0].inboNrDigitoconta.value;
		url += '&localPagamento1=' + document.forms[0].inboDsLocalPagamento1.value;
		url += '&localPagamento2=' + document.forms[0].inboDsLocalPagamento2.value;
		url += '&cedente=' + document.forms[0].inboDsCedente.value;
		url += '&instrucao1=' + document.forms[0].inboDsInstrucoes.value;
		url += '&numeroDocumento=' + document.forms[0].inboNrNumdocumento.value;
		url += '&especieDoc=' + document.forms[0].inboDsEspecie.value;
		url += '&aceite=' + document.forms[0].inboDsAceite.value;
		url += '&carteira=' + document.forms[0].inboDsCarteira.value;
		url += '&especieMoeda=' + document.forms[0].inboDsMoeda.value;
		url += '&quantidadeMoeda=' + document.forms[0].inboNrQtdmoeda.value;
		url += '&valorMoeda=' + document.forms[0].inboVlMoeda.value;
		url += '&nossoNumero=' + document.forms[0].inboDsNossoNumero.value;
		url += '&caminhoImagem=' + document.forms[0].inboDsPathImagen.value;
		url += '&inboDsTamnossonumero=' + document.forms[0].inboDsTamnossonumero.value;
		url += '&inboNrCodfornecidoagencia=' + document.forms[0].inboNrCodfornecidoagencia.value;
		

		url += '&dataProcessamento=01/01/2000';		
		url += '&dataDocumento=01/01/2000';
		url += '&codCliente=100543';
		url += '&nomeSacado=Nome do Sacado';
		url += '&cpfSacado=123.456.789-10';
		url += '&enderecoSacado=Av. Paulista, 1000';
		url += '&bairroSacado=Cerqueira Cesar';
		url += '&cidadeSacado=S�O PAULO';
		url += '&ufSacado=SP';
		url += '&cepSacado=04560-011';
		url += '&vencimento=25/12/2009';
		url += '&valorDocumento=1.00';
		url += '&tipoModalidadeCarteira=102';
				
		//ifrmVisualizaBoleto.location.href = url;
		
		window.open(url,'boleto', '');
		
		//document.getElementById("trVisualizarBoleto").style.display = 'none';
		
		//O boleto n�o abre com showModalDialog
		//showModalDialog(url, 0, 'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	}
	
	function numberValidate( obj, digits, dig1, dig2, evnt ) {
      var posValue = 0;
      var valueChar;
      var strRetNumber;
      
		//Nao eh keypress
		if (evnt.keyCode != 0){			
			var caracter = String.fromCharCode(evnt.keyCode);				
			var isDigito = (caracter == dig1 || caracter == dig2);
			
		    if (((evnt.keyCode < 48 && !isDigito) || (evnt.keyCode > 57 && !isDigito)) && evnt.keyCode != 8){				
		        evnt.returnValue = null;
		        return false;
			}
			
		}

      var value = prepare( obj.value, dig2 );
      
      if ( value == '' )
      {
         return true;
      }
      
      var part1, part2;
      
      var pos = value.lastIndexOf( dig2 );
      if ( pos == -1 )
      {
         part1 = value;
         part2 = '';
      }
      else
      {
         part1 = value.substring( 0, pos );
         part2 = value.substring( pos + 1, value.length );
      }
      
      part1 = getDigitsOf( part1 );
      part2 = getDigitsOf( part2 );
      
      if ( digits == 0 )
      {
         strRetNumber = getDigitsOf( part1 );
      }
      else
      {
         
         if ( part2.length <= digits )
         {
            var len = digits - part2.length;
            for( pos = 0; pos < len; pos++ )
            {
               part2 = part2 + "0";
            }
         }
         else
         {
            part2 = part2.substring( 0, digits );
         }
   
         var size = part1.length;
         
         strRetNumber = "";
         
         for( pos = 0; pos < size; pos++)
         {
            valueChar = part1.charAt( part1.length - pos - 1 );
            
            if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
            {
               strRetNumber = dig1 + strRetNumber;
            }
            strRetNumber = valueChar + strRetNumber;
         }
         
         if ( strRetNumber == '' )
         {
            strRetNumber = '0';
         }
         
         if ( digits > 0 )
         {
            strRetNumber = strRetNumber + dig2 + part2;
         }
      }
      
      obj.value = strRetNumber;
   }
	
   function getDigitsOf(strNumber)
   {
      var number;
      var strRetNumber="";
   
      for (var i=0 ; i < strNumber.length ; i++)
      {
         number = parseInt(strNumber.charAt(i));
         if ( number )
         {
            strRetNumber += strNumber.charAt(i)
         }
         else
         {
            if ( number == 0 )
            {
               strRetNumber += strNumber.charAt(i)
            }
         }
      }
      return strRetNumber;
   }
	
	
	function prepare( value, sep ){
      if ( sep == ',' )
      {
         value = strReplaceAll( value, '.', ',' );
      }
      else if ( sep == '.' )
      {
         value = strReplaceAll( value, ',', '.' );
      }
      
      return value;
   }
   
   function strReplaceAll(str,strFind,strReplace)  {
      var returnStr = str;
      var start = returnStr.indexOf(strFind);
      while (start>=0)
      {
         returnStr = returnStr.substring(0,start) + strReplace + returnStr.substring(start+strFind.length,returnStr.length);
         start = returnStr.indexOf(strFind,start+strReplace.length);
      }
      return returnStr;
   }
   
	function adicionaCamposEspeciais(){
		showModalOpen('infoBoleto.do?userAction=abreCamposEspeciaisInfoBoleto', window, 'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:350px,dialogTop:100px,dialogLeft:650px');
	}
	 
	function pressEnter(ev) {
    	if (ev.keyCode == 13) {
    		pesquisar();
    	}
	}
	
	function adicionarRangeAux() {
		if(document.forms[0].idPcoCdBancoBoleto.value == '') {
			alert('O campo Banco � obrigat�rio!');
			return false;
		}
	
		if(document.forms[0].bonnNrRangeinicial.value == '') {
			alert('O campo Range Inicial � obrigat�rio!');
			return false;
		}
		
		if(document.forms[0].bonnNrRangefinal.value == '') {
			alert('O campo Range Final � obrigat�rio!');
			return false;
		}
		
		if(document.forms[0].bonnNrRegra.value == '') {
			alert('O campo Regra � obrigat�rio!');
			return false;
		}
		
		adicionarRange(document.forms[0].bonnNrRangeinicial.value, document.forms[0].bonnNrRangefinal.value, false, '', document.forms[0].bonnNrRegra.value, '');
		
		//Limpando os campos
	   	document.forms[0].bonnNrRangeinicial.value = '';
	   	document.forms[0].bonnNrRangefinal.value = '';
	   	document.forms[0].bonnNrRegra.value = '';
	}
	
	var coutLstRange = new Number(0);
	
	function adicionarRange(rangeInicial, rangeFinal, rangePrincipal, valorAtual, regra, sequencia) {				
		var textoDiv = '';
		textoDiv += '';
		textoDiv += '<table name=tabRange'+coutLstRange+' id=tabRange'+coutLstRange+' width=95% border=0 cellspacing=0 cellpadding=0 align=center>';
	    textoDiv += '    <tr>'; 
	    textoDiv += '        <td class=principalLstImpar width=5%>&nbsp;<img src=webFiles/images/botoes/lixeira.gif class=geralCursoHand alt=Excluir onclick=excluirRange('+coutLstRange+')></td>';
	    textoDiv += '        <td width=20% class=principalLstImpar>&nbsp;' + rangeInicial + '</td>';
	    textoDiv += '        <td width=20% class=principalLstImpar>&nbsp;' + rangeFinal + '</td>';
	    textoDiv += '        <td width=20% class=principalLstImpar>&nbsp;' + valorAtual + '</td>';
	    textoDiv += '        <td width=12% class=principalLstImpar>&nbsp;' + regra + '</td>';
	    textoDiv += '		 <td width=8% class=principalLstImpar align=center>';
    	textoDiv += '        	<input type=text name=bonnNrSequenciaArray class=principalObjForm value=\''+sequencia+'\' maxlength=8 onkeypress="return validaCampoNumerico(this);" onblur="validaDuplicados(this);"/>';
	    textoDiv += '		 </td>';
	    textoDiv += '		 <td width=25% class=principalLstImpar align=center>';
	    if(rangePrincipal) {
	    	textoDiv += '        <input type=checkbox name=checkInPrincipalArray value=S onclick=verificaClick('+coutLstRange+') checked=true />';
	    	textoDiv += '        <input type=hidden name=bonnInPrincipalArray value="S"/>';
	    } else {
	    	textoDiv += '        <input type=checkbox name=checkInPrincipalArray value=N onclick=verificaClick('+coutLstRange+') />';
	    	textoDiv += '        <input type=hidden name=bonnInPrincipalArray value="N"/>';
	    }    	
	    textoDiv += '        	 <input type=hidden name=bonnNrRangeinicialArray value=\''+rangeInicial+'\'/>';
	    textoDiv += '        	 <input type=hidden name=bonnNrRangefinalArray value=\''+rangeFinal+'\'/>';
	    textoDiv += '        <input type=hidden name=bonnNrRegraArray value=\''+regra+'\'/>';
	    textoDiv += '		</td>';
	    textoDiv += '    </tr>';
	   	textoDiv += '</table>';
	   	
	   	coutLstRange++;
	   	
	   	document.getElementsByName("lstRange").item(0).insertAdjacentHTML("BeforeEnd", textoDiv);
	}
	
	function excluirRange(indice) {
		if (confirm('Deseja excluir o registro?')) {
			objIdTbl = window.document.all.item("tabRange"+indice);
			document.getElementById("lstRange").removeChild(objIdTbl);
			coutLstRange--;
		}
	}
	
	function verificaClick(indice) {
		if(coutLstRange > 1) {
			var statusCheckbox = document.forms[0].checkInPrincipalArray[indice].checked;
							
			for(i = 0; i < document.forms[0].checkInPrincipalArray.length; i++) {
				document.forms[0].checkInPrincipalArray[i].checked = false;
				document.forms[0].bonnInPrincipalArray[i].value = 'N';
			}
			
			document.forms[0].checkInPrincipalArray[indice].checked = statusCheckbox;
			if(statusCheckbox)
				document.forms[0].bonnInPrincipalArray[indice].value = 'S';
			else
				document.forms[0].bonnInPrincipalArray[indice].value = 'N';
		} else if(coutLstRange == 1) {
			if(document.forms[0].checkInPrincipalArray.checked) {
				document.forms[0].bonnInPrincipalArray.value = 'S';
			} else {
				document.forms[0].bonnInPrincipalArray.value = 'N';
			}
		} 
	}
	
	function validaDuplicados(obj) {
		var valor = obj.value;
		var countExistentes = new Number(0);

		if(coutLstRange > 1) {		
			if(valor != '') {				
				for(i = 0; i < document.forms[0].bonnNrSequenciaArray.length; i++) {
					if(document.forms[0].bonnNrSequenciaArray[i].value == valor) {
						countExistentes++;
					}					
				}
			}
		}
		
		if(countExistentes > 1) {
			alert('J� existe uma sequ�ncia com este valor!');
			obj.value = '';
			obj.focus();
			return false;
		}
	}
	
	function setPath(obj){
		document.forms[0].inboDsPathImagen.value=obj.value;
		document.forms[0].dsPathImagen.value=obj.value;
	}
</script>
</head>
 
<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');AtivarPasta('<bean:write name="infoBoletoForm" property="flagAba"/>');verificaCampos('<bean:write name="infoBoletoForm" property="flagAba"/>','<bean:write name="infoBoletoForm" property="flagHabilitaCampos"/>');inicio();">

<html:form action="/infoBoleto" styleId="infoBoletoForm">
<html:hidden property="userAction"/>
<html:hidden property="alterando"/>
<html:hidden property="idInfoBoleto"/>
<html:hidden property="inboDsInstrucoes"  />
<html:hidden property="inboDsDescricoes"  />

<input type="hidden" name="dsCnpj" value=""/>
<input type="hidden" name="dsNome" value=""/>
<input type="hidden" name="inBanco" value=""/>
<input type="hidden" name="dsConta" value=""/>
<input type="hidden" name="dsAgencia" value=""/>
<input type="hidden" name="dsNossoNumero" value=""/>
<input type="hidden" name="dsLocalPagamento1" value=""/>
<input type="hidden" name="dsInstrucoes" value=""/>
<input type="hidden" name="dsPathImagen" value=""/>
<input type="hidden" name="dhInativo" value=""/>
<input type="hidden" name="dsDigitoconta" value=""/>
<input type="hidden" name="dsDigitoagencia" value=""/>
<input type="hidden" name="dsLocalpagamento2" value=""/>
<input type="hidden" name="dsCarteira" value=""/>
<input type="hidden" name="dsAceite" value=""/>
<input type="hidden" name="dsEspecie" value=""/>
<input type="hidden" name="dsQtdmoeda" value=""/>
<input type="hidden" name="dsMoeda" value=""/>
<input type="hidden" name="dsDescricoes" value=""/>
<input type="hidden" name="dsNumdocumento" value=""/>
<input type="hidden" name="dsCedente" value=""/>
<input type="hidden" name="dsDigitoCedente" value=""/>
<input type="hidden" name="dsNumconvenio" value=""/>
<input type="hidden" name="dsCodoperacao" value=""/>
<input type="hidden" name="dsCodfornecidoagencia" value=""/>
<input type="hidden" name="ds_Moeda" value=""/>
<input type="hidden" name="ds_Tamnossonumero" value=""/>


  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
			<td class="principalPstQuadro" height="17" width="166">
				<bean:message key="infoBoleto.title.descricao"/>	
            </td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="300"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="500"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado" name="tdProcurar" id="tdProcurar" onClick="AtivarPasta('pesquisar');"><bean:message key="infoBoleto.janela.procurar"/></td>
                <td class="principalPstQuadroLinkNormalMaior" name="tdInfoBoleto" id="tdInfoBoleto" onClick="AtivarPasta('editar');"><bean:message key="infoBoleto.janela.informacoesDoBoleto"/></td>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr> 
                <td height="465" valign="top"> 
                  <div id="Procurar" style="width:99%; height:380px;display: block;"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="principalLabel"><bean:message key="infoBoletoForm.descricao"/></td>
                      </tr>
                      <tr> 
                        <td width="56%"> 
                          <html:text property="inboDsDescricao" name="infoBoletoForm" styleClass="principalObjForm" onkeyup="pressEnter(event)"/>
                        </td>
                        <td width="44%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="pesquisar();" alt="Pesquisar"></td>
                      </tr>
                      <tr> 
                        <td width="56%">&nbsp;</td>
                        <td width="44%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2"> 
                       <div style="height:300px; overflow: auto;">
                          <table width="99%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLstCab" width="5%">&nbsp;</td>
                              <td class="principalLstCab" width="25%">&nbsp;<bean:message key="infoBoleto.lista.nome"/></td>
                              <td class="principalLstCab" width="20%">&nbsp;<bean:message key="infoBoleto.lista.cnpj"/></td>
                              <td class="principalLstCab" width="20%">&nbsp;&nbsp;<bean:message key="infoBoleto.lista.banco"/></td>
                              <td class="principalLstCab" width="16%">&nbsp;&nbsp;<bean:message key="prompt.tipo"/></td>
                              <td class="principalLstCab" width="14%">&nbsp;&nbsp;<bean:message key="infoBoleto.lista.inativo"/></td>
                            </tr>
                           <logic:notEmpty name="CbCdtbInfoBoletoInbo">
                            	 <logic:notEmpty name="CbCdtbInfoBoletoInbo">
                            	<logic:iterate name="CbCdtbInfoBoletoInbo" id="infoBoleto" indexId="indice">
                            	
                            		<input type="hidden" id="inboDsDesc" value="<bean:write name="infoBoleto" property="field(INBO_DS_DESCRICOES)" />">
                            		<input type="hidden" id="inboDsInst" value="<bean:write name="infoBoleto" property="field(INBO_DS_INSTRUCOES)"/>">
                            	
		                            <tr class="geralCursoHand">
										<td class="principalLstPar" width="5%" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" onclick="excluir('<bean:write name="infoBoleto" property="field(ID_INBO_CD_INFOBOLETO)"/>');" width="14" height="14" class="geralCursoHand" alt="Excluir"/></td>
										<td class="principalLstPar" width="25%" align="left" onclick="alterar(
		                            			'<bean:write name="infoBoleto" property="field(ID_INBO_CD_INFOBOLETO)"/>',
		                            			'<bean:write name="infoBoleto" property="field(INBO_DS_CNPJ)"/>',
		                            			'<bean:write name="infoBoleto" property="field(INBO_DS_NOME)"/>',
		                              			'<bean:write name="infoBoleto" property="field(ID_PCBO_CD_BANCOBOLETO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CONTA)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_AGENCIA)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_NOSSONUMERO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_LOCALPAGAMENTO1)"/>',
		                              			'',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_PATHIMAGEM)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DH_INATIVO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_DIGITOAGENCIA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_DIGITOCONTA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_LOCALPAGAMENTO2)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CARTEIRA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_ACEITE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_ESPECIE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_QTDMOEDA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_VL_MOEDA)" />',
		                              			'',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_NUMDOCUMENTO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CEDENTE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_NUMCONVENIO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_CODOPERACAO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_CODFORNECIDOAGENCIA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_MOEDA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_TAMNOSSONUMERO)" />',
		                              			'<bean:write name="infoBoleto" property="field(ID_TPBO_CD_TIPOBOLETO)" />',
		                              			'<%=indice%>');">&nbsp;<script>acronym('<bean:write name="infoBoleto" property="field(INBO_DS_NOME)"/>', 40);</script></td>
										<td class="principalLstPar" width="20%" align="left" onclick="alterar(
		                            			'<bean:write name="infoBoleto" property="field(ID_INBO_CD_INFOBOLETO)"/>',
		                            			'<bean:write name="infoBoleto" property="field(INBO_DS_CNPJ)"/>',
		                            			'<bean:write name="infoBoleto" property="field(INBO_DS_NOME)"/>',
		                              			'<bean:write name="infoBoleto" property="field(ID_PCBO_CD_BANCOBOLETO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CONTA)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_AGENCIA)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_NOSSONUMERO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_LOCALPAGAMENTO1)"/>',
		                              			'',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_PATHIMAGEM)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DH_INATIVO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_DIGITOAGENCIA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_DIGITOCONTA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_LOCALPAGAMENTO2)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CARTEIRA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_ACEITE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_ESPECIE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_QTDMOEDA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_VL_MOEDA)" />',
		                              			'',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_NUMDOCUMENTO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CEDENTE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_NUMCONVENIO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_CODOPERACAO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_CODFORNECIDOAGENCIA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_MOEDA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_TAMNOSSONUMERO)" />',
		                              			'<bean:write name="infoBoleto" property="field(ID_TPBO_CD_TIPOBOLETO)" />',
		                              			'<%=indice%>');">
		                              			&nbsp;
		                              			<bean:write name="infoBoleto" property="field(INBO_DS_CNPJ)"/>
		                              	</td>		
										<td class="principalLstPar" width="20%" align="left" onclick="alterar(
		                            			'<bean:write name="infoBoleto" property="field(ID_INBO_CD_INFOBOLETO)"/>',
		                            			'<bean:write name="infoBoleto" property="field(INBO_DS_CNPJ)"/>',
		                            			'<bean:write name="infoBoleto" property="field(INBO_DS_NOME)"/>',
		                              			'<bean:write name="infoBoleto" property="field(ID_PCBO_CD_BANCOBOLETO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CONTA)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_AGENCIA)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_NOSSONUMERO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_LOCALPAGAMENTO1)"/>',
		                              			'',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_PATHIMAGEM)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DH_INATIVO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_DIGITOAGENCIA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_DIGITOCONTA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_LOCALPAGAMENTO2)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CARTEIRA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_ACEITE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_ESPECIE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_QTDMOEDA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_VL_MOEDA)" />',
		                              			'',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_NUMDOCUMENTO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CEDENTE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_NUMCONVENIO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_CODOPERACAO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_CODFORNECIDOAGENCIA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_MOEDA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_TAMNOSSONUMERO)" />',
		                              			'<bean:write name="infoBoleto" property="field(ID_TPBO_CD_TIPOBOLETO)" />',
		                              			'<%=indice%>');">
		                              			&nbsp;
		                              			<script>acronym('<bean:write name="infoBoleto" property="field(BCBO_DS_BANCO)"/>', 20);</script>
		                             	</td>
		                             	<td class="principalLstPar" width="16%" onclick="alterar(
		                            			'<bean:write name="infoBoleto" property="field(ID_INBO_CD_INFOBOLETO)"/>',
		                            			'<bean:write name="infoBoleto" property="field(INBO_DS_CNPJ)"/>',
		                            			'<bean:write name="infoBoleto" property="field(INBO_DS_NOME)"/>',
		                              			'<bean:write name="infoBoleto" property="field(ID_PCBO_CD_BANCOBOLETO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CONTA)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_AGENCIA)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_NOSSONUMERO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_LOCALPAGAMENTO1)"/>',
		                              			'',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_PATHIMAGEM)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DH_INATIVO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_DIGITOAGENCIA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_DIGITOCONTA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_LOCALPAGAMENTO2)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CARTEIRA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_ACEITE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_ESPECIE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_QTDMOEDA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_VL_MOEDA)" />',
		                              			'',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_NUMDOCUMENTO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CEDENTE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_NUMCONVENIO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_CODOPERACAO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_CODFORNECIDOAGENCIA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_MOEDA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_TAMNOSSONUMERO)" />',
		                              			'<bean:write name="infoBoleto" property="field(ID_TPBO_CD_TIPOBOLETO)" />',
		                              			'<%=indice%>');">
		                              			<logic:equal name="infoBoleto" property="field(ID_TPBO_CD_TIPOBOLETO)" value="1">
		                              				NEGOCIA��O/ACORDO
		                              			</logic:equal>
		                              			<logic:equal name="infoBoleto" property="field(ID_TPBO_CD_TIPOBOLETO)" value="2">
		                              				BOLETAGEM
		                              			</logic:equal>
		                              			
		                              	</td>
										<td class="principalLstPar" width="12%" onclick="alterar(
		                            			'<bean:write name="infoBoleto" property="field(ID_INBO_CD_INFOBOLETO)"/>',
		                            			'<bean:write name="infoBoleto" property="field(INBO_DS_CNPJ)"/>',
		                            			'<bean:write name="infoBoleto" property="field(INBO_DS_NOME)"/>',
		                              			'<bean:write name="infoBoleto" property="field(ID_PCBO_CD_BANCOBOLETO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CONTA)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_AGENCIA)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_NOSSONUMERO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_LOCALPAGAMENTO1)"/>',
		                              			'',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_PATHIMAGEM)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DH_INATIVO)"/>',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_DIGITOAGENCIA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_DIGITOCONTA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_LOCALPAGAMENTO2)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CARTEIRA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_ACEITE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_ESPECIE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_QTDMOEDA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_VL_MOEDA)" />',
		                              			'',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_NUMDOCUMENTO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_CEDENTE)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_NUMCONVENIO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_CODOPERACAO)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_NR_CODFORNECIDOAGENCIA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_MOEDA)" />',
		                              			'<bean:write name="infoBoleto" property="field(INBO_DS_TAMNOSSONUMERO)" />',
		                              			'<bean:write name="infoBoleto" property="field(ID_TPBO_CD_TIPOBOLETO)" />',
		                              			'<%=indice%>');">
		                              			&nbsp;
		                              			<bean:write name="infoBoleto" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(INBO_DH_INATIVO)" filter="html"/>
		                              	</td>
									  </tr>
									  <script>
		                            	countRegistro++;
		                              </script>
                            	</logic:iterate>
                            </logic:notEmpty> 
                            </logic:notEmpty>
							<logic:empty name="infoBoleto">
								<tr> 
									<td align="center" colspan="7" > 
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
											<tr id="nenhumRegistro" ><td height="260" width="800" align="center"  class="principalLstPar"><br><b><bean:message key="lista.nenhum.registro.encontrado"/> </b></td></tr>
										</table>
									&nbsp;</td>
								</tr>
							</logic:empty>                            
                          </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
				  <div id="InfoBoleto" style="overflow:auto; width:99%; height:460px;display: none; ">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="16%"> 
                          <bean:message key="prompt.codigo"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="76%"> 
                          <html:text property="idInboCdInfoBoleto" name="infoBoletoForm" styleClass="principalObjForm" disabled="true" style="width: 120px"/>
                        </td>
                        <td width="9%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="16%"> 
                          <bean:message key="infoBoletoForm.cnpj"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="76%"> 
                          <html:text property="inboDsCnpj" maxlength="15" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this);return formataCNPJ(this);" onblur="formataCNPJ(this); return false;" style="width: 120px"/>
                        </td>
                        <td width="9%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="16%">
							<bean:message key="infoBoletoForm.nome"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="76%"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%" class="principalLabel"> 
                              	<html:text property="inboDsNome" maxlength="80" name="infoBoletoForm" styleClass="principalObjForm" style="width: 388px"/>
                              </td>
                              <td width="27%" class="principalLabel">
                              	<logic:equal value="" name="infoBoletoForm" property="inboDhInativo">
                              		<input type="checkbox" name="inativo" id="inativo" value="ativo"/>                              	                              	
                              	</logic:equal>
                              	<logic:notEqual value="" name="infoBoletoForm" property="inboDhInativo">                                
                              		<input type="checkbox" name="inativo" checked="checked" value="inativo"/>                              	
                              	</logic:notEqual> 	                              	          
                              	<bean:message key="infoBoletoForm.inativo"/>
                              	<html:hidden property="check" name="infoBoletoForm"/>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                        </td>
                        <td width="9%" class="principalLabel">&nbsp;</td>
                      </tr>                      
					</table>
					<!-- INICIO ABAS -->                    
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td valign="top">
							<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
							  <tr> 
							    <td class="principalPstQuadroLinkSelecionado" name="tdDadosBoleto" id="tdDadosBoleto" onclick="AtivarPasta('DADOSBOLETO')"><bean:message key="prompt.DadosBoleto" /></td>
							    <td class="principalPstQuadroLinkNormal" name="tdNossoNumero" id="tdNossoNumero" onclick="AtivarPasta('NOSSONUMERO')"><bean:message key="infoBoletoForm.nossoNumero" /> </td>
							    <td >&nbsp;</td>
							    <td class="principalLabel">&nbsp;</td>
							  </tr>
							</table>
							<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro" height="440">
							  <tr>
							    <td valign="top">
							    	<div id="dadosboleto" style="width:100%;display: none;">	
							    		<div id="executaEstrategia" style="width:68px; height:27px;float: left"> 
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr align="center"> 
													<td>&nbsp;</td>
													<td><img src="webFiles/images/botoes/Acao_Programa.gif" name="imgVisualizarBoleto" id="imgVisualizarBoleto" width="30" height="30" alt="Visualizar Boleto" title="Visualizar Boleto" class="geralCursoHand" onclick="visualizaBoletoAux()"></td>
												</tr>
											</table>
									    </div>
									    <div style="width: 100%;float: right;">	      
								    	<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
					                       <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">
												<bean:message key="prompt.tipo"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%"> 
						                               <html:select property="idTpboCdTipoboleto" name="infoBoletoForm" styleClass="principalObjForm">
						                            		<html:option value="">-- Selecione uma Op��o --</html:option>
						                            		<html:option value="1">NEGOCIA��O/ACORDO</html:option>
						                            		<html:option value="2">BOLETAGEM</html:option>
						                            	</html:select>
					                              </td>
					                              <td width="27%" class="principalLabel">&nbsp;</td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>	
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">
												<bean:message key="infoBoletoForm.banco"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%"> 
						                               <html:select property="idPcoCdBancoBoleto" name="infoBoletoForm" styleClass="principalObjForm">
						                            		<html:option value="">-- Selecione uma Op��o --</html:option>
						                            		<logic:notEmpty name="cbDmtbBancoboletoBcbo">
						                            			<bean:define name="cbDmtbBancoboletoBcbo" id="boletoVO" />
						                            			<html:options collection="boletoVO"  property="field(ID_PCBO_CD_BANCOBOLETO)" labelProperty="field(BCBO_DS_BANCO)"/>
						                            		</logic:notEmpty>
						                            	</html:select>
					                              </td>
					                              <td width="27%" class="principalLabel">&nbsp;</td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>					                      
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">
												<bean:message key="infoBoletoForm.ag�ncia"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="20%"> 
					                                <html:text property="inboDsAgencia" maxlength="20" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/>
					                              </td>
					                              <td width="80%">
					                              	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
							                            <tr> 
							                              <td width="30%" class="principalLabel"  align="right"> 
							                                &nbsp;&nbsp;<bean:message key="infoBoletoForm.digitoAg�ncia"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							                              </td>
							                              <td width="20%">
							                              	<html:text property="inboNrDigitoagencia" maxlength="4" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/>
							                              </td>
							                              <td width="50%" class="principalLabel"> 
							                                &nbsp;
							                              </td>
							                            </tr>
							                          </table>
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">
												<bean:message key="infoBoletoForm.conta"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="20%"> 
					                                <html:text property="inboDsConta" maxlength="20" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/>
					                              </td>
					                              <td width="80%" class="principalLabel">
					                              	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
							                            <tr> 
							                              <td width="30%" class="principalLabel" align="right"> 
							                                &nbsp;&nbsp;<bean:message key="infoBoletoForm.digitoConta"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							                              </td>
							                              <td width="20%">
							                              	<html:text property="inboNrDigitoconta" maxlength="4" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/>
							                              </td>
							                              <td width="50%" class="principalLabel"> 
							                                &nbsp;
							                              </td>
							                            </tr>
							                          </table>
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">
												<bean:message key="infoBoletoForm.carteira"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="20%"> 
					                                <html:text property="inboDsCarteira" name="infoBoletoForm" maxlength="20" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/>
					                              </td>
					                              <td width="80%" class="principalLabel">
					                              	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
							                            <tr> 
							                              <td width="30%" class="principalLabel" align="right"> 
							                                &nbsp;&nbsp;<bean:message key="infoBoletoForm.especie"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							                              </td>
							                              <td width="20%">
							                              	<html:text property="inboDsEspecie" maxlength="20" name="infoBoletoForm" styleClass="principalObjForm"  style="width: 90px"/>
							                              </td>
							                              <td width="50%" class="principalLabel"> 
							                                &nbsp;
							                              </td>
							                            </tr>
							                          </table>
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">
												<bean:message key="infoBoletoForm.quantidadeMoeda"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="20%"> 
					                                <html:text property="inboNrQtdmoeda" name="infoBoletoForm" maxlength="9" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/>
					                              </td>
					                              <td width="80%" class="principalLabel">
					                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							                            <tr> 
							                              <td width="30%" class="principalLabel" align="right"> 
							                                &nbsp;&nbsp;<bean:message key="infoBoletoForm.valorMoeda"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							                              </td>
							                              <td width="20%">
							                              	<html:text property="inboVlMoeda" maxlength="10" name="infoBoletoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 2, '.', ',', event);" style="width: 90px"/>
							                              </td>
							                              <td width="50%" class="principalLabel"> 
							                                &nbsp;
							                              </td>
							                            </tr>
							                          </table>
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%" title="<bean:message key="prompt.CampoUtilizadoParaOBanco" /> <bean:message key="prompt.CAIXAECONOMICA" />/<bean:message key="infoBoletoForm.santander" />">
												<bean:message key="infoBoletoForm.codigoFornecidoAgencia"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="20%" title="<bean:message key="prompt.CampoUtilizadoParaOBanco" /> <bean:message key="prompt.CAIXAECONOMICA" />/<bean:message key="infoBoletoForm.santander" />"> 
					                                <html:text property="inboNrCodfornecidoagencia" name="infoBoletoForm" maxlength="9" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/>
					                              </td>
					                              <td width="80%" class="principalLabel">
					                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							                            <tr> 
							                              <td width="30%" class="principalLabel" align="right" title="<bean:message key="prompt.CampoUtilizadoParaOBanco" /> <bean:message key="prompt.CAIXAECONOMICA" />"> 
							                                &nbsp;&nbsp;<bean:message key="infoBoletoForm.codigoOperacao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							                              </td>
							                              <td width="20%" title="<bean:message key="prompt.CampoUtilizadoParaOBanco" /> <bean:message key="prompt.CAIXAECONOMICA" />">
							                              	<html:text property="inboNrCodoperacao" maxlength="9" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false;" style="width: 90px"/>
							                              </td>
							                              <td width="50%" class="principalLabel"> 
							                                &nbsp;
							                              </td>
							                            </tr>
							                          </table>
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%" title="<bean:message key="prompt.CampoUtilizadoParaOBanco" /> <bean:message key="prompt.CAIXAECONOMICA" />">
												<bean:message key="infoBoletoForm.numeroMoeda"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="20%" title="<bean:message key="prompt.CampoUtilizadoParaOBanco" /> <bean:message key="prompt.CAIXAECONOMICA" />"> 
					                                <html:text property="inboDsMoeda" name="infoBoletoForm" maxlength="9" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/>
					                              </td>
					                              <td width="80%" class="principalLabel">
					                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							                            <tr> 
							                              <td width="30%" class="principalLabel" align="right" title="<bean:message key="prompt.CampoUtilizadoParaOBanco" /> <bean:message key="prompt.CAIXAECONOMICA" />"> 
							                                &nbsp;&nbsp;<bean:message key="infoBoletoForm.tamNossoNum"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							                              </td>
							                              <td width="20%" title="<bean:message key="prompt.CampoUtilizadoParaOBanco" /> <bean:message key="prompt.CAIXAECONOMICA" />">
							                              	<html:text property="inboDsTamnossonumero" maxlength="9" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/>
							                              </td>
							                              <td width="50%" class="principalLabel"> 
							                                &nbsp;
							                              </td>
							                            </tr>
							                          </table>
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      
					                      
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%" title="<bean:message key="prompt.CampoUtilizadoParaOBanco" /> <bean:message key="prompt.BANCODOBRASIL" />">
												<bean:message key="infoBoletoForm.numeroConvenio"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%" title="<bean:message key="prompt.CampoUtilizadoParaOBanco" /> <bean:message key="prompt.BANCODOBRASIL" />"> 
					                                <html:text property="inboNrNumconvenio" maxlength="9" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 345px"/>
					                              </td>
					                              <td width="27%" class="principalLabel">&nbsp; </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">
												<bean:message key="infoBoletoForm.cedente"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%"> 
					                                <html:text property="inboDsCedente" maxlength="100" name="infoBoletoForm" styleClass="principalObjForm" style="width: 345px"/>
					                              </td>
					                              <td width="27%" class="principalLabel">&nbsp; </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">
												<bean:message key="infoBoletoForm.localDePagamento1"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%"> 
					                                <html:text property="inboDsLocalPagamento1" maxlength="100" name="infoBoletoForm" styleClass="principalObjForm" style="width: 345px"/>
					                              </td>
					                              <td width="27%" class="principalLabel">&nbsp; </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">
												<bean:message key="infoBoletoForm.localDePagamento2"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%"> 
					                                <html:text property="inboDsLocalPagamento2" maxlength="100" name="infoBoletoForm" styleClass="principalObjForm" style="width: 345px"/>
					                              </td>
					                              <td width="27%" class="principalLabel">&nbsp; </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>                      
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%" valign="top">
												<bean:message key="infoBoletoForm.instrucoes"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7" > 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%" height="35px"> 
					                                <!--<html:textarea property="inboDsInstrucoes" name="infoBoletoForm" styleClass="principalObjForm" style="width:397px; height:40px;" onkeyup="return isMaxLength(this, 2000)"/>-->
					                                <div id="divInstrucoes" class="principalLabel" style="overflow: auto; width: 397px; height: 30px; background-color: #FFFFFF; border: 1px none #000000; display: block;"></div>
					                                <script>
										    			document.getElementById("divInstrucoes").innerHTML = document.forms[0].inboDsInstrucoes.value;
										       		</script>
					                              </td>
					                              <td width="27%" class="principalLabel">
					                              	&nbsp;
					                              	<img src="webFiles/images/botoes/bt_CriarCarta.gif" name="imgCarta" id="imgCarta" alt="Editar instru��es" class="geralCursoHand" onclick="adicionaCamposEspeciais()">
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%" valign="top">
												<bean:message key="infoBoletoForm.descricoes"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7" > 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%" height="35px"> 
					                                <div id="divDescricoes" class="principalLabel" style="overflow: hidden; width: 397px; height: 30px; background-color: #FFFFFF; border: 1px none #000000; display: block;">
					                                	<script>
					                                		document.write(document.forms[0].inboDsDescricoes.value);
													    </script>
					                                </div>
					                              </td>
					                              <td width="27%" class="principalLabel">
					                              	&nbsp;
					                              	<img src="webFiles/images/botoes/bt_CriarCarta.gif" name="imgCarta" id="imgCarta" alt="Editar Descricoes" class="geralCursoHand" onClick="MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=document.forms[0].inboDsDescricoes&carta=false','Documento','width=850,height=494,top=0,left=0')">
					                              </td>
					                            </tr>
					                          </table>
					                          
					                        
					                          <!-- table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%"> 
					                                <html:textarea property="inboDsDescricoes" name="infoBoletoForm" styleClass="principalObjForm" style="width:397px; height:40px;" onkeyup="return isMaxLength(this, 2000)"/>
					                              </td>
					                              <td width="27%" class="principalLabel">&nbsp; </td>
					                            </tr>
					                          </table -->
					                          
					                          
					                          
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">
												<bean:message key="infoBoletoForm.imagemSuperiorDoBoleto"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%">
					                                <html:file property="inboDsPathImagenAux" maxlength="1000" styleClass="principalObjForm" onchange="setPath(this)"/>
					                              </td>
					                              <td width="27%" class="principalLabel">&nbsp; </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">&nbsp;</td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="77%">
					                                <html:text property="inboDsPathImagen" name="infoBoletoForm" styleClass="principalObjForm" readonly="true" />
					                              </td>
					                              <td width="27%" class="principalLabel">&nbsp; </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="22%">&nbsp;</td>
					                        <td class="principalLabel" align="right"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="27%" class="principalLabel" align="left"> 
					                              	<logic:equal value="S" name="infoBoletoForm" property="inboDsAceite">
					                              		<input type="checkbox" name="inboDsAceite" checked="checked" value="S" onclick="checkAceite()"/>                              	                              	
					                              	</logic:equal>
					                              	<logic:notEqual value="S" name="infoBoletoForm" property="inboDsAceite">                                
					                              		<input type="checkbox" name="inboDsAceite" value="N" onclick="checkAceite()"/>                              	
					                              	</logic:notEqual> 	                              	          
					                              	<bean:message key="infoBoletoForm.aceite"/>
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                      <!-- Os campos Nosso N�mero e N� Documento s� devem ser exibidos quando clicado no bot�o de visualizar boleto -->
					                      <tr id="trVisualizarBoleto" style="display: table-row"> 
					                        <td class="principalLabel" align="right" height="28" width="26%">
												<bean:message key="infoBoletoForm.nossoNumero"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td colspan="2" class="principalLabel"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="20%"> 
					                                <html:text property="inboDsNossoNumero" name="infoBoletoForm" maxlength="20" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;visualizaBoletoAux();" style="width: 90px"/>
					                              </td>
					                              <td width="80%" class="principalLabel">
					                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							                            <tr> 
							                              <td width="30%" class="principalLabel" align="right"> 
							                                &nbsp;&nbsp;<bean:message key="infoBoletoForm.numDocumento"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							                              </td>
							                              <td width="20%">
							                              	<html:text property="inboNrNumdocumento" maxlength="20" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;visualizaBoletoAux();" style="width: 90px"/>
							                              </td>
							                              <td width="50%" class="principalLabel"> 
							                                &nbsp;
							                              </td>
							                            </tr>
							                          </table>
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                    </table>
					                    </div>
					            	</div>
					            	<div id="nossonumero" style="width:100%;display: none">
					            		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="26%">
												<bean:message key="prompt.RangeInicial"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td width="30%" class="principalLabel"> 
					                          <html:text property="bonnNrRangeinicial" maxlength="10" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
					                        </td>
					                        <td width="44%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                	</table>
					                	<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="26%">
												<bean:message key="prompt.RangeFinal"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td width="30%" class="principalLabel"> 
					                          <html:text property="bonnNrRangefinal" maxlength="10" name="infoBoletoForm" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
					                        </td>
					                        <td width="44%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                	</table>
					                	<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="26%">
												<bean:message key="prompt.regra"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td width="30%" class="principalLabel"> 
					                          <html:select property="bonnNrRegra" styleClass="principalObjForm">
					                          	<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/></html:option>
					                          	<html:option value="1">1 - <bean:message key="prompt.Contrato"/></html:option>
					                          	<html:option value="2">2 - <bean:message key="infoBoletoForm.nossoNumero"/></html:option>
					                          </html:select>
					                        </td>
					                        <td width="44%" class="principalLabel" colspan="2">&nbsp;</td>
					                      </tr>
					                	</table>
					                	<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
					                      <tr> 
					                        <td class="principalLabel" align="right" height="28" width="26%">
												&nbsp; 
					                        </td>
					                        <td width="30%" class="principalLabel" align="right"> 
					                          <!--<html:checkbox property="bonnInPrincipal" value="S"></html:checkbox>
					                          &nbsp;<bean:message key="prompt.EmUso" />-->
					                        </td>
					                        <td width="22%" class="principalLabel" align="center">&nbsp;<img src="webFiles/images/icones/setaDown.gif" name="imgAddRange" id="imgAddRange"  class="geralCursoHand" alt="Adicionar Range" onclick="adicionarRangeAux()"></td>
					                        <td width="22%" class="principalLabel">&nbsp;</td>
					                      </tr>
					                	</table>
					                	<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
					                		<tr> 
								            	<td colspan="7" class="EspacoPequeno">&nbsp;</td>
								            </tr>
									        <tr> 
									            <td class="principalLstCab" width="5%">&nbsp; </td>
									            <td width="20%" class="principalLstCab">&nbsp;<bean:message key="prompt.RangeInicial"/></td>
									            <td width="20%" class="principalLstCab"><bean:message key="prompt.RangeFinal"/></td>
									            <td width="20%" class="principalLstCab"><bean:message key="prompt.ValorAtual"/></td>
									            <td width="12%" class="principalLstCab"><bean:message key="prompt.regra"/></td>
									            <td width="8%" class="principalLstCab"><bean:message key="prompt.sequencia"/></td>
									            <td width="15%" class="principalLstCab" align="center"><bean:message key="prompt.EmUso"/></td>
									        </tr>
									   </table>
									   <div id="lstRange" name="lstRange" style="width:100%; height:315px;display: block; overflow: auto;"></div>
					            	</div>
							    </td>
							  </tr>
							</table>	
					    </td>
                      </tr>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/new.gif" alt="Novo" name="imgNovo" id="imgNovo" class="geralCursoHand" onclick="novo();"/>
                      </td>
                      <td width="25" align="center">
                      	<img src="webFiles/images/botoes/gravar.gif" alt="Gravar" name="imgGravar" id="imgGravar" class="geralCursoHand" onclick="salvar();"/>
                      </td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/cancelar.gif" alt="Cancelar" name="imgCancelar" id="imgCancelar" class="geralCursoHand" onclick="cancelar()"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <iframe id="ifrmVisualizaBoleto" name="ifrmVisualizaBoleto" src="" width="0%" height="0%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	<div id="camposEspeciais" title="Campos Especias">
		<div id="camposEspeciaisDetail"></div>
	</div>
  </html:form>
</body>

<!-- Adicionando as associacoes de nossonumero x boleto j� gravadas para este registro -->
<logic:present name="cbAdtbBoletoNossonumeroBonn">
	<logic:iterate name="cbAdtbBoletoNossonumeroBonn" id="cbAdtbBoletoNossonumeroBonn">
		<script>
			adicionarRange('<bean:write name="cbAdtbBoletoNossonumeroBonn" property="field(bonn_nr_rangeinicial)"/>',
						   '<bean:write name="cbAdtbBoletoNossonumeroBonn" property="field(bonn_nr_rangefinal)"/>',
						   '<bean:write name="cbAdtbBoletoNossonumeroBonn" property="field(bonn_in_principal)"/>' == 'S' ? true : false,
						   '<bean:write name="cbAdtbBoletoNossonumeroBonn" property="field(bonn_nr_posicaoatual)"/>',
						   '<bean:write name="cbAdtbBoletoNossonumeroBonn" property="field(bonn_nr_regra)"/>',
						   '<bean:write name="cbAdtbBoletoNossonumeroBonn" property="field(bonn_nr_sequencia)"/>');
		</script>
	</logic:iterate>
</logic:present>


<script>
	temPermissaoIncluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_BOLETO_INCLUSAO_CHAVE%>');
	temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_BOLETO_ALTERACAO_CHAVE%>');
	temPermissaoExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_BOLETO_EXCLUSAO_CHAVE%>');
	
	function verificaPermissaoExcluir() {
		if (!temPermissaoExcluir){
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.infoBoletoForm.btExcluir'+i+');');
			}
		}
	}
	
	verificaPermissaoExcluir();
</script>

<logic:equal name="infoBoletoForm" property="userAction" value="init">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_BOLETO_ALTERACAO_CHAVE%>', document.infoBoletoForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_BOLETO_ALTERACAO_CHAVE%>', document.infoBoletoForm.imgGravar);
			desabilitaCamposInfoBoletos();
		}
	</script>
</logic:equal>
<logic:equal name="infoBoletoForm" property="userAction" value="alterar">
	<script>
		if(!temPermissaoAlterar) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_BOLETO_ALTERACAO_CHAVE%>', document.infoBoletoForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_BOLETO_ALTERACAO_CHAVE%>', document.infoBoletoForm.imgGravar);
			desabilitaCamposInfoBoletos();
		}
	</script>
</logic:equal>
<logic:equal name="infoBoletoForm" property="userAction" value="cancelar">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_BOLETO_ALTERACAO_CHAVE%>', document.infoBoletoForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_BOLETO_ALTERACAO_CHAVE%>', document.infoBoletoForm.imgGravar);
			desabilitaCamposInfoBoletos();
		}	
	</script>
</logic:equal>	

<script>
	//habilitaListaEmpresas();
	desabilitaListaEmpresas();
</script>
</html>

<script type="text/javascript" SRC="webFiles/funcoes/funcoesMozilla.js"></script>