<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


String fileIncludeIdioma="../../../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/grupoRegraNegociacao.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/number.js"></SCRIPT>
<link rel="stylesheet" href="webFiles/cobranca/css/global.css" type="text/css">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<script type="text/javascript">
	function pressEnter(ev) {
    	if (ev.keyCode == 13) {
    		pesquisar();
    	}
	}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');AtivarPasta('<bean:write name="grupoRegraNegociacaoForm" property="flagAba"/>');verificaCampos('<bean:write name="grupoRegraNegociacaoForm" property="flagAba"/>','<bean:write name="grupoRegraNegociacaoForm" property="flagHabilitaCampos"/>');inicio();">
<html:form action="/grupoRegraNegociacao" method="post">
<input type="hidden" name="grreDhInativo" />
<input type="hidden" name="idGrnpCdGruporeneparcelaExclusao" />
<html:hidden property="userAction"/>
<html:hidden property="alterando"/>
<!-- Campo abaixo esta aqui para n�o aparecer na tela, para voltar descomentar a linha de codigo do campo e retirar a linha abaixo -->
<html:hidden property="grreVlValordescpagvista"/>

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.GrupoRegraNegociacao"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>      
          <td valign="top" height="500"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado" name="tdProcurar" id="tdProcurar" onClick="AtivarPasta('pesquisar');"><bean:message key="prompt.procurar"/></td>
                <td class="principalPstQuadroLinkNormalMaior" name="tdEstrategias" id="tdEstrategias" onClick="AtivarPasta('editar');"><bean:message key="prompt.GrupoRegraNegociacao"/></td>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr> 
                <td height="480" valign="top"> 
                  <div id="Procurar" style="position:absolute; width:99%; height:460px; z-index:1; visibility: visible;"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="principalLabel"><bean:message key="prompt.Descricao"/></td>
                      </tr>
                      <tr> 
                        <td width="56%"> 
                          <html:text property="grreDsGruporenePesquisar" name="grupoRegraNegociacaoForm" styleClass="principalObjForm" maxlength="60" onkeyup="pressEnter(event)"/>  
                        </td>
                        <td width="44%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="pesquisar();" alt="Pesquisar"></td>
                      </tr>
                      <tr> 
                        <td width="56%">&nbsp;</td>
                        <td width="44%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2"> 
                        <div style="height:300px; overflow: auto;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLstCab" width="5%">&nbsp;</td>
                              <td class="principalLstCab" width="5%">&nbsp;</td>
                              <td class="principalLstCab" width="10%"><bean:message key="prompt.codigo"/></td>
                              <td class="principalLstCab" width="60%"><bean:message key="prompt.Descricao"/></td>
                              <td class="principalLstCab" width="20%"><bean:message key="prompt.inativo"/></td>
                            </tr>
                             <logic:notEmpty name="cbCdtbGruporeneGrre" >
								<logic:iterate name="cbCdtbGruporeneGrre" id="cbCdtbGruporeneGrre" indexId="indice">
			                    	<tr class="geralCursoHand"> 
	           						    <td class="principalLstPar" width="5%" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" onclick="excluir('<bean:write name="cbCdtbGruporeneGrre" property="field(ID_GRRE_CD_GRUPORENE)"/>');" width="14" height="14" class="geralCursoHand" alt="Excluir"/></td>
			                            <td class="principalLstPar" width="5%" align="center" onclick="abrirModalIdioma('CB_ASTB_IDIOMAGRUPORENE_IMRN.xml','<bean:write name="cbCdtbGruporeneGrre" property="field(ID_GRRE_CD_GRUPORENE)"/>');"><img src="webFiles/images/botoes/GloboAuOn.gif" width="18" height="18" class="geralCursoHand"/></td>
			                          	<td class="principalLstPar" width="10%"onclick="alterar('<bean:write name="cbCdtbGruporeneGrre" property="field(ID_GRRE_CD_GRUPORENE)"/>',
		                            			'<bean:write name="cbCdtbGruporeneGrre" property="field(GRRE_DS_GRUPORENE)"/>',
		                            			'<bean:write name="cbCdtbGruporeneGrre" property="field(GRRE_VL_VALORDESCPAGVISTA)"/>',
		                              			'<bean:write name="cbCdtbGruporeneGrre" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(GRRE_DH_INATIVO)"filter="html"/>',
		                              			'<bean:write name="cbCdtbGruporeneGrre" property="field(GRRE_DS_COMANDOSQL)"/>');">&nbsp;<bean:write name="cbCdtbGruporeneGrre" property="field(ID_GRRE_CD_GRUPORENE)"/></td>
			                          <td class="principalLstPar" width="60%"onclick="alterar('<bean:write name="cbCdtbGruporeneGrre" property="field(ID_GRRE_CD_GRUPORENE)"/>',
		                            			'<bean:write name="cbCdtbGruporeneGrre" property="field(GRRE_DS_GRUPORENE)"/>',
		                            			'<bean:write name="cbCdtbGruporeneGrre" property="field(GRRE_VL_VALORDESCPAGVISTA)"/>',
		                              			'<bean:write name="cbCdtbGruporeneGrre" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(GRRE_DH_INATIVO)"filter="html"/>',
		                              			'<bean:write name="cbCdtbGruporeneGrre" property="field(GRRE_DS_COMANDOSQL)"/>');">&nbsp;<bean:write name="cbCdtbGruporeneGrre" property="field(GRRE_DS_GRUPORENE)"/></td>		                          			
			                          <td class="principalLstPar" width="20%"onclick="alterar('<bean:write name="cbCdtbGruporeneGrre" property="field(ID_GRRE_CD_GRUPORENE)"/>',
		                            			'<bean:write name="cbCdtbGruporeneGrre" property="field(GRRE_DS_GRUPORENE)"/>',
		                            			'<bean:write name="cbCdtbGruporeneGrre" property="field(GRRE_VL_VALORDESCPAGVISTA)"/>',
		                              			'<bean:write name="cbCdtbGruporeneGrre" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(GRRE_DH_INATIVO)"filter="html"/>',
		                              			'<bean:write name="cbCdtbGruporeneGrre" property="field(GRRE_DS_COMANDOSQL)"/>');">&nbsp;<bean:write name="cbCdtbGruporeneGrre" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(GRRE_DH_INATIVO)" filter="html"/></td>
	                                 </tr>	
	                                 <script>
		                            	countRegistro++;
		                            </script>										
			                      </logic:iterate>
			                      </logic:notEmpty>		
									<logic:empty name="cbCdtbGruporeneGrre">
										<tr> 
											<td align="center" colspan="7" > 
												<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
													<tr id="nenhumRegistro" ><td height="260" width="800" align="center"  class="principalLstPar"><br><b><bean:message key="lista.nenhum.registro.encontrado"/> </b></td></tr>
												</table>
											&nbsp;</td>
										</tr>
									</logic:empty>			                      	                                          
                          </table>
                          </div>
                        </td>
                      </tr>
                      </table>
                  </div>
                  <div id="Estrategias" style="position:absolute; width:99%; height:390px; z-index:2; visibility: hidden;"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                     <tr>
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="prompt.codigo"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="20%"> 
                                <html:text property="idGrreCdGruporene" styleId="idGrreCdGruporene" name="grupoRegraNegociacaoForm" styleClass="principalObjForm" disabled="true"/>
                              </td>
                              <td width="80%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel">&nbsp;</td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="prompt.Descricao"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                                <html:text property="grreDsGruporene" maxlength="60" name="grupoRegraNegociacaoForm" styleClass="principalObjForm"/>
                              </td>
                              <td width="27%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%">&nbsp;<bean:message key="prompt.comandoSql"/>  
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%">
                                <html:textarea property="grreDsComandosql" styleClass="principalObjForm"></html:textarea>
                              </td>
                              <td width="27%" class="principalLabel">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%">&nbsp;</td>
                        <td colspan="2" class="principalLabel" align="right"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="61%">&nbsp; </td>
                              <td width="30%" class="principalLabel">   
								<logic:equal value="" name="grupoRegraNegociacaoForm" property="grreDhInativo">
                              		<input type="checkbox" name="inativo" value="ativo"/>                              	                              	
                              	</logic:equal>
                              	<logic:notEqual value="" name="grupoRegraNegociacaoForm" property="grreDhInativo">                                
                              		<input type="checkbox" name="inativo" value="inativo" checked="checked"/>                              	
                              	</logic:notEqual> 	                              	          
                              	<bean:message key="prompt.inativo"/>
                             </td>
							 <td width="9%" align="right">&nbsp;<img src="webFiles/images/botoes/setaDown.gif" name="imgSetaDown" id="imgSetaDown" width="21" height="18" class="geralCursoHand" onclick="adicionarLinhaEmBranco()" alt="Adicionar Linha" ></td>
                            </tr>
                          </table>                          
                        </td>
                        <td width="4%" class="principalLabel" colspan="2"></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              <tr> 
		                <td class="EspacoPequeno">&nbsp;</td>
		              </tr>
		            </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		              <tr> 
		                <td>
		                	<div id="tituloLista" style="scroll: no; overflow: hidden; width: 100%; z-index: 1; position: relative">
								<table  border="0" width="100%" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="principalLstCab" width="5%" align="center">&nbsp;</td>
							        	<td class="principalLstCab" width="15%" align="center">&nbsp;<bean:message key="prompt.ParcelamentoInicial" /></td>
							        	<td class="principalLstCab" width="20%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.ParcelamentoFinal" /></td>
							        	<td class="principalLstCab" width="20%">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.TaxaDeParcelamento" /></td>
							        	<td class="principalLstCab" width="20%">&nbsp;&nbsp;&nbsp;<bean:message key="prompt.DescontoInicial" /></td>
							        	<td class="principalLstCab" width="20%">&nbsp;&nbsp;<bean:message key="prompt.DescontoFinal" /></td>
							        	<!--
							        	<td class="principalLstCab" width="15%"><bean:message key="prompt.TaxaDeDesconto" /></td>
							        	-->
									</tr>
								</table> 
							</div>
							<div id="lstRegistro" style="overflow: scroll; height: 310px; width: 100%; z-index: 2; position: absolute" onScroll="tituloLista.scrollLeft=this.scrollLeft;"> 
								<logic:present name="cbAstbGruporeneparcelaGrnpVector">
									<logic:iterate name="cbAstbGruporeneparcelaGrnpVector" id="cbAstbGruporeneparcelaGrnpVector" indexId="indice">
										<div id="divRegistro<%=indice.intValue()%>" name="divRegistro<%=indice.intValue()%>">
											<table id="table<%=indice.intValue()%>" width="100%"  border="0" cellspacing="0" cellpadding="0">
												<tr> 
													<input type="hidden" name="idGrnpCdGruporeneparcela" value="<bean:write name="cbAstbGruporeneparcelaGrnpVector" property="field(id_grnp_cd_gruporeneparcela)" />" />
													<td class="intercalaLst<%=indice.intValue()%2%>" width="5%" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluiCampo<%=indice.intValue()%>" id="btExcluiCampo<%=indice.intValue()%>" class="geralCursoHand" alt="<bean:message key="prompt.excluir"/>"  onclick="excluirRegistroGrupoParcela('<%=indice.intValue()%>');" /></td>
										        	<td class="intercalaLst<%=indice.intValue()%2%>" width="15%">
										        		<input type="text" name="grnpNrParcelamentoinicial" maxlength="255" class="principalObjForm" onkeypress="return isDigito(event);" value="<bean:write name="cbAstbGruporeneparcelaGrnpVector" property="field(grnp_nr_parcelamentoinicial)" />" />
										        	</td>
										        	<td class="intercalaLst<%=indice.intValue()%2%>" width="20%">
										        		<input type="text" name="grnpNrParcelamentofinal" maxlength="255" class="principalObjForm" onkeypress="return isDigito(event);" value="<bean:write name="cbAstbGruporeneparcelaGrnpVector" property="field(grnp_nr_parcelamentofinal)" />" />
										        	</td>
										        	<td class="intercalaLst<%=indice.intValue()%2%>" width="20%">
										        		<input type="text" name="grnpVlTaxaparcelamento" maxlength="10" class="principalObjForm" onkeydown="return isDigito(event);" onblur="return numberValidate(this, 2, '.', ',', event);" value="<bean:write name="cbAstbGruporeneparcelaGrnpVector" property="field(grnp_vl_taxaparcelamento)" />"/>
													</td>
										        	
										        	<td class="intercalaLst<%=indice.intValue()%2%>" width="20%">
										        		<input type="text" name="grnpVlDescontoinicial" maxlength="10" class="principalObjForm" onkeydown="return isDigito(event);" onblur="return numberValidate(this, 2, '.', ',', event);" value="<bean:write name="cbAstbGruporeneparcelaGrnpVector" property="field(grnp_vl_descontoinicial)" />"/>
										        	</td>
										        	<td class="intercalaLst<%=indice.intValue()%2%>" width="20%">
										        		<input type="text" name="grnpVlDescontofinal" maxlength="10" class="principalObjForm" onkeydown="return isDigito(event);" onblur="return numberValidate(this, 2, '.', ',', event);" value="<bean:write name="cbAstbGruporeneparcelaGrnpVector" property="field(grnp_vl_descontofinal)" />"/>
										        	</td>
										        	<!-- 
										        	<td class="intercalaLst<%=indice.intValue()%2%>" width="15%">
										        		<input type="text" name="grnpVlTaxadesconto" maxlength="255" class="principalObjForm" onkeydown="return isDigito(event);" onblur="return numberValidate(this, 2, '.', ',', event);" value="<bean:write name="cbAstbGruporeneparcelaGrnpVector" property="field(grnp_vl_taxadesconto)" />"/>
										        	</td>
										        	 -->
										        	<script>
										        		countRegistrosGrupoParc++;
										        	</script>
												</tr>
											</table>
										</div>
									</logic:iterate>
								</logic:present>  
							</div>
		                </td>
		              </tr>    
		            </table>
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/new.gif" alt="Novo" name="imgNovo" id="imgNovo" class="geralCursoHand" onclick="novo();"/>
                      </td>
                      <td width="25" align="center">
                      	<img src="webFiles/images/botoes/gravar.gif" alt="Gravar" name="imgGravar" id="imgGravar" class="geralCursoHand" onclick="salvar();"/>
                      </td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/cancelar.gif" alt="Cancelar" name="imgCancelar" id="imgCancelar" class="geralCursoHand" onclick="cancelar()"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>
</body>

<script>
	temPermissaoIncluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_INCLUSAO_CHAVE%>');
	temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>');
	temPermissaoExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_EXCLUSAO_CHAVE%>');
	
	function verificaPermissaoExcluir() {
		if (!temPermissaoExcluir){
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.grupoRegraNegociacaoForm.btExcluir'+i+');');
			}
		}
	}
	
	verificaPermissaoExcluir();
</script>

<logic:equal name="grupoRegraNegociacaoForm" property="userAction" value="init">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.grupoRegraNegociacaoForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.grupoRegraNegociacaoForm.imgGravar);
			desabilitaCampos();
		}
	</script>
</logic:equal>
<logic:equal name="grupoRegraNegociacaoForm" property="userAction" value="alterar">
	<script>
		if(!temPermissaoAlterar) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.grupoRegraNegociacaoForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.grupoRegraNegociacaoForm.imgGravar);
			desabilitaCampos();
		}
	</script>
</logic:equal>
<logic:equal name="grupoRegraNegociacaoForm" property="userAction" value="cancelar">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.grupoRegraNegociacaoForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GRUPO_REGRA_NEGOCIACAO_COBRANCA_ALTERACAO_CHAVE%>', document.grupoRegraNegociacaoForm.imgGravar);
			desabilitaCampos();
		}	
	</script>
</logic:equal>

<script>
	habilitaListaEmpresas();
	setaArquivoXml("CB_ASTB_IDIOMAGRUPORENE_IMRN.xml");
	habilitaTelaIdioma();
</script>
</html>