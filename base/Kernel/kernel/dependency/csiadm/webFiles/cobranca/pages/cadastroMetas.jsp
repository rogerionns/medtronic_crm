<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/css/global.css" type="text/css">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/cadastroMetas.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<script type="text/javascript">
	function pressEnter(ev) {
    	if (ev.keyCode == 13) {
    		pesquisar();
    	}
	}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="fecharAguarde();AtivarPasta('<bean:write name="cadastroMetasForm" property="flagAba"/>');verificaCampos('<bean:write name="cadastroMetasForm" property="flagAba"/>','<bean:write name="cadastroMetasForm" property="flagHabilitaCampos"/>');inicio();">
<html:form action="/cadastroMetas" method="post">
<html:hidden property="userAction"/>
<html:hidden property="alterando"/>
<html:hidden property="idMeta"/>
<html:hidden property="idListaResultado"/>

<input type="hidden" name="idArea" value=""/>
<input type="hidden" name="idFuncionario" value=""/>
<input type="hidden" name="idCampanha" value=""/>
<input type="hidden" name="idCampanhaSub" value=""/>
<input type="hidden" name="area" value=""/>
<input type="hidden" name="funcionario" value=""/>
<input type="hidden" name="campanha" value=""/>
<input type="hidden" name="campanhaSub" value=""/>
<input type="hidden" name="metaContatos" value=""/>
<input type="hidden" name="metaRecuperado" value=""/>
<input type="hidden" name="metaNegociado" value=""/>
<input type="hidden" name="resultadoCombo" value=""/>
<input type="hidden" name="mereQtde" value=""/>
<input type="hidden" name="check" value=""/> 
<input type="hidden" name="dataInativa" value=""/>

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="cadastroMetasForm.title.cadastroMetas"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="500"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado" name="tdProcurar" id="tdProcurar" onClick="AtivarPasta('pesquisar');"><bean:message key="cadastroMetasForm.aba.procurar"/></td>
                <td class="principalPstQuadroLinkNormalMaior" name="tdEstrategias" id="tdEstrategias" onClick="AtivarPasta('editar');"><bean:message key="cadastroMetasForm.aba.cadastroMetas"/></td>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr> 
                <td height="400" valign="top"> 
                  <div id="Procurar" style="position:absolute; width:99%; height:50px; z-index:1; visibility: hidden;"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="principalLabel"><bean:message key="cadastroMetasForm.descricao"/></td>
                      </tr>
                      <tr> 
                        <td width="56%"> 
                        	<html:text property="metaPesquisar" name="cadastroMetasForm" styleClass="principalObjForm" maxlength="60" onkeyup="pressEnter(event)"/>
                        </td>
                        <td width="44%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" alt="<bean:message key="cadastroMetasForm.botao.pesquisar"/>" onclick="pesquisar();" ></td>
                      </tr>
                      <tr> 
                        <td width="56%">&nbsp;</td>
                        <td width="44%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2"> 
                        <div style="height:300px; overflow: auto; ">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLstCab" align="center" width="3%">&nbsp;</td>
                              <td class="principalLstCab" width="10%"><bean:message key="cadastroMetasForm.lista.codigo"/></td>
                              <td class="principalLstCab" width="17%"><bean:message key="cadastroMetasForm.lista.area"/></td>
                              <td class="principalLstCab" width="20%">&nbsp;<bean:message key="cadastroMetasForm.lista.funcionario"/></td>
                              <td class="principalLstCab" width="15%"><bean:message key="cadastroMetasForm.lista.campanha"/></td>
                              <td class="principalLstCab" width="21%"><bean:message key="cadastroMetasForm.lista.subCampanha"/></td>
                              <td class="principalLstCab" width="14%"><bean:message key="cadastroMetasForm.lista.inativo"/></td>
                            </tr>
                            <logic:notEmpty name="cbCdtbMetaMeta">
                            	<logic:iterate name="cbCdtbMetaMeta" id="cbCdtbMetaMeta" indexId="indice">
                            		<tr class="geralCursoHand">
                            			
                            			<td class="principalLstPar" width="4%" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" onclick="excluir('<bean:write name="cbCdtbMetaMeta" property="field(ID_META_CD_META)"/>');" width="14" height="14" class="geralCursoHand" alt="Excluir"/></td>                    			
                            			
                            			<td class="principalLstPar" width="10%" onclick="alterar('<bean:write name="cbCdtbMetaMeta" property="field(ID_META_CD_META)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_AREA_CD_AREA)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_FUNC_CD_FUNCIONARIO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_CAMP_CD_CAMPANHA)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_PUBL_CD_PUBLICO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_NR_CONTATOS)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_VL_RECUPERADO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_VL_NEGOCIADO)"/>',
																												'<bean:write name="cbCdtbMetaMeta" property="field(META_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>');">&nbsp;<bean:write name="cbCdtbMetaMeta" property="field(ID_META_CD_META)"/></td>
                            																					
										<td class="principalLstPar" width="17%" onclick="alterar('<bean:write name="cbCdtbMetaMeta" property="field(ID_META_CD_META)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_AREA_CD_AREA)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_FUNC_CD_FUNCIONARIO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_CAMP_CD_CAMPANHA)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_PUBL_CD_PUBLICO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_NR_CONTATOS)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_VL_RECUPERADO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_VL_NEGOCIADO)"/>',
																												'<bean:write name="cbCdtbMetaMeta" property="field(META_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>');">&nbsp;<script>acronym('<bean:write name="cbCdtbMetaMeta" property="field(AREA_DS_AREA)" />', 15);</script></td>
                            			
                            			<td class="principalLstPar" width="20%" onclick="alterar('<bean:write name="cbCdtbMetaMeta" property="field(ID_META_CD_META)"/>',
                         																		 '<bean:write name="cbCdtbMetaMeta" property="field(ID_AREA_CD_AREA)"/>',
                         																		 '<bean:write name="cbCdtbMetaMeta" property="field(ID_FUNC_CD_FUNCIONARIO)"/>',
                         																		 '<bean:write name="cbCdtbMetaMeta" property="field(ID_CAMP_CD_CAMPANHA)"/>',
                         																		 '<bean:write name="cbCdtbMetaMeta" property="field(ID_PUBL_CD_PUBLICO)"/>',
                         																		 '<bean:write name="cbCdtbMetaMeta" property="field(META_NR_CONTATOS)"/>',
                         																		 '<bean:write name="cbCdtbMetaMeta" property="field(META_VL_RECUPERADO)"/>',
                         																		 '<bean:write name="cbCdtbMetaMeta" property="field(META_VL_NEGOCIADO)"/>',
																								 '<bean:write name="cbCdtbMetaMeta" property="field(META_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>');">&nbsp;<script>acronym('<bean:write name="cbCdtbMetaMeta" property="field(FUNC_NM_FUNCIONARIO)"/>', 15);</script></td>
                            			
                            			<td class="principalLstPar" width="15%" onclick="alterar('<bean:write name="cbCdtbMetaMeta" property="field(ID_META_CD_META)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_AREA_CD_AREA)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_FUNC_CD_FUNCIONARIO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_CAMP_CD_CAMPANHA)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_PUBL_CD_PUBLICO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_NR_CONTATOS)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_VL_RECUPERADO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_VL_NEGOCIADO)"/>',
																												'<bean:write name="cbCdtbMetaMeta" property="field(META_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>');">&nbsp;<script>acronym('<bean:write name="cbCdtbMetaMeta" property="field(CAMP_DS_CAMPANHA)"/>', 12);</script></td>
										
										<td class="principalLstPar" width="21%" onclick="alterar('<bean:write name="cbCdtbMetaMeta" property="field(ID_META_CD_META)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_AREA_CD_AREA)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_FUNC_CD_FUNCIONARIO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_CAMP_CD_CAMPANHA)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_PUBL_CD_PUBLICO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_NR_CONTATOS)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_VL_RECUPERADO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_VL_NEGOCIADO)"/>',
																												'<bean:write name="cbCdtbMetaMeta" property="field(META_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>');">&nbsp;<script>acronym('<bean:write name="cbCdtbMetaMeta" property="field(PUBL_DS_PUBLICO)"/>', 20);</script></td>
										
										<td class="principalLstPar" width="14%" onclick="alterar('<bean:write name="cbCdtbMetaMeta" property="field(ID_META_CD_META)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_AREA_CD_AREA)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_FUNC_CD_FUNCIONARIO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_CAMP_CD_CAMPANHA)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(ID_PUBL_CD_PUBLICO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_NR_CONTATOS)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_VL_RECUPERADO)"/>',
                            																					'<bean:write name="cbCdtbMetaMeta" property="field(META_VL_NEGOCIADO)"/>',
																												'<bean:write name="cbCdtbMetaMeta" property="field(META_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>');">&nbsp;<bean:write name="cbCdtbMetaMeta" property="field(META_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>                            																					                            																					                            																					
                            		</tr>
                            		<script>
		                            	countRegistro++;
		                            </script>
                            	</logic:iterate>
                            </logic:notEmpty>
                            <logic:empty name="cbCdtbMetaMeta">
                            <tr> 
                              <td align="center" colspan="7" > 
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
							  <tr id="nenhumRegistro" ><td height="260" width="800" align="center"  class="principalLstPar"><br><b><bean:message key="lista.nenhum.registro.encontrado"/> </b></td></tr>
							</table>
							&nbsp;</td>
							</tr>
                            </logic:empty>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </div>
                  
                  <div id="Estrategias" style="position:absolute; width:99%; height:390px; z-index:2; visibility: hidden; "> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td width="26%" align="right" class="principalLabel">&nbsp;</td>
                        <td width="51%" class="principalLabel">&nbsp;</td>
                        <td width="23%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="cadastroMetasForm.codigo"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="51%"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="17%"> 
                              	<html:text property="idMetaCdMeta" maxlength="10" name="cadastroMetasForm" styleClass="principalObjForm" disabled="true"/>
                              </td>
                              <td width="83%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="23%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="cadastroMetasForm.area"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="51%"> 
							<html:select property="idAreaCdAreaCombo" name="cadastroMetasForm" styleClass="principalObjForm" onchange="atualizaCombos();">
								<html:option value="0">-- Selecione uma Op��o --</html:option>
								<logic:notEmpty name="findCsCdtbAreaAreaByDescricaoByEmpresa">
									<bean:define name="findCsCdtbAreaAreaByDescricaoByEmpresa" id="CsCdtbAreaAreaVo"/>
									<html:options collection="CsCdtbAreaAreaVo" property="idAreaCdArea" labelProperty="areaDsArea"/>
								</logic:notEmpty>
							</html:select>
						</td>
                        <td width="23%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="cadastroMetasForm.funcionario"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="51%"> 
							<html:select property="idFuncCdFuncionarioCombo" name="cadastroMetasForm" styleClass="principalObjForm" >
								<html:option value="0">-- Selecione uma Op��o --</html:option>
								<logic:notEmpty name="findCsCdtbFuncionarioFuncByDescricaoByEmpresa">
									<bean:define name="findCsCdtbFuncionarioFuncByDescricaoByEmpresa" id="CsCdtbFuncionarioFuncVo"/>
									<html:options collection="CsCdtbFuncionarioFuncVo" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>
								</logic:notEmpty>
							</html:select>
						</td>
                        <td width="23%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="cadastroMetasForm.campanha"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="51%"> 
							<html:select property="idCampCdCampanhaCombo" name="cadastroMetasForm" styleClass="principalObjForm" onchange="atualizaCombos();">
								<html:option value="0">-- Selecione uma Op��o --</html:option>
								<logic:notEmpty name="findCsCdtbCampanhaCampByDescricaoByEmpresa">
									<bean:define name="findCsCdtbCampanhaCampByDescricaoByEmpresa" id="CsCdtbCampanhaCampVo"/>
									<html:options collection="CsCdtbCampanhaCampVo" property="idCampCdCampanha" labelProperty="campDsCampanha" />
								</logic:notEmpty>
							</html:select>
						</td>
                        <td width="23%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="cadastroMetasForm.subCampanha"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="51%"> 
                          <html:select property="idPublCdPublicoCombo" name="cadastroMetasForm" styleClass="principalObjForm">
								<html:option value="0">-- Selecione uma Op��o --</html:option>
								<logic:notEmpty name="findCsCdtbPublicoPublByDescricaoByEmpresa">
									<bean:define name="findCsCdtbPublicoPublByDescricaoByEmpresa" id="CsCdtbPublicoPublVo"/>
									<html:options collection="CsCdtbPublicoPublVo" property="idPublCdPublico" labelProperty="publDsPublico"/>
								</logic:notEmpty>											
							</html:select>
                        </td>
                        <td width="23%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="cadastroMetasForm.qtdContratosEfetivos"/> 
                        	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="51%"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLabel" width="17%"> 
                                <html:text property="metaNrContatos" maxlength="5" name="cadastroMetasForm"  styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
                              </td>
                              <td width="83%" align="right">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="23%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="cadastroMetasForm.valorRecuperado"/>
                        	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="51%"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLabel" width="17%"> 
                                <html:text property="metaVlRecuperado" maxlength="10" name="cadastroMetasForm" styleClass="principalObjForm" onkeypress="return(formataDecimal(this,',',event,9))"/>
                              </td>
                              <td width="83%" align="right">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="23%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="cadastroMetasForm.valorNegociado"/> 
                        	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="51%"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="17%"> 
                                <html:text property="metaVlNegociado" maxlength="10" name="cadastroMetasForm" styleClass="principalObjForm" onkeypress="return(formataDecimal(this,',',event,9))"/>
                              </td>
                              <td width="83%" class="principalLabel">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="23%" class="principalLabel"> 
                          <logic:equal value="" name="cadastroMetasForm" property="metaDhInativo">
								<input type="checkbox" name="inativo" value="ativo"/>                            	                              	
						  </logic:equal>
						  <logic:notEqual value="" name="cadastroMetasForm" property="metaDhInativo">                                
								<input type="checkbox" name="inativo" value="inativo" checked="checked"/>                            	
						  </logic:notEqual> 
                          <bean:message key="cadastroMetasForm.inativo"/>
                       	</td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td>
                          <hr>
                        </td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td width="26%" align="right" class="principalLabel" height="28"><bean:message key="cadastroMetasForm.resultado"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="21%"> 
							<html:select property="idComboResultado" name="cadastroMetasForm" styleClass="principalObjForm">
								<html:option value="0">-- Selecione uma Op��o --</html:option>
								<logic:notEmpty name="findCsCdtbResultadoResuByDescricaoByEmpresa">
									<bean:define name="findCsCdtbResultadoResuByDescricaoByEmpresa" id="CsCdtbResultadoResuVo"/>
									<html:options collection="CsCdtbResultadoResuVo" property="idResuCdResultado" labelProperty="resuDsResultado"/>
								</logic:notEmpty>
							</html:select>
						</td>
                        <td width="19%" align="right" class="principalLabel"><bean:message key="cadastroMetasForm.qtd"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="11%"> 
                          <html:text property="mereNrQtde" maxlength="5"  name="cadastroMetasForm" styleClass="principalObjForm" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
                        </td>
                        <td width="23%"><img src="webFiles/images/botoes/setaDown.gif" name="imgAdicionaMeta" id="imgAdicionaMeta" width="21" height="18" class="geralCursoHand" onclick="adicionaLista();" alt="Adicionar"> 
                        </td>
                      </tr>
                    </table>
                     <iframe id="cadastroMetasLista" name="cadastroMetasLista" src="cadastroMetas.do?userAction=carregaCadastroMetasLista&idMeta=<bean:write property="idMetaCdMeta" name="cadastroMetasForm"/>" width="98%" height="100px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>          
				 </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
		                <td> 
		                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <tr> 
		                      <td>&nbsp;</td>
		                      <td width="25" align="center">
			                      <img src="webFiles/images/botoes/new.gif" alt="Novo" name="imgNovo" id="imgNovo" class="geralCursoHand" onclick="novo();"/>
		                      </td>
		                      <td width="25" align="center">
		                      	<img src="webFiles/images/botoes/gravar.gif" alt="Gravar" name="imgGravar" id="imgGravar" class="geralCursoHand" onclick="salvar();"/>
		                      </td>
		                      <td width="25" align="center">
			                      <img src="webFiles/images/botoes/cancelar.gif" alt="Cancelar" name="imgCancelar" id="imgCancelar" class="geralCursoHand" onclick="cancelar()"/>
		                      </td>
		                    </tr>
		                  </table>
		                </td>
		              </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
 </html:form> 
</body>
<script>
	temPermissaoIncluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_META_INCLUSAO_CHAVE%>');
	temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_META_ALTERACAO_CHAVE%>');
	temPermissaoExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_META_EXCLUSAO_CHAVE%>');
	
	function verificaPermissaoExcluir() {
		if (!temPermissaoExcluir){
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.cadastroMetasForm.btExcluir'+i+');');
			}	
		}
	}
	
	verificaPermissaoExcluir();
</script>

<logic:equal name="cadastroMetasForm" property="userAction" value="init">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_META_ALTERACAO_CHAVE%>', document.cadastroMetasForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_META_ALTERACAO_CHAVE%>', document.cadastroMetasForm.imgGravar);
			desabilitaCamposMeta();
		}
	</script>
</logic:equal>
<logic:equal name="cadastroMetasForm" property="userAction" value="alterar">
	<script>
		if(!temPermissaoAlterar) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_META_ALTERACAO_CHAVE%>', document.cadastroMetasForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_META_ALTERACAO_CHAVE%>', document.cadastroMetasForm.imgGravar);
			desabilitaCamposMeta();
		}
	</script>
</logic:equal>
<logic:equal name="cadastroMetasForm" property="userAction" value="cancelar">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_META_ALTERACAO_CHAVE%>', document.cadastroMetasForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_META_ALTERACAO_CHAVE%>', document.cadastroMetasForm.imgGravar);
			desabilitaCamposMeta();
		}	
	</script>
</logic:equal>	

<script>
	habilitaListaEmpresas();
</script>
</html>