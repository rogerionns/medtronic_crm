<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String fileIncludeIdioma="../../../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/css/global.css" type="text/css">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/regrasNegociacao.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/number.js"></SCRIPT>
<script type="text/javascript">

  function strReplaceAll(str,strFind,strReplace)
   {
      var returnStr = str;
      var start = returnStr.indexOf(strFind);
      while (start>=0)
      {
         returnStr = returnStr.substring(0,start) + strReplace + returnStr.substring(start+strFind.length,returnStr.length);
         start = returnStr.indexOf(strFind,start+strReplace.length);
      }
      return returnStr;
   }
   
   function prepare( value, sep )
   {

      if ( sep == ',' )
      {
         value = strReplaceAll( value, '.', ',' );
      }
      else if ( sep == '.' )
      {
         value = strReplaceAll( value, ',', '.' );
      }

      return value;
      
   }

   function numberValidateWithSignal( obj, digits, dig1, dig2 )
   {
      var posValue = 0;
      var valueChar;
      var strRetNumber;
      
      var value = prepare( obj.value, dig2 );
      
      if ( value == '' )
      {
         return;
      }
      
      var part1, part2;
      
      var pos = value.lastIndexOf( dig2 );
      if ( pos == -1 )
      {
         part1 = value;
         part2 = '';
      }
      else
      {
         part1 = value.substring( 0, pos );
         part2 = value.substring( pos + 1, value.length );
      }
      
      part1 = getDigitsWithSignalOf( part1 );
      part2 = getDigitsOf( part2 );
      
      if ( digits == 0 )
      {
         strRetNumber = getDigitsOf( part1 );
      }
      else
      {
         
         if ( part2.length <= digits )
         {
            var len = digits - part2.length;
            for( pos = 0; pos < len; pos++ )
            {
               part2 = part2 + "0";
            }
         }
         else
         {
            part2 = part2.substring( 0, digits );
         }
   
         var size = part1.length;
         
         strRetNumber = "";
         
         for( pos = 0; pos < size; pos++)
         {
            valueChar = part1.charAt( part1.length - pos - 1 );
            
            if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
            {
               strRetNumber = dig1 + strRetNumber;
            }
            strRetNumber = valueChar + strRetNumber;
         }
         
         if ( strRetNumber == '' )
         {
            strRetNumber = '0';
         }
         
         if ( digits > 0 )
         {
            strRetNumber = strRetNumber + dig2 + part2;
         }
      }
      
      obj.value = strRetNumber;
   }
   
   function numberValidate( obj, digits, dig1, dig2, evnt )
   {
      var posValue = 0;
      var valueChar;
      var strRetNumber;

		//Nao eh keypress
		if (evnt.keyCode != 0){			
			var caracter = String.fromCharCode(evnt.keyCode);				
			var isDigito = (caracter == dig1 || caracter == dig2);
			
		    if (((evnt.keyCode < 48 && !isDigito) || (evnt.keyCode > 57 && !isDigito)) && evnt.keyCode != 8){				
		        evnt.returnValue = null;
		        return false;
			}
			
		}

      var value = prepare( obj.value, dig2 );
      
      if ( value == '' )
      {
         return true;
      }
      
      var part1, part2;
      
      var pos = value.lastIndexOf( dig2 );
	
      if ( pos == -1 )
      {
         part1 = value;
         part2 = '';
      }
      else
      {
         part1 = value.substring( 0, pos );
         part2 = value.substring( pos + 1, value.length );
      }
            
      part1 = getDigitsWithSignalOf( part1 );
      part2 = getDigitsWithSignalOf( part2 );
  	
      if ( digits == 0 )
      {
         strRetNumber = getDigitsOf( part1 );
      }
      else
      {
         if ( part2.length <= digits )
         {
            var len = digits - part2.length;
            for( pos = 0; pos < len-2; pos++ )
            {
               part2 = part2 + "0";
            }
         }
         else
         {
            part2 = part2.substring( 0, digits );
         }

         var size = part1.length;
         
         strRetNumber = "";

          for( pos = 0; pos < size; pos++)
         {
            valueChar = part1.charAt( part1.length - pos - 1 );

            if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
            {
                strRetNumber = dig1 + strRetNumber;
            }

            strRetNumber = valueChar + strRetNumber;
         }
          
        //quando � centena com o sinal de (-), ele acha que tem que utilizar ponto, para virar milhar
  		 if(size==4){
  			strRetNumber = strReplaceAll(strRetNumber, '.', '');
 		 }
		
         if ( strRetNumber == '' )
         {
            strRetNumber = '0';
         }

         if ( digits > 0 )
         {
             
            strRetNumber = strRetNumber + dig2 + part2;
         }
      }
      
      obj.value = strRetNumber;



      validaDigitoEsquerda(obj,3);

      
   }
   
   function getDigitsOf(strNumber)
   {
      var number;
      var strRetNumber="";
   
      for (var i=0 ; i < strNumber.length ; i++)
      {
         number = parseInt(strNumber.charAt(i));
         if ( number )
         {
            strRetNumber += strNumber.charAt(i)
         }
         else
         {
            if ( number == 0 )
            {
               strRetNumber += strNumber.charAt(i)
            }
         }
      }
      return strRetNumber;
   }

   function getDigitsWithSignalOf(strNumber)
   {
      var number;
      var strRetNumber="";
      var signal = "";

      if ( strNumber.length > 0 )
      {
         var firstChar = strNumber.charAt(i);
         if ( firstChar == '-' )
         {
            signal = firstChar;
         }
      }
   
      for (var i=0 ; i < strNumber.length ; i++)
      {
         number = parseInt(strNumber.charAt(i));
         if ( number )
         {
            strRetNumber += strNumber.charAt(i)
         }
         else
         {
            if ( number == 0 )
            {
               strRetNumber += strNumber.charAt(i)
            }
         }
      }

      if ( strRetNumber.length > 0 )
      {
         strRetNumber = signal + strRetNumber;
      }

      return strRetNumber;
   }
   
	function pressEnter(ev) {
    	if (ev.keyCode == 13) {
    		pesquisar();
    	}
	}


	function validaDigitoEsquerda(obj,maxLen){
		var valorCampo = obj.value;
		var valorAntesVirgula = "";
		var nPosVirgula = -1;
		if(valorCampo.indexOf(",") > -1){
			nPosVirgula = valorCampo.indexOf(",");
			valorAntesVirgula = valorCampo.substring(0,nPosVirgula);
			if(valorAntesVirgula.length > maxLen){
				alert("Por favor digite somente " + maxLen + " casas a esquerda !");
				obj.focus();
			}
			
			
		}
		
		
	}

	function abreCampoFormula(cObj){
		
		showModalDialog('regrasNegociacao.do?userAction=formulaRegra&nameObj=' + cObj,window,'help:no;scroll:yes;Status:NO;dialogWidth:620px;dialogHeight:225px,dialogTop:0px,dialogLeft:200px');

	}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="fecharAguarde();showError('<%=request.getAttribute("msgerro")%>');AtivarPasta('<bean:write name="regrasNegociacaoForm" property="flagAba"/>');verificaCampos('<bean:write name="regrasNegociacaoForm" property="flagAba"/>','<bean:write name="regrasNegociacaoForm" property="flagHabilitaCampos"/>');inicio();">
<html:form action="/regrasNegociacao" method="post" styleId="regrasNegociacaoForm">
<html:hidden property="userAction"/>
<html:hidden property="alterando"/>
<html:hidden property="idNegociacao"/>

<input type="hidden" name="dsRegnegociacao" value=""/>
<input type="hidden" name="nrDiaspripag" value=""/>
<input type="hidden" name="nrMinparcela" value=""/>
<input type="hidden" name="nrMaxparcela" value=""/>
<input type="hidden" name="vlJurosparcelamento" value=""/>
<input type="hidden" name="nrTaxaadm" value=""/>
<input type="hidden" name="vlMindesconto" value=""/>
<input type="hidden" name="vlMaxdesconto" value=""/>
<input type="hidden" name="vlJurosdiasatraso" value=""/>

<input type="hidden" name="nrMulta" value=""/>

<input type="hidden" name="vlCustoboleto" value=""/>
<input type="hidden" name="inImpressaoboleto" value=""/>
<input type="hidden" name="inIsencaojuros" value=""/>
<input type="hidden" name="inIsencaomulta" value=""/>

<input type="hidden" name="dhInativo" value=""/>
<input type="hidden" name="inPermissaoprimpag" value=""/>
<input type="hidden" name="inPermissaoparce" value=""/>
<input type="hidden" name="inPermissaodesconto" value=""/>
<input type="hidden" name="nrDiaspripagmax" value=""/>
<input type="hidden" name="vlEntrada" value=""/>
<input type="hidden" name="vlPercentualentrada" value=""/>
<input type="hidden" name="inPermissaoentrada" value=""/>
<input type="hidden" name="nrIof" value=""/>
<input type="hidden" name="InTipoCobrancaJuros" value=""/>
<input type="hidden" name="InTipoCobrancaMulta" value="">
<input type="hidden" name="vlSeguro" value=""/>
<input type="hidden" name="check" value=""/>
<input type="hidden" name="idInfoboleto" value="">
<input type="hidden" name="tpArredondamento" value="">

<input type="hidden" name="nrDiasinicio" value="">
<input type="hidden" name="nrDiasfim" value="">
<input type="hidden" name="nrSequencia" value="">
<input type="hidden" name="dsCampocalculo" value="">
<input type="hidden" name="vlRemunoperador" value="">
<input type="hidden" name="inTpremunoperador" value="">
<input type="hidden" name="vlRemunoperadora" value="">
<input type="hidden" name="inTpremunoperadora" value="">
<input type="hidden" name="dsFormula" value="">

<html:hidden property="reneDsCampocalculo"/>


  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="regrasNegociacaoForm.title.regrasNegociacao"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado" name="tdProcurar" id="tdProcurar" onClick="AtivarPasta('pesquisar');"><bean:message key="regrasNegociacaoForm.aba.procurar"/></td>
                <td class="principalPstQuadroLinkNormalMaior" name="tdEstrategias" id="tdEstrategias" onClick="AtivarPasta('editar');"><bean:message key="regrasNegociacaoForm.aba.regraNegociacao"/></td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr> 
                <td height="500" valign="top"> 
                
                  <div id="Procurar" style="position:absolute; width:99%; height:380px; z-index:1; visibility: visible;"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="principalLabel"><bean:message key="regrasNegociacaoForm.descricao"/></td>
                      </tr>
                      <tr> 
                        <td width="56%"> 
                          <html:text property="reneDsRegnegociacaoDescricao" name="regrasNegociacaoForm" styleClass="principalObjForm" maxlength="100" onkeyup="pressEnter(event)"/>  
                        </td>
                        <td width="44%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="pesquisar();"alt="Pesquisar"></td>
                      </tr>
                      <tr> 
                        <td width="56%">&nbsp;</td>
                        <td width="44%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2"> 
                        <div style="height:290px; overflow: auto;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                             
                              <td class="principalLstCab" width="4%">&nbsp;</td>
                              <td class="principalLstCab" width="4%">&nbsp;</td>
                              <td class="principalLstCab" width="9%"><bean:message key="regrasNegociacaoForm.lista.codigo"/></td>
                              <td class="principalLstCab" width="62%"><bean:message key="regrasNegociacaoForm.lista.regraNegociacao"/></td>
                              <td class="principalLstCab" width="25%"><bean:message key="regrasNegociacaoForm.lista.inativo"/></td>
                            </tr>
                            
                            <logic:notEmpty name="cbCdtbRegnegociacaoRene">
                            	<logic:iterate id="cbCdtbRegnegociacaoRene" name="cbCdtbRegnegociacaoRene" indexId="indice">
                            		 <tr class="geralCursoHand">
                            		 	                            		 	
                            		 	<td class="principalLstPar" width="4%" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" onclick="excluir('<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_RENE_CD_REGNEGOCIACAO)"/>');" width="14" height="14" class="geralCursoHand" alt="Excluir"/></td>
                            		 	                         		 	
                            		 	<td class="principalLstPar" width="4%" align="center" onclick="abrirModalIdioma('CS_ASTB_IDIOMREGNEGOCI_IDNE.xml','<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_RENE_CD_REGNEGOCIACAO)"/>');"><img src="webFiles/images/botoes/GloboAuOn.gif" width="18" height="18" class="geralCursoHand"/></td>
                            		 	
                            		 	<td class="principalLstPar" width="9%"onclick="alterar('<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_RENE_CD_REGNEGOCIACAO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DS_REGNEGOCIACAO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASPRIPAG)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_MINPARCELA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_MAXPARCELA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_JUROSPARCELAMENTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_TAXAADM)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_MINDESCONTO)" />',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_MAXDESCONTO)"  />',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_JUROSDIASATRASO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_MULTA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_CUSTOBOLETO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_IMPRESSAOBOLETO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_ISENCAOJUROS)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_ISENCAOMULTA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DH_INATIVO)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAOPRIMPAG)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAOPARCE)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAODESCONTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_INBO_CD_INFOBOLETO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASPRIPAGMAX)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_ENTRADA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_PERCENTUALENTRADA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAOENTRADA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_IOF)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TIPOCOBRANCAJUROS)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TIPOCOBRANCAMULTA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_SEGURO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TPARREDONDAMENTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASINICIO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASFIM)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_SEQUENCIA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DS_CAMPOCALCULO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_REMUNOPERADOR)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TPREMUNOPERADOR)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_REMUNOPERADORA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TPREMUNOPERADORA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_GRRE_CD_GRUPORENE)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DS_FORMULA)"/>'
                            		 														   );">&nbsp;<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_RENE_CD_REGNEGOCIACAO)"/></td>
		                            		
		                               <td class="principalLstPar" width="62%"onclick="alterar('<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_RENE_CD_REGNEGOCIACAO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DS_REGNEGOCIACAO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASPRIPAG)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_MINPARCELA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_MAXPARCELA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_JUROSPARCELAMENTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_TAXAADM)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_MINDESCONTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_MAXDESCONTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_JUROSDIASATRASO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_MULTA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_CUSTOBOLETO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_IMPRESSAOBOLETO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_ISENCAOJUROS)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_ISENCAOMULTA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DH_INATIVO)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAOPRIMPAG)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAOPARCE)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAODESCONTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_INBO_CD_INFOBOLETO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASPRIPAGMAX)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_ENTRADA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_PERCENTUALENTRADA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAOENTRADA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_IOF)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TIPOCOBRANCAJUROS)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TIPOCOBRANCAMULTA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_SEGURO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TPARREDONDAMENTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASINICIO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASFIM)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_SEQUENCIA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DS_CAMPOCALCULO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_REMUNOPERADOR)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TPREMUNOPERADOR)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_REMUNOPERADORA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TPREMUNOPERADORA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_GRRE_CD_GRUPORENE)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DS_FORMULA)"/>'
                            		 														   );">&nbsp;<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DS_REGNEGOCIACAO)"/></td>
		                            	
		                               <td class="principalLstPar" width="25%"onclick="alterar('<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_RENE_CD_REGNEGOCIACAO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DS_REGNEGOCIACAO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASPRIPAG)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_MINPARCELA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_MAXPARCELA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_JUROSPARCELAMENTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_TAXAADM)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_MINDESCONTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_MAXDESCONTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_JUROSDIASATRASO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_MULTA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_CUSTOBOLETO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_IMPRESSAOBOLETO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_ISENCAOJUROS)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_ISENCAOMULTA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DH_INATIVO)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAOPRIMPAG)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAOPARCE)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAODESCONTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_INBO_CD_INFOBOLETO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASPRIPAGMAX)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_ENTRADA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_PERCENTUALENTRADA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_PERMISSAOENTRADA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_IOF)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TIPOCOBRANCAJUROS)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TIPOCOBRANCAMULTA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_SEGURO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TPARREDONDAMENTO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASINICIO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_DIASFIM)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_NR_SEQUENCIA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DS_CAMPOCALCULO)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_REMUNOPERADOR)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TPREMUNOPERADOR)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_VL_REMUNOPERADORA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_IN_TPREMUNOPERADORA)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(ID_GRRE_CD_GRUPORENE)"/>',
                            		 														   '<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DS_FORMULA)"/>'
                            		 														   );">&nbsp;<bean:write name="cbCdtbRegnegociacaoRene" property="field(RENE_DH_INATIVO)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/></td>													  									
                            		 </tr>
                            		 <script>
		                            	countRegistro++;
		                            </script>
                            	</logic:iterate>
                            </logic:notEmpty>
                            <logic:empty name="cbCdtbRegnegociacaoRene">
								<tr> 
									<td align="center" colspan="7" > 
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
											<tr id="nenhumRegistro" ><td height="260" width="800" align="center"  class="principalLstPar"><br><b><bean:message key="lista.nenhum.registro.encontrado"/></b></td></tr>
										</table>
									&nbsp;</td>
								</tr>
							</logic:empty>
                          </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>                 
                  <div id="Estrategias" style="position:absolute; width:99%; height:400px; z-index:2; visibility: hidden;"> 
                    <table width="98%" border="0" height="380px" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.codigo"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td> 
                        <td width="13%" class="principalLabel"> 
                        	<html:text property="idReneCdRegnegociacao" maxlength="10" disabled="true" name="regrasNegociacaoForm" styleClass="principalObjForm"/></td>
                        <td class="principalLabel" colspan="3">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        		<td class="principalLabel" align="right" height="26" width="30%">
                        			<bean:message key="regrasNegociacaoForm.regraNegociacao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                        		</td>
                        		<td class="principalLabel"> 
                          			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            			<tr> 
                              				<td width="80%"> 
                                			<html:text property="reneDsRegnegociacao" maxlength="200" name="regrasNegociacaoForm" styleClass="principalObjForm"/></td>
                              				<td>&nbsp;</td>
                            			</tr>
                          			</table>
                        		</td>
                        	</table>
						</td>
                      </tr>
                      
                      <tr>
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.sequencia"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                        <td colspan="1" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="100%"> 
                                <html:text property="reneNrSequencia" maxlength="6" name="regrasNegociacaoForm" styleClass="principalObjForm"/></td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td class="principalLabel" colspan="3">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        		<td class="principalLabel" align="right" height="26" width="30%">
                        			<bean:message key="prompt.GrupoRegraDeNeg"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                        		</td>
                        		<td class="principalLabel"> 
                          			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            			<tr> 
                              				<td width="80%"> 
                                				<html:select property="idGrreCdGruporene" name="regrasNegociacaoForm" styleClass="principalObjForm">
													<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option>
													<logic:present name="cbCdtbGruporeneGrre">
														<html:options collection="cbCdtbGruporeneGrre" property="field(id_grre_cd_gruporene)" labelProperty="field(grre_ds_gruporene)"/>
													</logic:present>
										    	</html:select>
                                			</td>
                              				<td>&nbsp;</td>
                            			</tr>
                          			</table>
                        		</td>
                        	</table>
                        </td>
                      </tr>
                      
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="prompt.diasdeatraso"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">
                              	<bean:message key="regrasNegociacaoForm.min"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneNrDiasinicio" maxlength="6" name="regrasNegociacaoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false; "/></td>
                              <td width="10%" align="right" class="principalLabel">
                              	<bean:message key="regrasNegociacaoForm.max"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td width="13%"> 
                                <html:text property="reneNrDiasfim" maxlength="6" name="regrasNegociacaoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur="validaForm();validaCampoNumerico(this); return false;"/></td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      
                      <tr>
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="prompt.dataBaseAtraso"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="46%"> 
                           		<select name="cmbDsCampocalculo" class="principalObjForm" onchange="setCampoCalculo();">
									<option value="0"><bean:message key="prompt.selecione_uma_opcao"/></option>
						  			<option value="PARC_DH_VENCIMENTO">DATA VENC. PARCELA</option>
							    </select>
                              </td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <script>
                      	if(regrasNegociacaoForm.reneDsCampocalculo.value != ""){
                      		regrasNegociacaoForm.cmbDsCampocalculo.value = regrasNegociacaoForm.reneDsCampocalculo.value;
                      	}
						
                      	function setCampoCalculo(){
                      		if(regrasNegociacaoForm.cmbDsCampocalculo.value != "0"){
                      			regrasNegociacaoForm.reneDsCampocalculo.value == regrasNegociacaoForm.reneDsCampocalculo.value;
                      		}
                      	}
                      </script>
                      
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.quantidadeDiasPrimeiroPagto"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">
                              	<bean:message key="regrasNegociacaoForm.min"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneNrDiaspripag" maxlength="3" name="regrasNegociacaoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false; "/></td>
                              <td width="10%" align="right" class="principalLabel">
                              	<bean:message key="regrasNegociacaoForm.max"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td width="13%"> 
                                <html:text property="reneNrDiaspripagmax" maxlength="3" name="regrasNegociacaoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur="validaForm();validaCampoNumerico(this); return false;"/></td>
                              <td width="54%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
										<td width="12%" align="right" class="principalLabel"> 
											<html:checkbox property="verInPermissaoprimpag" value="S"/></td>
										<td width="37%" class="principalLabel">
											<bean:message key="regrasNegociacaoForm.operadorVisualiza"/></td> 
										<td width="18%" align="right" class="principalLabel">
											<html:checkbox property="altInPermissaoprimpag" value="S"/></td> 
										<td width="33%" class="principalLabel">
											<bean:message key="regrasNegociacaoForm.operadorAltera"/></td>
								</tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      
                      <tr> 
						<td class="principalLabel" align="right" height="26" width="30%">
							<bean:message key="regrasNegociacaoForm.valorFixoEntrada"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
						<td colspan="2" class="principalLabel"> 
						  <table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
							  <td width="10%" align="right" class="principalLabel">
							  	<bean:message key="regrasNegociacaoForm.porcentagem"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							  <td width="13%" class="principalLabel"> 
								<html:text property="reneVlPercentualentrada" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);"/></td>
							  <td width="10%" align="right" class="principalLabel">
							  	<bean:message key="regrasNegociacaoForm.valor"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							  <td width="13%"> 
								<html:text property="reneVlEntrada" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onkeypress="return(formataDecimal(this,',',event,9))"/></td>
							  <td width="54%"> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td width="12%" align="right" class="principalLabel"> 
											<html:checkbox property="verInPermissaoparce" value="S"/></td>
										<td width="37%" class="principalLabel">
											<bean:message key="regrasNegociacaoForm.operadorVisualiza"/></td> 
										<td width="18%" align="right" class="principalLabel">
											<html:checkbox property="altInPermissaoparce" value="S"/></td> 
										<td width="33%" class="principalLabel">
											<bean:message key="regrasNegociacaoForm.operadorAltera"/></td>
									</tr>								
								</table>
							  </td>
							</tr>
						  </table>
						</td>
						<td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
					 </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.quantidadeparcelas"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">
                              	<bean:message key="regrasNegociacaoForm.min"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneNrMinparcela" maxlength="2" name="regrasNegociacaoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false;"/></td>
                              <td width="10%" align="right" class="principalLabel">
                              	<bean:message key="regrasNegociacaoForm.max"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td width="13%"> 
                                <html:text property="reneNrMaxparcela" maxlength="3" name="regrasNegociacaoForm" styleClass="principalObjForm" onkeypress="return validaCampoNumerico(this)" onblur= "validaForm(); validaCampoNumerico(this); return false;"/></td>
                              <td width="54%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td width="12%" align="right" class="principalLabel"> 
											<html:checkbox property="verInPermissaoentrada" value="S"/></td>
										<td width="37%" class="principalLabel">
											<bean:message key="regrasNegociacaoForm.operadorVisualiza"/></td> 
										<td width="18%" align="right" class="principalLabel">
											<html:checkbox property="altInPermissaoentrada" value="S"/></td> 
										<td width="33%" class="principalLabel">
											<bean:message key="regrasNegociacaoForm.operadorAltera"/></td>
									</tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.multaContratoAtraso"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">&nbsp;</td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneNrMulta" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);"/>
                               </td>
                               <td>
                               		<img src="webFiles/images/icones/funcao.gif" width="24" height="24" class="geralCursoHand" onclick="abreCampoFormula('reneNrMulta');" alt="F�rmula">
                               </td>
                              <td class="principalLabel" align="right" height="26" width="35%">
                              	<bean:message key="regrasNegociacaoForm.formacobrancamulta"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td colspan="2" class="principalLabel" width="54%"> 
		                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr> 
		                              <td class="principalLabelValorFixo"> 
		                              	<html:radio property="reneInTipoCobrancaMulta" value="F"/>Fixo 
		                                <html:radio property="reneInTipoCobrancaMulta" value="P"/>Percentual </td>
		                         </tr>
		                       </table>
		                     </td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.juroMoradiariaContrato"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td> 
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">&nbsp;</td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneVlJurosdiasatraso" maxlength="35" style="width:70;" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);"/>
                              </td>
                              <td>
                               	<img src="webFiles/images/icones/funcao.gif" width="24" height="24" class="geralCursoHand" onclick="abreCampoFormula('reneVlJurosdiasatraso');" alt="F�rmula">
                              </td>
                              <td class="principalLabel" align="right" height="26" width="35%">
                              	<bean:message key="regrasNegociacaoForm.formacobrancajuros"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                              </td>
                              <td colspan="2" class="principalLabel" width="54%"> 
		                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr> 
		                              <td class="principalLabelValorFixo"> 
		                              	<html:radio property="reneInTipoCobrancaJuros" value="F"/>Fixo 
		                                <html:radio property="reneInTipoCobrancaJuros" value="P"/>Percentual</td>
		                         </tr>
		                       </table>
		                     </td>
                               <td>&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.custoEmissaoBoleto"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">&nbsp;</td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneVlCustoboleto" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this,10, '.', ',', event);"/></td>
                              <td>&nbsp; </td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.desconto"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td> 
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">
                              	<bean:message key="regrasNegociacaoForm.min"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td> 
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneVlMindesconto" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);"/></td>
                              <td width="10%" align="right" class="principalLabel">
                              	<bean:message key="regrasNegociacaoForm.max"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td width="13%"> 
                                 <html:text property="reneVlMaxdesconto" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);"/></td>
                              <td width="54%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
									<td width="11%" align="right" class="principalLabel"> 
										<html:checkbox property="verInPermissaodesconto" value="S"/></td>
									<td width="39%" class="principalLabel">
										<bean:message key="regrasNegociacaoForm.operadorVisualiza"/></td> 
									<td width="17%" align="right" class="principalLabel">
										<html:checkbox property="altInPermissaodesconto" value="S"/></td> 
									<td width="33%" class="principalLabel"><bean:message key="regrasNegociacaoForm.operadorAltera"/></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.parcelamento"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">&nbsp;</td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneVlJurosparcelamento" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);"/></td>
                              <td class="principalLabel" align="right" height="26" width="35%">
                              	<bean:message key="regrasNegociacaoForm.permiteImpressaoBoleto"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td colspan="2" class="principalLabel" width="54%"> 
                          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            		<tr> 
                              			<td class="principalLabelValorFixo"> 
                              				<html:radio property="reneInImpressaoboleto" value="S"/>Sim 
                                			<html:radio property="reneInImpressaoboleto" value="N"/>N�o
                                		</td>
		                            </tr>
		                        </table>
		                     </td>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.taxaSeguro"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">&nbsp;</td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneVlSeguro" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);" /></td>
                              <td class="principalLabel" align="right" height="26" width="35%">
                              	<bean:message key="regrasNegociacaoForm.permiteIsencaoJuros"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td colspan="2" class="principalLabel" width="54%"> 
		                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr> 
		                              <td class="principalLabelValorFixo"> 
		                              	<html:radio property="reneInIsencaojuros" value="S"/>Sim 
		                                <html:radio property="reneInIsencaojuros" value="N"/>N�o </td>
		                         </tr>
		                       </table>
		                     </td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.iof"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">&nbsp;</td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneNrIof" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);"/>
                              </td>
                              <td>
                               	<img src="webFiles/images/icones/funcao.gif" width="24" height="24" class="geralCursoHand" onclick="abreCampoFormula('reneNrIof');" alt="F�rmula">
                              </td>
                              <td class="principalLabel"  align="right" height="26" width="35%">
                              	<bean:message key="regrasNegociacaoForm.permiteIsencaoMulta"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							  <td colspan="2" class="principalLabel"> 
		                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr> 
		                              <td class="principalLabelValorFixo" width="54%">
		                              	<html:radio property="reneInIsencaomulta" value="S"/>Sim 
		                                <html:radio property="reneInIsencaomulta" value="N"/>N�o</td>
		                         </tr>
		                       </table>
		                     </td>                              
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.taxas"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">&nbsp;</td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneNrTaxaadm" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);" /></td>
                              
                              <td class="principalLabel" align="right" height="26" width="35%">
                              	<bean:message key="prompt.formula"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td colspan="2" class="principalLabel"> 
		                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr> 
		                              <td class="principalLabelValorFixo">
		                                <html:text property="reneDsFormula" style="width: 220px" maxlength="2000" name="regrasNegociacaoForm" styleClass="principalObjForm"/>
		                              </td>
		                              <td>
		                              	<img src="webFiles/images/icones/funcao.gif" width="24" height="24" class="geralCursoHand" onclick="abreCampoFormula('reneDsFormula');" alt="F�rmula">
		                              </td>
		                         	</tr>
		                       	  </table>
		                      </td>                              
                              
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      
					  <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="promtp.valorRemuneracaoOperador"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">&nbsp;</td>
                              <td width="13%" class="principalLabel">
                              	<html:text property="reneVlRemunoperador" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);"/></td>
                              <td class="principalLabel" align="right" height="26" width="35%">
                              	<bean:message key="promtp.tipoRemuneracao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td colspan="2" class="principalLabel"> 
		                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr> 
		                              <td class="principalLabelValorFixo">
		                              	<html:radio property="reneInTpremunoperador" value="F"/>Fixo
		                                <html:radio property="reneInTpremunoperador" value="P"/>Percentual</td>
		                         	</tr>
		                       	  </table>
		                      </td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="promtp.valorRemuneracaoOperadora"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" align="right" class="principalLabel">&nbsp;</td>
                              <td width="13%" class="principalLabel"> 
                                <html:text property="reneVlRemunoperadora" maxlength="13" name="regrasNegociacaoForm" styleClass="principalObjForm" onblur="return numberValidate(this, 11, '.', ',', event);"/></td>
                              <td class="principalLabel" align="right" height="26" width="35%">
                              	<bean:message key="promtp.tipoRemuneracao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                              <td colspan="2" class="principalLabel"> 
		                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr> 
		                              <td class="principalLabelValorFixo"> 
		                              	<html:radio property="reneInTpremunoperadora" value="F"/>Fixo 
		                                <html:radio property="reneInTpremunoperadora" value="P"/>Percentual</td>
		                         	</tr>
		                       	  </table>
		                      </td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="6%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      
                      <tr> 
                        <td class="principalLabel" align="right" height="26" width="30%">
                        	<bean:message key="regrasNegociacaoForm.informacoesParaBoleto"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="46%"> 
		                      	<html:select property="idInboCdInfoboleto" name="regrasNegociacaoForm" styleClass="principalObjForm">
                              			<html:option value=""><bean:message key="prompt.selecione_uma_opcao"/></html:option>
                       					<logic:notEmpty name="cbCdtbInfoBoletoInbo">
                       						<bean:define name="cbCdtbInfoBoletoInbo" id="formapagtoVO"/>
                       						<html:options collection="formapagtoVO" property="field(ID_INBO_CD_INFOBOLETO)" labelProperty="field(INBO_DS_NOME)"/>
                       					</logic:notEmpty>
                               	</html:select>
                              </td>
                              <td width="64%" align="right"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalLabelValorFixo"> 
                                    </td>
                                    <td width="80%" class="principalLabel" align="right"> 
										<logic:equal value="" name="regrasNegociacaoForm" property="reneDhInativo">
											  
											<input type="checkbox" name="checkado" value="ativo"/>                              	                              	
												
										</logic:equal>	                              	          
										<logic:notEqual value="" name="regrasNegociacaoForm" property="reneDhInativo">                                
										 	
											<input type="checkbox" name="checkado" checked="checked" value="inativo"/>                              	
												
										</logic:notEqual>                      
										<bean:message key="regrasNegociacaoForm.inativo"/>
                              		</td>
                                  </tr>
                                </table>
                              </td>
                             
							</tr>
                          </table>
                          	<tr>
	                              <td class="principalLabel" align="right" height="26" width="30%">
	                              	<bean:message key="regrasNegociacaoForm.tipoarredondamento"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
	                              <td colspan="2" class="principalLabel" width="54%"> 
			                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                            <tr> 
			                              <td class="principalLabelValorFixo"> 
			                              	<html:radio property="reneInTparredondamento" value="C"/>Cima 
			                                <html:radio property="reneInTparredondamento" value="B"/>Baixo 
			                                <html:radio property="reneInTparredondamento" value="T"/>Truncar 
			                              </td>
			                         	</tr>
			                       	  </table>
			                      </td>
		                      </tr>
                        </td>
                      </tr>
					</table>
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/new.gif" alt="Novo" name="imgNovo" id="imgNovo" class="geralCursoHand" onclick="novo();"/>
                      </td>
                      <td width="25" align="center">
                      	<img src="webFiles/images/botoes/gravar.gif" alt="Gravar" name="imgGravar" id="imgGravar" class="geralCursoHand" onclick="salvar();"/>
                      </td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/cancelar.gif" alt="Cancelar" name="imgCancelar" id="imgCancelar" class="geralCursoHand" onclick="cancelar()"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
 </html:form>
</body>

<script>
	temPermissaoIncluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_REGRA_NEGOCIACAO_INCLUSAO_CHAVE%>');
	temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_REGRA_NEGOCIACAO_ALTERACAO_CHAVE%>');
	temPermissaoExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_REGRA_NEGOCIACAO_EXCLUSAO_CHAVE%>');
	
	function verificaPermissaoExcluir() {
		if (!temPermissaoExcluir){
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.regrasNegociacaoForm.btExcluir'+i+');');
			}
		}
	}
	
	verificaPermissaoExcluir();
</script>

<logic:equal name="regrasNegociacaoForm" property="userAction" value="init">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.regrasNegociacaoForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.regrasNegociacaoForm.imgGravar);
			desabilitaCamposRegraNegociacao();
		}
	</script>
</logic:equal>
<logic:equal name="regrasNegociacaoForm" property="userAction" value="alterar">
	<script>
		if(!temPermissaoAlterar) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.regrasNegociacaoForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.regrasNegociacaoForm.imgGravar);
			desabilitaCamposRegraNegociacao();
		}
	</script>
</logic:equal>
<logic:equal name="regrasNegociacaoForm" property="userAction" value="cancelar">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.regrasNegociacaoForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.regrasNegociacaoForm.imgGravar);
			desabilitaCamposRegraNegociacao();
		}	
	</script>
</logic:equal>	

<script>
	habilitaListaEmpresas();
	setaArquivoXml("CS_ASTB_IDIOMREGNEGOCI_IDNE.xml");
	habilitaTelaIdioma();
</script>

<script type="text/javascript">
function validaForm(){
	if ((document.regrasNegociacaoForm.reneNrDiaspripag.value != "") && (document.regrasNegociacaoForm.reneNrDiaspripagmax.value != "")){ 
		if ((parseInt(document.regrasNegociacaoForm.reneNrDiaspripagmax.value) < parseInt(document.regrasNegociacaoForm.reneNrDiaspripag.value))){ 
			alert("O valor m�ximo de dias n�o pode ser menor que o valor m�nimo de dias!");
			return false;
		}
	}
	
	if ((document.regrasNegociacaoForm.reneNrMinparcela.value != "") && (document.regrasNegociacaoForm.reneNrMaxparcela.value != "")){ 
		
		if((parseInt(document.regrasNegociacaoForm.reneNrMinparcela.value) > parseInt(document.regrasNegociacaoForm.reneNrMaxparcela.value))){
			alert('O n�mero m�nimo de parcelas � maior que o n�mero m�ximo de parcelas!');
			document.regrasNegociacaoForm.reneNrMaxparcela.focus();
			return false;
		}
	}
	
	if ((document.regrasNegociacaoForm.reneVlMindesconto.value != "") && (document.regrasNegociacaoForm.reneVlMaxdesconto.value != "")){ 
		
		if(parseInt(document.regrasNegociacaoForm.reneVlMindesconto.value) > parseInt(document.regrasNegociacaoForm.reneVlMaxdesconto.value)){
			alert('O valor m�nimo de desconto � maior que o valor m�ximo de desconto!');
			document.regrasNegociacaoForm.reneVlMaxdesconto.focus();
			return false;
		}
	}		
}
</script>

</html>