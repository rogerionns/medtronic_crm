<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/manifestacaoSimplificado.js"></SCRIPT>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>	
	
	<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<script>
	function carregaObjeto(cObj){

		var cObjTela = window.dialogArguments.document.getElementsByName(cObj)[0];

		window.document.regrasNegociacaoForm.reneDsFormula.value = cObjTela.value;
	}

	function adicionaFormula(cObj){

		var cObjTela = window.dialogArguments.document.getElementsByName(cObj)[0];
		
		cObjTela.value = window.document.regrasNegociacaoForm.reneDsFormula.value;
		window.close();
	}

	
</script>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="carregaObjeto('<%=request.getParameter("nameObj")%>')">
<html:form action="/regrasNegociacao.do" method="post">
<html:hidden property="userAction"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="10" width="166">&nbsp;<bean:message key="prompt.formula"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="150"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="15%" class="EspacoPequeno" align="right" valign="top">&nbsp;</td>
                <td width="71%" class="EspacoPequeno">&nbsp;</td>
                <td width="14%" class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td width="15%" class="principalLabel" align="right" valign="top">&nbsp;</td>
                <td width="71%" class="principalLabel"><bean:message key="prompt.descricao"/></td>
                <td width="14%" class="principalLabel">&nbsp;</td>
              </tr>
              <tr> 
                <td width="15%" class="principalLabel" align="right" valign="top">&nbsp;</td>
                <td width="71%" class="principalLabel"> 
                	<html:textarea property="reneDsFormula" styleClass="principalObjForm" rows="6"/>
                </td>
                <td width="14%" class="principalLabel"><img src="webFiles/images/botoes/setaDown.gif" alt="<bean:message key="prompt.Adicionar"/>" class="geralCursoHand" onclick="adicionaFormula('<%=request.getParameter("nameObj")%>');"/></td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
    <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
   <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand" align="right"></td>
  </tr>
  
  </table>
 </html:form> 
</body>
</html>
