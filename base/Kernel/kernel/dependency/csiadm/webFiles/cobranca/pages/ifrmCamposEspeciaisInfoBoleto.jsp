<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<link rel="stylesheet" href="webFiles/css/estilos.css" type="text/css">
<SCRIPT TYPE="text/javascript" SRC="webFiles/cobranca/js/infoBoleto.js"></SCRIPT>
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script type="text/javascript">
	var instrucoes = '';
	var win;

	function sair() {
		if(confirm('Tem certeza que deseja salvar as alterações?')) {
			win.document.getElementById("divInstrucoes").innerHTML = '';
			win.document.getElementById("divInstrucoes").innerHTML = document.camposEspeciaisForm.instrucoes.value;
			win.document.forms[0].inboDsInstrucoes.value = document.camposEspeciaisForm.instrucoes.value;
		}
		window.close();
	}
	
	function adicionaCamposEspecial() {		
		if(document.camposEspeciaisForm.camposEspecias.value != '') {
			var elementRef = document.forms[0].instrucoes;
			var valor = document.camposEspeciaisForm.camposEspecias.value;
			
			if ( document.selection ) { // Internet Explorer...
				elementRef.focus();
				var selectionRange = document.selection.createRange();
				selectionRange.text = valor;
			} else if ((elementRef.selectionStart) || (elementRef.selectionStart == '0')) { // Mozilla/Netscape...
				var startPos = elementRef.selectionStart;
				var endPos = elementRef.selectionEnd;
				elementRef.value = elementRef.value.substring(0, startPos) +
			 	valor + elementRef.value.substring(endPos, elementRef.value.length);
			 } else {
			 	elementRef.value += valor;
			 }
		} else {
			alert('Selecione um campo especial!');
		}	
	}
</script>
</head>

<body class="framePadrao" onload="showError('<%=request.getAttribute("msgerro")%>');">
	<html:form action="/infoBoleto" styleId="camposEspeciaisForm">		
		<div id="bordaPrincipal">
			<div class="titulo">
				Campos Especiais
			</div>
			
			<div class="borda" style=" height : 250px;">	
				<div style="float: left; width : 100%px; height : 200px;">
					<table width="99%" align="center" border="0" cellspacing="2" cellpadding="0">
						<tr>
							<td>
								<table width="90%" align="center" border="0" cellspacing="2" cellpadding="0">
									<tr>
								      	<td width="16%" align="right" class="principalLabel">Campos Especias <img src="webFiles/images/icones/setaAzul.gif" width="6" height="7"> </td>
									  	<td width="77%"> 
											<select name="camposEspecias" class="principalObjForm">
												<option value=""><bean:message key="prompt.selecione_uma_opcao" /></option>
												<option value="[[Juros]]">Juros</option>
												<option value="[[Multa]]">Multa</option>
											</select>
									  	</td>
										<td width="7%"><img src="webFiles/images/botoes/setaDown.gif" alt="Adicionar campo especial" onclick="adicionaCamposEspecial()" class="geralCursoHand"> </td>
									</tr>
								</table>
								<table width="90%" align="center" border="0" cellspacing="2" cellpadding="0">
									<tr>
								      	<td width="16%" align="right" class="principalLabel">Instruções <img src="webFiles/images/icones/setaAzul.gif" width="6" height="7"> </td>
									  	<td width="84%"> 
											<textarea filter="false" style="width:475px;" name="instrucoes" styleClass="principalObjForm" rows="13" cols="60" onkeyup="return isMaxLength(this, 4000)"></textarea>
									  	</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<br>
			<div style="clear: left; float: right">
				<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" class="geralCursoHand" onClick="sair();">
			</div>
		</div>
		<script>
		
		function inicio() {
			win = (window.dialogArguments?window.dialogArguments:window.opener);
			instrucoes = win.document.forms[0].inboDsInstrucoes.value;
			document.camposEspeciaisForm.instrucoes.value = instrucoes;
		}
		
		inicio();
		</script>
	</html:form>
</body>
</html>

<script type="text/javascript" SRC="webFiles/funcoes/funcoesMozilla.js"></script>