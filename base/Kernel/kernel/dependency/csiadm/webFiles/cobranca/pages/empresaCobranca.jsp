<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String fileIncludeIdioma="../../../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="webFiles/cobranca/js/empresaCobranca.js"></SCRIPT>
<script type="text/javascript" src="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<script type="text/javascript" src="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<script type="text/javascript" src="/plusoft-resources/javascripts/TratarDados.js"></script>
<link rel="stylesheet" href="webFiles/cobranca/css/global.css" type="text/css">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<script type="text/javascript">
	function pressEnter(ev) {
    	if (ev.keyCode == 13) {
    		pesquisar();
    	}
	}

	function FormataCIC (numCIC) {
		numCIC = String(numCIC);
		switch (numCIC.length){
		case 11 :
		 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
		case 14 :
		 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
		default : 
					// Se estiver em formato inv�lido, deixa como est�.
					alert("O CNPJ informado est� em um formato inv�lido.");
					
				 	return numCIC;
		}
	}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="fecharAguarde();showError('<%=request.getAttribute("msgerro")%>');AtivarPasta('<bean:write name="empresaCobrancaForm" property="flagAba"/>');verificaCampos('<bean:write name="empresaCobrancaForm" property="flagAba"/>','<bean:write name="empresaCobrancaForm" property="flagHabilitaCampos"/>');verificar();inicio();">
<html:form action="/empresaCobranca" method="post">
<html:hidden property="userAction"/>
<html:hidden property="alterando"/>
<html:hidden property="idEmpresaCobranca"/>
<html:hidden property="compara"/>
<html:hidden property="idEmprCdEmpresa"/>
<input type="hidden" name="codigo" value=""/>			
<input type="hidden" name="check" value=""/> 
<input type="hidden" name="idEmpreco" value=""/>
<input type="hidden" name="dsEmpresa" value=""/>
<input type="hidden" name="dsCnpj" value=""/>
<input type="hidden" name="dsEndereco" value=""/>
<input type="hidden" name="dsNumero" value=""/>
<input type="hidden" name="dsBairro" value=""/>
<input type="hidden" name="dsCep" value=""/>
<input type="hidden" name="dsUf" value=""/>
<input type="hidden" name="dsCidade" value=""/>
<input type="hidden" name="dsContato" value=""/>
<input type="hidden" name="dsTelefone" value=""/>
<input type="hidden" name="dsEmail" value=""/>
<input type="hidden" name="emcoDhInativo" value=""/>


  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="empresaCobranca.title.descricao"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>      
          <td valign="top" height="500"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado" name="tdProcurar" id="tdProcurar" onClick="AtivarPasta('pesquisar');"><bean:message key="empresaCobranca.janela.procurar"/></td>
                <td class="principalPstQuadroLinkNormalMaior" name="tdEstrategias" id="tdEstrategias" onClick="AtivarPasta('editar');"><bean:message key="empresaCobranca.janela.empresadecobranca"/></td>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr> 
                <td height="400" valign="top"> 
                
                  <div id="Procurar" style="position:absolute; width:99%; height:380px; z-index:1; visibility: visible;"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="principalLabel"><bean:message key="empresaCobrancaForm.descricao"/></td>
                      </tr>
                      <tr> 
                        <td width="56%"> 
                          <html:text property="emcoDsEmpresaPesquisar" name="empresaCobrancaForm" styleClass="principalObjForm" maxlength="60" onkeyup="pressEnter(event)"/>  
                        </td>
                        <td width="44%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="pesquisar();" alt="Pesquisar"></td>
                      </tr>
                      <tr> 
                        <td width="56%">&nbsp;</td>
                        <td width="44%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2"> 
                        <div style="height:300px; overflow: auto;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLstCab" width="5%">&nbsp;</td>
                              <td class="principalLstCab" width="5%">&nbsp;</td>
                              <td class="principalLstCab" width="10%"><bean:message key="prompt.codigo"/></td>
                              <td class="principalLstCab" width="36%"><bean:message key="empresacobranca.lista.nome"/></td>
                              <td class="principalLstCab" width="35%"><bean:message key="empresacobranca.lista.contato"/></td>
                              <td class="principalLstCab" width="14%"><bean:message key="empresacobranca.lista.inativo"/></td>
                            </tr>
                             <logic:notEmpty name="CbCdtbEmprecobEmco" >
								<logic:iterate name="CbCdtbEmprecobEmco" id="CbCdtbEmprecobEmco" indexId="indice">
			                    	<tr class="geralCursoHand"> 
	           						    <td class="principalLstPar" width="4%" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" onclick="excluir('<bean:write name="CbCdtbEmprecobEmco" property="field(ID_EMCO_CD_EMPRECOB)"/>');" width="14" height="14" class="geralCursoHand" alt="Excluir"/></td>
			                            <td class="principalLstPar" width="4%" align="center" onclick="abrirModalIdioma('CS_ASTB_IDIOMAEMPRECOB_IDCO.xml','<bean:write name="CbCdtbEmprecobEmco" property="field(ID_EMCO_CD_EMPRECOB)"/>');"><img src="webFiles/images/botoes/GloboAuOn.gif" width="18" height="18" class="geralCursoHand"/></td>
			                          	<td class="principalLstPar" width="10%"onclick="alterar('<bean:write name="CbCdtbEmprecobEmco" property="field(ID_EMCO_CD_EMPRECOB)"/>',
		                            			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_EMPRESA)"/>',
		                            			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CNPJ)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_ENDERECO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_NUMERO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_BAIRRO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CEP)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_UF)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CIDADE)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CONTATO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_TELEFONE)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_EMAIL)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EMCO_DH_INATIVO)"filter="html"/>');">&nbsp;<bean:write name="CbCdtbEmprecobEmco" property="field(ID_EMCO_CD_EMPRECOB)"/></td>
			                          <td class="principalLstPar" width="36%"onclick="alterar('<bean:write name="CbCdtbEmprecobEmco" property="field(ID_EMCO_CD_EMPRECOB)"/>',
		                            			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_EMPRESA)"/>',
		                            			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CNPJ)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_ENDERECO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_NUMERO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_BAIRRO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CEP)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_UF)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CIDADE)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CONTATO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_TELEFONE)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_EMAIL)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EMCO_DH_INATIVO)"filter="html"/>');">&nbsp;<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_EMPRESA)"/></td>
			                          <td class="principalLstPar" width="35%" align="left" onclick="alterar('<bean:write name="CbCdtbEmprecobEmco" property="field(ID_EMCO_CD_EMPRECOB)"/>',
		                            			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_EMPRESA)"/>',
		                            			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CNPJ)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_ENDERECO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_NUMERO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_BAIRRO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CEP)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_UF)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CIDADE)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CONTATO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_TELEFONE)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_EMAIL)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EMCO_DH_INATIVO)"filter="html"/>');">&nbsp;<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CONTATO)"/></td>
			                          			
			                          <td class="principalLstPar" width="14%"onclick="alterar('<bean:write name="CbCdtbEmprecobEmco" property="field(ID_EMCO_CD_EMPRECOB)"/>',
		                            			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_EMPRESA)"/>',
		                            			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CNPJ)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_ENDERECO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_NUMERO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_BAIRRO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CEP)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_UF)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CIDADE)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_CONTATO)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_TELEFONE)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" property="field(EMCO_DS_EMAIL)"/>',
		                              			'<bean:write name="CbCdtbEmprecobEmco" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EMCO_DH_INATIVO)"filter="html"/>');">&nbsp;<bean:write name="CbCdtbEmprecobEmco" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EMCO_DH_INATIVO)" filter="html"/></td>
	                                 </tr>	
	                                 <script>
		                            	countRegistro++;
		                            </script>										
			                      </logic:iterate>
			                      </logic:notEmpty>		
									<logic:empty name="CbCdtbEmprecobEmco">
										<tr> 
											<td align="center" colspan="7" > 
												<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
													<tr id="nenhumRegistro" ><td height="260" width="800" align="center"  class="principalLstPar"><br><b><bean:message key="lista.nenhum.registro.encontrado"/> </b></td></tr>
												</table>
											&nbsp;</td>
										</tr>
									</logic:empty>			                      	                                          
                          </table>
                          </div>
                        </td>
                      </tr>
                      </table>
                  </div>
                  <div id="Estrategias" style="position:absolute; width:99%; height:390px; z-index:2; visibility: hidden;"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                     <tr>
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="prompt.codigo"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="20%"> 
                                <html:text property="idEmcoCdEmpreco" name="empresaCobrancaForm" styleClass="principalObjForm" disabled="true"/>
                              </td>
                              <td width="80%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel">&nbsp;</td>
                      </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="empresaCobrancaForm.nome"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                                <html:text property="emcoDsEmpresa" maxlength="30" name="empresaCobrancaForm" styleClass="principalObjForm"/>
                              </td>
                              <td width="27%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="empresaCobrancaForm.cnpj"/>  
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="20%">
                                <html:text property="emcoDsCnpj" maxlength="14" name="empresaCobrancaForm" styleClass="principalObjForm" onblur="this.value=FormataCIC(this.value);" onkeydown="return ValidaTipo(this, 'N', event);" />
                              </td>
                              <td width="80%" class="principalLabel">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="empresaCobrancaForm.endereco"/>
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                                <html:text property="emcoDsEndereco" maxlength="60" name="empresaCobrancaForm" styleClass="principalObjForm"/>
                              </td>
                              <td width="27%" class="principalLabel">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="empresaCobrancaForm.numero"/>
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="20%"> 
                                <html:text property="emcoDsNumero" maxlength="6" name="empresaCobrancaForm" styleClass="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);" />
                              </td>
                              <td width="80%" class="principalLabel"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="21%" align="right" class="principalLabel"><bean:message key="empresaCobrancaForm.bairro"/> 
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="45%" class="principalLabel"> 
                                      <html:text property="emcoDsBairro" maxlength="30" name="empresaCobrancaForm" styleClass="principalObjForm"/>
                                    </td>
                                    <td width="34%" class="principalLabel">&nbsp;</td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="empresaCobrancaForm.uf"/>
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="20%"> 
                                <html:text property="emcoDsUf" maxlength="02" name="empresaCobrancaForm" styleClass="principalObjForm"/>
                              </td>
                              <td width="80%" class="principalLabel"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="21%" align="right" class="principalLabel"><bean:message key="empresaCobrancaForm.cidade"/>  
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="45%" class="principalLabel"> 
                                      <html:text property="emcoDsCidade" maxlength="30" name="empresaCobrancaForm" styleClass="principalObjForm"/>
                                    </td>
                                    <td width="34%" class="principalLabel">&nbsp;</td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="empresaCobrancaForm.cep"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="20%"> 
                                <!--<html:text property="emcoDsCep" maxlength="09" name="empresaCobrancaForm" styleClass="principalObjForm" onkeypress="return digitos(event, this);" onkeyup="MascaraCEP(this, event);"/>-->
                                <html:text property="emcoDsCep" maxlength="09" name="empresaCobrancaForm" styleClass="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);" />
                              </td>
                              <td width="80%" class="principalLabel">&nbsp; 
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="empresaCobrancaForm.contato"/>
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                                <html:text property="emcoDsContato" maxlength="30" name="empresaCobrancaForm" styleClass="principalObjForm"/>
                              </td>
                              <td width="27%" class="principalLabel">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="empresaCobrancaForm.telefone"/>
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">                                                   
                            <tr> 
                              <td width="20%"> 
                                <!--<html:text property="emcoDsTelefone" maxlength="13" name="empresaCobrancaForm" styleClass="principalObjForm" onkeypress="return digitos(event, this);" onkeyup="MascaraTelefone(this, event); return false;"/>-->
                                <html:text property="emcoDsTelefone" maxlength="13" name="empresaCobrancaForm" styleClass="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);"  />
                              </td>
                              <td width="80%" class="principalLabel">&nbsp; </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="empresaCobrancaForm.email"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                                <html:text property="emcoDsEmail" maxlength="80" name="empresaCobrancaForm" styleClass="principalObjForm" onblur="validaEmail(this)"/>
                              </td>
                              <td width="27%" class="principalLabel">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%">&nbsp;</td>
                        <td colspan="2" class="principalLabel" align="right"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="61%">&nbsp; </td>
                              <td width="39%" class="principalLabel">   

								<logic:equal value="" name="empresaCobrancaForm" property="emcoDhInativo">
                              		<input type="checkbox" name="inativo" value="ativo"/>                              	                              	
                              	</logic:equal>
                              	<logic:notEqual value="" name="empresaCobrancaForm" property="emcoDhInativo">                                
                              		<input type="checkbox" name="inativo" value="inativo" checked="checked"/>                              	
                              	</logic:notEqual> 	                              	          
                              	<bean:message key="empresaCobrancaForm.inativo"/>
                              	</td>             
                            </tr>
                          </table>                          
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/new.gif" alt="Novo" name="imgNovo" id="imgNovo" class="geralCursoHand" onclick="novo();"/>
                      </td>
                      <td width="25" align="center">
                      	<img src="webFiles/images/botoes/gravar.gif" alt="Gravar" name="imgGravar" id="imgGravar" class="geralCursoHand" onclick="salvar();"/>
                      </td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/cancelar.gif" alt="Cancelar" name="imgCancelar" id="imgCancelar" class="geralCursoHand" onclick="cancelar()"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>
</body>

<script>
	temPermissaoIncluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_COBRANCA_INCLUSAO_CHAVE%>');
	temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_COBRANCA_ALTERACAO_CHAVE%>');
	temPermissaoExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_COBRANCA_EXCLUSAO_CHAVE%>');
	
	function verificaPermissaoExcluir() {
		if (!temPermissaoExcluir){
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.empresaCobrancaForm.btExcluir'+i+');');
			}
		}
	}
	
	verificaPermissaoExcluir();
</script>

<logic:equal name="empresaCobrancaForm" property="userAction" value="init">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_COBRANCA_ALTERACAO_CHAVE%>', document.empresaCobrancaForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_COBRANCA_ALTERACAO_CHAVE%>', document.empresaCobrancaForm.imgGravar);
			desabilitaCamposEmpresaCobranca();
		}
	</script>
</logic:equal>
<logic:equal name="empresaCobrancaForm" property="userAction" value="alterar">
	<script>
		if(!temPermissaoAlterar) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_COBRANCA_ALTERACAO_CHAVE%>', document.empresaCobrancaForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_COBRANCA_ALTERACAO_CHAVE%>', document.empresaCobrancaForm.imgGravar);
			desabilitaCamposEmpresaCobranca();
		}
	</script>
</logic:equal>
<logic:equal name="empresaCobrancaForm" property="userAction" value="cancelar">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_COBRANCA_ALTERACAO_CHAVE%>', document.empresaCobrancaForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_COBRANCA_ALTERACAO_CHAVE%>', document.empresaCobrancaForm.imgGravar);
			desabilitaCamposEmpresaCobranca();
		}	
	</script>
</logic:equal>

<script>
	habilitaListaEmpresas();
	setaArquivoXml("CS_ASTB_IDIOMAEMPRECOB_IDCO.xml");
	habilitaTelaIdioma();
</script>

<script type="text/javascript">
function MascaraCEP(campo, teclaPress) {
  if (window.event){
    var tecla = teclaPress.keyCode;
  } else {
    tecla = teclaPress.which;
  }
  var s = new String(campo.value);
  s = s.replace(/(\.|\(|\)|\/|\-| )+/g,'');
   tam = s.length + 1;
  if (tam > 5 && tam < 7){
    campo.value = s.substr(0,5) + '-' + s.substr(5, tam);
    }
    
}

function digitos(event){
  if (window.event) {
    // IE
    key = event.keyCode;
  } 
  else if (event.which) {
    // netscape
    key = event.which;
  }
  if (key != 8 || key != 13 || key < 48 || key > 57)
    return (((key > 47) && (key < 58)) || (key == 8 ) || (key == 13));
    return true;
}

function MascaraTelefone(campo, teclaPress){ 
   if (window.event){
    var tecla = teclaPress.keyCode;
  } else {
    tecla = teclaPress.which;
  }
   
   if(campo.value.length == 0)
     campo.value = '(' + campo.value;

   if(campo.value.length == 3)
      campo.value = campo.value + ')';

 if(campo.value.length == 8)
     campo.value = campo.value + '-';
}
</script>
</html>