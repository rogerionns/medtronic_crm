<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/cadastroMetas.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="parent.parent.document.all.item('LayerAguarde').style.visibility = 'hidden';">

<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td align="right" class="EspacoPequeno" width="4%">&nbsp;</td>
    <td class="EspacoPequeno" align="right" width="89%">&nbsp;</td>
    <td width="7%" class="EspacoPequeno">&nbsp;</td>
  </tr>
  <tr> 
    <td width="4%" align="right" class="principalLabel" height="25">&nbsp;</td>
    <td align="right" class="principalLabel" height="25" width="89%">
    <div style="height:200px; overflow: auto"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLstCab" width="8%">&nbsp; </td>
          <td width="25%" class="principalLstCab">&nbsp;<bean:message key="cadastroMetasLista.campanha"/></td>
          <td width="25%" class="principalLstCab"><bean:message key="cadastroMetasLista.subCampanha"/></td>
          <td width="22%" class="principalLstCab"><bean:message key="cadastroMetasLista.resultado"/></td>
          <td width="20%" class="principalLstCab" align="center">&nbsp;<bean:message key="cadastroMetasLista.qtd"/>&nbsp;</td>
        </tr>
        
      </table>
      </div>
    </td>
    <td width="7%">&nbsp;</td>
  </tr>
</table>
</body>
</html>