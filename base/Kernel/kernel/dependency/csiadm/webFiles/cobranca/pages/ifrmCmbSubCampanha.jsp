<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<script type="text/javascript">
	function inicio() {	
		//Verifica se deve desabilitar o campo ou n�o
		acaoCobrancaForm.idPublCdPublico.disabled = parent.acaoCobrancaForm.idCampCdCampanha.disabled;
	
		if(parent.document.forms[0].idPublCdPublico.value != ''){
			document.forms[0].idPublCdPublico.value = parent.document.forms[0].idPublCdPublico.value;
		}
	}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/acaoCobranca" styleId="acaoCobrancaForm" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
	  <td>
	    <html:select property="idPublCdPublico" styleClass="principalObjForm" style="width:345px">
			<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option>
			<logic:present name="csCdtbPublicoPublVector">
				<html:options collection="csCdtbPublicoPublVector" property="idPublCdPublico" labelProperty="publDsPublico"/>
			</logic:present>
      </html:select>
    </td>
  </tr>
</table>
</html:form>
</body>          
</html>