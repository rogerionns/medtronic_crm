<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/cobranca/js/estrategias.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>

<script type="text/javascript">
	var countCenarios = new Number(0);
	var cenarioSelecionado = 9999;

	function inicio() {
		//Verifica se o campo deve estar desabilitado ou n�o
		var habilita = parent.document.forms[0].estrDsEstrategia.disabled;
		if(habilita) {
			setPermissaoImageDisable(false, document.forms[0].imgConfirmarCenario);
		}
	}

	function cenariosSimulados(){	
		try{
			if(countCenarios > 1) {
				for (cont = 0;cont < document.forms[0].selecionado.length; cont++){
					if(document.forms[0].selecionado[cont].checked){
						cenarioSelecionado = document.forms[0].selecionado[cont].value;
					}
				}
			} else {
				if(document.forms[0].selecionado.checked) {
					cenarioSelecionado = document.forms[0].selecionado.value;
				}
			}
		}catch(e){}
		
		if(cenarioSelecionado == 9999) {
			alert('� necess�rio selecionar um cen�rio!');
			return false;
		} else {
			parent.ifInclusao.location.href = "estrategias.do?userAction=carregaifInclusao&idSelecionado=" + cenarioSelecionado;
			parent.AtivarPasta2('INCLUSAO');
		}
	} 
	
	function checaCenario(obj) {
		cenarioSelecionado = obj.value;
	}
		
	function checkedRadio(){
		var cenarioChecado = parent.document.forms[0].elements["idSelecionado"].value;
		
		try{
			if(countCenarios > 1) {
				for (i = 0; i < document.forms[0].elements["selecionado"].length; i++){
					if (document.forms[0].elements["selecionado"][i].value == cenarioChecado){
						document.forms[0].elements["selecionado"][i].checked = true;
						cenarioSelecionado = cenarioChecado;
					} 
				}
			} else if (document.forms[0].elements["selecionado"].value == cenarioChecado){
				document.forms[0].elements["selecionado"].checked = true;	
				cenarioSelecionado = cenarioChecado;
			}
		}catch(e){} 
	}
	
	function limpaCriterios(){
		parent.ifCriteriosSimulados.location.href = "estrategias.do?userAction=ifCriteriosSimulados&limparCriterios=true";
	}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="inicio();limpaCriterios();checkedRadio();">
	<html:form action="/estrategias" method="post">
		<div style=" height:285px; overflow: auto;"> 
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr> 
	              <td class="principalLstCab" width="4%">&nbsp;</td>
	              <td width="42%" class="principalLstCab"><bean:message key="estrategiaForm.lista.cenario"/></td>
	              <td width="25%" class="principalLstCab"><bean:message key="estrategiaForm.lista.registroAlocados"/></td>
	              <td width="25%" class="principalLstCab" align="center"><bean:message key="estrategiaForm.lista.valorCobranca"/></td>
	              <!--<td width="23%" class="principalLstCab" align="center"><bean:message key="estrategiaForm.lista.rec.Historica"/></td>
	              <td width="24%" class="principalLstCab" align="center"><bean:message key="estrategiaForm.lista.projetoRecuperacao"/></td>-->
	              <td class="principalLstCab" width="4%">&nbsp;</td>
	            </tr>
                <logic:notEmpty name="cenarioSimulado">
                	<logic:iterate name="cenarioSimulado" id="cenarioSimulado">
                    	<tr>
	                   		<td class="principalLstPar" width="4%"><input type="radio" name="selecionado" value="<bean:write name="cenarioSimulado" property="field(id)"/>" onclick="checaCenario(this)" /></td>
	                           <td width="42%" class="principalLstPar">&nbsp;<bean:write name="cenarioSimulado" property="field(nomeCenario)"/>
			                </td>
			                <td width="25%" class="principalLstPar">&nbsp;<bean:write name="cenarioSimulado" property="field(totalRegistro)"/></td>	
			                <td width="25%" class="principalLstPar"  align="center">&nbsp;<bean:write name="cenarioSimulado" property="field(totalValorCobranca)" format="##,###,##0.00"/></td>
							<!--<td width="23%" class="principalLstPar" align="center">&nbsp;<bean:write name="cenarioSimulado" property="field(percentual)" format="#0.00"/>%
			                </td>
							<td width="24%" class="principalLstPar" align="center">&nbsp;<bean:write name="cenarioSimulado" property="field(totalRecuperado)" format="##,###,##0.00"/>
			                </td>-->
			                <td class="principalLstPar" width="4%">&nbsp;</td>											 			                                    																					 				                                    																					 
                        </tr>
                        <script>
                        	countCenarios++;
                        </script>
					</logic:iterate>
				</logic:notEmpty>
			</table>
		</div>
		 <div id="Layer1" style="left:640px; width:100%; height:27px; z-index:5"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr align="right"> 
                <td><img src="webFiles/images/botoes/confirmaEdicao.gif" name="imgConfirmarCenario" id="imgConfirmarCenario" width="21" height="18" alt="Confirmar" class="geralCursoHand" onClick="cenariosSimulados()"></td>
              </tr>
            </table>
          </div>
	</html:form>
</body>          
</html>