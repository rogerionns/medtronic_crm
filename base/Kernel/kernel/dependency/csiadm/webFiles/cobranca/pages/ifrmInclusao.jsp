<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/cobranca/js/estrategias.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
	
<script type="text/javascript">
	function inicio() {
		//Verifica se o campo deve estar desabilitado ou n�o
		var habilita = parent.document.forms[0].estrDsEstrategia.disabled;
		if(habilita) {
			setPermissaoImageDisable(false, document.forms[0].imgVisualizarCriterio);
			setPermissaoImageDisable(false, document.forms[0].imgInclusaoAcoes);
		}
	}

	function adicionarAcao(idCriterio){
		//window.open("estrategias.do?userAction=popupInclusaoAcoesEstrategias&idSelecionado=" + document.forms[0].idSelecionado.value + "&idCriterio=" + idCriterio,'0','help=no; scroll=no; Status=NO; width=800px; height=390px; top=0px; left=200px');
		showModalDialog('estrategias.do?userAction=popupInclusaoAcoesEstrategias&idSelecionado=' + document.forms[0].idSelecionado.value + '&idCriterio=' + idCriterio, 0, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:425px,dialogTop:0px,dialogLeft:200px');
	}
	
	function visualizarCriterio(idCriterio) {
		//window.open('estrategias.do?userAction=popupCriterioSimularInclusoes&idCriterio=' + idCriterio, 'x', 'help=no; scroll=no; Status=NO; width=450px; height=190px; Top=100px; left=200px');
		showModalDialog('estrategias.do?userAction=popupCriterioSimularInclusoes&idCriterio=' + idCriterio, 0, 'help:no;scroll:no;Status:NO;dialogWidth:450px;dialogHeight:190px,dialogTop:100px,dialogLeft:200px');
	}
</script>	
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="inicio()">
<html:form styleId="estrategiasForm">
<html:hidden property="idSelecionado"/>

	<div style="height:270px; overflow: auto;"> 
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
				<td width="29%" class="principalLstCab">&nbsp;&nbsp;<bean:message key="estrategiaForm.lista.criterios"/></td>
				<td width="19%" class="principalLstCab"><bean:message key="estrategiaForm.lista.registroAlocados"/></td>
				<td width="19%" class="principalLstCab" align="center"><!--<bean:message key="estrategiaForm.lista.valorCobranca"/>--></td>
				<td width="24%" class="principalLstCab" align="center"><!--<bean:message key="estrategiaForm.lista.rec.Historica"/>--></td>
				<td width="9%" class="principalLstCab" align="center">&nbsp;</td>
			</tr>
			<logic:notEmpty name="inclusaoAcao">
				<logic:iterate name="inclusaoAcao" id="inclusaoAcao">
					<tr>
						<td class="principalLstPar">&nbsp;<bean:write name="inclusaoAcao" property="field(nomeCriterio)"/>
						</td>
						<td class="principalLstPar" align="center">&nbsp;<bean:write name="inclusaoAcao" property="field(registrosAlocados)"/>
						</td>
						<td class="principalLstPar" align="center">&nbsp;<!--<bean:write name="inclusaoAcao" property="field(valorCobranca)" format="##,###,##0.00"/>-->
						</td>
						<td  class="principalLstPar" align="center">&nbsp;<!--<bean:write name="inclusaoAcao" property="field(recuperacaoHistorica)" format="#0.00"/>-->
						</td>
						<td  class="principalLstPar" align="center">&nbsp;
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr align="center">
									<!-- <td width="50%">&nbsp;<img src="webFiles/images/botoes/lupa.gif" width="15" height="15" onClick="window.open('estrategias.do?userAction=popupCriterioSimularInclusoes&idCriterio=<bean:write name="inclusaoAcao" property="field(id)"/>','x','help=no; scroll=no; Status=NO; width=450px; height=190px; Top=100px; left=200px'); " class="geralCursoHand" alt="Crit�rio Sele��o"></td>-->
									<td width="50%">&nbsp;<img src="webFiles/images/botoes/lupa.gif" name="imgVisualizarCriterio" id="imgFecharCenario" width="15" height="15" onclick="visualizarCriterio('<bean:write name="inclusaoAcao" property="field(id)"/>')" class="geralCursoHand" alt="Crit�rio Sele��o"></td>
									<td width="50%">&nbsp;<img src="webFiles/images/icones/agendamentos.gif" name="imgInclusaoAcoes" id="imgInclusaoAcoes" width="25" height="24" alt="Inclus&atilde;o de A&ccedil;&otilde;es" onClick="adicionarAcao('<bean:write name="inclusaoAcao" property="field(id)"/>');" class="geralCursoHand">&nbsp;</td>
								</tr>
							</table>
						</td>                        																					 			                                    																					 			                                    																					 
					</tr>
				</logic:iterate>
			</logic:notEmpty>
		</table>
	</div> 
</html:form>	
</body>          
</html>