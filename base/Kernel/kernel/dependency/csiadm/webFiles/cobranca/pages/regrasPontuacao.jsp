<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<% 
String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/regrasPontuacao.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>

<script type="text/javascript">

var podeGravar = true;

	function verificaId() {
		var achouId = false;
		if(document.forms[0].codigoArray != undefined){
			for(i = 0; i < document.forms[0].codigoArray.length; i++) {
				if(document.forms[0].codigoArray[i].value == document.forms[0].idRegrCdRelacionada.value) {
					achouId = true;
				}
			}
		}
		
		if(!achouId) {
			alert('O IDRelacionado n�o existe!');
			podeGravar = false;
		}
	}
	
	
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('LayerAguarde').style.visibility = 'hidden';inicio();">
<html:form action="/regraPontuacao" method="post">
<html:hidden property="userAction"/>
<html:hidden property="lblIdRegrCdRegra"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="regraPontuacao.title.descricao"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="450"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="350"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="90" valign="top"> 
                  <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="regraPontuacaoForm.id"/>
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td width="8%" class="principalLabel"> 
                        <html:text property="idRegrCdRegra" styleClass="principalObjForm" maxlength="10" disabled="true" value=""/>                        
                      </td>
                      <td class="principalLabel" colspan="3">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="regraPontuacaoForm.campo"/> 
                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td colspan="2" class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="43%"> 
                              <html:select property="idCampCdCampo" styleClass="principalObjForm" disabled="true" value="">
                              	<html:option value="">-- Selecione uma op��o --</html:option>                             	             	
                              	<logic:notEqual value="" name="cbCdtbCampoCamp">             
                              		<bean:define name="cbCdtbCampoCamp" id="cbCdtbCampoCamp" />                 	
                              		<html:options collection="cbCdtbCampoCamp" property="idCampCdCampo" labelProperty="campoDsCampo"/>
                              	</logic:notEqual>
                              </html:select>
                            </td>
                            <td width="57%">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                      <td width="4%" class="principalLabel">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="regraPontuacaoForm.operador"/> 
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td colspan="2" class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="43%"> 
                              	<html:select property="regrInRegra" styleClass="principalObjForm" disabled="true" value="">
                              	  <html:option value="">-- Selecione uma op��o --</html:option>	
		                           	<logic:notEqual value="" name="cbDmtbOperacaoOper">             
		                           		<bean:define name="cbDmtbOperacaoOper" id="cbDmtbOperacaoOper" />                 	
		                           		<html:options collection="cbDmtbOperacaoOper" property="field(ID_OPER_CD_OPERACAO)" labelProperty="field(OPER_DS_OPERACAO)"/>
		                           	</logic:notEqual>
                              	</html:select>
                            </td>
                            <td width="57%" class="principalLabel" align="center">&nbsp; 
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="regraPontuacaoForm.valor"/> 
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td colspan="2" class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="43%"> 
                              <html:text property="regrDsValor" styleClass="principalObjForm" maxlength="60" disabled="true" value="" onkeypress="return(formataDecimal(this,',',event,9))"/>                              
                            </td>
                            <td width="57%">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                      <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="regraPontuacaoForm.idRelacionado"/>
                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td colspan="2" class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="43%"> 
                              
                              <html:text property="idRegrCdRelacionada" styleClass="principalObjForm" maxlength="60" disabled="true" value="" onblur="verificaId()" />                              
                           	 
                            </td>
                            <td width="57%">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                      <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="regraPontuacaoForm.pontuacao"/>
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td colspan="2" class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="43%"> 
                              <html:text property="regrNrPontuacao" styleClass="principalObjForm" maxlength="10" disabled="true" value="" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false;"/>                              
                            </td>
                            <td width="57%">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                      <!-- <td width="4%" class="principalLabel" colspan="2"><img src="images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand"></td> -->
                    </tr>
                    <tr> 
                      <td class="principalLabel" align="right" width="26%">&nbsp;</td>
                      <td colspan="2" class="principalLabel">&nbsp;</td>
                      <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                    </tr>
                  </table>
                  <div style="height: 250px; overflow: auto;">
                  <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                     
                      <td class="principalLstCab" width="4%">&nbsp;</td>
                      <td class="principalLstCab" width="7%"><bean:message key="regraPontuacao.lista.id"/></td>
                      <td class="principalLstCab" width="23%"><bean:message key="regraPontuacao.lista.campo"/></td>
                      <td class="principalLstCab" width="16%"><bean:message key="regraPontuacao.lista.operador"/></td>
                      <td class="principalLstCab" width="17%" align="center"><bean:message key="regraPontuacao.lista.valor"/></td>
                      <td class="principalLstCab" width="16%" align="center"><bean:message key="regraPontuacao.lista.idRelacionado"/></td>
                      <td class="principalLstCab" width="16%" align="center"><bean:message key="regraPontuacao.lista.pontuacao"/></td>
                    </tr>
                    <logic:notEmpty name="cbCdtbRegraRegr" >
                    	<logic:iterate name="cbCdtbRegraRegr" id="cbCdtbRegraRegr" indexId="indice">
	                      <tr class="geralCursoHand"> 
	                       
	                       <td class="principalLstPar" width="4%" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" onclick="excluir('<bean:write name="cbCdtbRegraRegr" property="idRegrCdRegra"/>');" width="14" height="14" class="geralCursoHand" alt="Excluir"/></td>
	                       
			                    	    
	                       
	                        <td class="principalLstPar" width="7%" onclick="alterar('<bean:write name="cbCdtbRegraRegr" property="idRegrCdRegra"/>','<bean:write name="cbCdtbRegraRegr" property="idCampCdCampo"/>','<bean:write name="cbCdtbRegraRegr" property="regrInRegra"/>','<bean:write name="cbCdtbRegraRegr" property="regrDsValor"/>','<bean:write name="cbCdtbRegraRegr" property="lblIdRegrCdRelacionada"/>','<bean:write name="cbCdtbRegraRegr" property="lblRegrNrPontuacao"/>');">&nbsp;<bean:write name="cbCdtbRegraRegr" property="idRegrCdRegra"/></td>
	                        <td class="principalLstPar" width="23%" onclick="alterar('<bean:write name="cbCdtbRegraRegr" property="idRegrCdRegra"/>','<bean:write name="cbCdtbRegraRegr" property="idCampCdCampo"/>','<bean:write name="cbCdtbRegraRegr" property="regrInRegra"/>','<bean:write name="cbCdtbRegraRegr" property="regrDsValor"/>','<bean:write name="cbCdtbRegraRegr" property="lblIdRegrCdRelacionada"/>','<bean:write name="cbCdtbRegraRegr" property="lblRegrNrPontuacao"/>');">&nbsp;<bean:write name="cbCdtbRegraRegr" property="lblDescricaoCampo"/></td>
	                        <td class="principalLstPar" width="16%" onclick="alterar('<bean:write name="cbCdtbRegraRegr" property="idRegrCdRegra"/>','<bean:write name="cbCdtbRegraRegr" property="idCampCdCampo"/>','<bean:write name="cbCdtbRegraRegr" property="regrInRegra"/>','<bean:write name="cbCdtbRegraRegr" property="regrDsValor"/>','<bean:write name="cbCdtbRegraRegr" property="lblIdRegrCdRelacionada"/>','<bean:write name="cbCdtbRegraRegr" property="lblRegrNrPontuacao"/>');">&nbsp;<bean:write name="cbCdtbRegraRegr" property="lblDescricaoOperacao"/></td>
	                        <td class="principalLstPar" width="17%"  align="center" onclick="alterar('<bean:write name="cbCdtbRegraRegr" property="idRegrCdRegra"/>','<bean:write name="cbCdtbRegraRegr" property="idCampCdCampo"/>','<bean:write name="cbCdtbRegraRegr" property="regrInRegra"/>','<bean:write name="cbCdtbRegraRegr" property="regrDsValor"/>','<bean:write name="cbCdtbRegraRegr" property="lblIdRegrCdRelacionada"/>','<bean:write name="cbCdtbRegraRegr" property="lblRegrNrPontuacao"/>');">&nbsp;<bean:write name="cbCdtbRegraRegr" property="regrDsValor"/></td>
	                        <td class="principalLstPar" width="16%"  align="center" onclick="alterar('<bean:write name="cbCdtbRegraRegr" property="idRegrCdRegra"/>','<bean:write name="cbCdtbRegraRegr" property="idCampCdCampo"/>','<bean:write name="cbCdtbRegraRegr" property="regrInRegra"/>','<bean:write name="cbCdtbRegraRegr" property="regrDsValor"/>','<bean:write name="cbCdtbRegraRegr" property="lblIdRegrCdRelacionada"/>','<bean:write name="cbCdtbRegraRegr" property="lblRegrNrPontuacao"/>');">&nbsp;<bean:write name="cbCdtbRegraRegr" property="lblIdRegrCdRelacionada"/></td>
	                        <td class="principalLstPar" width="16%"  align="center" onclick="alterar('<bean:write name="cbCdtbRegraRegr" property="idRegrCdRegra"/>','<bean:write name="cbCdtbRegraRegr" property="idCampCdCampo"/>','<bean:write name="cbCdtbRegraRegr" property="regrInRegra"/>','<bean:write name="cbCdtbRegraRegr" property="regrDsValor"/>','<bean:write name="cbCdtbRegraRegr" property="lblIdRegrCdRelacionada"/>','<bean:write name="cbCdtbRegraRegr" property="lblRegrNrPontuacao"/>');">&nbsp;<bean:write name="cbCdtbRegraRegr" property="lblRegrNrPontuacao"/></td>
	                        <input type="hidden" name="codigoArray" value='<bean:write name="cbCdtbRegraRegr" property="idRegrCdRegra"/>'>
	                       	<input type="hidden" name="codigoRelArray" value='<bean:write name="cbCdtbRegraRegr" property="lblIdRegrCdRelacionada"/>'>
	                      </tr>
                    	 <script>
							
		                    countRegistro++;
		                 </script>
                    	</logic:iterate>
                    </logic:notEmpty> 
                  </table>
                  </div>
                </td>
              </tr>
            </table>
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                      <td width="25" align="center">
                      	<a title="<bean:message key="botoes.incluir"/>" >
                      		<img src="webFiles/images/botoes/new.gif" name="imgNovo" class="geralCursoHand" onclick="novo();" width="14" height="16">
                      	</a>
                      </td>
                    
                      <td width="25" align="center">
                      	<a title="<bean:message key="botoes.salvar"/>">
                      		<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" class="geralCursoHand" onclick="return gravar()" width="20" height="20">
                      	</a>
                      </td>
                      
                      <td width="25" align="center">
                      	<a title="<bean:message key="botoes.cancelar"/>" onclick="cancelar();">
                      		<img src="webFiles/images/botoes/cancelar.gif" name="imgCancelar" class="geralCursoHand" width="20" height="20">
                      	</a>
                      </td>
                      
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>  
</body>
<logic:equal name="regraPontuacaoForm" property="userAction" value="init">
	<script>
		//Verifica permiss�o de gravar
		//setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_REGRA_PONTUACAO_INCLUSAO_CHAVE%>', document.regraPontuacaoForm.imgGravar, "<bean:message key='prompt.gravar'/>");
		
		if(!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_REGRA_PONTUACAO_INCLUSAO_CHAVE%>')) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_REGRA_PONTUACAO_ALTERACAO_CHAVE%>', document.regraPontuacaoForm.imgGravar);
			desabilitaCamposRegraPontuacao();
			document.regraPontuacaoForm.idRegrCdRegra.disabled= false;
			document.regraPontuacaoForm.idRegrCdRegra.value= '';
			document.regraPontuacaoForm.idRegrCdRegra.disabled= true;			
		}
			
		//Verifica permiss�o de alterar
		temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_REGRA_PONTUACAO_ALTERACAO_CHAVE%>');
		if (!temPermissaoAlterar){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_REGRA_PONTUACAO_ALTERACAO_CHAVE%>', document.regraPontuacaoForm.imgNovo);	
			desabilitaCamposRegraPontuacao();
			
		}
		
		//Verifica permiss�o de excluir
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_REGRA_PONTUACAO_EXCLUSAO_CHAVE%>')){
			//desabilitaCamposRegraPontuacao();
			
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.regraPontuacaoForm.btExcluir'+i+');');
			}
		}
	</script>
</logic:equal>
<script>
	habilitaListaEmpresas();
</script>
</html>