<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>-- Detalhes da Execu��o --</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<link rel="stylesheet" href="webFiles/css/estilos.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript">
	function inicio() {
		var win = window.dialogArguments;
		
		try {					
			if(win.document.forms[0].jaAtualizouUmaVez.value == '') {
				document.forms[0].registrosProcessados.value = '0';	
			}
		
			if(win.document.forms[0].mensagem.value == '') {
				setTimeout('autoRefresh()', 3000);
			} else {
				var mens = win.document.forms[0].mensagem.value;
				while(mens.indexOf("QBRLNH") > -1) {
					mens = mens.replace("QBRLNH", "\n");
				}
				
				alert(mens);
				document.getElementById("divAguarde").style.visibility = 'hidden';
			}
			
			win.document.forms[0].jaAtualizouUmaVez.value = 'true';
		} catch(e){}
	}
	
	function autoRefresh() {
		document.forms[0].target = this.name = "autoRefreshEstrategia";
		document.forms[0].submit();
	}	
</script>
</head>

<body class="framePadrao" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/estrategias" method="post">
	<html:hidden property="idEstrategia"/>
	<html:hidden property="jaAtualizouUmaVez"/>
	<html:hidden property="userAction" value="autoRefreshEstrategia"/>
	
	<div id="bordaPrincipal">
		<div class="titulo">
			Detalhes da Execu��o
		</div>
		
		<div class="borda" style=" height : 250px;">	
			<div style="float: left; width : 90%px; height : 200px;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
     				<tr> 
       					<td class="principalLabel" width="40%" align="right">
       						Total de registros processados <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
       					</td>
       					<td width="60%">
       						&nbsp;<html:text property="registrosProcessados" styleClass="principalLabelFundo" readonly="true"/>
       					</td>
       				</tr>
       			</table>
			</div>
		</div>
		<div style="clear: left; float: right">
			<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" class="geralCursoHand" onClick="javascript:window.close()">
		</div>
	</div>
	<div id="divAguarde" style="position:absolute; left:120px; top:90px; width:199px; height:148px; z-index:10; visibility: visible"> 
	  	<div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
	</div>
</html:form>
</body>
</html>