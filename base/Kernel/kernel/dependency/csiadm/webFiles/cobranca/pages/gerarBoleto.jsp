<%@page import="br.com.plusoft.fw.jboleto.bancos.BancosImpl"%>
<%@page import="br.com.plusoft.fw.jboleto.Banco"%>
<%@page import="br.com.plusoft.fw.jboleto.JBoleto"%>
<%@page import="br.com.plusoft.csi.adm.cobranca.helper.JBoletoBeanImpl"%>
<%@page import="java.util.Vector"%>


<%

String banco = request.getParameter("banco");
int codBanco = 0;

if(banco != null && !banco.equals("")){
 	codBanco = Integer.parseInt(banco);
}

String agencia = request.getParameter("agencia");
String dvAgencia = request.getParameter("dvAgencia");
String cc = request.getParameter("cc");
String dvCc = request.getParameter("dvCc");
//Sacado
String codCliente = request.getParameter("codCliente");
String nomeSacado = request.getParameter("nomeSacado");
String cpfSacado = request.getParameter("cpfSacado");
String enderecoSacado = request.getParameter("enderecoSacado");
String bairroSacado = request.getParameter("bairroSacado");
String cidadeSacado = request.getParameter("cidadeSacado");
String ufSacado = request.getParameter("ufSacado");
String cepSacado = request.getParameter("cepSacado");
//Local de pagamento
String localPagamento1 = request.getParameter("localPagamento1");
String localPagamento2 = request.getParameter("localPagamento2");
//Cedente
String cedente = request.getParameter("cedente");
//Instrucoes
String instrucao1 = request.getParameter("instrucao1");
String instrucao2 = request.getParameter("instrucao2");
String instrucao3 = request.getParameter("instrucao3");
String instrucao4 = request.getParameter("instrucao4");
String instrucao5 = request.getParameter("instrucao5");
//Dados do boleto
String dataDocumento = request.getParameter("dataDocumento");
String numeroDocumento = request.getParameter("numeroDocumento");
String especiaDoc = request.getParameter("especieDoc");
String aceite = request.getParameter("aceite");
String dataProcessamento = request.getParameter("dataProcessamento");
String usoBanco = request.getParameter("usoBanco");
String carteira = request.getParameter("carteira");
String especieMoeda = request.getParameter("especieMoeda");
String quantidadeMoeda = request.getParameter("quantidadeMoeda");
String valorMoeda = request.getParameter("valorMoeda");
String dataVencimento = request.getParameter("vencimento");
String nossoNumero = request.getParameter("nossoNumero");
String valorDocumento = request.getParameter("valorDocumento");
String valorDesconto = request.getParameter("valorDesconto");
String outrasDeducoes = request.getParameter("outrasDeducoes");
String moraMulta = request.getParameter("moraMulta");
String outrosAcrecimos = request.getParameter("outrosAcrecimos");
String valorCobrado = request.getParameter("valorCobrado");
String caminhoImagem = request.getParameter("caminhoImagem");
String inboDsTamnossonumero = request.getParameter("inboDsTamnossonumero");
String inboNrCodfornecidoagencia = request.getParameter("inboNrCodfornecidoagencia");
String tipoModalidadeCarteira = request.getParameter("tipoModalidadeCarteira");

JBoletoBeanImpl jBoletoBean = new JBoletoBeanImpl();

//agencia
jBoletoBean.setAgencia(agencia);
jBoletoBean.setDvAgencia(dvAgencia);
//Parâmetros necessários para o Banco Caixa Econômica
jBoletoBean.setCodigoOperacao("1111");
jBoletoBean.setCodigoFornecidoAgencia(inboNrCodfornecidoagencia);
//Parâmetros necessários para o Banco do Brasil
jBoletoBean.setNumConvenio("1");
//CC
jBoletoBean.setContaCorrente(cc);
jBoletoBean.setDvContaCorrente(dvCc);
//Cedente
jBoletoBean.setCedente(cedente);
jBoletoBean.setCodCliente(codCliente);
//Dados do boleto
jBoletoBean.setDataDocumento(dataDocumento);
jBoletoBean.setNoDocumento(numeroDocumento);
jBoletoBean.setEspecieDocumento(especiaDoc);
jBoletoBean.setAceite(aceite);
jBoletoBean.setDataProcessamento(dataProcessamento);
//usobanco
jBoletoBean.setCarteira(carteira);

jBoletoBean.setTipoModalidadeCarteira(tipoModalidadeCarteira);

jBoletoBean.setMoeda(especieMoeda);


jBoletoBean.setQtdMoeda(quantidadeMoeda);
jBoletoBean.setValorMoeda(valorMoeda);
jBoletoBean.setDataVencimento(dataVencimento);

if(inboDsTamnossonumero != null && !inboDsTamnossonumero.equals("")){
	jBoletoBean.setNossoNumero(nossoNumero, Integer.parseInt(inboDsTamnossonumero));
}

jBoletoBean.setValorBoleto(valorDocumento);
//Sacado
jBoletoBean.setNomeSacado(nomeSacado);
jBoletoBean.setEnderecoSacado(enderecoSacado);
jBoletoBean.setBairroSacado(bairroSacado);
jBoletoBean.setCidadeSacado(cidadeSacado);
jBoletoBean.setUfSacado(ufSacado);
jBoletoBean.setCepSacado(cepSacado);  
jBoletoBean.setCpfSacado(cpfSacado);
//Local de pagamento
jBoletoBean.setLocalPagamento(localPagamento1);
jBoletoBean.setLocalPagamento2(localPagamento2);
//jBoletoBean.setImagemMarketing("C:/temp/boleto.jpg");
Vector descricoes = new Vector();
//descricoes.add("Hospedagem I - teste descricao1 - R$ 39,90");
jBoletoBean.setDescricoes(descricoes);
//Instrucao
jBoletoBean.setInstrucao1(instrucao1);
jBoletoBean.setInstrucao2(instrucao2);
jBoletoBean.setInstrucao3(instrucao3);
jBoletoBean.setInstrucao4(instrucao4);
jBoletoBean.setInstrucao5(instrucao5);
//JBoleto jBoleto = new JBoleto();
JBoleto jBoleto = JBoleto.getInstance(jBoletoBean, BancosImpl.getBancoClazz(codBanco));
//jBoleto.addBoleto(jBoletoBean, codBanco);
response.setContentType("application/pdf");
response.setHeader("Content-disposition", "inline; filename=\"" + "boleto" + ".pdf\"");
//byte arquivo[] = jBoleto.writeToByteArray().toByteArray();
byte arquivo[] = jBoleto.getBytes();
response.setContentType("application/pdf");
response.setContentLength(arquivo.length);
ServletOutputStream ouputStream = response.getOutputStream();
ouputStream.write(arquivo, 0, arquivo.length);
ouputStream.flush();
//ouputStream.close();
%>