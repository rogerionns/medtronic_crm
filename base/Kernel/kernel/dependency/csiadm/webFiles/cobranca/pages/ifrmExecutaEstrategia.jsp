<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>

<script type="text/javascript">
	function inicio() {
		showError('<%=request.getAttribute("msgerro")%>');
		if(document.forms[0].mensagem.value != '') {
			parent.document.forms[0].mensagem.value = document.forms[0].mensagem.value;
		}
	}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="inicio();">
<html:form action="/estrategias" method="post">
<html:hidden property="mensagem"/>
</html:form> 
</body>
</html>
