<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/cobranca/js/estrategias.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>

<script type="text/javascript">
	function inicio() {
		//Verifica se o campo deve estar desabilitado ou n�o
		var habilita = parent.document.forms[0].estrDsEstrategia.disabled;
		if(habilita) {
			setPermissaoImageDisable(false, document.forms[0].imgAtualizarVisao);
			setPermissaoImageDisable(false, document.forms[0].imgListarVisao);
		}
		
		document.forms[0].idVisaCdVisao.disabled = habilita;
	}

	function criarEstrategia2() {
		if(document.forms[0].idVisaCdVisao.value == ''){
			alert("Por favor selecione uma vis�o");
			return;
		}
		
		parent.parent.document.all.item('LayerAguarde').style.visibility = 'visible';
		parent.ifCriteriosSimulados.location.href = "estrategias.do?userAction=adicionarCriteriosSimulados&limparCriterios=false&idVisaCdVisao=" + document.forms[0].idVisaCdVisao.value;	
		document.forms[0].idVisaCdVisao.value = "";
	}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="inicio();showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/estrategias" styleId="estrategiasForm" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
	  <td width="19%" align="right" class="principalLabel" height="28"><bean:message key="prompt.visao"/> 
	    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	  </td>
	  <td width="49%"> 
    	<html:select property="idVisaCdVisao" name="estrategiasForm" styleClass="principalObjForm">
			<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option>
			<logic:present name="csCdtbVisaoVisaVector">
				<html:options collection="csCdtbVisaoVisaVector" property="field(id_visa_cd_visao)" labelProperty="field(visa_ds_visao)"/>
			</logic:present>
    	</html:select>
    </td>
    <td width="4%">&nbsp;<img src="webFiles/images/icones/historico.gif" name="imgAtualizarVisao" id="imgAtualizarVisao" width="22" height="23" class="geralCursoHand" onclick="atualizaCmbVisao()" alt="Atualizar vis�o"></td>
    <td width="4%">&nbsp;<img src="webFiles/images/botoes/setaDown.gif" name="imgListarVisao" id="imgAtualizaVisao" width="21" height="18" class="geralCursoHand" onclick="criarEstrategia2()" alt="Listar" ></td>
    <td width="25%">&nbsp;</td>
  </tr>
</table>
</html:form>
</body>          
</html>