<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/estrategias.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/date-picker.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/TratarDados.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>

<script type="text/javascript">
function excluirAcao(idAcaocriterio){
	if(confirm('Confirma a exclus�o?')){
		parent.document.forms[0].elements["idAcaocriterio"].value = idAcaocriterio;
		parent.document.forms[0].userAction.value = "excluirAcao";
		parent.document.forms[0].target = this.name = "excluirAcao";
		parent.document.forms[0].submit();
	}
}

function carregaListaAcao(){

	if(document.forms[0].elements["descricaoAcao"].value == 0){
		alert("O campo a��o � obrigat�rio!");
		document.forms[0].elements["descricaoAcao"].focus();
		return;
	}

	/*if(document.forms[0].elements["idEmcoCdEmpreco"].value == ''){
		alert("O campo empresa de cobran�a � obrigat�rio!");
		document.forms[0].elements["idEmcoCdEmpreco"].focus();
		return;
	}*/
	/*if(document.forms[0].elements["idReneCdRegnegociacao"].value == ''){
		alert("O campo regras de negocia��o � obrigat�rio!");
		document.forms[0].elements["idReneCdRegnegociacao"].focus();
		return;
	}*/

	
			
	var selIndexAcao = document.forms[0].descricaoAcao.selectedIndex;
	document.forms[0].elements["descAcao"].value = document.forms[0].descricaoAcao.options[selIndexAcao].text;

	var idAcao = document.forms[0].elements["descricaoAcao"].value;
	var accoInTipo = idAcao.substring(0, 1);
	
	idAcao  = idAcao.substring(1, document.forms[0].elements["descricaoAcao"].value.length);
	document.forms[0].elements["lblReguaDetalheAcao"].value = idAcao; 
	
	var selIndexEmpresa = document.forms[0].idEmcoCdEmpreco.selectedIndex;
	document.forms[0].elements["descEmpresa"].value = document.forms[0].idEmcoCdEmpreco.options[selIndexEmpresa].text;
	
	//var selIndexRegraNeg = document.forms[0].idReneCdRegnegociacao.selectedIndex;
	//document.forms[0].elements["descRegraNegociacao"].value = document.forms[0].idReneCdRegnegociacao.options[selIndexRegraNeg].value != '' ? document.forms[0].idReneCdRegnegociacao.options[selIndexRegraNeg].text : '';

	var selIndexGrupoRegraNeg = document.forms[0].idGrreCdGruporene.selectedIndex;
	document.forms[0].elements["descGrupoRegraNegociacao"].value = document.forms[0].idGrreCdGruporene.options[selIndexGrupoRegraNeg].value != '' ? document.forms[0].idGrreCdGruporene.options[selIndexGrupoRegraNeg].text : '';

	if(accoInTipo == 'C'){
		if(document.forms[0].elements["idGrreCdGruporene"].value == ''){
			alert("O campo grupo regras de negocia��o � obrigat�rio!");
			document.forms[0].elements["idGrreCdGruporene"].focus();
			return;
		}
	}
	
	if(accoInTipo == 'C'){
		if(document.forms[0].elements["idCampCdCampanha"].value=="" && document.forms[0].elements["idCampCdCampanha"].value==0){
			alert("Selecione uma campanha.");
			return false;
		}

		var dist = document.forms[0].elements["idCampCdCampanha"].options[document.forms[0].elements["idCampCdCampanha"].selectedIndex].getAttribute("dist");
		
		if(dist!="S" && ifrmCmbSubCampanhaEstrategia.document.forms[0].elements["idPublCdPublico"].value=="" && ifrmCmbSubCampanhaEstrategia.document.forms[0].elements["idPublCdPublico"].value==0){
			alert("Selecione uma sub-campanha");
			return false;
		} 
	}

	if(document.forms[0].elements["accrNrDias"].value == 0){
		alert("O campo dias �teis � obrigat�rio!");
		document.forms[0].elements["accrNrDias"].focus();
		return;
	}
	if(document.forms[0].elements["accrNrSequencia"].value == 0){
		alert("O campo sequ�ncia � obrigat�rio!");
		document.forms[0].elements["accrNrSequencia"].focus();
		return;
	}
	
	document.forms[0].elements["idPublCdPublico"].value = ifrmCmbSubCampanhaEstrategia.document.forms[0].elements["idPublCdPublico"].value;
	
	parent.document.forms[0].userAction.value = "carregaListaAcao";
	parent.document.forms[0].target = this.name = "carregaListaAcao";
	parent.document.forms[0].submit();
}

function disableCmb(){

	document.forms[0].elements["idCampCdCampanha"].disabled = true;
	ifrmCmbSubCampanhaEstrategia.document.forms[0].elements["idPublCdPublico"].disabled = true;
	
}

function cmbAtivo(idAcco_TpAccoInTipo){

	var accoInTipo = idAcco_TpAccoInTipo.substring(0, 1);
	
	if(accoInTipo == 'C'){
		document.forms[0].elements["idCampCdCampanha"].disabled = false;
		ifrmCmbSubCampanhaEstrategia.document.forms[0].elements["idPublCdPublico"].disabled = false;
	}else{
		document.forms[0].elements["idCampCdCampanha"].disabled = true;
		ifrmCmbSubCampanhaEstrategia.document.forms[0].elements["idPublCdPublico"].disabled = true;
		document.forms[0].elements["idCampCdCampanha"].value = "";
		ifrmCmbSubCampanhaEstrategia.document.forms[0].elements["idPublCdPublico"].value = "";
	}	
}

function cmbSubCampanha(obj){
	var idCampCdCampanha = obj.value;
	var dist = obj.options[obj.selectedIndex].getAttribute("dist");

	document.getElementById("lblCmbSubCampanha").style.visibility = (dist==null || dist=="S") ? "hidden" : "";
	document.getElementById("tdCmbSubCampanha").style.visibility = (dist==null || dist=="S") ? "hidden" : "";
	
	if(idCampCdCampanha>0){
		ifrmCmbSubCampanhaEstrategia.location.href = "estrategias.do?userAction=carregaSubCampanha&idCampCdCampanha=" + idCampCdCampanha; 
	}
}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="disableCmb();">
<html:form action="/estrategias" method="post">
<html:hidden property="userAction"/>
<input type="hidden" name="descAcao" value=""/>
<input type="hidden" name="descEmpresa" value=""/>
<input type="hidden" name="descRegraNegociacao" value=""/>
<input type="hidden" name="descGrupoRegraNegociacao" value=""/>
<input type="hidden" name="idAcaocriterio" value=""/>
<html:hidden property="idSelecionado"/>
<html:hidden property="idCriterio"/>
<html:hidden property="idPublCdPublico"/>
<html:hidden property="lblReguaDetalheAcao"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="estrategiasForm.inclusaoAcoes"/></td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="330">
          <tr>
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabel" align="right" height="28" width="26%">A&ccedil;&atilde;o 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
                </td>
                <td colspan="2" class="principalLabel"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="73%"> 
                      <% 
                      Vector vectorAcco = (Vector)request.getAttribute("cbCdtbAcaoAcaoByAtivo");
                      long idAcco = 0;
                      String strTexto = "";
                      String strAccoInTipo = "";
                      String tipoHtml = "";
                      
                      tipoHtml = "<select name='descricaoAcao' class='principalObjForm' onchange='cmbAtivo(this.value)'>";
                      tipoHtml = tipoHtml + "<option value='0'>-- Selecione uma op��o --</option>";
                      
                      if (vectorAcco!= null && vectorAcco.size()>0){
							for(int i = 0; i < vectorAcco.size(); i++){
								Vo voAcco = (Vo)vectorAcco.get(i);
								idAcco = Long.parseLong(voAcco.getFieldAsString("ID_ACCO_CD_ACAOCOB"));
								strTexto = voAcco.getFieldAsString("ACCO_DS_ACAOCOB");
								strAccoInTipo = voAcco.getFieldAsString("ID_TPAC_CD_TPACAOCOB"); 
								
								tipoHtml = tipoHtml + "<option value='"+strAccoInTipo+idAcco+"'>"+strTexto+"</option>";
							}
                      }
                      tipoHtml = tipoHtml + "</select>";
                      %>
                      
                      <%=tipoHtml%>
                      </td>
                      <td width="27%" class="principalLabel"><!-- <img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand"> --> 
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td class="principalLabel" align="right" height="28" width="26%">Empresa de Cobran&ccedil;a <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                <td colspan="2" class="principalLabel">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="73%"> 
                        <html:select property="idEmcoCdEmpreco"  styleClass="principalObjForm">
                       		<html:option value="">-- Selecione uma op��o --</html:option>
                       		<logic:notEmpty name="cbCdtbEmprecobEmco">
                       			<bean:define name="cbCdtbEmprecobEmco" id="empresaVO" />
                       			<html:options collection="empresaVO"  property="field(ID_EMCO_CD_EMPRECOB)" labelProperty="field(EMCO_DS_EMPRESA)"/>
                       		</logic:notEmpty>
                       </html:select>	
                      </td>
                      <td width="27%" class="principalLabel">&nbsp;</td>
                    </tr>
                  </table>
                </td>
                <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
              </tr>
             
              <tr>
                <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="prompt.GrupoRegrasDeNegociacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
                <td colspan="2" class="principalLabel">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="73%"> 
                        <html:select property="idGrreCdGruporene" styleClass="principalObjForm">
							<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option>
							<logic:present name="cbCdtbGruporeneGrre">
								<html:options collection="cbCdtbGruporeneGrre" property="field(id_grre_cd_gruporene)" labelProperty="field(grre_ds_gruporene)"/>
							</logic:present>
				    	</html:select>
                      </td>
                      <td width="27%" class="principalLabel">&nbsp;</td>
                    </tr>
                  </table>
                </td>
                <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td colspan="5" class="principalLabel"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="prompt.campanha" />
	                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
	                </td>  
                    <td width="25%"> 
						<html:select property="idCampCdCampanha" styleClass="principalObjForm" onchange="cmbSubCampanha(this)">
							<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option>
							
							<logic:present name="csCdtbCampanhaCamp">
							<logic:iterate id="camp" name="csCdtbCampanhaCamp">
								<option value="<bean:write name="camp" property="field(id_camp_cd_campanha)" />" dist="<bean:write name="camp" property="field(camp_in_distribuicao)" />"><bean:write name="camp" property="field(camp_ds_campanha)" /></option>
							</logic:iterate>
							</logic:present>
				    	</html:select>
                    </td>
					<td id="lblCmbSubCampanha" style="visibility: hidden; " class="principalLabel" align="right" height="28" width="15%"><bean:message key="prompt.subcampanha" /> 
                  		<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;
                	</td>
					<td id="tdCmbSubCampanha" style="visibility: hidden; " width="25%">
						<iframe id="ifrmCmbSubCampanhaEstrategia" name="ifrmCmbSubCampanhaEstrategia" src="estrategias.do?userAction=carregaSubCampanha" width="98%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
					</td>
                    <td class="principalLabel" align="right" height="28" width="9%">&nbsp;</td>
                  </table>
                </td>
              </tr>
              <tr> 
                <td colspan="6" class="principalLabel"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <td class="principalLabel" align="right" height="28" width="26%">Dias &uacute;teis
	                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
	                </td>  
                      <td width="12%"> 
                      	<html:text property="accrNrDias" name="estrategiasForm" maxlength="8" styleClass="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);"/>
                      </td>
					<td class="principalLabel" align="right" height="28" width="10%">Sequ&ecirc;ncia 
                  		<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
                	</td>
                	<td width="12%"> 
                        <html:text property="accrNrSequencia" name="estrategiasForm" maxlength="8" styleClass="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);"/>
                    </td>
                    <td width="4%" class="principalLabel">&nbsp;</td>
                    <td width="61%" class="principalLabel" align="left"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="carregaListaAcao()"> 
                    </td>
                  </table>
                </td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td align="right" class="EspacoPequeno" width="4%">&nbsp;</td>
                <td class="EspacoPequeno" align="right" width="89%">&nbsp;</td>
                <td width="7%" class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td width="4%" align="right" class="principalLabel" height="25">&nbsp;</td>
                <td align="right" class="principalLabel" height="25" width="89%"> 
                <div style="height:150px; width:800px; overflow: auto;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLstCab" width="3%">&nbsp;</td>
                      <td width="20%" class="principalLstCab">A&ccedil;&atilde;o</td>
                      <td width="20%" class="principalLstCab">Empresa de Cobran�a</td>
                      <!-- <td width="20%" class="principalLstCab">Regra de Negocia��o</td>-->
                      <td width="20%" class="principalLstCab">Grupo Regras de Negocia��o</td>
                      <td width="10%" class="principalLstCab" align="center">Dias &uacute;teis</td>
                      <td width="10%" class="principalLstCab" align="center">Sequ&ecirc;ncia</td>
                      <td width="17%" class="principalLstCab" align="center">Dt. Execu��o</td>
                   </tr>
                  <logic:notEmpty name="cbCdtbAcaoAcao">
                  	<logic:iterate name="cbCdtbAcaoAcao" id="cbCdtbAcaoAcao">
                  		<script>
                  			if('<bean:write name="cbCdtbAcaoAcao" property="field(ID_CRIT_CD_CRITERIO)"/>' == document.forms[0].idCriterio.value && 
                  			   '<bean:write name="cbCdtbAcaoAcao" property="field(ID_CENA_CD_CENARIO)"/>' == document.forms[0].idSelecionado.value) {
                  			   	var descAcao = acronymLst('<bean:write name = "cbCdtbAcaoAcao" property="field(ACCO_DS_ACAOCOB)"/>', 20);
                  				document.write('<tr>');
                  				document.write("<td class=principalLstPar width=3% align=center onclick=excluirAcao('<bean:write name="cbCdtbAcaoAcao" property="field(ID_ACCR_CD_ACAOCRITERIO)"/>');><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand alt=Excluir></td>");
	                  			document.write('<td class=principalLstPar width=20% >&nbsp;'+ descAcao + '&nbsp;</td>');
	                  			document.write('<td class=principalLstPar width=20% >&nbsp;<bean:write name = "cbCdtbAcaoAcao" property="field(EMCO_DS_EMPRESA)"/>&nbsp;</td>');
	                  			//document.write('<td class=principalLstPar width=20% >&nbsp;<bean:write name = "cbCdtbAcaoAcao" property="field(RENE_DS_REGNEGOCIACAO)"/>&nbsp;</td>');
	                  			document.write('<td class=principalLstPar width=20% >&nbsp;<bean:write name = "cbCdtbAcaoAcao" property="field(GRRE_DS_GRUPORENE)"/>&nbsp;</td>');
	                  			document.write('<td class=principalLstPar width=10% align=center><bean:write name = "cbCdtbAcaoAcao" property="field(ACCR_NR_DIAS)"/>&nbsp;</td>');
	                  			document.write('<td class=principalLstPar width=10% align=center><bean:write name = "cbCdtbAcaoAcao" property="field(ACCR_NR_SEQUENCIA)"/></td>');
	                  			document.write('<td class=principalLstPar width=17% align=center>&nbsp;&nbsp;<bean:write name = "cbCdtbAcaoAcao" property="field(ACCR_DH_EXECUCAO)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/></td>');
	                  			document.write('</tr>');
                  			}
                  		</script>
                  	</logic:iterate>
                  </logic:notEmpty>
                  </table>
                  </div>
                </td>
                <td width="7%">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td>&nbsp; </td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>