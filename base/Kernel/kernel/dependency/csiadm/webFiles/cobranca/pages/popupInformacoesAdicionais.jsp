<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/popupInformacoesAdicionais.jsp"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/date-picker.js"></SCRIPT>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/cadastroTela" method="post">
<html:hidden property="userAction"/>

	<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="140"><bean:message key="popupInformacoesAdicionais.title.informaçõesAdicionais"/></td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
        
          <td height="4" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
       
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr>
                <td valign="top" height="200"> 
                  <div id="informacaoAdicional" style="position:absolute; width:100%; height:125px; z-index:1;"> 
                   
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                      <tr>
						<td valign="top"> 
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
								<tr> 
									<td>&nbsp;</td>
								</tr>
							</table>
								<logic:notEmpty name="cbCdtbTelaadiconalTeadPrincipal"> 
									<logic:iterate name="cbCdtbTelaadiconalTeadPrincipal" id="voGrupo" >
										<table width="50%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
											<tr> 
												<td width="1007" colspan="2"> 
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
															<td class="principalPstQuadro" height="17" width="166"> 
																<bean:write name="voGrupo" property="field(CAAD_DS_GRUPO)"/>
															</td>
															<td class="principalQuadroPstVazia" height="17"> 
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr> 
																		<td width="400"> 
																			<div align="center"></div>
																		</td>
																	</tr>
																</table>
															</td>
															<td height="17" width="4">
																 
																<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
															
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr> 
												<td class="principalBgrQuadro" valign="top"> 
													<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
														<tr> 
															<td valign="top"> 
																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
																	<tr> 
																		<td>&nbsp;</td>
																	</tr>
																</table>
																<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
																	<tr> 
																		<td align="right" class="EspacoPequeno" width="28%">&nbsp;</td>
																		<td class="EspacoPequeno" width="72%">&nbsp;</td>
																	</tr>
																	<logic:notEmpty name="voGrupo" property="field(campos)"> 
																		<logic:iterate  name="voGrupo" property="field(campos)" id="campo">
																			<tr>
																				<td align="right" class="principalLabel" width="28%" height="15">
																					<bean:write name="campo" property="field(CAAD_DS_CAMPO)"/>
																						<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
																				</td> 
																				<td class="principalLabelValorFixo" width="72%"></td>
																			</tr>
																		</logic:iterate>
																	</logic:notEmpty>                          
																	<tr> 
																		<td align="right" class="EspacoPequeno" width="28%">&nbsp;</td>
																		<td class="EspacoPequeno" width="72%">&nbsp;</td>
																	</tr>
																</table>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
																	<tr> 
																		<td>&nbsp;</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
												<td width="4" height="1">
													<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
												</td>
											</tr>
											<tr> 
												<td width="100%">
													<img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4">
												</td>
												<td width="4">
													<img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4">
												</td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
											<tr> 
												<td>&nbsp;</td>
											</tr>
										</table>
									</logic:iterate>
								</logic:notEmpty>
								
							</td>
						</tr>
                    
                    </table>
				
					<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr align="center"> 
                        <td align="right" class="EspacoPequeno">&nbsp;</td>
                      </tr>                      
						<tr align="center"> 
							<td align="right" class="EspacoPequeno">&nbsp;</td>
						</tr>                      
                    </table>					
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>

      </td>
   
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
     
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>
