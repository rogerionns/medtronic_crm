<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/cadastroMetas.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<script type="text/javascript">
	var countRegistroLstMetas = new Number(0);
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="parent.parent.document.all.item('LayerAguarde').style.visibility = 'hidden';">
<html:form action="/cadastroMetas" styleId="cadastroMetasForm" method="post">
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td align="right" class="EspacoPequeno" width="4%">&nbsp;</td>
    <td class="EspacoPequeno" align="right" width="89%">&nbsp;</td>
    <td width="7%" class="EspacoPequeno">&nbsp;</td>
  </tr>
  <tr> 
    <td width="4%" align="right" class="principalLabel" height="25">&nbsp;</td>
    <td align="right" class="principalLabel" height="25" width="89%">
    <div style="height:91px; overflow: auto;"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLstCab" width="8%">&nbsp; </td>
          <td width="25%" class="principalLstCab">&nbsp;<bean:message key="cadastroMetasLista.campanha"/></td>
          <td width="25%" class="principalLstCab"><bean:message key="cadastroMetasLista.subCampanha"/></td>
          <td width="22%" class="principalLstCab"><bean:message key="cadastroMetasLista.resultado"/></td>
          <td width="20%" class="principalLstCab" align="center">&nbsp;<bean:message key="cadastroMetasLista.qtd"/>&nbsp;</td>
        </tr>
        <logic:notEmpty name="cbCdtbMetaresultadoMere">
          <logic:iterate name="cbCdtbMetaresultadoMere" id="cbCdtbMetaresultadoMere" indexId="indice">
        	<logic:notEqual name="cbCdtbMetaresultadoMere" property="field(excluido)" value="true" >
        		<tr class="geralCursoHand">
        			<td width="8%" class="principalLstImpar"  onclick="excluirLista('<bean:write name="cbCdtbMetaresultadoMere" property="field(ID_MERE_CD_METARESULTADO)"/>');"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" width="14" height="14" class="geralCursoHand" alt="Excluir"></td>
        			<td width="25%" class="principalLstImpar">&nbsp;<bean:write name="cbCdtbMetaresultadoMere" property="field(CAMP_DS_CAMPANHA)"/></td>      			
        			<td width="10%" class="principalLstImpar">&nbsp;<bean:write name="cbCdtbMetaresultadoMere" property="field(PUBL_DS_PUBLICO)"/></td>
        			<td width="22%" class="principalLstImpar">&nbsp;<bean:write name="cbCdtbMetaresultadoMere" property="field(RESU_DS_RESULTADO)"/></td>
					<td width="35%" class="principalLstImpar" align="center">&nbsp;<bean:write name="cbCdtbMetaresultadoMere" property="field(MERE_NR_QTDE)"/></td>        			 																
        		</tr>
        	  </logic:notEqual>
        	  <script>
        	  	countRegistroLstMetas++;
        	  </script>
        	</logic:iterate>
        </logic:notEmpty>
      </table>
      </div>
    </td>
    <td width="7%">&nbsp;</td>
  </tr>
</table>
</html:form>
</body>
<script>
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_META_EXCLUSAO_CHAVE%>')){
		for(i = 0; i < countRegistroLstMetas; i++) {
			eval('setPermissaoImageDisable(false, document.cadastroMetasForm.btExcluir'+i+');');
		}		
	}
</script>
</html>