<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/cobranca/js/estrategias.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>

<script type="text/javascript">
	var countCriteriosSimulados = new Number(0);
	var mensagemCamposObrigatorios = '';

	function visualizarCriterio(idCriterio) {
		showModalDialog('estrategias.do?userAction=popupCriterioSimular&idCriterio=' + idCriterio, 0, 'help:no;scroll:no;Status:NO;dialogWidth:450px;dialogHeight:190px,dialogTop:100px,dialogLeft:200px');
	}
	
	function verificaCamposObrigatorios() {
		mensagemCamposObrigatorios = '';
		
		//Verifica se existe algum campo obrigatorio que nao foi passado na query da visao
		if(countCriteriosSimulados > 1) {
			for(i = 0; i < countCriteriosSimulados; i++) {
				if(document.forms[0].mensagemCamposObrigatorios[i].value != '') {
					mensagemCamposObrigatorios += document.forms[0].mensagemCamposObrigatorios[i].value;
				}
			}
		} else if(countCriteriosSimulados == 1) {
			if(document.forms[0].mensagemCamposObrigatorios.value != '') {
				mensagemCamposObrigatorios += document.forms[0].mensagemCamposObrigatorios.value;
			}
		}
		
		if(mensagemCamposObrigatorios != '') {
			alert(mensagemCamposObrigatorios);
			return false;
		}
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="verificaCamposObrigatorios();limpaFiltro();parent.parent.document.all.item('LayerAguarde').style.visibility = 'hidden';">
<html:form action="/estrategias" method="post" styleId="estrategiasForm">
<html:hidden property="limparCriterios"/>
	<div style="height:285px; overflow:auto;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="4%" class="principalLstCab">&nbsp;</td>
				<td width="42%" class="principalLstCab">&nbsp;&nbsp;<bean:message key="estrategiaForm.criterios"/></td>
				<td width="25%" class="principalLstCab"><bean:message key="estrategiaForm.registrosAlocados"/></td>
				<td width="25%" class="principalLstCab" align="center"><bean:message key="estrategiaForm.valorCobranca"/></td>
				<!--<td width="18%" class="principalLstCab" align="center"><bean:message key="estrategiaForm.rec.Historica"/></td>
				<td width="22%" class="principalLstCab" align="center"><bean:message key="estrategiaForm.rec.projecaoPecuperacao"/></td>-->
				<td width="4%" class="principalLstCab" align="center">&nbsp;</td>
			</tr>
			<logic:equal name="estrategiasForm" property="limparCriterios" value="false">
				<logic:notEmpty name="criterioSimulado">
					<logic:iterate name="criterioSimulado" id="criterioSimulado">
						<tr class="geralCursoHand">
							<td  class="principalLstPar">&nbsp;<img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18" height="18" class="geralCursoHand" alt="<bean:message key="prompt.excluir"/>" onclick="excluirCriterioSimulado('<bean:write name="criterioSimulado" property="field(id)"/>')" /></td>
							<td  class="principalLstPar">&nbsp;<script>acronym('<bean:write name="criterioSimulado" property="field(nomeCriterio)"/>', 45);</script></td>
							<td  class="principalLstPar" >&nbsp;<bean:write name="criterioSimulado" property="field(registrosAlocados)"/></td>
							<td  class="principalLstPar"  align="center">&nbsp;R$ <bean:write name="criterioSimulado" property="field(valorCobranca)" format="##,###,##0.00"/></td>
							<!--<td  class="principalLstPar"  align="center">&nbsp;<bean:write name="criterioSimulado" property="field(recuperacaoHistorica)" format="#0.00"/></td>
							<td  class="principalLstPar"  align="center">&nbsp;<bean:write name="criterioSimulado" property="field(projecaoRecuperacao)" format="##,###,##0.00"/></td>-->
							<td  class="principalLstPar" align="center"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" alt="Crit�rio de Sele��o" onClick="visualizarCriterio('<bean:write name="criterioSimulado" property="field(id)"/>')" class="geralCursoHand"></td>
							<input type="hidden" name="mensagemCamposObrigatorios" value="<bean:write name="criterioSimulado" property="field(possuiTodosCamposObrigatorios)"/>" />
							
							<script>
								countCriteriosSimulados++;
							</script>			                                    																				
						</tr>
					</logic:iterate>
				</logic:notEmpty>
			</logic:equal>
		</table>
	</div>
	
	<logic:equal name="estrategiasForm" property="limparCriterios" value="false">
		<logic:present name="totaisCriterio">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td width="4%"  class="principalLabelValorFixo">&nbsp;</td>
					<td width="42%" class="principalLabelValorFixo" align="right"><bean:message key="estrategiaForm.total"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td width="25%" class="principalLabelValorFixo">&nbsp;<bean:write name="totaisCriterio" property="field(totalRegistro)"/></td>
					<td width="25%" class="principalLabelValorFixo" align="center">R$ <bean:write name="totaisCriterio" property="field(totalValor)" format="##,###,##0.00"/></td>
					<!--<td width="18%" class="principalLabelValorFixo" align="center"><bean:write name="totaisCriterio" property="field(percentual)" format="#0.00"/></td>
					<td width="22%" class="principalLabelValorFixo" align="center"><bean:write name="totaisCriterio" property="field(totalRecuperado)" format="##,###,##0.00"/></td>-->
					<td width="4%"  class="principalLabelValorFixo" align="center">&nbsp;</td>
				</tr>
			</table>
		</logic:present>
	</logic:equal>
</html:form>
</body>                                              
</html>