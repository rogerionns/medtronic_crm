<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 

response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


String fileIncludeIdioma="../../../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>
<% 
String fileInclude="../../../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/cobranca/css/global.css" type="text/css">
<link rel="stylesheet" href="webFiles/cobranca/pages/css/global.css" type="text/css">
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/acoesCobranca.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/TratarDados.js"></SCRIPT>

<script type="text/javascript">
	function carregaCmbSubCampanha() {
		ifrmCmbSubCampanha.location.href = 'acaoCobranca.do?userAction=carregaCmbSubCampanha&idCampCdCampanha=' + document.forms[0].idCampCdCampanha.value;
	}
	
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');AtivarPasta('<bean:write name="acaoCobrancaForm" property="flagAba"/>');verificaCampos('<bean:write name="acaoCobrancaForm" property="flagAba"/>','<bean:write name="acaoCobrancaForm" property="flagHabilitaCampos"/>');inicio();">
<html:form action="/acaoCobranca" method="post">
<html:hidden property="userAction"/>

<input type="hidden" name="idAcao" value=""/>
<input type="hidden" name="inTipo" value=""/>
<input type="hidden" name="inEmpresa" value=""/>
<input type="hidden" name="descricao" value=""/>
<input type="hidden" name="custoDescricao" value=""/>
<input type="hidden" name="inPesquisarRealizar" value=""/>
<input type="hidden" name="carta" value=""/>
<input type="hidden" name="anexar" value=""/>
<input type="hidden" name="idManifesto" value=""/> 
<input type="hidden" name="accoDhInativo" value=""/>
<input type="hidden" name="check" value=""/>

<html:hidden property="cartaEnviar"/>
<html:hidden property="idGrdoCdCarta"/>

<html:hidden property="idDocuCdSms"/> 
<html:hidden property="idGrdoCdSms"/>
<html:hidden property="idIpchCdImplementchat"/>
<html:hidden property="idPublCdPublico"/> 

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="acaoCobranca.title.descricao"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="550"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado" name="tdProcurar" id="tdProcurar" onClick="AtivarPasta('pesquisar');"><bean:message key="acaoCobranca.janela.procurar"/></td>
                <td class="principalPstQuadroLinkNormalMaior" name="tdEstrategias" id="tdEstrategias" onClick="AtivarPasta('editar');"><bean:message key="acaoCobranca.janela.acoesCobranca"/></td>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr> 
                <td height="500" valign="top"> 
                  <div id="Procurar" style="width:99%; height:450px;display: block;"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="principalLabel"><bean:message key="acaoCobrancaForm.descricao"/></td>
                      </tr>
                      <tr> 
                   
                        <td width="56%">
                          <html:text property="descricaoAcao" onkeydown="if(event.keyCode==13) pesquisar();" styleClass="principalObjForm"/>
                        </td>
                      
                        <td width="44%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="pesquisar();" alt="Pesquisar"></td>
                      </tr>
                      <tr> 
                        <td width="56%">&nbsp;</td>
                        <td width="44%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="2"> 
                        <div style="height: 300px; overflow: auto;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLstCab" width="4%">&nbsp;</td>
                              <td class="principalLstCab" width="4%">&nbsp;</td>
                              <td class="principalLstCab" width="9%">&nbsp;<bean:message key="acaoCobranca.lista.codigo"/></td>
                              <td class="principalLstCab" width="62%"><bean:message key="acaoCobranca.lista.acao"/></td>
                              <td class="principalLstCab" width="25%"><bean:message key="acaoCobranca.lista.inativo"/></td>
                            </tr>
                            <logic:notEmpty name="cbCdtbAcaoAcao" >
                            	<logic:iterate name="cbCdtbAcaoAcao" id="acoes" indexId="indice">
		                            <tr class="geralCursoHand"> 
		                              <td class="principalLstPar" width="4%" align="center"><img src="webFiles/images/botoes/lixeira.gif" name="btExcluir<%=indice%>" id="btExcluir<%=indice%>" onclick="excluir('<bean:write name="acoes" property="field(ID_ACCO_CD_ACAOCOB)"/>');" width="14" height="14" class="geralCursoHand" alt="Excluir"/></td>
		                    	     
		                    	      <td class="principalLstPar" width="4%" align="center" onclick="abrirModalIdioma('CS_ASTB_IDIOMACAOCOB_IDAC.xml','<bean:write name="acoes" property="field(ID_ACCO_CD_ACAOCOB)"/>');"><img src="webFiles/images/botoes/GloboAuOn.gif" width="18" height="18" class="geralCursoHand"/></td>
		                    	    																
		                    	      <td class="principalLstPar" width="9%" onclick="alterar('<bean:write name="acoes" property="field(ID_ACCO_CD_ACAOCOB)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_IN_TIPO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_EMCO_CD_EMPRECOB)"/>',
                            			 													   '<bean:write name="acoes" property="field(ACCO_DS_ACAOCOB)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_VL_CUSTO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_PESQ_CD_PESQUISA)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_DOCU_CD_DOCUMENTO)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_DS_ARQUIVO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_MAAU_CD_MANIFESTACAOAUTOMA)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_DH_INATIVO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_CAMP_CD_CAMPANHA)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_PUBL_CD_PUBLICO)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_TX_MENSAGEM)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_IMCO_CD_IMPORTACAOCOBR)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_NR_INICIOVIGENCIA)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_NR_FIMVIGENCIA)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_NR_DIASLIMITEEXPIRACAO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_DOCU_CD_SMS)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_GRDO_CD_GRUPODOCUMENTO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_IPCH_CD_IMPLEMENTCHAT)"/>',
		                              														   '<bean:write name="acoes" property="field(GRUPODOCUMENTO_CARTA)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_TPAC_CD_TPACAOCOB)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_IN_BOLETAGEM)"/>');">&nbsp;&nbsp;<bean:write name="acoes" property="field(ID_ACCO_CD_ACAOCOB)"/></td>
		                              														   
		                              <td class="principalLstPar" width="62%" onclick="alterar('<bean:write name="acoes" property="field(ID_ACCO_CD_ACAOCOB)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_IN_TIPO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_EMCO_CD_EMPRECOB)"/>',
                            			 													   '<bean:write name="acoes" property="field(ACCO_DS_ACAOCOB)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_VL_CUSTO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_PESQ_CD_PESQUISA)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_DOCU_CD_DOCUMENTO)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_DS_ARQUIVO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_MAAU_CD_MANIFESTACAOAUTOMA)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_DH_INATIVO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_CAMP_CD_CAMPANHA)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_PUBL_CD_PUBLICO)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_TX_MENSAGEM)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_IMCO_CD_IMPORTACAOCOBR)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_NR_INICIOVIGENCIA)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_NR_FIMVIGENCIA)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_NR_DIASLIMITEEXPIRACAO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_DOCU_CD_SMS)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_GRDO_CD_GRUPODOCUMENTO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_IPCH_CD_IMPLEMENTCHAT)"/>',
		                              														   '<bean:write name="acoes" property="field(GRUPODOCUMENTO_CARTA)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_TPAC_CD_TPACAOCOB)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_IN_BOLETAGEM)"/>');">&nbsp;<bean:write name="acoes" property="field(ACCO_DS_ACAOCOB)"/></td>
		                             
		                              <td class="principalLstPar" width="25%" onclick="alterar('<bean:write name="acoes" property="field(ID_ACCO_CD_ACAOCOB)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_IN_TIPO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_EMCO_CD_EMPRECOB)"/>',
                            			 													   '<bean:write name="acoes" property="field(ACCO_DS_ACAOCOB)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_VL_CUSTO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_PESQ_CD_PESQUISA)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_DOCU_CD_DOCUMENTO)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_DS_ARQUIVO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_MAAU_CD_MANIFESTACAOAUTOMA)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_DH_INATIVO)" />',
		                              														   '<bean:write name="acoes" property="field(ID_CAMP_CD_CAMPANHA)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_PUBL_CD_PUBLICO)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_TX_MENSAGEM)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_IMCO_CD_IMPORTACAOCOBR)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_NR_INICIOVIGENCIA)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_NR_FIMVIGENCIA)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_NR_DIASLIMITEEXPIRACAO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_DOCU_CD_SMS)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_GRDO_CD_GRUPODOCUMENTO)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_IPCH_CD_IMPLEMENTCHAT)"/>',
		                              														   '<bean:write name="acoes" property="field(GRUPODOCUMENTO_CARTA)"/>',
		                              														   '<bean:write name="acoes" property="field(ID_TPAC_CD_TPACAOCOB)"/>',
		                              														   '<bean:write name="acoes" property="field(ACCO_IN_BOLETAGEM)"/>');">&nbsp;<bean:write name="acoes" property="field(ACCO_DH_INATIVO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>		                              
		                            </tr>
		                            <script>
		                            	countRegistro++;
		                            </script>
                            	</logic:iterate>
                            </logic:notEmpty> 
                            <logic:empty name="cbCdtbAcaoAcao">
								<tr> 
									<td align="center" colspan="7" > 
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
											<tr id="nenhumRegistro" ><td height="260" width="800" align="center"  class="principalLstPar"><br><b><bean:message key="lista.nenhum.registro.encontrado"/> </b></td></tr>
										</table>
									&nbsp;</td>
								</tr>
							</logic:empty>
                          </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
                  <div id="Estrategias" style="width:99%; height:450px;display: none;"> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%">&nbsp;</td>
                        <td width="8%" class="principalLabel">&nbsp;</td>
                        <td class="principalLabel" colspan="3">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="acaoCobrancaForm.codigo"/>
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                       
                        <td width="8%" class="principalLabel">
                          <html:text property="idAcaoCdAcao" name="acaoCobrancaForm" styleClass="principalObjForm" disabled="true"/>                          
                        </td>
                    
                        <td class="principalLabel" colspan="3">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"> 
                          <bean:message key="acaoCobrancaForm.tipoAcao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                              	<html:select property="acaoInTipo" name="acaoCobrancaForm" styleClass="principalObjForm" onchange="verificaTipoAcao()">
                              		<html:option value="">-- Selecione uma Op��o --</html:option>
	                            		<logic:notEmpty name="tipoAcao">
	                            			<bean:define name="tipoAcao" id="tipoAcaoVO" />
	                            			<html:options collection="tipoAcaoVO" property="field(id_tpac_cd_tpacaocob)" labelProperty="field(tpac_ds_tpacaocob)"/>
	                            		</logic:notEmpty>
                              	</html:select>
                              </td>
                              <td width="27%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="acaoCobrancaForm.empresa"/>  
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                              
                                 <html:select property="empresa" name="acaoCobrancaForm" styleClass="principalObjForm">
	                            		<html:option value="0">-- Selecione uma Op��o --</html:option>
	                            		<logic:notEmpty name="cbCdtbEmprecobEmco">
	                            			<bean:define name="cbCdtbEmprecobEmco" id="empresaVO" />
	                            			<html:options collection="empresaVO"  property="field(ID_EMCO_CD_EMPRECOB)" labelProperty="field(EMCO_DS_EMPRESA)"/>
	                            		</logic:notEmpty>
	                            </html:select>
                               
                              </td>
                              <td width="27%" class="principalLabel" align="center">&nbsp; 
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>                 
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="acaoCobrancaForm.descricao"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%">
                                <html:text property="acaoDsAcao" name="acaoCobrancaForm" maxlength="60" styleClass="principalObjForm"/>                                                       
                              </td>
                              <td width="27%" class="principalLabel" align="center">&nbsp; 
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="acaoCobrancaForm.implementacao"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%">
                                <html:text property="tpacDsClasseimplementacao" name="acaoCobrancaForm" maxlength="60" styleClass="principalObjForm"/>                                                       
                              </td>
                              <td width="27%" class="principalLabel" align="center">&nbsp; 
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="prompt.exportacaoCobranca"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%">
                                <html:select property="idImcoCdImportacaocobr" styleClass="principalObjForm"> 
							  		<html:option value=""><bean:message key="prompt.selecione_uma_opcao"/></html:option>
							  		<logic:present name="cbCdtbCdImportacaocobrImcoVector">
							  			<html:options collection="cbCdtbCdImportacaocobrImcoVector" property="field(id_imco_cd_importacaocobr)" labelProperty="field(imco_ds_importacaocobranca)"/>
							  		</logic:present> 
							  	</html:select>                                                       
                              </td>
                              <td width="27%" class="principalLabel" align="center">&nbsp; 
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>                      
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="acaoCobrancaForm.custoAcao"/>
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="12%"> 
                                <html:text property="acaoDsCusto" name="acaoCobrancaForm" styleClass="principalObjForm" maxlength="60" onkeypress="return(formataDecimal(this,',',event,9))"/>                                
                              </td>
                              <td width="88%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="prompt.InicioVigencia"/>
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="12%"> 
                                <html:text property="accoNrIniciovigencia" name="acaoCobrancaForm" styleClass="principalObjForm" maxlength="3" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>                                
                              </td>
                              <td width="88%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr> 
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="prompt.FimVigencia"/>
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="12%"> 
                                <html:text property="accoNrFimvigencia" name="acaoCobrancaForm" styleClass="principalObjForm" maxlength="3" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>                                
                              </td>
                              <td width="88%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>   
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="prompt.DiasLimiteDeExpiracao"/>
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="12%"> 
                                <html:text property="accoNrDiaslimiteexpiracao" name="acaoCobrancaForm" styleClass="principalObjForm" maxlength="8" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>                                
                              </td>
                              <td width="88%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="prompt.Boletagem"/>
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel">
                        	<html:checkbox property="accoInBoletagem" name="acaoCobrancaForm" value="S" />                                
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      
                      <!-- <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"> 
                          <bean:message key="acaoCobrancaForm.pesquisaRealizar"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                              	<html:select property="pesquisarRealizar" name="acaoCobrancaForm" styleClass="principalObjForm"  >
                                	<html:option value="">-- Selecione uma op&ccedil;&atilde;o--</html:option>                                	                                
                               		<logic:notEmpty name="csCdtbPesquisaPesq">
                            			<bean:define name="csCdtbPesquisaPesq" id="pesquisaVO" />
                            			<html:options collection="pesquisaVO"  property="idPesqCdPesquisa" labelProperty="pesqDsPesquisa"/>
                            		</logic:notEmpty>                               
                                </html:select>
                              </td>
                              <td width="27%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" align="right" height="28" width="26%"><bean:message key="acaoCobrancaForm.arquivoAnexar"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="2" class="principalLabel">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="73%"> 
                              	<html:file property="arquivoAnexar" maxlength="100" name="acaoCobrancaForm" styleClass="principalObjForm"/>
                         	  </td>
                              <td width="27%">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>-->
                      <tr>
                        <td class="principalLabel" align="right" height="28" width="26%">&nbsp;<!--<bean:message key="acaoCobrancaForm.idManifestacaoAutomatica"/> 
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> -->
                        </td>
                        <td colspan="2" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="12%"> 
                               	&nbsp;<!--<html:text property="idManifestacaoAutomatica" name="acaoCobrancaForm" styleClass="principalObjForm" maxlength="60" onkeypress="return validaCampoNumerico(this)" onblur="validaCampoNumerico(this); return false;"/>-->
                              </td>
                              <td width="60%" class="principalLabel" align="right"> 
                               	<logic:equal value="" name="acaoCobrancaForm" property="accoDhInativo">
									<input type="checkbox" name="inativo" value="ativo"/>                              	                              	
								</logic:equal>
								<logic:notEqual value="" name="acaoCobrancaForm" property="accoDhInativo">                                
									<input type="checkbox" name="inativo" value="inativo" checked="checked"/>                              	
								</logic:notEqual> 
                                <bean:message key="acaoCobrancaForm.inativo"/>
                              </td>
                              <td width="28%"  align="center"> 
                                &nbsp;
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
                      </tr> 
                      <tr> 
                        <td colspan="7" class="espacoPqn"> 
                        	&nbsp;
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="7" align="center"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100">
						     <tr>
						       <td valign="top">
									<!-- INICIO ABAS -->
									<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
									  <tr> 
									    <td class="principalPstQuadroLinkSelecionado" name="tdCampanha" id="tdCampanha" onclick="AtivarPasta('CAMPANHA')"><bean:message key="prompt.campanha"/></td>
									    <td class="principalPstQuadroLinkNormal" 	  name="tdCorrespondencia" id="tdCorrespondencia" onclick="AtivarPasta('CORRESPONDENCIA')"><bean:message key="prompt.Carta"/></td>
									    <td class="principalPstQuadroLinkNormal" 	  name="tdSMS" id="tdSMS" onclick="AtivarPasta('SMS')"><bean:message key="prompt.SMS"/></td>
									    <td >&nbsp;</td>
									    <td class="principalLabel">&nbsp;</td>
									  </tr>
									</table>
									<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro" height="88">
									  <tr>
									    <td valign="top">					      
									      <div id="campanha" style="width:100%;display: none"> 
									        <table width="100%" border="0" cellspacing="0" cellpadding="0">
									          <tr> 
						                        <td class="principalLabel" align="right" height="28" width="18%"><bean:message key="prompt.campanha"/>  
						                          	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
						                        </td>
						                        <td colspan="2" class="principalLabel">
						                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
						                            <tr> 
						                              <td> 
						                              	<html:select property="idCampCdCampanha" styleClass="principalObjForm" onchange="carregaCmbSubCampanha()"  style="width:345px"> 
													  		<html:option value=""><bean:message key="prompt.selecione_uma_opcao"/></html:option>
													  		<logic:present name="csCdtbCampanhaCampVector">
													  			<html:options collection="csCdtbCampanhaCampVector" property="idCampCdCampanha" labelProperty="campDsCampanha"/>
													  		</logic:present> 
													  	</html:select>
						                              </td>
						                              <td>&nbsp;</td>
						                            </tr>
						                          </table>
						                        </td>
						                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
						                      </tr>
						                      <tr> 
						                        <td class="principalLabel" align="right" height="28" width="100%"><bean:message key="prompt.subcampanha"/>  
						                          	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
						                        </td>
						                        <td colspan="2" class="principalLabel">
						                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
						                            <tr> 
						                              <td> 
						                                <iframe id="ifrmCmbSubCampanha" name="ifrmCmbSubCampanha" src="acaoCobranca.do?userAction=carregaCmbSubCampanha" width="100%" height="25px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
						                              </td>
						                            </tr>
						                          </table>
						                        </td>
						                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
						                      </tr>
									        </table>
								          </div>
									      <div id="correspondencia" style="width:100%;display: none"> 
									        <table width="100%" border="0" cellspacing="0" cellpadding="0">
									          <tr> 
						                        <td class="principalLabel">
						                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
							                          <tr> 
								                        <td class="principalLabel" align="right" height="28" width="30%"><plusoft:message key="prompt.grupoDocumento" />  
								                          	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
								                        </td>
								                        <td colspan="2" class="principalLabel">
															<select id="idGrdoCdGrupodocumentoCarta" onchange="carregaDocuCarta();" class="principalLabel" style="width:330px"> 
																<option value=""><bean:message key="prompt.selecione_uma_opcao" /></option>
																<logic:present name="grdoList"><logic:iterate id="csCdtbGrupoDocumentoGrdoVo" name="grdoList">
																	<option value="<bean:write name="csCdtbGrupoDocumentoGrdoVo" property="idGrdoCdGrupoDocumento" />"><bean:write name="csCdtbGrupoDocumentoGrdoVo" property="grdoDsGrupoDocumento" /></option>
																</logic:iterate></logic:present>
															</select>
								                        </td>
								                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
								                      </tr>
											          <tr> 
								                        <td class="principalLabel" align="right" height="28" width="30%"><plusoft:message key="prompt.documento" />  
								                          	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
								                        </td>
								                        <td colspan="2" class="principalLabel">
							                              	<select id="idDocuCdDocumentoCarta" onchange="carregaIdDocuCarta();" class="principalLabel" style="width:330px"> 
																<option value=""><bean:message key="prompt.selecione_uma_opcao" /></option> 
															</select>
								                        </td>
								                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
								                      </tr>
						                      
						                      
						                            <!--tr> 
						                              <td width="73%">                        
						                                <html:select property="cartaEnviar" name="acaoCobrancaForm" styleClass="principalLabel" style="width:345px">
						                                	<html:option value="">-- Selecione uma op&ccedil;&atilde;o--</html:option>                                
						                               			<logic:notEmpty name="csCdtbDocumentoDocu">
							                            			<bean:define name="csCdtbDocumentoDocu" id="documentoVO" />
							                            			<html:options collection="documentoVO"  property="idDocuCdDocumento" labelProperty="docuDsDocumento"/>
							                            		</logic:notEmpty>
						                                </html:select>                    
						                              </td>
						                              <td width="27%">&nbsp;</td>
						                            </tr-->
						                            
						                            
						                          </table>
						                        </td>
						                      </tr>
									        </table>
									      </div>
									      <div id="sms" style="width:100%;display: none"> 
									        <table width="100%" border="0" cellspacing="0" cellpadding="0">
									        <tr> 
						                        <td class="principalLabel" align="right" height="28" width="30%"><bean:message key="prompt.classeImplementacao"/>  
						                          	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
						                        </td>
						                        <td colspan="2" class="principalLabel">
													<select id="idIpch" class="principalLabel" style="width:330px" onchange="setImplement()"> 
														<option value=""><bean:message key="prompt.selecione_uma_opcao" /></option>
														<logic:present name="ipchList"><logic:iterate id="ipchVo" name="ipchList">
															<option value="<bean:write name="ipchVo" property="field(id_ipch_cd_implementchat)" />"><bean:write name="ipchVo" property="field(ipch_ds_implementchat)" /></option>
														</logic:iterate></logic:present>
													</select>
						                        </td>
						                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
						                      </tr>
									          <tr> 
						                        <td class="principalLabel" align="right" height="28" width="30%"><plusoft:message key="prompt.grupoDocumento" />  
						                          	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
						                        </td>
						                        <td colspan="2" class="principalLabel">
													<select id="idGrdoCdGrupodocumento" onchange="carregaDocu();" class="principalLabel" style="width:330px"> 
														<option value=""><bean:message key="prompt.selecione_uma_opcao" /></option>
														<logic:present name="grdoList"><logic:iterate id="csCdtbGrupoDocumentoGrdoVo" name="grdoList">
															<option value="<bean:write name="csCdtbGrupoDocumentoGrdoVo" property="idGrdoCdGrupoDocumento" />"><bean:write name="csCdtbGrupoDocumentoGrdoVo" property="grdoDsGrupoDocumento" /></option>
														</logic:iterate></logic:present>
													</select>
						                        </td>
						                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
						                      </tr>
									          <tr> 
						                        <td class="principalLabel" align="right" height="28" width="30%"><plusoft:message key="prompt.documento" />  
						                          	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
						                        </td>
						                        <td colspan="2" class="principalLabel">
					                              	<select id="idDocuCdDocumento" onchange="carregaIdDocu();" class="principalLabel" style="width:330px"> 
														<option value=""><bean:message key="prompt.selecione_uma_opcao" /></option> 
													</select>
						                        </td>
						                        <td width="4%" class="principalLabel" colspan="2">&nbsp;</td>
						                      </tr>
									        </table>
									      </div>
									    </td>
									  </tr>
									</table>	
							    </td>
						     </tr>
						   </table>
                        </td>
                      </tr>
                    </table>				
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td>&nbsp;</td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/new.gif" alt="Novo" name="imgNovo" id="imgNovo" class="geralCursoHand" onclick="novo();"/>
                      </td>
                      <td width="25" align="center">
                      	<img src="webFiles/images/botoes/gravar.gif" alt="Gravar" name="imgGravar" id="imgGravar" class="geralCursoHand" onclick="salvar();"/>
                      </td>
                      <td width="25" align="center">
	                      <img src="webFiles/images/botoes/cancelar.gif" alt="Cancelar" name="imgCancelar" id="imgCancelar" class="geralCursoHand" onclick="cancelar()"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>
</body>

	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	
<script>
	temPermissaoIncluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_INCLUSAO_CHAVE%>');
	temPermissaoAlterar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>');
	temPermissaoExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_EXCLUSAO_CHAVE%>');
		
	function verificaPermissaoExcluir() {
		if (!temPermissaoExcluir){		
			for(i = 0; i < countRegistro; i++) {
				eval('setPermissaoImageDisable(false, document.acaoCobrancaForm.btExcluir'+i+');');
			}
		}
	}
	
	verificaPermissaoExcluir();
</script>

<logic:equal name="acaoCobrancaForm" property="userAction" value="init">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.acaoCobrancaForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.acaoCobrancaForm.imgGravar);
			desabilitaCamposAcoesCobranca();
		}
	</script>
</logic:equal>
<logic:equal name="acaoCobrancaForm" property="userAction" value="alterar">
	<script>
		if(!temPermissaoAlterar) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.acaoCobrancaForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.acaoCobrancaForm.imgGravar);
		}
	</script>
</logic:equal>
<logic:equal name="acaoCobrancaForm" property="userAction" value="cancelar">
	<script>
		if(!temPermissaoIncluir) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.acaoCobrancaForm.imgNovo);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ACAO_COBRANCA_ALTERACAO_CHAVE%>', document.acaoCobrancaForm.imgGravar);
			desabilitaCamposAcoesCobranca();
		}	
	</script>
</logic:equal>

<script>
	habilitaListaEmpresas();
	setaArquivoXml("CS_ASTB_IDIOMACAOCOB_IDAC.xml");
	habilitaTelaIdioma();

	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(document.forms[0].idAcao.value);
	
	if(acaoCobrancaForm.idGrdoCdSms.value != ''){
		document.forms[0].elements["idGrdoCdGrupodocumento"].value = acaoCobrancaForm.idGrdoCdSms.value;
	}
	
	if(acaoCobrancaForm.idGrdoCdCarta.value != ''){
		document.forms[0].elements["idGrdoCdGrupodocumentoCarta"].value = acaoCobrancaForm.idGrdoCdCarta.value;
	}
	
	if(acaoCobrancaForm.idIpchCdImplementchat.value != ''){
		document.forms[0].elements["idIpch"].value = acaoCobrancaForm.idIpchCdImplementchat.value;
	}
	
	
	carregaDocu();
	carregaDocuCarta();
</script>
</html> 