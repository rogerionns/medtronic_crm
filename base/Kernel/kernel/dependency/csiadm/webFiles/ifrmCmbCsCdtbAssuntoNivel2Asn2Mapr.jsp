<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/AdministracaoCsAstbMateriaProdutoMapr.do" styleId="administracaoCsAstbMateriaProdutoMaprForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
  <html:select property="idAsn2CdAssuntoNivel2" styleClass="principalObjForm">
    <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
    <html:options collection="csCdtbAssuntoNivel2Asn2Vector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2" />
  </html:select>
</html:form>
</body>
</html>