<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsAstbFuncareavendaFuavForm" action="/AdministracaoCsAstbFuncareavendaFuav.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario"/>
	<html:hidden property="csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario"/>

<script>var possuiRegistros=false;</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:present name="csAstbFuncareavendaFuavVector">
  <logic:iterate id="cagaaVector" name="csAstbFuncareavendaFuavVector">
  <script>possuiRegistros=true;</script>
  <tr> 
    <td width="5%" class="principalLstPar"> 
      &nbsp; &nbsp; <bean:write name="cagaaVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" /> 
    </td>
    <td class="principalLstParMao" width="3%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />','<bean:write name="cagaaVector" property="csCdtbFuncVendedorFuncVo.idFuncCdFuncionario" />')"> 
      &nbsp; </td>
    <td class="principalLstParMao" align="left" width="51%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />','<bean:write name="cagaaVector" property="csCdtbFuncVendedorFuncVo.idFuncCdFuncionario" />')"> 
      <bean:write name="cagaaVector" property="csCdtbFuncAreaFuncVo.funcNmFuncionario" /> 
    </td>
	<td> 
    </td>
  </tr>
  </logic:iterate>
  </logic:present>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>