<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.util.Datetime"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script>

function inicio() {
	document.csCdtbDesenhoprocessoDeprForm.depr_nr_prazonormal.disabled = true;
	document.csCdtbDesenhoprocessoDeprForm.depr_nr_prazoespecial.disabled = true;

	calculaPrazoEmHorasDias()
}

function onClickInativo() {
	document.csCdtbDesenhoprocessoDeprForm.dataInativo.value = '';
}

function habilitaDesabilitaCampos(valor){	
	//document.csCdtbDesenhoprocessoDeprForm.id_depr_cd_desenhoprocesso.disabled= valor;
	document.csCdtbDesenhoprocessoDeprForm.depr_ds_desenhoprocesso.disabled= valor;
	document.csCdtbDesenhoprocessoDeprForm.depr_dh_inativo.disabled= valor;
	//document.csCdtbDesenhoprocessoDeprForm.depr_nr_prazonormal.disabled= valor;
	//document.csCdtbDesenhoprocessoDeprForm.depr_nr_prazoespecial.disabled= valor;
	//document.csCdtbDesenhoprocessoDeprForm.id_tppr_cd_tpprazonormal.disabled= valor;
	//document.csCdtbDesenhoprocessoDeprForm.id_tppr_cd_tpprazoespecial.disabled= valor;
}

function limpaCampos(){	
	document.csCdtbDesenhoprocessoDeprForm.id_depr_cd_desenhoprocesso.value= "";
	document.csCdtbDesenhoprocessoDeprForm.depr_ds_desenhoprocesso.value= "";
	document.csCdtbDesenhoprocessoDeprForm.depr_nr_prazonormal.value= "";
	document.csCdtbDesenhoprocessoDeprForm.depr_nr_prazoespecial.value= "";
	//document.csCdtbDesenhoprocessoDeprForm.id_tppr_cd_tpprazonormal.value= "";
	//document.csCdtbDesenhoprocessoDeprForm.id_tppr_cd_tpprazoespecial.value= "";
	document.csCdtbDesenhoprocessoDeprForm.acao.value= "";
}

function abrePopupFluxoWorkflow() {
	if(document.csCdtbDesenhoprocessoDeprForm.id_depr_cd_desenhoprocesso.value == '') {
		if(confirm('<bean:message key="prompt.ParaEditarOFluxoENecessarioGravar"/>')) {
			parent.clearError();
			parent.submeteSalvar();
		}
	} else {
		var url = 'AbrePopupFluxoWorkflow.do?id_depr_cd_desenhoprocesso='+csCdtbDesenhoprocessoDeprForm.id_depr_cd_desenhoprocesso.value;
		//url += '&etpr_nr_prazonormal_cabecalho='+csCdtbDesenhoprocessoDeprForm.depr_nr_prazonormal.value;
		//url += '&etpr_nr_prazoespecial_cabecalho='+csCdtbDesenhoprocessoDeprForm.depr_nr_prazoespecial.value;
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:740px,dialogTop:150px,dialogLeft:150px');
		//window.open(url,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:740px,dialogTop:150px,dialogLeft:150px');
	}
	
}

function calculaPrazoEmHorasDias(){
	if(document.forms[0].depr_nr_prazonormal.value != ''){
		prazoNormal = new Number(document.forms[0].depr_nr_prazonormal.value);
		prazoEspecial = new Number(document.forms[0].depr_nr_prazoespecial.value);
	
		document.getElementById('spanHorasEDiasNormal').innerHTML = (prazoNormal / 60) + ' horas / ' + (prazoNormal / 60 / <%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_DESENHOPROCESSO_QTDHORASDIARIAS, request)%>) + ' dias';
		document.getElementById('spanHorasEDiasEspecial').innerHTML = (prazoEspecial / 60) + ' horas / ' + (prazoEspecial / 60 / <%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_DESENHOPROCESSO_QTDHORASDIARIAS, request)%>) + ' dias';
	}

}
</script>
<body class= "principalBgrPageIFRM" onLoad="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form styleId="csCdtbDesenhoprocessoDeprForm" action="/EditaCsCdtbDesenhoprocessoDepr.do">
<html:hidden property="acao"/>
<html:hidden property="dataInativo"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
  <tr> 
    <td>&nbsp; </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="30%" class="principalLabel" align="right"><bean:message key="prompt.codigo"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td width="70%" class="principalLabel"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="16%"> 
           <html:text property="id_depr_cd_desenhoprocesso" styleClass="principalObjForm" disabled="true" />
          </td>
          <td width="84%">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="30%" class="principalLabel" align="right"><bean:message key="prompt.DescricaoDoProcesso"/>
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td width="70%" class="principalLabel"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="51%"> 
            <html:text property="depr_ds_desenhoprocesso" styleClass="principalObjForm" maxlength="100" style="width: 257px"/>
          </td>
          <td width="49%">&nbsp;<img src="webFiles/images/icones/FluxoWorkflow.gif" width="32" height="32" title="<bean:message key='prompt.fluxoWorkflow'/>" class="geralCursoHand" onClick="abrePopupFluxoWorkflow()"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="16%" class="principalLabel" align="right">&nbsp;</td>
    <td width="14%" class="principalLabel" align="right"><bean:message key="prompt.PrazoNormal"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	<td width="70%" class="principalLabel"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="25%"> 
            <html:text property="depr_nr_prazonormal" styleClass="principalObjFormCentralizado" maxlength="5" onkeypress="isDigito(this)" />
          </td>
          <td width="26%"> 
         	 <html:select property="id_tppr_cd_tpprazonormal" styleClass="principalObjForm" disabled="true" > 
				<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
				<logic:present name="csDmtbTpprazoTpprVector">
					<html:options collection="csDmtbTpprazoTpprVector" property="field(id_tppr_cd_tpprazo)" labelProperty="field(tppr_ds_tpprazo)"/>
				</logic:present> 
			 </html:select> 
          </td>
          <td width="85%" class="principalLabel"><span id="spanHorasEDiasNormal"></span></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="16%" class="principalLabel" align="right">&nbsp;</td>
    <td width="14%" class="principalLabel" align="right"><bean:message key="prompt.PrazoEspecial"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	<td width="70%" class="principalLabel"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="25%"> 
            <html:text property="depr_nr_prazoespecial" styleClass="principalObjFormCentralizado" maxlength="5" onkeypress="isDigito(this)" />
          </td>
          <td width="26%"> 
         	 <html:select property="id_tppr_cd_tpprazoespecial" styleClass="principalObjForm" disabled="true" > 
				<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
				<logic:present name="csDmtbTpprazoTpprVector">
					<html:options collection="csDmtbTpprazoTpprVector" property="field(id_tppr_cd_tpprazo)" labelProperty="field(tppr_ds_tpprazo)"/>
				</logic:present> 
			 </html:select> 
          </td>
          <td width="85%" class="principalLabel"><span id="spanHorasEDiasEspecial"></span></td>
        </tr>
      </table>
    </td>  
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="30%" class="principalLabel" align="right">&nbsp;</td>
    <td width="70%" class="principalLabel"> 
      <html:checkbox value="true" property="depr_dh_inativo" onclick="onClickInativo()"/>
      <bean:message key="prompt.inativo"/>&nbsp; </td>
  </tr>
</table>
</html:form>
</body>

<script>
	if(document.csCdtbDesenhoprocessoDeprForm.id_depr_cd_desenhoprocesso.value == "") {
		if(getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DESENHO_PROCESSO_INCLUSAO_CHAVE%>')) {
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DESENHO_PROCESSO_INCLUSAO_CHAVE%>', parent.document.csCdtbDesenhoprocessoDeprForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DESENHO_PROCESSO_INCLUSAO_CHAVE%>', document.csCdtbDesenhoprocessoDeprForm.imgCalendario, "Calendário");
			habilitaDesabilitaCampos(false);
		} else {
			habilitaDesabilitaCampos(true);
		}
	}
	else if(document.csCdtbDesenhoprocessoDeprForm.id_depr_cd_desenhoprocesso.value != "") {
		if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DESENHO_PROCESSO_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DESENHO_PROCESSO_ALTERACAO_CHAVE%>', parent.document.forms[0].imgGravar);
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DESENHO_PROCESSO_ALTERACAO_CHAVE%>',  document.csCdtbDesenhoprocessoDeprForm.imgCalendario, "Calendário");	
			habilitaDesabilitaCampos(false);
		} else {
			habilitaDesabilitaCampos(true);
		}
	}
	
	if(document.csCdtbDesenhoprocessoDeprForm.acao.value == "excluir") {
		habilitaDesabilitaCampos(true);
		confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
		parent.setConfirm(confirmacao);
	}
</script>
</html>

<script>
	try{document.csCdtbDesenhoprocessoDeprForm['depr_ds_desenhoprocesso'].focus();}
	catch(e){}
</script>