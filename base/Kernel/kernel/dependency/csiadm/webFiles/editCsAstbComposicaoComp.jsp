<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<!-- meta http-equiv="Content-Type" content="text/html; charset=utf-8" /-->
</head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/inputFileStyle.js"></script>

<style>
    .pOF label.cabinetNew { width: 18px; height: 18px; background: url(../../../plusoft-resources/images/botoes/textfolder-18.gif) 0 0 no-repeat; display: ; overflow: hidden; cursor: pointer; vertical-align: middle;   }
	.pOF label.cabinetNew input.file { height: 100%; width: auto; opacity: 0; -moz-opacity: 0; filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0); cursor: pointer; }
</style>

<script language="JavaScript">
var cTela = "";
var cAcao = "";

function validaPath(campo){
	var valor = campo.value;
	if(campo.value.substring(0,4)=='www.'){
		alert('<bean:message key="prompt.O_campo_deve_comecar_por_http"/>');
		document.administracaoCsAstbComposicaoCompForm.txtPath.focus();
	}
}
<!--
nLinha = new Number(0);
var modalWin;

function MontaPath(){

	cPath=document.administracaoCsAstbComposicaoCompForm.txtPath.value;
	//Chamado: 90988 - 26/09/2013 - Carlos Nunes
	if(cPath.length>2000){
		alert("<bean:message key="prompt.Por_favor_informe_o_caminho_do_arquivo_menor_tamanho_maximo_ultrapassado"/>");
		return false;
	}

	if (document.administracaoCsAstbComposicaoCompForm.NrLinha.value != "0"){
		AltPathArq(document.administracaoCsAstbComposicaoCompForm.txtPath.value , document.administracaoCsAstbComposicaoCompForm.nIdPath.value , document.administracaoCsAstbComposicaoCompForm.NrLinha.value );
	}else{
		if(document.administracaoCsAstbComposicaoCompForm.txtPath.value != ""){
			AddPathArq(document.administracaoCsAstbComposicaoCompForm.txtPath.value , document.administracaoCsAstbComposicaoCompForm.nIdPath.value );
		}else{
			alert("<bean:message key="prompt.Por_favor_informe_o_caminho_do_arquivo"/>");
			document.administracaoCsAstbComposicaoCompForm.txtPath.focus();
			return false;	
		}
	}	
	document.administracaoCsAstbComposicaoCompForm.nIdPath.value = "0";
	document.administracaoCsAstbComposicaoCompForm.NrLinha.value = "0";
	//document.getElementById("path").innerHTML = "<input type=file name=txtPath class=text>";
	document.administracaoCsAstbComposicaoCompForm.txtPath.value = "";
	document.administracaoCsAstbComposicaoCompForm.txtPath.focus();
	
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  if (modalWin!=null && !modalWin.closed){
	modalWin.focus();
  }else{
	 modalWin = window.open(theURL,winName,features);
  }
}

function AtualizaPath(){
	AltPathArq(document.administracaoCsAstbComposicaoCompForm.txtPath.value,13)
}

function AddPathArq(cPathArq,nIdPath)
{
	nLinha = nLinha + 1;

	strTxt = "";
	strTxt += "<div id=\"lstPath" + nLinha + "\" style='width:95%; height:15px; z-index:1;'>";
	strTxt += "	<table name=\"" + nLinha + "\" id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
	strTxt += "		<tr> ";
	if (nIdPath == "0")	
		strTxt += "		<td class=principalLstParMao width=30 align=center><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=RemovePathArq(\"" + nLinha + "\") title='<bean:message key="prompt.excluir"/>'></td>";
	else
		strTxt += "		<td class=principalLstParMao width=30 align=center><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=RemovePathArqBD(\"" + nLinha + "\",\"" + nIdPath + "\") title='<bean:message key="prompt.excluir"/>'></td>";		
	
	//strTxt += "		<td class=principalLstPar width=723 onclick=\"javascript:EditPathArq(" + nLinha + ",'" + cPathArq + "'," + nIdPath + ")\">&nbsp;" + cPathArq ;
	strTxt += "		<td class=principalLstPar width=723>&nbsp;" + acronymLst(cPathArq, 100);
	strTxt += "    <input type=\"hidden\" name=\"cPathArq\" value=\"" + cPathArq + "\">";
	strTxt += "    <input type=\"hidden\" name=\"idPathArq\" value=\"" + nIdPath + "\">";
	strTxt += "		</td> ";
	strTxt += "		</tr> ";
	strTxt += " </table> ";
	strTxt += "</div> ";	
	
	lstPath.innerHTML = lstPath.innerHTML + strTxt;
}


function AltPathArq(cPathArq,nIdPath,NrLinha)
{
	strTxt = "";
	strTxt += "	<table class=geralCursoHand name=\"" + NrLinha + "\" id=\"" + NrLinha + "\" width=95% border=0 cellspacing=0 cellpadding=0>";
	strTxt += "		<tr> ";
	if (nIdPath == "0")	
		strTxt += "		<td class=principalLstPar width=30 align=center><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=RemovePathArq(\"" + NrLinha + "\") title='<bean:message key="prompt.excluir"/>'></td>";
	else
		strTxt += "		<td class=principalLstPar width=30 align=center><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=RemovePathArqBD(\"" + NrLinha + "\",\"" + nIdPath + "\") title='<bean:message key="prompt.excluir"/>'></td>";		
	
	strTxt += "		<td class=principalLstPar width=723 onclick=\"javascript:EditPathArq(" + NrLinha + ",'" + cPathArq + "'," + nIdPath + ")\">&nbsp;" + cPathArq ;
	strTxt += "    <input type=\"hidden\" name=\"cPathArq\" value=\"" + cPathArq + "\">";
	strTxt += "    <input type=\"hidden\" name=\"idPathArq\" value=\"" + nIdPath + "\">";
	strTxt += "		</td> ";
	strTxt += "		</tr> ";
	strTxt += " </table> ";
	
	RemoveAltPathArq(NrLinha);
	
	eval("lstPath" + NrLinha).innerHTML = eval("lstPath" + NrLinha).innerHTML + strTxt;
}



function RemovePathArq(nTblExcluir){
	msg = "<bean:message key='prompt.desejaExcluirEsseLink' /> ?"
	if (confirm(msg)) {
		//objIdTbl = lstPath.all.item("lstPath" + nTblExcluir);
		objIdTbl = document.getElementById("lstPath" + nTblExcluir);
		lstPath.removeChild(objIdTbl);		
	}
}

function RemoveAltPathArq(nTblExcluir){
	//objIdTbl = eval("lstPath" + nTblExcluir).all.item(nTblExcluir);
	objIdTbl = document.getElementById("lstPath" + nTblExcluir);
	eval("lstPath" + nTblExcluir).removeChild(objIdTbl);	
}

function RemovePathArqBD(nTblExcluir, nIdPath) {
	msg = "<bean:message key="prompt.Deseja_excluir_esse_E-mail"/>"
	if (confirm(msg)) {
		//objIdTbl = lstPath.all.item("lstPath" + nTblExcluir);
		objIdTbl = document.getElementById("lstPath" + nTblExcluir);
		lstPath.removeChild(objIdTbl);
		document.administracaoCsAstbComposicaoCompForm.infoArquivoExcluidos.value += nIdPath + ";";
	}
}

function EditPathArq (NrLinha,cPathArq,nIdPath){
	document.administracaoCsAstbComposicaoCompForm.txtPath.value = cPathArq;
	document.administracaoCsAstbComposicaoCompForm.NrLinha.value = NrLinha;
	document.administracaoCsAstbComposicaoCompForm.nIdPath.value = nIdPath;
}

function PreencheLstPathArq() {

	try{
		
		document.administracaoCsAstbComposicaoCompForm.idInarCdSequencial.value = "";
		document.administracaoCsAstbComposicaoCompForm.inarDsPatharquivo.value = "";
		
		if (document.administracaoCsAstbComposicaoCompForm.idPathArq.length == undefined) {
			document.administracaoCsAstbComposicaoCompForm.idInarCdSequencial.value = document.administracaoCsAstbComposicaoCompForm.idPathArq.value + ";";
			document.administracaoCsAstbComposicaoCompForm.inarDsPatharquivo.value = document.administracaoCsAstbComposicaoCompForm.cPathArq.value + ";";
		} else {
			  for (var i = 0; i < document.administracaoCsAstbComposicaoCompForm.idPathArq.length; i++) {
				document.administracaoCsAstbComposicaoCompForm.idInarCdSequencial.value += document.administracaoCsAstbComposicaoCompForm.idPathArq[i].value + ";";
				document.administracaoCsAstbComposicaoCompForm.inarDsPatharquivo.value += document.administracaoCsAstbComposicaoCompForm.cPathArq[i].value + ";";
			  }
		}
		
	}catch(e){
		
	}
}



//-->

function MontaLista(){
	//Acrescentado o carregamento da lista quando houver a TAG DE VARIEDADE
	document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value + "@" + document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value;

	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
		cmbVariedade.location.href= "AdministracaoCsAstbComposicaoComp.do?tela=cmbCsCdtbAssuntoNivel2Asn2&acao=<%=Constantes.ACAO_VISUALIZAR%>&idAsnCdAssuntoNivel=" +  document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value+"&idLinhCdLinha="+ document.administracaoCsAstbComposicaoCompForm.idLinhCdLinha.value; +"&idAsn1CdAssuntonivel1="+ document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value+"&idAsn2CdAssuntonivel2=" + document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value;
		lstArqCarga.location.href= "AdministracaoCsAstbComposicaoComp.do?tela=administracaoLstCsAstbComposicaoComp&acao=<%=Constantes.ACAO_VISUALIZAR%>&idAsnCdAssuntoNivel=" + document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value +"&idAsn1CdAssuntonivel1="+document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value+"&idAsn2CdAssuntonivel2=" + document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value;
	<%} else{%>
		lstArqCarga.location.href= "AdministracaoCsAstbComposicaoComp.do?tela=administracaoLstCsAstbComposicaoComp&acao=<%=Constantes.ACAO_VISUALIZAR%>&idAsnCdAssuntoNivel=" + document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value;
	<%}%>

   	
}

//Chamado 78869 - Vinicius - Sempre levar em conta a PRAS e a PRMA
function MontaComboManifestacao(){	
	var ajax = new window.top.ConsultaBanco("","/csiadm/AdministracaoCsAstbComposicaoComp.do");
	ajax.addField("idAsn1CdAssuntonivel1", ifrmCmbProduto.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value);
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		ajax.addField("idAsn2CdAssuntonivel2", cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value);
	<%}%>
	
	<%if(Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
		ajax.addField("idLinhCdLinha", administracaoCsAstbComposicaoCompForm.idLinhCdLinha.value);
	<%}%>
	
	ajax.addField("tela", '<%=MAConstantes.TELA_CMB_MANIFESTACAO%>');
	ajax.addField("acao", '<%=Constantes.ACAO_VISUALIZAR%>');
	
	ajax.aguardeCombo(document.administracaoCsAstbComposicaoCompForm.idMatpCdManifTipo);
	
	ajax.executarConsulta(function(){ 
		ajax.popularCombo(document.administracaoCsAstbComposicaoCompForm.idMatpCdManifTipo, "id_matp_cd_maniftipo", "matp_ds_maniftipo", "", true, "");
	});
	
}

function MontaComboProduto(){	
	cTela = document.administracaoCsAstbComposicaoCompForm.tela.value;
	cAcao = document.administracaoCsAstbComposicaoCompForm.acao.value;
	document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_ASSUNTONIVEL1_ASN1%>';
	document.administracaoCsAstbComposicaoCompForm.target = ifrmCmbProduto.name;
	document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.disabled = false;
	document.administracaoCsAstbComposicaoCompForm.submit();
	document.administracaoCsAstbComposicaoCompForm.tela.value = cTela;
	document.administracaoCsAstbComposicaoCompForm.acao.value = cAcao;
}


function selecionaChecks() {
	if (lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idCompCdInativos != null) {
		if (lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idCompCdInativos.length == undefined) {
			lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idCompCdInativos.checked = document.administracaoCsAstbComposicaoCompForm.selecionaTodos.checked;
		} else {
			for (var i = 0; i < lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idCompCdInativos.length; i++) {
				lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idCompCdInativos[i].checked = document.administracaoCsAstbComposicaoCompForm.selecionaTodos.checked;
			}
		}
	}
}

function carregaGrupo() {
	var acao2 = administracaoCsAstbComposicaoCompForm.acao.value;
	var tela2 = administracaoCsAstbComposicaoCompForm.tela.value;

<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
	document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_SUPERGRUPO_SUGR%>';
	document.administracaoCsAstbComposicaoCompForm.target = cmbSuperGrupo.name;	
<%} else {%>
	document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
	document.administracaoCsAstbComposicaoCompForm.target = cmbGrupo.name;	
<% } %>

	document.administracaoCsAstbComposicaoCompForm.submit();
	document.administracaoCsAstbComposicaoCompForm.acao.value = acao2;
	document.administracaoCsAstbComposicaoCompForm.tela.value = tela2;
}

function carregaVariedade(){
  <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
	var acao2 = document.administracaoCsAstbComposicaoCompForm.acao.value;
	var tela2 = document.administracaoCsAstbComposicaoCompForm.tela.value;
	document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_ASSUNTONIVEL2_ASN2%>';
	document.administracaoCsAstbComposicaoCompForm.target = cmbVariedade.name;
	document.administracaoCsAstbComposicaoCompForm.submit();
	document.administracaoCsAstbComposicaoCompForm.acao.value = acao2;
	document.administracaoCsAstbComposicaoCompForm.tela.value = tela2;
	<%}%>

}

function carregaTipo(){
	  
	var acao2 = document.administracaoCsAstbComposicaoCompForm.acao.value;
	var tela2 = document.administracaoCsAstbComposicaoCompForm.tela.value;
	document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_CMB_TPIN%>';
	document.administracaoCsAstbComposicaoCompForm.target = ifrmCmbTpin.name;
	document.administracaoCsAstbComposicaoCompForm.submit();
	document.administracaoCsAstbComposicaoCompForm.acao.value = acao2;
	document.administracaoCsAstbComposicaoCompForm.tela.value = tela2;
}

function carregaTopico(){
	 
	var acao2 = document.administracaoCsAstbComposicaoCompForm.acao.value;
	var tela2 = document.administracaoCsAstbComposicaoCompForm.tela.value;
	document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_CMB_TOIN%>';
	document.administracaoCsAstbComposicaoCompForm.target = ifrmCmbToin.name;
	document.administracaoCsAstbComposicaoCompForm.submit();
	document.administracaoCsAstbComposicaoCompForm.acao.value = acao2;
	document.administracaoCsAstbComposicaoCompForm.tela.value = tela2;		

}

function setFunction(cRetorno, campo){
	if(campo=="administracaoCsAstbComposicaoCompForm.compTxInformacao"){
		Layer2.innerHTML = cRetorno;
		document.administracaoCsAstbComposicaoCompForm.compTxInformacao.value=cRetorno;
	}
}

function desabilitaCamposComposicao(){
	try{
		setPermissaoImageDisable('', document.administracaoCsAstbComposicaoCompForm.imgEditor);
		document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.disabled= true;	
		ifrmCmbProduto.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.disabled= true;	
		document.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.disabled= true;	
		document.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.disabled= true;	
		document.administracaoCsAstbComposicaoCompForm.idMatpCdManifTipo.disabled= true;
		document.administracaoCsAstbComposicaoCompForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].disabled= true;
		cmbGrupo.administracaoCsAstbComposicaoCompForm.idGrmaCdGrupoManifestacao.disabled= true;
		cmbTpManifestacao.administracaoCsAstbComposicaoCompForm.idTpmaCdTpManifestacao.disabled= true;
		document.administracaoCsAstbComposicaoCompForm.compTxInformacao.disabled= true;										
		document.administracaoCsAstbComposicaoCompForm.compDhInativo.disabled= true;
		document.administracaoCsAstbComposicaoCompForm.selecionaTodos.disabled= true;
		document.administracaoCsAstbComposicaoCompForm.txtPath.disabled= true;
		document.administracaoCsAstbComposicaoCompForm.imgEditor.disabled= true;
		document.administracaoCsAstbComposicaoCompForm.imgAdicionaLink.disabled= true;
	}
	catch(e){
		setTimeout("desabilitaCamposComposicao();",500);
	}
}

function validaChkCompInformacao(id) {
	
	try {
		if(document.getElementById("chkTiposInformacao").checked) {
			ifrmCmbTpin.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.selectedIndex = 0;
			ifrmCmbTpin.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.disabled = true;
			ifrmCmbTpin.document.getElementById("divBotao").style.visibility="hidden";
		} else {
			//ifrmCmbTpin.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.selectedIndex = 0;
			ifrmCmbTpin.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.disabled = false;
			ifrmCmbTpin.document.getElementById("divBotao").style.visibility="visible";
		}
	
		if(document.getElementById("chkTopicosInformacao").checked){
			ifrmCmbToin.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.selectedIndex = 0;
			ifrmCmbToin.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.disabled = true;
			ifrmCmbToin.document.getElementById("divBotao").style.visibility="hidden";
		}  else {
			//ifrmCmbToin.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.selectedIndex = 0;
			ifrmCmbToin.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.disabled = false;
			ifrmCmbToin.document.getElementById("divBotao").style.visibility="visible";
		}

	} catch(e) {
		setTimeout('validaChkCompInformacao(\''+id+'\')', 500);
		return false;
	}

	return true;
	
}

function validaChk(id){
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			if(id == "chkVariedadesProduto" && document.getElementById("chkProdutosLinha").checked){
				document.getElementById("chkProdutosLinha").checked = false;
			}
			
			if(document.getElementById("chkProdutosLinha").checked){
				ifrmCmbProduto.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.selectedIndex = 0;
				ifrmCmbProduto.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.disabled = true;
				ifrmCmbProduto.document.getElementById("divBotao").style.visibility="hidden";
	
				<%	if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
					document.getElementById("chkVariedadesProduto").checked = false;
					cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.selectedIndex = 0;
					cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.disabled = true;
				<%	}%>
				return;
			}
			
		<%	}%>
		
		<%	if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			if(document.getElementById("chkVariedadesProduto").checked){
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
					document.getElementById("chkProdutosLinha").checked = false;
				<%	}%>
				cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.selectedIndex = 0;
				administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.disabled = false;
				cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.disabled = true;
				cmbVariedade.document.getElementById("divBotao").style.visibility="hidden";
				return;
			}
		<%	}%>
		
		administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.disabled = false;
		<%	if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				if(document.getElementById("chkVariedadesProduto").checked){
					document.getElementById("chkProdutosLinha").checked = false;
					cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.selectedIndex = 0;
					ifrmCmbProduto.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.disabled = false;
					ifrmCmbProduto.document.getElementById("divBotao").style.visibility="visible";
					cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.disabled = true;
					cmbVariedade.document.getElementById("divBotao").style.visibility="hidden";
				} else{
					ifrmCmbProduto.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.disabled = false;
					ifrmCmbProduto.document.getElementById("divBotao").style.visibility="visible";
		
				<%	if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
						cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.disabled = false;
						cmbVariedade.document.getElementById("divBotao").style.visibility="visible";
				<%	}%>
				}
		<%	}%>

}

function inicio(){
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>

	document.administracaoCsAstbComposicaoCompForm.idLinhCdLinha.value=1;
	
	// Adicionado para carregar o combo de Produto quando n�o tem LINHA para carrega-lo
	if(	document.administracaoCsAstbComposicaoCompForm.acao.value != '<%=Constantes.ACAO_VISUALIZAR%>' ||
		document.administracaoCsAstbComposicaoCompForm.tela.value != '<%=MAConstantes.TELA_EDIT_CS_ASTB_COMPOSICAO_COMP%>'){
			MontaComboProduto();
	}
	<%}%>
	
	setaChavePrimaria(administracaoCsAstbComposicaoCompForm.idCompCdSequencial.value);
}

function carregaProduto(){
	
	<%//if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
		<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
			//document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value;	
		</logic:equal>
	<%//}%>
	
	//if(document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value != "" && document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value > 0){
	//	document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value;	
	//}

	MontaComboProduto();
}

</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();MontaLista();carregaProduto();carregaGrupo();carregaVariedade();carregaTipo();carregaTopico();">
<html:form styleId="administracaoCsAstbComposicaoCompForm" action="/AdministracaoCsAstbComposicaoComp.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<input type="Hidden" name="NrLinha" value="0">
	<input type="Hidden" name="nIdPath" value="0">
	<html:hidden property="idAsnCdAssuntoNivel" />
	<html:hidden property="idAsn1CdAssuntonivel1" />
	<html:hidden property="idAsn2CdAssuntonivel2" />
	<html:hidden property="idTpmaCdTpManifestacao" />
	<html:hidden property="idGrmaCdGrupoManifestacao" />
	<html:hidden property="idTpinCdTipoinformacao" />
	<html:hidden property="idToinCdTopicoinformacao" />	
	<html:hidden property="compTxInformacao" />	
	<html:hidden property="compFonteRetiradaOld"/>
	
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td  style="height: 40px" width="30%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td> <html:text property="idCompCdSequencial" styleClass="text" disabled="true" style="width: 100px;" readonly="true" /> 
    <script>document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.value<=0?document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.value='':'';</script>
    </td>
    <td width="35%">&nbsp;</td>
    <td width="5%">&nbsp;</td>
  </tr>
  <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
	  <tr> 
		<td align="right" class="principalLabel"> <%= getMessage("prompt.linha", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td>
			<!-- COMBO DE LINHA -->
			<html:select property="idLinhCdLinha" styleClass="principalObjForm" onchange="MontaComboProduto()"> 
				<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/> 
			</html:select>
		</td>
		<td class="principalLabel">
			<!-- TODOS OS PRODUTOS DA LINHA -->
			<input type="checkbox" id="chkProdutosLinha" name="chkProdutosLinha" onclick="validaChk(this.id);" >
			<bean:message key="prompt.todos_tipos_produtos_linha"/>
		</td>
		<td >&nbsp;</td>
	  </tr>
  <%} else {%>

	  <tr style="display:none"> 
		<td align="right" class="principalLabel"> <%= getMessage("prompt.linha", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td> 
			<div style="position:absolute; heigth:23; width:80; z-index:-1; visibility: hidden" >
				<html:select property="idLinhCdLinha" styleClass="principalObjForm" onchange="MontaComboProduto()"> 
			  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/> 
			  </html:select> 
			</div>
		</td>
		<td class="principalLabel" style="visibility: hidden">
			<!-- TODOS OS PRODUTOS DA LINHA -->
			<input type="checkbox" id="chkProdutosLinha" name="chkProdutosLinha" onclick="validaChk(this.id);" >
			<bean:message key="prompt.todos_tipos_produtos_linha"/>
		</td>
		<td>&nbsp;</td>
	  </tr>
 <%}%>
 
  <tr> 
    <td align="right" class="principalLabel"> <%= getMessage("prompt.assuntoNivel1", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td>
    	<!-- PRODUTO ASSUNTO -->
<!-- 
		<html:select property="idAsn1CdAssuntonivel1" styleClass="principalObjForm" onchange="MontaLista()"> 
			<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
			<html:options collection="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" labelProperty="csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1"/>  
		</html:select>
 -->
		<iframe name="ifrmCmbProduto" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	</td>
	<td class="principalLabel">
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
		<!-- TODAS AS VARIEDADES DO PRODUTO -->
		<input type="checkbox" id="chkVariedadesProduto" name="chkVariedadesProduto" onclick="validaChk(this.id);" >
		<bean:message key="prompt.todos_produtos_tipo_produto"/>
	<% } %>
	</td>
    <td >&nbsp;</td>
  </tr>
  <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
	  <tr> 
		<td class="principalLabel"> 
		  <div align="right"><%= getMessage("prompt.assuntoNivel2", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
		</td>
		<td>
		  <!-- VARIEDADE -->
		  <iframe name="cmbVariedade" src="AdministracaoCsAstbComposicaoComp.do?acao=visualizar&tela=cmbCsCdtbAssuntoNivel2Asn2" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
		</td>
		<td>&nbsp;</td>
		<td >&nbsp;</td>
	  </tr>
  <%}%>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.tipoInformacao"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td>
		<iframe id=ifrmCmbTpin name="ifrmCmbTpin" src="AdministracaoCsAstbComposicaoComp.do?acao=visualizar&tela=<%= MAConstantes.TELA_CMB_TPIN %>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
	</td>
	<td class="principalLabel">
		<input type="checkbox" id="chkTiposInformacao" name="chkTiposInformacao" onclick="return validaChkCompInformacao(this.id);" >
		<bean:message key="prompt.todos_tipo_informacao"/>
	</td>
    <td >&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.topicoInformacao"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td> 
    	<iframe id=ifrmCmbToin name="ifrmCmbToin" src="AdministracaoCsAstbComposicaoComp.do?acao=visualizar&tela=<%= MAConstantes.TELA_CMB_TOIN %>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
    </td>
	<td class="principalLabel">
		<input type="checkbox" id="chkTopicosInformacao" name="chkTopicosInformacao" onclick="return validaChkCompInformacao(this.id);" >
		<bean:message key="prompt.todos_topico_informacao"/>
	</td>
    <td >&nbsp;</td>
  </tr>
  <tr> 
    <td class="principalLabel"> 
      <div align="right"><%= getMessage("prompt.manifestacao", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="2">
      <html:select property="idMatpCdManifTipo" styleClass="principalObjForm" onchange="carregaGrupo()"> 
        <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
        <!-- html:options collection="csCdtbManifTipoMatpVector" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo"/ -->
      </html:select>
    </td>
    <td >&nbsp;</td>
  </tr>
  
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
  <tr> 
    <td class="principalLabel"> 
      <div align="right"><bean:message key="prompt.Supergrupo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="2">
      <iframe name="cmbSuperGrupo" id="cmbSuperGrupo" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
    <td >&nbsp;</td>
  </tr>
<%} %>

  <tr> 
    <td class="principalLabel"> 
      <div align="right"><%= getMessage("prompt.grupomanif", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="2">
      <iframe name="cmbGrupo" id="cmbGrupo" src="AdministracaoCsAstbComposicaoComp.do?acao=visualizar&tela=cmbCsCdtbGrupoManifestacaoGrma" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
    <td >&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><%= getMessage("prompt.tipomanif", request)%>
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
      <iframe name="cmbTpManifestacao" src="AdministracaoCsAstbComposicaoComp.do?acao=visualizar&tela=cmbCsCdtbTpManifestacaoTpma" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
    <td >
      &nbsp;
    </td>
  </tr>
  <tr> 
   <td class="principalLabel"> 
	<div align="right"><bean:message key="prompt.respostaEMail"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
   </td>
   <td colspan="2"> <html:select property="csCdtbDocumentoDocuVo.idDocuCdDocumento" styleClass="principalObjForm" > 
	<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/> 
	</html:select> </td>
   <td >&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
	   <div id="Layer2" class="principalLabel" style="position:relative; width:553; z-index:1; overflow: auto; height: 60; background-color: #FFFFFF; border: 1px none #000000"> 
		<script language="JavaScript">
		document.write(document.administracaoCsAstbComposicaoCompForm.compTxInformacao.value);
	   </script>
	  </div>
    </td>
    <td ><img src="webFiles/images/botoes/bt_CriarCarta.gif" name="imgEditor" width="25" height="22" class="geralCursoHand" border="0" title="<bean:message key="prompt.editor"/>" onClick="window.open('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=administracaoCsAstbComposicaoCompForm.compTxInformacao&carta=false','Documento','width=1000; height=700; scroll=no')" > </td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.fonteDeRetirada"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2" class="principalLabel">
    	<div id="Layer3" style="position:relative; width:560; z-index:1; overflow: auto; height: 40"> 
	 		<html:textarea styleClass="principalObjForm" property="compFonteRetirada" onkeypress="textCounter(this, 240)" onkeyup="textCounter(this, 240)"/>
	 	</div>
    </td>
    <td >&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.dataDeElaboracao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td class="principalLabel">
	 	<html:text property="compDhElaboracao" readonly="true" styleClass="principalObjForm"/>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel">&nbsp;</td>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
         <% // 91850 - 04/11/2013 - Jaider Alba %>
          <td class="principalLabel"> 
       		<html:checkbox value="true" property="compInWeb"/>
       		&nbsp;<bean:message key="prompt.adm.web"/>
       	  </td>
          <td class="principalLabel">
          	<html:checkbox value="true" property="compDhInativo"/>
          	&nbsp;<bean:message key="prompt.inativo"/> 
          </td>
        </tr>
      </table>
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.link"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><!-- Chamado: 90988 - 26/09/2013 - Carlos Nunes  -->
		<!-- 	Jonathan Costa - Internet Explorer 11 / Chamado: 97986  -->
   	  	<div>
   		<div style="width: 500px;float: left;">
   			<input type="text" class="pOF" maxlength="2000" name="txtPath" style="width: 500px;" onblur="validaPath(this); " />
   		</div>
		<div style="width: 18px;float: left;">
			<label class="cabinetNew"> 
				<input type="file" id="browseFile" style="width:18px;left: 0px; top:0px;" class="file" onchange="document.getElementsByName('txtPath')[0].value = this.value;" title="<bean:message key='prompt.procurar'/>"/>
			</label>
		</div>
		      	  
		<html:hidden property="idInarCdSequencial"/>
		<html:hidden property="inarDsPatharquivo"/>
		<html:hidden property="infoArquivoExcluidos"/>

      <div style="width: 18px;float: left;"><img src="webFiles/images/icones/setaDown.gif" style="display: block;" name="imgAdicionaLink" class="geralCursoHand" onClick="return MontaPath()" title="<bean:message key="prompt.confirmar"/>"></div>
      </div>
    </td>
    <td >&nbsp;</td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td colspan="3" height="70" valign="top"> 
      <div id="lstPath" style="position:absolute; width:97%; height:70; z-index:1; overflow: auto"> 
      </div>
	</td>
    <td  height="40">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="90" width="50%">
      <table width="98%" border="0" cellspacing="0" cellpadding="0" align="left">
        <tr> 
          <td width="0%" valign="top"> &nbsp;</td>
          <td width="3%" class="principalLstCab" height="1"><input type="checkbox" name="selecionaTodos" onclick="selecionaChecks()"></td>
          <td width="3%" class="principalLstCab">&nbsp;</td>
          <td align="left" class="principalLstCab" width="10%"> <bean:message key="prompt.codigo"/></td>
          <td class="principalLstCab" width="35%"> <bean:message key="prompt.topicoInformacao"/></td>
          <td class="principalLstCab" width="32%"> <bean:message key="prompt.tipoInformacao"/></td>
          <td align="left" class="principalLstCab" width="30px"> <bean:message key="prompt.inativo"/></td>
          <!-- 
          <td align="left" class="principalLstCab" width="5%">&nbsp;</td>
           -->
        </tr>
        <tr height="90" width="100%"> 
          <td width="0%" valign="top"> &nbsp;</td>
          <td colspan="6" width="100%"> 
          	<iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%"  scrolling="auto" frameborder="0" ></iframe></td>
        </tr>
        <tr> 
          <td width="0%"> &nbsp;</td>
          <td colspan="6" valign="bottom" align="right">
          	<img src="webFiles/images/botoes/Inativar.gif" name="imgInativar" width="68" height="30" class="geralCursoHand" title="<bean:message key='prompt.inativar'/>" onclick="parent.clearError();parent.submeteFormInativar()">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<script language="JavaScript">
<logic:present name="csCdtbInfoarquivoInarVector">	
  <logic:iterate id="cciiVector" name="csCdtbInfoarquivoInarVector">
		AddPathArq('<bean:write name="cciiVector" property="inarDsPatharquivo" />','<bean:write name="cciiVector" property="idInarCdSequencial" />');
  </logic:iterate>
</logic:present>
</script> 

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
	<script language="JavaScript">
	//	desabilitaCamposComposicao();
		confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
		parent.setConfirm(confirmacao);
	</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script language="JavaScript">
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsAstbComposicaoCompForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_INCLUSAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsAstbComposicaoCompForm.imgGravar);	
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_ALTERACAO_CHAVE%>', administracaoCsAstbComposicaoCompForm.imgInativar);
				desabilitaCamposComposicao();
			}else{
				administracaoCsAstbComposicaoCompForm.idCompCdSequencial.disabled= false;
				administracaoCsAstbComposicaoCompForm.idCompCdSequencial.value= '';
				administracaoCsAstbComposicaoCompForm.idCompCdSequencial.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
	<script language="JavaScript">

		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_ALTERACAO_CHAVE%>', parent.administracaoCsAstbComposicaoCompForm.imgGravar);
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_ALTERACAO_CHAVE%>', administracaoCsAstbComposicaoCompForm.imgInativar);
			desabilitaCamposComposicao();
		}
	</script>
</logic:equal>

</html:form>
</body>

</html>

<script language="JavaScript">
	try{ifrmCmbProduto.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.focus();
	
		SI.Files.stylizeById('browseFile');
	}catch(e){}
</script>