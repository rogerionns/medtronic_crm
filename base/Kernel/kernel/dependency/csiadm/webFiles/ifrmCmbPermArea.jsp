<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
function atualizaFunc() {
	document.administracaoPermissionamentoForm.tela.value = '<%= MAConstantes.TELA_IFRM_CMB_PERMFUNC %>';
	document.administracaoPermissionamentoForm.target = parent.ifrmCmbPermFunc.name;
	document.administracaoPermissionamentoForm.submit();
}

function inicio(){
	if(!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_PERMISSIONAMENTO_FUNCIONARIO_CHAVE%>')){
		document.administracaoPermissionamentoForm["csCdtbAreaAreaVo.idAreaCdArea"].disabled = true;
	}else{
		document.administracaoPermissionamentoForm["csCdtbAreaAreaVo.idAreaCdArea"].disabled = false;
	}
}
</script>

</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form action="/AdministracaoPermissionamento.do" styleId="administracaoPermissionamentoForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
		
	<html:select property="csCdtbAreaAreaVo.idAreaCdArea" styleClass="principalObjForm" onchange="atualizaFunc();">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		<html:options collection="vectorArea" property="idAreaCdArea" labelProperty="areaDsArea"/>
	</html:select>
</html:form>
</body>
</html>
