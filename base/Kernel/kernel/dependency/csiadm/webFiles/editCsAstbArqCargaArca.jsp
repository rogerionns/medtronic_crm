<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
</head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
	function VerificaCampos(){
		if (document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value == "0" ){
			document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.value = "" ;
		}
		if (document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.value == "0" ){
			document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.value = "" ;
		}
		if (document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.value == "0" ){
			document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.value = "" ;
		}			
	}
	function MontaLista(){
		lstArqCarga.location.href= "AdministracaoCsAstbArqCargaArca.do?tela=administracaoLstCsAstbArqCargaArca&acao=<%=Constantes.ACAO_VISUALIZAR%>&idLaouCdSequencial=" + document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.value;
	}
	
	function desabilitaCamposArqCarga(){
		document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.disabled= true;	
		document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.disabled= true;
		document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.disabled= true;
	}
	
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos();MontaLista()" style="overflow: hidden;">
<html:form styleId="administracaoCsAstbArqCargaArcaForm" action="/AdministracaoCsAstbArqCargaArca.do"> 
<html:hidden property="modo" /> 
<html:hidden property="acao" /> 
<html:hidden property="tela" /> 
<html:hidden property="topicoId" /> 
<html:hidden property="idCaloCdSequencialAntigo" /> 

<input type="hidden" name="txtSequencia" value="<bean:write name="administracaoCsAstbArqCargaArcaForm" property="arcaNrSequencia" />">
<input type="hidden" name="txtIdCalo" value="<bean:write name="administracaoCsAstbArqCargaArcaForm" property="idCaloCdSequencial" />">
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0" class="principalLabel">
        <tr> 
          <td width="23%"> 
            <div align="right"><bean:message key="prompt.layout"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td colspan="3"> <html:select property="idLaouCdSequencial" onchange="MontaLista()" styleClass="principalObjForm" > 
            <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbLayoutLaouVector" property="idLaouCdSequencial" labelProperty="laouDsLayout"/> 
            </html:select> </td>
          <td width="12%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="23%"> 
            <div align="right"><bean:message key="prompt.camposLayout"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td colspan="3"> <html:select property="idCaloCdSequencial" styleClass="principalObjForm" > 
            <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbCamposLayoutCaloVector" property="idCaloCdSequencial" labelProperty="caloDsDescricao"/> 
            </html:select> </td>
          <td width="12%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="23%"> 
            <div align="right"><bean:message key="prompt.sequencia"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td width="17%"> <html:text property="arcaNrSequencia" styleClass="text" maxlength="6" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" /> 
          </td>
          <td width="26%">&nbsp;</td>
          <td width="22%">&nbsp;</td>
          <td width="12%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="23%"> 
            <div align="right"><bean:message key="prompt.inicio"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td width="17%"> <html:text property="arcaNrInicio" styleClass="text" maxlength="6" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/> 
          </td>
          <td width="26%">&nbsp;</td>
          <td width="22%">&nbsp;</td>
          <td width="12%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="23%"> 
            <div align="right"><bean:message key="prompt.tamanho"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
          </td>
          <td width="17%"> <html:text property="arcaNrTamanho" styleClass="text" maxlength="6" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/> 
          </td>
          <td width="26%">&nbsp;</td>
          <td width="22%">&nbsp;</td>
          <td width="12%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="23%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
          <td width="12%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="23%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
          <td width="12%">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <br>
            <table width="93%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="4%" class="principalLstCab" height="1">&nbsp;</td>
                <td class="principalLstCab" width="42%"> &nbsp;<bean:message key="prompt.layout"/></td>
                <td class="principalLstCab" width="31%"><bean:message key="prompt.descricao"/></td>
                <td class="principalLstCab" width="7%"><bean:message key="prompt.seq"/></td>
                <td class="principalLstCab" width="7%"><bean:message key="prompt.tipo"/></td>
                <td class="principalLstCab" width="7%"><bean:message key="prompt.inicio"/></td>
                <td class="principalLstCab" width="8%"><bean:message key="prompt.tamanho"/></td>
              </tr>
              <tr valign="top"> 
                <td colspan="7" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form> 
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			/*
			document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.disabled= true;	
			document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.disabled= true;	
			document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.disabled= true;	
			document.administracaoCsAstbArqCargaArcaForm.arcaNrSequencia.disabled= true;	
			document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.disabled= true;	
			*/
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			/*
			document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.disabled= false;	
			document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.disabled= false;	
			*/
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPOSDOLAYOUT_INCLUSAO_CHAVE%>', parent.document.administracaoCsAstbArqCargaArcaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPOSDOLAYOUT_INCLUSAO_CHAVE%>')){
				desabilitaCamposArqCarga();
				document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.disabled= false;	
				document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.disabled= false;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPOSDOLAYOUT_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPOSDOLAYOUT_ALTERACAO_CHAVE%>', parent.document.administracaoCsAstbArqCargaArcaForm.imgGravar);	
				desabilitaCamposArqCarga();
				document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.disabled= false;	
				document.administracaoCsAstbArqCargaArcaForm.idCaloCdSequencial.disabled= false;	
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsAstbArqCargaArcaForm.idLaouCdSequencial.focus();}
	catch(e){}
</script>