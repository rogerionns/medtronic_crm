<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

function MontaLista(){
	lstArqCarga.location.href= "AdministracaoCsCatbDocumentoAnexoDoan.do?tela=administracaoLstCsCatbDocumentoAnexoDoan&acao=<%=Constantes.ACAO_VISUALIZAR%>&idDocuCdDocumento=" + document.administracaoCsCatbDocumentoAnexoDoanForm.idDocuCdDocumento.value;
}

function desabilitaCamposDocumentoAnexo(){
	cmbDocumento.document.administracaoCsCatbDocumentoAnexoDoanForm.idDocuCdDocumento.disabled= true;
	document.administracaoCsCatbDocumentoAnexoDoanForm.idAnmaCdAnexoMail.disabled= true;
	document.administracaoCsCatbDocumentoAnexoDoanForm.idGrdoCdGrupodocumento.disabled= true;
}

function acaoCmbGrupoDocumento(){
	cmbDocumento.document.location = "AdministracaoCsCatbDocumentoAnexoDoan.do?tela=cmbCsCdtbDocumentoDocu&acao=<%=Constantes.ACAO_VISUALIZAR%>&idGrdoCdGrupodocumento="+ administracaoCsCatbDocumentoAnexoDoanForm.idGrdoCdGrupodocumento.value;
}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista()">
<html:form styleId="administracaoCsCatbDocumentoAnexoDoanForm" action="/AdministracaoCsCatbDocumentoAnexoDoan.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="idDocuCdDocumento" />
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.grupoDocumento"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td>
		<html:select property="idGrdoCdGrupodocumento" styleClass="principalObjForm" style="width: 380px;" onchange="acaoCmbGrupoDocumento();"> 
			<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
			<logic:present name="csCdtbGrupoDocumentoGrdoVector">
				<html:options collection="csCdtbGrupoDocumentoGrdoVector" property="idGrdoCdGrupoDocumento" labelProperty="grdoDsGrupoDocumento"/>
			</logic:present>
		</html:select> 
   	</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.documento"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><iframe id="cmbDocumento" name="cmbDocumento" frameborder="0" height="20" style="width: 384px;" src="AdministracaoCsCatbDocumentoAnexoDoan.do?tela=cmbCsCdtbDocumentoDocu&acao=<%=Constantes.ACAO_VISUALIZAR%>&idGrdoCdGrupodocumento=<bean:write name='baseForm' property='idGrdoCdGrupodocumento'/>&idDocuCdDocumento=<bean:write name='baseForm' property='idDocuCdDocumento'/>"></iframe></td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.anexo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td> <html:select property="idAnmaCdAnexoMail" styleClass="principalObjForm" style="width: 380px;" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbAnexoMailAnmaVector" property="idAnmaCdAnexoMail" labelProperty="anmaDsAnexoMail"/> 
      </html:select> </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="2" height="220"> 
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="6%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="36%"> <bean:message key="prompt.anexo"/></td>
          <td class="principalLstCab" width="33%">&nbsp;</td>
          <td class="principalLstCab" width="11%">&nbsp;</td>
          <td class="principalLstCab" width="14%">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DOCUMENTOANEXO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCatbDocumentoAnexoDoanForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DOCUMENTOANEXO_INCLUSAO_CHAVE%>')){
				desabilitaCamposDocumentoAnexo();
			}else{
				document.administracaoCsCatbDocumentoAnexoDoanForm.idDocuCdDocumento.disabled= false;
				document.administracaoCsCatbDocumentoAnexoDoanForm.idAnmaCdAnexoMail.disabled= false;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DOCUMENTOANEXO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DOCUMENTOANEXO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCatbDocumentoAnexoDoanForm.imgGravar);	
				desabilitaCamposDocumentoAnexo();
			}
		</script>
</logic:equal>

</html>