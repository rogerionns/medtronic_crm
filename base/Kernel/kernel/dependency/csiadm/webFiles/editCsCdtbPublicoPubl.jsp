<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");
%>

<html>
<head>

<script language="JavaScript">	
	
	function inicio(){
		showError('<%=request.getAttribute("msgerro")%>');
		validarPermissao();
		setaChavePrimaria(administracaoCsCdtbPublicoPublForm.idPublCdPublico.value);

		var btn = document.getElementById("btnDefinirOperadores");
		var img = document.getElementById("imgDefinirOperadores");
		if(administracaoCsCdtbPublicoPublForm.idPublCdPublico.value == 0 || 
				administracaoCsCdtbPublicoPublForm.idPublCdPublico.value == "") {
			
			btn.className = "geralImgDisable";
			img.className = "geralImgDisable";
		} else {
			
			btn.className = "principalLabel";
			img.className = "";
		}
			
	}
	
	function validarPermissao(){
		<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_SUBCAMPANHA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_SUBCAMPANHA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbPublicoPublForm.imgGravar);	
				desabilitaCamposPublico();
			}	
		</logic:equal>
	}
	
	function desabilitaCamposPublico(){
		try{
			document.administracaoCsCdtbPublicoPublForm.publNrMaximoagend.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.publDsPublico.disabled= true;	
			document.administracaoCsCdtbPublicoPublForm.publDhInativo.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.idCampCdCampanha.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.idPesqCdPesquisa.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.publDhInicio.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.publNrTempoAtendimento.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.publNrMetaDiaria.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.publNrQtdePA.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.publNrPrioridade.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.publInAtivo.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.publInReceptivo.disabled= true;
			document.administracaoCsCdtbPublicoPublForm.publCdCorporativo.disabled= true;
			ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm.idResuCdResultado.disabled= true;
			ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm.chkFuncDispTodos.disabled= true;
			ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm.chkFuncAtivos.disabled= true;
			ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm.chkFuncSel.disabled= true;
			ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm.chkOperadorSelecionado.disabled= true;
			ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm.idMoenCdMotivoEncerra.disabled= true;
			ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm.publDhEncerramento.disabled= true;
		}
		catch(e){
			setTimeout("desabilitaCamposPublico();",200);
		}
	}

	function definirOperadores() {
		if(administracaoCsCdtbPublicoPublForm.idPublCdPublico.value == 0 || 
				administracaoCsCdtbPublicoPublForm.idPublCdPublico.value == "") {
			alert("<bean:message key="prompt.cliqueEmGravarAntesDeDefinirOperadoresDaSubcampanha" />");
			return false;
		}

		var opts = "width=920px,height=620px,top=80px,left=50px,help=0,location=0,menubar=0,resizable=0,scrollbars=0,status=0";
		var wndCriterios = window.open("/csiadm/campanha/criterios/abrir.do?id="+administracaoCsCdtbPublicoPublForm.idPublCdPublico.value + "&idEmprCdEmpresa=" + window.top.ifrmCmbEmpresa.administracaoEmpresaForm["idEmprCdEmpresa"].value, "criteriosPubl", opts, true);
		

	}

</script>

</head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language='javascript' src='webFiles/funcoes/TratarDados.js'></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>

<body class= "principalBgrPageIFRM" onload="inicio();" style="overflow: hidden;">
<html:form styleId="administracaoCsCdtbPublicoPublForm" action="/AdministracaoCsCdtbPublicoPubl.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="publDhEncerramento" />
	<html:hidden property="idMoenCdMotivoEncerra" />
	<html:hidden property="publTxObservacao" />

	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico1" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico2" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico3" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico4" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico5" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico6" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico7" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico8" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico9" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico10" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico11" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico12" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico13" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico14" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico15" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico16" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico17" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico18" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico19" />
	<html:hidden property="csCdtbLabelpublicoLapuVo.lapuDsPublico20" />
	
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel" style="margin-left: 5px;">
  <tr>
	<td class="principalLabel">
	  <div id="lstResultado" style="position:absolute; width:1px; height:1px; z-index:1; visibility: hidden"> 
	  </div>
	  <div id="lstOperadores" style="position:absolute; width:1px; height:1px; z-index:1; visibility: hidden"> 
	  </div>
	</td>
  </tr>	
  <tr>
    <td valign="top" height="320"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" height="365">
        <tr> 
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="360">
              <tr> 
                <td align="center"  width="796" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td valign="top" height="350"> 
                        <div id="Area" style="position:absolute; width:100%; height:350px; z-index:2; visibility: visible"> 

      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
		<td width="10%" class="principalLabel"><bean:message key="prompt.codigo"/> </td>
		<td width="10%" class="principalLabel"><bean:message key="prompt.codigoCorporativo"/> </td>								
		<td >
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td class="principalLabel"><bean:message key="prompt.subcampanha"/></td>
				<!--
				<td class="principalLabel" align="right"> Automatizar descricao<input type="checkbox" onclick="addDesc()" name="chkAtiva" checked>
				-->
			  </tr>
			</table>
		</td>
                      </tr>
                      <tr> 
                        <td width="10%"> 
		<html:text property="idPublCdPublico" styleClass="text" disabled="true" />
                        </td>
                        <td width="10%"> 
		<html:text property="publCdCorporativo" styleClass="text" maxlength="20"/>
                        </td>
                        
                        <td> 
		<html:text property="publDsPublico" styleClass="text" disabled="false" maxlength="60" />
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="3" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
			<td width="50%" class="principalLabel"><bean:message key="prompt.campanha"/> </td>
			<td width="50%" class="principalLabel"><bean:message key="prompt.pesquisa"/> </td>
                            </tr>
                            <tr> 
                              <td> 
			  <html:select property="idCampCdCampanha" styleClass="principalObjForm" onchange=""> 
			  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbCampanhaCampVector" property="idCampCdCampanha" labelProperty="campDsCampanha"/>
			  </html:select>
                              </td>
                              <td> 
			  <html:select property="idPesqCdPesquisa" styleClass="principalObjForm" > 
			  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbPesquisaPesqVector" property="idPesqCdPesquisa" labelProperty="pesqDsPesquisa"/>
			  </html:select>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="3" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLabel" width="25%" height="3"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="15%" class="principalLabel"><bean:message key="prompt.dataInicial" /></td>
                                    <td width="10">&nbsp;</td>
                                    <td width="10">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td width="20%" class="principalLabel"><bean:message key="prompt.tmaOperador" /></td>
                                    <td width="20%" class="principalLabel"><bean:message key="prompt.metaDiaria" /></td>
                                    <td width="20%" class="principalLabel">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.subcampanha.qtdePa" /></td>
                                    <td width="80%" class="principalLabel"><bean:message key="prompt.camapanha.prioridade" /></td>
                                    
                                  </tr>
                                  <tr> 
                                    <td width="15%"> <html:text property="publDhInicio" styleClass="text" maxlength="10" onblur="verificaData(this)" disabled="false" onkeydown="return validaDigito(this, event)"/></td>
                                    <td width="10"><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario"/>" width="16" height="15" onClick="show_calendar('administracaoCsCdtbPublicoPublForm.publDhInicio')" class="principalLstParMao" ></td>
                                    <td width="10">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td width="20%"> <html:text property="publNrTempoAtendimento" onblur="" styleClass="text" maxlength="5" onkeydown="return ValidaTipo(this, 'N', event);" disabled="false" /></td>
                                    <td width="20%"> <html:text property="publNrMetaDiaria" onblur="" styleClass="text" maxlength="6" onkeydown="return ValidaTipo(this, 'N', event);" disabled="false" /></td>
                                    <td width="20%"> <html:text property="publNrQtdePA" onblur="" styleClass="text" maxlength="6" onkeydown="return ValidaTipo(this, 'N', event);" disabled="false" /></td>
                                    <td width="80%"><html:text property="publNrPrioridade" onblur="" styleClass="text" maxlength="1" onkeydown="return ValidaTipo(this, 'N', event);" disabled="false" /></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      
                      <tr> 
                        <td colspan="3" class="principalLabel"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLabel" width="25%" height="3"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="15%" class="principalLabel"><bean:message key="prompt.csCsCdtbPublicoPubl.publNrTotalOriginal" /></td>
                                    <td width="15%" class="principalLabel"><bean:message key="prompt.csCsCdtbPublicoPubl.publNrTotalSelecao" /></td>
                                    <td width="18%" class="principalLabel"><bean:message key="prompt.csCsCdtbPublicoPubl.publNrMaximoagendamento" /></td>
                                    <td width="30%" class="principalLabel"><bean:message key="prompt.campanha.tpatendimento" /></td>  
                                    <td width="32%" class="principalLabel" colspan="2" rowspan="2">
                                    	<div id="btnDefinirOperadores" onclick="definirOperadores()" class="principalLabel" style="cursor: pointer; " title="<bean:message key="prompt.titleCriteriosOperadores" />">
											<img id="imgDefinirOperadores" src="/plusoft-resources/images/icones/Programa_Perfil.gif" align="absmiddle" /> <bean:message key="prompt.criteriosOperadores" />
										</div>	
                                    </td>
                                  </tr>
                                  <tr> 
                                    <td width="15%"> <html:text property="publNrTotalOriginal" onblur="" styleClass="text" maxlength="10" onkeypress="" disabled="true" /></td>
                                    <td width="15%"> <html:text property="publNrTotalSelecao" onblur="" styleClass="text" maxlength="10" onkeypress="" disabled="true" /></td>
                                    <td width="13%"> <html:text property="publNrMaximoagend" onblur="" styleClass="text" maxlength="2" onkeydown="return ValidaTipo(this, 'N', event);"/></td>
                                    <td width="30%">
                                    		<table>
                                    			<tr>
                                    				<td class="principalLabel"><bean:message key="prompt.campanha.ativo" /></td>
                                    				<td class="principalLabel"><html:checkbox value="true" property="publInAtivo"/>
                                    				<td class="principalLabel"><bean:message key="prompt.campanha.receptivo" /></td>
                                    				<td class="principalLabel"><html:checkbox value="true" property="publInReceptivo"/>
                                    			</tr>
                                    		</table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="3" class="principalLabel" height="170px"><iframe id="ifrmCsCdtbPublicoPublSubAbas" name="ifrmCsCdtbPublicoPublSubAbas" src="AdministracaoCsCdtbPublicoPubl.do?tela=<%=MAConstantes.TABLE_IFRM_SUBABAS%>&acao=<%=Constantes.ACAO_CONSULTAR%>&idPublCdPublico=<bean:write name="administracaoCsCdtbPublicoPublForm" property="idPublCdPublico"/>" width="100%" height="170px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                        </td>
                      </tr>
                      
		  <tr> 
			<td colspan="3" class="principalLabel" width="28%"> 
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
				  <td align="right" width="100%"> 
					  <html:checkbox value="true" property="publDhInativo"/>
				  </td>
				  <td class="principalLabel" width="29%"><bean:message key="prompt.inativo"/></td>
				</tr>
			  </table>
			</td>
		  </tr>
        </table>
      </div>
         </td>
       </tr>
     </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  
  
</table>
     
</html:form>
</body>

<script>
	document.administracaoCsCdtbPublicoPublForm.publNrPrioridade.value = <bean:write name="administracaoCsCdtbPublicoPublForm" property="publNrPrioridade" />
</script>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>

			document.administracaoCsCdtbPublicoPublForm.publDsPublico.disabled= true;	
			document.administracaoCsCdtbPublicoPublForm.publDhInativo.disabled= true;

			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_SUBCAMPANHA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbPublicoPublForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_SUBCAMPANHA_INCLUSAO_CHAVE%>')){
				desabilitaCamposPublico();
			}else{
				document.administracaoCsCdtbPublicoPublForm.publDsPublico.disabled= false;
				document.administracaoCsCdtbPublicoPublForm.idPublCdPublico.value= '';
				document.administracaoCsCdtbPublicoPublForm.publNrPrioridade.value= '';
				document.administracaoCsCdtbPublicoPublForm.publNrTotalOriginal.value= '';
				document.administracaoCsCdtbPublicoPublForm.publNrTotalSelecao.value= '';
				document.administracaoCsCdtbPublicoPublForm.idPublCdPublico.disabled= true;
		    	document.administracaoCsCdtbPublicoPublForm.publNrTempoAtendimento.value = '';
		    	document.administracaoCsCdtbPublicoPublForm.publNrMetaDiaria.value = '';
		    	document.administracaoCsCdtbPublicoPublForm.publNrQtdePA.value = '';
		    	document.administracaoCsCdtbPublicoPublForm.publNrMaximoagend.value= '';
		    }
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbPublicoPublForm.publDsPublico.focus();}
	catch(e){}
</script>