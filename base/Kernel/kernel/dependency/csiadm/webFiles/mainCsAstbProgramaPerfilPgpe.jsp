<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="Javascript">
function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	document.administracaoCsAstbProgramaPerfilPgpeForm.target = admIframe.name;
	document.administracaoCsAstbProgramaPerfilPgpeForm.acao.value ='filtrar';
	document.administracaoCsAstbProgramaPerfilPgpeForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsAstbProgramaPerfilPgpeForm.filtro.value = '';
}

function submeteFormIncluir() {
	editIframe.document.administracaoCsAstbProgramaPerfilPgpeForm.target = editIframe.name;
	editIframe.document.administracaoCsAstbProgramaPerfilPgpeForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_PROGRAMAPERFIL_PGPE %>';
	editIframe.document.administracaoCsAstbProgramaPerfilPgpeForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsAstbProgramaPerfilPgpeForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo1, codigo2){
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idTppgCdTipoPrograma.value = codigo1;
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idPerfCdPerfil.value = codigo2;
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_PROGRAMAPERFIL_PGPE %>';
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.target = editIframe.name;
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){
	if (window.document.all.item("Manifestacao").style.visibility == "hidden") {
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	} else {
		if (tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idTppgCdTipoPrograma.value == "") {
			alert("<bean:message key="prompt.Selecione_um_Programa"/>.");
			tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idTppgCdTipoPrograma.focus();
			return false;
		}
		if (tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idPerfCdPerfil.value == "") {
			alert("<bean:message key="prompt.Selecione_um_Perfil"/>.");
			tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idPerfCdPerfil.focus();
			return false;
		}
		tab.document.administracaoCsAstbProgramaPerfilPgpeForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_PROGRAMAPERFIL_PGPE %>';
		tab.document.administracaoCsAstbProgramaPerfilPgpeForm.target = admIframe.name;
		disableEnable(tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idTppgCdTipoPrograma, false);
		tab.document.administracaoCsAstbProgramaPerfilPgpeForm.submit();
		disableEnable(tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idTppgCdTipoPrograma, true);
		
		setTimeout("editIframe.lstProgramaPerfil.location.reload()", 2500);
		tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idTppgCdTipoPrograma.disabled = false;
		tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idPerfCdPerfil.value = "";
	}
}

function submeteExcluir(codigo1,codigo2) {
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idTppgCdTipoPrograma.value = codigo1;
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idPerfCdPerfil.value = codigo2;
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_PROGRAMAPERFIL_PGPE %>';
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.target = editIframe.name;
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsAstbProgramaPerfilPgpeForm.submit();
}

function setConfirm(confirmacao){
	if (confirmacao == true){
		editIframe.document.administracaoCsAstbProgramaPerfilPgpeForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_PROGRAMAPERFIL_PGPE %>';
		editIframe.document.administracaoCsAstbProgramaPerfilPgpeForm.target = admIframe.name;
		editIframe.document.administracaoCsAstbProgramaPerfilPgpeForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsAstbProgramaPerfilPgpeForm.idTppgCdTipoPrograma, false);
		editIframe.document.administracaoCsAstbProgramaPerfilPgpeForm.submit();
		tab.document.administracaoCsAstbProgramaPerfilPgpeForm.idPerfCdPerfil.value = "";
		tab.document.administracaoCsAstbProgramaPerfilPgpeForm.acao.value = '<%=Constantes.ACAO_INCLUIR %>';
	} else {
		cancel();	
	}
}

function cancel(){
	editIframe.location = 'AdministracaoCsAstbProgramaPerfilPgpe.do?tela=editCsAstbProgramaPerfilPgpe&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}

function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}

// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsAstbProgramaPerfilPgpeForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsAstbProgramaPerfilPgpeForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_PROGRAMAPERFIL_PGPE %>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			break;
		case editIframe:
			tab.document.administracaoCsAstbProgramaPerfilPgpeForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_PROGRAMAPERFIL_PGPE %>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsAstbProgramaPerfilPgpeForm"	action="/AdministracaoCsAstbProgramaPerfilPgpe.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td width="100%" colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
					<tr>
						<td class="principalQuadroPst" height="100%">&nbsp;</td>
						<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
						<td height="100%" width="4">
							<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td height="254">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalPstQuadroLinkVazio">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalPstQuadroLinkSelecionado" id="tdDestinatario" name="tdDestinatario" onclick="AtivarPasta(admIframe);">
													<bean:message key="prompt.procurar"/>
												</td>
												<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao" name="tdManifestacao" onClick="AtivarPasta(editIframe);">
													<bean:message key="prompt.programaPerfil"/>
												</td>
											</tr>
										</table>
									</td>
									<td width="4">
										<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
									</td>
								</tr>
							</table>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
					                <td valign="top" class="principalBgrQuadro" height="400">
					                	<br>
					                    <div name="Manifestacao" id="Manifestacao" style="position: absolute; width: 97%; height: 225px; z-index: 6; visibility: hidden">
						                    <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
												<tr>
							                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
							                        	<iframe id="editIframe" name="editIframe"
														src="AdministracaoCsAstbProgramaPerfilPgpe.do?tela=editCsAstbProgramaPerfilPgpe&acao=incluir"
														width="100%" height="100%" scrolling="Default" frameborder="0"
														marginwidth="0" marginheight="0"></iframe>
													</td>
												</tr>
											</table>
										</div>
						                <div name="Destinatario" id="Destinatario" style="position: absolute; width: 99%; height: 199px; z-index: 2; visibility: visible">
						                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
												<tr>
											        <td class="principalLabel" width="8%" colspan="2"><bean:message key="prompt.nomeCampanha"/></td>
												</tr>
												<tr>
											        <td width="65%"> 
						                        		<table border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td width="25%">
																	<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
																</td>
																<td width="05%">
																	&nbsp;<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="filtrar()">
																</td>
															</tr>	
														</table>
													</td>
												</tr>
												<tr>
											        <td class="principalLabel" width="8%">&nbsp;</td>
							                        <td class="principalLabel" width="49%">&nbsp;</td>
												</tr>
											</table>
											<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
												<tr>
							                        <td class="principalLstCab" colspan="3" width="49%">&nbsp;<bean:message key="prompt.campanha"/></td>
												</tr>
												<tr valign="top">
							                        <td colspan="3" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
							                        	<iframe id=admIframe name="admIframe"
														src="AdministracaoCsAstbProgramaPerfilPgpe.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
														width="100%" height="98%" scrolling="Default" frameborder="0"
														marginwidth="0" marginheight="0"></iframe>
													</td>
												</tr>
											</table>
										</div>
					                </td>
									<td width="4" height="400"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
								</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
					</tr>
				</table>
				<table border="0" cellspacing="0" cellpadding="4" align="right">
					<tr align="center">
						<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif"	width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
						</td>
						<td width="20">
							<img src="webFiles/images/botoes/gravar.gif"	width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
						</td>
						<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
						</td>
					</tr>
				</table>
				<table align="center" >
					<tr>
						<td>
							<label id="error">
							</label>
						</td>
					</tr>
				</table>
			</td>
			<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		</tr>
		<tr>
			<td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
			<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		</tr>
	</table>
</html:form>
<script language="JavaScript">
	var tab = admIframe ;
</script>
</body>
</html>
