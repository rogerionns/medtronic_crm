<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>
function inicio() {
	try{
		//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 
		if (document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.length == 2){
			document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1[1].selected = true;
		}
	}catch(e){}
	verifica();
}	

	//Atualiza o combo de manifesta��o
	function atualizaManifestacao(){
		try{
			document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
			document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_MANIFESTACAO %>';
			document.administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbManifestacao.name;

			verificaHabilitaComboSubmit();
			document.administracaoCsAstbProdutoManifPrmaForm.submit();
		}
		catch(x){
			//Controle para evitar dar erro na tela por nao ter carregado os objetos dos outros iframes
			setTimeout("atualizaManifestacao();", 5000);
		}
	}

function desabilitaLinha() {
	if (parent.ifrmCmbLinhaLinh.document.readyState != 'complete') {
		t = setTimeout ("desabilitaLinha()",100);
	}
	else {
		parent.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.disabled = true;
		try { clearTimeout(t); } catch(e){}
	}
}

function verificaHabilitaComboSubmit() {
	if (administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1==undefined) { return false; }
	
	if(!administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled) 
		return false;

	administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled = false;
	setTimeout('administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled = true;', 500);
}

var nAtualizaVariedade = 0;
function atualizaVariedade() {
	try{
		administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
		administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_VARIEDADE %>';
		administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbLinhaProdutoVariedade.name;

		//Chamado: 90205 - 29/08/2013
		if(parent.administracaoCsAstbProdutoManifPrmaForm.optProduto.checked)
		{
			document.administracaoCsAstbProdutoManifPrmaForm.tipoLinha.value = "S";
		}
		else
		{
			document.administracaoCsAstbProdutoManifPrmaForm.tipoLinha.value = "N";
		}
		
		verificaHabilitaComboSubmit();
		administracaoCsAstbProdutoManifPrmaForm.submit();
		
		nAtualizaVariedade = 0;
	}
	catch(x){
		if(nAtualizaVariedade > 30){
			alert(x);
		}
		else{
			nAtualizaVariedade++;
			setTimeout("atualizaVariedade();", 500);
		}
	}
	
}

function verifica() {
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		atualizaVariedade();
	<%}else{%>
		atualizaManifestacao();
	<%}%>
	parent.validaChkCombo();
}


	function mostraCampoBuscaProd(){
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = "<%=Constantes.ACAO_CONSULTAR%>";
		document.administracaoCsAstbProdutoManifPrmaForm.tela.value = "<%= MAConstantes.TELA_CMB_PRMA_PRODUTO %>";
		administracaoCsAstbProdutoManifPrmaForm.target = this.name;

		verificaHabilitaComboSubmit();
		document.administracaoCsAstbProdutoManifPrmaForm.submit();
	}
	
	function buscarProduto(){

		if (document.administracaoCsAstbProdutoManifPrmaForm['asn1DsAssuntoNivel1'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}

		document.administracaoCsAstbProdutoManifPrmaForm.tela.value = "<%= MAConstantes.TELA_CMB_PRMA_PRODUTO %>";
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = "<%=MAConstantes.ACAO_POPULACOMBO%>";
		administracaoCsAstbProdutoManifPrmaForm.target = this.name;
		
		verificaHabilitaComboSubmit();
		document.administracaoCsAstbProdutoManifPrmaForm.submit();

	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}

</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); inicio(); ">
<html:form action="/AdministracaoCsAstbProdutoManifPrma.do" styleId="administracaoCsAstbProdutoManifPrmaForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="idLinhCdLinha" />	
	<html:hidden property="idAsn2CdAssuntoNivel2" />		
	<html:hidden property="idMatpCdManifTipo" />
	<html:hidden property="idTpmaCdTpManifestacao" />
	<html:hidden property="idGrmaCdGrupoManifestacao" />
	<html:hidden property="tipoLinha" />
	
	<logic:notEqual name="administracaoCsAstbProdutoManifPrmaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="95%">
					<html:select property="idAsn1CdAssuntoNivel1" styleClass="principalObjForm" onchange="verifica();">
						<html:option value="0"> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
						
						<logic:present name="combo">
						<html:options collection="combo" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" labelProperty="csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1"/>
						</logic:present> 
					</html:select>                              
			 	</td>
			 	<td width="5%" valign="middle">
			 		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
			 	</td>
			</tr>
		</table>
	
		<logic:present name="combo">
			<logic:iterate name="combo" id="csCdtbAssuntoNivel1Asn1Vector">
				<input type="hidden" name='txtAsn1<bean:write name="csCdtbAssuntoNivel1Asn1Vector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>' value='<bean:write name="csCdtbAssuntoNivel1Asn1Vector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>'>
			</logic:iterate>	  
		</logic:present>
	</logic:notEqual>
	  
	<logic:equal name="administracaoCsAstbProdutoManifPrmaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
			<tr>
				<td width="95%">
					<html:text property="asn1DsAssuntoNivel1" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
				</td>
				<td width="5%" valign="middle">
					<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
				</td>
			</tr> 	
		</table>
	</logic:equal>
</html:form>

	<script>
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			if (parent.getFlagProduto() == 1){			
				desabilitaLinha();
			}
		<%}%>
	</script>

</body>
</html>
<logic:equal name="administracaoCsAstbProdutoManifPrmaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	<script>
		document.administracaoCsAstbProdutoManifPrmaForm['asn1DsAssuntoNivel1'].focus();
	</script>
</logic:equal>