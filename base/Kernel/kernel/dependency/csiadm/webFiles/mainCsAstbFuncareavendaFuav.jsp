<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	administracaoCsAstbFuncareavendaFuavForm.target = admIframe.name;
	administracaoCsAstbFuncareavendaFuavForm.acao.value ='filtrar';
	administracaoCsAstbFuncareavendaFuavForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	administracaoCsAstbFuncareavendaFuavForm.filtro.value = '';
}

function submeteFormIncluir() {
	editIframe.administracaoCsAstbFuncareavendaFuavForm.target = editIframe.name;
	editIframe.administracaoCsAstbFuncareavendaFuavForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_FUNCAREAVENDA_FUAV%>';
	editIframe.administracaoCsAstbFuncareavendaFuavForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.administracaoCsAstbFuncareavendaFuavForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo1,codigo2){
	tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value = codigo1;
	tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].value = codigo2;
	tab.document.forms[0].tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_FUNCAREAVENDA_FUAV%>';
	tab.document.forms[0].target = editIframe.name;
	tab.document.forms[0].acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.forms[0].submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){
	/*
	if(tab.document.forms[0].tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_FUNCAREAVENDA_FUAV%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.forms[0].tela.value == '<%=MAConstantes.TELA_EDIT_CS_ASTB_FUNCAREAVENDA_FUAV%>'){
	*/

	if(window.document.all.item("Manifestacao").style.display == "none"){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else{	
		if (tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value == "" ) {
			alert('<bean:message key="prompt.SelecionarEquipeDeVendas"/>');
			tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].focus();
			return false;
		}
		if (tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].value == "") {
			alert('<bean:message key="prompt.selecionar_funcionario_vendedor"/>');
			tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].focus();
			return false;
		}
		
		/*if(tab.lstArqCarga.possuiResponsavel && tab.document.forms[0]['csAstbFuncareavendaFuavVo.fuavInResponsavel'].checked){
			alert('<bean:message key="prompt.alert.estaAreaJaPossuiUmFuncionarioResponsavel"/>');
			return false;
		}*/
		
		tab.document.forms[0].tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_FUNCAREAVENDA_FUAV%>';
		tab.document.forms[0].acao.value = '<%=Constantes.ACAO_INCLUIR %>';
		tab.document.forms[0].target = admIframe.name;
		disableEnable(tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'], false);
		
		tab.document.forms[0]['csAstbFuncareavendaFuavVo.fuavInEditar'].disabled = false;
		tab.document.forms[0]['csAstbFuncareavendaFuavVo.fuavInVer'].disabled = false;
		
		tab.document.forms[0].submit();
		//disableEnable(tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'], true);
		//cancel();

		setTimeout("editIframe.lstArqCarga.location.reload()", 2500);
		//tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value = "";
		tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].value = "";
		
	}
}

var removerCombo = false;
function submeteExcluir(codigo1,codigo2, nomeFunc) {
	tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value = codigo1;
	tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].value = codigo2;
	removerCombo = false;
	
	if(tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].selectedIndex < 0){
		var op = new Option();
		op.text = nomeFunc;
		op.value = codigo2;
		
		tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].add(op);
		tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].value = codigo2;
		
		removerCombo = true;
	}

	confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
	setConfirm(confirmacao);
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		//editIframe.administracaoCsAstbFuncareavendaFuavForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_FUNCAREAVENDA_FUAV%>';
		//editIframe.administracaoCsAstbFuncareavendaFuavForm.target = editIframe.lstArqCarga.name;
		editIframe.administracaoCsAstbFuncareavendaFuavForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_FUNCAREAVENDA_FUAV%>';
		editIframe.administracaoCsAstbFuncareavendaFuavForm.target = admIframe.name;
		editIframe.administracaoCsAstbFuncareavendaFuavForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		editIframe.administracaoCsAstbFuncareavendaFuavForm.submit();

		setTimeout("editIframe.lstArqCarga.location.reload()", 2000);
		
	}else{
/*		tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].value = "";
		tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].remove(tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].selectedIndex);*/	
	}
	
	if(removerCombo){
		setTimeout("editIframe.administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].remove(tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].selectedIndex);", 200);
		setTimeout("editIframe.administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].value = '';", 500);
	}
	else{
		tab.document.forms[0]['csAstbFuncareavendaFuavVo.csCdtbFuncVendedorFuncVo.idFuncCdFuncionario'].value = "";
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsAstbFuncareavendaFuav.do?tela=editCsAstbFuncareavendaFuav&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsAstbFuncareavendaFuavForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.forms[0].tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_FUNCAREAVENDA_FUAV%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			break;
		case editIframe:
			tab.document.forms[0].tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_FUNCAREAVENDA_FUAV%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsAstbFuncareavendaFuavForm"	action="/AdministracaoCsAstbFuncareavendaFuav.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="100%" width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/><!-- ## --></td>
									<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.EquipeDeVendasXVendedor"/><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="width: 99%; height: 450px;display: none"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0" height="100%"
								align="center">
								<tr>
									
                        <td height="100%"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsAstbFuncareavendaFuav.do?tela=editCsAstbFuncareavendaFuav&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="width: 99%; height: 450px;display: block"> 
                    		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									
                        <td class="principalLabel" width="8%" colspan="2"><bean:message key="prompt.EquipeDeVendas"/></td>

								</tr>

								<tr>
									
                        <td width="65%"> 
                          <table border="0" cellspacing="0" cellpadding="0">
									<tr>
									<td width="25%">
									<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
									</td>
									<td width="05%">
									&nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
									</td>
									</tr>	
									</table>
									</td>
								</tr>
								<tr>
									
                        <td class="principalLabel" width="8%">&nbsp;</td>
									
                        <td class="principalLabel" width="49%">&nbsp;</td>
								</tr>
							</table>
							<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" height="100%">
								<tr>
									
                        <td class="principalLstCab" width="8%">&nbsp;&nbsp;</td>
                        <td class="principalLstCab" width="51%" align="left"><bean:message key="prompt.EquipeDeVendas"/> </td>
                        <td class="principalLstCab" width="*">&nbsp;&nbsp;</td>
									
								</tr>
								
								<tr valign="top">
									
                        <td colspan="3" height="100%"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsAstbFuncareavendaFuav.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                </td>
							<td width="4" height="100%" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="1" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">

	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_FUNCIONARIOVENDEDOR_INCLUSAO_CHAVE%>', administracaoCsAstbFuncareavendaFuavForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_FUNCIONARIOVENDEDOR_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_FUNCIONARIOVENDEDOR_ALTERACAO_CHAVE%>')){
			administracaoCsAstbFuncareavendaFuavForm.imgGravar.disabled=true;
			administracaoCsAstbFuncareavendaFuavForm.imgGravar.className = 'geralImgDisable';
			administracaoCsAstbFuncareavendaFuavForm.imgGravar.title='';
	}
/*
	<logic:equal name="baseForm" property="tela" value="<%= MAConstantes.TELA_MAIN_CS_ASTB_FUNCAREAVENDA_FUAV %>">
		<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_CANCELAR %>">
			//Acabou de remover o registro do banco de dados
			tab = admIframe;
			submeteFormEdit(<bean:write name="administracaoCsAstbFuncareavendaFuavForm" property="csAstbFuncareavendaFuavVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />, 0);
		</logic:equal>
	</logic:equal>
	*/
</script>


</body>
</html>