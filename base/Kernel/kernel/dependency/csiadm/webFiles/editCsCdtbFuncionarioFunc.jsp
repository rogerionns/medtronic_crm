<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	

long idIdioma=0;
if (session != null && session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
	idIdioma = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();
}

boolean FEATURE_SAAS = "S".equalsIgnoreCase(Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SAAS, request));
%>


<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>

<% // Chamado 80983 - 18/11/2013 - Jaider Alba %>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script> 

<script>
	function desabilitaCamposFuncionario(){
		document.administracaoCsCdtbFuncionarioFuncForm.idAreaCdArea.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.idNiveCdNivelAcesso.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.idFuncCdSuperior.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.funcDsFax.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.funcDsMail.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.funcDsLoginname.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.funcInDestinatario.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.funcDhInativo.disabled= true;			
		document.administracaoCsCdtbFuncionarioFuncForm.funcNmFuncionario.disabled= true;	
		document.administracaoCsCdtbFuncionarioFuncForm.funcDhInativo.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.funcDsCodigoMsd.disabled= true;	
		document.administracaoCsCdtbFuncionarioFuncForm.funcDsCelular.disabled= true;	
		document.administracaoCsCdtbFuncionarioFuncForm.funcDsFuncao.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.funcNrMaxchat.disabled= true;	
		document.administracaoCsCdtbFuncionarioFuncForm.funcInDestFoup.disabled= true;
		document.administracaoCsCdtbFuncionarioFuncForm.funcInDestinatarioEmail.disabled= true;
	}	

	function gerarSenha(){
		showModalDialog('AdministracaoCsCdtbFuncionarioFunc.do?acao=visualizar&tela=ifrmSenhaGerada',window,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:155px,dialogTop:200px,dialogLeft:450px');
	}
	
	function revogarSenha() {
		if (confirm('<bean:message key="prompt.confirmaRevogacaoSenhaDesteFuncionario" />')) { 
			document.administracaoCsCdtbFuncionarioFuncForm.funcInRandomica.value=true;
		}
	}

	function frmLoad(){
		if(administracaoCsCdtbFuncionarioFuncForm.inBloqueado.value=="true"){
			bloqueado.style.visibility="visible";
		}
		
	}
	
	function inicio(){
		frmLoad();
		setaAssociacaoMultiEmpresa();
		verificaClickDestinatarioEmail();
		
		//Chamado 76061 - Vinicius - Chama a fun��o para validar se � area resolvedora para "esconder" os campos login e password
		verificaAreaResolvedora();

		<%if (FEATURE_SAAS){%>
			document.forms[0].funcDsLoginname.disabled = true;
		<%}%>
	}

	function verificaAreaResolvedora(){
		//if(document.administracaoCsCdtbFuncionarioFuncForm.idNiveCdNivelAcesso.value == <%=MAConstantes.ID_NIVE_CD_NIVELACESSO_AREARESOLVEDORA%>){
		if(document.administracaoCsCdtbFuncionarioFuncForm.idNiveCdNivelAcesso.value == <%=MAConstantes.ID_NIVE_CD_NIVELACESSO_AREARESOLVEDORA%> || document.administracaoCsCdtbFuncionarioFuncForm.idNiveCdNivelAcesso.value == <%=MAConstantes.ID_NIVE_CD_NIVELACESSO_AREAVENDEDOR%>){		
			document.getElementById("trLogin").style.display="none";
			//document.getElementById("trSenha").style.display="none";
			//document.administracaoCsCdtbFuncionarioFuncForm.funcDsLoginname.disabled= true;
			//document.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.disabled= true;
		}else{
			//document.administracaoCsCdtbFuncionarioFuncForm.funcDsLoginname.disabled= false;
			//document.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.disabled= false;
			document.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.disabled= false;
			document.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.readOnly= true;
			document.getElementById("trLogin").style.display="block";
			//document.getElementById("trSenha").style.display="block";
		}
	}

	function verificaClickDestinatarioEmail() {
		if(document.forms[0].funcInDestinatarioEmail.checked) {
			document.forms[0].funcDsLoginname.value = '';
			document.forms[0].funcDsPassword.value = '';
			document.forms[0].funcDsPassword.disabled = true;
			document.getElementById("trLogin").style.display="none";
			
			$("#tdModulos").hide();
			
		} else {
			document.forms[0].funcDsPassword.disabled = false;
			document.getElementById("trLogin").style.display="block";
			
			if(parent.administracaoCsCdtbFuncionarioFuncForm.licencaNominal.value == "S"){
				$("#tdModulos").show();
			}
		}
	}
	
	function validaEmail(obj){
		if(obj.value != ""){
			var cEmail = obj.value;
			obj.value = trim(cEmail.toLowerCase());
	
			if(window.top.jQuery) {
				cEmail = window.top.jQuery.trim(cEmail);
			} else if (window.dialogArguments) {
				cEmail = window.dialogArguments.top.jQuery.trim(cEmail);
			}
			
			if (cEmail.search(/\S/) != -1) {
				//Chamado: 93047 - 05/02/2014 - Carlos Nunes
				regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9._-]{1,}\.[A-Za-z0-9]{2,}/
				//regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{1,}\.[A-Za-z0-9]{2,}/
				if (cEmail.length < 7 || cEmail.search(regExp) == -1){
					alert ('<bean:message key="prompt.alert.email.correto" />');
					obj.focus();
				    return false;
				}						
			}
			num1 = cEmail.indexOf("@");
			num2 = cEmail.lastIndexOf("@");
			if (num1 != num2){
			    alert ('<bean:message key="prompt.alert.email.correto" />');
			    obj.focus();
				return false;
			}
		}
	}

	// Se for feature SAAS o login � sempre igual ao e-mail.
	function copiaEMail() {
		<% if (FEATURE_SAAS) { %>
		if(!document.forms[0].funcInDestinatarioEmail.checked) {
			document.forms[0].funcDsLoginname.value = document.forms[0].funcDsMail.value;
		} else {
			document.forms[0].funcDsLoginname.value = '';
		}
		<% } %>
	}

	function abrirModalModulos() {
		var url = 'AdministracaoCsCdtbFuncionarioFunc.do?acao=visualizar&tela=ifrmLstModulos&idModuCdModulo=' + document.forms[0].idModuCdModulo.value;
		showModalDialog(url, window, 'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:340px');
	}

	function buscaAreaEmpresa(){

		var cTxt = '';

		cTxt += '<select id="idAreaCdArea" name="idAreaCdArea" class="principalObjForm" >';
		cTxt += '	<option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </option>';
		cTxt += '	<option value="0"> Carregando dados... </option>';
		cTxt += '</select>';

		document.getElementById("tdArea").innerHTML = cTxt;

		var ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/CS_CDTB_AREA_AREA.xml", "ConsultaBanco.do",true);	
		ajax.addField("id_idio_cd_idioma",'<%=idIdioma%>');
		ajax.addField("id_empr_cd_empresa",document.administracaoCsCdtbFuncionarioFuncForm.idEmprCdEmpresa.value);

		ajax.executarConsulta(montaAreaEmpresa, false, true);
	}

	function montaAreaEmpresa(ajax){
		if(ajax.getMessage() != ''){
			alert(ajax.getMessage());
			return;
		}

		rs = ajax.getRecordset();
		var cTxt = '';

		cTxt += '<select id="idAreaCdArea" name="idAreaCdArea" class="principalObjForm" >';
		cTxt += '	<option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </option>';
		while(rs.next()){
			cTxt += '	<option value="' + rs.get("id_area_cd_area") + '">' + rs.get("area_ds_area") + '</option>';
		}
		cTxt += '</select>';

		document.getElementById("tdArea").innerHTML = cTxt;
	}


	function idLodoCdLogindomain_onChange(o) {
		if(o.selectedIndex==0) { 
			document.getElementById("imgTesteDominio").onclick=function() {};
			document.getElementById("imgTesteDominio").className="geralImgDisable";
		} else {
			document.getElementById("imgTesteDominio").className="geralCursoHand";
			document.getElementById("imgTesteDominio").onclick=testeDominio;
		}
	}

	function testeDominio() {
		showModalOpen("TelaTesteLoginDominio.do", window, "help:no;scroll:no;Status:NO;dialogWidth:320px;dialogHeight:255px,dialogTop:300px,dialogLeft:250px");
	}
	
	<% // Chamado 80983 - 18/11/2013 - Jaider Alba %>
	var onClickSalvar;
	var onClickSalvarManter;
	function verificaSuperior(inativo){
		
		var btGravar = parent.document.getElementById('imgGravar');
		var btGravarManter = parent.document.getElementById('imgGravarManter');
		
		if(inativo.checked 
				&& document.forms[0].idFuncCdFuncionario.value != '' 
				&& parseInt(document.forms[0].idFuncCdFuncionario.value) > 0){
		
			document.body.style.cursor = 'wait';
			
			btGravar.disabled = true;
			btGravarManter.disabled = true;
			btGravar.className = 'geralImgDisable';
			btGravarManter.className = 'geralImgDisable';
			onClickSalvar = btGravar.onclick;
			onClickSalvarManter = btGravarManter.onclick;
			btGravar.onclick = function(){};
			btGravarManter.onclick = function(){};

			try{		
				var dadospost = { 
						idFuncCdFuncionario : document.forms[0].idFuncCdFuncionario.value
					};
				
				$.post("/csiadm/GenericAdmFindFuncBySuperior.do", dadospost, function(ret) {	
					
					document.body.style.cursor = 'default';
					
					btGravar.disabled = false;
					btGravarManter.disabled = false;					
					btGravar.className = 'geralCursoHand';
					btGravarManter.className = 'geralCursoHand';					
					btGravar.onclick = onClickSalvar;
					btGravarManter.onclick = onClickSalvarManter;
					
					if(ret.msgerro!=undefined) {
						alert("<bean:message key='prompt.erro_carregar_funcionarios' /> "+ret.msgerro);
						return false;						
					} else if(ret.superior!=null && ret.superior!=undefined && ret.superior == 'true') {
						
						if(confirm("<bean:message key='prompt.designar_superior' />")) {
							var idFuncCdFuncionario = document.forms[0].idFuncCdFuncionario.value;
							// AdequacaoSafari - 29/11/2013 - Jaider Alba
							var superiorResult = showModalDialog("/csiadm/AdministracaoCsCdtbFuncionarioFunc.do?acao=<%=MAConstantes.ACAO_CARREGAR_POPUP_SUPERIORES%>&idFuncCdFuncionario="+idFuncCdFuncionario, "", "font-family:Verdana;font-size:12;dialogWidth:410px;dialogHeight:170px");
							
							if(superiorResult!=undefined && superiorResult!='' && superiorResult!=false){
								document.forms[0].idFuncCdSuperiorRepassa.value = superiorResult;
							}
						}
						else{
							document.forms[0].idFuncCdSuperiorRepassa.value = 0;
						}
					}
					
				}, "json");
			}
			catch(x){		
				alert("Erro em findFuncBySupervisor()"+ x.description);		
			}
		}
		else{
			document.forms[0].idFuncCdSuperiorRepassa.value = 0;
		}
		
	}

	//Chamado 104883 - 09/11/2015 Victor Godinho
	function resetarSenha() {
		document.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.readOnly= false;
		document.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.value = "";
		document.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.focus();
	}
</script>
</head>

<body class="principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbFuncionarioFuncForm" action="/AdministracaoCsCdtbFuncionarioFunc.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<html:hidden property="funcDsLoginnameAnterior" />
	<html:hidden property="funcDsCtiAgenteAnterior" />
	<html:hidden property="funcInRandomica"/> 
	<html:hidden property="inBloqueado"/>
	<html:hidden property="idModuCdModulo"/>
	<input type="hidden" name="idNiveCdNivelAcessoBanco" value="<bean:write name="administracaoCsCdtbFuncionarioFuncForm" property="idNiveCdNivelAcesso" />"/>
	<input type="hidden" name="limparSessao" value="false"></input>
	<!-- Chamado: 89956 - 01/08/2013 - Carlos Nunes -->
	<html:hidden property="manterEdicao" /> 
	
	<% // Chamado 80983 - 18/11/2013 - Jaider Alba %>
	<html:hidden property="idFuncCdSuperiorRepassa" />
	
	<br>
	 
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="43%" colspan="4"> <html:text property="idFuncCdFuncionario" styleClass="text" disabled="true" /> 
    </td>
    <td colspan="2">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
	    <tr>
		    <td width="43%" align="right" class="principalLabel"><bean:message key="prompt.codigoMSD"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    <td width="43%"> <html:text property="funcDsCodigoMsd" styleClass="text" maxlength="20"/> 
		    <td width="14%">&nbsp;</td>
	    </tr>
	    </table>
	 </td>
  	</tr>
  	<!-- Combo de idioma -->
  	<tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.idioma"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="43%" colspan="4">
    	<html:select property="idIdioCdIdioma" styleClass="text">
    		<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option>
    		<html:options collection="csCdtbIdiomaIdioVector" property="field(id_idio_cd_idioma)" labelProperty="field(idio_ds_idioma)"/> 
    	</html:select>    
    </td>
    <td colspan="2">&nbsp;</td>
  </tr>
	<tr> 
		<td width="20%" align="right" class="principalLabel"><bean:message key="prompt.empresa.area"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td colspan="4">
			<html:select property="idEmprCdEmpresa" styleClass="principalObjForm" onchange="buscaAreaEmpresa()"> 
				<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
				<html:options collection="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" labelProperty="emprDsEmpresa"/> 
			</html:select>     
		</td>
	    <td colspan="2">&nbsp;</td>
	</tr>
	<tr> 
		<td width="20%" align="right" class="principalLabel"><bean:message key="prompt.area"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td id="tdArea" name="tdArea" colspan="4">
			<html:select property="idAreaCdArea" styleClass="principalObjForm" > 
				<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
				<html:options collection="csCdtbAreaAreaVector" property="idAreaCdArea" labelProperty="areaDsArea"/> 
			</html:select>     
		</td>
		<td colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="2%">&nbsp;</td>
					<td width="4%" align="left" class="principalLabel"><html:checkbox value="true" property="funcDhInativo" onclick="verificaSuperior(this)" /> </td>
					<td width="40%" align="left" class="principalLabel">&nbsp;<bean:message key="prompt.inativo"/></td>
				</tr>
			</table>
		</td>
	</tr>
  
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.tipo_de_funcionario"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="4">
       	<html:select property="idNiveCdNivelAcesso" styleId="idNiveCdNivelAcesso" styleClass="principalObjForm" onchange="verificaAreaResolvedora()"> 
            <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
            <html:options collection="csCdtbNivelAcessoNiveVector" property="idNiveCdNivelAcesso" labelProperty="niveDsNivelacesso"/> 
        </html:select>     
    </td>
    <td colspan="2">
    
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
        	<td width="2%">&nbsp;</td>
        	<td width="4%" align="left" class="principalLabel"><html:checkbox value="true" property="funcInDestinatario"/> </td>
        	<td width="40%" align="left" class="principalLabel">&nbsp;<bean:message key="prompt.destinatario"/></td>
        </tr>
      </table>
    
    </td>
  </tr>
  <tr>
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.superior"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="4">
       	<html:select property="idFuncCdSuperior" styleClass="principalObjForm" > 
            <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
            <html:options collection="CsCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/> 
        </html:select> 
    </td>
    <td colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
        	<td width="2%">&nbsp;</td>
        	<td width="4%" align="left" class="principalLabel"><html:checkbox value="true" property="funcInDestFoup"/> </td>
        	<td width="40%" align="left" class="principalLabel">&nbsp;<bean:message key="prompt.destinatarioFollowup"/></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.funcionario"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="4"> <html:text property="funcNmFuncionario" style="width:324px" styleClass="text" maxlength="100" /> 
    </td>
    
    	<td colspan="2" class="nominal">
    		<!-- Jonathan Costa | FullScreen 12/2013 -->
	      <table width="100%" border="0" cellspacing="0" cellpadding="0" >
	        <tr> 
	        	<td width="2%">&nbsp;</td>
	        	<td width="4%" align="left" class="principalLabel"><html:checkbox value="true" property="funcInDestinatarioEmail" onclick="verificaClickDestinatarioEmail()"/> </td>
	        	<td width="40%" align="left" class="principalLabel">&nbsp;<bean:message key="prompt.DestinatarioEmail"/></td>
	        </tr>
	      </table>
	    </td>
  </tr>
  <%//Chamado 79280 - inclus�o do campo apelido%>
   <tr> 
    <td width="20%"> <div align="right"><bean:message key="prompt.func.apelido"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div></td>
    <td colspan="4"><html:text property="funcNmApelido" styleClass="text" maxlength="80" style="width:324px"/></td>
    <td>&nbsp;</td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%"> <div align="right"><bean:message key="prompt.eMail"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div></td>
    <td colspan="4"><html:text property="funcDsMail" styleClass="text" maxlength="80" style="width:324px" onblur="copiaEMail();validaEmail(this)"/></td>
    <td>&nbsp;</td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr>
  <td colspan="7" align="left">
	  <table id="trLogin" name="trLogin" cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="150px" align="right" class="principalLabel">
					<bean:message key="prompt.sso.dominio" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				</td>
				<td>
					<html:select property="idLodoCdLogindomain" styleClass="principalObjForm" style="width: 300px;" onchange="idLodoCdLogindomain_onChange(this);">
						<html:option value="">
							<bean:message key="prompt.selecione_uma_opcao" />
						</html:option>
						<html:options collection="csCdtbLogindomainLodoVector" property="field(id_lodo_cd_logindomain)" labelProperty="field(lodo_ds_logindomain)" />
					</html:select>
					
					<img id="imgTesteDominio" src="/plusoft-resources/images/botoes/bt_Agente.gif" title="<bean:message key='prompt.testarLoginDominio'/>" width="20" class="geralCursoHand" style="vertical-align: middle;" onclick="testeDominio();"  />
				</td>
				<td>&nbsp;</td>
				<td width="18%">&nbsp;</td>
			</tr>
			<tr> 
		    <td width="150px" align="right" class="principalLabel"><bean:message key="prompt.login"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    <td>
		          <html:text property="funcDsLoginname" styleClass="text" maxlength="100" style="width:324px"/>
		    </td>
 			<td>&nbsp;</td>
    		<td width="18%">&nbsp;</td>
		  </tr>
		  <tr> 
		    <td width="150px" align="right" class="principalLabel"><bean:message key="prompt.password"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    <td width="43%">
		          <html:password property="funcDsPassword" styleClass="text" maxlength="20" style="width:324px"/> 
		    </td>
		    <td colspan="2" valign="bottom" align="left">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
		    		<tr align="left">
		    			<td width="5%">
		    				<img src="webFiles/images/botoes/editar.gif" alt="<bean:message key="prompt.alterarSenha"/>" title="<bean:message key="prompt.alterarSenha"/>" class="geralCursoHand" onclick="resetarSenha()">
		    			</td>
		    			<td width="5%">
		    				<img alt="<bean:message key="prompt.gerarSenhaRandomica"/>" class="geralCursoHand" src="webFiles/images/botoes/geraratend.gif" onclick="gerarSenha()" width="21" height="20">
		    			</td>
		    			<td width="5%">
		    				<img alt="<bean:message key="prompt.revogarSenha"/>" class="geralCursoHand" src="webFiles/images/botoes/bt_CriarCarta.gif" onclick="revogarSenha()" width="21" height="20">
		    			</td>
		    			<td width="90%" align="center">
			    			<font color="red">
			    				<b><div align="right" id="bloqueado" name="bloqueado" style="visibility: hidden">Usu�rio bloqueado</div></b>
					 		</font>
					 	</td>
		    		</tr>	
		    	</table>
			</td>
		  </tr>  
	  </table>
  </td>
  </tr>
  <tr> 
    <td width="20%"> 
      <div align="right"><bean:message key="prompt.telefone"/> / <bean:message key="prompt.ramal"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="4"> 
      <html:text property="funcDsFax" styleClass="text" maxlength="30" />
    </td>
    <td>&nbsp;</td>
    <td width="18%">&nbsp;</td>
  </tr>

  <tr> 
    <td width="20%"> 
      <div align="right"><bean:message key="prompt.telefonecelular"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="4"> 
      <html:text property="funcDsCelular" styleClass="text" maxlength="20" />
    </td>
    <td>&nbsp;</td>
    <td width="18%">
    	&nbsp;
	</td>
  </tr>
  <tr> 
    <td width="20%"> 
      <div align="right"><bean:message key="prompt.funcao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="4"> 
      <html:text style="width:325px" property="funcDsFuncao" styleClass="text" maxlength="100" />
    </td>
    <td>&nbsp;</td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
	<td width="20%"> 
		<div align="right"><bean:message key="prompt.maxChat"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
  	</td>
  	<td colspan="4">
		<html:text style="width:325px" property="funcNrMaxchat" styleClass="text" maxlength="4" />
	</td>
	
	<td colspan="2" class="nominal" id="tdModulos"><img src="webFiles/Menu/images/mvert/Programa_Perfil.gif" width="32" height="32" class="geralCursoHand" onclick="abrirModalModulos()" title="<bean:message key="prompt.AssociacaoDeFuncionarioXModulo" />" ></td>
	
	<td class="concorrente">&nbsp;</td>
    <td class="concorrente" width="18%">&nbsp;</td>
	
  </tr>  
  
  <tr> 
    <td width="20%"> 
      <div align="right"></div>
    </td>
    <td colspan="5">&nbsp;</td>
    <td width="18%">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="principalLabel">&nbsp;</td>
  </tr>
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalPstQuadroLinkSelecionado">Telefonia</td>
    <td class="principalLabel">&nbsp;</td>
  </tr>
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro" height="85">
  <tr> 
    <td valign="top" class="principalBordaQuadro">
      <div id="Telefonia" style="position:absolute; width:94%; height:80px; z-index:1;"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td colspan="4" align="right" class="principalLabel">&nbsp;</td>
          </tr>
          <tr> 
            <td width="15%" align="right" class="principalLabel">&nbsp;<bean:message key="prompt.CTIRamal"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
            </td>
            <td width="40%" class="principalLabel"> 
				<html:text property="funcNrCtiRamal" styleClass="text" maxlength="10" /> 
            </td>
            <td width="14%" align="right" class="principalLabel">&nbsp;<bean:message key="prompt.CTIGrupo"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
            </td>
            <td width="31%" class="principalLabel"> 
 				<html:text property="funcDsCtiGrupo" styleClass="text" maxlength="20" />
            </td>
          </tr>
          <tr> 
            <td width="15%" align="right" class="principalLabel">&nbsp;<bean:message key="prompt.CTILogin"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
            </td>
            <td width="40%" class="principalLabel">
				<html:text property="funcDsCtiLogin" styleClass="text" maxlength="20" />            	 
            </td>
            <td width="14%" align="right" class="principalLabel">&nbsp;<bean:message key="prompt.CTIAgente"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
            </td>
            <td width="31%" class="principalLabel"> 
				<html:text property="funcDsCtiAgente" styleClass="text" maxlength="60" /> 
            </td>
          </tr>
          <tr> 
            <td width="15%" align="right" class="principalLabel">&nbsp;<bean:message key="prompt.CTISenha"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
            </td>
            <td width="40%" class="principalLabel"> 
            	<html:password property="funcDsCtiSenha" styleClass="text" maxlength="20" />            	
            </td>
            <td width="14%" align="right" class="principalLabel">&nbsp;</td>
            <td width="31%" class="principalLabel">&nbsp; </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposFuncionario();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbFuncionarioFuncForm.imgGravar, "<bean:message key='prompt.gravar'/>");			
			//Chamado: 89956 - 01/08/2013 - Carlos Nunes
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbFuncionarioFuncForm.imgGravarManter, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIO_INCLUSAO_CHAVE%>')){
				desabilitaCamposFuncionario();
			}
			document.getElementById("administracaoCsCdtbFuncionarioFuncForm").idFuncCdFuncionario.disabled= false;

			if(document.getElementById("administracaoCsCdtbFuncionarioFuncForm").manterEdicao.value != 'S')
			{
				document.getElementById("administracaoCsCdtbFuncionarioFuncForm").idFuncCdFuncionario.value= '';
			}
			
			document.getElementById("administracaoCsCdtbFuncionarioFuncForm").idFuncCdFuncionario.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			verificaAreaResolvedora();
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbFuncionarioFuncForm.imgGravar);
				//Chamado: 89956 - 01/08/2013 - Carlos Nunes
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbFuncionarioFuncForm.imgGravarManter);	
				desabilitaCamposFuncionario();
			}
		</script>
</logic:equal>

</html>

<!-- Chamado: 89956 - 01/08/2013 - Carlos Nunes -->
<script type="text/javascript" src="webFiles/funcoes/funcionario/funcionarioJS.js"></script>
<logic:present name="funcionarioJS"><script type="text/javascript" src="<bean:write name='funcionarioJS' />funcoesFuncionario.js"></script></logic:present>

<script>
    /**************************************************************************/
    /*O bloco abaixo � utilizado para substituir a feature de LICEN�A NOMINAL */
    /**************************************************************************/
    if(parent.administracaoCsCdtbFuncionarioFuncForm.licencaNominal.value == "S"){
    	$(".nominal").show();
        $(".concorrente").hide();
    }
    else{

        $(".nominal").hide();
        $(".concorrente").show();
    }
    /**************************************************************************/

	try{document.administracaoCsCdtbFuncionarioFuncForm.funcDsCodigoMsd.focus();}
	catch(e){}	
</script>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>