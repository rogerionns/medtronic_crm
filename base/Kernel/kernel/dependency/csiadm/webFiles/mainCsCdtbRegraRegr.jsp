<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsCdtbRegraRegrForm.target = admIframe.name;
	document.administracaoCsCdtbRegraRegrForm.acao.value ='filtrar';
	document.administracaoCsCdtbRegraRegrForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbRegraRegrForm.filtro.value = '';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsCdtbRegraRegrForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbRegraRegrForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_REGRA_REGR%>';
	editIframe.document.administracaoCsCdtbRegraRegrForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbRegraRegrForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo){
	tab.document.administracaoCsCdtbRegraRegrForm.idRegrCdRegra.value = codigo;
	tab.document.administracaoCsCdtbRegraRegrForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_REGRA_REGR%>';
	tab.document.administracaoCsCdtbRegraRegrForm.target = editIframe.name;
	tab.document.administracaoCsCdtbRegraRegrForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbRegraRegrForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){

	if(tab.document.administracaoCsCdtbRegraRegrForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_REGRA_REGR%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbRegraRegrForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_REGRA_REGR%>'){

		if(tab.document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.options[tab.document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.selectedIndex].getAttribute("definirAssunto") == "S"){
			if (tab.document.administracaoCsCdtbRegraRegrForm.idAsmeCdAssuntoMail.value == "") {
				alert("<bean:message key="prompt.Por_favor_selecione_um_assunto_de_E-mail"/>.");
				tab.document.administracaoCsCdtbRegraRegrForm.idAsmeCdAssuntoMail.focus();
				return  false;
			}
		}

		if (tab.document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_uma_acao"/> .");
			tab.document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.focus();
			return  false;
		}
		
		if(tab.document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.options[tab.document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.selectedIndex].getAttribute("encaminhar") == "S" && tab.document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.value == ""){
			alert("<bean:message key="prompt.alert.campoEmailEncaminharObrigatorio"/>.");
			return  false;
		}

		if(tab.document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.value != ""){
			if (tab.document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.value.search(/\S/) != -1) {
				regExp = /[A-Za-z0-9_]+@[A-Za-z0-9_-]{2,}\.[A-Za-z]{2,}/
				if (tab.document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.value.length < 7 || tab.document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.value.search(regExp) == -1){
					alert ("<bean:message key="prompt.alert.por.favor.preencha.corretamente.o.email"/>.");
				    tab.document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.focus();
				    return false;
				}						
			}
			num1 = tab.document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.value.indexOf("@");
			num2 = tab.document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.value.lastIndexOf("@");
			if (num1 != num2){
			    alert ("<bean:message key="prompt.alert.por.favor.preencha.corretamente.o.email"/>.");
			    tab.document.administracaoCsCdtbRegraRegrForm.regrDsEmailEncaminhar.focus();
				return false;
			}
		}
				
		if (tab.document.administracaoCsCdtbRegraRegrForm.idDocuCdDocumento.value == "" && 
			(tab.document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.options[tab.document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.selectedIndex].getAttribute("responder") == "S" ||
			tab.document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.options[tab.document.administracaoCsCdtbRegraRegrForm.idAcreCdAcaoRegraAux.selectedIndex].getAttribute("encaminhar") == "S")) {
			
			alert("<bean:message key="prompt.Por_favor_selecione_um_documento"/>.");
			tab.document.administracaoCsCdtbRegraRegrForm.idDocuCdDocumento.focus();
			return  false;
		}
		
		if (trim(tab.document.administracaoCsCdtbRegraRegrForm.regrDsRegra.value) == "") {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbRegraRegrForm.regrDsRegra.focus();
			return  false;
		}

		tab.document.administracaoCsCdtbRegraRegrForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_REGRA_REGR%>';
		tab.document.administracaoCsCdtbRegraRegrForm.target = admIframe.name;
		disableEnable(tab.document.administracaoCsCdtbRegraRegrForm.idRegrCdRegra, false);
		tab.document.administracaoCsCdtbRegraRegrForm.submit();
		disableEnable(tab.document.administracaoCsCdtbRegraRegrForm.idRegrCdRegra, true);
		cancel();

	}
}

function submeteExcluir(codigo) {
	
	tab.document.administracaoCsCdtbRegraRegrForm.idRegrCdRegra.value = codigo;
	tab.document.administracaoCsCdtbRegraRegrForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_REGRA_REGR%>';
	tab.document.administracaoCsCdtbRegraRegrForm.target = editIframe.name;
	tab.document.administracaoCsCdtbRegraRegrForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbRegraRegrForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbRegraRegrForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_REGRA_REGR%>';
		editIframe.document.administracaoCsCdtbRegraRegrForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbRegraRegrForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbRegraRegrForm.idRegrCdRegra, false);
		editIframe.document.administracaoCsCdtbRegraRegrForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbRegraRegrForm.idRegrCdRegra, true);
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbRegraRegr.do?tela=editCsCdtbRegraRegr&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbRegraRegr.do?tela=editCsCdtbRegraRegr&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsCdtbRegraRegrForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbRegraRegrForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_REGRA_REGR%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormal');
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbRegraRegrForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_REGRA_REGR%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionado');
			setaIdiomaHabilita();	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbRegraRegrForm"	action="/AdministracaoCsCdtbRegraRegr.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/><!-- ## --></td>
									<td class="principalPstQuadroLinkNormal" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.regra"/><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="position: absolute; width: 97%; height: 225px; z-index: 6; visibility: hidden"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsCdtbRegraRegr.do?tela=editCsCdtbRegraRegr&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="position: absolute; width: 97%; height: 199px; z-index: 2; visibility: visible"> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLabel" width="10%"><bean:message key="prompt.descricao"/></td>
                        <td class="principalLabel" colspan="3">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="65%"> 
                          <table border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="25%"> <html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
                              </td>
                              <td width="05%"> &nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" onclick="filtrar()" title='<bean:message key="prompt.aplicarFiltro"/>'> 
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" width="10%">&nbsp;</td>
                        <td class="principalLabel" colspan="3">&nbsp;</td>
                      </tr>
                   </table>
                   <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLstCab" width="18%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.codigo"/> 
                        </td>
                        <td class="principalLstCab" width="30%"><bean:message key="prompt.descricao"/></td>
                        <td class="principalLstCab" width="20%"><bean:message key="prompt.assuntoEMail"/></td>
                        <td class="principalLstCab" width="20%"><bean:message key="prompt.acaoRegra"/> </td>
                        <td class="principalLstCab" align="left" width="12%"><bean:message key="prompt.inativo"/> 
                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="5" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsCdtbRegraRegr.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
                      </tr>
                    </table>
							</div>

							
                </td>
							<td width="4" height="400"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir"	width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar"	width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td> 
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="540px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="540px"></td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
	desabilitaListaEmpresas();
	
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_REGRA_INCLUSAO_CHAVE%>', document.administracaoCsCdtbRegraRegrForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_REGRA_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_REGRA_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbRegraRegrForm.imgGravar.disabled=true;
			document.administracaoCsCdtbRegraRegrForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbRegraRegrForm.imgGravar.title='';
	   }
	setaArquivoXml("CS_ASTB_IDIOMAREGRA_IDRE.xml");
	habilitaTelaIdioma();
</script>

</body>
</html>
