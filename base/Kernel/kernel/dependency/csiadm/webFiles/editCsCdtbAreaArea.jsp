<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
 	function desabilitaCamposArea(){
 		document.administracaoCsCdtbAreaAreaForm.idAreaCdArea.disabled= true;
		document.administracaoCsCdtbAreaAreaForm.areaDsArea.disabled= true;	
		document.administracaoCsCdtbAreaAreaForm.areaDhInativo.disabled= true;
	}	
	
	function inicio(){
		setaChavePrimaria(administracaoCsCdtbAreaAreaForm.idAreaCdArea.value);
	}
	
	function validaEmail(obj){
		if(obj.value != ""){
			var cEmail = obj.value;
			obj.value = trim(cEmail.toLowerCase());
	
			if(window.top.jQuery) {
				cEmail = window.top.jQuery.trim(cEmail);
			} else if (window.dialogArguments) {
				cEmail = window.dialogArguments.top.jQuery.trim(cEmail);
			}
			
			if (cEmail.search(/\S/) != -1) {
			    //Chamado: 93047 - 05/02/2014 - Carlos Nunes
				regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9._-]{1,}\.[A-Za-z0-9]{2,}/
				if (cEmail.length < 7 || cEmail.search(regExp) == -1){
					alert ('<bean:message key="prompt.alert.email.correto" />');
					obj.focus();
				    return false;
				}						
			}
			num1 = cEmail.indexOf("@");
			num2 = cEmail.lastIndexOf("@");
			if (num1 != num2){
			    alert ('<bean:message key="prompt.alert.email.correto" />');
			    obj.focus();
				return false;
			}
		}
	}
</script>

<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbAreaAreaForm" action="/AdministracaoCsCdtbAreaArea.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>		

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idAreaCdArea" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="areaDsArea" styleClass="text" style="width:250px" maxlength="60" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.eMail"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="areaDsEmail" styleClass="text" style="width:250px" maxlength="60" onblur="validaEmail(this);"/> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="areaDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbAreaAreaForm.areaDsArea.disabled= true;	
			document.administracaoCsCdtbAreaAreaForm.areaDhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbAreaAreaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_INCLUSAO_CHAVE%>')){
				desabilitaCamposArea();
			}		
			document.administracaoCsCdtbAreaAreaForm.idAreaCdArea.disabled= false;
			document.administracaoCsCdtbAreaAreaForm.idAreaCdArea.value= '';
			document.administracaoCsCdtbAreaAreaForm.idAreaCdArea.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbAreaAreaForm.imgGravar);	
				desabilitaCamposArea();
			}
		</script>
</logic:equal>
</html>
<script>
	try{document.administracaoCsCdtbAreaAreaForm.areaDsArea.focus();}
	catch(e){}	
</script>