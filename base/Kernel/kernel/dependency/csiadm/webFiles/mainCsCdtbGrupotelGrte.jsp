<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	document.administracaoCsCdtbGrupotelGrteForm.target = admIframe.name;
	document.administracaoCsCdtbGrupotelGrteForm.acao.value ='filtrar';
	document.administracaoCsCdtbGrupotelGrteForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbGrupotelGrteForm.filtro.value = '';
}

function submeteFormIncluir() {
	editIframe.document.administracaoCsCdtbGrupotelGrteForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbGrupotelGrteForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_GRUPOTEL_GRTE%>';
	editIframe.document.administracaoCsCdtbGrupotelGrteForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbGrupotelGrteForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo){
	tab.document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"].value = codigo;
	tab.document.administracaoCsCdtbGrupotelGrteForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_GRUPOTEL_GRTE%>';
	tab.document.administracaoCsCdtbGrupotelGrteForm.target = editIframe.name;
	tab.document.administracaoCsCdtbGrupotelGrteForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbGrupotelGrteForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){
	if(tab.document.administracaoCsCdtbGrupotelGrteForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_GRUPOTEL_GRTE%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbGrupotelGrteForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_GRUPOTEL_GRTE%>'){
	//alert(tab.document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"]);
		if (trim(tab.document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.grteDsGrupotel"].value) != "") {
			tab.document.administracaoCsCdtbGrupotelGrteForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_GRUPOTEL_GRTE%>';
			tab.document.administracaoCsCdtbGrupotelGrteForm.target = admIframe.name;
			disableEnable(tab.document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"], false);
			tab.document.administracaoCsCdtbGrupotelGrteForm.submit();
			disableEnable(tab.document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"], true);
			cancel();
		} else {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.grteDsGrupotel"].focus();
		}
	}
}

function submeteExcluir(codigo) {
	tab.document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"].value = codigo;
	tab.document.administracaoCsCdtbGrupotelGrteForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_GRUPOTEL_GRTE%>';
	tab.document.administracaoCsCdtbGrupotelGrteForm.target = editIframe.name;
	tab.document.administracaoCsCdtbGrupotelGrteForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbGrupotelGrteForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbGrupotelGrteForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_GRUPOTEL_GRTE%>';
		editIframe.document.administracaoCsCdtbGrupotelGrteForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbGrupotelGrteForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"], false);
		editIframe.document.administracaoCsCdtbGrupotelGrteForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbGrupotelGrteForm["csCdtbGrupotelGrteVo.idGrteCdGrupotel"], true);
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbGrupotelGrte.do?tela=editCsCdtbGrupotelGrte&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbGrupotelGrte.do?tela=editCsCdtbGrupotelGrte&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsCdtbGrupotelGrteForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbGrupotelGrteForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_GRUPOTEL_GRTE%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormal');
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbGrupotelGrteForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_GRUPOTEL_GRTE%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionado');	
			setaIdiomaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbGrupotelGrteForm" action="/AdministracaoCsCdtbGrupotelGrte.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />

	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">
						&nbsp;
					</td>
					<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top">
				<br>
				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td height="254">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="principalPstQuadroLinkSelecionado" id="tdDestinatario" name="tdDestinatario"	onClick="AtivarPasta(admIframe);">
											<bean:message key="prompt.procurar" />
										</td>
										<td class="principalPstQuadroLinkNormal" id="tdManifestacao" name="tdManifestacao" onClick="AtivarPasta(editIframe);">
											<bean:message key="prompt.grupoTelefone" />
										</td>
									</tr>
								</table>
							</td>
							<td width="4">
								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
							</td>
						</tr>
						</table>
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr>				
			                <td valign="top" class="principalBgrQuadro" height="400">
			                	<br>
			                	<div name="Manifestacao" id="Manifestacao" style="width: 97%; height: 225px;display: none"> 
									<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr>									
				                        <td height="380"> 
					                    	<!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
    					                    <iframe id=editIframe name="editIframe"	src="AdministracaoCsCdtbGrupotelGrte.do?tela=editCsCdtbGrupotelGrte&acao=incluir" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0">
    					                    </iframe>
    					                </td>
									</tr>
									</table>
								</div>
			                    
			                    <div name="Destinatario" id="Destinatario" style="width: 97%; height: 199px;display: block">     
									<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr>
										<td class="principalLabel" width="15%">
											<bean:message key="prompt.descricao"/>
										</td>
										<td class="principalLabel" width="65%">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td width="65%">
											<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="25%">
													<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
												</td>
												<td width="05%">
													<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
												</td>
											</tr>	
											</table>
										</td>
									</tr>
									<tr>
										<td class="principalLabel" width="15%">
											&nbsp;
										</td>
										<td class="principalLabel" width="64%">
											&nbsp;
										</td>
									</tr>
									</table>
									<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr>
										<td class="principalLstCab" width="3%"></td>
										<td class="principalLstCab" width="10%">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalLstCab" width="25%">
													&nbsp;
												</td>
												<td class="principalLstCab" width="75%">
													&nbsp;&nbsp;<bean:message key="prompt.codigo"/>
												</td>
											</tr>
										</table>
										</td>
										<td class="principalLstCab" width="49%">
											<bean:message key="prompt.descricaoGrupoTelefone"/>
										</td>
										<td class="principalLstCab" width="30%">
											<bean:message key="prompt.numero"/>
										</td>
										<td class="principalLstCab" width="10%">
											<bean:message key="prompt.inativo"/> 
										</td>
									</tr>
									<tr valign="top">
										<td colspan="5" height="300"> 
				                        	<!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          					<iframe id=admIframe name="admIframe" 
                          						src="AdministracaoCsCdtbGrupotelGrte.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
                          						width="100%" height="98%" scrolling="Default" frameborder="0"
                          						marginwidth="0" marginheight="0">
                          					</iframe>
                          				</td>
									</tr>		
									</table>							
								</div>
							</td>
							<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
						</tr>
						<tr>
							<td width="100%" height="8" valign="top"><img 
							src="webFiles/images/linhas/horSombra.gif" width="100%" 
							height="4"></td>
							<td width="4" height="8" valign="top"><img 
							src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" 
							height="4"></td>
						</tr>
						</table>		
					</td>
				</tr>
				<tr>
					<td>
						<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3">
					</td>
				</tr>
				</table>			
				<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
						<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
						<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
						<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
				</tr>
				</table>					
				
				<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
				</table>				
			</td>					
			<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
		</tr>
		<tr>
			<td width="100%" valign="top"><img 
			src="webFiles/images/linhas/horSombra.gif" width="100%" 
			height="4"></td>
			<td width="4" valign="top"><img 
			src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" 
			height="4"></td>
		</tr>
	</table>
	</body>
	
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_GRUPOTELEFONE_INCLUSAO_CHAVE%>', document.administracaoCsCdtbGrupotelGrteForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_GRUPOTELEFONE_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_GRUPOTELEFONE_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbGrupotelGrteForm.imgGravar.disabled=true;
			document.administracaoCsCdtbGrupotelGrteForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbGrupotelGrteForm.imgGravar.title='';
	}
	
	desabilitaListaEmpresas();
	
	setaArquivoXml("CS_ASTB_IDIOMAGRUPOTEL_IDGT.xml");
	habilitaTelaIdioma();
	
</script>



</body>
</html>
