<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

function MontaLista(){
	ifmlstCsCdtbHorasuteisHrut.location.href= "AdministracaoCsCdtbHorasuteisHrut.do?tela=ifrmLstCsCdtbHorasuteisHrut&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbHorasuteisHrutVo.idHrutCdHorasuteis=" + document.administracaoCsCdtbHorasuteisHrutForm['csCdtbHorasuteisHrutVo.idHrutCdHorasuteis'].value;
}

function desabilitaCampos(){
	document.administracaoCsCdtbHorasuteisHrutForm['csCdtbHorasuteisHrutVo.idHrutCdHorasuteis'].disabled= true;
	document.administracaoCsCdtbHorasuteisHrutForm['csCdtbHorasuteisHrutVo.hrutNrSequencia'].disabled= true;
	document.administracaoCsCdtbHorasuteisHrutForm['csCdtbHorasuteisHrutVo.hrutDsInicio'].disabled= true;
	document.administracaoCsCdtbHorasuteisHrutForm['csCdtbHorasuteisHrutVo.hrutDsFim'].disabled= true;
	document.administracaoCsCdtbHorasuteisHrutForm['inInativo'].disabled= true;
	
}

function limpaCampos(){
	document.administracaoCsCdtbHorasuteisHrutForm['csCdtbHorasuteisHrutVo.hrutDsInicio'].value="";
	document.administracaoCsCdtbHorasuteisHrutForm['csCdtbHorasuteisHrutVo.hrutDsFim'].value="";
	document.administracaoCsCdtbHorasuteisHrutForm.inInativo.checked=false;
}

function limpaEdit(){
}


</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista()">
<html:form styleId="administracaoCsCdtbHorasuteisHrutForm" action="/AdministracaoCsCdtbHorasuteisHrut.do">
	
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<html:hidden property="csCdtbHorasuteisHrutVo.hrutNrSequencia" />
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.diaDaSemana"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
		<html:select property="csCdtbHorasuteisHrutVo.idHrutCdHorasuteis" styleClass="principalObjForm" onchange="MontaLista();limpaCampos();"> 
			<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
			<html:option value="1"><bean:message key="prompt.domingo" /></html:option>
			<html:option value="2"><bean:message key="prompt.segunda" /></html:option>
			<html:option value="3"><bean:message key="prompt.terca" /></html:option>
			<html:option value="4"><bean:message key="prompt.quarta" /></html:option>
			<html:option value="5"><bean:message key="prompt.quinta" /></html:option>
			<html:option value="6"><bean:message key="prompt.sexta" /></html:option>
			<html:option value="7"><bean:message key="prompt.sabado" /></html:option>
		</html:select>
    
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.periodo"/> <bean:message key="prompt.de"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="56%" colspan="2">
    	<table width="100%">
    		<tr width="100%">
    			<td width="37%"><html:text property="csCdtbHorasuteisHrutVo.hrutDsInicio" styleClass="text" onkeydown="return validaDigitoHora(this,event)" maxlength="5" /></td>
    			<td width="3%">&nbsp;</td>
    			<td width="10%" class="principalLabel"><bean:message key="prompt.ate"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    			<td width="37%"><html:text property="csCdtbHorasuteisHrutVo.hrutDsFim" styleClass="text" onkeydown="return validaDigitoHora(this,event)" maxlength="5" /></td>
    		</tr>
    	</table>
    </td>
	<td width="31%" align="left">&nbsp;</td>
  </tr>
   <tr> 
    <td width="13%">&nbsp;</td>
    <td class="principalLabel" colspan="2"><html:radio value="M" property="csCdtbHorasuteisHrutVo.hrutInTipo"/> <bean:message key="prompt.manifestacao"/> <html:radio value="C" property="csCdtbHorasuteisHrutVo.hrutInTipo"/> <bean:message key="prompt.chat"/>&nbsp;&nbsp;<html:radio value="P" property="csCdtbHorasuteisHrutVo.hrutInTipo"/> <bean:message key="prompt.campanha"/></td>
    <td width="31%" ></td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td class="principalLabel" align="right" width="28%"><html:checkbox value="true" property="inInativo"/> <bean:message key="prompt.inativo"/> </td>
    <td width="31%" ></td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td class="principalLabel" align="right" width="28%"> </td>
    <td width="31%" ></td>
  </tr>  
  <tr> 
    <td colspan="4" height="220"> 
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="6%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="37%"> <bean:message key="prompt.periodoTempo"/></td>
          <td class="principalLstCab" width="33%"> <bean:message key="prompt.tipo"/></td>
          <td class="principalLstCab" width="10%"> <bean:message key="prompt.inativo"/></td>
          <td class="principalLstCab" width="14%">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <iframe id="ifmlstCsCdtbHorasuteisHrut" name="ifmlstCsCdtbHorasuteisHrut" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_HORASUTEIS_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbHorasuteisHrutForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_HORASUTEIS_INCLUSAO_CHAVE%>')){
				desabilitaCampos();
			}else{
				//administracaoCsCdtbFeriadoFeriForm.idEstadoCivil.disabled= false;
				//administracaoCsCdtbFeriadoFeriForm.idEstadoCivil.value= '';
				//administracaoCsCdtbFeriadoFeriForm.idEstadoCivil.disabled= true;
			}

		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_HORASUTEIS_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_HORASUTEIS_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbHorasuteisHrutForm.imgGravar);	
				desabilitaCampos();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbHorasuteisHrutForm['csCdtbHorasuteisHrutVo.idHrutCdHorasuteis'].focus();}
	catch(e){}
</script>