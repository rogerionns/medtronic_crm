<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsCdtbClassificacaoClasForm" action="/AdministracaoCsCdtbClassificacaoClas.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csCdtbClassificacaoClasVector"> 
  <tr> 
    <td class="principalLstParMao" width="3%" onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="idClasCdClassificacao" />')" > 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" width="18" height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>"  > 
    </td>
    <td width="3%" class="principalLstParMao"> 
    	<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMACLAS_IDCL.xml','<bean:write name="ccttrtVector" property="idClasCdClassificacao" />')">
    </td>    
    <td  class="principalLstParMao" width="10%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idClasCdClassificacao" />','<bean:write name="ccttrtVector" property="csCdtbTpClassificacaoTpclVo.idTpclCdTpClassificacao" />')"  >
       &nbsp;<bean:write name="ccttrtVector" property="idClasCdClassificacao" /> 
    </td>
    <td  class="principalLstParMao" width="70%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idClasCdClassificacao" />','<bean:write name="ccttrtVector" property="csCdtbTpClassificacaoTpclVo.idTpclCdTpClassificacao" />')"  >
       <bean:write name="ccttrtVector" property="clasDsClassificacao" /> 
    </td>
    <td  class="principalLstParMao" width="17%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idClasCdClassificacao" />','<bean:write name="ccttrtVector" property="csCdtbTpClassificacaoTpclVo.idTpclCdTpClassificacao" />')"  >
       &nbsp;<bean:write name="ccttrtVector" property="clasDhInativo" /> 
    </td>
  </tr>
  </logic:iterate>
</table>
</html:form>
</body>
</html>