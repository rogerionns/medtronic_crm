<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 
if(request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) == null)
	response.sendRedirect("/csiadm/webFiles/index.jsp?sessao=expirada");

response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title>..: <plusoft:message key="prompt.loginNoDominio"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" style="overflow: hidden;">
	<plusoft:frame height="180" width="310" label="prompt.loginNoDominio" contentStyle="width: 310px;" >
		<br/>
		<table border="0" cellpadding="0" cellspacing="0" width="90%" >
			<tr>
				<td class="principalLabel" width="30%" align="right">
					<plusoft:message key="prompt.logindomain.config"/>&nbsp;<img src="/plusoft-resources/images/icones/setaAzul.gif" />&nbsp;
				</td>
				<td width="70%">
					<input type="text" class="principalObjForm" id="config" maxlength="2000" />
				</td>			
			</tr>
			<tr>
				<td class="principalLabel" width="30%" align="right">
					<plusoft:message key="prompt.sso.dominio"/>&nbsp;<img src="/plusoft-resources/images/icones/setaAzul.gif" />&nbsp;
				</td>
				<td width="70%">
					<input type="text" class="principalObjForm" id="dominio" maxlength="60" />
				</td>			
			</tr>
			<tr>
				<td class="principalLabel" width="30%" align="right">
					<plusoft:message key="prompt.usuario"/>&nbsp;<img src="/plusoft-resources/images/icones/setaAzul.gif" />&nbsp;
				</td>
				<td width="70%">
					<input type="text" class="principalObjForm" id="usuario"  maxlength="100" />
				</td>			
			</tr>
			<tr>
				<td class="principalLabel" width="30%" align="right">
					<plusoft:message key="prompt.senha"/>&nbsp;<img src="/plusoft-resources/images/icones/setaAzul.gif" />&nbsp;
				</td>
				<td width="70%">
					<input type="text" class="principalObjForm senha" maxlength="100" id="senha-clear" value="" style="display: none; " />
					<input type="password" class="principalObjForm senha" maxlength="100" id="senha-pwd" value="" onchange="" />
					
					<input type="hidden" class="principalObjForm" id="senha-hdn" value="" />
					
				</td>			
			</tr>
			<tr>
				<td class="principalLabel" width="30%" align="right">
					&nbsp;
				</td>
				<td width="70%" class="principalLabel">
					<input type="checkbox" id="esconder" checked="checked" /> Esconder caracteres da senha
				</td>			
			</tr>
			<tr>
				<td class="principalLabel" colspan="2">
					&nbsp;
			</tr>
			<tr>
				<td class="principalLabel" width="30%" align="right">
					&nbsp;
				</td>
				<td width="70%" class="principalLabel" align="right">
					<div id="testar" class="geralCursoHand" style="width: 100px;">
					<b>Testar Login</b> 
					<img src="/plusoft-resources/images/botoes/bt_Agente.gif" width="20" style="vertical-align: middle;" />
					</div>
				</td>			
			</tr>

		</table>
	</plusoft:frame>
	
	<img src="/plusoft-resources/images/botoes/out.gif" title="<bean:message key="prompt.sair"/>" onClick="window.close();" class="geralCursoHand"
			style="position: absolute; bottom: 10px; right: 10px;" /> 
	

	<div id="divAguarde" class="aguarde" style="position: absolute; top: 0; left: 0; height: 100%; width: 120%; opacity:0.4;filter:alpha(opacity=40); background-color: #CDCDCD; z-index: 500; cursor: wait; "></div>
	<img id="imgAguarde" class="aguarde" src="/plusoft-resources/images/icones/ajax-loader.gif" style="position: absolute; top: 90px; left: 160px; z-index: 501; cursor: wait; " />
	
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {

		// Se a tela n�o foi carregada de lugar nenhum, deve redirecionar pra tela de Login
		if(window.opener==undefined) {
			document.location="/csiadm/webFiles/index.jsp?sessao=expirada";
			return;
		}
		

		$(document).ajaxError(function(e, xhr, settings, exception) {
			alert(exception+' error in: \n\n' + settings.url + ' \n'+'error:\n' + xhr.responseText );
		}); 


		$.post("/csiadm/generic-adm/consulta-banco", {	
			"entity":"<%=MAConstantes.ENTITY_CS_CDTB_LOGINDOMAIN_LODO %>",
			"statement":"select-by-pk",
			"id_lodo_cd_logindomain":window.opener.document.forms[0].idLodoCdLogindomain.value, 
			"type":"json"}, function(ret) {
				$(".aguarde").each(function() { $(this).hide(); });

				if(ret.msgerro || ret.resultado==undefined){
					alert("<bean:message key='prompt.naofoiPossivelObterDadosConexao'/>" +ret.msgerro);
					return false;
				}
				
				if(ret.resultado.length>0){
					$("#config").val(ret.resultado[0].lodo_ds_config);
					$("#dominio").val(ret.resultado[0].lodo_ds_domain);
					$("#usuario").val(window.opener.document.forms[0].funcDsLoginname.value.toLowerCase());
				}
			}, "json");
		
		$(".senha").change(function(e) { $("#senha-hdn").val($(this).val()); } );
		$('#esconder').click(function(e) {
			$(".senha").each( function() { $(this).val($("#senha-hdn").val()); } );

			
			$("#senha-clear").hide();
			$("#senha-pwd").hide();
			if($(this).attr("checked")==true){
				$("#senha-pwd").show();
			} else {
				$("#senha-clear").show();
			}  
		}); 

		
		$("#testar").click(function() {
			if($("#config").val()=="" || $("#dominio").val()=="" || $("#usuario").val()=="" || $("#senha-hdn").val()=="") {
				alert("<bean:message key='prompt.paraEfetuarTesteTodosParametrosObrigatorios'/>");
				return false;
			} 

			$(".aguarde").each(function() { $(this).show(); });
			$.post("/csiadm/security-adm/ntlm-authenticate", {	
				"config":$("#config").val(),
				"domain":$("#dominio").val(), 
				"u":$("#usuario").val(), 
				"p":$("#senha-hdn").val(),
				"type":"json"}, function(ret) {
					$(".aguarde").each(function() { $(this).hide(); });

					if(ret.msgerro){
						alert("<bean:message key='prompt.erroAoValidarUsuario'/>:\n\n"+ret.msgerro);
						return false;
					}
					
					alert(ret.resultado);
				}, "json");
			
		});
		

	});
	</script>
</body>
</html>