<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<%  
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsCdtbBotaoBotaForm" action="/AdministracaoCsCdtbBotaoBota.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idBotaCdBotao"/>

<script>var possuiRegistros=false;</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
  <logic:iterate id="ccttrtVector" name="CsCdtbBotaoBotaVector" indexId="sequencia">
  <script>possuiRegistros=true;</script>
  <tr> 
    <td width="3%" class="principalLstPar">
      <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="idBotaCdBotao" />')">
    </td>
     <td width="3%" class="principalLstParMao"> 
    	&nbsp;<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMABOTAO_IDBO.xml','<bean:write name="ccttrtVector" property="idBotaCdBotao" />')">
    </td>
    <td class="principalLstParMao" width="10%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idBotaCdBotao" />')">
      <bean:write name="ccttrtVector" property="idBotaCdBotao" />
    </td>

    <td class="principalLstParMao" align="left" width="23%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idBotaCdBotao" />')">
     &nbsp; <bean:write name="ccttrtVector" property="botaDsBotao" />
	</td>

    <td class="principalLstParMao" align="left" width="22%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idBotaCdBotao" />')">
		&nbsp;
		<logic:equal name="ccttrtVector" property="botaInLocal" value="M">
			<bean:message key="prompt.manifestacao"/>		
		</logic:equal>
		<logic:equal name="ccttrtVector" property="botaInLocal" value="F">
			<bean:message key="prompt.funcoesextra"/>		
		</logic:equal>		
		<logic:equal name="ccttrtVector" property="botaInLocal" value="P">
			<bean:message key="prompt.pessoa"/>		
		</logic:equal>
		<logic:equal name="ccttrtVector" property="botaInLocal" value="A">
			<bean:message key="prompt.cadastro"/>		
		</logic:equal>
		<logic:equal name="ccttrtVector" property="botaInLocal" value="R">
			<bean:message key="prompt.ressarcimento"/>		
		</logic:equal>
		<logic:equal name="ccttrtVector" property="botaInLocal" value="T">
			<bean:message key="prompt.produto"/>		
		</logic:equal>
		<logic:equal name="ccttrtVector" property="botaInLocal" value="S">
			<bean:message key="prompt.funcoesextraSfa"/>		
		</logic:equal>
		<logic:equal name="ccttrtVector" property="botaInLocal" value="C">
			<bean:message key="prompt.pessoaCliente"/>		
		</logic:equal>
		<logic:equal name="ccttrtVector" property="botaInLocal" value="L">
			<bean:message key="prompt.pessoaLead"/>		
		</logic:equal>
		<logic:equal name="ccttrtVector" property="botaInLocal" value="O">
			<bean:message key="prompt.oportunidade"/>		
		</logic:equal>
		<logic:equal name="ccttrtVector" property="botaInLocal" value="1">
			<bean:message key="prompt.MobileFuncoesExtra"/>		
		</logic:equal>
		<logic:equal name="ccttrtVector" property="botaInLocal" value="2">
			<bean:message key="prompt.MobilePessoa"/>		
		</logic:equal>
	</td>	
	
	<td class="principalLstParMao" align="left" width="13%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idBotaCdBotao" />')">
	&nbsp;
      <bean:write name="ccttrtVector" property="botaDhInativo" />
	</td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_BOTAO_EXCLUSAO_CHAVE%>', editCsCdtbBotaoBotaForm.lixeira);
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>