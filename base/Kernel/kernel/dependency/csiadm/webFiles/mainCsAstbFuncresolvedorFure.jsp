<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsAstbFuncresolvedorFureForm.target = admIframe.name;
	document.administracaoCsAstbFuncresolvedorFureForm.acao.value ='filtrar';
	document.administracaoCsAstbFuncresolvedorFureForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsAstbFuncresolvedorFureForm.filtro.value = '';
}

function submeteFormIncluir() {
	editIframe.document.administracaoCsAstbFuncresolvedorFureForm.target = editIframe.name;
	editIframe.document.administracaoCsAstbFuncresolvedorFureForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_FUNCRESOLVEDOR_FURE%>';
	editIframe.document.administracaoCsAstbFuncresolvedorFureForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsAstbFuncresolvedorFureForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo1,codigo2){
	tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value = codigo1;
	tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].value = codigo2;
	tab.document.administracaoCsAstbFuncresolvedorFureForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_FUNCRESOLVEDOR_FURE%>';
	tab.document.administracaoCsAstbFuncresolvedorFureForm.target = editIframe.name;
	tab.document.administracaoCsAstbFuncresolvedorFureForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsAstbFuncresolvedorFureForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){
	/*
	if(tab.document.administracaoCsAstbFuncresolvedorFureForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_FUNCRESOLVEDOR_FURE%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsAstbFuncresolvedorFureForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_ASTB_FUNCRESOLVEDOR_FURE%>'){
	*/
	tab.document.administracaoCsAstbFuncresolvedorFureForm.acao.value = '<%=Constantes.ACAO_INCLUIR %>';
	if(window.document.all.item("Manifestacao").style.visibility == "hidden"){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else{	
		if (tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value == "" ) {
			alert('<bean:message key="prompt.selecionar_area_resolvedora"/>');
			tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].focus();
			return false;
		}
		if (tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].value == "") {
			alert('<bean:message key="prompt.selecionar_funcionario_resolvedor"/>');
			tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].focus();
			return false;
		}
		tab.document.administracaoCsAstbFuncresolvedorFureForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_FUNCRESOLVEDOR_FURE%>';
		tab.document.administracaoCsAstbFuncresolvedorFureForm.target = admIframe.name;
		disableEnable(tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'], false);
		tab.document.administracaoCsAstbFuncresolvedorFureForm.submit();
		//disableEnable(tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'], true);
		//cancel();

		setTimeout("editIframe.lstArqCarga.location.reload()", 2500);
		//tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value = "";
		tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].value = "";
		tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.fureInVer'].checked = false;
		tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.fureInResolver'].checked = false;
		
	}
}

function submeteExcluir(codigo1,codigo2) {

	//CHAMADO 67966 - VINICIUS - EXCLUINDO FUNCION�RIOS DIFERENTES - foi alterado onde � setado os ids dos funcionarios
	tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value = codigo1;
	tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].value = codigo2;
	// Luis Mario
	// Como o funcionario selecionado pode n�o existir na lista (inativo)
	// Fizemos uma gambiarra para passar o id deste no IdSuperior
	if (tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].value == "") {
		tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdSuperior'].value = codigo2;
	}
	tab.document.administracaoCsAstbFuncresolvedorFureForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_FUNCRESOLVEDOR_FURE%>';
	tab.document.administracaoCsAstbFuncresolvedorFureForm.target = editIframe.name;
	tab.document.administracaoCsAstbFuncresolvedorFureForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsAstbFuncresolvedorFureForm.submit();

	/////////////////////////////////////
	//AtivarPasta(editIframe);
	//MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
	/////////////////////////////////////	
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsAstbFuncresolvedorFureForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_FUNCRESOLVEDOR_FURE%>';
		editIframe.document.administracaoCsAstbFuncresolvedorFureForm.target = admIframe.name;
		editIframe.document.administracaoCsAstbFuncresolvedorFureForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		//disableEnable(editIframe.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'], false);
		editIframe.document.administracaoCsAstbFuncresolvedorFureForm.submit();
		//disableEnable(editIframe.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'], true);

		/////////////////////////////////
		//AtivarPasta(admIframe);
		//MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		//editIframe.location = 'AdministracaoCsAstbFuncresolvedorFure.do?tela=editCsAstbFuncresolvedorFure&acao=incluir';
		/////////////////////////////////
	
		//tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value = "";
		tab.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].value = "";
		setTimeout("editIframe.lstArqCarga.location.reload()", 2000);
			
	}else{
	
		//editIframe.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value = codigo1;
		editIframe.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncResolvedorFuncVo.idFuncCdFuncionario'].value = "";
		editIframe.document.administracaoCsAstbFuncresolvedorFureForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_FUNCRESOLVEDOR_FURE%>';
		editIframe.document.administracaoCsAstbFuncresolvedorFureForm.target = editIframe.name;
		editIframe.document.administracaoCsAstbFuncresolvedorFureForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';		
		AtivarPasta(editIframe);
		MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
		//tab = admIframe;
		//submeteFormEdit(editIframe.document.administracaoCsAstbFuncresolvedorFureForm['csAstbFuncresolvedorFureVo.csCdtbFuncAreaFuncVo.idFuncCdFuncionario'].value, 0);
		//cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsAstbFuncresolvedorFure.do?tela=editCsAstbFuncresolvedorFure&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsAstbFuncresolvedorFureForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsAstbFuncresolvedorFureForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_FUNCRESOLVEDOR_FURE%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			break;
		case editIframe:
			tab.document.administracaoCsAstbFuncresolvedorFureForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_FUNCRESOLVEDOR_FURE%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsAstbFuncresolvedorFureForm"	action="/AdministracaoCsAstbFuncresolvedorFure.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/><!-- ## --></td>
									<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.resolvedor"/><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="position: absolute; width: 97%; height: 225px; z-index: 6; visibility: hidden"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsAstbFuncresolvedorFure.do?tela=editCsAstbFuncresolvedorFure&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="position: absolute; width: 97%; height: 199px; z-index: 2; visibility: visible"> 
                    	<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									
                        <td class="principalLabel" width="8%" colspan="2"><bean:message key="prompt.resolvedor"/></td>

								</tr>

								<tr>
									
                        <td width="65%"> 
                          <table border="0" cellspacing="0" cellpadding="0">
									<tr>
									<td width="25%">
									<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
									</td>
									<td width="05%">
									&nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
									</td>
									</tr>	
									</table>
									</td>
								</tr>
								<tr>
									
                        <td class="principalLabel" width="8%">&nbsp;</td>
									
                        <td class="principalLabel" width="49%">&nbsp;</td>
								</tr>
					</table>
					<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									
                        <td class="principalLstCab" width="12%">&nbsp;&nbsp;C�digo</td>
                        <td width="3%" class="principalLstCab"></td>
									
                        <td class="principalLstCab" width="85%"><bean:message key="prompt.resolvedor"/> </td>
									
								</tr>
								
								<tr valign="top">
									
                        <td colspan="3" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsAstbFuncresolvedorFure.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                </td>
							<td width="4" height="400"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="540px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="540px"></td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">

	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIORESOLVEDOR_INCLUSAO_CHAVE%>', document.administracaoCsAstbFuncresolvedorFureForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIORESOLVEDOR_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIORESOLVEDOR_ALTERACAO_CHAVE%>')){
			document.administracaoCsAstbFuncresolvedorFureForm.imgGravar.disabled=true;
			document.administracaoCsAstbFuncresolvedorFureForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsAstbFuncresolvedorFureForm.imgGravar.title='';
	}

	desabilitaListaEmpresas();

</script>


</body>
</html>
