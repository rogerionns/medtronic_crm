<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">


<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/validadata.js"></script>

<script>
	function desabilitaCamposFeriado(){
		document.administracaoCsCdtbFeriadoFeriForm["csCdtbFeriadoFeriVo.idFeriCdFeriado"].disabled= true;
		document.administracaoCsCdtbFeriadoFeriForm["csCdtbFeriadoFeriVo.feriDsFeriado"].disabled= true;
		document.administracaoCsCdtbFeriadoFeriForm["csCdtbFeriadoFeriVo.feriDsMesdia"].disabled= true;
		document.administracaoCsCdtbFeriadoFeriForm["csCdtbFeriadoFeriVo.feriDhFeriado"].disabled= true;
		document.administracaoCsCdtbFeriadoFeriForm.inInativo.disabled= true;
		document.administracaoCsCdtbFeriadoFeriForm.inFixo.disabled= true;		
	}	
	
	function exibirCampo() {
		if( document.administracaoCsCdtbFeriadoFeriForm.inFixo.checked ) {
			trFeriDsMesdia.style.display = "block";
			trFeriDhFeriado.style.display = "none";
		}
		else {
			trFeriDhFeriado.style.display = "block";
			trFeriDsMesdia.style.display = "none";
		}
	}
	
	function inicio() {
		exibirCampo();
		setaChavePrimaria(administracaoCsCdtbFeriadoFeriForm['csCdtbFeriadoFeriVo.idFeriCdFeriado'].value);
	}
	
	function verificaQdadeCaracteres( obj ) {

		var comeco = obj.value.substring(0,2);
		var meio = obj.value.substring(2,3);
		var fim = obj.value.substring(3,5);

		if(meio.indexOf('/') == -1) {
			alert('<bean:message key="prompt.informeDataFormatoDD_MM" />');
			obj.focus();
		}
	}
	
	function verificarCampoData(obj){
		var txt = obj.value;
		
		if(txt != "" && (isNaN(txt.substring(0,2)) || isNaN(txt.substring(3,5)) || txt.charAt(2) != "/")){
			alert('<bean:message key="prompt.informeDataFormatoDD_MM" />');
			return false;
		}
		
		verificaDataDiaMes(obj);
	}
	
	function insereMascaraData(obj, event){
		var txt = obj.value;
		
		if(event.keyCode != 8 && txt.length == 2)
			obj.value += "/";
	}
	
	document.dataRetorno = new Object();
	document.dataRetorno.value = "";
	
	function retornoData(){
		if(vWinCal != undefined && vWinCal.closed){
			administracaoCsCdtbFeriadoFeriForm["csCdtbFeriadoFeriVo.feriDhFeriado"].value = document.dataRetorno.value;
		}
		else{
			setTimeout("retornoData()", 500);
		}
	}
</script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbFeriadoFeriForm" action="/AdministracaoCsCdtbFeriadoFeri.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="csCdtbFeriadoFeriVo.feriDhInativo"/>
	
	<br>
	<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
		<td align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
        	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
        <td> 
           	<html:text property="csCdtbFeriadoFeriVo.idFeriCdFeriado" styleClass="text" disabled="true" style="width:305"/>
	    </td>
	</tr>
	<tr> 
		<td align="right" class="principalLabel"><bean:message key="prompt.descricaoFeriado"/>
			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td> 
			<html:text property="csCdtbFeriadoFeriVo.feriDsFeriado" styleClass="text" maxlength="60" style="width:305" /> 
		</td>
	</tr>
	<tr id="trFeriDsMesdia"> 
		<td align="right" class="principalLabel"><bean:message key="prompt.mesdia"/>
							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/></td>
		<td align="left" >
		<html:text property="csCdtbFeriadoFeriVo.feriDsMesdia" style="width:305" styleClass="text" maxlength="5" onblur="verificarCampoData(this)" onkeydown="insereMascaraData(this, event);" />
		</td>
	</tr>
	<tr id="trFeriDhFeriado" style="display:none"> 
		<td class="principalLabel" align="right"><bean:message key="prompt.data"/>
							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/></td>
		<td>
			<div >
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td> 
							<html:text property="csCdtbFeriadoFeriVo.feriDhFeriado" style="width:280" styleClass="text" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/> 
						</td>
						<td><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario"/>" name="imgCalendario" width="16" height="15" onclick="show_calendar('dataRetorno');retornoData();" class="principalLstParMao" ></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	<tr> 
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr> 
		<td align="right"><html:checkbox value="true" property="inFixo" onclick="exibirCampo()"/></td>
		<td align="left" class="principalLabel">
			<bean:message key="prompt.fixo"/>
		</td>
	</tr>
	<tr> 
		<td align="right"><html:checkbox value="true" property="inInativo"/></td>
		<td align="left" class="principalLabel">
			<bean:message key="prompt.inativo"/>
		</td>
	</tr>
	</table>

</html:form>
</body>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposFeriado();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_FERIADO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbFeriadoFeriForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_FERIADO_INCLUSAO_CHAVE%>')){
				desabilitaCamposFeriado();
			}else{
				//document.administracaoCsCdtbFeriadoFeriForm.idEstadoCivil.disabled= false;
				//document.administracaoCsCdtbFeriadoFeriForm.idEstadoCivil.value= '';
				//document.administracaoCsCdtbFeriadoFeriForm.idEstadoCivil.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_FERIADO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_FERIADO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbFeriadoFeriForm.imgGravar);	
				desabilitaCamposFeriado();
			}
		</script>
</logic:equal>

</html>

<script>
	//try{document.administracaoCsCdtbFeriadoFeriForm.dsEstadoCivil.focus();}
	//catch(e){}
</script>