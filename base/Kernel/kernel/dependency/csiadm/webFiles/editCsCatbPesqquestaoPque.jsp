<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head>
<script language="JavaScript">
<!--
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function VerificaClick(){

	if (document.administracaoCsCatbPesqquestaoPqueForm.pqueInResposta.checked == true) {
		MM_showHideLayers('Layer1','','show')
	}
	if (document.administracaoCsCatbPesqquestaoPqueForm.pqueInResposta.checked == false) {
		MM_showHideLayers('Layer1','','hide');
		document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.value = "";
		document.administracaoCsCatbPesqquestaoPqueForm.pqueInTipodado[0].checked = false;
		document.administracaoCsCatbPesqquestaoPqueForm.pqueInTipodado[1].checked = false;
		document.administracaoCsCatbPesqquestaoPqueForm.pqueInTipodado[2].checked = false;
	}
}

	function VerificaCampos(){
	
		if(document.administracaoCsCatbPesqquestaoPqueForm.pqueInTipodado[2].checked){
			document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.value = '10';
			document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.disabled=true;
		}
	
		if (document.administracaoCsCatbPesqquestaoPqueForm.pqueNrSequencia.value == "0" ){
			document.administracaoCsCatbPesqquestaoPqueForm.pqueNrSequencia.value = "" ;
		}
		if (document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.value == "0" ){
			document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.value = "" ;
		}
		VerificaClick();
	}

	function MontaLista(){
		lstArqCarga.location.href= "AdministracaoCsCatbPesqquestaoPque.do?tela=administracaoLstCsCatbPesqquestaoPque&acao=<%=Constantes.ACAO_VISUALIZAR%>&idPesqCdPesquisa=" + document.administracaoCsCatbPesqquestaoPqueForm.idPesqCdPesquisa.value;
	}

	function desabilitaCamposPesqquestao(){

		document.administracaoCsCatbPesqquestaoPqueForm.pqueNrSequencia.disabled= true;	
		document.administracaoCsCatbPesqquestaoPqueForm.pqueInEncerramento.disabled= true;	
		document.administracaoCsCatbPesqquestaoPqueForm.pqueInObrigatorio.disabled= true;	
		document.administracaoCsCatbPesqquestaoPqueForm.pqueInResposta.disabled= true;	
		document.administracaoCsCatbPesqquestaoPqueForm.pqueInTabulacao.disabled= true;	
		document.administracaoCsCatbPesqquestaoPqueForm.pqueInTipodado.disabled= true;	
		document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.disabled= true;
	}

	function alertaTamanho(){
		alert('<bean:message key="prompt.tamanhoLimiteResposta900Caracteres" />')
	}
	
//-->
</script>
</head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos();MontaLista()">

<html:form styleId="administracaoCsCatbPesqquestaoPqueForm" action="/AdministracaoCsCatbPesqquestaoPque.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.pesquisa"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idPesqCdPesquisa" styleClass="principalObjForm" onchange="MontaLista()"> 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbPesquisaPesqVector" property="idCodigo" labelProperty="pesqDsPesquisa"/> 
      </html:select> </td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.questao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idQuesCdQuestao" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbQuestaoQuesVector" property="idCodigo" labelProperty="quesDsQuestao"/> 
      </html:select> </td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.sequencia"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><html:text property="pqueNrSequencia" styleClass="text" maxlength="5" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/></td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%">&nbsp;</td>
    <td width="14%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="9%"> <html:checkbox value="true" property="pqueInEncerramento"/> 
          </td>
          <td class="principalLabel" width="91%">&nbsp;<bean:message key="prompt.encerramento"/> </td>
        </tr>
      </table>
    </td>
    <td class="principalLabel" width="55%">&nbsp; </td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%">&nbsp;</td>
    <td width="14%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="9%"> <html:checkbox value="true" property="pqueInObrigatorio"/> 
          </td>
          <td class="principalLabel" width="91%">&nbsp;<bean:message key="prompt.obrigatorio"/></td>
        </tr>
      </table>
    </td>
    <td class="principalLabel" width="55%">&nbsp;</td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%">&nbsp;</td>
    <td width="14%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="9%" > <html:checkbox value="true" property="pqueInResposta" onclick="VerificaClick()" /> 
          </td>
          <td class="principalLabel" width="91%">&nbsp;<bean:message key="prompt.resposta"/></td>
        </tr>
      </table>
    </td>
    <td class="principalLabel" width="55%">&nbsp;</td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%">&nbsp;</td>
    <td width="14%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="9%"> <html:checkbox value="true" property="pqueInTabulacao"/> 
          </td>
          <td class="principalLabel" width="91%">&nbsp;<bean:message key="prompt.tabulacao"/></td>
        </tr>
      </table>
    </td>
    <td class="principalLabel" width="55%">&nbsp;</td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%">&nbsp;</td>
    <td colspan="2">&nbsp; </td>
    <td width="14%"> 
      <div id="Layer1" style="position:absolute; left:271px; top:118px; width:265px; height:39px; z-index:1; background-color: #E5E5E5; border: 1px none #000000; visibility: hidden"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
          <tr> 
            <td width="23%"> 
              <div align="right"></div>
            </td>
            <td colspan="2"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td align="right" width="2%"><html:radio value="N" property="pqueInTipodado" onclick="document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.disabled=false;"/> 
                  </td>
                  <td class="principalLabel" width="11%"> &nbsp;<bean:message key="prompt.numerico"/> 
                  </td>
                  <td class="principalLabel" width="2%"><html:radio value="C" property="pqueInTipodado" onclick="document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.disabled=false;"/></td>
                  <td class="principalLabel" width="8%"> &nbsp;<bean:message key="prompt.texto"/></td>
                  <td class="principalLabel" width="2%"><html:radio value="D" property="pqueInTipodado" onclick="document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.value = '10';document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.disabled=true;"/></td>
                  <td class="principalLabel" width="59%">&nbsp;<bean:message key="prompt.data"/></td>
                </tr>
              </table>
            </td>
            <td width="8%">&nbsp;</td>
          </tr>
          <tr> 
            <td width="23%"> 
              <div align="right"><bean:message key="prompt.tamanho"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
            </td>
            <td width="26%"><html:text property="pqueNrTamanhodado" styleClass="text" maxlength="5" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); if(eval(this.value) > 900){alertaTamanho(); this.value=900; } return false;"/></td>
            <td width="43%">&nbsp;</td>
            <td width="8%">&nbsp;</td>
          </tr>
        </table>
      </div>
  </tr>
  <tr> 
    <td colspan="4" height="175"> 
      <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="3%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="63%"> <bean:message key="prompt.questao"/></td>
          <td class="principalLstCab" width="10%"><bean:message key="prompt.sequencia"/></td>
          <td class="principalLstCab" width="13%"><bean:message key="prompt.encerramento"/></td>
          <td class="principalLstCab" width="11%"><bean:message key="prompt.obrigatorio"/></td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="170"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			/*	
			//document.administracaoCsCatbPesqquestaoPqueForm.idPesqCdPesquisa.disabled= true;	
			//document.administracaoCsCatbPesqquestaoPqueForm.idQuesCdQuestao.disabled= true;	
			document.administracaoCsCatbPesqquestaoPqueForm.pqueNrSequencia.disabled= true;	
			document.administracaoCsCatbPesqquestaoPqueForm.pqueInEncerramento.disabled= true;	
			document.administracaoCsCatbPesqquestaoPqueForm.pqueInObrigatorio.disabled= true;	
			document.administracaoCsCatbPesqquestaoPqueForm.pqueInResposta.disabled= true;	
			document.administracaoCsCatbPesqquestaoPqueForm.pqueInTabulacao.disabled= true;	
			document.administracaoCsCatbPesqquestaoPqueForm.pqueInTipodado.disabled= true;	
			document.administracaoCsCatbPesqquestaoPqueForm.pqueNrTamanhodado.disabled= true;	
			*/
			VerificaClick();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISAQUESTAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCatbPesqquestaoPqueForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISAQUESTAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposPesqquestao();
			}else{
				document.administracaoCsCatbPesqquestaoPqueForm.idPesqCdPesquisa.disabled= false;	
				document.administracaoCsCatbPesqquestaoPqueForm.idQuesCdQuestao.disabled= false;	
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISAQUESTAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISAQUESTAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCatbPesqquestaoPqueForm.imgGravar);	
				desabilitaCamposPesqquestao();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCatbPesqquestaoPqueForm.idPesqCdPesquisa.focus();}
	catch(e){}
</script>