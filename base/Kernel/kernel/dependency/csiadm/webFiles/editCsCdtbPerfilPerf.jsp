<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
function VerificaCampos(){
	if (document.administracaoCsCdtbPerfilPerfForm.perfNrSequencia.value == "0" ){
		document.administracaoCsCdtbPerfilPerfForm.perfNrSequencia.value = "" ;
	}

}

function desabilitaCamposPerfil(){
	document.administracaoCsCdtbPerfilPerfForm.perfDsPerfil.disabled= true;	
	document.administracaoCsCdtbPerfilPerfForm.perfDhInativo.disabled= true;
	document.administracaoCsCdtbPerfilPerfForm.perfNrSequencia.disabled= true;
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(administracaoCsCdtbPerfilPerfForm.idPerfCdPerfil.value);
}


</script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>');VerificaCampos()">

<html:form styleId="administracaoCsCdtbPerfilPerfForm" action="/AdministracaoCsCdtbPerfilPerf.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<input type="hidden" name="limparSessao" value="false"></input>
	
	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idPerfCdPerfil" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="perfDsPerfil" styleClass="text" maxlength="40" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>

        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.sequencia"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="perfNrSequencia" styleClass="text" maxlength="5" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td class="principalLabel" width="13%" align="right">&nbsp;</td>
          <td class="principalLabel" colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="perfDhInativo"/>
				</td>
                <td class="principalLabel" width="17%">
                  &nbsp;<bean:message key="prompt.inativo"/>
                </td>
              </tr>
            </table>
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposPerfil();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_PERFIL_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbPerfilPerfForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_PERFIL_INCLUSAO_CHAVE%>')){
				desabilitaCamposPerfil();
			}else{
				document.administracaoCsCdtbPerfilPerfForm.idPerfCdPerfil.disabled= false;
				document.administracaoCsCdtbPerfilPerfForm.idPerfCdPerfil.value= '';
				document.administracaoCsCdtbPerfilPerfForm.idPerfCdPerfil.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_PERFIL_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_PERFIL_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbPerfilPerfForm.imgGravar);	
				desabilitaCamposPerfil();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbPerfilPerfForm.perfDsPerfil.focus();}
	catch(e){}
</script>