<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsCdtbResptabuladaRetaForm" action="/AdministracaoCsCdtbResptabuladaReta.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <logic:iterate id="ccttrtVector" name="csCdtbResptabuladaRetaVector" indexId="sequencia"> 
  <tr> 
    <td width="5%" class="principalLstPar"> 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>"  onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="idRetaCdResptabulada" />')"> 
    </td>
    <td width="5%" class="principalLstPar"> 
    	&nbsp;<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMARESPTABU_IDRT.xml','<bean:write name="ccttrtVector" property="idRetaCdResptabulada" />')">
    </td>
    <td width="60%" class="principalLstParMao" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idRetaCdResptabulada" />','<bean:write name="ccttrtVector" property="csCdtbTabelaTabeVo.idTabeCdTabela" />')" >
     <bean:write name="ccttrtVector" property="retaDsResptabulada" /> 
    </td>
    <%//Chamado 72934 - Vinicius - Inclus�o do campo RETA_CD_CORPORATIVO %>
     <td width="20%" class="principalLstParMao" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idRetaCdResptabulada" />','<bean:write name="ccttrtVector" property="csCdtbTabelaTabeVo.idTabeCdTabela" />')" >
     &nbsp;<bean:write name="ccttrtVector" property="retaCdCorporativo" /> 
    </td>
    <td width="10%" class="principalLstParMao" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idRetaCdResptabulada" />','<bean:write name="ccttrtVector" property="csCdtbTabelaTabeVo.idTabeCdTabela" />')" >
     &nbsp;<bean:write name="ccttrtVector" property="retaDhInativo" /> 
    </td>
  </tr>
  </logic:iterate>
</table>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_RESPOSTATABULADA_EXCLUSAO_CHAVE%>', editCsCdtbResptabuladaRetaForm.lixeira);
</script>

</html:form>
</body>
</html>