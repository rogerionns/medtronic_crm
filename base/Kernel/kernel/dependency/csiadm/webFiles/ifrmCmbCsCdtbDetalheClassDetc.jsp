<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>


<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/AdministracaoCsAstbMateriaDetalheMadt.do" styleId="administracaoCsAstbMateriaDetalheMadtForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="modo" />
  
  <logic:present name="csCdtbDetalheClassDetcVector">
    <html:select property="idDetcCdDetalheClass" styleClass="principalObjForm">
      <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
      <html:options collection="csCdtbDetalheClassDetcVector" property="idDetcCdDetalheClass" labelProperty="detcDsDetalheClass" />
    </html:select>
    <script>parent.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsDetalheClass.value = "";</script>
  </logic:present>
  <logic:notPresent name="csCdtbDetalheClassDetcVector">
    <html:text property="detcDsDetalheClass" styleClass="text" maxlength="60" />
    <script>
      if (parent.document.administracaoCsAstbMateriaDetalheMadtForm.modo.value == 'busca') {
        alert("<bean:message key="prompt.Nenhuma_descricao_encontrada"/>");
      }
      if (parent.parent.Manifestacao.style.visibility != 'hidden')
	      document.administracaoCsAstbMateriaDetalheMadtForm.detcDsDetalheClass.focus();
    </script>
  </logic:notPresent>

  <script>parent.document.administracaoCsAstbMateriaDetalheMadtForm.modo.value = "";</script>
</html:form>
</body>
</html>