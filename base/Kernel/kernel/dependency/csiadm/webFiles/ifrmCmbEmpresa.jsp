<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>


<%@page import="br.com.plusoft.saas.SaasHelper"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

</head>

<script>
	
var itemAnterior = 0;

function cmbEmpresa_onChange(){
	
	if(confirm('<bean:message key="prompt.confirmarEmpresa"/>')){
		executarReload=true;
	}else{
		document.administracaoEmpresaForm.idEmprCdEmpresa.value=itemAnterior;
	}

	document.administracaoEmpresaForm.tela.value="ifrmCmbEmpresa";
	document.administracaoEmpresaForm.acao.value="editar";
	document.administracaoEmpresaForm.submit();
}

function inicio(){
	itemAnterior=document.administracaoEmpresaForm.idEmprCdEmpresa.value;
	
	window.top.recarregaIfrmConteudo();
	window.top.recarregarLogoTipo();
}

</script>

<body class="principalBgrPageIFRM" text="#000000">

<html:form styleId="administracaoEmpresaForm" action="/AdministracaoEmpresa.do">
	
	<html:hidden property="tela"/>
	<html:hidden property="acao"/>
	<html:hidden property="mostrarSelUmaOpcao"/>
	
	<html:select property="idEmprCdEmpresa" styleClass="principalObjForm" onchange="cmbEmpresa_onChange()">
		<html:options collection="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" labelProperty="emprDsEmpresa"/>
	</html:select>
	<%
      String url = "";
		CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
		try{
			url = Geral.getActionProperty("inicializaSessaoADMCliente", empresaVo.getIdEmprCdEmpresa());
			CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			if(url != null && !url.equals("") && funcVo != null){
				url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();			
			}			
		}catch(Exception e){}
	  %>
	<script>
		var urlEspec = '<%=url%>';
	</script>
          <iframe src="<%=url %>" id="ifrmSessaoEspec" name="ifrmSessaoEspec"  style="display:none"></iframe>  
	
	<%
		long idFuncCdFuncionarioSaas = 0;
		long idEmprCdEmpresaSaas = 0;
	
		if(SaasHelper.aplicacaoSaas) { 
			idFuncCdFuncionarioSaas = new SaasHelper().getSessionData().getIdFuncCdFuncionarioSaas();
			idEmprCdEmpresaSaas = new SaasHelper().getSessionData().getIdEmprCdEmpresaSaas();
		} 
       
		try{			
			url = "../ChatWEB/inicializarSessao.do";
			CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			if(url != null && !url.equals("") && funcVo != null){
				url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();
				
				if(idFuncCdFuncionarioSaas > 0) {
					url+= "&idFuncCdFuncionarioSaas="+idFuncCdFuncionarioSaas;
				}
			}			
		}catch(Exception e){}
	  %>
          <iframe src="<%=url %>" id="ifrmSessaoChat" name="ifrmSessaoChat"  style="display:none"></iframe>  
	

	
</html:form>

	<script type="text/javascript">
		inicio();
	</script>
</body>
</html>

<jsp:include page='<%=fileInclude%>' flush="true"/>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>