<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbPerfilPerfVo"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
%>

<%
	String fileIncludeIdioma = "../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true" />

<html>
<head>
<script language="JavaScript">
<!--
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}


function VerificaCampos(){
	if (document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrSequencia.value == "0" ){
		document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrSequencia.value = "" ;
	}
	if (document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].value == '0' ){
		document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].value = '';
	}
	if (document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].value == '0' ){
		document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].value = '';
	}
	
}
function LimpaCampos(){
	//valdeci, ao clicar no combo de tabela, desmarca os checkboxs
	try{
		document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][0].checked = false;
		document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][1].checked = false;
		document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][2].checked = false;
	}catch(e){}
	
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].value = "";
}

function LimpaTabela(){
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.idTabeCdTabela'].value = "";	
}

function textCounter(field, countfield, maxlimit) {
	if (field.value.length > maxlimit){
		alert("<bean:message key='prompt.tamanhoMaximoCampoAtingido' />");
		field.value = field.value.substring(0, 3998);
	}else {
		countfield.value = maxlimit - field.value.length;
}
}

function textCounter2(field, maxlimit) {
	if (field.value.length > maxlimit){
		alert("<bean:message key='prompt.tamanhoMaximoCampoAtingido' />");
		field.value = field.value.substring(0, maxlimit);
	}
}

function desabilitaCamposDetperfil(){
	document.administracaoCsCdtbDetperfilDtpeForm.dtpeDsDetperfil.disabled= true;	
	document.administracaoCsCdtbDetperfilDtpeForm.dtpeDhInativo.disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm.idPerfCdPerfil.disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrRespostas[0].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrRespostas[1].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrSequencia.disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm.dtpeTxOrientacao.disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm.dtpeDhInativo.disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.idTabeCdTabela'].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreDsTitulo'].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInObrigatorio'][0].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInObrigatorio'][1].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][0].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][1].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][2].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreDsTitulo'].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInObrigatorio'][0].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInObrigatorio'][1].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInTpresposta'][0].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInTpresposta'][1].disabled= true;
	document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInTpresposta'][2].disabled= true;	
}

function validaOption(nVar){
	if(nVar == 'D'){
		document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].disabled= true;
		document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].value = '10';		
	}else if(nVar == 'D2'){
		document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].disabled= true;
		document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].value = '10';		
	}else{
		
		//estava limpando os valores da pergunta errada
		if(nVar == 'C' || nVar == 'N'){
			document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].value = '';	
			document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].disabled= false;	
		}
		
		if(nVar == 'C2' || nVar == 'N2'){
			document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].value = '';	
			document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].disabled= false;
		}
		
	}
}

	function inicio(){
		setaChavePrimaria(administracaoCsCdtbDetperfilDtpeForm.idDtpeCdDetperfil.value);
		
		//se for do tipo data o campo tamanho fica deabilitado.
		try{
			if((document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][2].checked)){
				document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].disabled = true;
			}
			if((document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInTpresposta'][2].checked)){
				document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].disabled = true;
			}
		}catch(e){}
		
	}

//-->
</script>
</head>
	<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

	<script language="JavaScript"
		src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	
	<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<body class="principalBgrPageIFRM"
	onload='showError("<%=request.getAttribute("msgerro")%>");VerificaCampos();inicio()'>

	<html:form styleId="administracaoCsCdtbDetperfilDtpeForm"
		action="/AdministracaoCsCdtbDetperfilDtpe.do">
		<input readonly type="hidden" name="remLen" size="3" maxlength="3"
			value="4000">
		<html:hidden property="modo" />
		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<html:hidden property="topicoId" />
		<html:hidden property="CDataInativo" />
		<html:hidden property="csCdtbSegundaRespTpreVo.idTpreCdTpresposta" />
		<html:hidden property="csCdtbPrimeiraRespTpreVo.idTpreCdTpresposta" />

		<br>

		<table width="95%" border="0" cellspacing="0" cellpadding="0"
			align="center" class="principalLabel">
			<tr>
				<td align="right" class="principalLabel"><bean:message
						key="prompt.codigo" /> <img
					src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td><html:text property="idDtpeCdDetperfil" styleClass="text"
						disabled="true" /></td>
			</tr>
			<tr>
				<td align="right" class="principalLabel"><bean:message
						key="prompt.perfil" /> <img
					src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td><html:select property="idPerfCdPerfil"
						styleId="idPerfCdPerfil" styleClass="principalObjForm">
						<html:option value="">
							<bean:message key="prompt.selecione_uma_opcao" />
						</html:option>
						<logic:present name="csCdtbPerfilPerfVector">
							<logic:iterate name="csCdtbPerfilPerfVector" id="ccppVector">
								<%
									if (Configuracoes.obterConfiguracao(
															ConfiguracaoConst.CONF_APL_MR, request)
															.equals("S")) {
								%>
								<html:option
									value="<%=String
											.valueOf(((CsCdtbPerfilPerfVo) ccppVector)
													.getIdPerfCdPerfil())%>">
									<bean:write name="ccppVector" property="perfDsPerfil" /> (<script>document.write(<bean:write name="ccppVector" property="perfInMr" />?'MR':'PESSOA');</script>)</html:option>
								<%
									} else {
								%>
								<html:option
									value="<%=String
											.valueOf(((CsCdtbPerfilPerfVo) ccppVector)
													.getIdPerfCdPerfil())%>">
									<bean:write name="ccppVector" property="perfDsPerfil" />
								</html:option>
								<%
									}
								%>
							</logic:iterate>
						</logic:present>
					</html:select></td>
			</tr>
			<tr>
				<td align="right" class="principalLabel"><bean:message
						key="prompt.nivelResposta" /> <img
					src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td>
					<table class="principalLabel" border="0" cellspacing="0"
						cellpadding="0">
						<tbody>
							<tr>
								<td><html:radio property="dtpeNrRespostas" value="1"
										onclick="MM_showHideLayers('UmaResp','','show','DuasResp','','hide')" />
									<bean:message key="prompt.uma" /></td>
								<td><html:radio property="dtpeNrRespostas" value="2"
										onclick="MM_showHideLayers('UmaResp','','show','DuasResp','','show')" />
									<bean:message key="prompt.duas" /></td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td align="right" class="principalLabel"><bean:message
						key="prompt.sequencia" /> <img
					src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td>
					<table class="principalLabel" border="0" cellspacing="0"
						cellpadding="0">
						<tr>
							<td><html:text property="dtpeNrSequencia" styleClass="text"
									maxlength="5" onkeypress="mascara(this,soNumeros)"
									onblur="mascara(this,soNumeros); return false;" /></td>
							<td><table width="100%" border="0" cellspacing="0"
									cellpadding="0">
									<tr>
										<td align="right" width="9%"><html:checkbox value="true"
												property="dtpeDhInativo" /></td>
										<td class="principalLabel" width="91%">&nbsp;<bean:message
												key="prompt.inativo" />
										</td>
									</tr>
								</table></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="17%" align="right" class="principalLabel"><bean:message
						key="prompt.descricao" /> <img
					src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td><html:text property="dtpeDsDetperfil" styleClass="text"
						maxlength="40" /></td>
			</tr>
			<tr>
				<td align="right" class="principalLabel"><bean:message
						key="prompt.orientacao" /> <img
					src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				<td><html:textarea property="dtpeTxOrientacao"
						style="width:99%" styleClass="text" rows="5" cols="50"
						onkeypress="textCounter2(this.form.dtpeTxOrientacao,1995)"
						onkeyup="textCounter2(this.form.dtpeTxOrientacao,1995)" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					
					<div id="UmaResp"
						style="left: 25px; top: 218px; width: 640px; height: 78px; z-index: 1; visibility: hidden;margin-bottom: 40px;">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="180" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="principalPstQuadroNormalGrande"><bean:message
													key="prompt.primeiraResposta" /></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalObjAzulForm">
						<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="principalLabel">
										<tr>
											<td width="17%">
												<div align="right">
													<bean:message key="prompt.tabela" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7">
												</div>
											</td>
											<td width="50%"><html:select
													property="csCdtbPrimeiraRespTpreVo.idTabeCdTabela"
													onchange="LimpaCampos()" styleClass="principalObjForm">
													<html:option value="">
														<bean:message key="prompt.selecione_uma_opcao" />
													</html:option>
													<html:options collection="csCdtbTabelaTabeVector"
														property="idTabeCdTabela" labelProperty="tabeDsTabela" />
												</html:select></td>
											<td width="13%">&nbsp;</td>
											<td width="16%">&nbsp;</td>
											<td width="4%">&nbsp;</td>
										</tr>
										<tr>
											<td width="17%">
												<div align="right">
													<bean:message key="prompt.tituloCampo" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7">
												</div>
											</td>
											<td width="50%"><html:text
													property="csCdtbPrimeiraRespTpreVo.tpreDsTitulo"
													styleClass="text" maxlength="40" /></td>
											<td width="13%">
												<div align="right">
													<bean:message key="prompt.obrigatorio" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7">
												</div>
											</td>
											<td width="16%"><html:radio value="true"
													property="csCdtbPrimeiraRespTpreVo.tpreInObrigatorio" /> <bean:message
													key="prompt.sim" /> <html:radio value="false"
													property="csCdtbPrimeiraRespTpreVo.tpreInObrigatorio" /> <bean:message
													key="prompt.nao" /></td>
											<td width="4%">&nbsp;</td>
										</tr>
										<tr>
											<td width="17%">
												<div align="right">
													<bean:message key="prompt.tipoResposta" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7">
												</div>
											</td>
											<td width="50%"><html:radio
													onclick="validaOption('C');LimpaTabela();" value="C"
													property="csCdtbPrimeiraRespTpreVo.tpreInTpresposta" /> <bean:message
													key="prompt.caracter" /> <html:radio
													onclick="validaOption('N');LimpaTabela();" value="N"
													property="csCdtbPrimeiraRespTpreVo.tpreInTpresposta" /> <bean:message
													key="prompt.numerico" /> <html:radio
													onclick="validaOption('D');LimpaTabela();" value="D"
													property="csCdtbPrimeiraRespTpreVo.tpreInTpresposta" /> <bean:message
													key="prompt.data" /></td>
											<td width="13%">
												<div align="right">
													<bean:message key="prompt.tamanho" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7">
												</div>
											</td>
											<td width="16%"><html:text
													property="csCdtbPrimeiraRespTpreVo.tpreNrTamanho"
													styleClass="text" maxlength="5"
													onkeypress="mascara(this,soNumeros)"
													onblur="mascara(this,soNumeros); return false;" /></td>
											<td width="4%">&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<div id="DuasResp"
						style="left: 25px; top: 311px; width: 639px; height: 60px; z-index: 2; visibility: hidden;">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" >
							<tr>
								<td>
									<table width="180" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="principalPstQuadroNormalGrande"><bean:message
													key="prompt.segundaResposta" /></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalObjAzulForm">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="principalLabel">
										<tr>
											<td width="17%">
												<div align="right">
													<bean:message key="prompt.tituloCampo" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7">
												</div>
											</td>
											<td width="50%"><html:text
													property="csCdtbSegundaRespTpreVo.tpreDsTitulo"
													styleClass="text" maxlength="40" /></td>
											<td width="13%">
												<div align="right">
													<bean:message key="prompt.obrigatorio" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7">
												</div>
											</td>
											<td width="16%"><html:radio value="true"
													property="csCdtbSegundaRespTpreVo.tpreInObrigatorio" /> <bean:message
													key="prompt.sim" /> <html:radio value="false"
													property="csCdtbSegundaRespTpreVo.tpreInObrigatorio" /> <bean:message
													key="prompt.nao" /></td>
											<td width="4%">&nbsp;</td>
										</tr>
										<tr>
											<td width="17%">
												<div align="right">
													<bean:message key="prompt.tipoResposta" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7">
												</div>
											</td>
											<td width="50%"><html:radio onclick="validaOption('C2')"
													value="C"
													property="csCdtbSegundaRespTpreVo.tpreInTpresposta" /> <bean:message
													key="prompt.caracter" /> <html:radio
													onclick="validaOption('N2')" value="N"
													property="csCdtbSegundaRespTpreVo.tpreInTpresposta" /> <bean:message
													key="prompt.numerico" /> <html:radio
													onclick="validaOption('D2')" value="D"
													property="csCdtbSegundaRespTpreVo.tpreInTpresposta" /> <bean:message
													key="prompt.data" /></td>
											<td width="13%">
												<div align="right">
													<bean:message key="prompt.tamanho" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7">
												</div>
											</td>
											<td width="16%"><html:text
													property="csCdtbSegundaRespTpreVo.tpreNrTamanho"
													styleClass="text" maxlength="5"
													onkeypress="mascara(this,soNumeros)"
													onblur="mascara(this,soNumeros); return false;" /></td>
											<td width="4%">&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
		<script>
		if(document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrRespostas[0].value == <bean:write name='administracaoCsCdtbDetperfilDtpeForm' property="dtpeNrRespostas"/>)
		{
			MM_showHideLayers('UmaResp','','show','DuasResp','','hide');
		}
		if(document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrRespostas[1].value == <bean:write name='administracaoCsCdtbDetperfilDtpeForm' property="dtpeNrRespostas"/>)
		{
			MM_showHideLayers('UmaResp','','show','DuasResp','','show');
		}
	</script>


	</html:form>
</body>
<logic:equal name="baseForm" property="acao"
	value="<%=Constantes.ACAO_EXCLUIR%>">
	<script>
			desabilitaCamposDetperfil();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao"
	value="<%=Constantes.ACAO_INCLUIR%>">
	<script>
			setPermissaoImageEnable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_DETALHEDOPERFIL_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbDetperfilDtpeForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_DETALHEDOPERFIL_INCLUSAO_CHAVE%>')){
				desabilitaCamposDetperfil();
			}else{
				document.administracaoCsCdtbDetperfilDtpeForm.idDtpeCdDetperfil.disabled= false;
				document.administracaoCsCdtbDetperfilDtpeForm.idDtpeCdDetperfil.value= '';
				document.administracaoCsCdtbDetperfilDtpeForm.idDtpeCdDetperfil.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao"
	value="<%=Constantes.ACAO_EDITAR%>">
	<script>
			if (!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_DETALHEDOPERFIL_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_DETALHEDOPERFIL_ALTERACAO_CHAVE%>',
					parent.document.administracaoCsCdtbDetperfilDtpeForm.imgGravar);
			desabilitaCamposDetperfil();
		}
	</script>
</logic:equal>

</html>

<script>
	try {
		document.administracaoCsCdtbDetperfilDtpeForm.idPerfCdPerfil.focus();
	} catch (e) {
	}
</script>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>