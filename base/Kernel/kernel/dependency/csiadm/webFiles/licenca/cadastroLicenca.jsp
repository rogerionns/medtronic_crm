<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html>
	<head>
		<title><bean:message key="prompt.licenca.title"/></title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
		<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
		<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
		<% } %>
	</head>

	<body class="tela nomargin">
		<div id="aguarde" style="display: none;">
			<div class="block"></div>
			<div class="aguarde"></div>
		</div>
		
		<div class="frame shadow margin" style="width: 99%; height: 95%;">
			<div class="title"><plusoft:message key="prompt.gerenciamentoLicenca" /></div>
				
			<div class="innerframe">
			<div class="list topmargin">
				<table width="100%">
					<thead>
						<tr>
							<td width="125px">&nbsp;<plusoft:message key="prompt.aplicacao" /></td>
							<td width="80px"><plusoft:message key="prompt.modulo" /></td>
							<td width="90px"><plusoft:message key="prompt.numSerie" /></td>
							<td width="60px"><plusoft:message key="prompt.numLicenca" /></td>
							<td width="60px" align="center"><plusoft:message key="prompt.data" /></td>
							<td width="90px" align="center"><plusoft:message key="prompt.numDias" /></td>
							<td width="60px" ><plusoft:message key="prompt.empresa" /></td>
							<td width="90px" align="center"><plusoft:message key="prompt.tipo" /></td>
							
						</tr>
					</thead>
				</table>
				<div id="scrolllicenca" class="scrolllist" style="height: 120px;">
					<table id="licencas" width="100%">
						<tbody>
							<logic:present name="licencaVector"><logic:iterate id="psf1" name="licencaVector">
							<tr idPsf1CdPlusoft3="<bean:write name="psf1" property="idPsf1CdPlusoft3" />">
								<td width="145px">&nbsp;<plusoft:acronym name="psf1" property="idPsf1CdPlusoft5" length="25" /></td>
								<td width="100px"><bean:write name="psf1" property="idPsf1CdPlusoft6" /></td>
								<td width="100px"><bean:write name="psf1" property="idPsf1CdPlusoft3" /></td>
								<td width="70px"><bean:write name="psf1" property="idPsf1CdPlusoft7" /></td>
								<td width="80px" align="center"><bean:write name="psf1" property="idPsf1CdPlusoft9" /></td>
								<td width="90px" align="center">
									<logic:equal name="psf1" property="idPsf1CdPlusoft8" value="0">
										Permanente
									</logic:equal>
									<logic:notEqual name="psf1" property="idPsf1CdPlusoft8" value="0">
										<bean:write name="psf1" property="idPsf1CdPlusoft8" />
									</logic:notEqual>
								</td>
								<td width="90px">
								
									<logic:empty name="psf1" property="idPsf1CdPlusoft11">
										<bean:message key="prompt.licenca.compartilhada"/>
									</logic:empty>
									<logic:notEmpty name="psf1" property="idPsf1CdPlusoft11">
										<plusoft:acronym name="psf1" property="idPsf1CdPlusoft11" length="12" />
									</logic:notEmpty>
								
									
								</td>
								<td width="90px" align="center">
									<logic:equal name="psf1" property="idPsf1CdPlusoft4" value="N">
										<plusoft:message key="prompt.nominal" />
									</logic:equal>
									<logic:notEqual name="psf1" property="idPsf1CdPlusoft4" value="N">
										<plusoft:message key="prompt.concorrente" />
									</logic:notEqual>
								</td>
							</tr>
							</logic:iterate></logic:present>
						</tbody>
					</table>
				</div>
			</div>
			
			<div id="importacao" class="frame shadow topmargin" style="width: 100%; height: 300px;">
				<div class="title"><plusoft:message key="prompt.importacao" /></div>
				
				<div class="innerframe">
			
				<div style="width: 300px; height: 40px; ">
					<div id="uploaddiv">
						<plusoft:message key="prompt.cadastroLicenca.selecioneUmArquivo" /><br/><input class="text" type="file" name="licfile" id="licfile" />
					</div>
					<div id="aguardediv" class="topmargin" style="display: none; ">
						<span class="img aguardeimg left"></span>&nbsp;&nbsp;Aguarde ...
					</div>
				</div>
				
				<div id="detalhe" class="detalhe topmargin" style="width: 600px; display: none; ">
					<b>Detalhes:</b><br/>
					
					<div class="list topmargin" style="width: 700px;">
						<table width="100%">
							<thead>
								<tr>
									<td width="250px">&nbsp;<plusoft:message key="prompt.aplicacao" /></td>
									<td width="120px">&nbsp;<plusoft:message key="prompt.modulo" /></td>
									<td width="100px"><plusoft:message key="prompt.numLicenca" /></td>
									<td><plusoft:message key="prompt.empresa" /></td>
								</tr>
							</thead>
						</table>
						<div class="scrolllist" style="height: 120px;">
							<table width="100%" id="detalhelicenca">	
								<tbody>
								
								</tbody>
							</table>
						</div>
					</div>
					
					<a class="gravar topmargin" title="<plusoft:message key="prompt.gravar" />"></a>
					
				</div>
				</div>
			</div>
			</div>
		</div>
		
		<div class="right margin" id="sair" title="<plusoft:message key="prompt.sair" />">
			<a href="javascript:window.close();" class="sair"></a>
		</div>
		
		<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-upload.js"></script>
		<script type="text/javascript">
			var dadoslicenca = {};

			/**
			  * 
			  * Se as licen�as forem todas compartilhadas, a empresa j� vira "Compartilhada" por padr�o e a valida��o n�o ter� efeito
			  * N�o � permitido gravar se o usu�rio n�o selecionou nenhuma op��o.
			  * Cada empresa deve ter somente 1 n�mero de s�rie para cada m�dulo
			  * Ou seja, n�o � permitido selecionar mais de uma licen�a, para o mesmo m�dulo, para a mesma empresa.
			  * 
			  * @author jvarandas
			  * @since  28/01/2011
			  */
			var gravando = false;
			var gravarLicenca = function() {
				if(gravando) return false;
				gravando = true;
				
				// Se j� tiverem licen�as gravadas, confirme se quer mesmo sobrescrever
				if($("#licencas tr").length > 0) {
					if(!confirm("<bean:message key='prompt.temCertezaGravarLicencas' />")) {
						gravando=false;
						return false;
					}
				}

				// Mostra o aguarde
				$("#aguarde").css("display", "");

				// Executa o Post dos dados das licen�as
				$.post("GravarLicenca.do", dadoslicenca, function(ret) {

					// Se retornou erro, exibe um alerta e esconde o aguarde
					if(ret.msgerro!=undefined) {
						gravando=false;
						alert(ret.msgerro);
						$("#aguarde").css("display", "none");
	                } else {

		                // Se as licen�as foram registradas, d� um refresh na tela para recarregar as licen�as
		                alert("<bean:message key='prompt.licencasForamRegistradasComSucesso'/>");

						if(window.name=="ifrmConteudo") {
			                document.location.reload();
						} else {
							window.close();
						}
	                }
				}, "json");
			};
			
			$(document).ready(function() {
				$("#licencas tr:odd").css('background-color', '#cfdbe7');


				if(window.top.getPermissao!=undefined && !window.top.getPermissao("adm.util.gerenciamentolicencas.inclusao")) {
					$("#importacao").hide();
					$("#scrolllicenca").css("height", "400px");
				}

				if(window.name=="ifrmConteudo") {
					$("#sair").hide();
				}

				$(".gravar").click(gravarLicenca);
				
				$('#licfile').change(function() {
					if($(this).val=="") return;
					
					$("#uploaddiv").hide();
					$("#aguardediv").show();
					
		            $(this).upload('ImportarLicenca.do', function(res) {
		            	$("#uploaddiv").show();
						$("#aguardediv").hide();
						
	                	if(res.msgerro!=undefined) {
			                alert(res.msgerro);
			                $("#licfile").val("");
		                } else {
		                	$("#detalhe").css("display", "");

							dadoslicenca = res;
		                	
		                	var modempresa = "";
		                	var list = document.getElementById("detalhelicenca");
		                	while(list.rows.length > 0) list.deleteRow(list.rows.length-1);

		                	
		                	for(var k in dadoslicenca.modulos) {
			                	var modulo = dadoslicenca.modulos[k];

			                	if(modulo.multi_empresa=="S") {
									modempresa = "&nbsp;"; 
									modulo.codigoempresaplusoft="";
			                	} else {
			                		modempresa = "<bean:message key="prompt.licenca.compartilhada"/>";
			                		modulo.codigoempresaplusoft="0";
			                	}
			                	
			                	$("#detalhelicenca").find("tbody").append($("<tr>").attr("indice", k)
			                			.append($("<td>").css("width", "250px").html("&nbsp;"+modulo.ds_apli))
			                			.append($("<td>").css("width", "120px").text(modulo.ds_modu))
			    						.append($("<td>").css("width", "100px").text(modulo.nr_lic))
			    						.append($("<td id=\"tdempresa"+k+"\">").html(modempresa))
			    				);

			                	if(modulo.multi_empresa=="S") {
	   								$("<select>").addClass("cmbempresa").addClass("text").css("width", "200px")
					                	.append(   $("<option>").text("<plusoft:message key="prompt.selecione_uma_opcao" />")   )
										.append(   $("<option>").val("0").text("<bean:message key="prompt.licenca.compartilhada"/>")	) <logic:present name="empresaVector"><logic:iterate id="empresa" name="empresaVector">
										.append(   $("<option>").val("<bean:write name="empresa" property="idEmprCdEmpresa" />").text("<bean:write name="empresa" property="emprDsEmpresa" />")	) </logic:iterate></logic:present>
										.appendTo( $("#tdempresa"+k) );
			                	}
					            
			                	$("#detalhelicenca tr:odd").css('background-color', '#cfdbe7');
		                	}

		                	
		                }

		                $(".cmbempresa").change(function(e) {
			                dadoslicenca.modulos[this.parentNode.parentNode.getAttribute("indice")].codigoempresaplusoft=this.value;
			            });
		            }, 'json');
		        });
			});
		</script>
		
		
	</body>
</html:html>
