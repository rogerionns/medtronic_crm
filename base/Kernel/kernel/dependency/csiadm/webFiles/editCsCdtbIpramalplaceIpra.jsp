<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposAnexoMail(){
		document.administracaoCsCdtbIpramalplaceIpraForm.ipraDsIp.disabled= true;	
		document.administracaoCsCdtbIpramalplaceIpraForm.ipraDhInativo.disabled= true;
		document.administracaoCsCdtbIpramalplaceIpraForm.ipraDsPlace.disabled= true;
	}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbIpramalplaceIpraForm" action="/AdministracaoCsCdtbIpramalplaceIpra.do" enctype="multipart/form-data">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<html:hidden property="ipraDsIpAnterior" />
	<html:hidden property="ipraDsPlaceAnterior" />
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="14%"> <html:text property="idIpraCdIpramalplace" styleClass="text" disabled="true" /> 
    </td>
    <td width="43%">&nbsp;</td>
    <td width="23%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.ip"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="ipraDsIp" styleClass="text" maxlength="15" /> 
    </td>
    <td width="23%">&nbsp;</td>
  </tr>
<!--
  <tr> 
    <td width="20%"> 
      <div align="right"><bean:message key="prompt.ramal"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="2">
	     <html:text property="ipraDsRamal" styleClass="text" maxlength="4"/> 
    </td>
    <td width="23%">&nbsp;</td>
  </tr>
-->
  <tr> 
    <td width="20%"> 
      <div align="right"><bean:message key="prompt.place"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="2"><html:text property="ipraDsPlace" styleClass="text" maxlength="20" /> 
    </td>
    <td width="23%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%"> 
      <div align="right">&nbsp;</div>
    </td>
    <td colspan="3">
    </td>
  </tr>
  <tr> 
    <td width="20%">&nbsp;</td>
    <td width="14%">&nbsp;</td>
    <td class="principalLabel" width="43%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="83%"> <html:checkbox value="true" property="ipraDhInativo"/></td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/></td>
        </tr>
      </table>
    </td>
    <td width="23%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposAnexoMail();				
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_IPPLACE_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbIpramalplaceIpraForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_IPPLACE_INCLUSAO_CHAVE%>')){
				desabilitaCamposAnexoMail();
			}
			document.administracaoCsCdtbIpramalplaceIpraForm.idIpraCdIpramalplace.disabled= false;
			document.administracaoCsCdtbIpramalplaceIpraForm.idIpraCdIpramalplace.value= '';
			document.administracaoCsCdtbIpramalplaceIpraForm.idIpraCdIpramalplace.disabled= true;
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_IPPLACE_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_IPPLACE_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbIpramalplaceIpraForm.imgGravar);
				desabilitaCamposAnexoMail();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbIpramalplaceIpraForm.ipraDsIp.focus();}
	catch(e){}
</script>