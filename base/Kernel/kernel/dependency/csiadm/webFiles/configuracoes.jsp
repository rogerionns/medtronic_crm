<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsDmtbConfiguracaoConfVo"%>
<%@ page import="java.util.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");

// 91698 - 30/10/2013 - Jaider Alba
boolean isW3c = br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request);
%>

<html>
<head>
<title>Dados Banc&aacute;rios</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/configuracoes.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>

<!-- 91698 - 30/10/2013 - Jaider Alba -->
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<link rel="stylesheet" href="/plusoft-resources/css/bootstrap/bootstrap.min.css">
<script language="JavaScript" src="/plusoft-resources/javascripts/jquery-1.9.1.js"></script>
<script src="/plusoft-resources/javascripts/jquery-bootstrap.min.js"></script>
<!--[if lt IE 9]>
  <script src="/plusoft-resources/javascripts/html5shiv.js"></script>
<![endif]-->

<script>
// 91697 - 21/10/2013 - Jaider Alba
function carregaCombo(idConfCdConfig){
	
	var obj = document.getElementById('cmbParent'+idConfCdConfig);
	var value = obj.options[obj.selectedIndex].value;
	var chave = document.getElementById('confDsChave'+idConfCdConfig).value;
	var oSelect = document.getElementById('cmbValue'+idConfCdConfig);
	var oOpt;
	var oDesc;
	
	while(oSelect.hasChildNodes()){
		oSelect.removeChild(oSelect.firstChild);	
	}
	
	if(value != null){
		try{		
			var dadospost = { 
					optValue : value,
					dsChave : chave
				};
			
			$.post("/csiadm/GenericAdmGetConfiguracaoCombo.do", dadospost, function(ret) {					
				if(ret.msgerro!=undefined) {
					alert("<bean:message key='prompt.erro_carregar_configuracao' /> "+ret.msgerro);
					return false;						
				} else if(ret.opts!=null && ret.opts!=undefined) {
					
					for(var x = 0; x < ret.opts.length; x++){
													
						oOpt = document.createElement('option');
						oOpt.setAttribute('value', ret.opts[x][0]);
						
						oDesc = document.createTextNode(ret.opts[x][1]);
						
						oOpt.appendChild(oDesc);
						oSelect.appendChild(oOpt);							
					}
				}
				
				oOpt = document.createElement('option');
				oOpt.setAttribute('value','');
				
				oDesc = document.createTextNode('<bean:message key="prompt.selecione_uma_opcao" />');
				
				oOpt.appendChild(oDesc);					
				oSelect.insertBefore(oOpt, oSelect.hasChildNodes() ? oSelect.childNodes[0] : null);
				oSelect.selectedIndex = 0;
				
			}, "json");
		}
		catch(x){		
			alert("Erro em getConfiguracaoCombo()"+ x.description);		
		}
	}
}
	
</script>

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>

<script>
function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 
  function AtivarPasta(pasta) {
	switch (pasta) {
	
		case 'HOME':
			SetClassFolder('tr01','principalLstSelected');	
			SetClassFolder('tr02','principalLstImpar');
			SetClassFolder('tr03','principalLstPar');	
			SetClassFolder('tr04','principalLstImpar');
			SetClassFolder('tr05','principalLstPar');
			SetClassFolder('tr06','principalLstImpar');
														
			break;
			
		case 'TAREFAS':
			SetClassFolder('tr01','principalLstPar');	
			SetClassFolder('tr02','principalLstSelected');
			SetClassFolder('tr03','principalLstPar');	
			SetClassFolder('tr04','principalLstImpar');
			SetClassFolder('tr05','principalLstPar');
			SetClassFolder('tr06','principalLstImpar');

			break;
	}	
}
	
	function findConfigById(idGrupoConf,dsGrupo){
				
			if (idGrupoConf > 0){
				administracaoCsDmtbConfiguracaoConfForm.tela.value = 'configuracoes';
				administracaoCsDmtbConfiguracaoConfForm.acao.value = 'allConfiguracoes';
				administracaoCsDmtbConfiguracaoConfForm['csDmtbConfiguracaoConfVo.idGrcoCdGrupoconfig'].value = idGrupoConf;
				administracaoCsDmtbConfiguracaoConfForm.lblMenu.value = dsGrupo;
				administracaoCsDmtbConfiguracaoConfForm.submit();

			}else{
				alert('<bean:message key="prompt.selecioneGrupoConfiguracaoPrimeiro" />.');
				return false;
			}
	}
	
	
	function refreshMenu(){
		if(administracaoCsDmtbConfiguracaoConfForm.lblMenu.value==''){
			setValue(document.all.item('lblChave'), 'ENVIO');
		}else{
			setValue(document.all.item('lblChave'), administracaoCsDmtbConfiguracaoConfForm.lblMenu.value);
		}
		 
		if(administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.idGrcoCdGrupoconfig"].value > 0){
			document.getElementById("registro" + administracaoCsDmtbConfiguracaoConfForm["csDmtbConfiguracaoConfVo.idGrcoCdGrupoconfig"].value).className = 'principalObjAzulForm';
		}
		
		desabilitaListaEmpresas();


		//Chamado 105505 - 01/12/2015 Victor Godinho
		if (document.documentMode != undefined) {
			try {
				if (parseInt(document.documentMode) > 6) {
					document.getElementById("idMenuGrupo").style.overflow = "scroll";
				}
			}catch(e){}
		}
	}
	
	function limpaTela(){
			
			/*administracaoCsDmtbConfiguracaoConfForm.tela.value = 'configuracoes';
			administracaoCsDmtbConfiguracaoConfForm.acao.value = 'sairconfiguracoes';
			administracaoCsDmtbConfiguracaoConfForm['csDmtbConfiguracaoConfVo.idGrcoCdGrupoconfig'].value = 0;
			administracaoCsDmtbConfiguracaoConfForm.lblMenu.value = '';
			administracaoCsDmtbConfiguracaoConfForm.target = 'seguratela';
			administracaoCsDmtbConfiguracaoConfForm.submit();*/

			window.top.ifrmConteudo.location.href = "AdministracaoCsDmtbConfiguracaoConf.do";
	}
	
	function limpaConf(){
			if(this.name == "seguratela"){
				parent.limpaConf();
			}else{
				administracaoCsDmtbConfiguracaoConfForm.tela.value = 'configuracoes';
				administracaoCsDmtbConfiguracaoConfForm.acao.value = '';
				administracaoCsDmtbConfiguracaoConfForm.target = "";
				administracaoCsDmtbConfiguracaoConfForm['csDmtbConfiguracaoConfVo.idGrcoCdGrupoconfig'].value='';
				administracaoCsDmtbConfiguracaoConfForm.submit();
			}
	}
	
	function atualizaRegistros(){
			administracaoCsDmtbConfiguracaoConfForm.tela.value = 'configuracoes';
			administracaoCsDmtbConfiguracaoConfForm.acao.value = 'updconfiguracoes';
			administracaoCsDmtbConfiguracaoConfForm['csDmtbConfiguracaoConfVo.idGrcoCdGrupoconfig'].value = 0;
			administracaoCsDmtbConfiguracaoConfForm.lblMenu.value = '';
			administracaoCsDmtbConfiguracaoConfForm.submit();
	}
	
</script>
</head>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showQuestion('<%=request.getAttribute("msgQuestion")%>');showError('<%=request.getAttribute("msgerro")%>');refreshMenu()">
<html:form styleId="administracaoCsDmtbConfiguracaoConfForm" action="/AdministracaoCsDmtbConfiguracaoConf.do">
<html:hidden property="tela" />
<html:hidden property="acao" />
<html:hidden property="csDmtbConfiguracaoConfVo.idGrcoCdGrupoconfig" />
<html:hidden property="lblMenu" />

<table border="0" cellspacing="0" cellpadding="0" style="width: 100%; height: 100%;">
  <tr valign="top"> 
    <td width="20%">
      <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro"  height="100%">
        <tr valign="top" style="height: 20px"> 
          <td width="100%" colspan="2"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" >
              <tr> 
              <td class="principalPstQuadro" height="2" width="100%">Configura��es</td>
              </tr>
            </table>
          </td>
        </tr>
		<tr valign="top">
		  <td valign="top"> 
		  <div id="idMenuGrupo" style="position:relative; width:100%; height: 100%; overflow: auto;" > 
			<table width="98%" cellspacing="3" cellpadding="3" align="center" class="principalLabel">
				<logic:present name="csDmtbGrupoConfGrcoVector">
				  <logic:iterate id="ccttrtVector" name="csDmtbGrupoConfGrcoVector" indexId="sequencia">
					  <tr id="registro<bean:write name="ccttrtVector" property="idGrcoCdGrupoConfig"/>" class="geralCursoHand" onclick="findConfigById('<bean:write name="ccttrtVector" property="idGrcoCdGrupoConfig"/>','<bean:write name="ccttrtVector" property="grcoDsGrupoConfig"/>')"> 
						<td name="tdLista" id="tdLista"><bean:write name="ccttrtVector" property="grcoDsGrupoConfig"/>&nbsp<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
					  </tr>
				 </logic:iterate>
				</logic:present>
			</table>
			</div>
		  </td>
		</tr>
 	  </table>
    </td>
    <td>
      <!-- Jonathan Costa | FullScreen 12/2013 --> 	 
      <div id="configuracoes" style="position:relative; width:100%; height:100%; z-index:1; visibility: visible; border: 0px solid #00F5FF;"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0"  height="100%">
            <tr> 
              <td width="100%" colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                  <tr> 
                  	<td class="principalPstQuadro" height="17" width="100%"><span id="lblChave" name="lblChave"></span></td>
                    <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr> 
              
            <td class="principalBgrQuadro" valign="top"> 
             <!-- Jonathan Costa | FullScreen 12/2013 -->
              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                  <tr> 
                  <td valign="top"> 
                  	<!-- Jonathan Costa | FullScreen 12/2013 -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0"  height="100%">
                        <tr valign="top" height="10px"> 
                          <td> 
                           <!-- Jonathan Costa | FullScreen 12/2013 -->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                              <tr> 
                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <!-- Jonathan Costa | FullScreen 12/2013 -->
<!--                      </table>-->
                      
<!--                    <table width="100%"  cellspacing="0" cellpadding="0" align="center" bordercolor="orange" height="">-->
                      <tr> 
                        <td height="5" valign="top"><font size="1"></font></td>
                      </tr>
                      <tr valign="top"> 
                        <td valign="top">
                          <!-- Jonathan Costa | FullScreen 12/2013 -->	
                          <div id="campos" style="position: relative; width:100%; height:400px; z-index:2; overflow: auto;" > 
                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
									  <%
									  Vector confVector = (Vector)request.getAttribute("CsDmtbConfiguracaoConfVector");
									  
									  if (confVector!= null && confVector.size()>0){
											CsDmtbConfiguracaoConfVo confVo;
											for(int i = 0; i < confVector.size(); i++){
											  	confVo = (CsDmtbConfiguracaoConfVo)confVector.get(i);
											  	
											  	long idCdConfig = 0;
											  	long nrMaxLength = 0;
											  	double nrNumber = 0;
											  	String inTipo = "";
											  	String inTipoCombo = "";
											  	String descHtml = "";
											  	String tipoHtml = "";
											  	String valCampo = "";
											  	String dhData = "";
											  	String hHora = "";
											  	String dsMaskara = ""; 
											  	
											  	idCdConfig = confVo.getIdConfCdConfiguracao();
											  	nrMaxLength = confVo.getConfNrMaxlen();
											  	inTipo = confVo.getConfInTipocampo();
											  	inTipoCombo = confVo.getConfInTpcombo();
											  	dsMaskara = confVo.getConfDsMascara();
											  	descHtml = confVo.getConfDsConfiguracao();
											  	
											  	// 91697 - 21/10/2013 - Jaider Alba
											  	String txAjuda = confVo.getConfTxAjuda();
											  	String confDsChave = confVo.getConfDsChave().substring(0, confVo.getConfDsChave().indexOf("@"));
												
											  	if (inTipoCombo.equals("N")){  												  
													  if (inTipo.equals("C")){
    	    												valCampo = confVo.getConfDsValstring()!=null?confVo.getConfDsValstring():"";
   															tipoHtml = "<input type='text' style='width:400px;' name='arrTxtValores' class='principalObjForm' maxlength='"+nrMaxLength+"' value='"+valCampo+"'>";
   															tipoHtml += "<input type='hidden' name='arrCodTipo' class='principalObjForm' value='"+idCdConfig+"_"+inTipo+"_"+inTipoCombo+"'>";
   															tipoHtml += "<input type='hidden' name='arrTxtValAnterior' class='principalObjForm' value='"+valCampo+"'>";
   															tipoHtml += "<input type='hidden' name='arrFrtMaskara' class='principalObjForm' value='"+dsMaskara+"'>";
													  }
													  if (inTipo.equals("D")){
														    if(dsMaskara.equals("DD")){
															    dhData = confVo.getConfDhValdatetime().substring(0,2);
														    }else if(dsMaskara.equals("DD/MM")){
																dhData = confVo.getConfDhValdatetime().substring(0,5);													    
														    }else if(dsMaskara.equals("DD/MM/YYYY")){
															    dhData = confVo.getConfDhValdatetime().substring(0,10);
														    }
														    tipoHtml = "<input type='text' style='width:400px;' name='arrTxtValores' class='principalObjForm' maxlength='"+nrMaxLength+"' onkeypress='validaDigito(\""+dsMaskara+"\",this)' onBlur='verificaDataMask(\""+dsMaskara+"\",this)' value='"+dhData+"'>";
   															tipoHtml += "<input type='hidden' name='arrCodTipo' class='principalObjForm' value='"+idCdConfig+"_"+inTipo+"_"+inTipoCombo+"'>";
   															tipoHtml += "<input type='hidden' name='arrTxtValAnterior' class='principalObjForm' value='"+dhData+"'>";
   															tipoHtml += "<input type='hidden' name='arrFrtMaskara' class='principalObjForm' value='"+dsMaskara+"'>";
													  }
													  if (inTipo.equals("T")){
														    if(dsMaskara.equals("HH:MM")){
															    hHora = confVo.getConfDhValdatetime().substring(13,18);														    
														    }else if(dsMaskara.equals("HH:MM:SS")){
															    hHora = confVo.getConfDhValdatetime().substring(13,21);													    
														    }else if(dsMaskara.equals("MM:SS")){
    														    hHora = confVo.getConfDhValdatetime().substring(16,21);
														    }
														    tipoHtml = "<input type='text' style='width:400px;' name='arrTxtValores' class='principalObjForm' maxlength='"+nrMaxLength+"' onkeypress='validaDigitoHora(\""+dsMaskara+"\",this)' onblur='validaHora(\""+dsMaskara+"\",this)' value='"+hHora+"'>";
													     	tipoHtml += "<input type='hidden' name='arrCodTipo' class='principalObjForm' value='"+idCdConfig+"_"+inTipo+"_"+inTipoCombo+"'>";
													     	tipoHtml += "<input type='hidden' name='arrTxtValAnterior' class='principalObjForm' value='"+hHora+"'>";
													     	tipoHtml += "<input type='hidden' name='arrFrtMaskara' class='principalObjForm' value='"+dsMaskara+"'>";
													  }
													  if (inTipo.equals("N")){
													  		nrNumber = confVo.getConfNrValnumber();
														    tipoHtml = "<input type='text' style='width:400px;' name='arrTxtValores' class='principalObjForm' maxlength='"+nrMaxLength+"' value='"+nrNumber+"'>";
													     	tipoHtml += "<input type='hidden' name='arrCodTipo' class='principalObjForm' value='"+idCdConfig+"_"+inTipo+"_"+inTipoCombo+"'>";
													     	tipoHtml += "<input type='hidden' name='arrTxtValAnterior' class='principalObjForm' value='"+nrNumber+"'>";
													     	tipoHtml += "<input type='hidden' name='arrFrtMaskara' class='principalObjForm' value='"+dsMaskara+"'>";
													  }
													  if (inTipo.equals("L")){
														  	nrNumber = confVo.getConfNrValnumber();
														    tipoHtml = "<input type='text' style='width:400px;' name='arrTxtValores' class='principalObjForm' maxlength='"+nrMaxLength+"' value='"+nrNumber+"'>";
													     	tipoHtml += "<input type='hidden' name='arrCodTipo' class='principalObjForm' value='"+idCdConfig+"_"+inTipo+"_"+inTipoCombo+"'>";
													     	tipoHtml += "<input type='hidden' name='arrTxtValAnterior' class='principalObjForm' value='"+nrNumber+"'>";
													     	tipoHtml += "<input type='hidden' name='arrFrtMaskara' class='principalObjForm' value='"+dsMaskara+"'>";
													  }
													  if (inTipo.equals("P")){
														  	valCampo = confVo.getConfDsValstring()!=null?confVo.getConfDsValstring():"";
 															tipoHtml = "<input type='password' style='width:400px;' name='arrTxtValores' class='principalObjForm' maxlength='"+nrMaxLength+"' value='"+valCampo+"'>";
 															tipoHtml += "<input type='hidden' name='arrCodTipo' class='principalObjForm' value='"+idCdConfig+"_"+inTipo+"_"+inTipoCombo+"'>";
 															tipoHtml += "<input type='hidden' name='arrTxtValAnterior' class='principalObjForm' value='"+valCampo+"'>";
 															tipoHtml += "<input type='hidden' name='arrFrtMaskara' class='principalObjForm' value='"+dsMaskara+"'>";
													  }
													  
											  	}else if (inTipoCombo.equals("F")){
											  			
											  			String dsOpcoescombo = confVo.getConfDsOpcoescombo(); 
														StringTokenizer st = new StringTokenizer(dsOpcoescombo, "|");
			
														
														tipoHtml = "<select name='arrCmbValores' class='principalObjForm' style='width:400px;'>";
														//tipoHtml = tipoHtml + "<option value=''>-- Selecione uma op��o --</option>";
														
														while (st.hasMoreTokens()){
															String valCombo = st.nextToken();
															
															String valueCombo = "";
															String descCombo = "";
															
															if(valCombo != null && !valCombo.equals("")){
																if(valCombo.indexOf("(")>-1 && valCombo.indexOf(")") >-1){
																	valueCombo = valCombo.substring(valCombo.indexOf("(")+1,valCombo.indexOf(")"));
																	descCombo = valCombo.substring(0,valCombo.indexOf("("));
																}
																if(!valueCombo.equals("") && !descCombo.equals("")){
																	valCampo = confVo.getConfDsValstring();
																	if(valCampo.equals(valueCombo)){
																		tipoHtml = tipoHtml + "<option selected value='"+valueCombo+"'>"+descCombo+"</option>";
																	}else{
																		tipoHtml = tipoHtml + "<option value='"+valueCombo+"'>"+descCombo+"</option>";
																	}
																}else{
																	valCampo = confVo.getConfDsValstring();
																	if(valCampo.equals(valCombo)){
																		tipoHtml = tipoHtml + "<option selected value='"+valCombo+"'>"+valCombo+"</option>";
																	}else{
																		tipoHtml = tipoHtml + "<option value='"+valCombo+"'>"+valCombo+"</option>";
																	}
																}
															}
														}
														 
														tipoHtml = tipoHtml + "</select>";
														tipoHtml = tipoHtml + "<input type='hidden' name='arrTxtValores' class='principalObjForm'>";
														tipoHtml = tipoHtml + "<input type='hidden' name='arrCodTipo' class='principalObjForm' value='"+idCdConfig+"_"+inTipo+"_"+inTipoCombo+"'>"; 
														tipoHtml = tipoHtml + "<input type='hidden' name='arrFrtMaskara' class='principalObjForm' value='"+dsMaskara+"'>";
														
												}else if (inTipoCombo.equals("D")){
											  			//Sera implementado depois
											  			//br.plusoft
											  	}
											  	// 91697 - 21/10/2013 - Jaider Alba
												else if (inTipoCombo.equals("Q")){
													
													String selected = "";
													String changeFunction = "";
													
													if(confVo.getComboParent() != null){
														
														tipoHtml+= "<input type='hidden' id='confDsChave"+idCdConfig+"' value='"+confDsChave+"' />";
														
														changeFunction = (confVo.getCombo() != null) 
																? " onChange=\"javascript:carregaCombo('"+idCdConfig+"');\" " : "";
																
														tipoHtml+= "<select name='arrCmbValores' id='cmbParent"+idCdConfig+"' class='principalObjForm' style='width:196px;' "+changeFunction+">";
														tipoHtml+= "<option value=''>-- Selecione uma op��o --</option>";
														
														for(int x = 0; x < confVo.getComboParent().size(); x++){
															
															String[] option = (String[])confVo.getComboParent().get(x);
															
															selected = (confVo.getConfDsValStringParent()!=null 
																	&& confVo.getConfDsValStringParent().equals(option[0])) ? "selected" : "";
															
															tipoHtml+= "<option value='"+option[0]+"' "+selected+">"+option[1]+"</option>";
														}
														
														tipoHtml+= "</select>";
													}
													
													if(confVo.getCombo() != null){
														
														String style = (!changeFunction.equals("")) 
																? "style='width:196px; margin-left: 8px;'" 
																: "style='width:400px;'"; 
														
														tipoHtml+= "<input type='hidden' name='arrCodTipo' value='"+idCdConfig+"_"+inTipo+"_"+inTipoCombo+"' />";
														
														if(confVo.getComboParent() == null){
															tipoHtml+= "<input type='hidden' name='arrCmbValores' value='' />";
														}
														
														tipoHtml+= "<select name='arrTxtValores' id='cmbValue"+idCdConfig+"' class='principalObjForm' "+style+">";
														tipoHtml+= "<option value=''>-- Selecione uma op��o --</option>";
																													
														for(int x = 0; x < confVo.getCombo().size(); x++){
															
															String[] option = (String[])confVo.getCombo().get(x);
															
															selected = (confVo.getConfDsValstring().equals(option[0])) ? "selected" : "";
																															
															tipoHtml+= "<option value='"+option[0]+"' "+selected+">"+option[1]+"</option>";
														}	
														
														tipoHtml+= "</select>";
													}													
										  		}
											  	
											    %>
												  <tr valign="top"> 
													<td width="17%" align="right" class="principalLabel"><%= confVo.getConfDsConfiguracao()%><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td align="left">
														<div style="float:left;">
														<%=tipoHtml%>
														</div>
														<% 
														// 91698 - 30/10/2013 - Jaider Alba 
														if(txAjuda!=null && !txAjuda.trim().equals("")) { %>
														<div id="help-<%=confVo.getIdConfCdConfiguracao()%>" class="<%=(isW3c)?"infoIcon64":"infoIcon"%>">
														</div>
														<div id="popoverContent-<%=confVo.getIdConfCdConfiguracao()%>" style="display: none">
															<div>
																<%=txAjuda%>
															</div>
														</div>
														<script>
															$("#help-<%=confVo.getIdConfCdConfiguracao()%>").popover({
																placement : 'left',
																title : '<%= confVo.getConfDsConfiguracao()%>',
																html : true,
																trigger: "click",
																left: "180px",
																//delay: { show: 500, hide: 3000 },
																content : function() {
																	$(".popover").each(function(i){this.style.display = 'none';});
																	return $('#popoverContent-<%=confVo.getIdConfCdConfiguracao()%>').html();
																}
															});
														</script>
														<%}%>
													</td>
													<td width="5%" align="left"></td>
												  </tr>
												  <tr></tr>
											  <%
											}
									  }
									  %>
                            </table>
                          </div>
<!--                          <tr>-->
<!--                          </tr>-->
							</td>
                      </tr>
                      <tr height="20px"> 
                        <td valign="top" align="right"> 
                          <table width="60" border="0" cellspacing="0" cellpadding="0" height="11">
                            <tr align="center"> 
                              <td><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="atualizaRegistros()"></td>
                              <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="limpaTela()"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr height="10px"> 
                        <td valign="top" height="5"><font size="1"></font></td>
                      </tr>
                    </table>
                  </td>
                  </tr>
                </table>
              <iframe id=seguratela name="seguratela" src="" width="0" height="0" frameborder="0" ></iframe></div>
            </td>
              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
            </tr>
            <tr> 
              <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
            </tr>
          </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
