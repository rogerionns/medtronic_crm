<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	
long idEmpresa = empresaVo.getIdEmprCdEmpresa();

/* int nSenha = 6;
try{
	nSenha = Integer.parseInt( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CADASTRO_FUNCIONARIO_SENHA, request));
}catch(Exception e){ } */

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function submitPaginacao(regDe,regAte){
	
	document.administracaoCsCdtbFuncionarioFuncForm.regDe.value=regDe;
	document.administracaoCsCdtbFuncionarioFuncForm.regAte.value=regAte;
	if(regDe > -1 && regAte > -1)
		filtrar();
}

function inicio(){
	initPaginacao();
	filtrar();
}

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsCdtbFuncionarioFuncForm.target = admIframe.name;
	document.administracaoCsCdtbFuncionarioFuncForm.acao.value ='filtrar';
	document.administracaoCsCdtbFuncionarioFuncForm.submit();
	//setTimeout('limpaCampoFiltro()', 10); //Chamado: 80990 - 23/07/2012 - Carlos Nunes

	document.administracaoCsCdtbFuncionarioFuncForm.regDe.value='0';
	document.administracaoCsCdtbFuncionarioFuncForm.regAte.value='0';
	
	initPaginacao();
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbFuncionarioFuncForm.filtro.value = '';
	document.administracaoCsCdtbFuncionarioFuncForm.funcDsLoginname.value = '';
	document.administracaoCsCdtbFuncionarioFuncForm.filtroStatus.value = 'ATIVO';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsCdtbFuncionarioFuncForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbFuncionarioFuncForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_FUNCIONARIO_FUNC%>';
	editIframe.document.administracaoCsCdtbFuncionarioFuncForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbFuncionarioFuncForm.limparSessao.value = "true";
	editIframe.document.administracaoCsCdtbFuncionarioFuncForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo){
	tab.document.administracaoCsCdtbFuncionarioFuncForm.idFuncCdFuncionario.value = codigo;
	tab.document.administracaoCsCdtbFuncionarioFuncForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_FUNCIONARIO_FUNC%>';
	tab.document.administracaoCsCdtbFuncionarioFuncForm.target = editIframe.name;
	tab.document.administracaoCsCdtbFuncionarioFuncForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbFuncionarioFuncForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

//Chamado: 89956 - 01/08/2013 - Carlos Nunes
function submeteSalvar(acao){
	var accffForm = tab.document.administracaoCsCdtbFuncionarioFuncForm;

	if(accffForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_FUNCIONARIO_FUNC%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(accffForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_FUNCIONARIO_FUNC%>'){

		if(accffForm.idIdioCdIdioma.value == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_um_Idioma"/>.");
			accffForm.idIdioCdIdioma.focus();
			return false;
		}

		if(accffForm.idAreaCdArea.value == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_uma_Area"/>.");
			accffForm.idAreaCdArea.focus();
			return false;
		}
	
		if(accffForm.idNiveCdNivelAcesso.value == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_um_Tipo_de_Funcionario"/>.");
			accffForm.idNiveCdNivelAcesso.focus();
			return false;
		}
	
		if (trim(accffForm.funcNmFuncionario.value) == "") {
			alert("<bean:message key="prompt.Por_favor_digite_o_nome_do_funcionario"/>.");
			accffForm.funcNmFuncionario.focus();
			return false;		
		}

	
		/*
		if (tab.document.administracaoCsCdtbFuncionarioFuncForm.funcDsMail.value == "") {
			alert("<bean:message key="prompt.Por_favor_digite_o_E-mail"/>.");
			tab.document.administracaoCsCdtbFuncionarioFuncForm.funcDsMail.focus();
			return false;		
		}
		*/
		
		if (accffForm.funcDsMail.value.search(/\S/) != -1) {
//			regExp = /[A-Za-z0-9_]+@[A-Za-z0-9_-]{2,}\.[A-Za-z]{2,}/
			//Danilo Prevides - 04/09/2009 - 66241 - 29/10/2009 - 67200 - 67331  INI 
			//regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{2,}\.[A-Za-z]{2,}/
			//Chamado: 93047 - 05/02/2014 - Carlos Nunes
			regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9._-]{1,}\.[A-Za-z0-9]{2,}/
			//regExp = /[A-Za-z0-9_]+@[A-Za-z0-9_-]{1,}\.[A-Za-z0-9]{2,}/
			//Danilo Prevides - 04/09/2009 - 66241 - 29/10/2009 - 67200 - 67331 FIM
			if (accffForm.funcDsMail.value.length < 7 || accffForm.funcDsMail.value.search(regExp) == -1){
				alert ("<bean:message key="prompt.Por_favor_preencha_corretamente_o_seu_E_Mail"/>.");
				accffForm.funcDsMail.focus();
			    return false;
			}						
		}
		num1 = accffForm.funcDsMail.value.indexOf("@");
		num2 = accffForm.funcDsMail.value.lastIndexOf("@");
		if (num1 != num2){
		    alert ("<bean:message key="prompt.Por_favor_preencha_corretamente_o_seu_E_Mail"/>.");
		    accffForm.funcDsMail.focus();
			return false;
		}

		//VINICIUS - INCLUS�O DO MAConstantes.ID_NIVE_CD_NIVELACESSO_AREAVENDEDOR PARA N�O VALIDAR O LOGIN QUANDO SALVAR
		if(!tab.document.forms[0].funcInDestinatarioEmail.checked) {

			if(accffForm.idNiveCdNivelAcesso.value != <%=MAConstantes.ID_NIVE_CD_NIVELACESSO_AREARESOLVEDORA%> && accffForm.idNiveCdNivelAcesso.value != <%=MAConstantes.ID_NIVE_CD_NIVELACESSO_AREAVENDEDOR%>){
				if (accffForm.funcDsLoginname.value == "") {
					alert("<bean:message key="prompt.Por_favor_digite_o_Login"/>.");
					accffForm.funcDsLoginname.focus();
					return false;		
				}
			}
			
			/**********************************************************************/
		    /*O bloco abaixo � utilizado para fazer o controle de LICEN�A NOMINAL */
		    /**********************************************************************/
			if(administracaoCsCdtbFuncionarioFuncForm.licencaNominal.value == "S"){
				
				if (trim(accffForm.idModuCdModulo.value) == "") {
					alert("<bean:message key="prompt.obrigatoriedade_modulo"/>");
					accffForm.funcNmFuncionario.focus();
					return false;		
				}
			}
			/**********************************************************************/
		}else{
			accffForm.idModuCdModulo.value = "";
		}
		
/*				
		if (tab.document.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.value == "") {
			alert("<bean:message key="prompt.Por_favor_digite_a_Senha"/>.");
			tab.document.administracaoCsCdtbFuncionarioFuncForm.funcDsPassword.focus();
			return false;		
		}
		
		if (tab.document.administracaoCsCdtbFuncionarioFuncForm.funcDsFax.value == "") {
			alert("<bean:message key="prompt.Por_favor_digite_o_Telefone_Ramal"/>.");
			tab.document.administracaoCsCdtbFuncionarioFuncForm.funcDsFax.focus();
			return false;		
		}		
*/
		<%-- if ((tab.document.getElementById("trLogin").style.display!="none") && (accffForm.idLodoCdLogindomain.selectedIndex==0) && (accffForm.funcDsPassword.value.length < new Number(<%=nSenha%>))) {
			alert("<bean:message key="prompt.A_senha_digitada_nao_contem_o_minimo_de_caracteres_exigido"/>.");
			accffForm.funcDsPassword.focus();
			return false;		
		} --%>

		if(podeGravarAssociacao()==false){
			return false;
		}

        //Chamado: 89956 - 01/08/2013 - Carlos Nunes
		if(acao != undefined && acao == 'manter')
		{
			accffForm.target = editIframe.name;
			accffForm.manterEdicao.value="S";
		}
		else
		{
			accffForm.manterEdicao.value="N";
			accffForm.target = admIframe.name;
		}
		
		accffForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_FUNCIONARIO_FUNC%>';
		disableEnable(accffForm.idFuncCdFuncionario, false);
		disableEnable(accffForm.funcDsLoginname, false);
		tab.document.administracaoCsCdtbFuncionarioFuncForm.submit();
		disableEnable(accffForm.idFuncCdFuncionario, true);
		
		setTimeout("try{filtrar();}catch(e){}",1000);

        //Chamado: 89956 - 01/08/2013 - Carlos Nunes
		if(acao == undefined || acao != 'manter')
		{
			cancel();
		}
		
	}
}

function submeteExcluir(codigo) {
	
	tab.document.administracaoCsCdtbFuncionarioFuncForm.idFuncCdFuncionario.value = codigo;
	tab.document.administracaoCsCdtbFuncionarioFuncForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_FUNCIONARIO_FUNC%>';
	tab.document.administracaoCsCdtbFuncionarioFuncForm.target = editIframe.name;
	tab.document.administracaoCsCdtbFuncionarioFuncForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbFuncionarioFuncForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbFuncionarioFuncForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_FUNCIONARIO_FUNC%>';
		editIframe.document.administracaoCsCdtbFuncionarioFuncForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbFuncionarioFuncForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbFuncionarioFuncForm.idFuncCdFuncionario, false);
		editIframe.document.administracaoCsCdtbFuncionarioFuncForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbFuncionarioFuncForm.idFuncCdFuncionario, true);
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbFuncionarioFunc.do?tela=editCsCdtbFuncionarioFunc&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbFuncionarioFunc.do?tela=editCsCdtbFuncionarioFunc&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsCdtbFuncionarioFuncForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbFuncionarioFuncForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_FUNCIONARIO_FUNC%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormal');
			setaListaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbFuncionarioFuncForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_FUNCIONARIO_FUNC%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionado');
			setaListaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form styleId="administracaoCsCdtbFuncionarioFuncForm"	action="/AdministracaoCsCdtbFuncionarioFunc.do"> 
<html:hidden property="modo" /> 
<html:hidden property="acao" /> 
<html:hidden property="tela" /> 
<html:hidden property="regDe" />
<html:hidden property="regAte" />
<html:hidden property="topicoId" /> 
<!-- Chamado: 89956 - 01/08/2013 - Carlos Nunes -->
<html:hidden property="manterEdicao" /> 

<html:hidden property="licencaNominal" />

<body class="principalBgrPage" text="#000000"> 
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="100%" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
        <tr> 
          <td class="principalQuadroPst" height="100%">&nbsp;</td>
          <td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
          <td height="17px" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="17px"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"><br>
      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td height="254"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkVazio"> 
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadroLinkSelecionado" id="tdDestinatario" name="tdDestinatario" onClick="AtivarPasta(admIframe);"> 
                        <bean:message key="prompt.procurar"/> 
                        <!-- ## -->
                      </td>
                      <td class="principalPstQuadroLinkNormal" id="tdManifestacao" name="tdManifestacao" onClick="AtivarPasta(editIframe);"> 
                        <bean:message key="prompt.funcionario"/> 
                        <!-- ## -->
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td valign="top" class="principalBgrQuadro" height="490"><br>
                  <div name="Manifestacao" id="Manifestacao" style="position: absolute; width: 97%; height: 225px; z-index: 6; visibility: hidden"> 
                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td height="380">
                        	<iframe id=editIframe name="editIframe"
										src="AdministracaoCsCdtbFuncionarioFunc.do?tela=editCsCdtbFuncionarioFunc&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0"></iframe></td>
                      </tr>
                    </table>
                  </div>
                  
                  <div name="Destinatario" id="Destinatario" style="position: absolute; width: 97%; height: 199px; z-index: 2; visibility: visible"> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td colspan="2">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        		<tr>
                        			<td class="principalLabel" width="50%">
                        				<bean:message key="prompt.nome"/>
                        			</td>
                        			<td class="principalLabel" width="20%">
                        				<bean:message key="prompt.login"/>
                        			</td>
                        			<td class="principalLabel" width="20%">&nbsp;<bean:message key="prompt.status"/></td> <!-- Chamado: 80990 - 23/07/2012 - Carlos Nunes -->
                        			<td class="principalLabel" width="5%">&nbsp;</td>
                        		</tr>
                        	</table>
                        </td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" colspan="2">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        		<tr>
                        			<td class="principalLabel" width="50%">
                        				<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
                        			</td>
                        			<td class="principalLabel" width="25%">
                        				<html:text property="funcDsLoginname" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
                        			</td>
                        			<td class="principalLabel" width="20%"> <!-- Chamado: 80990 - 23/07/2012 - Carlos Nunes -->
                        				<html:select property="filtroStatus" styleClass="principalObjForm">
			                          		<html:option value="ATIVO">ATIVO</html:option>
			                          		<html:option value="INATIVO">INATIVO</html:option>
			                          		<html:option value="AMBOS">AMBOS</html:option>
			                          	</html:select>
                        			</td>
                        			<td class="principalLabel" width="5%">
			                        	<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
			                        </td>
                        		</tr>
                        	</table>
                        </td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" width="65%">&nbsp;</td>
                        <td class="principalLabel" width="35%">&nbsp;</td>
                      </tr>
                   </table>
                   <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLstCab" width="11%">&nbsp;&nbsp;<bean:message key="prompt.codigo"/> 
                        </td>
                        <td class="principalLstCab" width="20%"><bean:message key="prompt.nome"/></td>
                        <td class="principalLstCab" width="26%"><bean:message key="prompt.login"/></td>
                        <td class="principalLstCab" width="26%"><bean:message key="prompt.area"/></td>
                        <td class="principalLstCab" align="left" width="17%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.inativo"/> 
                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="5" height="295"> <iframe id=admIframe name="admIframe"
										src="AdministracaoCsCdtbFuncionarioFunc.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
                      </tr>
                      
                      <tr>
                      	<td colspan="5">
                      		<table>
                      			<tr>
                      				<td width="55%">
                      					&nbsp;
                      				</td>
                      				<td width="45%">
                      					<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>
                      				</td>
                      			</tr>
                      		</table>
                      	</td>
					  </tr>
                      
                    </table>
							</div>
				  
				  
                </td>
                <td width="4" height="490"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="490px"></td>
              </tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="x"></td>
        </tr>
      </table>
      <!-- Chamado: 89956 - 01/08/2013 - Carlos Nunes -->
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td width="20" align="center"> 
             <div id="btFuncionarioEspec" style="visibility: hidden;">
             	&nbsp;
             </div> 
          </td>
          <td width="20" align="right"> <img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()"> 
          </td>
          <td width="20"> <img src="webFiles/images/botoes/gravarDados.gif"	name="imgGravarManter" id="imgGravarManter" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravarManter'/>" onclick="clearError();submeteSalvar('manter');"> 
          </td>
          <td width="20"> <img src="webFiles/images/botoes/gravar.gif"	name="imgGravar" id="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();"> 
          </td>
          <td width="20"> <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();"> 
          </td>
        </tr>
      </table>
      <table align="center" >
        <tr> 
          <td> <label id="error"> </label> </td>
        </tr>
      </table>
    </td>
    <td width="4" height="565px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="565px"></td>
  </tr>
  <tr> 
    <td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
    <td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
  </tr>
</table>
</html:form> 
<script language="JavaScript">
	var tab = admIframe ;
</script>
<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIO_INCLUSAO_CHAVE%>', document.administracaoCsCdtbFuncionarioFuncForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIO_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_FUNCIONARIO_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbFuncionarioFuncForm.imgGravar.disabled=true;
			document.administracaoCsCdtbFuncionarioFuncForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbFuncionarioFuncForm.imgGravar.title='';
			
			//Chamado: 89956 - 01/08/2013 - Carlos Nunes
			document.administracaoCsCdtbFuncionarioFuncForm.imgGravarManter.disabled=true;
			document.administracaoCsCdtbFuncionarioFuncForm.imgGravarManter.className = 'geralImgDisable';
			document.administracaoCsCdtbFuncionarioFuncForm.imgGravarManter.title='';
	}
	
	habilitaListaEmpresas();

</script>


</body>
</html>
