<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsAstbGrupoAssuntoAsgrForm" action="/AdministracaoCsAstbGrupoAssuntoAsgr.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idAsmeCdAssuntoMail"/>
	<html:hidden property="idGrfuCdGrupoFunc"/>

<script>var possuiRegistros=false;</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="cagaaVector" name="csAstbGrupoAssuntoAsgrVector">
  <script>possuiRegistros=true;</script>
  <tr> 
    <td width="5%" class="principalLstPar"> <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key='prompt.excluir'/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="cagaaVector" property="csCdtbAssuntoMailAsmeVo.idAsmeCdAssuntoMail" />','<bean:write name="cagaaVector" property="csCdtbGrupoFuncGrfuVo.idGrfuCdGrupoFunc" />')"> 
    </td>
    <td class="principalLstParMao" width="3%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="csCdtbAssuntoMailAsmeVo.idAsmeCdAssuntoMail" />','<bean:write name="cagaaVector" property="csCdtbGrupoFuncGrfuVo.idGrfuCdGrupoFunc" />')"> 
      &nbsp; </td>
    <td class="principalLstParMao" align="left" width="51%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="csCdtbAssuntoMailAsmeVo.idAsmeCdAssuntoMail" />','<bean:write name="cagaaVector" property="csCdtbGrupoFuncGrfuVo.idGrfuCdGrupoFunc" />')"> 
      <bean:write name="cagaaVector" property="csCdtbAssuntoMailAsmeVo.asmeDsAssuntoMail" />&nbsp; 
    </td>
	<td class="principalLstParMao" align="left" width="41%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="csCdtbAssuntoMailAsmeVo.idAsmeCdAssuntoMail" />','<bean:write name="cagaaVector" property="csCdtbGrupoFuncGrfuVo.idGrfuCdGrupoFunc" />')"> 
      <bean:write name="cagaaVector" property="csCdtbGrupoFuncGrfuVo.grfuDsGrupoFunc" />&nbsp; 
    </td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_GRUPOASSUNTO_EXCLUSAO_CHAVE%>', editCsAstbGrupoAssuntoAsgrForm.lixeira);	
	
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>