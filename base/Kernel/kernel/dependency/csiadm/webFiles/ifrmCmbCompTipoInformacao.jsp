<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<!-- meta http-equiv="Content-Type" content="text/html; charset=utf-8" /-->
</head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">
function mostraCampoBusca(){
	document.administracaoCsAstbComposicaoCompForm.acao.value = "<%=Constantes.ACAO_CONSULTAR%>";
	document.administracaoCsAstbComposicaoCompForm.submit();
}

function cancelar() {
	document.administracaoCsAstbComposicaoCompForm['tpinDsTipoinformacao'].value = '';
	document.administracaoCsAstbComposicaoCompForm.tela.value = "<%= MAConstantes.TELA_CMB_TPIN %>";
	document.administracaoCsAstbComposicaoCompForm.acao.value = "<%=Constantes.ACAO_VISUALIZAR%>";
	document.administracaoCsAstbComposicaoCompForm.submit();
}

function buscar(){

	if (document.administracaoCsAstbComposicaoCompForm['tpinDsTipoinformacao'].value.length < 3){
		alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
		event.returnValue=null
		return false;
	}
	
	document.administracaoCsAstbComposicaoCompForm.tela.value = "<%= MAConstantes.TELA_CMB_TPIN %>";
	document.administracaoCsAstbComposicaoCompForm.acao.value = "<%=Constantes.ACAO_VISUALIZAR%>";
	
	document.administracaoCsAstbComposicaoCompForm.submit();

}

function pressEnter(event) {
    if (event.keyCode == 13) {
		buscar();
    } else if (event.keyCode == 27) {
		cancelar();
    }
}

</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form styleId="administracaoCsAstbComposicaoCompForm" action="/AdministracaoCsAstbComposicaoComp.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
		
  
  <logic:notEqual name="administracaoCsAstbComposicaoCompForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
				<html:select property="idTpinCdTipoinformacao" styleClass="principalObjForm" > 
					<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
					<html:options collection="csCdtbTpinformacaoTpinVector" property="idTpinCdTipoinformacao" labelProperty="tpinDsTipoinformacao"/> 
				</html:select>                             
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right" id="divBotao"><img id="botaoPesq" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBusca();"></div>
		  	</td>
	  	</tr>
	  </table>
	  
		  <logic:present name="combo">
			  <logic:iterate name="csCdtbTpinformacaoTpinVector" id="tpin">
				  <input type="hidden" name='txtTpin<bean:write name="tpin" property="idTpinCdTipoinformacao"/>' value='<bean:write name="tpin" property="idTpinCdTipoinformacao"/>'>
			  </logic:iterate>	  
		  </logic:present>
	  
	  </logic:notEqual>
	  
   	  <logic:equal name="administracaoCsAstbComposicaoCompForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
	  	<tr>
	  		<td width="95%">
	  			<html:text property="tpinDsTipoinformacao" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarTpin"/>' onclick="buscar();"></div>
		  	</td>
		</tr> 	
	  </table>	  
   	  </logic:equal>
</table>

</html:form>
</body>

</html>