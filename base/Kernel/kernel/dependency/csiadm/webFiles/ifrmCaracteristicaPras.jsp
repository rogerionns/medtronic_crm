<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
function iniciaTela(){
	atualizaCmbCaracteristica();
	atualizaLstCaracteristica();
}

function atualizaCmbCaracteristica(){
	var url="";

	url = "AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%=MAConstantes.TELA_CMB_CARACTERISTICA_PRAS%>";
	url = url +	"&acao=incluir";
	url = url +	"&idLinhCdLinha=" + parent.document.administracaoCsCdtbProdutoAssuntoPrasForm.idLinhCdLinha.value;

	cmbCaractIframe.document.location = url;
	
}

var nCountLstCaract = 0;

function atualizaLstCaracteristica(){

	try{
		var url="";

		url = "AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%=MAConstantes.TELA_LST_CARACTERISTICA_PRAS%>";
		url = url +	"&acao=<%=Constantes.ACAO_CONSULTAR%>";
		url = url +	"&idAsn1CdAssuntoNivel1=" + parent.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value;
		
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		url = url +	"&idAsn2CdAssuntoNivel2=" + parent.ifrmCmbAssuntoNivel2Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value;
	<%}else{%>
		url = url + "&idAsn2CdAssuntoNivel2=" + parent.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value;
	<%}%>
	
		lstCaractIframe.location.href = url;
	}catch(e){
		if(nCountLstCaract < 10){
			setTimeout('atualizaLstCaracteristica', 500);
			nCountLstCaract++;
		}
	}
}

function submitForm(){
	var idCapr = cmbCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd'].value;
	var idRtcp = cmbRespTabIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd'].value;

	if (idCapr == 0){
		alert ('<bean:message key="prompt.Por_favor_selecione_uma_caracteristica"/>');
		return false;
	}	

	if (idRtcp == 0){
		alert ('<bean:message key="prompt.Por_favor_selecione_uma_respTabulada"/>');
		return false;
	}	

	if (existeRepeticao(idCapr,idRtcp)){
		alert ('<bean:message key="prompt.Esta_caracteristica_ja_existe"/>');
		return false;
	}

	lstCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd'].value = idCapr;
	lstCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd'].value = idRtcp;
	
	lstCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value="<%=MAConstantes.TELA_LST_CARACTERISTICA_PRAS%>";
	lstCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value="<%=Constantes.ACAO_INCLUIR%>";
	
	lstCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();
	limpaCampos();
	
}

function existeRepeticao(idCapr,idRtcp){
	if (lstCaractIframe.result > 0){
		if (lstCaractIframe.result == 1){
			if (idCapr == lstCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.idCaprCdCaracteristicaProd.value && 
				idRtcp == lstCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.idRtcpRespTabCaractProd.value){

				return true;
			}	
		}else{
			for (i=0;i<lstCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.idCaprCdCaracteristicaProd.length;i++){
				if (idCapr == lstCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.idCaprCdCaracteristicaProd[i].value && 
					idRtcp == lstCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.idRtcpRespTabCaractProd[i].value){
	
					return true;
				}	
			}
		}
	}
	return false;
}

function limpaCampos(){
	cmbCaractIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd'].value = 0;
	
	//Limpando o combo de resposta tabulada
	while(cmbRespTabIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd'].length > 1)
		cmbRespTabIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd'].remove(1);
	cmbRespTabIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd'].value = 0;
}

</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form styleId="administracaoCsCdtbProdutoAssuntoPrasForm" action="/AdministracaoCsCdtbProdutoAssuntoPras.do">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="principalLabel">&nbsp;</td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="1%">&nbsp;</td>
		<td width="13%" class="principalLabel"><bean:message key="prompt.caracteristicaProduto"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="32%" class="principalLabel" align="left">
			<iframe id=cmbCaractIframe name="cmbCaractIframe" src="AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%=MAConstantes.TELA_CMB_CARACTERISTICA_PRAS%>&acao=incluir" width="100%" height="24" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>		
		</td>
		<td width="18%" class="principalLabel">&nbsp;<bean:message key="prompt.resptabcaractprod"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<td width="31%" class="principalLabel" align="left">
			<iframe id=cmbRespTabIframe name="cmbRespTabIframe" src="AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%=MAConstantes.TELA_CMB_RESPTABCARACT_PRAS%>&acao=incluir" width="100%" scrolling="no" height="24" frameborder="0" marginwidth="0" marginheight="0"></iframe>		
		</td>		
		<td width="5%">&nbsp;
			<img src="webFiles/images/icones/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="submitForm();" title="<bean:message key="prompt.Adicionar" />" />
		</td>		
	</tr>
	<tr>
		<td colspan="6">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="left">
				<tr>
					<td width="5%" class="principalLstCab">&nbsp;</td>
					<td width="50%" class="principalLstCab"><bean:message key="prompt.caracteristicaProduto"/></td>
					<td width="45%" class="principalLstCab"><bean:message key="prompt.resptabcaractprod"/></td>
				</tr>
			</table>			
		</td>
	</tr>
	<tr>	
		<td colspan="6">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="left">
				<tr>
					<td>
						<iframe id=lstCaractIframe name="lstCaractIframe" src="" width="100%" height="85" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
						<!-- AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%=MAConstantes.TELA_LST_CARACTERISTICA_PRAS%>&acao=<%=Constantes.ACAO_CONSULTAR%> -->
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


</html:form>
</body>
</html>