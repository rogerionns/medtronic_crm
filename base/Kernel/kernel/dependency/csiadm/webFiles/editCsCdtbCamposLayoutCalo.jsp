<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>

<SCRIPT LANGUAGE="JavaScript">
	function VerificaCampos(){
		if (document.administracaoCsCdtbCamposLayoutCaloForm.caloNrTamanho.value == "0" ){
			document.administracaoCsCdtbCamposLayoutCaloForm.caloNrTamanho.value = "" ;
		}
	}	
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos()">

<html:form styleId="administracaoCsCdtbCamposLayoutCaloForm" action="/AdministracaoCsCdtbCamposLayoutCalo.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="19%" align="right" class="principalLabel">C�digo <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="10%"> <html:text property="idCaloCdSequencial" styleClass="text" disabled="true" /> 
    </td>
    <td width="49%">&nbsp;</td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel">Descri��o <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="caloDsDescricao" styleClass="text" maxlength="60" /> 
    </td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel">Campo <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="caloDsCampo" styleClass="text" maxlength="60" /> 
    </td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel">Tamanho <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="10%"> <html:text property="caloNrTamanho" styleClass="text" maxlength="60" /> 
    </td>
    <td width="49%">&nbsp;</td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%" align="right" class="principalLabel">Tipo <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2" class="principalLabel"><html:radio value="C" property="caloInTipo"/> Caractere <html:radio value="N" property="caloInTipo"/>N&uacute;mero <html:radio value="D" property="caloInTipo"/>Data</td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="19%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbCamposLayoutCaloForm.caloDsDescricao.disabled= true;	
			document.administracaoCsCdtbCamposLayoutCaloForm.caloDsCampo.disabled= true;
			document.administracaoCsCdtbCamposLayoutCaloForm.caloNrTamanho.disabled= true;
			document.administracaoCsCdtbCamposLayoutCaloForm.caloInTipo[0].disabled= true;						
			document.administracaoCsCdtbCamposLayoutCaloForm.caloInTipo[1].disabled= true;						
			document.administracaoCsCdtbCamposLayoutCaloForm.caloInTipo[2].disabled= true;									

			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			document.administracaoCsCdtbCamposLayoutCaloForm.idCaloCdSequencial.disabled= false;
			document.administracaoCsCdtbCamposLayoutCaloForm.idCaloCdSequencial.value= '';
			document.administracaoCsCdtbCamposLayoutCaloForm.idCaloCdSequencial.disabled= true;
		</script>
</logic:equal>
</html>