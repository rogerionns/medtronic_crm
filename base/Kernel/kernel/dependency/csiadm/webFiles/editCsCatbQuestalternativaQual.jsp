<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>

	function VerificaCampos(){
		if (document.administracaoCsCatbQuestalternativaQualForm.qualNrProxima.value == "0" ){
			document.administracaoCsCatbQuestalternativaQualForm.qualNrProxima.value = "" ;
		}
		if (document.administracaoCsCatbQuestalternativaQualForm.qualNrPeso.value == "0" ){
			document.administracaoCsCatbQuestalternativaQualForm.qualNrPeso.value = "" ;
		}
	}
	function textCounter_xxx(field, countfield, maxlimit) {
		if (field.value.length > maxlimit) 
			field.value = field.value.substring(0, maxlimit);
		else 
			countfield.value = maxlimit - field.value.length;
	}

	function MontaLista(){
		lstQuestAlter.location.href= "AdministracaoCsCatbQuestalternativaQual.do?tela=<%=MAConstantes.TELA_ADMINISTRACAO_LST_CS_CATB_QUESTALTERNATIVA_QUAL%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&idPesqCdPesquisa=" + document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.value;
	}

	function desabilitaCamposQuestaoalternativa(){
		try{
			document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.disabled= true;
			document.administracaoCsCatbQuestalternativaQualForm.idQuesCdQuestao.disabled= true;			
			document.administracaoCsCatbQuestalternativaQualForm.idAlteCdAlternativa.disabled= true;	
			document.administracaoCsCatbQuestalternativaQualForm.qualNrProxima.disabled= true;	
			document.administracaoCsCatbQuestalternativaQualForm.qualNrPeso.disabled= true;	
			document.administracaoCsCatbQuestalternativaQualForm.qualInObjecao.disabled= true;	
			document.administracaoCsCatbQuestalternativaQualForm.qualInAceitafilho.disabled= true;	
			document.administracaoCsCatbQuestalternativaQualForm.idDocuCdDocumento.disabled= true;	
			document.administracaoCsCatbQuestalternativaQualForm.qualDsOrientacao.disabled= true;	
			document.administracaoCsCatbQuestalternativaQualForm.qualInEncerramento.disabled= true;
			cmbQuestao.document.administracaoCsCatbQuestalternativaQualForm.idQuesCdQuestao.disabled= true;
		}
		catch(e){
			setTimeout("desabilitaCamposQuestaoalternativa();",100);
		}	
	}

</script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos();MontaLista()">

<html:form styleId="administracaoCsCatbQuestalternativaQualForm" action="/AdministracaoCsCatbQuestalternativaQual.do">
	<input readonly type="hidden" name="remLen" size="3" maxlength="3" value="2000" > 
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="idQuesCdQuestao"/>
	<html:hidden property="idQuesCdQuestaoFixo"/>	
	<html:hidden property="idAlteCdAlternativaFixo"/>
	<br>
	 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.pesquisa"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idPesqCdPesquisa" styleClass="principalObjForm" onchange="cmbQuestao.MontaCmb();MontaLista()"> 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbPesquisaPesqVector" property="idCodigo" labelProperty="pesqDsPesquisa"/> 
      </html:select> </td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.questao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2" > 
	    <iframe id="cmbQuestao" name="cmbQuestao" src="AdministracaoCsCatbQuestalternativaQual.do?tela=cmbQuestao&acao=<%=Constantes.ACAO_CONSULTAR%>&idQuesCdQuestao=<bean:write name="administracaoCsCatbQuestalternativaQualForm" property="idQuesCdQuestao"/>&idPesqCdPesquisa=<bean:write name="administracaoCsCatbQuestalternativaQualForm" property="idPesqCdPesquisa"/>" width="100%" height="20" scrolling="no" frameborder="0" ></iframe>
    </td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.alternativa"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idAlteCdAlternativa" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbAlternativaAlteVector" property="idCodigo" labelProperty="alteDsAlternativa"/> 
      </html:select> </td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.proximaQuestao"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><html:text property="qualNrProxima" styleClass="text" maxlength="5" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/></td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.peso"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><html:text property="qualNrPeso" styleClass="text" maxlength="5" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/>
    </td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.orientacao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><html:textarea property="qualDsOrientacao" styleClass="text" style="width:512px" rows="4" cols="50" onkeydown="desabilitaEnter(this,event);textCounter(this,2000)" onkeyup="textCounter(this,2000)"/></td>
    <td width="14%">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        	<tr>
	          <td align="right" width="20%"> <html:checkbox value="true" property="qualInEncerramento"/></td>
	          <td class="principalLabel" width="80%">&nbsp;<bean:message key="prompt.encerramento"/></td>
        	</tr>
        	<tr>
	          <td align="right" width="20%"> <html:checkbox value="true" property="qualInObjecao"/></td>
	          <td class="principalLabel" width="80%">&nbsp;<bean:message key="prompt.objecao"/></td>          
        	</tr>
        	<tr>
	          <td align="right" width="20%"> <html:checkbox value="true" property="qualInAceitafilho"/></td>
	          <td class="principalLabel" width="80%">&nbsp;<bean:message key="prompt.aceitaFilho"/></td>
        	</tr>
        </table> 	
    </td>
  </tr>
  <tr style="display:none"> <!-- REMOVER DISPLAY --> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.documento"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idDocuCdDocumento" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/> 
      </html:select> </td>
    <td width="14%">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" >
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="5%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="60%"><bean:message key="prompt.questao"/></td>
          <td class="principalLstCab" width="35%"><bean:message key="prompt.alternativa"/></td>
        </tr>
        <tr valign="top"> 
          <td colspan="3" height="120"> <iframe id="lstQuestAlter" name="lstQuestAlter" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
/*
				document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.disabled= true;	
								
				document.administracaoCsCatbQuestalternativaQualForm.idAlteCdAlternativa.disabled= true;	
				document.administracaoCsCatbQuestalternativaQualForm.qualNrProxima.disabled= true;	
				document.administracaoCsCatbQuestalternativaQualForm.qualNrPeso.disabled= true;	
				document.administracaoCsCatbQuestalternativaQualForm.qualInObjecao.disabled= true;	
				document.administracaoCsCatbQuestalternativaQualForm.qualInAceitafilho.disabled= true;	
				document.administracaoCsCatbQuestalternativaQualForm.idDocuCdDocumento.disabled= true;	
				document.administracaoCsCatbQuestalternativaQualForm.qualDsOrientacao.disabled= true;	
				document.administracaoCsCatbQuestalternativaQualForm.qualInEncerramento.disabled= true;

*/
				confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');	

				document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.disabled= false;	
				//document.administracaoCsCatbQuestalternativaQualForm.idQuesCdQuestao.disabled= false;	
				document.administracaoCsCatbQuestalternativaQualForm.idAlteCdAlternativa.disabled= false;	
	
				parent.setConfirm(confirmacao);
			
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_QUESTAOALTERNATIVA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCatbQuestalternativaQualForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_QUESTAOALTERNATIVA_INCLUSAO_CHAVE%>')){
				desabilitaCamposQuestaoalternativa();
			}
			document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.disabled= false;
			document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.value= '';
			//document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.disabled= true;
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_QUESTAOALTERNATIVA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_QUESTAOALTERNATIVA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCatbQuestalternativaQualForm.imgGravar);	
				desabilitaCamposQuestaoalternativa();	
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.focus();}
	catch(e){}
</script>