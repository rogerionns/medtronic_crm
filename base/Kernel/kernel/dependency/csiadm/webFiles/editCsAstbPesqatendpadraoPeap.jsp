<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

function MontaLista(){
	lstArqCarga.location.href= "AdministracaoCsAstbPesqatendpadraoPeap.do?tela=administracaoLstCsAstbPesqatendpadraoPeap&acao=<%=Constantes.ACAO_VISUALIZAR%>&csAstbPesqatendpadraoPeapVo.idPesqCdPesquisa=" + document.administracaoCsAstbPesqatendpadraoPeapForm['csAstbPesqatendpadraoPeapVo.idPesqCdPesquisa'].value;
}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista()">
<html:form styleId="administracaoCsAstbPesqatendpadraoPeapForm" action="/AdministracaoCsAstbPesqatendpadraoPeap.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.pesquisa.atend.padrao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2" width="70%"> <html:select property="csAstbPesqatendpadraoPeapVo.idPesqCdPesquisa" styleClass="principalObjForm" onchange="MontaLista()"> 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/></html:option> 
      <html:options collection="csCdtbPesquisaPesqVector" property="idPesqCdPesquisa" labelProperty="pesqDsPesquisa"/> 
      </html:select> </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.atendimentoPadrao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="csAstbPesqatendpadraoPeapVo.idAtpdCdAtendpadrao" styleClass="principalObjForm"  > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
      <html:options collection="csCdtbAtendpadraoAtpaVector" property="idAtpdCdAtendpadrao" labelProperty="atpdDsAtendpadrao"/> 
      </html:select> </td>
    <td >&nbsp;</td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="principalLabel"> </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="220"> 
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="6%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="36%"> <bean:message key="prompt.atendimentoPadrao"/></td>
          <td class="principalLstCab" width="33%">&nbsp;</td>
          <td class="principalLstCab" width="11%">&nbsp;</td>
          <td class="principalLstCab" width="14%">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
			document.administracaoCsAstbPesqatendpadraoPeapForm['csAstbPesqatendpadraoPeapVo.idPesqCdPesquisa'].disabled= false;
			document.administracaoCsAstbPesqatendpadraoPeapForm['csAstbPesqatendpadraoPeapVo.idAtpdCdAtendpadrao'].disabled= false;
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISAATENDPADRAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsAstbPesqatendpadraoPeapForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			document.administracaoCsAstbPesqatendpadraoPeapForm['csAstbPesqatendpadraoPeapVo.idPesqCdPesquisa'].disabled= false;
			document.administracaoCsAstbPesqatendpadraoPeapForm['csAstbPesqatendpadraoPeapVo.idAtpdCdAtendpadrao'].disabled= false;
	
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISAATENDPADRAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISAATENDPADRAO_ALTERACAO_CHAVE%>', parent.administracaoCsCdtbAreaAreaForm.imgGravar);	
	
				document.administracaoCsAstbPesqatendpadraoPeapForm['csAstbPesqatendpadraoPeapVo.idPesqCdPesquisa'].disabled= false;
				document.administracaoCsAstbPesqatendpadraoPeapForm['csAstbPesqatendpadraoPeapVo.idAtpdCdAtendpadrao'].disabled= false;
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsAstbPesqatendpadraoPeapForm['csAstbPesqatendpadraoPeapVo.idPesqCdPesquisa'].focus();}
	catch(e){}
</script>