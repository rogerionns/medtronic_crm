<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsCdtbFeriadoFeriForm" action="/AdministracaoCsCdtbFeriadoFeri.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="csCdtbFeriadoFeriVo.idFeriCdFeriado"/>

	<script>var possuiRegistros=false;</script>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<logic:iterate id="ccttrtVector" name="CsCdtbFeriadoFeriVector" indexId="sequencia">
		<script>possuiRegistros=true;</script>
		<tr> 
  			<td width="3%" class="principalLstPar">
				<img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="idFeriCdFeriado" />')">
    		</td>
    		<td width="3%" class="principalLstParMao"> 
    			&nbsp;<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMAFERIADO_IDFE.xml','<bean:write name="ccttrtVector" property="idFeriCdFeriado" />')">
    		</td>
    		<td class="principalLstParMao" width="7%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFeriCdFeriado" />')">
      			&nbsp;<bean:write name="ccttrtVector" property="idFeriCdFeriado" />
    		</td>
    		<td class="principalLstParMao" align="left" width="40%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFeriCdFeriado" />')">
      			&nbsp;&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="feriDsFeriado" />',37);</script>
			</td>
			
			<logic:equal name="ccttrtVector" property="feriInFixo" value="S">
				<td class="principalLstParMao" align="left" width="17%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFeriCdFeriado" />')">
      				&nbsp;&nbsp;&nbsp;<bean:write name="ccttrtVector" property="feriDsMesdia" />
				</td>
			</logic:equal>
			<logic:notEqual name="ccttrtVector" property="feriInFixo" value="S">
				<td class="principalLstParMao" align="left" width="17%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFeriCdFeriado" />')">
      				&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="ccttrtVector" property="feriDhFeriado" />
				</td>
			</logic:notEqual>

			<td class="principalLstParMao" align="left" width="16%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFeriCdFeriado" />')">
      			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="ccttrtVector" property="feriInFixo" />
			</td>
			<td class="principalLstParMao" align="left" width="17%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idFeriCdFeriado" />')">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="ccttrtVector" property="feriDhInativo" />
			</td>
  		</tr>
  		
  		</logic:iterate>
		<tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
	</table>
	
	<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
	</div>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_UTIL_FERIADO_EXCLUSAO_CHAVE%>', administracaoCsCdtbFeriadoFeriForm.lixeira);
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>