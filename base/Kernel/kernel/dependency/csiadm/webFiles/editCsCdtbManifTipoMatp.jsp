<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbManiftipoMatpForm.idMatpCdManifTipo.value);
	}
</script>

<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbManiftipoMatpForm" action="/AdministracaoCsCdtbManifTipoMatp.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<input type="hidden" name="limparSessao" value="false"></input>

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idMatpCdManifTipo" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigoCorporativo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="matpCdCorporativo" styleClass="text" maxlength="15" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="matpDsManifTipo" styleClass="text" maxlength="40" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 

			<!--<div id="LayerInativo" style="left:0px; top:0px; width:0px; height:0px; z-index:1; visibility: visible">-->
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr> 
	                <td align="right" width="83%"> 
	                  <html:checkbox value="true" property="matpDhInativo"/><!-- @@ --></td>
	            
	                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
	                </td>
	              </tr>
	            </table>
            <!--</div>-->
            
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script language="JavaScript">
			document.getElementById('administracaoCsCdtbManiftipoMatpForm').matpDsManifTipo.disabled= true;	
			document.getElementById('administracaoCsCdtbManiftipoMatpForm').matpDhInativo.disabled= true;
			document.getElementById('administracaoCsCdtbManiftipoMatpForm').matpCdCorporativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script language="JavaScript">
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFTIPO_INCLUSAO_CHAVE%>', parent.administracaoCsCdtbManiftipoMatpForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFTIPO_INCLUSAO_CHAVE%>')){
				document.administracaoCsCdtbManiftipoMatpForm.matpDsManifTipo.disabled= true;
			}else{
				administracaoCsCdtbManiftipoMatpForm.idMatpCdManifTipo.disabled= false;
				administracaoCsCdtbManiftipoMatpForm.idMatpCdManifTipo.value= '';
				administracaoCsCdtbManiftipoMatpForm.idMatpCdManifTipo.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script language="JavaScript">
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFTIPO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFTIPO_ALTERACAO_CHAVE%>', parent.administracaoCsCdtbManiftipoMatpForm.imgGravar);	
				document.administracaoCsCdtbManiftipoMatpForm.matpDsManifTipo.disabled= true;
			}
		</script>
</logic:equal>

</html>

<script language="JavaScript">
	try{document.administracaoCsCdtbManiftipoMatpForm.matpDsManifTipo.focus();}
	catch(e){}
</script>