<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
function adicionarItem() {
	if (validaCampos()) {
		parent.lstLocal.incluirItem('0', document.administracaoCsCdtbMateriaMateForm.localizacaoMateria.value);
		document.administracaoCsCdtbMateriaMateForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
		document.administracaoCsCdtbMateriaMateForm.tela.value = '<%=MAConstantes.TELA_LOCALIZACAO_MATERIA%>';
		document.administracaoCsCdtbMateriaMateForm.submit();
	}
}

function validaCampos() {
	if (document.administracaoCsCdtbMateriaMateForm.localizacaoMateria.value == "") {
		alert('<bean:message key="prompt.Escolha_uma_materia" />');
		document.administracaoCsCdtbMateriaMateForm.localizacaoMateria.focus();
		return false;
	}
	return true;
}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsCdtbMateriaMateForm" action="/AdministracaoCsCdtbMateriaMate.do" enctype="multipart/form-data">
	<html:hidden property="acao" />
	<html:hidden property="tela" />

    <table cellspacing="0" cellpadding="0" valign="center" height="100%">
      <tr>
        <td width="95%">
          <html:file property="localizacaoMateria" styleClass="text" />
        </td>
        <td width="5%">
          <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" class="geralCursoHand" onclick="adicionarItem()" title="<bean:message key="prompt.confirmar"/>">
        </td>
      </tr>
    </table>

</html:form>
</body>
</html>