<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsCdtbPcQuestaoPcquForm" action="/AdministracaoCsCdtbPcQuestaoPcqu.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idPcquCdPcQuestao"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csCdtbPcQuestaoPcquVector" indexId="numero">
  <input type="hidden" name="pcquDsQuestao<bean:write name="numero"/>" value="<bean:write name="ccttrtVector" property="pcquDsQuestao" />">
  <tr> 
    <td width="3%" class="principalLstPar">
      <img src="webFiles/images/botoes/lixeira18x18.gif" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="idPcquCdPcQuestao" />')">
    </td>
    <td class="principalLstParMao" width="12%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPcquCdPcQuestao" />')">
      <bean:write name="ccttrtVector" property="idPcquCdPcQuestao" />
    </td>
    <td class="principalLstParMao" align="left" width="58%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPcquCdPcQuestao" />')">
      <script>
      	acronym(editCsCdtbPcQuestaoPcquForm.pcquDsQuestao<bean:write name="numero"/>.value, 62);
	   </script>
	</td>
	<td class="principalLstParMao" align="center" width="25%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPcquCdPcQuestao" />')">
	&nbsp;
      <bean:write name="ccttrtVector" property="pcquDhInativo" />
	</td>
  </tr>
  </logic:iterate>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>