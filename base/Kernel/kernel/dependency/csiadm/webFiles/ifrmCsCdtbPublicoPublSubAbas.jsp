<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsNatbFuncionariopublFupuVo"%>
<%@ page import="java.util.Vector"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");
%>
	

<%@page import="br.com.plusoft.csi.adm.util.SystemDate"%><html>
<head>
	<title>ifrmCsCdtbPublicoPublSubAbas</title>
	<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	<script language="JavaScript" src="<bean:message key='prompt.funcoes'/>/validadata.js"></script>
	<script language="JavaScript" src="<bean:message key='prompt.funcoes'/>/funcoes.js"></script>
	<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
	<script language="JavaScript" src="<bean:message key='prompt.funcoes'/>/date-picker.js"></script>

	<script language="JavaScript">

		function setFunction(cRetorno, campo){
			if(campo=="document.administracaoCsCdtbPublicoPublForm.publTxObservacao"){
				document.getElementById("divConteudoObservacao").innerHTML = cRetorno;
				document.administracaoCsCdtbPublicoPublForm.publTxObservacao.value=cRetorno;
			}
		}
		
	    function MM_openBrWindow(theURL,winName,features) { //v2.0
		  window.open(theURL,winName,features);
		}
	
		function MM_findObj(n, d) { //v4.0
		  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		  if(!x && document.getElementById) x=document.getElementById(n); return x;
		}
		
		function MM_showHideLayers() { //v3.0
		  var i,p,v,obj,args=MM_showHideLayers.arguments;
		  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
		    //Chamado: 81580
		    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
		    obj.display=v; }
		}
	
		
		nLinha = new Number(0);
		estilo = new Number(0);
		
		function adicionarResu(){
				if (document.administracaoCsCdtbPublicoPublForm['idResuCdResultado'].value == ""){
					alert("<bean:message key='prompt.porFavorEscolhaUmResultado' />");
					document.administracaoCsCdtbPublicoPublForm['idResuCdResultado'].focus();
					return false;
				}
				addResu(document.administracaoCsCdtbPublicoPublForm.idResuCdResultado[document.administracaoCsCdtbPublicoPublForm.idResuCdResultado.selectedIndex].text, document.administracaoCsCdtbPublicoPublForm.idResuCdResultado.value); 
		}
		
		function addResu(cResultado, nResultado) {
			nLinha = nLinha + 1;
			estilo++;
		
			objEMail = document.administracaoCsCdtbPublicoPublForm.idResu;
		
			for (nNode=0;nNode<objEMail.length;nNode++) {
			  if (objEMail[nNode].value == nResultado) {
				  nResultado=0;
				  }
			}
			if (nResultado > 0) { 
				strTxt = "";
				strTxt += "	<table id=\"" + nLinha + "\" width=99% border=0 cellspacing=0 cellpadding=0>";
				strTxt += "		<tr class='intercalaLst" + (estilo-1)%2 + "'> ";
				strTxt += "     	<td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 title=<bean:message key="prompt.retirardogrupo"/> class=geralCursoHand onclick=removeResu(\"" + nLinha + "\")></td> ";
				strTxt += "       	<input type=\"hidden\" name=\"idResu\" value=\"" + nResultado + "\" > ";
				strTxt += "     	<td class=principalLstPar width=55%> " + cResultado ;
				strTxt += "     	</td> ";
				strTxt += "		</tr> ";
				strTxt += " </table> ";

				lstResultado.innerHTML = lstResultado.innerHTML + strTxt;		
			}else{
				alert('<bean:message key="prompt.selecionarResultadoNaoRepetido" />');
			}
		}
		
		function removeResu(nTblExcluir) {
			msg = '<bean:message key="prompt.desejaExcluirEsseResultado" />';
			if (confirm(msg)) {
				//objIdTbl = window.document.all.item(nTblExcluir);
				objIdTbl = document.getElementById(nTblExcluir);
				lstResultado.removeChild(objIdTbl);
				//estilo--;
			}
		}
	
		function preencheHidden(){
		
			if (document.administracaoCsCdtbPublicoPublForm['pucoLimite'].value > 0 && document.administracaoCsCdtbPublicoPublForm['pucoLimite'].value != ""){
				document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.pucoNrLimitedistrib'].value=document.administracaoCsCdtbPublicoPublForm['pucoLimite'].value;
			}
		
		}
	
		function textCounter(field, countfield, maxlimit) {
			if (field.value.length > maxlimit) 
				field.value = field.value.substring(0, maxlimit);
			else 
				countfield.value = maxlimit - field.value.length;
		}
	
	
		function HabilitaLimite(chk) {
		
			if (chk.checked==false) {
				document.administracaoCsCdtbPublicoPublForm['pucoLimite'].disabled=false;
			}else{
				document.administracaoCsCdtbPublicoPublForm['pucoLimite'].disabled=true;
				document.administracaoCsCdtbPublicoPublForm['pucoLimite'].value='';
				document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.pucoNrLimitedistrib'].value='-1';
			}
		}
	
		function SetClassFolder(pasta, estilo) {
			stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
			eval(stracao);
		} 
	  
		function AtivarPasta(pasta) {
			switch (pasta)	{
				case 'RESULTADO':
					SetClassFolder('tdResultado','principalPstQuadroLinkSelecionado');
					SetClassFolder('tdEncerramento','principalPstQuadroLinkNormal');
					SetClassFolder('tdCamposEspeciais','principalPstQuadroLinkNormalMAIOR');
					SetClassFolder('tdObservacao','principalPstQuadroLinkNormal');
					MM_showHideLayers('Resultado','','show','lstResultado','','show','Encerramento','','hide', 'Observacao','','hide', 'CamposEspeciais','','hide');
				
					break;
					
				case 'ENCERRAMENTO':
					SetClassFolder('tdResultado','principalPstQuadroLinkNormal');
					SetClassFolder('tdEncerramento','principalPstQuadroLinkSelecionado');
					SetClassFolder('tdCamposEspeciais','principalPstQuadroLinkNormalMAIOR');
					SetClassFolder('tdObservacao','principalPstQuadroLinkNormal');
					MM_showHideLayers('Resultado','','hide','lstResultado','','hide','Encerramento','','show', 'Observacao','','hide', 'CamposEspeciais','','hide');
					
					break;
					
				case 'OBSERVACAO':
					SetClassFolder('tdResultado','principalPstQuadroLinkNormal');
					SetClassFolder('tdEncerramento','principalPstQuadroLinkNormal');
					SetClassFolder('tdCamposEspeciais','principalPstQuadroLinkNormalMAIOR');
					SetClassFolder('tdObservacao','principalPstQuadroLinkSelecionado');
					MM_showHideLayers('Resultado','','hide','lstResultado','','hide','Encerramento','','hide', 'Observacao','','show', 'CamposEspeciais','','hide');
					break;
					
				case 'CAMPO':
					SetClassFolder('tdResultado','principalPstQuadroLinkNormal');
					SetClassFolder('tdEncerramento','principalPstQuadroLinkNormal');
					SetClassFolder('tdObservacao','principalPstQuadroLinkNormal');
					SetClassFolder('tdCamposEspeciais','principalPstQuadroLinkSelecionadoMAIOR');
					MM_showHideLayers('Resultado','','hide','lstResultado','','hide','Encerramento','','hide', 'Observacao','','hide', 'CamposEspeciais','','show');
					break;	
			}
		}	
		
		//Esta fun??o insere novos options na combo que passarem pelo objeto
		function preencheCombo(Objeto,Valor,Texto){
			var Evento = document.createElement("OPTION");
				
			Evento.value = Valor;
			Evento.text = Texto;
			Objeto.add(Evento);
		}
		
		//Esta fun??o remove o option de um combo passado pelo objeto
		function removeCombo(Objeto,Item){
			Objeto.remove(Item);
		}
	
	
		//Esta fun??o faz todos os movimentos entre as duas combos de sele??o
		function tranferir(tudo, origem, destino){
			if (tudo){
				for (var i= 0; i < origem.length ; i++){
					preencheCombo(destino, origem.options(i).value, origem.options(i).text);
				}
				
				for (var i= origem.length; i >=  0 ; i--){
					removeCombo(origem,i);
				}
			}
			
			else{
			
				if(origem.selectedIndex == -1){
					alert('<bean:message key="prompt.alert.E_necessario_informar_no_minimo_um_item"/>');
				}
				else{
					for (var i= 0; i < origem.length ; i++){
						if (origem.options(i).selected){
							preencheCombo(destino, origem.options(i).value, origem.options(i).text);
						}
					}
					
					for (var i= origem.length -1; i >=  0 ; i--){
						if (origem.options(i).selected){
							removeCombo(origem,i);
						}
					}		
				}
			}
		}

		// jvarandas - 16/07/2010
		// Fun��o criada para tratar os links do documento exibido na edi��o.
		// Se o link possui campos especiais, ele n�o pode ser exibido, pois poderia ser aberto com erros
		function trataLinks(obj) {
			var links = obj.getElementsByTagName("a");
			for(var i=0; i<links.length; i++) {
				if(links[i].href.indexOf("[[") > -1) {
					links[i].href = "javascript:linkNaoDisponivel()";
				}	
			}
		}
		
	</script>
</head>
		
	
<body name='bodyMenu' id='bodyMenu' bgcolor="#FFFFFF" text="#000000" leftmargin="0" class="principalBgrPageIFRM" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">


<html:form styleId="administracaoCsCdtbPublicoPublForm" action="/AdministracaoCsCdtbPublicoPubl.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />

	
	<input readonly type="hidden" name="remLen" size="3" maxlength="3" value="4000" > 

	<html:hidden property="publTxObservacao"/>
		
	<table border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	    <td class="principalPstQuadroLinkSelecionado" align="center" id="tdResultado" name="tdResultado" onclick="AtivarPasta('RESULTADO')"><bean:message key="prompt.definirresultado"/></td>
	    <td class="principalPstQuadroLinkNormal" align="center" id="tdEncerramento" name="tdEncerramento" onclick="AtivarPasta('ENCERRAMENTO')"><bean:message key="prompt.encerramento"/></td>
	    <td class="principalPstQuadroLinkNormal" align="center" id="tdObservacao" name="tdObservacao" onclick="AtivarPasta('OBSERVACAO')"><bean:message key="prompt.observacao"/></td>
		<td class="principalPstQuadroLinkNormalMAIOR" align="center" id="tdCamposEspeciais" name="tdCamposEspeciais" onclick="AtivarPasta('CAMPO')"><bean:message key="prompt.camposEspeciais"/></td>    
	  </tr>
	</table>
	<table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" height="150">
	  <tr> 
	    <td valign="top"> 
	      <!-- Chamado: 81580 -->
	      <div id="Resultado" style="position:absolute; width:99%; height:130px; z-index:3; display: block;"> 
	        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	          <tr> 
	            <td class="principalEspacoPqn">&nbsp;</td>
	          </tr>
	          <tr> 
	            <td class="principalLabel"><bean:message key="prompt.resultado"/></td>
	          </tr>
	          <tr> 
	            <td> 
	              <table width="99%" border="0" cellspacing="0" cellpadding="0">
	                <tr> 
	                  <td width="91%"> 
						  <html:select property="idResuCdResultado" styleClass="principalObjForm"> 
						  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
						  <html:options collection="csCdtbResultadoResuVector" property="idResuCdResultado" labelProperty="resuDsResultado"/>
						  </html:select>
	                  </td>
	                  <td width="9%" align="center"><img src="webFiles/images/botoes/setaDown.gif" width="21" title="<bean:message key="prompt.adicionaraogrupo"/>" height="18" class="geralCursoHand" onclick="adicionarResu()"></td>
	                </tr>
	              </table>
	            </td>
	          </tr>
	          <tr> 
	            <td class="espacoPqn">&nbsp;</td>
	          </tr>
	          <tr> 
	            <td height="60" valign="top"> 
	              <!-- Chamado: 81580 -->
	              <div id="lstResultado" style="overflow:auto; position:absolute; width:99%; height:60px; z-index:4; display: block"> 
	              	<input type="hidden" name="idResu" value="0">
	
					<!--Inicio Lista Resultado -->
				 	<logic:present name="listResultadoVector">
					  <logic:iterate id="cnrrVector" name="listResultadoVector">
						<script language="JavaScript">
						  addResu('<bean:write name="cnrrVector" property="resuDsResultado" />',
								  '<bean:write name="cnrrVector" property="idResuCdResultado" />');
						</script>
					  </logic:iterate>
					</logic:present>
	              </div>
	            </td>
	          </tr>
	        </table>
	      </div>
	      
	      <!-- encerramento -->
	      <!-- Chamado: 81580 -->
	      <div id="Encerramento" style="position:absolute; width:99%; height:130px; z-index:3; display: none"> 
	        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	          <tr> 
	            <td class="principalEspacoPqn">&nbsp;</td>
	          </tr>
	          <tr> 
	            <td class="principalLabel"><bean:message key="prompt.motivoEncerramentoDesc"/></td>
	          </tr>
	          <tr> 
	            <td> 
	              <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                <tr> 
	                  <td> 
						  <html:select property="idMoenCdMotivoEncerra" styleClass="principalObjForm"> 
						  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
						  <html:options collection="csCdtbMotivoEncerraMoenVector" property="idMoenCdMotivoEncerra" labelProperty="moenDsMotivoEncerra"/>
						  </html:select>
	                  </td>
	                </tr>
	                <tr> 
	                  <td class="principalLabel"><bean:message key="prompt.data"/></td>
	                </tr>
	                <tr>
	                  <td>
	                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                      <tr>
	                        <td width="27%">
	                        	<html:text property="publDhEncerramento" styleClass="text" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" />
	                        </td>
	                       	<td width="73%"><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario"/>" width="16" height="15" onclick="show_calendar('administracaoCsCdtbPublicoPublForm.publDhEncerramento')" class="principalLstParMao" ></td>
	                      </tr>
	                    </table>
	                  </td>
	                </tr>
	              </table>
	            </td>
	          </tr>
	        </table>
	      </div>
	      <!-- encerramento -->
	      
	      <!-- observacao -->
	      <!-- Chamado: 81580 -->
	      <div id="Observacao" style="position:absolute; width:99%; height:130px; z-index:1; display: none"> 
	        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr> 
				  <td class="principalLabel">&nbsp;&nbsp;&nbsp;<bean:message key="prompt.observacao"/></td>	
			  </tr> 
	          <tr> 
	            <td>
	                 <div id="divConteudoObservacao" style="z-index:1;overflow: auto; width:635px; height: 120px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; overflow:auto;"> 
				      <script>
				    	document.write(document.administracaoCsCdtbPublicoPublForm.publTxObservacao.value);
				
				    	trataLinks(document.getElementById('divConteudoObservacao'));
				      </script>
				     </div>
			   </td>
			   <td width="5%"> 
			     <img src="webFiles/images/botoes/bt_CriarCarta.gif" name="imgEditor" width="25" height="22" class="geralCursoHand" border="0" title="<bean:message key="prompt.observacao"/>" onClick="MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=document.administracaoCsCdtbPublicoPublForm.publTxObservacao&carta=true','Documento','width=850,height=494,top=0,left=0')"> 
			   </td>
	          </tr>
	        </table>
	      </div>
	      <!-- observacao -->      
	
	      <!-- Campos Especiais -->
	      <!-- Chamado: 81580 --> 
	      <!--Jonathan | Adequa��o para o IE 10-->
	      <div id="CamposEspeciais" style="position:absolute; width:98%; height:130px; z-index:3; display: none; overflow: auto"> 
	        <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;1&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">		
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico1" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;2&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico2" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;3&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico3" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;4&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico4" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;5&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico5" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;6&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico6" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;7&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico7" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;8&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico8" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;9&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico9" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;10&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico10" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;11&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico11" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;12&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico12" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;13&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico13" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;14&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico14" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;15&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico15" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;16&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico16" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;17&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico17" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;18&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico18" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr> 
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;19&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico19" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr>
			  <tr> 			  
				  <td class="principalLabel" width="15%">
					  <bean:message key="prompt.descricao"/>&nbsp;20&nbsp; <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				  </td>
				  <td width="85%">
				  	<html:text property="csCdtbLabelpublicoLapuVo.lapuDsPublico20" styleClass="principalObjForm" maxlength="40"/>
				  </td>
			  </tr>  
	        </table>
	      </div>
	      <!-- label -->    
	    </td>
	  </tr>
	</table>
	
	<!-- 
		Valdeci -  29/05/2006 - Chamado:19420
		Bloco de codigo retirado para melhorar a performance da tela, devido lentidao do loop java script quando havia muitos
		funcionarios cadastrados. O algoritmo que verifica as pessoas que estao selecionadas esta no action. No iterate acima e feito a parte que esconde 
		os funcionarios que nao estao selecionados.
	-->

</html:form>
</body>
</html>
