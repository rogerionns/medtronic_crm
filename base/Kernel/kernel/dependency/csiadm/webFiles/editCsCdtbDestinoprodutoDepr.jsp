<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposDestinoproduto(){
		document.administracaoCsCdtbDestinoprodutoDeprForm.deprDsDestinoproduto.disabled= true;	
		document.administracaoCsCdtbDestinoprodutoDeprForm.deprDhInativo.disabled= true;
	}
	
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbDestinoprodutoDeprForm.idDeprCdDestinoproduto.value);
	}
	
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbDestinoprodutoDeprForm" action="/AdministracaoCsCdtbDestinoprodutoDepr.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>		

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idDeprCdDestinoproduto" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="deprDsDestinoproduto" styleClass="text" maxlength="40" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" colspan=3>&nbsp;
          	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
          	 	<tr>
          	 		<td align="right" class="principalLabel">
          	 			<html:checkbox property="deprInEnviarAnalise"/>
          	 		</td>
          	 		<td class="principalLabel">
          	 			<bean:message key="prompt.enviarAnalise"/>
          	 		</td>
          	 		<td align="right" class="principalLabel">
          	 			<html:checkbox property="deprInDisponiveloperador"/>
          	 		</td>
          	 		<td class="principalLabel">
          	 			<bean:message key="prompt.disponivelOperador"/>
          	 		</td>
                <td align="right">
                  <html:checkbox value="true" property="deprDhInativo"/><!-- @@ --></td>
                <td class="principalLabel">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposDestinoproduto();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_DESTINOPRODUTO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbDestinoprodutoDeprForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_DESTINOPRODUTO_INCLUSAO_CHAVE%>')){
				desabilitaCamposDestinoproduto();
			}else{
				document.administracaoCsCdtbDestinoprodutoDeprForm.idDeprCdDestinoproduto.disabled= false;
				document.administracaoCsCdtbDestinoprodutoDeprForm.idDeprCdDestinoproduto.value= '';
				document.administracaoCsCdtbDestinoprodutoDeprForm.idDeprCdDestinoproduto.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_DESTINOPRODUTO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_DESTINOPRODUTO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbDestinoprodutoDeprForm.imgGravar);	
				desabilitaCamposDestinoproduto();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbDestinoprodutoDeprForm.deprDsDestinoproduto.focus();}
	catch(e){}
</script>