<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="com.iberia.helper.Constantes"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
	function iniciaTela(){
		
		if(document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.length == 2){
			document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.selectedIndex = 1;
		}
		parent.validaChkCombo();
		
		if(parent.administracaoCsAstbProdutoManifPrmaForm.acao.value == '<%= Constantes.ACAO_EDITAR %>'){
			if(document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value != "0"){
				if(parent.desabilita == true){
					setTimeout('parent.desabilitaCamposProdutoManif()',500);
				}
			}
		}
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="iniciaTela();">
<html:form action="/AdministracaoCsAstbProdutoManifPrma.do" styleId="administracaoCsAstbProdutoManifPrmaForm">
	<html:hidden property="idGrmaCdGrupoManifestacao" />
	
	<html:select property="idTpmaCdTpManifestacao" styleClass="principalObjForm">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		<logic:present name="combo">
			<html:options collection="combo" property="idTpmaCdTpManifestacao" labelProperty="tpmaDsTpManifestacao"/>
		</logic:present>
	</html:select>
</html:form>
</body>
<script>
	validaFlagProduto();
	
function validaFlagProduto(){
	if (parent.ifrmCmbGrupoManif.document.readyState != "complete"){
		setTimeout("validaFlagProduto()",100);
	}else{
		if (parent.getFlagProduto() == 1){
			document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled = true;
			parent.ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.disabled = true;	
		}
	}	
} 

	
</script>
</html>