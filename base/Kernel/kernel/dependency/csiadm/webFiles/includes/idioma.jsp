<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="java.util.Vector"%>

<%@page import="com.iberia.helper.Constantes"%>
<script language="JavaScript">

	function desabilitaTelaIdioma(){
	
		window.top.desabilitadoIdioma=true;
		window.top.document.all.item('divInternacional').style.visibility = "visible";
	}

	function habilitaTelaIdioma(){
	
		window.top.desabilitadoIdioma=true;
		window.top.document.all.item('divInternacional').style.visibility = "hidden";
	}
	
	function setaArquivoXml(arquivo){
	
		window.top.arquivoXml = arquivo;
	}

	function setaChavePrimaria(chavePrimaria){
	
		window.top.chavePrimaria = chavePrimaria;
	}
	
	function setaIdiomaBloqueia(){
		window.top.document.getElementById("imgInternacional").disabled=false;
		window.top.desabilitadoIdioma=true;
	}
	
	function setaIdiomaHabilita(){
		window.top.desabilitadoIdioma=false;
		window.top.document.getElementById("imgInternacional").disabled=false;
	}
	
	function abrirModalIdioma(obj, idChave){
		var url = 'Idioma.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=ifrmIdioma&arquivoXml=' + obj + '&chavePrimaria=' + idChave ;
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:340px');
	}
	
	
</script>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>