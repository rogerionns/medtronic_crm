<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="com.plusoft.plugin.classloader.PluginRegister"%>
<%@page import="com.plusoft.plugin.classloader.PlusoftClassloader"%>
<%@page import="java.util.Locale"%>
<%@ page import="br.com.plusoft.fw.util.*"%>
<%!
	String acronymChar(String texto, int len){
		String result = null;
		if (texto == null || texto.equals(""))
			result = "&nbsp;";
		else{
			texto = Tools.strReplace("\\", "\\\\", texto);
			texto = Tools.strReplace("'", "\\\'", texto);
			texto = Tools.strReplace("\"", "&quot", texto);
			
			if (texto.length() > len) {
				
				if(texto.length() > 1000){
					result = "<ACRONYM style=\"border: 0\" title=\"" + texto.substring(0, 1000) + "...\">" +
					 texto.substring(0, len) + "...</ACRONYM>";
				}else{
					result = "<ACRONYM style=\"border: 0\" title=\"" + texto + "\">" +
					 texto.substring(0, len) + "...</ACRONYM>";
				}
				
			} else {
				result = texto;
			}
		}
		return result;
	}

	/**
	* Esta rotina tem como objetivo limitar o tamanho da string
	* para que a mesma nao ultrapasse o tamanho pré determinado
	*/
	String acronym(String texto, int len){
		String result = null;
		if (texto == null)
			result = "&nbsp;";
		else{
			if (texto.length() > len) {
				result = "<ACRONYM style=\"border: 0\" title=\"" + texto + "\">" +
						 texto.substring(0, len) + "...</ACRONYM>";
			} else {
				result = texto;
			}
		}
		return result;
	}
	
	/**
	* Este metodo tem como objetivo retornar a String de acordo com o idioma selecionado.
	*/
	String getMessage(String key, HttpServletRequest request){
		String idioma = ((Locale)request.getSession().getAttribute("org.apache.struts.action.LOCALE")).getLanguage();
		String desc = null;
		try{
			CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo) request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
			
			//Tenta objter a informa��o no arquivo especifico
			PlusoftClassloader plugin = PluginRegister.getPlugin(empresaVo.getIdEmprCdEmpresa());
			java.util.ResourceBundle appConf = null;
			if(plugin != null)
				appConf = java.util.ResourceBundle.getBundle("ApplicationResourcesEspec_" + idioma,((Locale)request.getSession().getAttribute("org.apache.struts.action.LOCALE")),plugin);
			else
				appConf = java.util.ResourceBundle.getBundle("ApplicationResourcesEspec_" + idioma);
			
			desc = appConf.getString(key);
		}catch(Exception e){
			try{
				//Tenta objter a informa��o no arquivo do kernel
				java.util.ResourceBundle appConf = java.util.ResourceBundle.getBundle("ApplicationResourcesAdm_" + idioma);
				desc = appConf.getString(key);
			}catch(Exception x){
				throw new com.plusoft.util.AppException("com.plusoft.util.PropertiesFile", "N�o foi poss�vel obter a descricao de acordo com o idioma("+ idioma + ") com o nome (" + key + ").", e);
			}
		}
		return desc;

	}
%>
