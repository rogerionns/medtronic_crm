<%@page import="br.com.plusoft.csi.adm.helper.MultiEmpresaHelper"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="java.util.Vector"%>

<script language="JavaScript">
	
	var empresasAssociadas = "";

	function setaAssociacaoMultiEmpresa(){
		
		var strTxt = "";
		
		<%if(request.getSession().getAttribute("csCdtbEmpresaEmprAssocVector") != null){%>
			<%Vector v = (Vector)request.getSession().getAttribute("csCdtbEmpresaEmprAssocVector");%>
			<%for(int i=0;i<v.size();i++){ %>
				<%MultiEmpresaVo vo = (MultiEmpresaVo)v.get(i);%>
					strTxt+="<input type=hidden name=empresas value=\'" + '<%=vo.getEmprDsEmpresa()%>' + "\'></input>";
			<%}%>
		<%}%>
		
		try{
			
			if(strTxt == "")
				strTxt = document.getElementById("empresasAssociadas").value;
			
		}catch(e){}
		
		window.top.document.getElementById("divEmpresas").innerHTML=strTxt;
	}
	
	function setDadosViewState(obj){
		
		try{
			
			//trecho multi-empresa espec
			if(window.top.listaIdEmprCdEmpresaAux != ''){
				obj.listaIdEmprCdEmpresa.value = window.top.listaIdEmprCdEmpresaAux;
			}else{
				obj.listaIdEmprCdEmpresa.value = obj.document.listaIdEmprCdEmpresaViewState;
			}
			
			if(window.top.listaIdNiveCdNivelacessoAux != '')
				obj.listaIdNiveCdNivelacesso.value = window.top.listaIdNiveCdNivelacessoAux;
			
			if(window.top.emprInPrincipalAux != '')
				obj.emprInPrincipal.value = window.top.emprInPrincipalAux;
			
			if(window.top.csCdtbEmpresaEmprAssocVectorAux != ''){
				obj.csCdtbEmpresaEmprAssocVector.value = window.top.csCdtbEmpresaEmprAssocVectorAux;
			}else{
				obj.csCdtbEmpresaEmprAssocVector.value = obj.document.csCdtbEmpresaEmprAssocVectorViewState;
			}
			
		}catch(e){}
		
	}
	
	function podeGravarAssociacao(){
		
		var nCount = 0;
		var nmEmpresas = "";
		var nmEmpresas2 = "";
		
		var obj = window.top.document.getElementsByName("empresas");
		if(obj != null && obj != undefined){
			if(obj.length != null && obj.length != undefined){
				for (nNode=0;nNode<obj.length;nNode++) {
					nCount++;
					nmEmpresas+=obj[nNode].value;
					if(nNode != (obj.length-1)){
						nmEmpresas+=" , "
					}
				}
			}else{
				nCount++;
			}
		}
		
		if(nCount<=0){
			if(window.top.ifrmCmbEmpresa.document.getElementById("administracaoEmpresaForm").idEmprCdEmpresa.value > 0){
				if(window.top.ifrmCmbEmpresa.document.getElementById("administracaoEmpresaForm").idEmprCdEmpresa[window.top.ifrmCmbEmpresa.document.getElementById("administracaoEmpresaForm").idEmprCdEmpresa.selectedIndex].textContent != null){
					nmEmpresa2 = window.top.ifrmCmbEmpresa.document.getElementById("administracaoEmpresaForm").idEmprCdEmpresa[window.top.ifrmCmbEmpresa.document.getElementById("administracaoEmpresaForm").idEmprCdEmpresa.selectedIndex].textContent;
				}else{
					nmEmpresa2 = window.top.ifrmCmbEmpresa.document.getElementById("administracaoEmpresaForm").idEmprCdEmpresa[window.top.ifrmCmbEmpresa.document.getElementById("administracaoEmpresaForm").idEmprCdEmpresa.selectedIndex].text;
				}
					if(confirm("<bean:message key="prompt.EsteRegistroSeraAssociadoaEmpresa"/>" + nmEmpresa2)){
						return true;
					}else{
						return false;
					}
			}else{
				alert("<bean:message key="prompt.selecioneUmaOpcao"/>");
				return false;
			}
		}else{
			if(confirm("<bean:message key="prompt.EsteRegistroSeraAssociadoAsEmpresas"/>" + nmEmpresas)){
				return true;
			}else{
				return false;
			}
		}
				
	}
	
	function desabilitaComboListaEmpresa(){

		//window.top.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.disabled=true;
		//window.top.document.getElementById("imgLstEmpresas").disabled = true;
		//window.top.document.getElementById("imgLstEmpresas").imgLstEmpresas.className = 'geralImgDisable';
		//window.top.document.getElementById("imgLstEmpresas").title = '';
		//window.top.document.getElementById("divDesabilita").style.visibility = 'visible';

	
	}
	
	
	function desabilitaListaEmpresas(){
		
		//if(window.top.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.disabled==true){
		//	window.top.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.disabled=false;
		//}
		
		//window.top.document.getElementById("imgLstEmpresas").disabled=true;
		//window.top.document.getElementById("imgLstEmpresas").className = 'geralImgDisable';
		//window.top.document.getElementById("imgLstEmpresas").title = '';
		window.top.document.getElementById("divDesabilita").style.visibility = "visible";

		removeCombo(window.top.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa,0);
		window.top.ifrmCmbEmpresa.document.administracaoEmpresaForm.mostrarSelUmaOpcao.value="N";
		
		
		if(window.top.ifrmCmbEmpresa.itemAnterior==0){
			window.top.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.selectedIndex=0;
			window.top.ifrmCmbEmpresa.cmbEmpresa_onChange();
		}
		
	}
	
	function habilitaListaEmpresas(){
	
		//if(window.top.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.disabled==true){
		//	window.top.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.disabled=false;
		//}
		
		window.top.desabilitado=true; 
		//window.top.document.getElementById("imgLstEmpresas").disabled=false;
		//window.top.document.getElementById("imgLstEmpresas").className = 'geralCursoHand';
		//window.top.document.getElementById("imgLstEmpresas").title = "<bean:message key="prompt.empresasCadastradas"/>";
		
		window.top.document.getElementById("divDesabilita").style.visibility = "hidden";
		
		preencheCombo(window.top.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa,0,"<bean:message key='prompt.selecione_uma_opcao' />");
		window.top.ifrmCmbEmpresa.document.administracaoEmpresaForm.mostrarSelUmaOpcao.value="S";
	}

	//Esta funcao insere novos options na combo que passarem pelo objeto
	function preencheCombo(Objeto,Valor,Texto){
		try{
			if(Objeto[0].value != 0){
				var Evento = document.createElement("OPTION");
				//Evento.value = Valor;
				//Evento.text = Texto;
				//Objeto.add(Evento,0);
				Objeto.options.add(Evento,0);
				Evento.text = Texto;
				Evento.value = Valor;
			}
		}catch(e){}
	}
	
	//Esta funcao remove o option de um combo passado pelo objeto
	function removeCombo(Objeto,Item){
		try{
			if(Objeto[0].value == 0){
				Objeto.remove(Item);
			}
		}catch(e){}
	}
	
	function setaListaBloqueia(){
		window.top.document.getElementById("imgLstEmpresas").disabled=false;
		window.top.desabilitado=true;
	}
	
	function setaListaHabilita(){
		window.top.desabilitado=false;
		window.top.document.getElementById("imgLstEmpresas").disabled=false;
	}
</script>
