<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	
function inicio(){
	showError('<%=request.getAttribute("msgerro")%>');
	validarPermissao();
}	

function validarPermissao(){
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LINHA_CARACT_PROD_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LINHA_CARACT_PROD_ALTERACAO_CHAVE%>', parent.administracaoCsCatbLinhaCaractProdLcprForm.imgGravar);
			desabilitaCamposLinhaCaractProd();
		}
	</logic:equal>
}

function desabilitaCamposLinhaCaractProd(){
	try{
		setPermissaoImageDisable('', administracaoCsCatbLinhaCaractProdLcprForm.imgAvancar);
		setPermissaoImageDisable('', administracaoCsCatbLinhaCaractProdLcprForm.imgVoltar);
		administracaoCsCatbLinhaCaractProdLcprForm["caprDsCaracteristicaprod"].disabled= true;
		administracaoCsCatbLinhaCaractProdLcprForm.idLinhCdLinha.disabled=true;
		lstCaracteristica.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.disabled=true;
		lstCaracteristica.administracaoCsCatbLinhaCaractProdLcprForm.todos.disabled=true;
		lstCaracteristicaGrupo.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.disabled=true;
		lstCaracteristicaGrupo.administracaoCsCatbLinhaCaractProdLcprForm.todos.disabled=true;
		
	}
	catch(e){
		setTimeout("desabilitaCamposLinhaCaractProd();", 300);
	}
}


function atualizaListaCaracteristica(bOrigemClick) {
	
	if(bOrigemClick){
		administracaoCsCatbLinhaCaractProdLcprForm["caprDsCaracteristicaprod"].value="";
	}
	
	oldAcao = administracaoCsCatbLinhaCaractProdLcprForm.acao.value;
	oldTela = administracaoCsCatbLinhaCaractProdLcprForm.tela.value;
	
	administracaoCsCatbLinhaCaractProdLcprForm.acao.value = '<%= Constantes.ACAO_CONSULTAR %>';
	administracaoCsCatbLinhaCaractProdLcprForm.tela.value = '<%= MAConstantes.TELA_IFRM_LST_CARACTERISTICAPROD %>';
	administracaoCsCatbLinhaCaractProdLcprForm.target = lstCaracteristica.name;
	administracaoCsCatbLinhaCaractProdLcprForm.submit();
	
	administracaoCsCatbLinhaCaractProdLcprForm.acao.value = oldAcao;
	administracaoCsCatbLinhaCaractProdLcprForm.tela.value = oldTela;
}	

function moveToRight() {
	selecionou = 0;
	if (lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.length == 0) {
		alert('<bean:message key="prompt.nao_existem_caracteristicas_na_lista_para_serem_selecionados"/>');
		return false;
	}
	for (i = 0; i < lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.length; i++) {
		if (lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.options[i].selected) { 
			selecionou = 1;
			if (!(verificaExistencia(lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.options[i].value, lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha))) {
				alert('<bean:message key="prompt.a_caracteristica"/> ' + lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.options[i].text + ' <bean:message key="prompt.ja_foi_selecionado_para_esta_linha"/>');
			}
			else {
				lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.options[lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.length] = new Option(lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.options[i].text, lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.options[i].value);											
				lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.options[i] = null;
				i--;
			}
		}
	}
	lstCaracteristica.atualizaTotal(lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.length);
	lstCaracteristicaGrupo.atualizaTotal(lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.length);
	if (selecionou == 0)
		alert('<bean:message key="prompt.Por_favor_selecione_uma_caracteristica"/>');
}

function moveToLeft() {
	selecionou = 0;
	if (lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.length == 0) {
		alert('<bean:message key="prompt.nao_existem_caracteristicas_na_lista_para_serem_selecionados"/>');
		return false;
	}
	for (i = 0; i < lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.length; i++) {
		if (lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.options[i].selected) { 
			selecionou = 1;
			if ((verificaExistencia(lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.options[i].value, lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica))) {
				lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.options[lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.length] = new Option(lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.options[i].text, lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.options[i].value);											
			}
			lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.options[i] = null;
			i--;
		}
	}
	lstCaracteristica.atualizaTotal(lstCaracteristica.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.length);
	lstCaracteristicaGrupo.atualizaTotal(lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.length);
	if (selecionou == 0)
		alert('<bean:message key="prompt.Por_favor_selecione_uma_caracteristica"/>');
}

// Verifica se o elemento ja foi copiado
function verificaExistencia(valor, obj) {
	for (j = 0; j < obj.length; j++) {
		if (obj.options[j].value == valor)
			return false;	
	}
	return true;
}

function filtrar(evnt) {
	if (evnt.keyCode == 13){
		atualizaListaCaracteristica(false);
		return false;
	}
}

function getListaCaracteristicaLinha() {
	var html = "";
	for (i = 0; i < lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.length; i++) {
		html += "<input type=\"hidden\" name=\"idCaprCdCaracteristicaLinha\" value=\"" + lstCaracteristicaGrupo.document.administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristicaLinha.options[i].value + "\">";
	}
	document.getElementById("lstCaractGrupo").innerHTML = html;
}

function atualizaListaCaracteristicaLinha(){
	var url = "";

	url = "AdministracaoCsCatbLinhaCaractProdLcpr.do?tela=<%= MAConstantes.TELA_LST_CS_CATB_LINHACARACTPROD_LCPR%>"
	url = url + "&acao=<%= Constantes.ACAO_CONSULTAR %>"
	url = url + "&idLinhCdLinha=" + administracaoCsCatbLinhaCaractProdLcprForm.idLinhCdLinha.value;
	
	lstCaracteristicaGrupo.location.href = url;
	
	url = "AdministracaoCsCatbLinhaCaractProdLcpr.do?tela=<%=MAConstantes.TELA_IFRM_LST_CARACTERISTICAPROD%>";
	url = url +	"&acao=<%=Constantes.ACAO_CONSULTAR%>";

	lstCaracteristica.location.href = url;
	
}

</script>
<body class= "principalBgrPageIFRM" onload="inicio()">

<html:form styleId="administracaoCsCatbLinhaCaractProdLcprForm" action="/AdministracaoCsCatbLinhaCaractProdLcpr.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>	
	
	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.linha"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
			<html:select property="idLinhCdLinha" styleClass="principalObjForm" onchange="atualizaListaCaracteristicaLinha();">
				<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
				<html:options collection="vectorLinh" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
			</html:select>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <!-- INICIO ABA DE Caracteristica -->
        <tr>
        	<td colspan="4">
	        	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td height="254">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"	align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
								<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado" id="caracteristica" name="caracteristica">
										<bean:message key="prompt.caracteristicaProduto" />
									</td>
								</tr>
								</table>
							</td>
							<td width="4">
								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
							</td>
						</tr>
						</table>

						<table width="100%" border="0" cellspacing="0" cellpadding="0"	align="center">
						<tr>
							<td valign="top" class="principalBgrQuadro" height="70">
								
								<!-- TABELA DE FORA -->
								<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
								<tr>
									<!-- LISTA DE Caracteristicas -->
									<td width="48%" height="30">
										<table border="0" cellpadding="0" cellspacing="4" width="100%" height="100%">
										<tr>
											<td class="principalLabel" height="15">
												<bean:message key="prompt.caracteristicaProduto"/><br>
												<html:text property="caprDsCaracteristicaprod" styleClass="text" maxlength="60" onkeydown="return filtrar(event);"/> 
											</td>	
										</tr>
										<tr>
											<td class="principalLabel" valign="top">
												<iframe name="lstCaracteristica" id="lstCaracteristica"	src="AdministracaoCsCatbLinhaCaractProdLcpr.do?tela=<%= MAConstantes.TELA_IFRM_LST_CARACTERISTICAPROD%>&acao=<%= Constantes.ACAO_CONSULTAR %>"	width="100%" height="175" scrolling="no" frameborder="0"	marginwidth="0" marginheight="0"></iframe>
											</td>	
										</tr>
										</table>	
									</td>
									<!-- LISTA DE FUNCIONARIOS -->
									
									<!-- BOTOES DE MOVIMENTACAO -->
									<td valign="top" width="4%" height="100">
										<table cellpadding="0" cellspacing="0" width="100%" height="80%" border="0" align="center">
										<tr><td height="80"></td></tr>
			  							<tr>
			  								<td valign="top" align="center"><img src="webFiles/images/botoes/avancar_unico.gif" name="imgAvancar" width=21 height=18 class=geralCursoHand title="<bean:message key="prompt.Adicionar_caracteristica_a_linha"/>" onclick="moveToRight();"></td>
			  							</tr>
			  				  			<tr>
			  								<td valign="top" align="center"><img src="webFiles/images/botoes/voltar_unico.gif" name="imgVoltar" width=21 height=18 class=geralCursoHand title="<bean:message key="prompt.Remover_caracteristica_a_linha"/>" onclick="moveToLeft();"></td>
							  			</tr>
			  							</table>
									</td>
									<!-- BOTOES DE MOVIMENTACAO -->
									
									<!-- LISTA DE Caracteristicas associadas com linha-->
									<td height="100">
										<table border="0" cellpadding="0" cellspacing="4" width="100%" height="100%">
										<tr>
											<td class="principalLabel" height="34">
												&nbsp;
											</td>	
										</tr>
										<tr>
											<td class="principalLabel" colspan="2" valign="top">
												<iframe name="lstCaracteristicaGrupo" id="lstCaracteristicaGrupo" src="AdministracaoCsCatbLinhaCaractProdLcpr.do?tela=<%= MAConstantes.TELA_LST_CS_CATB_LINHACARACTPROD_LCPR%>&acao=<%= Constantes.ACAO_CONSULTAR %>&idLinhCdLinha=<bean:write name="administracaoCsCatbLinhaCaractProdLcprForm" property="idLinhCdLinha"/>"	width="100%" height="175" scrolling="no" frameborder="0"	marginwidth="0" marginheight="0"></iframe>
											</td>	
										</tr>
										</table>	
									</td>
									<!-- LISTA DE CARACTERISTICA ASSOCIADAS A LINHA -->
									
								</tr>
								</table>
								<!-- FINAL DA TABELA DE FORA -->
								
							</td>
							<td width="4" height="230"><img	src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>						
						</tr>
						<tr>
							<td width="100%" height="8" valign="top"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
							<td width="4" height="8" valign="top"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
        <!-- FINAL ABA DE FUNCIONARIO -->
        
      </table>

      <div id="lstCaractGrupo"></div>
</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
		
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LINHA_CARACT_PROD_INCLUSAO_CHAVE%>', parent.administracaoCsCatbLinhaCaractProdLcprForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_LINHA_CARACT_PROD_INCLUSAO_CHAVE%>')){
				desabilitaCamposLinhaCaractProd();
			}else{
				//administracaoCsCatbLinhaCaractProdLcprForm["idLinhCdLinha"].disabled= false;
				//administracaoCsCatbLinhaCaractProdLcprForm["idLinhCdLinha"].disabled= true;
			}
		
		</script>
</logic:equal>

</html>

<script>
	try{administracaoCsCatbLinhaCaractProdLcprForm["idLinhCdLinha"].focus();}
	catch(e){}
</script>
