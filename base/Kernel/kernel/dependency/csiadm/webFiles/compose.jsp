<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");	
%>

<%@page import="com.iberia.helper.Constantes"%>

<html>
	<head>
		<title><bean:message key="prompt.tituloCorresp"/></title>
		<LINK href="webFiles/css/global.css" type=text/css rel=stylesheet>
		<!-- 90860 - 27/09/2013 - Jaider Alba
			 - Removida meta de compatibilidade do IE 8 para processar apenas a de IE7
			 - Colocado o include dentro da tag <head>, pois gerava "HTML1503: Marca de in�cio inesperada."
			   fazendo com q as tags meta n�o fossem processadas 
		<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
		-->
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
	</head>
	
	<script type="text/javascript" src="webFiles/fckeditor/fckeditor.js"></script>
	<script language="JavaScript">
		/**********************************************************
		 A��es executadas ao terminar de carregar o editor na tela
		**********************************************************/
		function FCKeditor_OnComplete(editorInstance){
		
			var valor = 0;
			if('<%=request.getParameter("campo")%>' != ''){
				valor = eval("window.opener.<%=request.getParameter("campo")%>.value");
			}
			
			FCKeditorAPI.GetInstance('EditorCartas').SetHTML(valor);
			document.getElementById("divAguarde").style.visibility = "hidden";
		}
		
		/************************************
		 Ao clicar no bot�o gravar do editor
		************************************/
		function acaoGravar(){

			//Chamado: 87490 - 05/04/2013 - Carlos Nunes
			var textoTamanho = FCKeditorAPI.GetInstance('EditorCartas').EditingArea.Document.body.innerText.length;

		    //Chamado: 91924 - 21/11/2013 - Carlos Nunes
			var cTamanhoAux1 = FCKeditorAPI.GetInstance('EditorCartas').GetXHTML(true);
			var cTamanhoAux2 = FCKeditorAPI.GetInstance('EditorCartas').EditingArea.Document.body.innerHTML.replace(/ /gim,'');

			var cTamanho = cTamanhoAux1;

			if(cTamanhoAux2.length > cTamanhoAux1.length)
			{
				cTamanho = cTamanhoAux2;
			}
			
			var tamParam = '<%=request.getParameter("tamanho")%>';
			
			//Chamado: 85355 - TERRA - Aprovado pelo Marcelo Braga
			if(tamParam != undefined && tamParam != null && tamParam > 0){
				//tamParam = Number(tamParam) - 10;
				if(tamParam < cTamanho.length || tamParam < textoTamanho){

					var msgLimite =  '<bean:message key="prompt.aviso.qtd.caracteres.excedidos"/>';
					    msgLimite += '\n<bean:message key="prompt.aviso.qtd.caracteres.permitido.texto"/>' + tamParam;
					    msgLimite += '\n\n<bean:message key="prompt.aviso.qtd.caracteres.informado.texto"/>' + textoTamanho;
					    msgLimite += '<bean:message key="prompt.aviso.qtd.caracteres.informado.texto.formatado"/>' + cTamanho.length;
						
					alert(msgLimite);	
					//alert('<bean:message key="prompt.tamanhomaximoatingido"/>'+(cTamanho.length-tamParam));
					return false;
				}
			}
			
			if(confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_e_fechar_este_documento"/>")){
				var cRetorno = FCKeditorAPI.GetInstance('EditorCartas').GetXHTML(true);
				<%//Chamado: 100038 KERNEL-1001 - 02/04/2015 - Marcos Donato //%>
				cRetorno = cRetorno.replace(/'/g, "&#39;");
				window.opener.setFunction(cRetorno, "<%=request.getParameter("campo")%>");
				window.close();
			}else{
				return false;
			}	
		}
		
		/*********************************************************
		 Retorna Array para preencher o combo de campos especiais
		*********************************************************/
		function getValoresCamposEspeciais(){
			return valoresCamposEspeciais;
		}
		
		/****************************************************
		 Valores para preencher o combo deo campos especiais
		****************************************************/
		var valoresCamposEspeciais = new Array();
		<logic:present name="csCdtbCampoEspecialCaesVector">		
			<logic:iterate id="cccecVector" name="csCdtbCampoEspecialCaesVector">
				valoresCamposEspeciais[valoresCamposEspeciais.length] = new Object();
				valoresCamposEspeciais[valoresCamposEspeciais.length - 1].valor = "<bean:write name="cccecVector" property="caesDsTag" />";
				valoresCamposEspeciais[valoresCamposEspeciais.length - 1].descricao = "<bean:write name="cccecVector" property="caesDsTituloCampo" />";
			</logic:iterate>  
		</logic:present>     
		
	</script>
	
	<body topmargin=0 leftmargin=0 bottommargin=0 rightmargin=0 style="overflow: hidden;">
		<form name="composeForm" method=post action="">
			<div id="divAguarde" style="position:absolute; left:380px; top:200px; width:199px; height:148px; visibility: visible"> 
				<div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
			</div>
			<div id="divTravaEditor" class="geralImgDisable" style="position: absolute; top: 195; left: 0; height: 420; width: 100%; background-color: #cdcdcd; visibility: hidden">&nbsp;</div>
			
			<!-- Editor HTML -->
			<script type="text/javascript">
				var oFCKeditor = new FCKeditor('EditorCartas');
								
				oFCKeditor.Config["AutoDetectLanguage"] = "false" ;
				oFCKeditor.Config["DefaultLanguage"] = "<bean:message key="prompt.language"/>" ;
				
				oFCKeditor.Create();
				
				
			</script>
		
		</form>
	</body>
</html>