<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html:html>
<head>
	<title><bean:message key="prompt.moduloDeCadastroMSD"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	<style type="text/css">
		body { background-image: url('webFiles/images/login/pt/tela_cadastro.jpg'); }
		
		div#loginSaas { position:absolute; top: 80px; left: 50px; width: 300px; height: 300px; }
	</style>

	<script type="text/javascript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	<script type="text/javascript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onLoad="showError('<%=request.getAttribute("msgerro")%>');" style="overflow: hidden;">
	<html:form>
	
	<html:hidden property="tela" />
	<html:hidden property="acao" value=""/>
	<html:hidden property="sobrescreverLogin" value=""/>

	<div id="loginSaas">
		<select name="empresaSaas" id="empresaSaas" size="10" class="pOF" ondblclick="document.forms[0].submit();">
			<logic:present name="empresasSaasVector">
			<logic:iterate name="empresasSaasVector" id="empresaSaas">
			
				<option value="<bean:write name="empresaSaas" property="field(id_empr_cd_empresa)"/>">
					<bean:write name="empresaSaas" property="field(empr_ds_empresa)"/>
				</option>
			</logic:iterate>
			</logic:present>
		
		
		</select>
		<img style="margin-top: 10px; cursor: pointer;" src="webFiles/images/login/bt_ok01.gif" id="ImageOK" width="72" height="29" onclick="document.forms[0].submit();" />
	</div>
	
	<img style="position:absolute; right: 45px; top: 35px; " src="/plusoft-resources/images/logo/saas_plusoft.gif" />
	
	</html:form>
</body>
</html:html>
