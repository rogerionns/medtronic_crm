<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>

<script>
function atualizaTipo() {
	
	
	if(administracaoCsCdtbAtendpadraoAtpaForm.idAsn1CdAssuntonivel1.value != ""){
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.value = administracaoCsCdtbAtendpadraoAtpaForm.idAsn1CdAssuntonivel1.value +"@"+ administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.value;
		<%}else{%>
			administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.value = administracaoCsCdtbAtendpadraoAtpaForm.idAsn1CdAssuntonivel1.value +"@1";
		<%}%>
	}
	else{
		administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.value = "0@"+ administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.value;
	}

	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
		administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.value = "1";
	<%}%>
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		
		document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%= MAConstantes.TELA_CMB_VARIEDADE_INFO %>';
		document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = "<%=MAConstantes.ACAO_POPULACOMBO%>";
		document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmCmbVariedadeInfo.name;
		document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	
	<%}else{%>
	
		document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%=MAConstantes.TELA_CMB_TIPO %>';
		document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmCmbTipo.name;
		document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
		
		document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%=MAConstantes.TELA_CMB_TOPICO %>';
		document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmCmbTopico.name;
		document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
		
		document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%=MAConstantes.TELA_TEXTO_INFORMACAO %>';
		document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmTextoInformacao.name;
		document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
		
	<%}%>
}

function iniciaTela(){
	<logic:notEqual name="administracaoCsCdtbAtendpadraoAtpaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
		try{
			if(document.administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.length == 2){
				document.administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.selectedIndex = 1;
			}
		}catch(e){}
	</logic:notEqual>

	atualizaTipo();
}

	function mostraCampoBuscaProd(){
		cTela = document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value;
		cAcao = document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value;
		document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = "<%=Constantes.ACAO_CONSULTAR%>";
		document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = "<%= MAConstantes.TELA_CMB_PRODUTO_INFO %>";
		administracaoCsCdtbAtendpadraoAtpaForm.target = this.name;
		document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
		document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = cTela;
		document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = cAcao;
	}
	
	function buscarProduto(){

		if (document.administracaoCsCdtbAtendpadraoAtpaForm['asn1DsAssuntonivel1'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}

		document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = "<%= MAConstantes.TELA_CMB_PRODUTO_INFO %>";
		document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = "<%=MAConstantes.ACAO_POPULACOMBO%>";
		administracaoCsCdtbAtendpadraoAtpaForm.target = this.name;
		document.administracaoCsCdtbAtendpadraoAtpaForm.submit();

	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}

	//Chamado: 89525 - 15/07/2013 - Carlos Nunes
	function atualizaDadosEdicao()
	{
		parent.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.value = "0";
		administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.value = "";
		administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.value = "0";

		atualizaTipo();
	}
</script>
	
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">

	<html:form action="/AdministracaoCsCdtbAtendpadraoAtpa.do" styleId="administracaoCsCdtbAtendpadraoAtpaForm">
		
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
        <html:hidden property="idTpinCdTipoinformacao" />
	    <html:hidden property="idToinCdTopicoinformacao" />
		<html:hidden property="idAsn2CdAssuntonivel2" />
		<html:hidden property="idAsnCdAssuntoNivel" />
		<html:hidden property="idLinhCdLinha" />
	    
		<logic:notEqual name="administracaoCsCdtbAtendpadraoAtpaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="95%">
						<html:select property="idAsn1CdAssuntonivel1" styleClass="principalObjForm" onchange="atualizaDadosEdicao();">
							<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
							<html:options collection="combo" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" labelProperty="prasDsProdutoAssunto"/> 
						</html:select>                              
				 	</td>
				 	<td width="5%" valign="middle">
				 		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
				 	</td>
				</tr>
			</table>
		</logic:notEqual>
	  
		<logic:equal name="administracaoCsCdtbAtendpadraoAtpaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
				<tr>
					<td width="95%">
						<html:text property="asn1DsAssuntonivel1" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
					</td>
					<td width="5%" valign="middle">
						<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
					</td>
				</tr> 	
			</table>
			<html:hidden property="idAsn1CdAssuntonivel1" />
		</logic:equal>

	</html:form>
</body>
</html>
<logic:equal name="administracaoCsCdtbAtendpadraoAtpaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	<script>
		document.administracaoCsCdtbAtendpadraoAtpaForm['asn1DsAssuntonivel1'].focus();
	</script>
</logic:equal>