<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>

<script language="JavaScript">
	function inicio(){
		//setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbTpagendamentoTpagForm['csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento'].value);
	}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbTpagendamentoTpagForm" action="/AdministracaoCsCdtbTpagendamentoTpag.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>	
	<html:hidden property="csCdtbTpagendamentoTpagVo.idPublCdPublico"/>

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="csCdtbTpagendamentoTpagVo.tpagDsTpagendamento" styleClass="text" maxlength="60" style="width: 312px;" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.subcampanha"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <iframe name="ifrmCmbPublico" id="ifrmCmbPublico" src="AdministracaoCsCdtbTpagendamentoTpag.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PUBLICO %>&csCdtbTpagendamentoTpagVo.idPublCdPublico=<bean:write name="administracaoCsCdtbTpagendamentoTpagForm" property="csCdtbTpagendamentoTpagVo.idPublCdPublico"/>" width="100%" height="22" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        
        
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.quantidade"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="csCdtbTpagendamentoTpagVo.tpagNrQuantidade" styleClass="text" maxlength="4"/>
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <!--
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.tempo"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="csCdtbTpagendamentoTpagVo.tpagNrTempo" styleClass="text" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        -->
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="inInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbTpagendamentoTpagForm["csCdtbTpagendamentoTpagVo.tpagDsTpagendamento"].disabled= true;	
			document.administracaoCsCdtbTpagendamentoTpagForm["inInativo"].disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			document.administracaoCsCdtbTpagendamentoTpagForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].disabled= false;
			document.administracaoCsCdtbTpagendamentoTpagForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value= '';
			document.administracaoCsCdtbTpagendamentoTpagForm["csCdtbTpagendamentoTpagVo.tpagNrQuantidade"].value= '';
			//document.administracaoCsCdtbTpagendamentoTpagForm["csCdtbTpagendamentoTpagVo.tpagNrTempo"].value= '';
			document.administracaoCsCdtbTpagendamentoTpagForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].disabled= true;
		</script>
</logic:equal>
</html>