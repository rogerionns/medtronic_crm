<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
function VerificaCampos(){
	if (document.administracaoCsAstbAcaoProgramaAcpgForm.acpgNrSequencia.value == "0" ){
		document.administracaoCsAstbAcaoProgramaAcpgForm.acpgNrSequencia.value = "" ;
	}
	if (document.administracaoCsAstbAcaoProgramaAcpgForm.acpgNrDiasUteis.value == "0" ){
		document.administracaoCsAstbAcaoProgramaAcpgForm.acpgNrDiasUteis.value = "" ;
	}

}


function MontaLista(){
	lstArqCarga.location.href= "AdministracaoCsAstbAcaoProgramaAcpg.do?tela=administracaoLstCsAstbAcaoProgramaAcpg&acao=<%=Constantes.ACAO_VISUALIZAR%>&idTppgCdTipoPrograma=" + document.administracaoCsAstbAcaoProgramaAcpgForm.idTppgCdTipoPrograma.value;
}


</script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos();MontaLista()" style="overflow: hidden;">

<html:form styleId="administracaoCsAstbAcaoProgramaAcpgForm" action="/AdministracaoCsAstbAcaoProgramaAcpg.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<input type="hidden" name="incluirIdAsnCdAssuntoNivel" />
	<input type="hidden" name="excluirIdAsnCdAssuntoNivel" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.tipoPrograma"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
    	<html:select property="idTppgCdTipoPrograma" styleClass="principalObjForm" onchange="MontaLista()"> 
         <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
         <html:options collection="csCdtbTpProgramaTppgVector" property="idTppgCdTipoPrograma" labelProperty="tppgDsTipoPrograma"/> 
      	</html:select> 
      </td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.acao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idAcaoCdAcao" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbAcaoAcaoVector" property="idAcaoCdAcao" labelProperty="acaoDsDescricao"/> 
      </html:select> </td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.diasUteis"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="8%"><html:text property="acpgNrDiasUteis" styleClass="text" maxlength="5" /></td>
    <td width="52%">&nbsp;</td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.sequencia"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="8%"><html:text property="acpgNrSequencia" styleClass="text" maxlength="5" /></td>
    <td width="52%" align="right"><img src="webFiles/images/botoes/Produto_Assunto.gif" width="22" height="22" class="geralCursoHand" title="<bean:message key="prompt.produtoacao" />" onclick="showModalDialog('AdministracaoCsAstbAcaoProgramaAcpg.do?acao=visualizar&tela=lstCsAstbProdutoAcaoPrac&idAcaoCdAcao=' + document.all('idAcaoCdAcao').value + '&idTppgCdTipoPrograma=' + document.all('idTppgCdTipoPrograma').value,window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')">&nbsp;</td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%" align="right" class="principalLabel">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%">&nbsp;</td>
    <td>&nbsp;</td>
    <td> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="83%"> <html:checkbox value="true" property="acpgDhInativo"/> 
          </td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/></td>
        </tr>
      </table>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="220"> 
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="6%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="36%"><bean:message key="prompt.acao"/></td>
          <td class="principalLstCab" width="33%">&nbsp;</td>
          <td class="principalLstCab" width="11%"><bean:message key="prompt.diasUteis"/></td>
          <td class="principalLstCab" width="14%"><bean:message key="prompt.sequencia"/></td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
/*
			document.administracaoCsAstbAcaoProgramaAcpgForm.idAcaoCdAcao.disabled= true;
			document.administracaoCsAstbAcaoProgramaAcpgForm.idTppgCdTipoPrograma.disabled= true;
			document.administracaoCsAstbAcaoProgramaAcpgForm.acpgNrDiasUteis.disabled= true;
			document.administracaoCsAstbAcaoProgramaAcpgForm.acpgNrSequencia.disabled= true;
*/
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			document.administracaoCsAstbAcaoProgramaAcpgForm.idAcaoCdAcao.disabled= false;
			document.administracaoCsAstbAcaoProgramaAcpgForm.idTppgCdTipoPrograma.disabled= false;

			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			/*document.administracaoCsAstbAcaoProgramaAcpgForm.csCdtbAcaoAcaoVo.idAcaoCdAcao.disabled= false;
			document.administracaoCsAstbAcaoProgramaAcpgForm.csCdtbAcaoAcaoVo.idAcaoCdAcao.value= '';
			document.administracaoCsAstbAcaoProgramaAcpgForm.csCdtbAcaoAcaoVo.idAcaoCdAcao.disabled= true;*/
		</script>
</logic:equal>
</html>