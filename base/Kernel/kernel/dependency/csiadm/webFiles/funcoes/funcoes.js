function formataCPFCNPJ(campo) { 
	var texto = "";
	for (var i = 0; i < campo.value.length; i++) {
		texto += campo.value.substring(i, i + 1).match(/[0-9]/);
	}
	num = String(texto);
	switch(num.length) {
		case 11 :
		 campo.value = num.substring(0,3) + "." + num.substring(3,6) + "." + num.substring(6,9) + "-" + num.substring(9,11);
		 return;
		case 14 :
		 campo.value = num.substring(0,2) + "." + num.substring(2,5) + "." + num.substring(5,8) + "/" + num.substring(8,12) + "-" + num.substring(12,14);
		 return;
		default : 
		 return;
	}
}

function showError(msgErro) {
	if (msgErro != 'null')
		showModalDialog('/csiadm/webFiles/erro.jsp?msgerro=' + msgErro,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:250px,dialogTop:0px,dialogLeft:200px');
}

/*
var message="Op??o inv?lida."
function click(e) { file://3.0
 if (document.all) {
  if (event.button == 2 || event.button == 3 || event.button == 6 || event.button == 7) {
   alert(message);
   return false;
   }
 }
 if (document.layers) {
  if (e.which == 3) {
   alert(message);
   return false;
   }
 }
}
 if (document.layers) {
  document.captureEvents(Event.MOUSEDOWN);
  }
document.onmousedown=click;
*/

function acronym(texto, nr) {
	if (texto.length > nr) {
		document.write("<ACRONYM style=\"border: 0\" title='" + texto + "'>");
		document.write(texto.substring(0, nr) + "...");
		document.write("</ACRONYM>");
	} else {
		document.write(texto);
	}
}

function acronymLst(texto, nr) {
	acro = "";
	if (texto.length > nr) {
		acro += "<ACRONYM title=\"" + texto + "\" style=\"border: 0\">";
		acro += texto.substring(0, nr) + "...";
		acro += "</ACRONYM>";
	} else {
		acro = texto;
	}
	return acro;
}

function textCounter(field, maxlimit) {
	if (field.value.length > maxlimit) 
		field.value = field.value.substring(0, maxlimit);
}
function isDigito(obj) {
    if (((event.keyCode < 48) ||(event.keyCode > 57)) && event.keyCode != 8)
        event.returnValue = false;
}

function desabilitaEnter(obj){
	if (event.keyCode == 13)
		event.returnValue = false;
}

function trim(cStr){
	if (typeof(cStr) != "undefined"){
		var re = /^\s+/
		cStr = cStr.replace (re, "")
		re = /\s+$/
		cStr = cStr.replace (re, "")
		return cStr
	}
	else
		return ""
}	

function showQuestion(msgErro) {
	if (msgErro != 'null')
		showModalDialog('webFiles/ifrmMsgQuetion.jsp?msgerro=' + msgErro,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:170px,dialogTop:0px,dialogLeft:200px');
}

function isDigitoVirgula(evnt) {
	var tk;
	// Recebe a tela pressionada
	tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : evnt.which;
	
	if(tk == 9 || tk == 8 || tk == 0){
		return true;
	}

	if( (!((tk >= 96 && tk <= 105) || (tk >= 48 && tk <= 57) || tk == 110 || tk == 190)) || tk == 13){
		(navigator.appName == "Microsoft Internet Explorer") ? event.returnValue = null : evnt.returnValue = null;
		return false;
	}
}


function date(sDate1, sDate2) {
	   var diff = new Date();
	   var date1, date2;
	   matchArray1 = sDate1.split(/\/|-/);
	   Day1 = matchArray1[0];
	   Month1 = matchArray1[1] - 1;
	   Year1 = matchArray1[2];
	   matchArray2 = sDate2.split(/\/|-/);
	   Day2 = matchArray2[0];
	   Month2 = matchArray2[1] - 1;
	   Year2 = matchArray2[2];
	   date1 = new Date(Year1, Month1, Day1);
	   date2 = new Date(Year2, Month2, Day2);
	   diff.setTime(Math.max(date1.getTime(), date2.getTime()));
	   timediff = diff.getTime();
	   if(date1.getTime() == date2.getTime()){
	   		return false;
	   }else if(timediff == date1.getTime()){
	   		return true;
	   }else{
	   		return false;
	   }
	   days = Math.floor(timediff/(1000 * 60 * 60 * 24));
	   return days;
	}


	function validaData(periodo){
		if(periodo.value.length == 10){
	    if(periodo.value != ""){
	 		 
	 		 	arrayOfString = periodo.value.split('/');
				if(arrayOfString.length == 10){
					alert("Data Invalida");
					periodo.focus();
					return false;
				}
				for (var i=0; i < arrayOfString.length; i++) {
					if(i==0) {
						dia = arrayOfString[i];
						if(dia > 31 || dia < 1){
							alert("Data Invalida");
							periodo.focus();
							return false;
						}
					}
					else if(i==1) {
						fevereiro = arrayOfString[0];
						mes = arrayOfString[i];
						
						if((fevereiro == 30 || fevereiro == 31) && mes == 02){
							alert("Data Invalida");
							periodo.focus();
							return false;
						}
						
						if(mes > 12 || mes < 1){
							alert("Data Invalida");
							periodo.focus();
							return false;
						}
					}
					else if(i==2) {
						ano = arrayOfString[i];
						if(ano < 1900){
							alert("Data Invalida");
							periodo.focus();
							return false;
						}
					}
				}	
	 	}
	 	}
	}

	function tamanhoData(obj){
		if(obj.value == "")
			return true;
		if(obj.value.length == 10){
			return true;
		}else{
			alert("Data Invalida");
			obj.focus();
			return false;
		}	
	}

	function formataTelefone(obj){
		if(obj.value.length == 4)
	        obj.value += "-"	
	}

	function formataData(obj){
		if(obj.value.length == 2 || obj.value.length == 5)
	        obj.value += "/"
	}