/* Exemplo
 *
 *   <input type="text" name="data" size=10 maxlength=10
 *      onKeyPress="javascript:validaDigito(this);" 
 *      onBlur="javascript:verificaData(this);"
 *      value="" >
 */      
function isEmpty(s) {
    return ((s == null) || (s.length == 0));
}

function validaDigito(maskara,obj) {

    if ((event.keyCode < 48) ||(event.keyCode > 57)){
        event.returnValue = false;
	}else if (maskara == 'DD/MM/YYYY'){
			if(obj.value.length == 2 || obj.value.length == 5){
		        	obj.value += "/"
			}		
	}else if (maskara == 'DD/MM'){
			if(obj.value.length == 2){
		    	obj.value += "/"
			}
	}
}

function verificaDataMask(maskara,inputObj){
    if(isEmpty(inputObj.value)){
       return false;
    } else if(maskara == 'DD/MM/YYYY'){
		    if(!isDate(inputObj.value)){
		          alert("Data inv�lida");
		          inputObj.focus();
		          return false;
		    }  
    } else if (maskara == 'DD/MM'){
				
			   if (!validaFormData(inputObj.value)) return false
			   
			   dia = inputObj.value.substring(0,2);     
			   mes = inputObj.value.substring(3,5);     
 			
 			   if (dia.substring(0,1) == 0) dia = dia.substring(1,2);
		       if (mes.substring(0,1) == 0) mes = mes.substring(1,2);
			
		       dia = parseInt(dia);
		       mes = parseInt(mes);
    				    
			   if (dia <= 0 || dia > 31){
					alert('Dia inv�lido');
					inputObj.focus();
					return false;
			   }
			   
			   if (mes <= 0 || mes >	12){
			   		alert('M�s inv�lido');
					inputObj.focus();
					return false;  
			   }
			   
    } else if (maskara == 'DD'){
	    	  
	    	  dia = inputObj.value.substring(0,2);
	    	  
	    	  if (dia.substring(0,1) == 0) dia = dia.substring(1,2);
	    	  
	    	  dia = parseInt(dia);
	    	  if (dia <= 0 || dia > 31){
					alert('Dia inv�lido');
					inputObj.focus();
					return false;
			  }
    }
    return true;    
}

function validaDigitoHora(maskara,obj) {
    
    if ((event.keyCode < 48) ||(event.keyCode > 57)){
	    event.returnValue = false;
    }else if (maskara=='HH:MM'){
	   		if(obj.value.length == 2){
	        	obj.value += ":";
	   		}
	}else if (maskara=='HH:MM:SS'){
	   		if(obj.value.length == 2 || obj.value.length == 5){
	        	obj.value += ":";
	    	}
    }else if (maskara=='MM:SS'){
    		if(obj.value.length == 2){
	        	obj.value += ":";
	   		}
    }
	
}

function validaHora(maskara,obj){
	
	if (obj.value!=""){
		if(maskara=='HH:MM:SS'){
			if (obj.value.length < 8) {
				alert('Formato inv�lido \n' + 'Ex.: ' + maskara);   
		        obj.focus();
		        return false;
		    } else if (obj.value.substring(0, 2) < 0 || obj.value.substring(0, 2) > 23) {
		        alert("Hora inv�lida");
		        obj.focus();
		        return false;
		    } else if (obj.value.substring(3, 5) < 0 || obj.value.substring(3, 5) > 59 || obj.value.substring(2, 3) != ":") {
		        alert("Minuto inv�lido");
		        obj.focus();
		        return false;
		    } else if (obj.value.substring(6, 8) < 0 || obj.value.substring(6, 8) > 59 || obj.value.substring(6, 8) == ":") {
		        alert("Segundo inv�lido");
		        obj.focus();
		        return false;
		    }
		}
		if(maskara=='HH:MM'){
			verificaHora(obj);
		}
		if(maskara=='MM:SS'){
			if (obj.value.substring(0, 2) < 0 || obj.value.substring(0, 2) > 59) {
        		alert("Minuto inv�lido");
        		obj.focus();
        		return false;
			}else if (obj.value.substring(3, 5) < 0 || obj.value.substring(3, 5) > 59 || obj.value.substring(3, 5) == ":") {
		        alert("Segundo inv�lido");
		        obj.focus();
		        return false;
		    }
		}
		if(maskara=='HH'){
			if (obj.value.substring(0, 2) < 0 || obj.value.substring(0, 2) > 23) {
        		alert("Hora inv�lida");
        		obj.focus();
        		return false;
			}
		}
		if(maskara=='MM'){
			if (obj.value.substring(0, 2) < 0 || obj.value.substring(0, 2) > 59) {
        		alert("Minuto inv�lido");
        		obj.focus();
        		return false;
			}
		}
		if(maskara=='SS'){
			if (obj.value.substring(0, 2) < 0 || obj.value.substring(0, 2) > 59) {
        		alert("Segundo inv�lido");
        		obj.focus();
        		return false;
			}
		}
	}

}

function novaDataENG(data) {
    return new Date(data.substring(3,5)+"/"+data.substring(0,2)+"/"+data.substring(6,data.length))
}

function isDate(texto) {
    // Se esta em branco ou tamanho menor que 10(dd/mm/aaaa)
    if ((texto == "") || (texto.length < 10)) return false

    // Se n�o � num�rico
    if (!validaFormatacaoData(texto)) return false

    dia = texto.substring(0,2);     
    mes = texto.substring(3,5);     
    ano = texto.substring(6,10);

    if (dia.substring(0,1) == 0) dia = dia.substring(1,2)
    if (mes.substring(0,1) == 0) mes = mes.substring(1,2)

    dia = parseInt(dia)
    mes = parseInt(mes)
    ano = parseInt(ano)

    if (ano < 1800) {
    	alert("Ano inferior a 1800")
    	return false
    }

        switch (mes) {
    case 1:
        diafim = 31;
        break; 
    case 2:
        diafim = (ano % 4==0)?29:28
        break;
    case 3:
        diafim = 31;
        break;  
    case 4:
        diafim = 30;
        break; 
    case 5:
        diafim = 31;
        break; 
    case 6:
        diafim = 30;
        break; 
    case 7:
        diafim = 31;
        break;
    case 8:
        diafim = 31;
        break;
    case 9:
        diafim = 30;
        break;
    case 10:
        diafim = 31;
        break;
    case 11:
        diafim = 30;
        break;
    case 12:
        diafim = 31;
        break;
    default:
        return false;
        break;
        }

    return (dia > diafim || dia < 1) ? false : true
}

function validaFormatacaoData(inputVal) {
    inputStr = inputVal.toString()

    for (var i = 0; i < inputStr.length; i++) {
        var oneChar = inputStr.charAt(i)

        // Se existem as barras (/) nos lugare certos
        if ((i == 2 || i == 5) && (oneChar == "/")) continue

        //N�o num�rico retorna false
        if (oneChar < "0" || oneChar > "9") return false
    }

    return true
}

function validaFormData(inputObj){
       inputStr = inputObj.toString()

	    for (var i = 0; i < inputStr.length; i++) {
	        var oneChar = inputStr.charAt(i)
	
	        // Se existem as barras (/) nos lugare certos
	        if ((i == 2) && (oneChar == "/")) continue
	
	        //N�o num�rico retorna false
	        if (oneChar < "0" || oneChar > "9") return false
	    }
	    return true
}

function validaPeriodo(ini,fim){
var dataIni;
var dataFim;

    dataIni = novaDataENG(ini);
    dataFim = novaDataENG(fim);

    if(dataFim<dataIni){
       alert("A data fim n�o pode ser menor que a de in�cio");
       return false;
    }
    return true;
}

function verificaDataAntigo(inputObj){
    if(!isEmpty(inputObj.value)){
       if(!isDate(inputObj.value)){
          alert("Data inv�lida");
          inputObj.focus();
          inputObj.value="";
          return false;
       }
    }
    return true;    
}

function verificaData(inputObj){
    if(isEmpty(inputObj.value)){
       return false;
    } else if(!isDate(inputObj.value)){
          alert("Data inv�lida");
          inputObj.focus();
	      inputObj.value="";
          return false;
    }
    return true;    
}

function verificaHora(inputObj) {
	if (inputObj.value.length > 6) {
        alert('Formato inv�lido \n Ex.: HH:SS');
        inputObj.focus();
        return false;
    } else if (inputObj.value.substring(0, 2) < 0 || inputObj.value.substring(0, 2) > 23) {
        alert("Hora inv�lida");
        inputObj.focus();
        return false;
    } else if (inputObj.value.substring(3, 5) < 0 || inputObj.value.substring(3, 5) > 59 || inputObj.value.substring(2, 3) != ":") {
        alert("Hora inv�lida");
        inputObj.focus();
        return false;
    }
    return true;    
}

/**
 * Retorna a diferen�a em dias entre duas datas.
 */
function dateDiff(sDate1, sDate2){
    var diff  = new Date();
    var date1, date2;

    matchArray1 = sDate1.split(/\/|-/)
    Day1 = matchArray1[0]
    Month1 = matchArray1[1]-1
    Year1 = matchArray1[2]
    
    matchArray2 = sDate2.split(/\/|-/)
    Day2 = matchArray2[0]
    Month2 = matchArray2[1]-1
    Year2 = matchArray2[2]
    
    date1 = new Date(Year1,Month1,Day1)
    date2 = new Date(Year2,Month2,Day2)

    diff.setTime(Math.abs(date1.getTime() - date2.getTime()));
    timediff = diff.getTime();
    days = Math.floor(timediff / (1000 * 60 * 60 * 24));
    return days;
}