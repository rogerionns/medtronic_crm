var sBrowser = navigator.userAgent.toLowerCase() ;

function isIE(){
	return (sBrowser.indexOf("msie") > -1);
}

function isFirefox(){
	return !isIE();
}
	
	//valida??o de campo numero, funciona no firefox no ie6 e ie7
	function mascara(o,f){
	    v_obj=o
	    v_fun=f
	    setTimeout("execmascara()",1)
	}

	function execmascara(){
	    v_obj.value=v_fun(v_obj.value)
	}
	
	function soNumeros(v){
    return v.replace(/\D/g,"")
	}

	function soNumerosComNegativos(v){
		return v.replace(/[^\d\-]/g,"")
	}

function formataCPFCNPJ(campo) { 
	var texto = "";
	for (var i = 0; i < campo.value.length; i++) {
		texto += campo.value.substring(i, i + 1).match(/[0-9]/);
	}
	num = String(texto);
	switch(num.length) {
		case 11 :
		 campo.value = num.substring(0,3) + "." + num.substring(3,6) + "." + num.substring(6,9) + "-" + num.substring(9,11);
		 return;
		case 14 :
		 campo.value = num.substring(0,2) + "." + num.substring(2,5) + "." + num.substring(5,8) + "/" + num.substring(8,12) + "-" + num.substring(12,14);
		 return;
		default : 
		 return;
	}
}

function showError(msgErro) {
	if (msgErro != 'null')
		showModalDialog('webFiles/erro.jsp?msgerro=' + msgErro,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:250px,dialogTop:0px,dialogLeft:200px');
}


/*
var message="Op??o inv?lida."
function click(e) { file://3.0
 if (document.all) {
  if (event.button == 2 || event.button == 3 || event.button == 6 || event.button == 7) {
   alert(message);
   return false;
   }
 }
 if (document.layers) {
  if (e.which == 3) {
   alert(message);
   return false;
   }
 }
}
 if (document.layers) {
  document.captureEvents(Event.MOUSEDOWN);
  }
document.onmousedown=click;
*/

function acronym(texto, nr) {
	if (texto.length > nr) {
		document.write("<ACRONYM style=\"border: 0\" title='" + texto + "'>");
		document.write(texto.substring(0, nr) + "...");
		document.write("</ACRONYM>");
	} else {
		document.write(texto);
	}
}

function acronymLst(texto, nr) {
	acro = "";
	if (texto.length > nr) {
		acro += "<ACRONYM title=\"" + texto + "\" style=\"border: 0\">";
		acro += texto.substring(0, nr) + "...";
		acro += "</ACRONYM>";
	} else {
		acro = texto;
	}
	return acro;
}

function textCounter(field, maxlimit) {
	if (field.value.length > maxlimit) 
		field.value = field.value.substring(0, maxlimit);
}

/*function isDigito(obj,evnt) {
	var tk;
	// Recebe a tela pressionada
	tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : evnt.which;

	if(tk == 8 || tk == 0){
		return true;
	}

	if((!((tk >= 96 && tk <= 105) || (tk >= 48 && tk <= 57))) || tk == 13){
		(navigator.appName == "Microsoft Internet Explorer") ? event.returnValue = null : evnt.returnValue = null;
		return false;
	}

}*/

function isDigito(evnt) {
    
    var tk;
    // Recebe a tela pressionada
    tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : evnt.which;
    
    if(tk == 9 || tk == 8 || tk == 0 || tk == 46 || tk == 37 || tk == 39 || tk == 16 || tk == 17 || tk == 35 || tk == 36 ){
          return true;
    }

    if((!((tk >= 96 && tk <= 105) || (tk >= 48 && tk <= 57))) || tk == 13){
          (navigator.appName == "Microsoft Internet Explorer") ? event.returnValue = null : evnt.returnValue = null;
          return false;
    }
}

function desabilitaEnter(obj,e){
	if (e.keyCode == 13)
        e.preventDefault();
}

function showKeyCode(e)
{
	alert("keyCode for the key pressed: " + e.keyCode + "\n");
}

function trim(cStr){
	if (typeof(cStr) != "undefined"){
		var re = /^\s+/
		cStr = cStr.replace (re, "")
		re = /\s+$/
		cStr = cStr.replace (re, "")
		return cStr
	}
	else
		return ""
}	

function showQuestion(msgErro) {
	if (msgErro != 'null')
		showModalDialog('webFiles/ifrmMsgQuetion.jsp?msgerro=' + msgErro,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:170px,dialogTop:0px,dialogLeft:200px');
}

function isDigitoVirgula(evnt) {
	
	var tk;
	// Recebe a tela pressionada
	//chamado 81229 - Vinicius - inclusao do sinal de menos (-) || tk == 189 || tk == 109
	tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : evnt.which;
	if(tk == 9 || tk == 8 || tk == 0 || tk == 37 || tk == 39 || tk == 46 || tk == 188 || tk == 189 || tk == 109){
		return true;
	}

	if( (!((tk >= 96 && tk <= 105) || (tk >= 48 && tk <= 57) || tk == 110 || tk == 190)) || tk == 13) {
		(navigator.appName == "Microsoft Internet Explorer") ? event.returnValue = null : evnt.returnValue = null;
		return false;
	}
}

function validaEmail(email)
{
	if (email.value.search(/\S/) != -1) {
		regExp = /[A-Za-z0-9_]+@[A-Za-z0-9_-]{2,}\.[A-Za-z]{2,}/
		if (email.value.length < 7 || email.value.search(regExp) == -1){
			alert ("Por favor preencha corretamente o seu E-Mail");
		    email.focus();
		    return false;
		}						
	}
	num1 = email.value.indexOf("@");
	num2 = email.value.lastIndexOf("@");
	if (num1 != num2){
		alert ("Por favor preencha corretamente o seu E-Mail");
	    email.focus();
		return false;
	}
}
