<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<script type="text/javascript">

	function openWindow(theURL,winName,features) { //v3.0 
		var x = window.open(theURL,winName,features);
		
		if(x==null) alert("<plusoft:message key="prompt.alert.bloqueadorPopups" />");
	}

	function fechaJanela() {
		window.top.opener = top; 
		window.open('','_parent',''); 
		window.top.close();
	}	

</script>
<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes 
AdequacaoSafari - 29/11/2013 - Jaider Alba -->
<body onload="openWindow('webFiles/indexFrame.jsp','','top=0,left=0,width=1014,height=700,top=0,left=0');fechaJanela();">

