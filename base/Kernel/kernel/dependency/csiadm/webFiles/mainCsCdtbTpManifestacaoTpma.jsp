<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	

long idEmpresa = empresaVo.getIdEmprCdEmpresa();

%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="Javascript">

function submitPaginacao(regDe,regAte){
	
	document.administracaoCsCdtbTpManifestacaoTpmaForm.regDe.value=regDe;
	document.administracaoCsCdtbTpManifestacaoTpmaForm.regAte.value=regAte;
	if(regDe > -1 && regAte > -1)
		filtrar();
}

function inicio(){
	initPaginacao();
	//filtrar();
}

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsCdtbTpManifestacaoTpmaForm.target = admIframe.name;
	document.administracaoCsCdtbTpManifestacaoTpmaForm.acao.value ='filtrar';
	document.administracaoCsCdtbTpManifestacaoTpmaForm.submit();
	setTimeout('limpaCampoFiltro()', 10);

	document.administracaoCsCdtbTpManifestacaoTpmaForm.regDe.value='0';
	document.administracaoCsCdtbTpManifestacaoTpmaForm.regAte.value='0';

	initPaginacao();
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbTpManifestacaoTpmaForm.filtro.value = '';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsCdtbTpManifestacaoTpmaForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_TPMANIFESTACAO_TPMA%>';
	editIframe.document.administracaoCsCdtbTpManifestacaoTpmaForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbTpManifestacaoTpmaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo){
	tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao.value = codigo;
	tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_TPMANIFESTACAO_TPMA%>';
	tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.target = editIframe.name;
	tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){

	if(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_TPMANIFESTACAO_TPMA%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_TPMANIFESTACAO_TPMA%>'){

		if(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.idMatpCdManifTipo.value == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_uma_manifestacao"/>");
			tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.idMatpCdManifTipo.focus();
			return false;
		}
		
		if(tab.cmbGrupoManifestacao.document.administracaoCsCdtbTpManifestacaoTpmaForm.idGrmaCdGrupoManifestacao.value == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_um_grupo_de_manifestacao"/>.");
			tab.cmbGrupoManifestacao.document.administracaoCsCdtbTpManifestacaoTpmaForm.idGrmaCdGrupoManifestacao.focus();
			return false;
		}
		
		/*
		if(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionario.value == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_um_responsavel"/>.");
			tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionario.focus();
			return false;
		}
		*/
		
		tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaDsTpManifestacao.value = trim(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaDsTpManifestacao.value);
		if (tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaDsTpManifestacao.value == "") {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaDsTpManifestacao.focus();
			return false;
		}

/*
		if (tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.value != "" && tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaInConclui.checked) {
			alert("<bean:message key="prompt.O_campo_dias_para_resolucao_nao_pode_ser_preenchido_ao_mesmo_tempo_que_o_campo_concluir_em_tempo_de_atendimento"/>.");
			tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.focus();
			return false;
		}
		if (tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.value == "") {
			alert("<bean:message key="prompt.O_campo_dias_para_resolucao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.focus();
			return false;
		}
*/

		if(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.value != ""){
			if (tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.value.search(/[^\d]/) != -1) {
				alert ("<bean:message key="prompt.Por_favor_digite_apenas_numeros"/>.");
				tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.focus();
				return false;
			}	
			
			if(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaInTempoResolucao.value == ""){
				alert("<bean:message key="prompt.Por_favor_informe_se_o_tempo_sera_em_dias_ou_horas"/>.");
				tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaInTempoResolucao.focus();
				return false;
			}	
		}
		
		if(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxProcedimento.value.length > 6000 ){
			alert("<bean:message key='prompt.campoProcedimentoExecedeuSeuLimite'/>")
		}

		if(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxOrientacao.value.length > 6000 ){
			alert("<bean:message key='prompt.campoOrientacaoExecedeuSeuLimite'/>")		
		}

		/*
		if(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxProcedimento.value == ""){
			alert("<bean:message key="prompt.Por_favor_digite_um_procedimento"/>.");
			tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxProcedimento.focus();
			return false;
		}
		if(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxOrientacao.value == ""){
			alert("<bean:message key="prompt.Por_favor_digite_uma_orientacao"/>.");
			tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxOrientacao.focus();
			return false;
		}
		*/
		
		tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionario.value = tab.cmbFuncionarioResponsavel.document.getElementById("administracaoCsCdtbTpManifestacaoTpmaForm").idFuncCdFuncionario.value;
		
		tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_TPMANIFESTACAO_TPMA%>';
		tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.target = admIframe.name;
		tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.idGrmaCdGrupoManifestacao.value = tab.cmbGrupoManifestacao.document.administracaoCsCdtbTpManifestacaoTpmaForm.idGrmaCdGrupoManifestacao.value;
		disableEnable(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao, false);
		tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.submit();
		disableEnable(tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao, true);

		setTimeout("try{filtrar();}catch(e){}",1000);
		cancel();

	}
}

function submeteExcluir(codigo) {
	
	tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao.value = codigo;
	tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_TPMANIFESTACAO_TPMA%>';
	tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.target = editIframe.name;
	tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_TPMANIFESTACAO_TPMA%>';
		editIframe.document.administracaoCsCdtbTpManifestacaoTpmaForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbTpManifestacaoTpmaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao, false);
		editIframe.document.administracaoCsCdtbTpManifestacaoTpmaForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao, true);
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbTpManifestacaoTpma.do?tela=editCsCdtbTpManifestacaoTpma&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbTpManifestacaoTpma.do?tela=editCsCdtbTpManifestacaoTpma&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsCdtbTpManifestacaoTpmaForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_TPMANIFESTACAO_TPMA%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_TPMANIFESTACAO_TPMA%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			setaIdiomaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbTpManifestacaoTpmaForm"	action="/AdministracaoCsCdtbTpManifestacaoTpma.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="regDe" />
	<html:hidden property="regAte" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/></td>
									<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.tipoManifestacao"/></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" height="480px" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td height="480" valign="top" class="principalBgrQuadro"> 
                  <div name="Manifestacao" id="Manifestacao"
								style="position: absolute; width: 97%; height: 450px; z-index: 6; top: 56px; left: 15px; visibility: hidden"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0"
								align="center">
                      <tr>
									
                        <td height="450" valign="top"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsCdtbTpManifestacaoTpma.do?tela=editCsCdtbTpManifestacaoTpma&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="position: absolute; width: 97%; height: 205px; z-index: 2; visibility: visible"> 
                    <table width="99%" border="0" cellspacing="0" cellpadding="0"
								align="center">
                      <tr> 
                        <td class="principalLabel">&nbsp;</td>
                        <td class="principalLabel" colspan="5">&nbsp;</td>
                        <td class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="7"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                             <tr> 
                              	<td width="100%">
                             		<table width="100%" cellspacing="0" cellpadding="0">
	                             		<tr>
	                             			<td class="principalLabel" width="100%" colspan="2">
	                             				<bean:message key="prompt.manifestacao"/>
	                             			</td>
	                             		</tr>
	                              		<tr>
	                              			<td width="50%">
		                              			<html:select property="idMatpCdManifTipo" styleClass="principalObjForm"> 
					    							<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option>
					    							<logic:present name="csCdtbManifTipoMatpVector">
					    								<html:options collection="csCdtbManifTipoMatpVector" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo"/>
					 								</logic:present>
					 							</html:select>
	                              			</td>
	                              			<td width="50%">
	                              				&nbsp;
	                              			</td>
	                              		</tr>
	                              		<tr>
	                             			<td class="principalLabel" width="100%" colspan="2">
	                             				<bean:message key="prompt.descricao"/>
	                             			</td>
	                             		</tr>
	                              		<tr>
		                              		<td>
		                              			<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
		                              		</td>
		                              		<td width="50%">
	                              				&nbsp;<img
												src="webFiles/images/botoes/setaDown.gif" width="21"
												height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()"> 
	                              			</td>
	                              		</tr>
                              		</table>
                              	</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" colspan="7">&nbsp;</td>
                      </tr>
                      <tr>
                      	<td colspan="7" width="100%">
                      	 <div id="barraTitulo" style="width:780px; z-index:1; overflow: hidden; scroll:no;">
                      		<table width="1090" border="0" cellspacing="0" cellpadding="0">
                      		<tr>
	                       	<td class="principalLstCab" align="center" width="11%">&nbsp;&nbsp;<bean:message key="prompt.codigo"/></td>
	                        <td class="principalLstCab" width="15%"><bean:message key="prompt.manifestacao"/></td>
	                        <td class="principalLstCab" width="15%"><bean:message key="prompt.grupoManifestacao"/></td>
	                        <td class="principalLstCab" width="15%"><bean:message key="prompt.tipoManifestacao"/></td>
	                        <td class="principalLstCab" width="15%"><bean:message key="prompt.responsavel"/></td>
	                        <td class="principalLstCab" width="11%"><bean:message key="prompt.classificacao"/></td>
	                        <td class="principalLstCab" width="9%"><bean:message key="prompt.diashoras"/></td>
	                        <td class="principalLstCab" width="9%"><bean:message key="prompt.inativo"/></td>
                        	</tr>
                       	 	</table>
                        </div>
                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="7" height="335"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsCdtbTpManifestacaoTpma.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="no" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
                      </tr>
                      
                      <tr>
                      	<td colspan="7">
                      		<table align="center">
                      			<tr>
                      				<td>
                      					<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>
                      				</td>
                      			</tr>
                      		</table>
                      	</td>
					  </tr>
					  
                    </table>
							</div>

							
                </td>
							<td width="4" height="100%"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="555px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="555px"></td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_INCLUSAO_CHAVE%>', document.administracaoCsCdtbTpManifestacaoTpmaForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbTpManifestacaoTpmaForm.imgGravar.disabled=true;
			document.administracaoCsCdtbTpManifestacaoTpmaForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbTpManifestacaoTpmaForm.imgGravar.title='';
	}
	
	desabilitaListaEmpresas();

	setaArquivoXml("CS_ASTB_IDIOMATPMA_IDTM.xml");
	habilitaTelaIdioma();
	
</script>


</body>
</html>
