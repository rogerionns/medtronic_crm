<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<!-- Chamado 77481 - Vinicius - Pesquisa Multiempresa -->
<%
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposQuestao(){
		document.administracaoCsCdtbQuestaoQuesForm.quesDsQuestao.disabled= true;	
		document.administracaoCsCdtbQuestaoQuesForm.quesDhInativo.disabled= true;
		document.administracaoCsCdtbQuestaoQuesForm.quesInMultipla.disabled= true;
		document.administracaoCsCdtbQuestaoQuesForm.quesInObservacao.disabled= true;
	}
	
	function inicio(){
		//Chamado 77481 - Vinicius - Pesquisa Multiempresa
		setaAssociacaoMultiEmpresa();
		
		setaChavePrimaria(administracaoCsCdtbQuestaoQuesForm.idQuesCdQuestao.value);
	}
</script>

<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbQuestaoQuesForm" action="/AdministracaoCsCdtbQuestaoQues.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />

	<br>
	 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> 
      <!-- ## -->
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="24%"> <html:text property="idQuesCdQuestao" styleClass="text" disabled="true" style="width: 150px" /> 
    </td>
    <td width="30%">&nbsp;</td>
    <td width="36%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> 
      <!-- ## -->
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
   	<td colspan="2"> 
    	<html:textarea property="quesDsQuestao" styleClass="text" style="width:430px;" onkeyup="textCounter(this, 1000)" onblur="textCounter(this, 1000)" rows="12"></html:textarea> 
    </td>
    <td width="36%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.grupoQuestao"/> 
      <!-- ## -->
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
   	<td colspan="2" > <!--Jonathan | Adequa��o para o IE 10-->
    	<html:select property="idGrquCdGrupoQuestao" style="width:432px" styleClass="principalObjForm">
          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
          <html:options collection="grupos" property="field(id_grqu_cd_grupoquestao)" labelProperty="field(grqu_ds_grupoquestao)"/> 
       	</html:select>  
    </td>
    <td width="36%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td width="24%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="9%"> <html:checkbox value="true" property="quesInMultipla"/> 
          </td>
          <td class="principalLabel" width="91%"> <bean:message key="prompt.multiplaEscolha"/></td>
        </tr>
      </table>
    </td>
    <td class="principalLabel" width="30%">&nbsp; </td>
    <td width="36%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td width="24%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="9%"> <html:checkbox value="true" property="quesInObservacao"/> 
          </td>
          <td class="principalLabel" width="91%">&nbsp;<bean:message key="prompt.observacao"/></td>
        </tr>
      </table>
    </td>
    <td class="principalLabel" width="30%">&nbsp;</td>
    <td width="36%">&nbsp;</td>
  </tr> 
  
  <tr> 
    <td colspan="3">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr><!--Jonathan | Adequa��o para o IE 10-->
			    <td width="220px" align="right" class="principalLabel"> Forma de apresenta��o no formu�rio WEB 
			      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
			    </td><!--Jonathan | Adequa��o para o IE 10-->
			   	<td> &nbsp; <!-- Chamado: 84223 - 10/09/2012 - Carlos Nunes  -->
			    	<html:select property="quesInPosicaovertical" style="width:150px" styleClass="principalObjForm">
			          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
			          <html:option value="S"> VERTICAL </html:option>
			          <html:option value="N"> HORIZONTAL </html:option>
			       	</html:select>  
			    </td>
			    <td width="2%"> &nbsp;</td>
			  </tr>
		 </table>
    </td>
    <td width="36%">&nbsp;</td>
  </tr>
  
  <tr>
    <td width="24%">&nbsp;</td>
    <td width="24%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="9%"> <html:checkbox value="true" property="quesDhInativo"/> 
          </td>
          <td class="principalLabel" width="91%">&nbsp;<bean:message key="prompt.inativo"/> </td>
        </tr>
      </table>
    </td>
    <td class="principalLabel" width="30%">&nbsp;</td>
    <td width="36%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposQuestao();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_QUESTAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbQuestaoQuesForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_QUESTAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposQuestao();
			}else{
				document.administracaoCsCdtbQuestaoQuesForm.idQuesCdQuestao.disabled= false;
				document.administracaoCsCdtbQuestaoQuesForm.idQuesCdQuestao.value= '';
				document.administracaoCsCdtbQuestaoQuesForm.idQuesCdQuestao.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_QUESTAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_QUESTAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbQuestaoQuesForm.imgGravar);	
				desabilitaCamposQuestao();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbQuestaoQuesForm.quesDsQuestao.focus();}
	catch(e){}
</script>