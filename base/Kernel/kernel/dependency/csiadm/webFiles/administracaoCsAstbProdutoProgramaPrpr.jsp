<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsAstbProdutoProgramaPrprForm" action="/AdministracaoCsAstbProdutoProgramaPrpr.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idTppgCdTipoPrograma"/>
	<html:hidden property="idAsn1CdAssuntoNivel1"/>
	<html:hidden property="idAsn2CdAssuntoNivel2"/>	

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="capppVector" name="csAstbProdutoProgramaPrprVector">
  <tr> 
    <td width="5%" class="principalLstPar"> <bean:write name="capppVector" property="csCdtbTpProgramaTppgVo.idTppgCdTipoPrograma" /> 
    </td>
    <td class="principalLstParMao" width="73%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="capppVector" property="csCdtbTpProgramaTppgVo.idTppgCdTipoPrograma" />','<bean:write name="capppVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />','<bean:write name="capppVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')"> 
      <bean:write name="capppVector" property="csCdtbTpProgramaTppgVo.tppgDsTipoPrograma" /> 
    </td>
    <td class="principalLstParMao" align="left" width="11%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="capppVector" property="csCdtbTpProgramaTppgVo.idTppgCdTipoPrograma" />','<bean:write name="capppVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />','<bean:write name="capppVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')"> 
      &nbsp; <bean:write name="capppVector" property="csCdtbTpProgramaTppgVo.tppgDhInicio" /> 
    </td>
    <td class="principalLstParMao" align="left" width="11%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="capppVector" property="csCdtbTpProgramaTppgVo.idTppgCdTipoPrograma" />','<bean:write name="capppVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />','<bean:write name="capppVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')"> 
      &nbsp; <bean:write name="capppVector" property="csCdtbTpProgramaTppgVo.tppgDhTermino" /> 
    </td>
  </tr>
  </logic:iterate>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>
<script>
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>