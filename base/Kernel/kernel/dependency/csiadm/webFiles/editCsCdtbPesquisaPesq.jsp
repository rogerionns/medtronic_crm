<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<!-- Chamado 77481 - Vinicius - Pesquisa Multiempresa -->
<%
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<SCRIPT LANGUAGE="JavaScript">
	function VerificaCampos(){
		if (document.administracaoCsCdtbPesquisaPesqForm.pesqNrPrevisaomax.value == "0" ){
			document.administracaoCsCdtbPesquisaPesqForm.pesqNrPrevisaomax.value = "" ;
		}
		if (document.administracaoCsCdtbPesquisaPesqForm.pesqNrPrevisaomin.value == "0" ){
			document.administracaoCsCdtbPesquisaPesqForm.pesqNrPrevisaomin.value = "" ;
		}
	}
	
	function desabilitaCamposPesquisa(){
		setPermissaoImageDisable('', document.administracaoCsCdtbPesquisaPesqForm.imgCalendario);
		document.administracaoCsCdtbPesquisaPesqForm.pesqDsPesquisa.disabled= true;	
		document.administracaoCsCdtbPesquisaPesqForm.pesqDhInativo.disabled= true;
		document.administracaoCsCdtbPesquisaPesqForm.pesqTxObservacao.disabled= true;
		document.administracaoCsCdtbPesquisaPesqForm.pesqNrPrevisaomax.disabled= true;
		document.administracaoCsCdtbPesquisaPesqForm.pesqNrPrevisaomin.disabled= true;
		document.administracaoCsCdtbPesquisaPesqForm.pesqDhInicial.disabled= true;
		document.administracaoCsCdtbPesquisaPesqForm.pesqDhFinal.disabled= true;
		document.administracaoCsCdtbPesquisaPesqForm.idDocuCdInicio.disabled= true;
		document.administracaoCsCdtbPesquisaPesqForm.idDocuCdFinal.disabled= true;
		
		<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
       <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
           	document.administracaoCsCdtbPesquisaPesqForm.pesqInAtiva.disabled= true;
		<%} %>
		
		document.administracaoCsCdtbPesquisaPesqForm.pesqInReceptiva.disabled= true;
		document.administracaoCsCdtbPesquisaPesqForm.pesqInQuestionario.disabled= true;
	}
	
	function inicio(){
		//Chamado 77481 - Vinicius - Pesquisa Multiempresa
		setaAssociacaoMultiEmpresa();
		
		setaChavePrimaria(administracaoCsCdtbPesquisaPesqForm.idPesqCdPesquisa.value);
	}

	function abrirCaracteristicasWEB(){
		pesq = document.forms[0].idPesqCdPesquisa.value;
		if(document.forms[0].idPesqCdPesquisa.value == ''){
			alert('<bean:message key="prompt.eNecessarioGravarPesquisa" />.');
			return;
		}
		showModalDialog('AbrirEdicaoSite.do?id_pesq_cd_pesquisa=' + pesq,window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:620px,dialogTop:0px,dialogLeft:200px');
		
	}
</script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos();inicio()">
<html:form styleId="administracaoCsCdtbPesquisaPesqForm" action="/AdministracaoCsCdtbPesquisaPesq.do">
	<input readonly type="hidden" name="remLen" size="3" maxlength="3" value="4000" > 
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<html:hidden property="idPesqCdPesquisaDuplicar" />
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
  <tr align="left"> 
    <td width="40%" align="right" class="principalLabel"> 
      <div align="right"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td> <!--Jonathan | Adequa��o para o IE 10-->
    <td width="13%"> <html:text property="idPesqCdPesquisa" style="width:150px" styleClass="text" disabled="true" /> 
    </td>
    <td width="66%">&nbsp;</td>
    <td width="3%">&nbsp;</td>
  </tr>
  <tr style="display:none"> <!-- REMOVER DISPLAY --> 
    <td width="40%" align="right" class="principalLabel"> 
      <div align="right"><bean:message key="prompt.documentoInicial"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="2"> <html:select property="idDocuCdInicio" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/> 
      </html:select> </td>
    <td width="3%">&nbsp;</td>
  </tr>
 <tr style="display:none"> <!-- REMOVER DISPLAY --> 
    <td width="40%"> 
      <div align="right"><bean:message key="prompt.documentoFinal"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="2"> <html:select property="idDocuCdFinal" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/> 
      </html:select> </td>
    <td width="3%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="40%"> 
      <div align="right"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td><!--Jonathan | Adequa��o para o IE 10-->
    <td colspan="2"><html:text property="pesqDsPesquisa" style="width:525px" styleClass="text" maxlength="60" /> 
    </td>
    <td width="3%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="40%"> 
      <div align="right"><bean:message key="prompt.observacao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td><!--Jonathan | Adequa��o para o IE 10-->
    <td colspan="2"><html:textarea property="pesqTxObservacao" styleClass="text" style="width:540px" rows="8" cols="50" onkeypress="textCounter(this,2000)" onkeyup="textCounter(this,2000)"/> 
    </td>
    <td width="3%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="40%"> 
      <div align="right"><bean:message key="prompt.previsaoMinima"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td><html:text property="pesqNrPrevisaomin" styleClass="text" maxlength="4" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/> 
    </td>
    <td >&nbsp;</td>
    <td width="3%" align="left">&nbsp;</td>
  </tr>
  <tr> 
    <td width="40%"> 
      <div align="right"><bean:message key="prompt.previsaoMaxima"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td><html:text property="pesqNrPrevisaomax" styleClass="text" maxlength="4" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" style="width: 90px"/> 
    </td>
    <td>&nbsp;</td>
    <td width="3%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="40%"> 
      <div align="right"><bean:message key="prompt.dataInicial"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td><!--Jonathan | Adequa��o para o IE 10-->
      <table width="96%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20%"><html:text property="pesqDhInicial" style="width:90px" styleClass="text" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" />  
          </td>
          <td width="60%" align="left" ><img src="webFiles/images/botoes/calendar.gif" name="imgCalendario" width="16" height="15" onclick="show_calendar('administracaoCsCdtbPesquisaPesqForm.pesqDhInicial')" class="principalLstParMao"  title="<bean:message key='prompt.calendario'/>" ></td>
        </tr>
      </table>
    </td>
    <td>&nbsp;</td>
    <td width="3%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="40%"> <!--Jonathan | Adequa��o para o IE 10-->
      <div align="right"><bean:message key="prompt.dataFinal"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td>
      <table width="96%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20%"><html:text property="pesqDhFinal" style="width:90px" styleClass="text" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" /> 
          </td>
          <td width="60%"><img src="webFiles/images/botoes/calendar.gif" name="imgCalendario" width="16" height="15" onclick="show_calendar('administracaoCsCdtbPesquisaPesqForm.pesqDhFinal')" class="principalLstParMao" title="<bean:message key='prompt.calendario'/>" ></td>
        </tr>
      </table>
    </td>
    <td>&nbsp;</td>
    <td width="3%"></td>
  </tr>
  <tr> 
    <td width="40%" align="right"><bean:message key="prompt.tipoPesquisa"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="2%">
            <html:checkbox value="true" property="pesqInReceptiva"/> 
          </td>
          <td class="principalLabel" width="10%">
            <div align="left">&nbsp;<bean:message key="prompt.receptiva"/></div>
          </td>
          <td class="principalLabel" width="2%">
          	<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
            	<html:checkbox value="true" property="pesqInAtiva"/>
			<%}else{ %>
            	&nbsp;            
            <%}%> 
          </td>
          <td class="principalLabel" width="8%">
          	<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
            	<div align="left">&nbsp;<bean:message key="prompt.ativa"/></div>
           <%}else{ %>
            	&nbsp;
            <%}%>
          </td>
          <td class="principalLabel" width="2%">
            <html:checkbox value="true" property="pesqInQuestionario"/> 
          </td>
          <td class="principalLabel" width="10%">
            <div align="left">&nbsp;<bean:message key="prompt.questionario"/></div>
          </td>
          <td class="principalLabel" width="2%">
            <html:checkbox value="true" property="pesqInQuestionarioWeb"/> 
          </td>
          <td class="principalLabel" width="25%">
            <div align="left">&nbsp;<bean:message key="prompt.disponivelWEB"/></div>
          </td>
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {%>          
          <td class="principalLabel" width="2%">
            <html:checkbox value="true" property="pesqInMR"/> 
          </td>
          <td class="principalLabel" width="74%">
            <div align="left">&nbsp;<bean:message key="prompt.mr"/></div>
          </td>
		<%}else{ %>           
          <td class="principalLabel" width="2%">
          </td>
          <td class="principalLabel" width="74%">
          </td>
		<%} %>           
        </tr>
      </table>
    </td>
    <td width="3%">&nbsp;</td>
  </tr>
  
  
  <tr> 
    <td width="40%"> 
      <div align="right"></div>
    </td>
    <td width="13%">  <!--Jonathan | Adequa��o para o IE 10-->
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="left" width="4%"> <html:checkbox value="true" property="pesqDhInativo"/> 
          </td>
          <td align="left" class="principalLabel" width="89%">&nbsp;<bean:message key="prompt.inativo"/> </td>
        </tr>
      </table>
    </td>
    <td class="principalLabel" width="66%">&nbsp; </td>
    <td width="3%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="30%"> 
      &nbsp;
    </td>
    <td width="13%" >
      &nbsp;
    </td>
    <td align="right" class="principalLabel">
      <img title="<bean:message key='prompt.pesquisaWEB'/>" src="webFiles/images/botoes/geraratend.gif"  style="cursor: pointer;" onclick="abrirCaracteristicasWEB();"><span style="cursor: pointer;" onclick="abrirCaracteristicasWEB();">&nbsp;WEB</span>
    </td>
    <td width="3%"></td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposPesquisa();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbPesquisaPesqForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISA_INCLUSAO_CHAVE%>')){
				desabilitaCamposPesquisa();
			}else{
				document.administracaoCsCdtbPesquisaPesqForm.idPesqCdPesquisa.disabled= false;
				document.administracaoCsCdtbPesquisaPesqForm.idPesqCdPesquisa.value= '';
				document.administracaoCsCdtbPesquisaPesqForm.idPesqCdPesquisa.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PESQUISA_PESQUISA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbPesquisaPesqForm.imgGravar);	
				desabilitaCamposPesquisa();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbPesquisaPesqForm.idDocuCdInicio.focus();}
	catch(e){}
</script>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>