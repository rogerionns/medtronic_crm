<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsAstbMateriaDetalheMadtForm.target = admIframe.name;
	document.administracaoCsAstbMateriaDetalheMadtForm.acao.value ='filtrar';
	document.administracaoCsAstbMateriaDetalheMadtForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsAstbMateriaDetalheMadtForm.filtro.value = '';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsAstbMateriaDetalheMadtForm.target = editIframe.name;
	editIframe.document.administracaoCsAstbMateriaDetalheMadtForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_MATERIADETALHE_MADT%>';
	editIframe.document.administracaoCsAstbMateriaDetalheMadtForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsAstbMateriaDetalheMadtForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo1){
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.idMateCdMateria.value = codigo1;
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_MATERIADETALHE_MADT%>';
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.target = editIframe.name;
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
	
}

function submeteSalvar(){
	if (window.document.all.item("Manifestacao").style.visibility == "hidden") {
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/>');
	} else {
		if (tab.document.administracaoCsAstbMateriaDetalheMadtForm.idMateCdMateria.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_uma_materia"/>");
			tab.document.administracaoCsAstbMateriaDetalheMadtForm.idMateCdMateria.focus();
			return false;
		}
		
		if (tab.cmbClassificacao.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_uma_classificacao"/>");
			tab.cmbClassificacao.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.focus();
			return false;
		}
		
		if (tab.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsDetalheClass.value == "") {
			alert("<bean:message key="prompt.Por_favor_preencha_o_campo_Descricao"/>");
			tab.cmbDescricao.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsDetalheClass.focus();
			return false;
		}
		
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value = tab.cmbClassificacao.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value;
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.acao.value = '<%= Constantes.ACAO_INCLUIR%>';
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_MATERIADETALHE_MADT%>';
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.target = admIframe.name;
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.submit();
		
		setTimeout("editIframe.lstArqCarga.location.reload()", 3000);
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.idTpclCdTpClassificacao.value = "";
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value = "";
		tab.cmbClassificacao.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value = "";
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.idDetcCdDetalheClass.value = "";
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsDetalheClass.value = "";
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsAutor.value = "";
	
		tab.carregaClassificacao();
	}
}

function submeteExcluir(idMateCdMateria, idDetcCdDetalheClass, idClasCdClassificacao, idTpclCdTpClassificacao) {
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.idMateCdMateria.value = idMateCdMateria;
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.idTpclCdTpClassificacao.value = idTpclCdTpClassificacao;
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value = idClasCdClassificacao;
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.idDetcCdDetalheClass.value = idDetcCdDetalheClass;
	tab.carregaClassificacao();
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_MATERIADETALHE_MADT%>';
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	submeteEsperaExcluir();
}

function submeteEsperaExcluir() {
	if (tab.cmbClassificacao.document.readyState != 'complete')
		a = setTimeout ("submeteEsperaExcluir()",100);
	else {
		try {
			clearTimeout(a);
		} catch(e) {}
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.idMateCdMateria.disabled= true;
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.idTpclCdTpClassificacao.disabled= true;
		tab.cmbClassificacao.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.disabled= true;
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsDetalheClass.disabled= true;
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsAutor.disabled= true;
		confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
		setConfirm(confirmacao);
	}
}

function setConfirm(confirmacao){
	if (confirmacao == true){
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.idMateCdMateria.disabled= false;
		tab.document.administracaoCsAstbMateriaDetalheMadtForm.idTpclCdTpClassificacao.disabled= false;
		editIframe.document.administracaoCsAstbMateriaDetalheMadtForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_MATERIADETALHE_MADT%>';
		editIframe.document.administracaoCsAstbMateriaDetalheMadtForm.target = admIframe.name;
		editIframe.document.administracaoCsAstbMateriaDetalheMadtForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		editIframe.document.administracaoCsAstbMateriaDetalheMadtForm.submit();
		setTimeout("editIframe.lstArqCarga.location.reload()", 3000);
	}
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.idTpclCdTpClassificacao.value = "";
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value = "";
	tab.cmbClassificacao.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value = "";
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.idDetcCdDetalheClass.value = "";
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsDetalheClass.value = "";
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsAutor.value = "";
	tab.carregaClassificacao();
	
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.idMateCdMateria.disabled= false;
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.idTpclCdTpClassificacao.disabled= false;
	tab.cmbClassificacao.document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.disabled= false;
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsDetalheClass.disabled = false;
	tab.document.administracaoCsAstbMateriaDetalheMadtForm.detcDsAutor.disabled = false;
}

function cancel(){

	editIframe.location = 'AdministracaoCsAstbMateriaDetalheMadt.do?tela=editCsAstbMateriaDetalheMadt&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsAstbMateriaDetalheMadtForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsAstbMateriaDetalheMadtForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_MATERIADETALHE_MADT%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			break;
		case editIframe:
			tab.document.administracaoCsAstbMateriaDetalheMadtForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_MATERIADETALHE_MADT%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsAstbMateriaDetalheMadtForm"	action="/AdministracaoCsAstbMateriaDetalheMadt.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="100%" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="100%"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td class="principalPstQuadroLinkVazio"> 
                  <table border="0" cellspacing="0" cellpadding="0" >
                    <tr>
									
                      <td class="principalPstQuadroLinkSelecionado"	id="tdDestinatario" name="tdDestinatario" onClick="AtivarPasta(admIframe);"><bean:message key="prompt.procurar" /></td>
									
                      <td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao" name="tdManifestacao" onClick="AtivarPasta(editIframe);"><bean:message key="prompt.materiaDetalhe" /></td>
								</tr>
							</table>
							
                </td>
							<td width="4">
							<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="position: absolute; width: 97%; height: 225px; z-index: 6; visibility: hidden"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsAstbMateriaDetalheMadt.do?tela=editCsAstbMateriaDetalheMadt&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="position: absolute; width: 99%; height: 199px; z-index: 2; visibility: visible"> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0"
								align="center">
                      <tr> 
                        <td class="principalLabel" width="9%"><bean:message key="prompt.descricao" /></td>
                        <td class="principalLabel" width="45%">&nbsp;</td>
                        <td class="principalLabel" width="30%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="4"> 
                          <table border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="25%"> <html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
                              </td>
                              <td width="05%"> &nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" onclick="filtrar()"> 
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" width="9%">&nbsp;</td>
                        <td class="principalLabel" width="45%">&nbsp;</td>
                        <td class="principalLabel" width="30%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLstCab" width="9%">&nbsp;&nbsp;<bean:message key="prompt.codigo" />
                        </td>
                        <td class="principalLstCab" width="45%"><bean:message key="prompt.materia" /></td>
                        <td class="principalLstCab" width="30%">&nbsp;</td>
                        <td class="principalLstCab" align="left" width="16%">&nbsp; 
                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="4" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsAstbMateriaDetalheMadt.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
                      </tr>
                    </table>
							</div>

							
                </td>
							<td width="4" height="400"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="100%"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="100%"></td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;

	desabilitaListaEmpresas();
	desabilitaTelaIdioma();
	   
</script>

</body>
</html>