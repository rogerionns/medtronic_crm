<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/tree_notasrapidas.js"></script>
<script>

function inicio() {
	//Adicionao registro principal
	addRegistro('0','<bean:message key="prompt.DesenhoDoProcesso" />','');
	if(window.dialogArguments != undefined && window.dialogArguments.fluxoWorkflowForm != undefined){
		document.getElementById("divPrazo").style.display = "block";
		document.getElementById("tdPrazoNormal").innerHTML = window.dialogArguments.fluxoWorkflowForm.etpr_nr_prazonormal_cabecalho.value;
		document.getElementById("tdPrazoEspecial").innerHTML = window.dialogArguments.fluxoWorkflowForm.etpr_nr_prazoespecial_cabecalho.value;
		document.getElementById("tdLblPrazoNormal").innerHTML = window.dialogArguments.document.getElementById("tdLblPrazoNormal").innerHTML;
		document.getElementById("tdLblPrazoEspecial").innerHTML = window.dialogArguments.document.getElementById("tdLblPrazoEspecial").innerHTML;
		document.getElementById("lstRegistro").innerHTML = window.dialogArguments.ifrmTreeView.document.getElementById("lstRegistro").innerHTML;
		
		var objetos = document.getElementsByTagName("div");
		for(i = 0; i < objetos.length; i++) {
			document.getElementById(objetos[i].id).style.backgroundColor = "";
		}
		
		window.print();
	}
}

function adicionaFilho(idPai, idFilho, descFilho, textoFuncao, etapaOk, idEtapaProcesso) {
	//Adicionando a etapa na treeview
	addRegistroFilho(idPai, idFilho, descFilho, cont, textoFuncao, etapaOk, idEtapaProcesso);
}

var estaExpandido = false;
function expandirTodos() {
	if(!estaExpandido) {
		expandeFecha();
		estaExpandido = true;
	}
}

function fecharTodos() {
	if(estaExpandido) {
		expandeFecha();
		estaExpandido = false;
	}
}

function expandeFecha() {	
	if(window.dialogArguments == undefined ||  window.dialogArguments.fluxoWorkflowForm == undefined){
		var iterate = new Number(0);	
		
		expandir(0, 0, false, 1);
		changeImage('imgExpand0', iterate);
		iterate++;
		
		for(i = 0; i < parent.arrayTreeView.length; i++) {
			expandir(parent.arrayTreeView[i].idFilho, parent.arrayTreeView[i].idFilho, false, 1);
			changeImage('imgExpand' + parent.arrayTreeView[i].idFilho, iterate);
			iterate++;
		}
	}
}

</script>
<body class="principalBgrPageIFRM" onload="inicio()">
	<div id="divPrazo" class="principalLabelValorFixo" style="position: absolute; display: none; height: 70px; width: 200px; border: #000000 0px solid; top: 20; left: 600;" align="center">
		<bean:message key="prompt.PrazoTotalDoFluxo" /><br/><br/>
		<table cellpadding="0" cellspacing="0" border="0" width="150">
			<tr>
				<td class="principalLabel" align="right" width="80">
					<bean:message key="prompt.Normal" />
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				</td>
				<td class="principalLabel" id="tdPrazoNormal" align="center">0</td>
				<td class="principalLabel" id="tdLblPrazoNormal">DIAS</td>
			<tr>
				<td class="principalLabel" align="right" width="80">
					<bean:message key="prompt.Especial" />
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				</td>
				<td class="principalLabel" id="tdPrazoEspecial" align="center">0</td>
				<td class="principalLabel" id="tdLblPrazoEspecial">DIAS</td>
			</tr>
		</table>
	</div>
	
	<%if ((request.getParameter("impressao")!=null) && (request.getParameter("impressao").equals("S"))) { %>
		<div id="lstRegistro" style="position: absolute; width: 100%; height: 100%; z-index: 2; overflow: auto"></div>
	<% } else { %>
		<div id="lstRegistro" style="position: absolute; width: 100%; height: 220px; z-index: 2; overflow: auto"></div>
	<% } %>
</body>
</html>