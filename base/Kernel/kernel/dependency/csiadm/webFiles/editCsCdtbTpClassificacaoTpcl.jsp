<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
 	function desabilitaCampos(){
 		administracaoCsCdtbTpClassificacaoTpclForm.idTpclCdTpClassificacao.disabled= true;
		administracaoCsCdtbTpClassificacaoTpclForm.tpclDsTpClassificacao.disabled= true;	
		administracaoCsCdtbTpClassificacaoTpclForm.tpclNrOrdem.disabled= true;
		administracaoCsCdtbTpClassificacaoTpclForm.tpclDhInativo.disabled= true;
	}	
	
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbTpClassificacaoTpclForm.idTpclCdTpClassificacao.value);
	}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbTpClassificacaoTpclForm" action="/AdministracaoCsCdtbTpClassificacaoTpcl.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>		

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idTpclCdTpClassificacao" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="tpclDsTpClassificacao" styleClass="text" maxlength="60" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.sequencia"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="tpclNrOrdem" styleClass="text" maxlength="7" onkeypress="numberValidate( this, 0, '', '', event );" onblur="numberValidate( this, 0, '', '', event ); return false;"/>
            <script>document.all('tpclNrOrdem').value = (document.all('tpclNrOrdem').value == '0'?'':document.all('tpclNrOrdem').value);</script>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="tpclDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			administracaoCsCdtbTpClassificacaoTpclForm.tpclDsTpClassificacao.disabled= true;	
			administracaoCsCdtbTpClassificacaoTpclForm.tpclNrOrdem.disabled= true;
			administracaoCsCdtbTpClassificacaoTpclForm.tpclDhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_TIPODECLASSIFICACAO_INCLUSAO_CHAVE%>', parent.administracaoCsCdtbTpClassificacaoTpclForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_TIPODECLASSIFICACAO_INCLUSAO_CHAVE%>')){
				desabilitaCampos();
			}
			administracaoCsCdtbTpClassificacaoTpclForm.idTpclCdTpClassificacao.disabled= false;
			administracaoCsCdtbTpClassificacaoTpclForm.idTpclCdTpClassificacao.value= '';
			administracaoCsCdtbTpClassificacaoTpclForm.tpclNrOrdem.disabled= false;
			administracaoCsCdtbTpClassificacaoTpclForm.idTpclCdTpClassificacao.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_TIPODECLASSIFICACAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_TIPODECLASSIFICACAO_ALTERACAO_CHAVE%>', parent.administracaoCsCdtbTpClassificacaoTpclForm.imgGravar);	
				desabilitaCampos();
			}
		</script>
</logic:equal>
</html>
<script>
	try{administracaoCsCdtbTpClassificacaoTpclForm.tpclDsTpClassificacao.focus();}
	catch(e){}	
</script>