<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>
function Toggle(item) {
	if (administracaoPermissionamentoForm["csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value == 0 && administracaoPermissionamentoForm["csAstbPermissionamentoPetoVo.csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].value == 0) {
		alert('<bean:message key="prompt.selecione_um_funcionario_ou_um_grupo_de_funcionarios"/>');
	}
	else {
		obj = document.getElementById(item);
		visible = (obj.style.display != "none");
		key = document.getElementById("x" + item);
		if (visible) {
			obj.style.display = "none";
			key.innerHTML = "<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>";
		} else {
			obj.style.display = "block";
			key.innerHTML = "<img src='webFiles/images/botoes/textfolder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>";
		}
	}
}

function marcaPai(obj) {
	arraySplit = obj.name.split('_');
	nameObjPai = "";
	for (i = 0; i < arraySplit.length - 1; i++) {
		if (i == arraySplit.length - 2)
			nameObjPai += arraySplit[i];
		else 
			nameObjPai = nameObjPai + arraySplit[i] + "_";
	}
	objPai = document.getElementsByName(nameObjPai).item(0);


	//Verifica��o para saber se � necess�rio check do pai, sem a verifica��o ocorre um erro de primary duplicada.
    var qtdFilhos = new Number(0);
	var collObjects = document.getElementsByTagName("input");
	var tamNome = objPai.getAttribute("key").length;
	for (var i = 0; i < collObjects.length; i++) {
		if (collObjects[i].type != "checkbox")
			continue;
		if (collObjects[i].getAttribute("key").length > tamNome) {
			prefixo = collObjects[i].getAttribute("key").substring(0, tamNome);
			if (prefixo == objPai.getAttribute("key")) {
					qtdFilhos++;
			}
		}	
	}

	if(qtdFilhos > 1){
		objPai.checked = true;
	}
	else
	{
		objPai.checked = obj.checked;
	}
}	

function marcarFilhos(obj) {
	if (document.administracaoPermissionamentoForm["csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value == 0 && document.administracaoPermissionamentoForm["csAstbPermissionamentoPetoVo.csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].value == 0) {
		alert('<bean:message key="prompt.selecione_um_funcionario_ou_um_grupo_de_funcionarios"/>');
		obj.checked = false;
		return false;
	}
	
	collObjects = document.getElementsByTagName("input");
	var tamNome = obj.getAttribute("key").length;
	for (var i = 0; i < collObjects.length; i++) {
		if (collObjects[i].type != "checkbox")
			continue;
		if (collObjects[i].getAttribute("key").length > tamNome) {
			prefixo = collObjects[i].getAttribute("key").substring(0, tamNome);
			if (prefixo == obj.getAttribute("key")) {
					collObjects[i].checked = obj.checked;
			}
		}	
	}
}

function Expand() {
   if (document.administracaoPermissionamentoForm["csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value == 0 && document.administracaoPermissionamentoForm["csAstbPermissionamentoPetoVo.csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].value == 0) {
		alert('<bean:message key="prompt.selecione_um_funcionario_ou_um_grupo_de_funcionarios"/>');
   }
   else {
	   divs = document.getElementsByTagName("DIV");
	   for (i = 0; i < divs.length; i++) {
 		 if (divs[i].id == 'dummy')
		 	continue;
    	 divs[i].style.display = "block";
	     key = document.getElementById("x" + divs[i].id);
	     if (key != null){
    	 	key.innerHTML = "<img src='webFiles/images/botoes/textfolder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>";
    	 }
	   }
   }
}

function Collapse() {
   if (document.administracaoPermissionamentoForm["csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value == 0 && document.administracaoPermissionamentoForm["csAstbPermissionamentoPetoVo.csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].value == 0) {
		alert('<bean:message key="prompt.selecione_um_funcionario_ou_um_grupo_de_funcionarios"/>');
   }
   else {
      divs = document.getElementsByTagName("DIV");
   	  for (i = 0; i < divs.length; i++) {
		 if (divs[i].id == 'dummy')
		 	continue;
	     divs[i].style.display = "none";
    	 key = document.getElementById("x" + divs[i].id);
    	 if (key != null){
    	 	key.innerHTML = "<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>";
    	 }
	     
	  }
   }
}

function submeteSalvar() {
	if (document.administracaoPermissionamentoForm["csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value == 0 && document.administracaoPermissionamentoForm["csAstbPermissionamentoPetoVo.csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].value == 0) {
		alert('<bean:message key="prompt.selecione_um_funcionario_ou_um_grupo_de_funcionarios"/>');
		return false;
	}
	if (!confirm('<bean:message key="prompt.deseja_gravar_as_alteracoes"/>')) 
		return false;
	collObjects = document.getElementsByTagName("input");
	var strTxt = "";
	for (var i = 0; i < collObjects.length; i++) {
		if (collObjects[i].type != "checkbox")
			continue;
		if (collObjects[i].checked) {
			valores = collObjects[i].name.split('_');
			if (valores.length > 3) { // permissao e nivel de permissao
				strTxt += "<input type='hidden' name='idFuciCdFuncionalidade' value='" + valores[2] + "'>";
				strTxt += "<input type='hidden' name='idPermCdPermissao' value='" + valores[3] + "'>";

				if (valores.length == 4)
					strTxt += "<input type='hidden' name='idNipeCdNivelpermissao' value='1'>";
				else 
					strTxt += "<input type='hidden' name='idNipeCdNivelpermissao' value='" + valores[4] + "'>";
			}
		}
	}
	dummy.innerHTML = strTxt;
	document.administracaoPermissionamentoForm.acao.value = '<%= Constantes.ACAO_INCLUIR %>';
	document.administracaoPermissionamentoForm.target = submissao.name;
	document.administracaoPermissionamentoForm.submit();
}

function marcarSelecionados(item) {
	var temp = item.split("_");
	if (temp[4] == 1){
		//Tratamento para nao dar erro se tentar setar algum permissionamento que nao esta na lista
		if(document.getElementsByName(temp[0] + "_" + temp[1] + "_" + temp[2] + "_" + temp[3]).item(0) != undefined){
			document.getElementsByName(temp[0] + "_" + temp[1] + "_" + temp[2] + "_" + temp[3]).item(0).checked = true;
		}
	}else{
		if(document.getElementsByName(item).item(0) != undefined){
			document.getElementsByName(item).item(0).checked = true;
		}
	}
}

function escondeAguarde(){
	parent.parent.parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');escondeAguarde();">

<html:form action="/AdministracaoPermissionamento.do" styleId="administracaoPermissionamentoForm">
	
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario"/>
	<html:hidden property="csAstbPermissionamentoPetoVo.csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"/>
	
	<table border=0 cellpadding='10' cellspacing=0>
	<tr>
		<td>
			<logic:present name="treeViewVector">
				<logic:iterate name="treeViewVector" id="modulos" indexId="numeroModulo">
					<table border=0 cellpadding='1' cellspacing=1>
					<tr>
						<td width='16'>
							<a id="x<bean:write name="modulos" property="idModuCdModulo"/>" href="javascript:Toggle('<bean:write name="modulos" property="idModuCdModulo"/>');">
								<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
							</a>
						</td>
						<td class="principalLabel">
							<input type="checkbox" key="<%= numeroModulo+"_"%>" name="check<bean:write name="modulos" property="idModuCdModulo"/>" onclick="marcarFilhos(this);">	
						</td>
						<td class="principalLabel">
							<bean:write name="modulos" property="moduDsModulo"/>
						</td>
					</tr>
					</table>
					<div id="<bean:write name="modulos" property="idModuCdModulo"/>" style="display: none; margin-left: 2em;">	
						<logic:iterate name="modulos" property="grupoFuncio" id="grupoFuncio" indexId="numeroGrupo">
							<table border=0 cellpadding='1' cellspacing=1>
							<tr>
								<td width='16'>
									<a id="x<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>" href="javascript:Toggle('<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>');">
										<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
									</a>
								</td>
								<td class="principalLabel">
									<input type="checkbox" key="<%= numeroModulo+ "_" + numeroGrupo + "_"%>" name="check<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>" onclick="marcarFilhos(this);">	
								</td>
								<td class="principalLabel">
									<bean:write name="grupoFuncio" property="grfuDsGrupofuncio"/>
								</td>
							</tr>	
							</table>
							<div id="<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>" style="display: none; margin-left: 2em;">	
								<logic:iterate name="grupoFuncio" property="funcionalidades" id="funcionalidades" indexId="numeroFunc">
									<table border=0 cellpadding='1' cellspacing=1>
									<tr>
										<td width='16'>
											<a id="x<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>_<bean:write name="funcionalidades" property="idFuciCdFuncionalidade"/>" href="javascript:Toggle('<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>_<bean:write name="funcionalidades" property="idFuciCdFuncionalidade"/>');">
												<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
											</a>
										</td>
										<td class="principalLabel">
											<input type="checkbox" key="<%= numeroModulo+ "_" + numeroGrupo + "_" + numeroFunc + "_"%>" name="check<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>_<bean:write name="funcionalidades" property="idFuciCdFuncionalidade"/>" onclick="marcarFilhos(this);">	
										</td>
										<td class="principalLabel">
											<logic:equal name="funcionalidades" property="fuciDsFuncionalidade" value="Linha">
												<%= getMessage("prompt.linha", request)%>
											</logic:equal>
											<logic:equal name="funcionalidades" property="fuciDsFuncionalidade" value="Produto Assunto">
												<%= getMessage("prompt.assuntoNivel1", request)%>
											</logic:equal>
											<logic:notEqual name="funcionalidades" property="fuciDsFuncionalidade" value="Linha">
												<logic:notEqual name="funcionalidades" property="fuciDsFuncionalidade" value="Produto Assunto">
													<bean:write name="funcionalidades" property="fuciDsFuncionalidade"/>
												</logic:notEqual>
											</logic:notEqual>
										</td>
									</tr>	
									</table>
									<div id="<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>_<bean:write name="funcionalidades" property="idFuciCdFuncionalidade"/>" style="display: none; margin-left: 2em;">
											<logic:iterate name="funcionalidades" property="permissoes" id="permissoes" indexId="numeroPerm">
											
											<!-- se existir um vector de niveis de permissao -->
											<logic:present name="permissoes" property="nivelPerm">
												<table border=0 cellpadding='1' cellspacing=1>
												<tr>
													<td width='16'>
														<a id="x<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>_<bean:write name="funcionalidades" property="idFuciCdFuncionalidade"/>_<bean:write name="permissoes" property="csAstbPermissaonivelPeniVo.idPermCdPermissao"/>" href="javascript:Toggle('<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>_<bean:write name="funcionalidades" property="idFuciCdFuncionalidade"/>_<bean:write name="permissoes" property="csAstbPermissaonivelPeniVo.idPermCdPermissao"/>');">
															<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
														</a>
													</td>
													<td class="principalLabel">
														<input type="checkbox" key="<%= numeroModulo+ "_" + numeroGrupo + "_" + numeroFunc + "_" + numeroPerm + "_"%>" name="check<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>_<bean:write name="funcionalidades" property="idFuciCdFuncionalidade"/>_<bean:write name="permissoes" property="csAstbPermissaonivelPeniVo.idPermCdPermissao"/>" onclick="marcarFilhos(this);">&nbsp;	
													</td>
													<td class="principalLabel">
														<bean:write name="permissoes" property="permDsPermissao"/>
													</td>
												</tr>	
												</table>
												
												<div id="<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>_<bean:write name="funcionalidades" property="idFuciCdFuncionalidade"/>_<bean:write name="permissoes" property="csAstbPermissaonivelPeniVo.idPermCdPermissao"/>" style="display: none; margin-left: 2em;">
													<logic:iterate name="permissoes" property="nivelPerm" id="nivelPerm" indexId="numeroNivel">
														<logic:notEqual name="nivelPerm" property="csAstbPermissaonivelPeniVo.idNipeCdNivelpermissao" value="<%= MAConstantes.ID_NIVEL_PERMISSAO_UNICO %>">
															<table border=0 cellpadding='1' cellspacing=1>	
															<tr>
																<td class="principalLabel">
																	<img src='webFiles/images/botoes/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
																</td>
																<td class="principalLabel">
																	<input type="checkbox" key="<%= numeroModulo+ "_" + numeroGrupo + "_" + numeroFunc + "_" + numeroPerm + "_" + numeroNivel + "_"%>" name="check<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>_<bean:write name="funcionalidades" property="idFuciCdFuncionalidade"/>_<bean:write name="permissoes" property="csAstbPermissaonivelPeniVo.idPermCdPermissao"/>_<bean:write name="nivelPerm" property="csAstbPermissaonivelPeniVo.idNipeCdNivelpermissao"/>" onclick="javascript: marcaPai(this);">
																</td>
																<td class="principalLabel">
																	<bean:write name="nivelPerm" property="nipeDsNivelpermissao"/>
																</td>
															</tr>
															</table>														
														</logic:notEqual> 
													</logic:iterate>
												</div>
											</logic:present>
											
											<logic:notPresent name="permissoes" property="nivelPerm">
												<table border=0 cellpadding='1' cellspacing=1>
												<tr>
													<td class="principalLabel">
														<img src='webFiles/images/botoes/text.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
													</td>
													<td class="principalLabel">
														<input type="checkbox" key="<%= numeroModulo+ "_" + numeroGrupo + "_" + numeroFunc + "_" + numeroPerm + "_"%>" name="check<bean:write name="modulos" property="idModuCdModulo"/>_<bean:write name="grupoFuncio" property="idGrfuCdGrupofuncio"/>_<bean:write name="funcionalidades" property="idFuciCdFuncionalidade"/>_<bean:write name="permissoes" property="csAstbPermissaonivelPeniVo.idPermCdPermissao"/>">
													</td>
													<td class="principalLabel">
														<bean:write name="permissoes" property="permDsPermissao"/>
													</td>
												</tr>
												</table>
											</logic:notPresent>
										</logic:iterate>
									</div>
								</logic:iterate>
							</div>
						</logic:iterate>
					</div>
				</logic:iterate>
			</logic:present>
		</td>
	</tr>
	</table>
<div id="dummy"></div>

<logic:present name="permissoesVector">
	<logic:iterate name="permissoesVector" id="permissoesVector">
		<script>
			marcarSelecionados('check<bean:write name="permissoesVector" property="csAstbFunciopeniFupnVo.csCdtbFuncionalidadeFuciVo.csDmtbGrupofuncioGrfuVo.csDmtbModuloModuVo.idModuCdModulo"/>_<bean:write name="permissoesVector" property="csAstbFunciopeniFupnVo.csCdtbFuncionalidadeFuciVo.csDmtbGrupofuncioGrfuVo.idGrfuCdGrupofuncio"/>_<bean:write name="permissoesVector" property="csAstbFunciopeniFupnVo.csCdtbFuncionalidadeFuciVo.idFuciCdFuncionalidade"/>_<bean:write name="permissoesVector" property="csAstbFunciopeniFupnVo.csDmtbPermissaoPermVo.csAstbPermissaonivelPeniVo.idPermCdPermissao"/>_<bean:write name="permissoesVector" property="csAstbFunciopeniFupnVo.csDmtbNivelpermissaoNipeVo.csAstbPermissaonivelPeniVo.idNipeCdNivelpermissao"/>');			
		</script>		
	</logic:iterate>
</logic:present>

<logic:present name="submetido">
	<script>alert('<bean:message key="prompt.dados_gravados_com_sucesso"/>');</script>
</logic:present>

<iframe name="submissao" id="submissao" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	
</html:form>
</body>
</html>