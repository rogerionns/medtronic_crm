<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/includes/data.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'PROCURAR':
	MM_showHideLayers('procurar','','show','conteudo','','hide')
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdConteudo','principalPstQuadroLinkNormalGrande');
	break;

case 'CONTEUDO':
	MM_showHideLayers('procurar','','hide','conteudo','','show')
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdConteudo','principalPstQuadroLinkSelecionadoGrande');
	break;
	


}
 eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}

//Est� funcao tira os espacos em branco da string
function trim(str){
	return str.replace(/^\s+|\s+$/g,"");
}

function gravar(){
	if(ifrmEdit.document.forms[0].laouInDelimitado.length != undefined ){
		if(ifrmEdit.document.forms[0].laouInDelimitado[ifrmEdit.document.forms[0].id_laou_cd_sequencial.selectedIndex].value == 'S'){
			if(trim(ifrmEdit.document.forms[0].agca_ds_separador.value) == ''){
				alert('<bean:message key="prompt.separadorCampos"/> <bean:message key="prompt.obrigatorio"/>');
				return;
			}
		}	
	}

	if(ifrmEdit.document.forms[0].agca_in_tipoagenda[0].checked == false && ifrmEdit.document.forms[0].agca_in_tipoagenda[1].checked == false){
		alert('<bean:message key="prompt.agendamentocampanha.tipoimportacao"/> <bean:message key="prompt.obrigatorio"/>');
		return;
	}
	
	
	if(trim(ifrmEdit.document.forms[0].agca_ds_agendacampanha.value)==''){
		alert('<bean:message key="prompt.descricao"/> <bean:message key="prompt.obrigatorio"/>');
		return;
	}

	if(trim(ifrmEdit.document.forms[0].agca_nr_sequencia.value)==''){
		alert('<bean:message key="prompt.sequencia"/> <bean:message key="prompt.obrigatorio"/>');
		return;
	}

	if(trim(ifrmEdit.document.forms[0].id_camp_cd_campanha.value)==''){
		alert('<bean:message key="prompt.campanha"/> <bean:message key="prompt.obrigatorio"/>');
		return;
	}

	if(trim(ifrmEdit.document.forms[0].id_orig_cd_origem.value)==''){
		alert('<bean:message key="prompt.origem"/> <bean:message key="prompt.obrigatorio"/>');
		return;
	}
	
	//ifrmEdit.document.forms[0].agca_ds_diretorioerro.value = ifrmEdit.document.forms[0].agca_ds_diretorioerro.value.replace(" ","");
	if(trim(ifrmEdit.document.forms[0].agca_ds_diretorioerro.value)==''){
		alert('<bean:message key="prompt.diretorioErro"/> <bean:message key="prompt.obrigatorio"/>');
		return;
	}

	if(trim(ifrmEdit.document.forms[0].agca_ds_emailnotificacao.value)==''){
		alert('<bean:message key="prompt.emailNotificacao"/> <bean:message key="prompt.obrigatorio"/>');
		return;
	}

	if(ifrmEdit.document.forms[0].agca_in_tipoimportacao[0].checked){
		if(trim(ifrmEdit.document.forms[0].id_laou_cd_sequencial.value)==''){
			alert('<bean:message key="prompt.layout"/> <bean:message key="prompt.obrigatorio"/>');
			return;
		}

		if(trim(ifrmEdit.document.forms[0].id_mean_cd_metodoanalise.value)==''){
			alert('<bean:message key="prompt.metodoAnalise"/> <bean:message key="prompt.obrigatorio"/>');
			return;
		}

		if(trim(ifrmEdit.document.forms[0].agca_ds_diretorioarquivo.value)==''){
			alert('<bean:message key="prompt.diretorioArquivo"/> <bean:message key="prompt.obrigatorio"/>');
			return;
		}

		if(trim(ifrmEdit.document.forms[0].agca_ds_diretoriobackup.value)==''){
			alert('<bean:message key="prompt.diretorioBackup"/> <bean:message key="prompt.obrigatorio"/>');
			return;
		}
	}else{
		if(trim(ifrmEdit.document.forms[0].id_visa_cd_visao.value)==''){
			alert('<bean:message key="prompt.visaoPlusinfo"/> <bean:message key="prompt.obrigatorio"/>');
			return;
		}

		if(trim(ifrmEdit.document.forms[0].agca_ds_analisarpor.value)==''){
			alert('<bean:message key="prompt.analisarPor"/> <bean:message key="prompt.obrigatorio"/>');
			return;
		}

		
	}

	if(trim(ifrmEdit.document.forms[0].agca_dh_execucaode.value)==''){
		alert('<bean:message key="prompt.execucaoAPartirDe"/> <bean:message key="prompt.obrigatorio"/>');
		return;
	}

	//Foi comentado por que a Homologa��o da Golden disse que n�o � para ser obrigatorio.
	//Bruno Ortega 04/11/2008
	//if(ifrmEdit.document.forms[0].agca_dh_execucaoate.value==''){
		//alert('<bean:message key="prompt.ate"/> <bean:message key="prompt.obrigatorio"/>');
		//return;
	//}

	if(!ifrmEdit.document.forms[0].agca_in_diario.checked
			&& !ifrmEdit.document.forms[0].agca_in_segunda.checked
			&& !ifrmEdit.document.forms[0].agca_in_terca.checked
			&& !ifrmEdit.document.forms[0].agca_in_quarta.checked
			&& !ifrmEdit.document.forms[0].agca_in_quinta.checked
			&& !ifrmEdit.document.forms[0].agca_in_sexta.checked
			&& !ifrmEdit.document.forms[0].agca_in_sabado.checked
			&& !ifrmEdit.document.forms[0].agca_in_domingo.checked){
		alert('<bean:message key="prompt.agendamentocampanha.diadasemana"/> <bean:message key="prompt.obrigatorio"/>');
		return;
	}
	
	if(date(ifrmEdit.document.forms[0].agca_dh_execucaode.value, ifrmEdit.document.forms[0].agca_dh_execucaoate.value)){
	 	alert('A Data At� deve ser maior ou igual a data <bean:message key="prompt.execucaoAPartirDe"/>!');
	 	return;
	 }
	
	ifrmEdit.document.forms[0].submit();
}

function editar(idAgca){
	try{
	ifrmEdit.document.forms[0].id_agca_cd_agendacampanha.value = idAgca;
	ifrmEdit.document.forms[0].action = 'AbrirEdicaoAgendaCampanha.do';
	ifrmEdit.document.forms[0].inativo.value;
	ifrmEdit.document.forms[0].submit();
	AtivarPasta('CONTEUDO');
	}catch(e){
		setTimeout('editar(' + idAgca + ');', 300);
	}
}

function remover(idAgca){
	if(!confirm("Deseja remover este Agendamento campanha?")){
		return false;
	}
	document.forms[0].id_agca_cd_agendacampanha.value = idAgca;
	document.forms[0].action = 'RemoverAgendaCampanha.do';
	document.forms[0].submit();
}

function novo(){
	ifrmEdit.document.forms[0].id_agca_cd_agendacampanha.value = '';
	ifrmEdit.document.forms[0].action = 'CancelarAgendaCampanha.do';
	ifrmEdit.document.forms[0].submit();
	AtivarPasta('CONTEUDO');
}

function cancelar(){
	document.forms[0].reset();
	document.forms[0].submit();
	AtivarPasta('PROCURAR');
}

</script>
</head>
	<body class="principalBgrPage" text="#000000">
		<html:form action="AbrirTelaAgendaCampanha">
<html:hidden property="id_agca_cd_agendacampanha"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLabel" height="30">&nbsp;</td>
        </tr>
      </table>
      <table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
        <tr> 
          <td height="254"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				   <tr>
					  <td class="principalPstQuadroLinkVazio">
						 <table border="0" cellspacing="0" cellpadding="0">
						    <tr>
							   <td class="principalPstQuadroLinkSelecionado" id="tdProcurar" name="tdProcurar" onClick="AtivarPasta('PROCURAR');">
									<bean:message key="prompt.procurar"/>
                               </td>
							   <td class="principalPstQuadroLinkNormalGrande" id="tdConteudo" name="tdConteudo"	onClick="AtivarPasta('CONTEUDO');">
									<bean:message key="prompt.agendamentoCampanha"/>
                               </td>
							</tr>
					     </table>
					  </td>
					  <td width="4"><img
						  src="/webFiles/images/separadores/pxTranp.gif" width="1"
					      height="1"></td>
				   </tr>
				</table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
              <tr> 
                <td valign="top" class="principalBgrQuadro" align="center"> 
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" height="450">
                    <tr> 
                      <td valign="top"> 
                        <div id="procurar" style="width:99%; height:450px;display: block"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
                            <tr> 
                              <td>&nbsp; </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="50%" class="principalLabel" height="13"><bean:message key="prompt.descricao"/></td>
                              <td class="principalLabel" height="13">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="50%" class="principalLabel"> 
								<html:text property="agca_ds_agendacampanha" styleClass="principalObjForm"></html:text>                                
                              </td>
                              <td class="principalLabel"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="Consulta" onclick="submit();"> 
                              </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td height="65" valign="top"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                	<thead>
                                		 <tr> 
                                    <td width="3%" class="principalLstCab">&nbsp;</td>
                                    <td width="8%" class="principalLstCab" align="left"><bean:message key="prompt.codigo"/></td>
                                    <td width="46%" class="principalLstCab">&nbsp;<bean:message key="prompt.descricao"/></td>                                    
                                    <td width="25%" class="principalLstCab" align="left"><bean:message key="prompt.inativo"/></td>
                                  </tr>
                                	</thead>
                                 <tbody style="width:100%;overflow-y: scroll">
                                 <logic:present name="agcaVector">
												<logic:iterate id="agenda" name="agcaVector">
													 <tr> 
			                                            <td class="principalLstParMao" width="3%" align="left"><img name="imgExcluir" src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" title="Excluir" onclick="remover('<bean:write name="agenda" property="field(id_agca_cd_agendacampanha)"/>')"></td>
			                                            <td class="principalLstParMao" width="10%" onClick="editar('<bean:write name="agenda" property="field(id_agca_cd_agendacampanha)"/>');">&nbsp;<bean:write name="agenda" property="field(id_agca_cd_agendacampanha)"/></td>
			                                            <td class="principalLstParMao" width="50%" onClick="editar('<bean:write name="agenda" property="field(id_agca_cd_agendacampanha)"/>');">&nbsp;<bean:write name="agenda" filter="HTML" property="acronymHTML(agca_ds_agendacampanha,65)"/></td>			                                            
			                                            <td class="principalLstParMao" width="25%" onClick="editar('<bean:write name="agenda" property="field(id_agca_cd_agendacampanha)"/>');">&nbsp;<bean:write name="agenda" property="field(agca_dh_inativo)" format="dd/MM/yyyy HH:mm:ss"/></td>
			                                          </tr>												
												</logic:iterate>
											</logic:present>
                                 </tbody>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <div id="conteudo" style="width:99%; height:450px;display: none"> 
                          <iframe id="ifrmEdit" name="ifrmEdit" src="AbrirEdicaoAgendaCampanha.do" width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4" height="230" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
              </tr>
              <tr> 
                <td width="100%" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%"	height="4"></td>
                <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="100%" height="4"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
        </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr align="center"> 
          <td width="20"><img name="imgNovo" src="webFiles/images/botoes/new.gif" width="14" height="16" class="geralCursoHand" onclick="novo();" title="Novo"> 
          </td>
          <td width="20"> <img name="imgGravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="gravar();" title="Gravar"></td>
          <td width="20"> <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="cancelar();" title="Cancelar"></td>
        </tr>
      </table>
    </td>
    <td width="4" height="1" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
  </tr>
  <tr> 
    <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
</html:form>
</body>
<script>
    setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_AGENDAMENTO_INCLUSAO_CHAVE%>', window.document.all.item("imgNovo"));
    setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_AGENDAMENTO_EXCLUSAO_CHAVE%>', window.document.all.item("imgExcluir"));
</script>
</html>