<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">   
<script language="JavaScript" src="../csicrm/webFiles/funcoes/date-picker.js"></script>
<script type="text/javascript" src="../csicrm/webFiles/javascripts/funcoesMozilla.js"></script>
<script type="text/javascript" src="../csicrm/webFiles/javascripts/TratarDados.js"></script>
<script language="JavaScript" src="../csicrm/webFiles/funcoes/pt/validadata.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>    
<script language="JavaScript">
<!--
function validaHora(obj){
		if (obj.value.length > 0){
			return verificaHora(obj);
		}
	}
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'PROCURAR':
	MM_showHideLayers('procurar','','show','conteudo','','hide','adicional','','hide')
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdConteudo','principalPstQuadroLinkNormal');
	SetClassFolder('tdAdicional','principalPstQuadroLinkNormal');
	break;

case 'CONTEUDO':
	MM_showHideLayers('procurar','','hide','conteudo','','show','adicional','','hide')
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdConteudo','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdAdicional','principalPstQuadroLinkNormal');
	break;
	
case 'ADICIONAL':
	MM_showHideLayers('procurar','','hide','conteudo','','hide','adicional','','show')
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdConteudo','principalPstQuadroLinkNormal');
	SetClassFolder('tdAdicional','principalPstQuadroLinkSelecionado');
	break;
}
 eval(stracao);
}

function AtivarPastaImp(pasta)
{
switch (pasta)
{
case 'ARQUIVO':
	MM_showHideLayers2('arquivo','','show','plusinfo','','hide')
	break;

case 'PLUSINFO':
	MM_showHideLayers2('arquivo','','hide','plusinfo','','show')
	break;
	
}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}


function MM_showHideLayers2() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers2.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}


function alterarTipoCampanha(){
	document.forms[0].action = 'AbrirEdicaoAgendaCampanha.do?acao=cmb'
	document.forms[0].submit();	
}

function inicio(){
	<logic:present name="gravacaoOK">
		parent.document.forms[0].submit();
	</logic:present>	
	<logic:equal name="agendaCampanhaForm" property="agca_in_tipoimportacao" value="A">
		AtivarPastaImp('ARQUIVO');
	</logic:equal>
	<logic:equal name="agendaCampanhaForm" property="agca_in_tipoimportacao" value="P">
		AtivarPastaImp('PLUSINFO');
	</logic:equal> 
	
	showError('<%=request.getAttribute("msgerro")%>');
}

function desabilitaCamposArea(){
	document.forms[0].id_agca_cd_agendacampanha.disabled = true;
	document.forms[0].agca_in_tipoagenda[0].disabled = true;
	document.forms[0].agca_in_tipoagenda[1].disabled = true;
	document.forms[0].agca_ds_agendacampanha.disabled = true;
	document.forms[0].agca_in_tipocampanha[0].disabled =true;
	document.forms[0].agca_in_tipocampanha[1].disabled = true;
	document.forms[0].agca_nr_sequencia.disabled = true;
	document.forms[0].id_camp_cd_campanha.disabled = true;
	document.forms[0].id_orig_cd_origem.disabled = true;
	document.forms[0].agca_ds_diretorioerro.disabled = true;
	document.forms[0].agca_ds_emailnotificacao.disabled = true;
	document.forms[0].agca_ds_emailnotificacao.disabled = true;
	document.forms[0].inativo.disabled = true;
	document.forms[0].agca_in_tipoimportacao[0].disabled = true;
	document.forms[0].agca_in_tipoimportacao[1].disabled = true;
	document.forms[0].id_laou_cd_sequencial.disabled = true;
	document.forms[0].id_mean_cd_metodoanalise.disabled = true;
	document.forms[0].agca_ds_diretorioarquivo.disabled = true;
	document.forms[0].agca_ds_diretoriobackup.disabled = true;
	document.forms[0].agca_ds_separador.disabled = true;
	document.forms[0].agca_in_cabecalho.disabled = true;
	document.forms[0].agca_in_duplicado.disabled = true;
	document.forms[0].id_visa_cd_visao.disabled = true;
	document.forms[0].agca_ds_analisarpor.disabled = true;
	document.forms[0].agca_dh_execucaode.disabled = true;
	document.forms[0].agca_dh_execucaoate.disabled = true;
	document.forms[0].agca_ds_horaexecucao.disabled = true;
	document.forms[0].agca_in_diario.disabled = true;
	document.forms[0].agca_in_segunda.disabled = true;
	document.forms[0].agca_in_terca.disabled = true;
	document.forms[0].agca_in_quarta.disabled = true;
	document.forms[0].agca_in_quinta.disabled = true;
	document.forms[0].agca_in_sexta.disabled = true;
	document.forms[0].agca_in_sabado.disabled = true;
	document.forms[0].agca_in_domingo.disabled = true;
	document.forms[0].agca_dh_atualizacao.disabled = true;
	document.forms[0].textfield32224.disabled = true;
	document.forms[0].agca_dh_cadastro.disabled = true;
	
}

</script>
</head>
<html:form action="SalvarAgendaCampanha">
<html:hidden property="agca_dh_inativo"/>
	<body class="principalBgrPageIFRM" text="#000000" onload="inicio()">

	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td>&nbsp; </td>
	     </tr>
	   </table>
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">C&oacute;digo 
	         <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="I">
	           <tr> 
	             <td width="22%"> 
	               <html:text property="id_agca_cd_agendacampanha" styleClass="principalObjForm" readonly="true"></html:text>
	             </td>
	             <td width="78%">
	               <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                 <tr> 
	                   <td class="principalLabel" align="center" width="5%"> 
	                     <html:radio property="agca_in_tipoagenda" value="I"></html:radio>
	                   </td>
	                   <td class="principalLabel" width="17%"><bean:message key="prompt.importacao"/></td>
	                   <td class="principalLabel" align="center" width="5%"> 
	                     <html:radio property="agca_in_tipoagenda" value="R"></html:radio>
	                   </td>
	                   <td class="principalLabel" width="73%"><bean:message key="prompt.remocao"/></td>
	                 </tr>
	               </table>
	             </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>	   
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.descricao"/> 
	         <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
					<html:text property="agca_ds_agendacampanha" styleClass="principalObjForm" maxlength="200" style="width: 435px;"></html:text>	               
	             </td>
	             <td width="26%">&nbsp; </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>	   
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.tipoCampanha"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
           </td>
           <td class="principalLabel" width="78%"> 
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr> 
                 <td width="21%"> 
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">
                     <tr> 
                       <td class="principalLabel" align="center" width="11%"> 
                         <html:radio property="agca_in_tipocampanha" value="M" onclick="alterarTipoCampanha()"></html:radio>
                       </td>
                       <td class="principalLabel" width="38%"><bean:message key="prompt.mailing"/></td>
                       <td class="principalLabel" align="center" width="12%"> 
                         <html:radio property="agca_in_tipocampanha" value="A" onclick="alterarTipoCampanha()"></html:radio>
                       </td>
                       <td class="principalLabel" width="39%"><bean:message key="prompt.telefone"/></td>
                     </tr>
                   </table>
                 </td>
                 <td width="79%"> 
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">
                     <tr> 
                       <td class="principalLabel" width="18%" align="right"><bean:message key="prompt.sequencia"/> 
                         <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                       </td>
                       <td class="principalLabel" width="82%"> 
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <tr> 
                             <td width="12%"> 
                               <html:text property="agca_nr_sequencia" styleClass="principalObjForm" maxlength="4" onfocus="SetarEvento(this, 'N')"></html:text>	 
                             </td>
                             <td width="88%">&nbsp; </td>
                           </tr>
                         </table>
                       </td>
                     </tr>
                   </table>
                 </td>
               </tr>
             </table>
           </td>
         </tr>
       </table>
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.campanha"/> 
	         <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
					<html:select property="id_camp_cd_campanha" styleClass="principalObjForm">					
						<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/></html:option>
						<logic:present name="campVector" >
							<html:options collection="campVector" property="idCampCdCampanha" labelProperty="campDsCampanha" />
						</logic:present>						
					</html:select>	
	             </td>
	             <td width="26%">&nbsp;</td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>	   
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.origem"/> 
	         <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
	               	<html:select property="id_orig_cd_origem" styleClass="principalObjForm">
						<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/></html:option>
						<logic:present name="origVector" >
							<html:options collection="origVector" property="idOrigCdOrigem" labelProperty="origDsOrigem" />
						</logic:present>
					</html:select>	
	             </td>
	             <td width="26%">&nbsp;</td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>	 
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.diretorioErro"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
	               <html:text property="agca_ds_diretorioerro" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
	             </td>
	             <td width="26%">&nbsp; </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>	   
	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
	     <tr> 
	       <td class="principalLabel" width="22%" align="right">
				<bean:message key="prompt.emailNotificacao"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	       </td>
	       <td class="principalLabel" width="78%"> 
	         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	           <tr> 
	             <td width="74%"> 
	               <html:text property="agca_ds_emailnotificacao" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
	             </td>
	             <td width="26%">
	               <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                 <tr> 
	                   <td class="principalLabel" align="right" width="50%"> 
	                     
	                     	<html:checkbox property="inativo" value="true"/>
							
	                   </td>
	                   <td class="principalLabel" width="50%"><bean:message key="prompt.inativo"/></td>
	                 </tr>
	               </table>
	             </td>
	           </tr>
	         </table>
	       </td>
	     </tr>
	   </table>		

		
		<br>
	   <table width="100%" border="0" cellspacing="0" cellpadding="0" height="170">
	     <tr>
	       <td valign="top">
				<!-- INICIO ABAS -->
					<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
					  <tr> 
					    <td class="principalPstQuadroLinkSelecionado" name="tdProcurar" id="tdProcurar" onclick="AtivarPasta('PROCURAR')"><bean:message key="prompt.importacao"/></td>
					    <td class="principalPstQuadroLinkNormal" name="tdConteudo" id="tdConteudo" onclick="AtivarPasta('CONTEUDO')"><bean:message key="prompt.periodicidade"/></td>
					    <td class="principalPstQuadroLinkNormal" name="tdAdicional" id="tdAdicional" onclick="AtivarPasta('ADICIONAL')"><bean:message key="prompt.resultado"/></td>
					    <td class="principalLabel">&nbsp;</td>
					  </tr>
					</table>
					<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro" height="200">
					  <tr>
					    <td valign="top">
					      <div id="procurar" style="width:100%;display: block"> 
					        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="200">
					          <tr>
					            <td valign="top">
									<!-- ABA IMPORTACAO -->
										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
										  <tr> 
										    <td>&nbsp;</td>
										  </tr>
										</table>									
									
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
										  <tr> 
										    <td width="22%" align="right" class="principalLabel"><bean:message key="prompt.tipoImportacao"/> 
										      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
										    <td width="78%"> 
										      <table width="100%" border="0" cellspacing="0" cellpadding="0">
										        <tr> 
										          <td class="principalLabel" align="center" width="5%">
													<html:radio property="agca_in_tipoimportacao" value="A" styleId="tdArquivo"  onclick="AtivarPastaImp('ARQUIVO')"></html:radio>										            
										          </td>
										          <td class="principalLabel" width="13%"><bean:message key="prompt.agendamentocampanha.arquivo"/></td>
										          <td class="principalLabel" align="center" width="4%">
										            <html:radio property="agca_in_tipoimportacao" value="P" styleId="tdPlusinfo"  onclick="AtivarPastaImp('PLUSINFO')"></html:radio>
										          </td>
										          <td class="principalLabel" width="78%"> <bean:message key="prompt.visaoPlusinfo"/></td>
										        </tr>
										      </table>
										    </td>
										  </tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
										  <tr> 
										    <td>&nbsp;</td>
										  </tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" height="160" >
										  <tr>
										    <td valign="top"> 
										      <table width="100%" border="0" cellspacing="0" cellpadding="0" id="arquivo">
												<tr>
													<td>														
													        <table width="100%" border="0" cellspacing="0" cellpadding="0">
													          <tr> 
													            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.layout"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
													            </td>
													            <td class="principalLabel" width="78%"> 
													              <table width="100%" border="0" cellspacing="0" cellpadding="0">
													                <tr> 
													                  <td width="74%"> 
													                    <html:select property="id_laou_cd_sequencial" styleClass="principalObjForm">
																			<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/></html:option>
																			<logic:present name="laouVector" >
																				<html:options collection="laouVector" property="idLaouCdSequencial" labelProperty="laouDsLayout" />
																			</logic:present>
																		</html:select>	
																		<input type="hidden" name="laouInDelimitado" value="N">
																		<logic:present name="laouVector" >
																			<logic:iterate name="laouVector" id="laou">
																				<input type="hidden" name="laouInDelimitado" value="<bean:write name="laou" property="laouInDelimitado"/>">
																			</logic:iterate>
																		</logic:present>																		
													                  </td>
													                  <td width="26%">&nbsp; </td>
													                </tr>
													              </table>
													            </td>
													          </tr>
													        </table>										        
													        <table width="100%" border="0" cellspacing="0" cellpadding="0">
													          <tr> 
													            <td class="principalLabel" width="22%" align="right">
																	<bean:message key="prompt.metodoAnalise"/>
																	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
													            </td>
													            <td class="principalLabel" width="78%"> 
													              <table width="100%" border="0" cellspacing="0" cellpadding="0">
													                <tr> 
													                  <td width="74%"> 
													                    <html:select property="id_mean_cd_metodoanalise" styleClass="principalObjForm">
																			<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/></html:option>
																			<logic:present name="meanVector" >
																				<html:options collection="meanVector" property="field(id_mean_cd_metodoanalise)" labelProperty="field(mean_ds_metodoanalise)" />
																			</logic:present>
																		</html:select>	
													                  </td>
													                  <td width="26%">&nbsp; </td>
													                </tr>
													              </table>
													            </td>
													          </tr>
													        </table>										       
													        <table width="100%" border="0" cellspacing="0" cellpadding="0">
													          <tr> 
													            <td class="principalLabel" width="22%" align="right">
																	<bean:message key="prompt.diretorioArquivo"/>
																	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
													            </td>
													            <td class="principalLabel" width="78%"> 
													              <table width="100%" border="0" cellspacing="0" cellpadding="0">
													                <tr> 
													                  <td width="74%"> 
													                    <html:text property="agca_ds_diretorioarquivo" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
													                  </td>
													                  <td width="26%">&nbsp; </td>
													                </tr>
													              </table>
													            </td>
													          </tr>
													        </table>										        
													        <table width="100%" border="0" cellspacing="0" cellpadding="0">
													          <tr> 
													            <td class="principalLabel" width="22%" align="right">
																	<bean:message key="prompt.diretorioBackup"/>
																	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
													            </td>
													            <td class="principalLabel" width="78%"> 
													              <table width="100%" border="0" cellspacing="0" cellpadding="0">
													                <tr> 
													                  <td width="74%"> 
													                    <html:text property="agca_ds_diretoriobackup" styleClass="principalObjForm" maxlength="255" style="width: 435px;"></html:text>
													                  </td>
													                  <td width="26%">&nbsp; </td>
													                </tr>
													              </table>
													            </td>
													          </tr>
													        </table>										        
													        <table width="100%" border="0" cellspacing="0" cellpadding="0">
													          <tr> 
													            <td class="principalLabel" width="22%" align="right">
																	<bean:message key="prompt.separadorCampos"/>
																	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
													            </td>
													            <td class="principalLabel" width="78%"> 
													              <table width="100%" border="0" cellspacing="0" cellpadding="0">
													                <tr> 
													                  <td width="8%" class="principalLabel"> 
													                    <html:text property="agca_ds_separador" styleClass="principalObjForm" maxlength="1"></html:text>
													                  </td>
													                  <td width="92%" class="principalLabel"> 
													                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
													                      <tr> 
													                        <td class="principalLabel" align="center" width="5%"> 
													                          <html:checkbox property="agca_in_cabecalho" value="S"></html:checkbox>
													                        </td>
													                        <td class="principalLabel" width="29%"><bean:message key="prompt.arquivoComCabecalho"/></td>
													                        <td class="principalLabel" align="center" width="3%"> 
													                          <html:checkbox property="agca_in_duplicado" value="S"></html:checkbox>
													                        </td>
													                        <td class="principalLabel" width="63%"><bean:message key="prompt.atualizarCadastro"/></td>
													                      </tr>
													                    </table>
													                  </td>
													                </tr>
													              </table>
													            </td>
													          </tr>
													        </table>
													</td>
												</tr>
											  	</table>
										     
										      <table width="100%" border="0" cellspacing="0" cellpadding="0" id="plusinfo" style="display: none">
												<tr>
													<td>	
												        <table width="100%" border="0" cellspacing="0" cellpadding="0">
												          <tr> 
												            <td class="principalLabel" width="22%" align="right">
																<bean:message key="prompt.visaoPlusinfo"/>
																<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
												            </td>
												            <td class="principalLabel" width="78%"> 
												              <table width="100%" border="0" cellspacing="0" cellpadding="0">
												                <tr> 
												                  <td width="74%"> 
												                    <html:select property="id_visa_cd_visao" styleClass="principalObjForm">
																		<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/></html:option>
																		<logic:present name="visaVector" >
																			<html:options collection="visaVector" property="idAplicIdVisao" labelProperty="visaDsVisao" />
																		</logic:present>
																	</html:select>	
												                  </td>
												                  <td width="26%">&nbsp;</td>
												                </tr>
												              </table>
												            </td>
												          </tr>
												        </table>			
												        						        
												        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="display:none">
												          <tr> 
												            <td class="principalLabel" width="22%" align="right"> <bean:message key="prompt.analisarPor"/> 
												              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
												            <td class="principalLabel" width="78%"> 
												              <table width="100%" border="0" cellspacing="0" cellpadding="0">
												                <tr > 
												                  <td width="74%"> 
												                    <html:select property="agca_ds_analisarpor" styleClass="principalObjForm">																		
																		<html:option value="PESS.ID_PESS_CD_PESSOA"><bean:message key="prompt.codigo" /></html:option>	    																
																	</html:select>	
												                  </td>
												                  <td width="26%">&nbsp;</td>
												                </tr>
												              </table>
												            </td>
												          </tr>
												        </table>
										      		</td>
												</tr>
											  	</table>
										    </td>
										  </tr>
										</table>
									<!-- FIM ABA IMPORTACAO -->
								</td>
					          </tr>
					        </table>
					        
					      </div>
					      <div id="conteudo" style="width:100%;display: none"> 
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td>&nbsp;</td>
					          </tr>
					        </table>
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.execucaoAPartirDe"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					            </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="27%"> 
					                    <table width="92%" border="0" cellspacing="0" cellpadding="0">
					                      <tr> 
					                        <td width="89%"> 
					                          <html:text property="agca_dh_execucaode" styleClass="principalObjForm" maxlength="10" onkeydown="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''"></html:text>
					                        </td>
					                        <td width="11%" align="center"> <img src="webFiles/images/botoes/calendar.gif" title="Calendário" width="16" height="15" class="geralCursoHand" onclick="show_calendar('forms[0].agca_dh_execucaode')"></td>
					                      </tr>
					                    </table>
					                  </td>
					                  <td width="73%"> 
					                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                      <tr> 
					                        <td width="15%" class="principalLabel" align="right"><bean:message key="prompt.ate"/> 
					                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td class="principalLabel" width="27%"> 
					                          <table width="92%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="89%"> 
					                                <html:text property="agca_dh_execucaoate" styleClass="principalObjForm" maxlength="10" onkeydown="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''"></html:text>
					                              </td>
					                              <td width="11%" align="center"> <img src="webFiles/images/botoes/calendar.gif" title="Calendário" width="16" height="15" class="geralCursoHand" onclick="show_calendar('forms[0].agca_dh_execucaoate')"></td>
					                            </tr>
					                          </table>
					                        </td>
					                        <td width="58%">&nbsp; </td>
					                      </tr>
					                    </table>
					                  </td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>					        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.horario"/> 
					              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					            </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="22%" class="principalLabel"> 
					                    <html:text property="agca_ds_horaexecucao" styleClass="principalObjForm" maxlength="5" onkeydown="return validaDigitoHora(this, event)" onblur="validaHora(this)"></html:text>
					                  </td>
					                  <td width="78%" class="principalLabel">&nbsp;</td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>					        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.umaVezAoDia"/> 
					              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="77%" class="principalLabel"> 
					                    <html:checkbox property="agca_in_diario" value="S"></html:checkbox>
					                  </td>
					                  <td width="23%" class="principalLabel">&nbsp;</td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>					        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.diaSemana"/> 
					              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					            </td>
					            <td class="principalLabel" width="78%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="77%" class="principalLabel"> 
					                    <html:checkbox property="agca_in_segunda" value="S"></html:checkbox>
					                    <bean:message key="prompt.segunda"/>
					                    <html:checkbox property="agca_in_terca" value="S"></html:checkbox>
					                    <bean:message key="prompt.terca"/>
					                    <html:checkbox property="agca_in_quarta" value="S"></html:checkbox>
					                    <bean:message key="prompt.quarta"/> 
					                    <html:checkbox property="agca_in_quinta" value="S"></html:checkbox>
					                    <bean:message key="prompt.quinta"/>
					                    <html:checkbox property="agca_in_sexta" value="S"></html:checkbox>
					                    <bean:message key="prompt.sexta"/>
					                    <html:checkbox property="agca_in_sabado" value="S"></html:checkbox>
					                    <bean:message key="prompt.sabado"/>
					                    <html:checkbox property="agca_in_domingo" value="S"></html:checkbox>
					                    <bean:message key="prompt.domingo"/></td>
					                  <td width="23%" class="principalLabel">&nbsp;</td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>
					        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
					          <tr> 
					            <td>&nbsp;</td>
					          </tr>
					        </table>
					        </div>
					      <div id="adicional" style="width:100%;display: none"> 
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td>&nbsp;</td>
					          </tr>
					        </table>
					        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					          <tr > 
					            <td class="principalLstCab" width="50%">&nbsp;<bean:message key="prompt.agendamentocampanha.arquivo"/></td>
					            <td class="principalLstCab" align="center" width="30%"><bean:message key="prompt.dataProcessamento"/></td>
					            <td class="principalLstCab" align="center" width="20%"><bean:message key="prompt.erro"/></td>
					          </tr>
					        </table>
					        <div style="overflow:auto;height: 130px;overflow-y: scroll">
					        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					          <logic:present name="historicoExec">
								<logic:iterate id="hiac" name="historicoExec">
								  <tr onClick="showModalDialog('AbrirDetalhesImportacao.do?id_agca_cd_agendacampanha=<bean:write name="hiac" property="field(id_agca_cd_agendacampanha)"/>&hiac_nr_sequencia=<bean:write name="hiac" property="field(hiac_nr_sequencia)"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand"> 
						            <td class="principalLstPar" width="48%">&nbsp;<script>acronym('<bean:write name="hiac" property="field(hiac_nm_arquivo)"/>',40)</script></td>
						            <td class="principalLstPar" width="37%" align="center"><bean:write name="hiac" property="field(hiac_dh_processamento)" format="dd/MM/yyyy HH:mm:ss"/></td>
						            <td class="principalLstPar" width="15%" align="center">
										<logic:equal name="hiac" property="field(hiac_in_houveerro)" value="S">
											<bean:message key="prompt.sim"/>
										</logic:equal>
										<logic:equal name="hiac" property="field(hiac_in_houveerro)" value="N">
											<bean:message key="prompt.nao"/>
										</logic:equal>
									</td>
						          </tr>
						       </logic:iterate>					          	  					          
					          </logic:present>								  
					        </table>
					        </div>
					       
					        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					          <tr> 
					            <td class="principalLabel" width="22%" align="right">&nbsp;</td>
					          </tr>
					          <tr> 
					            <td class="principalLabel" width="22%" align="right"> 
					              <HR>
					            </td>
					          </tr>
					          <tr> 
					            <td class="principalLabel" width="22%" align="right">&nbsp;</td>
					          </tr>
					        </table>					        
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
					            <td class="principalLabel" width="15%" align="right"><bean:message key="prompt.cadastradoEm"/> 
					              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
					            <td class="principalLabel" width="85%"> 
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tr> 
					                  <td width="15%" class="principalLabel"> 
					                    <html:text property="agca_dh_cadastro" styleClass="principalObjForm" maxlength="10" disabled="true"></html:text>
					                  </td>
					                  <td width="85%" class="principalLabel"> 
					                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                      <tr> 
					                        <td class="principalLabel" align="right" width="24%"><bean:message key="prompt.atualizadoEm"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                        </td>
					                        <td class="principalLabel" width="76%"> 
					                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr> 
					                              <td width="25%"> 
					                                <html:text property="agca_dh_atualizacao" styleClass="principalObjForm" maxlength="10" disabled="true"></html:text>
					                              </td>
					                              <td width="75%"> 
					                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                  <tr> 
					                                    <td class="principalLabel" align="right" width="61%"><bean:message key="prompt.ultimoProc"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					                                    </td>
					                                    <td class="principalLabel" width="39%"> 
					                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                        <tr> 
					                                          <td width="64%"> 
																<logic:present name="ultimoProcessamento">
																	<input type="text" name="textfield32224" class="principalObjForm" value="<bean:write name="ultimoProcessamento" property="field(hiac_dh_processamento)" format="dd/MM/yyyy HH:mm:ss"/>" disabled="true">
																</logic:present>
					                                            <logic:notPresent name="ultimoProcessamento">
																	<input type="text" name="textfield32224" class="principalObjForm" value="" disabled="true">
																</logic:notPresent>
					                                          </td>
					                                          <td width="36%">&nbsp;</td>
					                                        </tr>
					                                      </table>
					                                    </td>
					                                  </tr>
					                                </table>
					                              </td>
					                            </tr>
					                          </table>
					                        </td>
					                      </tr>
					                    </table>
					                  </td>
					                </tr>
					              </table>
					            </td>
					          </tr>
					        </table>
					        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
					          <tr> 
					            <td>&nbsp;</td>
					          </tr>
					        </table>
					      </div>
					    </td>
					  </tr>
					</table>	
				<!-- FIM ABAS -->
		   </td>
	     </tr>
	   </table>
	   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
	     <tr> 
	       <td>&nbsp;</td>
	     </tr>
	   </table>
	   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
	     <tr> 
	       <td>&nbsp;</td>
	     </tr>
	   </table>
</html:form>
</body>

<script>

		 	if(document.forms[0].id_agca_cd_agendacampanha.value == '' || document.forms[0].id_agca_cd_agendacampanha.value == 0){
				
			}else{
				if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_AGENDAMENTO_ALTERACAO_CHAVE%>')){
					setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_AGENDAMENTO_ALTERACAO_CHAVE%>', parent.window.document.all.item("imgGravar"));	
					desabilitaCamposArea();
				}				
			}
</script>
</html>