<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title>-- CRM -- Plusoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>
<body class="principalBgrPage" text="#000000" scroll="no" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroLinkSelecionadoMAIOR" height="17" width="166">Resultado 
            da Importa&ccedil;&atilde;o</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="98%" align="center"> 
                  <textarea name="textfield" class="principalObjForm" rows="15" readonly="readonly">
					<logic:present name="hiac">
						<bean:write name="hiac" property="field(hiac_ds_historicoimportacao)"/>
					</logic:present>
				  </textarea>
                </td>
                <td width="2%">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	          <tr > 
	            <td class="principalLstCab" width="10%">&nbsp;Linha Erro</td>
	            <td class="principalLstCab" align="center" width="40%">Descri��o Erro</td>
	            <td class="principalLstCab" align="center" width="40%">Linha Lida</td>
				<td class="principalLstCab" align="center" width="10%">Trace</td>
	          </tr>
	        </table>
	        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	          <tr>
				<td>	
					<div style="overflow: auto;height: 170px">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							 <logic:present name="dehiVector">
								<logic:iterate id="dehi" name="dehiVector" indexId="index">				   
						            <tr> 
						            	<td class="principalLstPar" width="10%"><bean:write name="dehi" property="field(dehi_nr_linhaerro)"/>&nbsp;</td>
						            	<td class="principalLstParMao" align="center" width="40%"><textarea rows="1" cols="1" name="descErro" id="descErro<bean:write name="index"/>" style="display: none"><bean:write name="dehi" property="field(dehi_ds_descricaoerro)"/></textarea>
											<img src="/plusoft-resources/images/icones/binoculo.gif" style="width : 13px; height : 13px;" title="Descri��o Erro" onclick="showModalDialog('DetalharDescricao.do',document.getElementById('descErro<bean:write name="index"/>').value,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:450px,dialogTop:0px,dialogLeft:650px');"/>
										</td>
						            	<td class="principalLstParMao" align="center" width="40%">
											<textarea rows="1" cols="1" name="leituraLinha" id="leituraLinha<bean:write name="index"/>"  style="display: none"><bean:write name="dehi" property="field(dehi_ds_leituralinha)"/></textarea>
											<img src="/plusoft-resources/images/icones/binoculo.gif" style="width : 13px; height : 13px;" title="Linha Lida" onclick="showModalDialog('DetalharDescricao.do',document.getElementById('leituraLinha<bean:write name="index"/>').value,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:450px,dialogTop:0px,dialogLeft:650px');"/>
										</td>
										<td class="principalLstParMao" align="center" width="10%">
											<textarea rows="1" cols="1" name="stackTrace" id="stackTrace<bean:write name="index"/>"  style="display: none"><bean:write name="dehi" property="field(dehi_tx_stacktrace)"/></textarea>
											<img src="/plusoft-resources/images/icones/binoculo.gif" style="width : 13px; height : 13px;" title="Stack Trance" onclick="showModalDialog('DetalharDescricao.do',document.getElementById('stackTrace<bean:write name="index"/>').value,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:450px,dialogTop:0px,dialogLeft:650px');"/>
										</td>
						            </tr>		         
								</logic:iterate>					          	  					          
					          </logic:present>	
						</table>	
					</div>				         							  
	        </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="30" border="0" cellspacing="0" cellpadding="0" align="right">
  <tr> 
    <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" onClick="javascript:window.close()" title="Sair"></td>
  </tr>
</table>
</body>
</html>