<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<link rel="stylesheet" href="../../../../../webFiles/css/estilos.css" type="text/css">
<link rel="stylesheet" href="webFiles/css/estilos.css" type="text/css">

<script type="text/javascript" src="webFiles/javascripts/funcoesMozilla.js"></script>


<script>
	function onLoad(){
		document.forms[0].texto.value =  window.dialogArguments;
	}
</script>

<body class="framePadrao" onload="onLoad()">
<html:form action="DetalharDescricao">
	<div id="bordaPrincipal">
		<div class="titulo">
			Detalhe
		</div>
		
		<div class="borda" style=" height : 400px;">	
			<div style="float: left; width : 90%px; height : 400px;">
				<li class="seta">
					<textarea rows="1" name="texto" rows="1" style="height : 360px; width : 700px;" readonly="true">
					</textarea>						
				</li>									
			</div>	
			
		</div>
		<div style="clear: left; float: right">
			<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Cancelar" onClick="javascript:window.close()" class="cursor">
		</div>
	</div>
</html:form>
</body>
