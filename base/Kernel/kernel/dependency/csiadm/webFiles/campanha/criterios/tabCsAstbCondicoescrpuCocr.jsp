<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<form action="#" name="criteriosPublCondicoesForm">
<div class="formrow">
	<input type="hidden" name="idCocrCdCondicoescrpu" id="idCocrCdCondicoescrpu" />
	<input type="hidden" name="cocrNrSequencia" id="cocrNrSequencia" />

	<table width="100%">
		<thead>
		<tr>
			<td><label for="idTadbCdTabeladb">Tabela</label></td>
			<td><label for="idCataCdCampotabela">Campo</label></td>
			<td><label for="idOpccCdOptcondcriteria">Condi��o</label></td>
			<td id="tdFiltroCondicao"><label for="cocrDsFiltro">Filtro/Valor</label></td>
			<td width="40px">&nbsp;</td>
		</tr>
		</thead>
		<tr>
			<td>
				<select name="idTadbCdTabeladb" id="idTadbCdTabeladb" class="tabeladb">
					<option value="">-- Selecione uma op��o--</option>
					
					<logic:present name="tadbList"><logic:iterate id="tadb" name="tadbList">
					<option value="<bean:write name="tadb" property="field(id_tadb_cd_tabeladb)" />"><bean:write name="tadb" property="field(tadb_ds_descricao)" /></option>
					</logic:iterate></logic:present>
				</select>
			</td>
			<td>
				<select name="idCataCdCampotabela" id="idCataCdCampotabela" class="campotabela">
					<option value="">-- Selecione uma op��o--</option>
				</select>
			</td>
			<td>
				<select name="idOpccCdOptcondcriteria" id="idOpccCdOptcondcriteria" class="optcondcriteria">
					<option value="">-- Selecione uma op��o--</option>
					
					<logic:present name="opccList"><logic:iterate id="opcc" name="opccList">
					<option value="<bean:write name="opcc" property="field(id_opcc_cd_optcondcriteria)" />"
						semfiltro="<bean:write name="opcc" property="field(opcc_in_semfiltro)" />" 
						tipod="<bean:write name="opcc" property="field(opcc_in_tipod)" />" 
						tipon="<bean:write name="opcc" property="field(opcc_in_tipon)" />" 
						tipos="<bean:write name="opcc" property="field(opcc_in_tipos)" />">
							<bean:write name="opcc" property="field(opcc_ds_descricao)" /></option>
					</logic:iterate></logic:present>
				</select>
			</td>
			<td id="tdFiltroCondicaoInput">
				<input type="text" class="text" name="cocrDsFiltro" id="cocrDsFiltro" maxlength="1000" />
			</td>
			<td width="30px"><a href="#" class="setadown" id="btIncluirCondicao" title="Incluir"></a></td>
		</tr>
	</table>
</div>
</form>

<div class="topmargin listcondicoes">
	<jsp:include page="listCsAstbCondicoescrpuCocr.jsp" />
</div>
