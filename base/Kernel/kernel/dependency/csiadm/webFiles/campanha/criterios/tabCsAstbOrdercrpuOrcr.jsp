<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<form action="#" name="criteriosPublOrderForm">

<div class="formrow">
	<input type="hidden" name="idOrcrCdOrdercrpu" id="idOrcrCdOrdercrpu" />
	<input type="hidden" name="orcrNrSequencia" id="orcrNrSequencia" />

	<table width="650">
		<thead>
		<tr>
			<td><label for="idTadbCdTabeladbOrcr">Tabela</label></td>
			<td><label for="idCataCdCampotabelaOrcr">Campo</label></td>
			<td width="180px">Ordena��o</td>
			<td width="40px">&nbsp;</td>
		</tr>
		</thead>
		<tr>
			<td>
				<select name="idTadbCdTabeladbOrcr" id="idTadbCdTabeladbOrcr" class="tabeladbOrder">
					<option value="">-- Selecione uma op��o--</option>
					
					<logic:present name="tadbOrderByList"><logic:iterate id="tadb" name="tadbOrderByList">
					<option value="<bean:write name="tadb" property="field(id_tadb_cd_tabeladb)" />"><bean:write name="tadb" property="field(tadb_ds_descricao)" /></option>
					</logic:iterate></logic:present>
				</select>
			</td>
			<td>
				<select name="idCataCdCampotabelaOrcr" id="idCataCdCampotabelaOrcr" class="campotabela">
					<option value="">-- Selecione uma op��o--</option>
				</select>
			</td>
			<td>
				<input type="radio" class="orcrorder" id="orcrInOrderAsc" name="orcrInOrder" value="A" checked /> <label for="orcrInOrderAsc">Asc</label>
				<input type="radio" class="orcrorder" id="orcrInOrderDesc" name="orcrInOrder" value="D" /> <label for="orcrInOrderDesc">Desc</label>
			</td>
			<td width="30px"><a href="#" class="setadown" id="btIncluirOrder" title="Incluir"></a></td>
		</tr>
	</table>
</div>
</form>

<div class="topmargin listorder">
	<jsp:include page="listCsAstbOrdercrpuOrcr.jsp" />
</div>
