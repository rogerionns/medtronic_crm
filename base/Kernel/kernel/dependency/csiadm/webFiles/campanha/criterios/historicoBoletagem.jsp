<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<table id="tablehist">
	<tbody>
		<logic:present name="historicoVec">
			<logic:iterate id="hist" name="historicoVec" indexId="idx">
				<tr>
					
					<td class="fix90Center"><bean:write name="hist" property="field(HIBO_DH_EXECUCAO)" format="dd/MM/yyyy HH:mm" /></td>
					<td class="fix60Center"><bean:write name="hist" property="field(HIBO_NR_PUBLICO)"/></td>
					<td class="fix110"><bean:write name="hist" property="acronymHTML(FUNC_NM_FUNCIONARIO, 12)" filter="html" /></td>
					<td class="fix60Center"><bean:write name="hist" property="field(HIBO_DH_DATABASE)" format="dd/MM/yyyy" /></td>
					<td class="fix70Center"><bean:write name="hist" property="field(HIBO_NR_DESCONTO)" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
					<td class="fix110"><bean:write name="hist" property="acronymHTML(GRRE_DS_GRUPORENE, 12)" filter="html" /></td>
					<td class="fix110"><bean:write name="hist" property="acronymHTML(ACCO_DS_ACAOCOB, 12)" filter="html" /></td>
					<td class="fix110"><bean:write name="hist" property="acronymHTML(INBO_DS_NOME, 12)" filter="html" /></td>
					<td class="image reenvio"><a href="#" class="carta" title="Reenviar" idhist="<bean:write name="hist" property="field(id_hibo_cd_historicobolet)" />"></a></td>
				</tr>
			</logic:iterate>
		</logic:present>
	</tbody>
</table>
				