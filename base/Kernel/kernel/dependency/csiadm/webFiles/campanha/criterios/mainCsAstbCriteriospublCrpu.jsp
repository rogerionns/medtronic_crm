<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>

<title><plusoft:message key="prompt.criteriosSubCampanha" /></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<% 
final String CONF_APL_COBRANCA = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_COBRANCA,request);

if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<script type="text/javascript">
	var CONF_APL_COBRANCA_AUX = '<%=CONF_APL_COBRANCA%>';
	
	var O_campo_descricao_e_obrigatorio = '<plusoft:message key="prompt.O_campo_descricao_e_obrigatorio" />';
	var O_campo_prioridade_e_obrigatorio = '<plusoft:message key="prompt.O_campo_prioridade_e_obrigatorio" />';
	var selecione_um_campo = '<plusoft:message key="prompt.selecione_um_campo" />';
	var selecione_uma_condicao = '<plusoft:message key="prompt.selecione_uma_condicao" />';
	var cliqueEmGravarAntesDeDefinirOperadoresDaSubcampanha = '<bean:message key="prompt.cliqueEmGravarAntesDeDefinirOperadoresDaSubcampanha" />';
	var cliqueEmGravarAntesDeVerificarOTotalDeRegitrosUtilizandoEsseCriterio = '<bean:message key="prompt.cliqueEmGravarAntesDeVerificarOTotalDeRegitrosUtilizandoEsseCriterio" />';
	var essa_acao_poder_demorar = '<plusoft:message key="prompt.criterio.confirm.essa_acao_poder_demorar" />';
	var totalDeRegitrosUtilizandoEsseCriterio = '<bean:message key="prompt.totalDeRegitrosUtilizandoEsseCriterio" />';
	var antecipaAgendamentos = '<bean:message key="prompt.antecipaAgendamentos"/>';
	var confirmaAtecipaAgendamento = '<plusoft:message key="prompt.confirmaAtecipaAgendamento" />';
	var boletagem = '<plusoft:message key="prompt.Boletagem" />';
		
	var idJ = '<%=request.getParameter("id") %>';
</script>
	
<style type="text/css">
	.tela { margin: 10px; }
	.listcriterios .list { width: 870px; }
	.listcriterios .scrolllist { height: 350px; } 
	.listcriterios .scrolllist table { width: 850px; }
	
	.listcondicoes .list { width: 770px; }
	.listcondicoes .scrolllist { height: 120px; }
	.listcondicoes .scrolllist table { width: 750px; }
	
	.listorder .list	   { width: 630px; }
	.listorder .scrolllist { height: 120px; }
	.listorder .scrolllist table { width: 610px; }
	
	.detalhe { margin: 40px; margin-bottom: 0px; }
	.geral .innertab { height: 450px; }
	.detalhe .innertab { height: 200px; }
	div.formtable { margin-top: 30px; }
	form input.text { padding: 3px; border-radius: 5px;  }
	form select  { height: 23px; padding: 3px; border-radius: 5px; cursor: pointer; width: 180px;  }
	
	div.btnoperadores { cursor: pointer; display: inline; position: absolute; top: 160px; left: 600px; }
	div.btnoperadores img { vertical-align: middle; }
	.clickable{ cursor: pointer; }
	
	div.btntotalcriterio { cursor: pointer; display: inline; position: absolute; top: 460px; left: 60px; }
	div.btntotalcriterio img { vertical-align: middle; }
	
	div.divBotoes  { width: 800px; height:30px; margin: 0; overflow:hidden; margin-left: 40px; margin-top: 5px; float:left;}
	div.divBotoes img { vertical-align: middle; }
	
	div.btnsEspec  { height:30px; margin: 0; }
	div.btnsEspec img { vertical-align: middle; }
	
	div.divSetaDir { cursor: pointer; display: inline; position: absolute; top: 468px; left: 875px; }
	div.divSetaDir img { vertical-align: middle; }

</style>

<logic:present name="criteriosCSS"><link rel="stylesheet" href="<bean:write name='criteriosCSS' />/criterios.css"></logic:present>

</head>
<body class="tela">
	<!-- Plusoft Tabs -->
	<div class="tabframe geral topmargin">
		<div class="tabtabs">
			<ul>
				<li id="procurar"><plusoft:message key="prompt.procurar" /></li>
				<li id="criterio"><plusoft:message key="prompt.criterio" /></li>
			</ul>
		</div>

		<div class="tabcontent">
			<div id="procurartab">
				<div class="listcriterios">
					<jsp:include page="administracaoCsAstbCriteriospublCrpu.jsp" />
				</div>
			</div>
			
			<div id="criteriotab">
				<jsp:include page="editCsAstbCriteriospublCrpu.jsp" />
			</div>
		</div>
	</div>
	
	<div class="btnDiv" style="float: right; margin: 10px;">
		<a href="javascript:clear('criterio'); " title="Novo" class="margin left novo"></a>
		<a href="javascript:gravar(); " title="Gravar" class="margin left gravar"></a>
		<a href="javascript:clear('procurar'); " title="Cancelar" class="margin left cancelar"></a>
	</div>
	<hr/>
	
	<div style="clear: right; float: right;">
		<a href="#" title="Sair" class="margin sair"></a>
	</div>

	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	
	<script type="text/javascript" src="/csiadm/webFiles/campanha/criterios/criterios.js"></script>
	<logic:present name="criteriosJS"><script type="text/javascript" src="<bean:write name='criteriosJS' />criterios.js"></script></logic:present>
	
	<script>
		
	</script>
</body>
</html:html>
