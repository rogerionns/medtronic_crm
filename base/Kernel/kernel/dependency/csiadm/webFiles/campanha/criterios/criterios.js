	var win = window.opener;
	if(window.opener.window.opener != undefined)
		win = window.opener.window.opener;

	var largura = 0;
	var finalizada = false;
	
	//Metodo que adiciona os bot�es espec na tela
	var addButton=function(src, id, title, width) {
		largura += new Number(width);
		var botao = "";
		
		botao += "<div id=\"child\" style=\"float: left; width: "+ width +"px;\">";
		botao += "<span class=\""+ id +"\" id=\"" + id + "\" style=\"cursor:pointer\"> <img src=\"" + src + "\" title=\"" + title + "\" />";
		botao += title;
		botao += "</span>"
		botao += "</div>";
		
		document.getElementById("btnsEspec").insertAdjacentHTML("BeforeEnd", botao);
		//$('.btnsEspe').html(botao);
	}
	
	//Chama a fun��o que adiciona os bot�es do kernel
	addButton("/plusoft-resources/images/botoes/qtd.gif", "btntotalcriterio", totalDeRegitrosUtilizandoEsseCriterio, "180");
	addButton("/plusoft-resources/images/botoes/agendaCalendario.gif", "btnAteciparAgenda", antecipaAgendamentos, "180");
	
	if(CONF_APL_COBRANCA_AUX != undefined && CONF_APL_COBRANCA_AUX == 'S')
		addButton("/plusoft-resources/images/botoes/boletagem.png", "btnBoletagem", boletagem, "100");
	
	
	//inclus�o de botoes para teste
	/*addButton("/plusoft-resources/images/botoes/agendaCalendario.gif", "11111", antecipaAgendamentos, "180");
	addButton("/plusoft-resources/images/botoes/agendaCalendario.gif", "22222", antecipaAgendamentos, "180");
	addButton("/plusoft-resources/images/botoes/agendaCalendario.gif", "33333", antecipaAgendamentos, "180");
	addButton("/plusoft-resources/images/botoes/agendaCalendario.gif", "btnAt444eciparAgenda", antecipaAgendamentos, "180");
	addButton("/plusoft-resources/images/botoes/agendaCalendario.gif", "btnAte55555ciparAgenda", antecipaAgendamentos, "180");
	addButton("/plusoft-resources/images/botoes/agendaCalendario.gif", "btnAtec6666iparAgenda", antecipaAgendamentos, "180");
	addButton("/plusoft-resources/images/botoes/agendaCalendario.gif", "btnA77777teciparAgenda", antecipaAgendamentos, "180");
	addButton("/plusoft-resources/images/botoes/agendaCalendario.gif", "btnA88888teciparAgenda", antecipaAgendamentos, "180");*/

	var tpca = "";
		
	$(".tabframe").inittabs();
	$("#crpuNrPrioridade").number();

	$("a.sair").bindClose();
	
	var clear=function(tab) {
		document.criteriosPublEditForm.reset();
		$(".listcondicoes").load("/csiadm/campanha/criterios/list/load.do", { fw:"cocr" }, onloadCondicoes);
		$(".listorder").load("/csiadm/campanha/criterios/list/load.do", { fw:"orcr" }, onloadOrder);
		if(tab) $("#"+tab).click();
	}

	var gravar=function() {
		if($.trim(document.criteriosPublEditForm.crpuDsCriteriospubl.value)=="") {
			alert(O_campo_descricao_e_obrigatorio);
			$("#crpuDsCriteriospubl").focus();
			return;
		}

		if($.trim(document.criteriosPublEditForm.crpuNrPrioridade.value)=="") {
			alert(O_campo_prioridade_e_obrigatorio);
			$("#crpuNrPrioridade").focus();
			return;
		}

		
		document.criteriosPublEditForm.condicoesViewState.value = $("#condicoesViewState").val();
		document.criteriosPublEditForm.orderViewState.value = $("#orderViewState").val();
		
		$("#procurar").click();
		$(".listcriterios").loadaguarde();

		$(".btnDiv a").attr("disabled", true);
		$(".btnDiv a").fadeTo('fast', .4);


		var jsondata = {
				id: getIdPublCdPublico(),
				condicoesViewState : $("#condicoesViewState").val(),
				orderViewState : $("#orderViewState").val(),
				idCrpuCdCriteriospubl : getIdCrpuCdCriteriospubl(),
				crpuDsCriteriospubl : document.criteriosPublEditForm.crpuDsCriteriospubl.value,
				crpuNrPrioridade : document.criteriosPublEditForm.crpuNrPrioridade.value,
				crpuInInativo : document.criteriosPublEditForm.crpuInInativo.checked?"S":"N"
		}
		
		$(".listcriterios").load("/csiadm/campanha/criterios/gravar.do", jsondata, function() {
			onload_crpulist();

			$(".btnDiv a").attr("disabled", false);
			$(".btnDiv a").fadeTo('fast', 1);
		});
		
	}

	var onloadCondicoes = function() {
		document.criteriosPublCondicoesForm.reset();
		$("#idOpccCdOptcondcriteria").change();
		$(".tabeladb").change();
		
		$(".listcondicoes").find(".lixeira").bind("click", function(event) {
			event.preventDefault();
			$(".listcondicoes").load("/csiadm/campanha/criterios/list/remove.do", { fw:"cocr", listViewState : $("#condicoesViewState").val(), n:$(this).parent().parent().attr("indx") }, onloadCondicoes);
		});
	}
	
	var onloadOrder = function() {
		document.criteriosPublOrderForm.reset();
		$(".tabeladbOrder").change();
		
		$(".listorder").find(".lixeira").bind("click", function(event) {
			event.preventDefault();
			$(".listorder").load("/csiadm/campanha/criterios/list/remove.do", { fw:"orcr", listViewState : $("#orderViewState").val(), n:$(this).parent().parent().attr("indx") }, onloadOrder);
		});
	}

	
	var onload_crpulist=function() {

		$("#filtro").bind("keydown", function(event) {
			if(event.which==13) {
				event.preventDefault();

				$("#filtrar").click();
				return false;
			}
		});
		
		/*
		 * Bot�o Filtar
		 */
		$("#filtrar").bind("click", function(event) {
			event.preventDefault();
			$("#crpulist").loadaguarde();
			$(".listcriterios").load("/csiadm/campanha/criterios/buscar.do", { 
				id : idJ, 
				filtro: document.criteriosPublProcurarForm.filtro.value 
			}, onload_crpulist);
		});

		/*
		 * Bot�es excluir da lista na aba Procurar
		 */
		if($("#crpulist tbody .lixeira").length > 1) {
			$("#crpulist tbody .lixeira").bind("click", function(event) {
				event.preventDefault();
				event.stopPropagation();
				$("#crpulist").loadaguarde();
				$(".listcriterios").load("/csiadm/campanha/criterios/remover.do", { id : idJ, crpu : $(this).parent().attr("crpu") }, onload_crpulist);
			});
		} else {
			$("#crpulist tbody .lixeira").fadeTo('fast', 0.4);
			$("#crpulist tbody .lixeira").attr('disabled', true);
		}

		/*
		 * Clique de cada item da lista da aba procurar
		 */
		$("#crpulist tbody tr td").bind("click", function(event) {
			var data = { 
					id: $(this).attr("crpu")
			};

			$.post("/csiadm/campanha/criterios/carregar.do", data, function(ret) {

				if(ret.msgerro) {
					alert(ret.msgerro);
					return;
				}

				document.criteriosPublEditForm.reset();
				document.criteriosPublEditForm.idCrpuCdCriteriospubl.value = ret.id_crpu_cd_criteriospubl;
				document.criteriosPublEditForm.crpuDsCriteriospubl.value = ret.crpu_ds_criteriospubl;
				document.criteriosPublEditForm.crpuNrPrioridade.value = ret.crpu_nr_prioridade;
				document.criteriosPublEditForm.crpuInInativo.checked = (ret.crpu_dh_inativo!="");
				document.criteriosPublEditForm.crpuDhInativo.value = ret.crpu_dh_inativo;
				
				$(".listcondicoes").load("/csiadm/campanha/criterios/list/load.do", { listViewState : ret.cocr, fw:"cocr" }, onloadCondicoes);
				$(".listorder").load("/csiadm/campanha/criterios/list/load.do", { listViewState : ret.orcr, fw:"orcr" }, onloadOrder);

				$("#criterio").click();
				
			});
		});

		clear('procurar');
		$("#filtro").focus();
	};
	onload_crpulist();


	$(".tabeladb").bind("change", function(event) {
		$(this).parent().parent().find(".campotabela").jsonoptions("/plusoft-eai/generic/consulta-banco", "id_cata_cd_campotabela", "cata_ds_descricao", {	
			"entity":"br/com/plusoft/csi/adm/dao/xml/cobranca/CS_CDTB_CAMPOTABELA_CATA.xml",
			"statement":"select-campos-campanha",
			"id_tadb_cd_tabeladb": $(this).val(), 
			"type":"json"
		});
	});
	
	$(".tabeladbOrder").bind("change", function(event) {
		$(this).parent().parent().find(".campotabela").jsonoptions("/plusoft-eai/generic/consulta-banco", "id_cata_cd_campotabela", "cata_ds_descricao", {	
			"entity":"br/com/plusoft/csi/adm/dao/xml/cobranca/CS_CDTB_CAMPOTABELA_CATA.xml",
			"statement":"select-campos-orderby",
			"id_tadb_cd_tabeladb": $(this).val(), 
			"type":"json"
		});
	});
	
	$(".optcondcriteria").bind("change", function(event) {
		if(tpca == "DECIMAL" && $(this).val() == 3){
			alert("Esta condi��o n�o pode ser usada para o campo tipo Moeda!");
			$(this).val("");
			return;
		}
	});
	
	$(".campotabela").bind("change", function(event) {
		var tpca = $(this).find(":selected").attr("tpca_ds_tpcampo")!=undefined?$(this).find(":selected").attr("tpca_ds_tpcampo"):"";
		if(tpca == "DECIMAL"){
			$("#cocrDsFiltro").money();
		}else{
			$("#cocrDsFiltro").money_destroy();
		}
	});
	

	$("#idOpccCdOptcondcriteria").bind("change", function(event) {
		var sfil = $(this).find(":selected").attr("semfiltro"); 

		if(sfil=="S") {
			$("#tdFiltroCondicao").css("visibility", "hidden");
			$("#tdFiltroCondicaoInput").css("visibility", "hidden");
		} else {
			$("#tdFiltroCondicao").css("visibility", "visible");
			$("#tdFiltroCondicaoInput").css("visibility", "visible");
		}
	});

	$("#btIncluirCondicao").bind("click", function(event) {
		event.preventDefault();
		
		if($("#idCataCdCampotabela").val()=="") {
			alert(selecione_um_campo);
			return false;
		}
		if($("#idOpccCdOptcondcriteria").val()=="") {
			alert(selecione_uma_condicao);
			return false;
		}
		
		var seq = new Number($("#cocrNrSequencia").val())+1;
		
		var fwdata = { 
			fw:"cocr", 
			listViewState : $("#condicoesViewState").val(),
			id_cocr_cd_condicoescrpu: $("#idCocrCdCondicoescrpu").val(),
			cocr_ds_filtro: $("#cocrDsFiltro").val(), 
			cocr_ds_sqlcondicao:"",
			cocr_nr_sequencia: seq,
			id_opcc_cd_optcondcriteria: $("#idOpccCdOptcondcriteria").val(),
			tpca_ds_tpcampo: $(".campotabela").find(":selected").attr("tpca_ds_tpcampo"),
			opcc_ds_descricao: $.trim($("#idOpccCdOptcondcriteria").find(":selected").text()), 
			id_cata_cd_campotabela: $("#idCataCdCampotabela").val(), 
			cata_ds_descricao: $.trim($("#idCataCdCampotabela").find(":selected").text()), 
			id_tadb_cd_tabeladb: $("#idTadbCdTabeladb").val(),
			tadb_ds_descricao: $.trim($("#idTadbCdTabeladb").find(":selected").text())
		};

		$(".listcondicoes").load("/csiadm/campanha/criterios/list/add.do", fwdata, onloadCondicoes);
		
	});
	
	
	$("#btIncluirOrder").bind("click", function(event) {
		event.preventDefault();
		if($("#idCataCdCampotabelaOrcr").val()=="") {
			alert(selecione_um_campo);
			return false;
		}
		
		var seq = new Number($("#orcrNrSequencia").val())+1;
		
		var fwdata = { 
			fw:"orcr", 
			listViewState : $("#orderViewState").val(),
			orcr_nr_sequencia: seq,
			orcr_in_order: $(".orcrorder:checked").val(),
			id_cata_cd_campotabela: $("#idCataCdCampotabelaOrcr").val(), 
			cata_ds_descricao: $.trim($("#idCataCdCampotabelaOrcr").find(":selected").text()), 
			id_tadb_cd_tabeladb: $("#idTadbCdTabeladbOrcr").val(),
			tadb_ds_descricao: $.trim($("#idTadbCdTabeladbOrcr").find(":selected").text())
		};

		$(".listorder").load("/csiadm/campanha/criterios/list/add.do", fwdata, onloadOrder);
	});

	$("div.btnoperadores").bind("click", function(event) {
		if(document.criteriosPublEditForm.idCrpuCdCriteriospubl.value=="") {
			alert(cliqueEmGravarAntesDeDefinirOperadoresDaSubcampanha);
			return false;
		}
		
		var w = window.open("/csiadm/AdministracaoCsNatbFuncionariopublFupu.do?idPublCdPublico=" + getIdPublCdPublico() +
				"&idCrpuCdCriteriospubl=" + getIdCrpuCdCriteriospubl() +
				"&publDsPublico="+ document.criteriosPublEditForm.crpuDsCriteriospubl.value + 
				"&idEmprCdEmpresa="+ document.criteriosPublEditForm.idEmprCdEmpresa.value +
				"&idFuncCdFuncionario="+ document.criteriosPublEditForm.idFuncCdFuncionario.value,
				"operadoresCrpu", "width=950px,height=630px,top=60px,left=50px,status=no", true);

		w.focus();
			
	});
	
	//Chamado: 80755 - Carlos Nunes - 02/02/2012
	//fun��o do bot�o de contador
	$(".btntotalcriterio").bind("click", function(event) {
		
		if(document.criteriosPublEditForm.idCrpuCdCriteriospubl.value=="") {
			alert(cliqueEmGravarAntesDeVerificarOTotalDeRegitrosUtilizandoEsseCriterio);
			return false;
		}
		
		$.modal.showconfirm(essa_acao_poder_demorar, { buttons: [
			{ label : "Sim", click : function(event) {
				
				$.modal.show("/csiadm/campanha/criterios/VerificarTotalCriterio.do", {
					idCrpuCdCriteriospubl : getIdCrpuCdCriteriospubl(),
					id : getIdPublCdPublico()
				}, totalDeRegitrosUtilizandoEsseCriterio, { width:400, height: 300, buttons: [{ label : "Sair", click : $.modal.close }] }, function() {
				
					$(".mostrarSql").bind("click", function() {
						if(!$(".mostrarSql").attr("selected")){
							$.modal.resize("650", "600");
							$(".conteudaSql").slideDown("slow");
							$(".mostrarSql").attr("selected",true);
						}else{
							$(".conteudaSql").slideUp("slow");
							$(".mostrarSql").attr("selected", false);
							setTimeout('$.modal.resize("400", "300")',300);
						}
					});
					
					$(".mostrarSql").attr("selected", false);
					$(".conteudaSql").hide();
					
				});
			}
			},
			{ label : "N�o", click : $.modal.close }
		] });
	});
	
	//fun��o do bot�o de antecipa��o de agendamento
	$(".btnAteciparAgenda").bind("click", function(event) {
		
		$.modal.showconfirm(confirmaAtecipaAgendamento, { buttons: [
			{ label : "Sim", click : function(event) {
				$.modal.show("/csiadm/campanha/criterios/VerificarTotalCriterio.do", {
					idCrpuCdCriteriospubl : getIdCrpuCdCriteriospubl(),
					antecipaAgendados : "true",
					id : getIdPublCdPublico()
				}, 'Plusoft CRM', { width:200, height: 200, buttons: [{ label : "Sair", click : $.modal.close }] }, function() {});
			}
			},
			{ label : "N�o", click : $.modal.close }
		] });
	});
	
	var carregaHistorico = function(){
		$(".historicobol").load("/csiadm/campanha/criterios/CarregaHistorico.do", { idPublCdPublico: getIdPublCdPublico(), idCrpuCdCriteriospubl: getIdCrpuCdCriteriospubl()}, function(text, status, xhr) {
			$(".scrolltablehist").find(".carta").bind("click", function() {
				var idHistorico = $(this).attr("idhist");
				 $("<div class='modalReenv'><p><b>Voc� realmente deseja reenviar esta boletagem?</b></p></div>").dialog({
			            title: "Alert",
			            modal: true,
			            buttons: {
			                "Sim": function () {
			                	var data = { 
				   						idHiboCdHistoricobolet:	idHistorico
				   					};

			                		$(".modalReenv").loadaguarde();

				   					$.post("/csiadm/campanha/criterios/ExecutarReenvioBoletagem.do", data, function(ret) {

				   						$(".modalReenv").find(".loadaguarde").remove();
				   						
				   						if(ret.msgerro) {
				   							alert(ret.msgerro);
				   							$(".modalReenv").dialog("close");
				   							return;
				   						}
				   				
				   						if(ret.msgsucesso) {
				   							alert(ret.msgsucesso);
				   							$(".modalReenv").dialog("close");
				   							return;
				   						}
				   						
				   					});
			                },
			                "N�o": function () {
			                    $(this).dialog("close");
			                }
			            }
			        });
					 
				});
		});
	}
	
	var verificaExecucao = function(){
		
		if(!finalizada){
			$.post("/csiadm/campanha/criterios/VerificaBoletagem.do", null, function(ret) {
				var texto = "";
				if(ret.msgerro) {
					alert(ret.msgerro);
					return;
				}
				if(ret.finalizado == undefined ){
					if(ret.mensagem != undefined){
						$(".gerarBoletagem").addClass("disabled").bind("click", function(){});
						
						abrirpopup(".popupdet", "350", "450", "Detalhe da execu��o");
						$(".popupdet").loadaguarde();
						
						texto = ret.mensagem;
						texto += '<br>';
						texto += '<br>';
						if(ret.qtdtotal != undefined){
							texto += ret.qtdtotal;
						}
						if(ret.qtdprocessada != undefined){
							texto += '<br>';
							texto += ret.qtdprocessada;
						}
						
						$(".lblDetExecucao").html(texto);
						
					}else{
						finalizada = true;
						$(".popupdet").dialog("close");
					}
				}else{
					finalizada = true;
					$(".popupdet").dialog("close");
				}
			});
			setTimeout('verificaExecucao()', 10000);
		}
	}
	
	var verificaExecExecucao = function(abrir){
		
		$.post("/csiadm/campanha/criterios/VerificaExecBoletagem.do", null, function(ret) {
			var texto = "";
			if(ret.msgerro) {
				alert(ret.msgerro);
				return;
			}
			if(ret.finalizado == undefined ){
				
				if(ret.mensagem != undefined || abrir){
					$(".gerarBoletagem").addClass("disabled").bind("click", function(){});
	
					abrirpopup(".popupdet", "350", "450", "Detalhe da execu��o");
					$(".popupdet").loadaguarde();
					
					if(abrir)
						texto = "PROCESSO EM EXECU��O!";
					else
						texto = ret.mensagem;
					
					texto += '<br>';
					texto += '<br>';
					if(ret.qtdtotal != undefined){
						texto += ret.qtdtotal;
					}
					
					if(ret.qtdprocessada != undefined){
						texto += '<br>';
						texto += ret.qtdprocessada;
					}
					
					$(".lblDetExecucao").html(texto);
					
				}else{
					finalizada = true;
					$(".popupdet").dialog("close");
				}
			}else{
				finalizada = true;
				$(".popupdet").dialog("close");
			}
		});
		setTimeout('verificaExecExecucao('+abrir+')', 5000);
	}
	
	var abrirpopup = function(popup, altura, largura, title) {
		$(popup).dialog({
			height: altura,
			width: largura,
			modal: true,
			resizable: true,
			title: title,
			buttons: {
				Ok : function() {
					$( this ).dialog( "close" );
				}
			},
			open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
	    	close: function(event, ui){
		    	$('body').css('overflow','auto'); 
		    } 
		});
	}
	
	//fun��o do bot�o de boletagem
	$(".btnBoletagem").bind("click", function(event) {
		$.modal.show("/csiadm/campanha/criterios/Boletagem.do", {
			idCrpuCdCriteriospubl : getIdCrpuCdCriteriospubl(),
			idPublCdPublico : getIdPublCdPublico(),
			crpuDsCriteriospubl : document.criteriosPublEditForm.crpuDsCriteriospubl.value
		}, '', { width:850, height: 500, buttons: [{ label : "Sair", click : function() {
						finalizada = true;
						$.modal.close();
					} 
				}] }, function() {
			
			$(".desc").money({
			    thousandsSeparator: '',
			    centsSeparator: '',
			    centsLimit: 0
			});
			
			carregaHistorico();
			
			finalizada = false;
			verificaExecExecucao(false);
			verificaExecucao();
			
			$(".desc").val("100");
			
			$("#db").datepicker();
			
			$("#db").bind("keydown", function(event) {
				return win.window.top.validaDigitoDataHora(this, event);
			});
			
			
			var gerarBoletagem = function(isTeste){
				if(finalizada){
					var envEmail = 'N';
					if($("#txtEnviaEmail:checked").val() == 'S')
						var envEmail = 'S';
					
					var data = { 
						idAccoCdAcaocob:		$(".idAccoCdAcaocob").val(),
						idInboCdInfoboleto: 	$(".idInboCdInfoboleto").val(),
						idGrreCdGruporene: 		$(".idGrreCdGruporene").val(),
						envioEmail:				envEmail,
						database:				$(".datab").val(),
						desconto:				$(".desc").val(),
						testar:					isTeste,
						qtd_teste:				$("#txtQtd").val(),
						email_teste:			$("#txtEmail").val(),
						idPublCdPublico:		getIdPublCdPublico(),
						idCrpuCdCriteriospubl:	getIdCrpuCdCriteriospubl(),
						total:					$("#pubTotal").val()
						//voViewState: 			document.getElementById("criteriosSubCampanhaForm").voViewState.value
					};
	
					finalizada = false;
					verificaExecExecucao(true);
					verificaExecucao();
					
					$.post("/csiadm/campanha/criterios/ExecutarBoletagem.do", data, function(ret) {
						
						carregaHistorico();
						
						if(ret.msgerro) {
							alert(ret.msgerro);
							return;
						}
				
						if(ret.msgsucesso) {
							alert(ret.msgsucesso);
							return;
						}
						
					});
				}else{
					alert("Favor aguardar o t�rmino da execu��o atual!");
				}
			};
			
			$(".gerarBoletagem").bind("click", function(event){
				event.preventDefault();
				event.stopPropagation();
				if(validaCampos()){
					gerarBoletagem(false);
				}
			});
			
			
			$(".testarEnvio").bind("click", function(event){
				event.preventDefault();
				event.stopPropagation();
				
				$("#txtQtd").money({
				    thousandsSeparator: '',
				    centsSeparator: '',
				    centsLimit: 0
				});
				
				if(validaCampos()){
					
						if($(".popup").dialog("option", "carregou") != true){
							$(".popup").dialog({
								autoOpen: false,
								height: 150,
								width: 350,
								modal: true,
								resizable: false,
								title: "Testar Boletagem",
								carregou: true,
								buttons: {
									Ok : function() {
										gerarBoletagem(true);
										$( this ).dialog( "close" );
									},
									Cancelar : function() {
										$( this ).dialog( "close" );
									}
								},
								open: function(event, ui){	$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
						    	close: function(event, ui){	$('body').css('overflow','hide');} 
							});
						}
						
						$(".popup").dialog("open");
					
				}
			});
			
			
			var validaCampos = function() {
				if($(".datab").val() == ""){
					alert("O campo data base � obrigat�rio!")
					return false;
				}
				
				if($(".desc").val() == ""){
					alert("O campo desconto � obrigat�rio!")
					return false;
				}
				
				if($(".idGrreCdGruporene").val() == ""){
					alert("O campo grupo de negoci��o � obrigat�rio!")
					return false;
				}
				
				if($(".idAccoCdAcaocob").val() == ""){
					alert("O campo grupo de negoci��o � obrigat�rio!")
					return false;
				}
				
				if($(".idInboCdInfoboleto").val() == ""){
					alert("O campo grupo de negoci��o � obrigat�rio!")
					return false;
				}
				
				return true;
			}
			
		});
	});
	
	
	
	var getIdPublCdPublico=function() {
		return document.criteriosPublEditForm.id.value;
	}
	
	var getIdCrpuCdCriteriospubl=function() {
		return document.criteriosPublEditForm.idCrpuCdCriteriospubl.value;
	}
	
	//adiciona a fun��o de scroll dos bot�es
	$("#setaLeft").bind("click", function(event) {
		document.getElementById("divBotoes").scrollLeft -= 50;
	});
	
	$("#setaRight").bind("click", function(event) {
		document.getElementById("divBotoes").scrollLeft += 50;
	});
	
	