<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<div class="list">
	<html:form>
		<html:hidden property="listViewState" styleId="condicoesViewState" />
	</html:form>

	<table>
		<thead>
			<tr>
				<td class="sph">&nbsp;</td>
				<td>Tabela</td>
				<td>Campo</td>
				<td class="fix150">Condi��o</td>
				<td>Filtro</td>
				<td class="sph">&nbsp;</td>
			</tr>
		</thead>
	</table>
	
	<div class="scrolllist">
		<table><tbody>
			<logic:present name="listViewState">
			<logic:iterate id="item" name="listViewState" indexId="i">
			<tr indx="<bean:write name="i" />">
				<td class="image"><a href="#" class="lixeira" title="Excluir"></a></td>
				<td><bean:write name="item" property="field(tadb_ds_descricao)" /></td>
				<td><bean:write name="item" property="field(cata_ds_descricao)" /></td>
				<td class="fix150"><bean:write name="item" property="field(opcc_ds_descricao)" /></td>
				<td><bean:write name="item" property="field(cocr_ds_filtro)" /></td>
			</tr>
			<script>criteriosPublCondicoesForm.cocrNrSequencia.value = <%=i%>+1;</script>
			</logic:iterate>
			</logic:present>
		</tbody></table>
	</div>
</div>