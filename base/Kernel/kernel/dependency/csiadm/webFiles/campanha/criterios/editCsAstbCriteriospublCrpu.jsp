<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<div class="formtable">
	<form action="#" name="criteriosPublEditForm">
		<input type="hidden" name="id" value="<%=request.getParameter("id") %>" />
		<input type="hidden" name="idEmprCdEmpresa" value="<%=request.getParameter("idEmprCdEmpresa") %>" />
		<input type="hidden" name="idFuncCdFuncionario" value="<%=request.getParameter("idFuncCdFuncionario") %>" />
		<input type="hidden" name="condicoesViewState" />
		<input type="hidden" name="orderViewState" />
		
		<table>
			<tr>
				<td class="formlabel">C�digo</td>
				<td class="seta"></td>
				<td><input type="text" class="codigo text" name="idCrpuCdCriteriospubl" readonly="true" /></td>
			</tr>
			<tr>
				<td class="formlabel"><label for="crpuDsCriteriospubl">Descri��o</label></td>
				<td class="seta"></td> <!-- Chamado: 89652 - 19/07/2013 - Carlos Nunes -->
				<td><input type="text" class="text" name="crpuDsCriteriospubl" id="crpuDsCriteriospubl" maxlength="80"/></td>
			</tr>
			<tr>
				<td class="formlabel"><label for="crpuNrPrioridade">Prioridade</label></td>
				<td class="seta"></td>
				<td><input type="text" class="text numeric" name="crpuNrPrioridade" id="crpuNrPrioridade" maxlength="2" /></td>
			</tr>
			<tr>
				<td class="formlabel"><label for="crpuInInativo">Inativo</label></td>
				<td class="seta"></td>
				<td><input type="checkbox" name="crpuInInativo" id="crpuInInativo" value="S" />
					<input type="hidden" name="crpuDhInativo" />
				</td>
			</tr>
		</table>
	</form>
</div>

<div class="btnoperadores">
	<img src="/plusoft-resources/images/icones/Programa_Perfil.gif" /> <bean:message key="prompt.definirOperadores" />
</div>

<%-- <div class="btntotalcriterio">
	<img src="/plusoft-resources/images/botoes/Acao_Programa.gif" name="imgVisualizarTotalCriterio" id="imgVisualizarTotalCriterio" width="30" height="30" title="Visualizar Total Crit�rio"/>
	<bean:message key="prompt.totalDeRegitrosUtilizandoEsseCriterio" />
</div> --%>
	
<div class="tabframe detalhe margin">
	<div class="tabtabs">
		<ul>
			<li id="cond">Condi��es</li>
			<li id="ord">Ordena��o</li>
		</ul>
	</div>

	<div class="tabcontent">
		<div id="condtab">
			<jsp:include page="tabCsAstbCondicoescrpuCocr.jsp" />
		</div>
		<div id="ordtab">
			<jsp:include page="tabCsAstbOrdercrpuOrcr.jsp" />
		</div>

	</div>
</div>
<div class="divBotoes" id="divBotoes">
	<div class="btnsEspe" id="btnsEspec">
	</div>
</div>

<div class="divSetaDir" style="float: left; margin: 0;">
	<img id="setaLeft" src="/plusoft-resources/images/botoes/esq.gif" style="cursor: pointer;">
	<img id="setaRight" src="/plusoft-resources/images/botoes/dir.gif" style="cursor: pointer;">
</div>	