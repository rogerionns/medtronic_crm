<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>


<style type="text/css">
	.subcampanha { width: 240px; }
	.microestrategia { width: 375px; }
	.publicototal { width: 170px; }
	
	.separador {margin-top: 15px; width: 100%;}
	
	.mg {margin: 1px;}
	
	.database { width: 90px; }
	.desconto { width: 150px; }
	.desc  { width: 50px; margin-right: 5px}
	.gruporegra { width: 175px; }
	.acao { width: 200px; }
	.boleto { width: 170px; }
	.qtd { width: 50px; float: left;  }
	.email { width: 250px; float: left;}
	
	.frame  { height: 400px; padding: 10px; width: 97%;}


	.envioEmail {width: 120px; float: right; vertical-align: middle;}
	.testarEnvio {width: 150px; float: right; vertical-align: middle;}
	.gerarBoletagem {cursor: pointer; width: 130px; float: right; vertical-align: middle;}
	div.gerarBoletagem img { vertical-align: middle; }
	div.testarEnvio img { vertical-align: middle; }
	
	
	.list td.fix40Center 	{ width: 40px; text-align: center; }
	.list td.fix40 			{ width: 40px; text-align: left;}
	.list td.fix60Center 	{ width: 60px; text-align: center; }
	.list td.fix60 			{ width: 60px; text-align: left;}
	.list td.fix70Center 	{ width: 70px; text-align: center; }
	.list td.fix70 			{ width: 70px; text-align: left;}
	.list td.fix60Right 	{ width: 60px; text-align: right;}
	.list td.fix80Center 	{ width: 80px; text-align: center; }
	.list td.fix80Right		{ width: 80px; text-align: right; }
	.list td.fix80 			{ width: 80px; text-align: left;}
	.list td.fix90Center 	{ width: 90px; text-align: center; }
	.list td.fix90Right		{ width: 90px; text-align: right; }
	.list td.fix90 			{ width: 90px; text-align: left;}
	.list td.fix95Center 	{ width: 95px; text-align: center; }
	.list td.fix95 			{ width: 95px; text-align: left;}
	.list td.fix100Center 	{ width: 100px; text-align: center; }
	.list td.fix100	 		{ width: 100px; text-align: left;}
	.list td.fix110Center 	{ width: 110px; text-align: center; }
	.list td.fix110	 		{ width: 110px; text-align: left;}
	.list td.fix120Center 	{ width: 120px; text-align: center; }
	.list td.fix120		 	{ width: 120px; text-align: left;}
	.list td.fix130		 	{ width: 130px; text-align: left;}
	.list td.fix130Center 	{ width: 130px; text-align: center;}
	.list td.fix140Center 	{ width: 140px; text-align: center; }
	.list td.fix140 		{ width: 140px; text-align: left;}
	.list td.fix200Center 	{ width: 200px; text-align: center; }
	.list td.fix200			{ width: 200px; text-align: left;}
	.list td.valor			{ width: 120px; text-align: right; }
	.list td.image			{ width: 20px; text-align: right; }
	.list td.imageCab		{ width: 35px; text-align: center; }
	
	a.carta  { width: 35px; height: 20px; background-image: url(/plusoft-resources/images/icones/carta.gif); }
	
	.listaHistorico  {margin-left: 5px; width: 790px;}
	.scrolltablehist  { height: 150px; }
	
	.borda {border: 1px solid #7ea4c2;}
	.title2		{ background-color: #7ea4c2; border-top-left-radius: 4px; border-bottom-right-radius: 5px; width: 160px; font-weight: bold; line-height: 18px; text-align: center; color: #ffffff; margin: -1px; margin-bottom: 0px; cursor: default; }
</style>

</head>

<body class="margin topmargin noscroll">

	<div class="frame shadow pgnBoletagem">
		<div class="title"><plusoft:message key="prompt.Boletagem" /></div>
		

		<div class="label subcampanha mg" style="float:left"><plusoft:message key="prompt.subcampanha" /></div>
		<div class="label microestrategia mg" style="float:left"><plusoft:message key="prompt.MicroEstrategia" /></div>
		<div class="label publicototal mg" style="float:left"><plusoft:message key="prompt.PublicoTotal" /></div>
		
		<div style="float:left">
			<input type="text" class="subcampanha mg" id="publDsPublico" readonly="true" value="<%=request.getAttribute("publDsPublico")%>"/>
		</div>
		<div style="float:left">
			<input type="text" class="microestrategia mg" id="crpuDsCriteriospubl" readonly="true" value="<%=request.getAttribute("crpuDsCriteriospubl")%>"/>
		</div>	
		<div style="float:left">
			<input type="text" class="publicototal mg" id="pubTotal" name="pubTotal" readonly="true" value="<%=request.getAttribute("pubTotal")%>"/>
		</div>
		
		<div class="separador">&nbsp;</div>
		
		<div class="label database mg" style="float:left"><plusoft:message key="prompt.DataBase" /></div>
		<div class="label desconto mg" style="float:left"><plusoft:message key="prompt.Desconto" /></div>
		<div class="label gruporegra mg" style="float:left"><plusoft:message key="prompt.GrupoRegraNegociacao" /></div>
		<div class="label acao mg" style="float:left"><plusoft:message key="prompt.acao" /></div>
		<div class="label boleto mg" style="float:left"><plusoft:message key="prompt.Boleto" /></div>
		
		<div style="float:left">
			<input type="text" class="left database datab mg" maxlength="10" id="db" />
		</div>
		<div class="desconto" style="float:left;">
			<input type="text" class="left desc mg" id="textPercentualDesc" maxlength="3" />
			<plusoft:message key="prompt.PercDoPermitido" />
		</div>
		<div style="float:left">
			<select class="idGrreCdGruporene gruporegra mg">
				<option value="">-- Selecione uma op��o--</option>
				<logic:present name="cbCdtbGruporeneGrreVector"><logic:iterate id="reneVo" name="cbCdtbGruporeneGrreVector">
				<option value="<bean:write name="reneVo" property="field(id_grre_cd_gruporene)" />"><bean:write name="reneVo" property="field(grre_ds_gruporene)" /></option>
				</logic:iterate></logic:present>
			</select>
		</div>
		<div style="float:left">
			<select class="idAccoCdAcaocob acao mg">
				<option value="">-- Selecione uma op��o--</option>
				<logic:present name="cbCdtbAcaocobAccoVector"><logic:iterate id="acaoVo" name="cbCdtbAcaocobAccoVector">
				<option value="<bean:write name="acaoVo" property="field(id_acco_cd_acaocob)" />"><bean:write name="acaoVo" property="field(acco_ds_acaocob)" /></option>
				</logic:iterate></logic:present>
			</select>
		</div>
		<div style="float:left">
			<select class="idInboCdInfoboleto boleto mg">
				<option value="">-- Selecione uma op��o--</option>
				<logic:present name="cbCdtbInfoboletoInboVector"><logic:iterate id="inboVo" name="cbCdtbInfoboletoInboVector">
				<option value="<bean:write name="inboVo" property="field(id_inbo_cd_infoboleto)" />"><bean:write name="inboVo" property="field(inbo_ds_nome)" /></option>
				</logic:iterate></logic:present>
			</select>
		</div>
		
		<br>
		<br>
		<div class="separador">&nbsp;</div>
		
		<div class="gerarBoletagem">
			<span style="cursor:pointer"><img src="/plusoft-resources/images/botoes/Acao_Programa.gif" title="<plusoft:message key="prompt.gerarBoletagem" />" />
			<plusoft:message key="prompt.gerarBoletagem" /></span>
		</div>
		
		<div class="testarEnvio">
			<span style="cursor:pointer"><img src="/plusoft-resources/images/botoes/testar_boletagem.png" title="<plusoft:message key="promtp.testarBoletagem" />" />
			<plusoft:message key="promtp.testarBoletagem" /></span>
		</div>
		
		<div class="envioEmail">
			<input type="checkbox" id="txtEnviaEmail" name="txtEnviaEmail" value="S">
			<plusoft:message key="promtp.enviarEmail" />
		</div>
		
		<br>
		<br>
		<br>
		
		<div class="borda">
			<div class="title2"><plusoft:message key="prompt.Historico" /></div>
			<div class="separador">&nbsp;</div>
			<div class="list borda listaHistorico">
				<table>
					<thead>
					
						<tr>
							<td class="fix90Center">&nbsp;<plusoft:message key="prompt.dataAcao"/></td>
							<td class="fix60Center">&nbsp;<plusoft:message key="prompt.qtd"/></td>
							<td class="fix110"><plusoft:message key="prompt.usuario"/></td>
							<td class="fix60Center"><plusoft:message key="prompt.DataBase"/></td>
							<td class="fix70Center"><plusoft:message key="prompt.Desc"/></td>
							<td class="fix110"><plusoft:message key="prompt.grupoRegra"/></td>
							<td class="fix110"><plusoft:message key="prompt.acao"/></td>
							<td class="fix110"><plusoft:message key="prompt.Boleto"/></td>
							<td>&nbsp;</td>
						</tr>
				
				
					</thead>
				</table>
				
				<div class="scrolllist scrolltablehist historicobol"></div>
			</div>
			<br>
		</div>
	</div>
	
	<div class="popup" style="display: none;">
		<div class="qtd"><plusoft:message key="prompt.qtd" /></div>
		<div class="email"><plusoft:message key="prompt.email" /></div>
		<div><input type="text" id="txtQtd" name="txtQtd" maxlength="3" class="qtd"></div>
		<div><input type="text" id="txtEmail" name="txtEmail" class="email"></div>
	</div>
	
	<div class="popupdet noscroll" style="display: none; overflow: hidden;">
		<div class="frameEspec lblDetExecucao" style="overflow: hidden;"></div>
	</div>
	
	<html:form styleId="criteriosSubCampanhaForm">
		<html:hidden property="voViewState" />
	</html:form>

<script>
	//define a largura do div interno (scroll) de acordo com os tamanhos informados na cria��o dos bot�es
	$('.btnsEspe').width(largura);
</script>
</body>
