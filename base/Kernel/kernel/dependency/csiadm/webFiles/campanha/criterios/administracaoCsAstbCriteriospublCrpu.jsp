<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<form action="#" name="criteriosPublProcurarForm">
<div class="formrow">
	<input type="hidden" name="idCocrCdCondicoescrpu" id="idCocrCdCondicoescrpu" />

	<table width="600px">
		<thead>
		<tr>
			<td><label for="idTadbCdTabeladb">Procurar</label></td>
			<td width="40px">&nbsp;</td>
		</tr>
		</thead>
		<tr>
			<td>
				<input type="text" class="text" name="filtro" id="filtro" maxlength="1000" />
			</td>
			<td width="40px"><a href="#" class="setadown" id="filtrar" title="Filtrar"></a></td>
		</tr>
	</table>
</div>
</form>
<hr/>

<div class="list margin" id="crpulist">
	<table>
		<thead>
			<tr>
				<td class="sph">&nbsp;</td>
				<td class="codigo">C�d.</td>
				<td>Descri��o</td>
				<td class="date">Prioridade</td>
				<td class="date">Operadores</td>
				<td class="date">Inativo</td>
				<td class="sph">&nbsp;</td>
			</tr>
		</thead>
	</table>
	
	<div class="scrolllist">
		<table><tbody>
			<logic:present name="crpuList"><logic:iterate name="crpuList" id="crpu">
			<tr>
				<td crpu="<bean:write name="crpu" property="field(id_crpu_cd_criteriospubl)" />" class="image"><a href="#" class="lixeira" title="Excluir"></a></td>
				<td crpu="<bean:write name="crpu" property="field(id_crpu_cd_criteriospubl)" />" class="codigo clickable"><bean:write name="crpu" property="field(id_crpu_cd_criteriospubl)" /></td>
				<td crpu="<bean:write name="crpu" property="field(id_crpu_cd_criteriospubl)" />" class="clickable"><bean:write name="crpu" property="field(crpu_ds_criteriospubl)" /></td>
				<td crpu="<bean:write name="crpu" property="field(id_crpu_cd_criteriospubl)" />" class="date clickable"><bean:write name="crpu" property="field(crpu_nr_prioridade)" /></td>
				<td crpu="<bean:write name="crpu" property="field(id_crpu_cd_criteriospubl)" />" class="date clickable"><bean:write name="crpu" property="field(crpu_nr_cntoperadores)" /></td>
				<td crpu="<bean:write name="crpu" property="field(id_crpu_cd_criteriospubl)" />" class="date clickable"><bean:write name="crpu" property="field(crpu_dh_inativo)" /></td>
			</tr>
			</logic:iterate></logic:present>
		</tbody></table>
	</div>
</div>