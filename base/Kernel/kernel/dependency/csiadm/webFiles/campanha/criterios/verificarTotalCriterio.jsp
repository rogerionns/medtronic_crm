<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<!-- Chamado: 80755 - Carlos Nunes - 02/02/2012 -->

<html:html>
<head>

<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<style type="text/css">
	.formrow { text-align: left;  font-style: normal; margin-left: 5px;}	
	.conteudaSql { text-align: left; font-style: normal; margin-left: 5px; }
</style>

</head>

<body>
	<div id="conteudo" class="formrow" style="display: block;">
		<label for="totalRegistrosAgendados">&nbsp;Registros Agendados: &nbsp;<b><bean:write name="retornoVo" property="field(agendado)"/></b></label><br>
		<label for="totalRegistrosNAgendados">&nbsp;Registros N�o Agendandos: &nbsp;<b><bean:write name="retornoVo" property="field(naoAgendado)"/></b></label><br>
		<br>
		<label for="totalRegistros">&nbsp;Total de registros: &nbsp;<b><bean:write name="retornoVo" property="field(total)"/></b></label><br>
		<label for="totalTempo">&nbsp;Total de tempo: &nbsp;<b><bean:write name="retornoVo" property="field(tempo)"/>&nbsp;Segundos</b></label>
		<p></p>
		<p></p>
		<input type="checkbox" class="mostrarSql"/>&nbsp;Mostrar SQL utilizado
	</div>
	<div style="clear:left;height: 20px;">&nbsp;</div>
	<div style="clear:left;width:98%; display: block;" class="conteudaSql" id="query">
		<p>
			Busca por agendados
			<br/>
			<br/>
			<bean:write name="retornoVo" property="field(select_a)"/>
			<br/>
			<br/>
			<br/>
			<br/>
			Busca por nao agendados
			<br/>
			<br/>
			<bean:write name="retornoVo" property="field(select_n)"/>
		</p>
	</div>
	
	<%-- <div id="divSair" style="position:absolute; top:150px; left:100px; display: none;">
		<p><a href="#" class="btnSair" title="<plusoft:message key="prompt.sair" />"><img src="/plusoft-resources/images/botoes/out.gif" class="left" />
		<bean:message key="prompt.sair"/></a></p>
	</div> --%>
</body>

<script>
	
	if('<bean:write name="retornoVo" property="field(antecipaagendados)"/>' == 'true'){
		document.getElementById("divBtnAntecipa").style.display = "none";
		document.getElementById("query").style.display = "none";
		document.getElementById("conteudo").innerHTML = '<bean:write name="retornoVo" property="field(mensagem)"/>';
	}
	
	
</script>

</html:html>