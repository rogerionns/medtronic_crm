<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsCdtbPublicoPublForm" action="/AdministracaoCsCdtbPublicoPubl.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="erro" />
	<html:hidden property="topicoId"/>
	<html:hidden property="idPublCdPublico"/>

<script>var possuiRegistros=false;</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="sortable">
  <logic:iterate id="ccttrtVector" name="csCdtbPublicoPublVector" indexId="sequencia">
  <script>possuiRegistros=true;</script>
  <tr> 
    <td width="3%" class="principalLstPar">
      <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="idPublCdPublico" />')">
    </td>

    <td width="3%" class="principalLstParMao"> 
    	<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMAPUBL_IDPU.xml','<bean:write name="ccttrtVector" property="idPublCdPublico" />')">
    </td>
    
    <td class="principalLstParMao" width="12%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPublCdPublico" />')">
      <bean:write name="ccttrtVector" property="idPublCdPublico" />
    </td>
    <td class="principalLstParMao" align="left" width="32%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPublCdPublico" />')">
      <%=acronymChar(((br.com.plusoft.csi.adm.vo.CsCdtbPublicoPublVo)ccttrtVector).getCsCdtbCampanhaCampVo().getCampDsCampanha(), 29)%>
	</td>    
    <td class="principalLstParMao" align="left" width="24%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPublCdPublico" />')">
      <%=acronymChar(((br.com.plusoft.csi.adm.vo.CsCdtbPublicoPublVo)ccttrtVector).getPublDsPublico(), 28)%>
	</td>
	<td sorttable_customkey="<plusoft:format name="ccttrtVector" property="publDhInativo" inputformat="DD/MM/YYYY HH:mm:SS" outputformat="YYYYMMDDHHmmSS" />" class="principalLstParMao" align="center" width="25%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idPublCdPublico" />')">
	&nbsp;
      <bean:write name="ccttrtVector" property="publDhInativo" />
      <script>
      	if('<bean:write name="ccttrtVector" property="publDhInativo" />' == '')
      	{
          	document.write('<font color="#f4f4f4">00/00/0000</font>');
      	}
      </script>
	</td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_SUBCAMPANHA_EXCLUSAO_CHAVE%>', editCsCdtbPublicoPublForm.lixeira);
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>