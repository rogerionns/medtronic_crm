<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposProdutovenda(){
		document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.prodDsProduto"].disabled= true;	
		document.administracaoCsCdtbProdutovendaProdForm["inInativo"].disabled= true;
		document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.csCdtbCategoriaprodCaprVo.idCaprCdCategoriaprod"].disabled= true;
		document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.prodDsCompl1"].disabled= true;
		document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.prodDsCompl2"].disabled= true;
		document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.prodDsCompl3"].disabled= true;
		document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.prodDsCompl4"].disabled= true;
		document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.prodDsCompl1"].disabled= true;
	}
	
	function inicio(){
		//setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbProdutovendaProdForm['csCdtbProdutovendaProdVo.idProdCdProduto'].value);
	}
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbProdutovendaProdForm" action="/AdministracaoCsCdtbProdutovendaProd.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>		

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="csCdtbProdutovendaProdVo.idProdCdProduto" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="csCdtbProdutovendaProdVo.prodDsProduto" styleClass="text" maxlength="40" /> 
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.categoriaProduto"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
			<td colspan="2"> 
			  <html:select property="csCdtbProdutovendaProdVo.csCdtbCategoriaprodCaprVo.idCaprCdCategoriaprod" styleClass="principalObjForm" onchange=""> 
			  	<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/></html:option> 
			  	<html:options collection="csCdtbCategoriaprodCaprVector" property="idCaprCdCategoriaprod" labelProperty="caprDsCategoriaprod"/>
			  </html:select>
			</td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.produtoVenda.compl1"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="csCdtbProdutovendaProdVo.prodDsCompl1" styleClass="text" maxlength="30" /> 
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.produtoVenda.compl2"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="csCdtbProdutovendaProdVo.prodDsCompl2" styleClass="text" maxlength="30" /> 
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.produtoVenda.compl3"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="csCdtbProdutovendaProdVo.prodDsCompl3" styleClass="text" maxlength="30" /> 
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.produtoVenda.compl4"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="csCdtbProdutovendaProdVo.prodDsCompl4" styleClass="text" maxlength="30" /> 
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.produtoVenda.compl5"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="csCdtbProdutovendaProdVo.prodDsCompl5" styleClass="text" maxlength="30" /> 
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="15%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="inInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposProdutovenda();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_PRODUTOVENDA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbProdutovendaProdForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_PRODUTOVENDA_INCLUSAO_CHAVE%>')){
				desabilitaCamposProdutovenda();
			}else{
				document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.idProdCdProduto"].disabled= false;
				document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.idProdCdProduto"].value= '';
				document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.idProdCdProduto"].disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_PRODUTOVENDA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_PRODUTOVENDA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbProdutovendaProdForm.imgGravar);	
				desabilitaCamposProdutovenda();
			}
		</script>
</logic:equal>


<script>
	try{
		document.administracaoCsCdtbProdutovendaProdForm["csCdtbProdutovendaProdVo.prodDsProduto"].focus();
	}
	catch(ex){}
</script>	

</html>