<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title>..: <bean:message key="prompt.detalhes" /> :.. </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../webFiles/css/global.css" type="text/css">
</head>

<script>
     
    var wi = (window.dialogArguments?window.dialogArguments:window.opener);
   
	function carregaTexto(){
		if(wi.objDetalhe == undefined){
			wi.value = document.getElementById("txtDescricao").value;
		}else{
			wi.objDetalhe.value = document.getElementById("txtDescricao").value;
		}
		
		window.close();
	}

	function inicio(){
		if(wi.objDetalhe == undefined){
			texto = wi.value;
		}else{
			texto = wi.objDetalhe.value;
		}
		document.getElementById("txtDescricao").value = texto;
	}
</script>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0"  onload="inicio()">
<form action="">
<div style="width: 800px; overflow: auto;height: 570px">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <textarea name="txtDescricao" id="txtDescricao" styleClass="pOF3D" rows="34" cols="300" ></textarea>
    </td>
  </tr>
</table>
</div>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> <img src="webFiles/images/botoes/gravar.gif" width="25" height="25" border="0" title="<bean:message key="prompt.gravar"/>" onClick="carregaTexto()" class="geralCursoHand"></td>
          <td> <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>          
        </tr>
      </table>
    </td>
  </tr>
</table>

</form>
</body>
</html>