<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<%
response.setContentType("text/html; charset=iso8859-1");
%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title>Pesquisa - WEB</title>
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">   
<script type="text/javascript" src="webFiles/funcoes/TratarDados.js"></script>
<script language="JavaScript" src="webFiles/funcoes/pt/validadata.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/resources/js/variaveis.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

var nIndAnexo = 0; 

function AtivarPasta(pasta)
{
switch (pasta)
{
case 'DADOS':
	MM_showHideLayers('dados','','show','perguntas','','hide')
	SetClassFolder('tdDados','principalPstQuadroLinkSelecionadoMAIOR');
	SetClassFolder('tdperguntas','principalPstQuadroLinkNormalMAIOR');
	break;

case 'PERGUNTAS':
	MM_showHideLayers('dados','','hide','perguntas','','show')
	SetClassFolder('tdDados','principalPstQuadroLinkNormalMAIOR');
	SetClassFolder('tdperguntas','principalPstQuadroLinkSelecionadoMAIOR');
	break;
	
}
 eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}

function retornaNomeArquivo(cFullArquivo) {
	var nIndice = cFullArquivo.lastIndexOf('\\');
	var cNome = cFullArquivo.substring(++nIndice);

	return cNome;
}

function inicio(){
	<logic:present name="gravacaoOK">
		alert('<bean:message key="prompt.dados_gravados_com_sucesso"/>');
		window.close();
	</logic:present>
	
	if(window.innerHeight != undefined){
		document.getElementById('div_main').style.height = window.innerHeight;	
	}else{
		document.getElementById('div_main').style.height = document.body.clientHeight;
	}
	
	
}
function inserirAnexo(){
	if(document.forms[0].anpe_bl_arquivo.value == ''){
		alert('<bean:message key="prompt.arquivoObrigatorio"/>');
		return;
	}	
	var cNome = retornaNomeArquivo(document.forms[0].anpe_bl_arquivo.value);
	for (var nTD=0;nTD<nIndAnexo;nTD++) {
		if (document.getElementById("linkAnexo"+nTD)!=undefined) { // AdequacaoSafari - 12/12/2013 - Jaider Alba
			if (trim(document.getElementById("linkAnexo"+nTD).innerHTML)==trim(cNome)) {
				alert('<bean:message key="prompt.arquivo.ja.existente"/>')
				return;
			}
		}
	}
	document.forms[0].action = 'InserirAnexo.do';
	document.forms[0].target = this.name = 'teste';
	document.forms[0].submit();
}

function removerAnexo(indice){
	if(confirm('<bean:message key="prompt.removerAnexo"/>')){
		document.forms[0].action = 'RemoverAnexo.do';
		document.forms[0].indiceAnexo.value = indice;
		document.forms[0].target = this.name = 'teste';
		document.forms[0].submit();
	}		
}

function gravar(){
	
	if(trim(document.forms[0].sipe_ds_titulo.value)==''){
		alert('<bean:message key="prompt.descricao.site.obrigatorio"/>');
		return;
	}

	document.forms[0].target = this.name = 'teste';
	document.forms[0].submit();
}

function abrirPreview(pesq, hipe){
	// 90467 - 12/09/2013 - Jaider Alba
	showModalDialog('../../../ChatWEB/AbrirQuestionarioWEB.do?preview=S&idPessCdPessoa=1&idIdioCdIdioma=1&idEmprCdEmpresa=1&idPesqCdPesquisa=' + pesq + '&hipe_nr_sequencial=' + hipe,window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
}

var objDetalhe = null;
function abreMaior(campo){
	objDetalhe = campo;
	showModalDialog('AbrirDetalhes.do',campo,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
}

function sair(){
	window.close();
}

// 90615 - 13/09/2013 - Jaider Alba
function downloadAnexo(idPesqCdPesquisa,nomeAnexo){
	
	var wi = (window.dialogArguments?window.dialogArguments:window.opener);
	idEmprCdEmpresa = wi.window.top.ifrmCmbEmpresa.document.getElementById("administracaoEmpresaForm").idEmprCdEmpresa.value;
	
	window.open('../../../ChatWEB/RecuperaResourcesQuestionario.do?hise='+
			'&idPesqCdPesquisa=' + idPesqCdPesquisa + 
			'&idEmprCdEmpresa=' + idEmprCdEmpresa + 
			'&anexo=' + nomeAnexo);	
	
	
}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="inicio();" >

<html:form action="SalvarSite" enctype="multipart/form-data">
<html:hidden property="anexosViewState"/>
<html:hidden property="historicoViewState"/>
<html:hidden property="indiceAnexo"/>
	
<div id="div_main" style="overflow: auto;width: 100%;">
	  
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
         <tr> 
           <td>&nbsp;</td>
         </tr>
       </table>
       <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
         <tr> 
           <td width="70%" align="center"> 
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="100%"><bean:message key="prompt.codigo"/></td>                 
               </tr>
               <tr>
                 <td class="principalLabel" width="100%"> 
                   <html:text property="id_pesq_cd_pesquisa" styleClass="principalObjForm" readonly="true" style="width: 100px"></html:text>
                 </td>                 
               </tr>
             </table>
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="50%"><bean:message key="prompt.tituloJanela"/></td> 
                 <td class="principalLabel" width="50%"><bean:message key="prompt.tipoQuestionario"/></td>                                 
               </tr>
               <tr>
                 <td class="principalLabel" width="50%"> 
                   <html:text property="sipe_ds_titulo" styleClass="principalObjForm" maxlength="60" readonly="false" style="width:400"></html:text>
                 </td>                 
                 <td class="principalLabel" width="50%"> 
                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		               <tr>
		                 <td class="principalLabel" width="150">
		                 	<html:radio property="sipe_in_tpquestionario" value="C" style="width: 30px"></html:radio> Cont�nuo (Scroll)
		                 </td>                 
		                 <td class="principalLabel" width="200">
		                 	<html:radio property="sipe_in_tpquestionario" value="Q" style="width: 30px"></html:radio> Por Quest�o
		                 </td>
		               </tr>		              
		             </table>
                 </td>  
               </tr>
             </table>
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="100%"><bean:message key="prompt.background"/></td>                 
               </tr>
               <tr>
                 <td class="principalLabel" width="100%"> 
                   <html:textarea property="sipe_tx_background" styleClass="principalObjForm" rows="5" style="width: 750px"></html:textarea>
                   <img src="../csicrm/webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="abreMaior(document.forms[0].sipe_tx_background);" title="<bean:message key='prompt.visualizar'/>" >
                 </td>                 
               </tr>
             </table>
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="100%"><bean:message key="prompt.introducao"/></td>                 
               </tr>
               <tr>
                 <td class="principalLabel" width="100%"> 
                   <html:textarea property="sipe_tx_introducao" styleClass="principalObjForm" rows="5" style="width: 750px"></html:textarea>
                   <img src="../csicrm/webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="abreMaior(document.forms[0].sipe_tx_introducao);" title="<bean:message key='prompt.visualizar'/>" >
                 </td>                 
               </tr>
             </table>
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="100%"><bean:message key="prompt.questionario"/></td>                 
               </tr>
               <tr>
                 <td class="principalLabel" width="100%"> 
                   <html:textarea property="sipe_tx_questionario" styleClass="principalObjForm" rows="5" style="width: 750px"></html:textarea>
                   <img src="../csicrm/webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="abreMaior(document.forms[0].sipe_tx_questionario);" title="<bean:message key='prompt.visualizar'/>" >
                 </td>                 
               </tr>
             </table>
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="100%"><bean:message key="prompt.conclusao"/></td>                 
               </tr>
               <tr>
                 <td class="principalLabel" width="100%"> 
                   <html:textarea property="sipe_tx_conclusao" styleClass="principalObjForm" rows="5" style="width: 750px"></html:textarea>
                   <img src="../csicrm/webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="abreMaior(document.forms[0].sipe_tx_conclusao);" title="<bean:message key='prompt.visualizar'/>" >
                 </td>                 
               </tr>
             </table>
           </td>           
         </tr>
       </table>
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
         <tr> 
           <td>&nbsp;</td>
         </tr>
       </table>       
       <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="150">
         <tr>
           <td valign="top">
           		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
				    <td class="principalPstQuadroLinkSelecionadoMAIOR" name="tdDados" id="tdDados" onClick="AtivarPasta('DADOS')"><bean:message key="prompt.anexos"/></td>
				    <td class="principalPstQuadroLinkNormalMAIOR" name="tdPerguntas" id="tdPerguntas" onClick="AtivarPasta('PERGUNTAS')"><bean:message key="prompt.historico"/></td>
				    <td class="principalLabel">&nbsp;</td>
				  </tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" height="80">
				  <tr> 
				    <td valign="top"> 
				      <div id="dados" style="width:97%; height:80px;overflow: auto;display: block"> 
				        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			             <tr>
			               <td class="principalLabel" width="5%"><bean:message key="prompt.arquivo"/></td>
			               <td class="principalLabel" width="60%"><html:file property="anpe_bl_arquivo" styleClass="principalObjForm"/></td>
			               <td class="principalLabel" width="35%"><img src="webFiles/images/botoes/setaDown.gif" class="geralCursoHand" title="<bean:message key='prompt.incluir'/>" onclick="inserirAnexo();"/></td>                     
			             </tr>
			            </table>
			            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			             <tr>
			               <td class="principalLstCab" width="5%">&nbsp;</td>
			               <td class="principalLstCab" width="95%"><bean:message key="prompt.arquivo"/></td>			                                    
			             </tr>			             
			             <tr>
			               <td class="principalLabel" width="100%" colspan="2"> 
			                 <div style="overflow: auto;height:45px">
			                 	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			                 	 <logic:present name="anexosViewState">
			                 	 	<logic:iterate name="anexosViewState" id="ansi" indexId="indice">
						             <tr>
						               <td class="principalLabel" width="5%"><img src="webFiles/images/botoes/lixeira.gif" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="removerAnexo('<bean:write name="indice"/>')"/></td>
						               <td class="principalLabel" width="95%" id="tdAnexo<bean:write name="indice"/>">
						               		<!-- 90615 - 13/09/2013 - Jaider Alba  -->						               		
						               		<a id="linkAnexo<bean:write name="indice"/>" href="#" onclick="javascript:downloadAnexo('<bean:write name="ansi" property="field(id_pesq_cd_pesquisa)" />', '<bean:write name="ansi" property="field(anpe_ds_anexossitepesq)"/>');" style="float:left;">
						               			<bean:write name="ansi" property="field(anpe_ds_anexossitepesq)"/>
					               			</a>
				               			</td>					                                    
						             </tr>
			                 	 	 <script>++nIndAnexo</script>
						            </logic:iterate>
					             </logic:present>
					            </table>
			                 </div>
			               </td>                 
			             </tr>
			           </table>
				      </div>
				      <div id="perguntas" style="width:97%; height:80px; z-index:1; display: none"> 
				        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			             <tr>
			               <td class="principalLstCab" width="15%"><bean:message key="prompt.tituloJanela"/></td>
			               <td class="principalLstCab" width="15%"><bean:message key="prompt.dataHistorico"/></td>
			               <td class="principalLstCab" width="15%"><bean:message key="prompt.funcionario"/></td>
			               <td class="principalLstCab" width="15%"><bean:message key="prompt.preview"/></td>
			               <td class="principalLstCab" width="8%"><bean:message key="prompt.rollback"/></td>
			               <td class="principalLstCab" width="2%">&nbsp;</td>				               			                                    
			             </tr>			             
			             <tr>
			               <td class="principalLabel" width="100%" colspan="8"> 
			                 <div style="height:60px; overflow: auto">
			                 	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			                 	 <logic:present name="historicoViewState">
			                 	 	<logic:iterate name="historicoViewState" id="hisc">
						             <tr>
						               <td class="principalLabel" width="15%"><bean:write name="hisc" property="field(sipe_ds_titulo)"/></td>
						               <td class="principalLabel" width="15%"><bean:write name="hisc" property="field(hipe_dh_historico)" format="dd/MM/yyyy HH:mm:ss"/></td>
						               <td class="principalLabel" width="15%"><bean:write name="hisc" property="field(func_nm_funcionario)"/></td>
						               <td class="principalLabel" width="15%"><img src="../../csicrm/webFiles/images/botoes/agendamentos.gif" onclick="abrirPreview('<bean:write name="hisc" property="field(id_pesq_cd_pesquisa)"/>', '<bean:write name="hisc" property="field(hipe_nr_sequencial)"/>')"/></td>
						               <td class="principalLabel" width="10%"><img src="../../csicrm/webFiles/images/botoes/agendamentos.gif"/></td>
						             </tr>
						            </logic:iterate>
					             </logic:present>
					            </table>
			                 </div>
			               </td>                 
			             </tr>
			           </table>
				      </div>
				    </td>
				  </tr>
				</table>
				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		         <tr>
					<td class="principalLabel" width="95%" align="right"><img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="gravar();"></td>                 
					<td width="05%" align="right"><img src="webFiles/images/botoes/out.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.sair'/>" onclick="sair()"></td>
		         </tr>         
		       </table>
           
           </td>
         </tr>
       </table>
       
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
         <tr> 
           <td>&nbsp;</td>
         </tr>
       </table>
      
</html:form>
</div>
</body>
</html>
<script type="text/javascript" src="webFiles/funcoes/funcoesMozilla.js"></script>