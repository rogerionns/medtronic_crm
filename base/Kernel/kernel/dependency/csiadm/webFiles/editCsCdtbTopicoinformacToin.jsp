<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposTopicoinformac(){
		document.administracaoCsCdtbTopicoinformacToinForm.toinDsTopicoinformacao.disabled= true;
		document.administracaoCsCdtbTopicoinformacToinForm.toinDhInativo.disabled= true;
	}	
	
	function inicio(){
		setaChavePrimaria(administracaoCsCdtbTopicoinformacToinForm.idToinCdTopicoinformacao.value);
	}	
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio()">

<html:form styleId="administracaoCsCdtbTopicoinformacToinForm" action="/AdministracaoCsCdtbTopicoinformacToin.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />	
	<html:hidden property="inativarComp"/>

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idToinCdTopicoinformacao" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="toinDsTopicoinformacao" styleClass="text" maxlength="60" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
          			<td width="10%" align="left"><html:checkbox property="toinDhInativo" value="true"/></td>
          			<td width="90%"class="principalLabel">&nbsp;<bean:message key="prompt.inativo"/></td>
          		</tr>
          	</table>
          </td>  
          <td>&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposTopicoinformac();	
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_TOPICODEINFORMACAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbTopicoinformacToinForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_TOPICODEINFORMACAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposTopicoinformac();
			}else{
				document.administracaoCsCdtbTopicoinformacToinForm.idToinCdTopicoinformacao.disabled= false;
				document.administracaoCsCdtbTopicoinformacToinForm.idToinCdTopicoinformacao.value= '';
				document.administracaoCsCdtbTopicoinformacToinForm.idToinCdTopicoinformacao.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_TOPICODEINFORMACAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_TOPICODEINFORMACAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbTopicoinformacToinForm.imgGravar);	
				desabilitaCamposTopicoinformac();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbTopicoinformacToinForm.toinDsTopicoinformacao.focus();}
	catch(e){}
</script>