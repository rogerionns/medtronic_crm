<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>

<script>
function atualizaProduto() {
	document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%=MAConstantes.TELA_CMB_PRODUTO_INFO %>';
	document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmCmbProdutoInfo.name;
	document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	
	/*document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%=MAConstantes.TELA_CMB_TIPO %>';
	document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmCmbTipo.name;
	document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	
	document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%=MAConstantes.TELA_CMB_TOPICO %>';
	document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmCmbTopico.name;
	document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	
	document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%=MAConstantes.TELA_TEXTO_INFORMACAO %>';
	document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmTextoInformacao.name;
	document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	*/
}

function iniciaTela(){
	if(document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.length == 2){
		document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.selectedIndex = 1;
	}
	
	atualizaProduto();
	
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/AdministracaoCsCdtbAtendpadraoAtpa.do" styleId="administracaoCsCdtbAtendpadraoAtpaForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idTpinCdTipoinformacao" />
  <html:hidden property="idToinCdTopicoinformacao" />
  <html:hidden property="idAsnCdAssuntoNivel" />
  <html:hidden property="idAsn2CdAssuntonivel2" />
  <html:hidden property="idAsn1CdAssuntonivel1" />
  <html:hidden property="prasInProdutoassunto" />
  
  <html:select property="idLinhCdLinha" styleClass="principalObjForm" onchange="atualizaProduto()">
	<logic:present name="combo">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
      <html:options collection="combo" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
    </logic:present>
  </html:select>
</html:form>
</body>
</html>