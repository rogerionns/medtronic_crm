<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>
	function VerificaCampos(){
		if (document.administracaoCsCdtbServicoServForm.servNrTempo.value == "0" ){
			document.administracaoCsCdtbServicoServForm.servNrTempo.value = "" ;
		}
	}

	//A��es do bot�o para adicionar hora marcada
	function acaoAdicionarHora(){
		if(document.getElementById("txtHoraMarcada").value == ""){
			alert("<bean:message key="prompt.alert.Informe_o_horario" />");
			document.getElementById("txtHoraMarcada").focus();
		}
		else{
			adicionarHoraMarcada(document.getElementById("txtHoraMarcada").value);
			document.getElementById("txtHoraMarcada").value = "";
		}
	}

	//Vetor para armazenas os agendamentos de horas
	var horasMarcadas = new Array();
	var nHorasMarcadas = 0;
	
	//Adiciona hora marcada ao servi�o
	function adicionarHoraMarcada(hora){
		var encontrou = false;
		for(i = 0; i < nHorasMarcadas; i++){
			if(horasMarcadas[i] == hora){
				encontrou = true;
				break;
			}
		}
		
		if(encontrou)
			alert("<bean:message key="prompt.alert.Horario_ja_existe" />");
		else{
			horasMarcadas[nHorasMarcadas] = hora;
			nHorasMarcadas++;
			listarHorasMarcadas();
		}
	}

	//Remove hora marcada do servi�o
	function removerHoraMarcada(hora){
		if(confirm("<bean:message key="prompt.Deseja_remover_esse_item" />")){
			for(i = 0; i < nHorasMarcadas; i++){
				if(horasMarcadas[i] == hora){
					for(j = i; j < nHorasMarcadas; j++){
						horasMarcadas[j] = horasMarcadas[j + 1];
					}
					
					nHorasMarcadas--;
					break;
				}
			}
			listarHorasMarcadas();
		}
	}
	
	//Lista todas as horas marcadas na tela
	function listarHorasMarcadas(){
		var conteudo = "";
		
		for(i = 0; i < nHorasMarcadas; i++){
			conteudo += "<input type=\"hidden\" name=\"horaMarcada\" value=\""+ horasMarcadas[i] +"\" />"+
						"<table width=\"100%\" cellpadding=0 cellspacing=0 border=0>"+
						"	<tr>"+
          				"		<td width=\"10%\"><img src=\"webFiles/images/botoes/lixeira.gif\" class=\"geralCursoHand\" "+
          				"					title=\"<bean:message key='prompt.excluir'/>\" onclick=\"removerHoraMarcada('"+ horasMarcadas[i] +"')\"></td>"+
          				"		<td class=\"principalLabel\">"+ horasMarcadas[i] +"</td>"+
          				"	</tr></table>";
		}
		
		document.getElementById("divLstHoras").innerHTML = conteudo;
	}

	//A��es do combo tempo
	var tempoOld = "";
	function acaoCmbTempo(){
		var obj = administracaoCsCdtbServicoServForm.servNrTempo;
		if(obj.value == "-"){
			obj.value = tempoOld;
		}
		else if(tempoOld == "-1" && obj.value != "-1" && nHorasMarcadas > 0){
			if(confirm("<bean:message key="prompt.alert.Todos_horarios_serao_removidos" />")){
				nHorasMarcadas = 0;
				horasMarcadas = new Array();
				listarHorasMarcadas();
				
				tempoOld = obj.value;
			}
			else{
				obj.value = tempoOld;
			}
		}
		else{
			tempoOld = obj.value;
		}
		
		//Habilitando ou desabilitando os campos de agendamento
		if(obj.value == "-1"){
			document.getElementById("imgSeta").disabled = false;
			administracaoCsCdtbServicoServForm.txtHoraMarcada.disabled = false;
		}
		else{
			document.getElementById("imgSeta").disabled = true;
			administracaoCsCdtbServicoServForm.txtHoraMarcada.disabled = true;
		}
	}

	//Verifica se a hora inserida � valida
	function validarHora(obj){
		var valorHora = obj.value;
		if(valorHora != "" && (valorHora.length < 5 || Number(valorHora.substring(0,2)) > 24 || Number(valorHora.substring(3,5)) > 59)){
			alert("<bean:message key="prompt.alert.Hora_invalida" />");
			obj.value = "";
			obj.focus();
		}
	}
	
	//Rotinas executadas ao iniciar a tela
	function inicio(){
		showError('<%=request.getAttribute("msgerro")%>');
		VerificaCampos();

		<logic:present name="horasMarcadas">
			<logic:iterate id="horasMarcadasVector" name="horasMarcadas">
				horasMarcadas[nHorasMarcadas] = "<bean:write name="horasMarcadasVector" property="sehoDsHora" />";
				nHorasMarcadas++;
			</logic:iterate>
			
			listarHorasMarcadas();
		</logic:present>
		
		if(nHorasMarcadas > 0){
			administracaoCsCdtbServicoServForm.servNrTempo.value = "-1";
		}
		else if(administracaoCsCdtbServicoServForm.servNrTempo.selectedIndex < 0){
			administracaoCsCdtbServicoServForm.servNrTempo.value = 0;
		}
		
		tempoOld = administracaoCsCdtbServicoServForm.servNrTempo.value;
		acaoCmbTempo();

		setaChavePrimaria(administracaoCsCdtbServicoServForm.idServCdServico.value);
	}
	
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos();inicio()" style="overflow: hidden;">

<html:form styleId="administracaoCsCdtbServicoServForm" action="/AdministracaoCsCdtbServicoServ.do">

<html:hidden property="modo" />
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="topicoId" />
<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> <!--Jonathan | Adequa��o para o IE 10-->
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="13%"> <html:text property="idServCdServico" style="width:100px" styleClass="text" disabled="true" /> 
    </td>
    <td width="34%">&nbsp;</td>
    <td width="36%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="servDsServico" styleClass="text2" size="60" maxlength="60" /> 
    </td>
    <td width="36%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.servAcesso"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="servDsAcesso" styleClass="text2" size="60" maxlength="1000"/> 
    </td>
    <td width="36%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.tempo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="13%" class="principalLabel"> <!--Jonathan | Adequa��o para o IE 10-->
		<html:select property="servNrTempo" style="width:80px" styleClass="principalObjForm" onchange="acaoCmbTempo()">
						<html:option value="5">  &nbsp;5 Minutos</html:option>
			<html:option value="15"> 15 Minutos</html:option>
			<html:option value="30"> 30 Minutos</html:option>
			<html:option value="60"> &nbsp;1 Hora</html:option>
			<html:option value="90"> &nbsp;1 Hora e meia</html:option>
			<html:option value="120">&nbsp;2 Horas</html:option>
			<html:option value="150">&nbsp;2 Horas e meia</html:option>
			<html:option value="180">&nbsp;3 Horas</html:option>
			<html:option value="210">&nbsp;3 Horas e meia</html:option>
			<html:option value="240">&nbsp;4 Horas</html:option>
			<html:option value="270">&nbsp;4 Horas e meia</html:option>						
			<html:option value="300">&nbsp;5 Horas</html:option>			
			<html:option value="330">&nbsp;5 Horas e meia</html:option>
			<html:option value="360">&nbsp;6 Horas</html:option>
			<html:option value="390">&nbsp;6 Horas e meia</html:option>
			<html:option value="420">&nbsp;7 Horas</html:option>
			<html:option value="450">&nbsp;7 Horas e meia</html:option>
			<html:option value="480">&nbsp;8 Horas</html:option>
			<html:option value="510">&nbsp;8 Horas e meia</html:option>
			<html:option value="540">&nbsp;9 Horas</html:option>
			<html:option value="570">&nbsp;9 Horas e meia</html:option>
			<html:option value="600">10 Horas</html:option>
			<html:option value="780">13 Horas</html:option>
			<html:option value="840">14 Horas</html:option>
			<html:option value="900">15 Horas</html:option>
			<html:option value="960">16 Horas</html:option>
			<html:option value="1020">17 Horas</html:option>
			<html:option value="1080">18 Horas</html:option>
			<html:option value="1140">19 Horas</html:option>
			<html:option value="1200">20 Horas</html:option>
			<html:option value="1260">21 Horas</html:option>
			<html:option value="1320">22 Horas</html:option>
			<html:option value="1380">23 Horas</html:option>
			<html:option value="1440">24 Horas</html:option>
			<html:option value="-">-----------------------</html:option>
			<html:option value="0">Nunca</html:option>
			<html:option value="-1">Hora Marcada</html:option>
		</html:select>
    </td>
    <td width="34%">&nbsp;</td>
    <td width="36%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="25%" align="right" class="principalLabel"><bean:message key="prompt.timeout_expiracao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="13%" class="principalLabel"> <!--Jonathan | Adequa��o para o IE 10-->
		<html:select property="servNrTimeout" style="width:80px" styleClass="principalObjForm">
			<html:option value="1">  &nbsp;1 Minuto</html:option><!-- Chamado: 91061 - 27/09/2013 - Carlos Nunes -->
			<html:option value="5">  &nbsp;5 Minutos</html:option>
			<html:option value="15"> 15 Minutos</html:option>
			<html:option value="30"> 30 Minutos</html:option>
			<html:option value="60"> &nbsp;1 Hora</html:option>
			<html:option value="90"> &nbsp;1 Hora e meia</html:option>
			<html:option value="120">&nbsp;2 Horas</html:option>
			<html:option value="150">&nbsp;2 Horas e meia</html:option>
			<html:option value="180">&nbsp;3 Horas</html:option>
			<html:option value="210">&nbsp;3 Horas e meia</html:option>
			<html:option value="240">&nbsp;4 Horas</html:option>
			<html:option value="270">&nbsp;4 Horas e meia</html:option>						
			<html:option value="300">&nbsp;5 Horas</html:option>			
			<html:option value="330">&nbsp;5 Horas e meia</html:option>
			<html:option value="360">&nbsp;6 Horas</html:option>
			<html:option value="390">&nbsp;6 Horas e meia</html:option>
			<html:option value="420">&nbsp;7 Horas</html:option>
			<html:option value="450">&nbsp;7 Horas e meia</html:option>
			<html:option value="480">&nbsp;8 Horas</html:option>
			<html:option value="510">&nbsp;8 Horas e meia</html:option>
			<html:option value="540">&nbsp;9 Horas</html:option>
			<html:option value="570">&nbsp;9 Horas e meia</html:option>
			<html:option value="600">10 Horas</html:option>
			<html:option value="780">13 Horas</html:option>
			<html:option value="840">14 Horas</html:option>
			<html:option value="900">15 Horas</html:option>
			<html:option value="960">16 Horas</html:option>
			<html:option value="1020">17 Horas</html:option>
			<html:option value="1080">18 Horas</html:option>
			<html:option value="1140">19 Horas</html:option>
			<html:option value="1200">20 Horas</html:option>
			<html:option value="1260">21 Horas</html:option>
			<html:option value="1320">22 Horas</html:option>
			<html:option value="1380">23 Horas</html:option>
			<html:option value="1440">24 Horas</html:option>
			<html:option value="-">-----------------------</html:option>
			<html:option value="0">Nunca</html:option>
		</html:select>
    </td>
    <td width="34%">&nbsp;</td>
    <td width="36%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">
      <div align="right" class="principalLabel"><bean:message key="prompt.tipo"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="3" class="principalLabel" >
    	<html:radio property="servInTipo" value="U" /><bean:message key="prompt.tipoUrl"/> &nbsp;&nbsp;<BR>
    	<html:radio property="servInTipo" value="E" /><bean:message key="prompt.tipoEjb"/> &nbsp;&nbsp;<BR>
    	<html:radio property="servInTipo" value="C" /><bean:message key="prompt.tipoClasse"/> &nbsp;&nbsp;
    </td>
  </tr>
   <tr><td colspan=5>&nbsp;</td></tr>
  <tr>
	<td align="right" class="principalLabel"><bean:message key="prompt.hora" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	<td><input type="text" name="txtHoraMarcada" id="txtHoraMarcada" class="text" maxlength="5" onkeypress="validaDigitoHora(this,event)" onblur="validarHora(this);" /></td>
	<td colspan=2><img src="webFiles/images/botoes/setaDown.gif" id="imgSeta" width="21" height="18" class="geralCursoHand" onclick="acaoAdicionarHora();"></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
	<td colspan=3><br>
      <table width="50%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="10%" class="principalLstCab" height="10">&nbsp;</td>
          <td class="principalLstCab"><bean:message key="prompt.hora" /></td>
        </tr>
        <tr valign="top"> 
          <td colspan="3" height="120">
          	<div id="divLstHoras" style="width: 100%; height: 100%; overflow: auto;">
          	</div>
          </td>
        </tr>
      </table>
	</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbServicoServForm.servDsServico.disabled= true;	
			document.administracaoCsCdtbServicoServForm.servNrTempo.disabled= true;
			document.administracaoCsCdtbServicoServForm.servInTipo[0].disabled= true;
			document.administracaoCsCdtbServicoServForm.servInTipo[1].disabled= true;
			document.administracaoCsCdtbServicoServForm.servInTipo[2].disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			document.administracaoCsCdtbServicoServForm.idServCdServico.disabled= false;
			document.administracaoCsCdtbServicoServForm.idServCdServico.value= '';
			document.administracaoCsCdtbServicoServForm.idServCdServico.disabled= true;
		</script>
</logic:equal>
</html>