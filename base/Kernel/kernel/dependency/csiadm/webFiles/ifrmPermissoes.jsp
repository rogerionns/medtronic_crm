<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>
	var objPermissao = new Array();
	
	/**
	Este metodo tem como objetivo verificar se o usuario logado no sistema tem direito a 
	uma determinada	funcionalidade
	*/
	function findPermissao(funcionalidade){
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PERMISSIONAMENTO,request).equals("S")) {
			out.println("return true");
		}
		%>
		var retorno = false;		
		for (var i = 0; i <= objPermissao.length; i++){
			if (objPermissao[i] == funcionalidade){
				retorno = true;
				break;
			}
		}
		
		return retorno;
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">

<html:form action="/AdministracaoPermissionamento.do" styleId="administracaoPermissionamentoForm">
	
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<input type="text" name="funcionalidade"/>
	<input type="button" value="Verify"/ onclick=alert(findPermissao(document.administracaoPermissionamentoForm.funcionalidade.value));>

	<script>
		<logic:present name="permissaoVector">
			<logic:iterate name="permissaoVector" id="permissao" indexId="sequencia">			
					objPermissao[<%= sequencia %>] = '<bean:write name="permissao" property="csAstbFunciopeniFupnVo.fupnDsFunciopeni"/>';
			</logic:iterate>
		</logic:present>
	</script>

</html:form>
</body>
</html>