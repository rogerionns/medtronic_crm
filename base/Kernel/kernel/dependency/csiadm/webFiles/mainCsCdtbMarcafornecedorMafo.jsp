<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>

<script language="Javascript">

var limpar = false;

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	administracaoCsCdtbMarcafornecedorMafoForm.target = admIframe.name;
	administracaoCsCdtbMarcafornecedorMafoForm.acao.value ='filtrar';
	administracaoCsCdtbMarcafornecedorMafoForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	administracaoCsCdtbMarcafornecedorMafoForm.filtro.value = '';
}

function submeteFormIncluir() {
	editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value = 0;
	admIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value = 0;
	editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm['csCdtbFornProdutoFoprVo.idFoprCdFornproduto'].value = 0;
	
	editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_MARCAFORNECEDOR_MAFO%>';
	editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo,codArea){
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value = codigo;
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm['csCdtbFornProdutoFoprVo.idFoprCdFornproduto'].value = codArea;
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_MARCAFORNECEDOR_MAFO%>';
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm.target = editIframe.name;
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){

	if(tab.document.administracaoCsCdtbMarcafornecedorMafoForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_MARCAFORNECEDOR_MAFO%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbMarcafornecedorMafoForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_MARCAFORNECEDOR_MAFO%>'){
		if (trim(tab.document.administracaoCsCdtbMarcafornecedorMafoForm.mafoDsMarcaFornecedor.value) != "") {
			if (tab.document.administracaoCsCdtbMarcafornecedorMafoForm['csCdtbFornProdutoFoprVo.idFoprCdFornproduto'].value > 0) {
				
				if(podeGravarAssociacao()==false){
					return false;
				}
				
				tab.document.administracaoCsCdtbMarcafornecedorMafoForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_MARCAFORNECEDOR_MAFO%>';
				tab.document.administracaoCsCdtbMarcafornecedorMafoForm.target = admIframe.name;
				disableEnable(tab.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor, false);
				tab.document.administracaoCsCdtbMarcafornecedorMafoForm.submit();
				disableEnable(tab.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor, true);
				limpar = true;
			}
			else {
			//	alert('<bean:message key="prompt.O_campo_fornecedor_e_obrigatorio"/>');
				alert('<bean:message key="prompt.Os_campos_descricao_e_fornecedor_sao_obrigatorios"/>');
				editIframe.AtivarPasta('DETALHE');
			}
		} else {
		//	alert('<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>');
			alert('<bean:message key="prompt.Os_campos_descricao_e_fornecedor_sao_obrigatorios"/>');			
			editIframe.AtivarPasta('DETALHE');
			tab.document.administracaoCsCdtbMarcafornecedorMafoForm.mafoDsMarcaFornecedor.focus();
		}
	}
}

function submeteExcluir(codigo) {
	
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value = codigo;
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_MARCAFORNECEDOR_MAFO%>';
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm.target = editIframe.name;
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbMarcafornecedorMafoForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_MARCAFORNECEDOR_MAFO%>';
		editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor, false);
		editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor, true);
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbMarcafornecedorMafo.do?tela=editCsCdtbMarcafornecedorMafo&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbMarcafornecedorMafo.do?tela=editCsCdtbMarcafornecedorMafo&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


function MM_showHideLayers() {
	var obj, i, p, v, obj, args = MM_showHideLayers.arguments;
	for (i=0; i < (args.length-2); i+=3){
		obj = document.getElementById(args[i]);
		if(obj != null){
			v = args[i + 2];
			v = (v == 'show') ? 'block' : (v = 'hide') ? 'none' : v;
			obj.style.display = v;
		}
	}
}

function  Reset(){
	document.administracaoCsCdtbMarcafornecedorMafoForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbMarcafornecedorMafoForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_MARCAFORNECEDOR_MAFO%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormal');
			setaListaBloqueia();
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbMarcafornecedorMafoForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_MARCAFORNECEDOR_MAFO%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionado');	
			setaListaHabilita();
			setaIdiomaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function sair() {
	document.location.href = 'AgendaEletronicaMenu.do';
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbMarcafornecedorMafoForm"	action="/AdministracaoCsCdtbMarcafornecedorMafo.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%" width="100%">
						&nbsp;
					</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/><!-- ## --></td>
									<td class="principalPstQuadroLinkNormal" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<%= getMessage("prompt.marca", request)%><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"><br>

                  <div id="Manifestacao" style="display: none;">  
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsCdtbMarcafornecedorMafo.do?tela=editCsCdtbMarcafornecedorMafo&acao=incluir"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
							</tr>
					 </table>
				  </div>

							
                  <div id="Destinatario" style="display: block;">
                    <table width="98%" border="0" cellspacing="0" cellpadding="0"
								align="center">
                      <tr> 
                        <td class="principalLabel" width="11%"><bean:message key="prompt.descricao"/></td>
                        <td class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td colspan="4"> 
                          <table border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="25%"> <html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
                              </td>
                              <td width="05%"> &nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" title="<bean:message key='prompt.aplicarFiltro'/>" width="21"
										height="18" class="geralCursoHand" onclick="filtrar()"> 
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" width="11%">&nbsp;</td>
                        <td class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLstCab" width="15%">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalLstCab" width="25%">
										&nbsp;
									</td>
									<td class="principalLstCab" width="75%">
										&nbsp;&nbsp;<bean:message key="prompt.codigo"/>
									</td>
								</tr>
							</table>
						</td>
                        <td class="principalLstCab" width="40%"><%= getMessage("prompt.marca", request)%></td>
                        <td class="principalLstCab" width="32%"><%= getMessage("prompt.fornecedor", request)%></td>
                        <td class="principalLstCab" align="left" width="17%">&nbsp;&nbsp;<bean:message key="prompt.inativo"/> 
                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="4" height="315"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsCdtbMarcafornecedorMafo.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
                      </tr>
                    </table>
							</div>

							
                </td>
							<td width="4" height="400px"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="400px"></td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="540px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="540px"></td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">
	/*setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_INCLUSAO_CHAVE%>', administracaoCsCdtbMarcafornecedorMafoForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RECLAMACAO_FABRICA_ALTERACAO_CHAVE%>')){
			administracaoCsCdtbMarcafornecedorMafoForm.imgGravar.disabled=true;
			administracaoCsCdtbMarcafornecedorMafoForm.imgGravar.className = 'geralImgDisable';
			administracaoCsCdtbMarcafornecedorMafoForm.imgGravar.title='';
	   }*/
	   
	   habilitaListaEmpresas();

		setaArquivoXml("CS_ASTB_IDIOMAMAFO_IDMF.xml");
		habilitaTelaIdioma();
	   
</script>


</body>
</html>
