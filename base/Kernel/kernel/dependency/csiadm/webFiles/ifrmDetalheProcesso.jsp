<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function AtivarPasta(pasta) {
	//Habilita o check de retorno para a area resolvedora apenas se selecionar
	//o Gerador da manifestação. Caso contrário desabilita e limpa o checkbox.
	document.getElementById("etpr_in_retorarearesol").disabled=!(pasta=='GERADOR');
	if (!(pasta=='GERADOR')) {
		document.getElementById("etpr_in_retorarearesol").checked=false;
	}

	switch (pasta) {
		case 'AREA':
			MM_showHideLayers('area','','show','etapa','','hide','gerador','','hide')
			document.getElementById("tblAreaFunc").style.visibility = '';
			break;
	
		case 'ETAPA':
			MM_showHideLayers('area','','hide','etapa','','show','gerador','','hide')
			break;
		
		case 'GERADOR':
			MM_showHideLayers('area','','show','etapa','','hide','gerador','','hide')
			document.getElementById("tblAreaFunc").style.visibility = 'hidden';
			document.getElementById("etpr_in_retorarearesol").disabled=false;
			break;
	}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function carregaCmbFuncionario() {
	ifrmCmbFuncionario.location.href = 'CarregaCmbFuncionarioFluxoWorkflowEtapa.do?id_area_cd_area=' + fluxoWorkflowForm.id_area_cd_area_etapa.value;
}

function verificaCmbFuncionarioJaCarregou() {
	if(ifrmCmbFuncionario.document.readyState == 'complete') {
		ifrmCmbFuncionario.fluxoWorkflowForm.id_func_cd_funcionario_etapa.value = fluxoWorkflowForm.id_func_cd_funcionario.value; 
	} else {
		setTimeout('verificaCmbFuncionarioJaCarregou()', 500);
	}
}

function inicio() {
	if(existeEtapaFilha()){
		document.getElementById("tdEtapaFinal").style.visibility = "hidden";
	}
	
	if(fluxoWorkflowForm.id_area_cd_area_etapa.value != '') {
		//Carregando o combo de funcionario
		carregaCmbFuncionario();
		
		verificaCmbFuncionarioJaCarregou();
	}
	
	marcarEtapaFinal(fluxoWorkflowForm.etpr_in_ultimaetapa);
	
	if(fluxoWorkflowForm.id_chli_cd_checklist.value != '')
		parent.fluxoWorkflowForm.id_chli_cd_checklist.value = fluxoWorkflowForm.id_chli_cd_checklist.value;
	if(fluxoWorkflowForm.id_inte_cd_integracao.value != '')
		parent.fluxoWorkflowForm.id_inte_cd_integracao.value = fluxoWorkflowForm.id_inte_cd_integracao.value;
		
	//Verificando qual a opcao Encaminhar Para esta flegada
	if(fluxoWorkflowForm.etpr_in_encaminharpara[0].checked)
		setTimeout("AtivarPasta('AREA')",100);
	else if(fluxoWorkflowForm.etpr_in_encaminharpara[1].checked)
		setTimeout("AtivarPasta('ETAPA')",100);
	else if(fluxoWorkflowForm.etpr_in_encaminharpara[2].checked)
		setTimeout("AtivarPasta('GERADOR')",100);
}

//Verifica se existe alguma etapa abaixo desta
function existeEtapaFilha(){
	for(i = 0; i < parent.arrayTreeView.length; i++) {
		if(parent.arrayTreeView[i].idPai == fluxoWorkflowForm.etpr_nr_etapa.value){
			return true;
		}
	}
	
	return false;
}

function marcarEtapaFinal(obj){
	var vlExibir = "visible";
	if(!fluxoWorkflowForm.etpr_in_etapainicial[0].disabled){
		if(obj.checked){
			vlExibir = "hidden";
		}
	}
	exibirEtapaFinal(vlExibir);
	exibirEtapaInicial(vlExibir);
}

function exibirEtapaFinal(cExibir) {
	document.getElementById("tdPerguntaEtapa1").style.visibility = cExibir;
	document.getElementById("tdPerguntaEtapa2").style.visibility = cExibir;
}

function exibirEtapaInicial(cExibir) {
	//Testar 
	if(cExibir=='visible') cExibir = '';

	document.getElementById("tdTituloEtapaSim1").style.visibility = cExibir;
	document.getElementById("tdEtapaSim1").style.visibility = cExibir;
	document.getElementById("tdTituloEtapaNao1").style.visibility = cExibir;
	document.getElementById("tdEtapaNao1").style.visibility = cExibir;
	document.getElementById("tdPerguntaEtapa3").style.visibility = cExibir;
}

function exibirTdsTitulos(cExibir) {
	document.getElementById("tdTituloEtapaSim1").style.visibility = cExibir;
	document.getElementById("tdTituloEtapaNao1").style.visibility = cExibir;
}

</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onLoad="showError('<%=request.getAttribute("msgerro")%>');inicio();" style="overflow: hidden;">
<html:form styleId="fluxoWorkflowForm" action="/CarregaDetalheProcesso.do">
<html:hidden property="id_chli_cd_checklist"/>
<html:hidden property="id_inte_cd_integracao"/>
<html:hidden property="id_func_cd_funcionario"/>

<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td class="principalLabel" align="right" width="8%">
    	<bean:message key="prompt.titulo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td class="principalLabel" width="9%" colspan="6"> 
      <html:text property="etpr_ds_tituloetapa" maxlength="80" styleClass="principalObjForm" />
    </td>

  <tr>
    <td class="principalLabel" align="right" width="8%"><bean:message key="prompt.NumEtapa" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td class="principalLabel" width="9%"> 
      <html:text property="etpr_nr_etapa" maxlength="4" styleClass="principalObjForm" onkeypress="isDigito(this)" readonly="true"/>
    </td>
    <td class="principalLabel" width="17%" align="right"> <bean:message key="prompt.NumEtapaPredecessora" /> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
    <td class="principalLabel" width="9%"> 
    	<html:text property="etpr_nr_etapapredecessora" maxlength="4" styleClass="principalObjForm" onkeypress="isDigito(this)" readonly="true"/>
    </td>
    <td id="tdPerguntaEtapa3" class="principalLabel" width="29%" align="right">
    	<input type="text" name="etapaInicialLabel" value='<bean:message key="prompt.EtapaInicial" />' class="principalLabelFundoFonteNormal" style="width:90%" readonly />
    	 <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td class="principalLabel" width="28%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
        <tr> 
          <td id="tdEtapaSim1"  align="center" width="13%"> 
          	<html:radio property="etpr_in_etapainicial" value="S" />
          </td>
          <td id="tdTituloEtapaSim1" width="17%"><bean:message key="prompt.sim" /></td>
          <td id="tdEtapaNao1" align="center" width="9%"> 
            <html:radio property="etpr_in_etapainicial" value="N" />
          </td>
          <td id="tdTituloEtapaNao1" width="13%"><bean:message key="prompt.nao" /></td>

          <td id="tdEtapaFinal" align="center" width="57%">
              <html:checkbox property="etpr_in_ultimaetapa" value="S" onclick="marcarEtapaFinal(this);"/>
              <bean:message key="prompt.EtapaFinal" />
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="espacoPqn" >&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td id="tdPerguntaEtapa1" class="principalLabel" align="right" width="34%"><bean:message key="prompt.PerguntaParaDefiniçãoFluxoPróximaEtapa" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td id="tdPerguntaEtapa2" class="principalLabel" width="27%"> 
      <html:text property="etpr_ds_perguntafluxo" maxlength="255" styleClass="principalObjForm" style="width: 210px" />
    </td>
    <td class="principalLabel" width="39%">&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalLabelValorFixo" width="14%"><bean:message key="prompt.EncaminharPara" /></td>
    <td align="center" width="2%" class="principalLabel"> 
    	<html:radio property="etpr_in_encaminharpara" value="A" onclick="AtivarPasta('AREA')" />
    </td>
    <td width="14%" class="principalLabel"><bean:message key="prompt.area" /> / <bean:message key="prompt.funcionario" /></td>
    <td align="center" width="2%" class="principalLabel"> 
    	<html:radio property="etpr_in_encaminharpara" value="E" onclick="AtivarPasta('ETAPA')" />
    </td>
    <td width="11%" class="principalLabel"><bean:message key="prompt.NumEtapa" /></td>
    <td align="center" width="2%" class="principalLabel"> 
    	<html:radio property="etpr_in_encaminharpara" value="M" onclick="AtivarPasta('GERADOR')" />
    </td>
    <td colspan="2" class="principalLabel"><bean:message key="prompt.GeradorDaManifestacao" /></td>
    <td class="principalLabel" width="35%" align="left">
    	<html:checkbox value="S" property="etpr_in_retorarearesol" styleId="etpr_in_retorarearesol"/>&nbsp;<bean:message key="prompt.area.resolvedora.gerador"/>
	</td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" height="70">
  <tr>
    <td valign="top">
      <div id="area" style="position:absolute; width:100%; z-index:1; height: 60; visibility: visible"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblAreaFunc">
          <tr> 
            <td class="principalLabel" align="right" width="10%"><bean:message key="prompt.area" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
            </td>
            <td class="principalLabel" width="37%"> 
              <html:select property="id_area_cd_area_etapa" styleClass="principalObjForm" onchange="carregaCmbFuncionario()" > 
				<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
				<logic:present name="csCdtbAreaAreaVector">
					<html:options collection="csCdtbAreaAreaVector" property="idAreaCdArea" labelProperty="areaDsArea"/>
				</logic:present> 
			  </html:select>
            </td>
            <td class="principalLabel" align="right" width="14%"><bean:message key="prompt.funcionario" /> 
              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
            <td class="principalLabel" width="39%"> 
            	<iframe name="ifrmCmbFuncionario" src="CarregaCmbFuncionarioFluxoWorkflowEtapa.do" width="90%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalLabelValorFixo" width="19%"><bean:message key="prompt.PrazoParaResolucao" /></td>
            <td width="57%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalLabel" width="22%" align="right"><bean:message key="prompt.PrazoNormal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                  </td>
                  <td class="principalLabel" width="27%"> 
                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                      <tr> 
                        <td width="43%"> 
                          <html:text property="etpr_nr_prazonormal" maxlength="4" styleClass="principalObjForm" onkeypress="isDigito(this)"/>
                        </td>
                        <td width="57%"> 
                          <html:select property="id_tppr_cd_prazonormal" styleClass="principalObjForm"  > 
							<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
							<logic:present name="csDmtbTpprazoTpprVector">
								<html:options collection="csDmtbTpprazoTpprVector" property="field(id_tppr_cd_tpprazo)" labelProperty="field(tppr_ds_tpprazo)"/>
							</logic:present> 
						 </html:select> 
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class="principalLabel" width="25%" align="right"><bean:message key="prompt.Especial" /> 
                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                  </td>
                  <td class="principalLabel" width="26%"> 
                    <table width="100%" border="0" cellspacing="2" cellpadding="0">
                      <tr> 
                        <td width="40%"> 
                          <html:text property="etpr_nr_prazoespecial" maxlength="4" styleClass="principalObjForm" onkeypress="isDigito(this)"/>
                        </td>
                        <td width="60%"> 
                          	<html:select property="id_tppr_cd_tpprazoespecial" styleClass="principalObjForm"  > 
								<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
								<logic:present name="csDmtbTpprazoTpprVector">
									<html:options collection="csDmtbTpprazoTpprVector" property="field(id_tppr_cd_tpprazo)" labelProperty="field(tppr_ds_tpprazo)"/>
								</logic:present> 
						 	</html:select>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="24%">&nbsp;</td>
          </tr>
        </table>
      </div>
      <div id="etapa" style="position:absolute; width:100%; z-index:1; height: 90; visibility: hidden"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalLabel" align="right" width="10%"><bean:message key="prompt.NumEtapa"/>
              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
            <td class="principalLabel" width="20%"> 
               <html:select property="etpr_nr_encaminharetapa" styleClass="principalObjForm"  > 
					<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
					<logic:present name="etapasVector">
						<html:options collection="etapasVector" property="field(etpr_nr_etapa)" labelProperty="field(etpr_nr_etapa)"/>
					</logic:present> 
			 	</html:select>
            </td>
            <td class="principalLabel" width="70%">&nbsp;</td>
          </tr>
        </table>
      </div>
      <div id="gerador" style="position:absolute; width:100%; z-index:1; visibility: hidden; height: 90"></div>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="principalLabel" align="right" width="3%">
    	<html:checkbox property="etpr_in_encaminharemail" value="S"/></td>
    <td class="principalLabelValorFixo" width="97%"><bean:message key="prompt.NotificarAreaResponsavelPorEmail"/></td>
  </tr>
</table>
</html:form>
</body>
</html>