<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>

	function inicio(){
		try{
			if(administracaoCsCdtbProdutoAssuntoPrasForm["csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd"].length == 2)
				administracaoCsCdtbProdutoAssuntoPrasForm["csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd"].selectedIndex = 1;
		}catch(e){}
	}

	function mostraCampoBuscaProd(){
		window.location.href = "AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%=MAConstantes.TELA_CMB_RESPTABCARACT_PRAS%>&acao=<%=Constantes.ACAO_CONSULTAR%>";	
	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}

	function buscarProduto(){
	
		//N�O UTILIZADO (MARCELO BRAGA - HENRIQUE - F�BIO : 28/08/2006)

		if (administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.rtcpDsRespTabCaractProd'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}
		
		var idCaract;
		idCaract = parent.cmbCaractIframe.administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd'].value;
		administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd'].value = idCaract;
		
		administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = "<%=MAConstantes.TELA_CMB_RESPTABCARACT_PRAS%>";
		administracaoCsCdtbProdutoAssuntoPrasForm.acao.value = "<%=Constantes.ACAO_FITRAR%>";

		administracaoCsCdtbProdutoAssuntoPrasForm.submit();
	
	}

</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbProdutoAssuntoPrasForm" action="/AdministracaoCsCdtbProdutoAssuntoPras.do">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="csCatbProdutoCaractPrcaVo.idCaprCdCaracteristicaProd"/>
	
	<logic:notEqual name="administracaoCsCdtbProdutoAssuntoPrasForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
	  				
				<html:select property="csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd" styleClass="principalObjForm" >
					<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
					<html:options collection="respTabCaractVector" property="idRtcpRespTabCaractProd" labelProperty="rtcpDsRespTabCaractProd"/>
				</html:select>

		  	</td>
		  	<td width="5%" valign="middle">
			  	<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
			</td>
	  	</tr>
	  </table>
	  
	  <logic:present name="respTabCaractVector">
		  <logic:iterate name="respTabCaractVector" id="respTabCaractVector">
			  <input type="hidden" name='txtLinha<bean:write name="respTabCaractVector" property="idRtcpRespTabCaractProd"/>' value='<bean:write name="respTabCaractVector" property="idRtcpRespTabCaractProd"/>'>
		  </logic:iterate>	  
	  </logic:present>
	  
	 </logic:notEqual>

   	  <logic:equal name="administracaoCsCdtbProdutoAssuntoPrasForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
		  	<tr>
		  		<td width="95%">
		  			<html:text property="csCatbProdutoCaractPrcaVo.rtcpDsRespTabCaractProd" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
			  	</td>
			  	<td width="5%" valign="middle">
			  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
			  	</td>
			</tr> 	
		  </table>
		  <script>
			//administracaoCsCdtbProdutoAssuntoPrasForm['csCatbProdutoCaractPrcaVo.idRtcpRespTabCaractProd'].select();
		  </script>
   	  </logic:equal>
 	  
</html:form>
</body>
</html>