<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposLayout(){
		document.administracaoCsCdtbLayoutLaouForm.laouDsLayout.disabled= true;	
		document.administracaoCsCdtbLayoutLaouForm.laouInDelimitado.disabled= true;
		document.administracaoCsCdtbLayoutLaouForm.laouDhInativo.disabled= true;
	}
	
	function inicio(){
		//setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbLayoutLaouForm.idLaouCdSequencial.value);
	}
</script>
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbLayoutLaouForm" action="/AdministracaoCsCdtbLayoutLaou.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="10%"> <html:text property="idLaouCdSequencial" styleClass="text" disabled="true" /> 
    </td>
    <td width="28%">&nbsp;</td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="laouDsLayout" styleClass="text" maxlength="40" /> 
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td class="principalLabel" width="28%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="71%"> <html:checkbox value="true" property="laouInDelimitado"/></td>
          <td class="principalLabel" width="29%"><bean:message key="prompt.delimitado"/></td>
        </tr>
      </table>
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td class="principalLabel" width="28%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="71%"> 
          	<html:checkbox value="true" property="laouDhInativo"/>
          </td>
          <td class="principalLabel" width="29%"><bean:message key="prompt.inativo"/></td>
        </tr>
      </table>
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposLayout();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_LAYOUT_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbLayoutLaouForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_LAYOUT_INCLUSAO_CHAVE%>')){
				desabilitaCamposLayout();
			}else{
				document.administracaoCsCdtbLayoutLaouForm.idLaouCdSequencial.disabled= false;
				document.administracaoCsCdtbLayoutLaouForm.idLaouCdSequencial.value= '';
				document.administracaoCsCdtbLayoutLaouForm.idLaouCdSequencial.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_LAYOUT_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_LAYOUT_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbLayoutLaouForm.imgGravar);	
				desabilitaCamposLayout();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbLayoutLaouForm.laouDsLayout.focus();}
	catch(e){}
</script>