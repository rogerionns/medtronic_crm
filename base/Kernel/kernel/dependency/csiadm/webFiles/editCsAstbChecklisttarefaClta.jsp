<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
var acao = 'I';
var countOrdem = new Number(1);

function adicionaLst() {
	if(csAstbChecklisttarefaCltaForm.id_chli_cd_checklist.value == '') {
		alert('<bean:message key="prompt.OCampoCheckListEObrigatorio"/>');
	} else if(csAstbChecklisttarefaCltaForm.id_tacl_cd_tarefachecklist.value == '') {
		alert('<bean:message key="prompt.OCampoTarefasEObrigatorio"/>');
	} else {
		adicionaLst2(countOrdem, csAstbChecklisttarefaCltaForm.id_tacl_cd_tarefachecklist[csAstbChecklisttarefaCltaForm.id_tacl_cd_tarefachecklist.selectedIndex].text, csAstbChecklisttarefaCltaForm.id_tacl_cd_tarefachecklist.value, csAstbChecklisttarefaCltaForm.id_chli_cd_checklist.value, '');
	}
}

nLinha = new Number(0);
function adicionaLst2(ordem, descricao, codTarefa, codCheckList, inativo) {
	if(!podeAdicionarNaLista(codTarefa, codCheckList)) {
		return false;
	}

	if(ordem > countOrdem) {
		countOrdem = ordem;
	} 

	nLinha ++;
	var strTxt = '';
	
	strTxt += "<table id=\"" + nLinha + "\" width=467px border=0 cellspacing=0 cellpadding=0>";
	strTxt += "  <tr> ";
	strTxt += "    <td width=5% class=principalLstParMao height=1>&nbsp;<img src=webFiles/images/botoes/lixeira.gif title=<bean:message key='prompt.excluir'/> width=14 height=14 class=geralCursoHand onclick=removeRegistro(\"" + nLinha + "\")></td>";
	strTxt += "    <td class=principalLstPar width=15%><input type=text class=principalObjForm name=clta_nr_ordem maxlength=4 onkeypress=isDigito(this) onblur=verificaOrdem(this) style=\"width: 50px;\" value=\"" + countOrdem + "\"/></td>";
	strTxt += "    <td class=principalLstPar width=65%>&nbsp;&nbsp;" + descricao + " </td>";
	
	if(inativo != '') {
		strTxt += "    <td class=principalLstPar width=15% align=center><input type=checkbox name=clta_dh_inativo checked/></td>";
	} else {
		strTxt += "    <td class=principalLstPar width=15% align=center><input type=checkbox name=clta_dh_inativo /></td>";
	}
	
	strTxt += "	   <input type=hidden name=codTarefa value=\"" + codTarefa + "\" />";
	strTxt += "	   <input type=hidden name=codCheckList value=\"" + codCheckList + "\" />";
	strTxt += "	   <input type=hidden name=dataInativo />";
	strTxt += "  </tr>";
	strTxt += "</table>";
	
	countOrdem ++;
	document.getElementById("lstTarefaChecklist").innerHTML +=  strTxt;
}

function podeAdicionarNaLista(codTarefa, codCheckList) {
	//Verifica se o item j� existe na lista
	if(csAstbChecklisttarefaCltaForm.codTarefa != null && csAstbChecklisttarefaCltaForm.codTarefa.length != undefined) {
		for(i = 0; i < csAstbChecklisttarefaCltaForm.codTarefa.length; i++) {
			if(csAstbChecklisttarefaCltaForm.codTarefa[i].value == codTarefa && csAstbChecklisttarefaCltaForm.codCheckList[i].value == codCheckList) {
				alert('<bean:message key="prompt.EsteRegistroJaExisteNaLista"/>');
				return false;
			}
		}
	} else if(csAstbChecklisttarefaCltaForm.clta_nr_ordem != null) {
		if(csAstbChecklisttarefaCltaForm.codTarefa.value == codTarefa && csAstbChecklisttarefaCltaForm.codCheckList.value == codCheckList) {
				alert('<bean:message key="prompt.EsteRegistroJaExisteNaLista"/>');
				return false;
			}
	}	
	return true;
}


function removeRegistro(nLinha) {
	if (confirm('<bean:message key="prompt.DesejaRemoverORegistro"/>')) {
		var objIdTbl = window.document.getElementById(nLinha);
		lstTarefaChecklist.removeChild(objIdTbl);
		
		//Comentado por causa do action center COD: 9296
	/*	if(csAstbChecklisttarefaCltaForm.clta_nr_ordem != null && csAstbChecklisttarefaCltaForm.clta_nr_ordem.length != 'undefined' ) {
			for(i = 0; i < csAstbChecklisttarefaCltaForm.clta_nr_ordem.length; i++) {
				countOrdem = csAstbChecklisttarefaCltaForm.clta_nr_ordem[i].value;
				csAstbChecklisttarefaCltaForm.clta_nr_ordem[i].value = i + 1;
			}
		} else {
			countOrdem = 1;
		}*/
		
	}
}

function verificaOrdem(obj) {
	var count = new Number(0);
	if(csAstbChecklisttarefaCltaForm.clta_nr_ordem != null && csAstbChecklisttarefaCltaForm.clta_nr_ordem.length != undefined) {
		for(i = 0; i < csAstbChecklisttarefaCltaForm.clta_nr_ordem.length; i++) {
			//Verifica se j� existe algum item na lista com esta ordem
			if(csAstbChecklisttarefaCltaForm.clta_nr_ordem[i].value == obj.value) {
				count ++;
				if(count > 1) {
					alert('<bean:message key="prompt.JaExisteUmRegistronaListaComEstaOrdem"/>');
					obj.value = '';
					parent.podeSalvar = false;
					obj.focus();
					return false;
				}
			}
			
			//Verifica se existe algum item na lista com a ordem em branco
			if(csAstbChecklisttarefaCltaForm.clta_nr_ordem[i].value == '' || csAstbChecklisttarefaCltaForm.clta_nr_ordem[i].value == '0') {
				alert('<bean:message key="prompt.AOrdemNaoPodeConterOValor0"/>');
				//obj.value = '';
				//obj.focus();
				parent.podeSalvar = false;
				return false;
			}
		}
	}
	parent.podeSalvar = true;
}
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')" style="overflow: hidden;">
<html:form styleId="csAstbChecklisttarefaCltaForm" action="/EditaCsAstbChecklisttarefaClta.do">
<html:hidden property="editando"/>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="21%" align="right" class="principalLabel"><bean:message key="prompt.Checklist"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
		<html:select property="id_chli_cd_checklist" styleClass="principalObjForm" onchange="document.csAstbChecklisttarefaCltaForm.submit()"> 
			<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/></html:option>
			<logic:present name="csCdtbChecklistChliVector"> 
				<html:options collection="csCdtbChecklistChliVector" property="field(id_chli_cd_checklist)" labelProperty="field(chli_ds_checklist)"/>
			</logic:present> 
		</html:select> 
	</td>
    <td width="15%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="21%" align="right" class="principalLabel"><bean:message key="prompt.TarefasDoChecklist"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
    	<html:select property="id_tacl_cd_tarefachecklist" styleClass="principalObjForm"  > 
			<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
			<logic:present name="csCdtbTarefachecklistTaclVector">
				<html:options collection="csCdtbTarefachecklistTaclVector" property="field(id_tacl_cd_tarefachecklist)" labelProperty="field(tacl_ds_tarefachecklist)"/>
			</logic:present> 
		</html:select> 
    </td>
    <td width="15%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key='prompt.Adicionar'/>" onclick="adicionaLst()"></td>
  </tr>
  <tr>
  	<td width="21%" >&nbsp;</td>
  	<td colspan="2"> 
  	 	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
  	 	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr> 
		    <td> 
		      <table width="99%" border="0" cellspacing="0" cellpadding="0">
		        <tr> 
		          <td width="5%" class="principalLstCab" height="1">&nbsp;</td>
		          <td class="principalLstCab" width="15%"><bean:message key="prompt.Ordem"/></td>
		          <td class="principalLstCab" width="65%">&nbsp;&nbsp;<bean:message key="prompt.TarefasDoChecklist"/></td>
		          <td class="principalLstCab" width="15%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.inativo"/></td>
		        </tr>
		      </table>
		      <table width="99%" height="280" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
				<tr>
					<td valign="top">
						<div id="lstTarefaChecklist" style="position: absolute; width: 467px; height: 278px; z-index: 2; overflow: auto"></div>
					</td>
				</tr>
				</table>
		    </td>
		  </tr>
		</table>
  	 </td>
  	 <td width="15%">&nbsp;</td>
  </tr>
</table>
</html:form>
</body>

<script>
	<logic:present name="csAstbChecklisttarefaClta">
		<logic:iterate name="csAstbChecklisttarefaClta" id="csAstbChecklisttarefaClta">
			adicionaLst2('<bean:write name="csAstbChecklisttarefaClta" property="field(clta_nr_ordem)" />', 
						 '<bean:write name="csAstbChecklisttarefaClta" property="field(tacl_ds_tarefachecklist)" />',
						 '<bean:write name="csAstbChecklisttarefaClta" property="field(id_tacl_cd_tarefachecklist)" />',
						 '<bean:write name="csAstbChecklisttarefaClta" property="field(id_chli_cd_checklist)" />',
						 '<bean:write name="csAstbChecklisttarefaClta" property="field(clta_dh_inativo)" />');
		</logic:iterate>
		
		//Indica que � uma edi��o
		acao = 'E';
	</logic:present>
</script>

<script>
	if(acao == 'I') {
		if(getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CHECKLIST_X_TAREFA_INCLUSAO_CHAVE%>')) {
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CHECKLIST_X_TAREFA_INCLUSAO_CHAVE%>', parent.document.csAstbChecklisttarefaCltaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			document.csAstbChecklisttarefaCltaForm['id_tacl_cd_tarefachecklist'].disabled= false;
			document.csAstbChecklisttarefaCltaForm['id_chli_cd_checklist'].disabled= false;
		} else {
			document.csAstbChecklisttarefaCltaForm['id_tacl_cd_tarefachecklist'].disabled= true;
			document.csAstbChecklisttarefaCltaForm['id_chli_cd_checklist'].disabled= true;
		}
	}
	else if(acao == 'E') {
		if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CHECKLIST_X_TAREFA_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CHECKLIST_X_TAREFA_ALTERACAO_CHAVE%>', parent.document.forms[0].imgGravar);	
	
			document.csAstbChecklisttarefaCltaForm['id_tacl_cd_tarefachecklist'].disabled= false;
			document.csAstbChecklisttarefaCltaForm['id_chli_cd_checklist'].disabled= false;
		} else {
			document.csAstbChecklisttarefaCltaForm['id_tacl_cd_tarefachecklist'].disabled= true;
			document.csAstbChecklisttarefaCltaForm['id_chli_cd_checklist'].disabled= true;
		}
	}
</script>

<%--
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CHECKLIST_X_TAREFA_INCLUSAO_CHAVE%>', parent.document.csAstbChecklisttarefaCltaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			document.csAstbChecklisttarefaCltaForm['csAstbPesqatendpadraoPeapVo.idPesqCdPesquisa'].disabled= false;
			document.csAstbChecklisttarefaCltaForm['csAstbPesqatendpadraoPeapVo.idAtpdCdAtendpadrao'].disabled= false;
	
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CHECKLIST_X_TAREFA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_CHECKLIST_X_TAREFA_ALTERACAO_CHAVE%>', parent.document.forms[0].imgGravar);	
	
				document.csAstbChecklisttarefaCltaForm['csAstbPesqatendpadraoPeapVo.idPesqCdPesquisa'].disabled= false;
				document.csAstbChecklisttarefaCltaForm['csAstbPesqatendpadraoPeapVo.idAtpdCdAtendpadrao'].disabled= false;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
			document.csAstbChecklisttarefaCltaForm['csAstbPesqatendpadraoPeapVo.idPesqCdPesquisa'].disabled= false;
			document.csAstbChecklisttarefaCltaForm['csAstbPesqatendpadraoPeapVo.idAtpdCdAtendpadrao'].disabled= false;
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
 --%>
</html>

<script>
	try{document.csAstbChecklisttarefaCltaForm['id_chli_cd_checklist'].focus();}
	catch(e){}
</script>