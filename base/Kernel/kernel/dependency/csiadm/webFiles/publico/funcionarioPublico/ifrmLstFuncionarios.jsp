<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title></title>
		  
		<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
		<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale" />/funcoes.js"></script>
			
			
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
		
		<script language="JavaScript">
			function inicio() {
				showError('<%=request.getAttribute("msgerro") %>');

				parent.showAguarde(false);
				parent.document.getElementById("checkAllFuncionarios").checked = false;

				parent.refreshPaginacaoFunc();
			}

		</script>	
		
		
	</head>
	<body class="principalBgrPageIFRM" onload="inicio();" style="margin: 0px;">
		<html:form styleId="csNatbFuncionariopublFupuForm" action="/AdministracaoCsNatbFuncionariopublFupu" >
			<html:hidden property="funcViewState" />
			<html:hidden property="tela" />
			<html:hidden property="acao" />
			<html:hidden property="idPublCdPublico" />
			<html:hidden property="idAreaCdArea" />
			<html:hidden property="regDe" />
			<html:hidden property="regAte" />
			<html:hidden property="regTotal" />
			
			<html:hidden property="funcNmFuncionario" />
			<html:hidden property="idCrpuCdCriteriospubl" />
			<html:hidden property="optVisualizacaoOperadorDisponivel" />
			<html:hidden property="idEmprCdEmpresa" />
		
			<table cellpadding="0" cellspacing="0" border="0" width="100%">	
			<logic:iterate id="funcionarioVo" name="funcViewState" indexId="indice">
				<tr>
					<td class="principalLstPar" width="3%">
						<input type="checkbox" name="idFuncCdFuncionario" value="<bean:write name="funcionarioVo" property="idFuncCdFuncionario" />"   />
					</td>
					<td class="principalLstPar" onclick="this.parentNode.childNodes[0].childNodes[0].click();">
						<%=acronymChar(((CsCdtbFuncionarioFuncVo)funcionarioVo).getFuncNmFuncionario(), 37)%>
					</td>
				</tr>
	 		</logic:iterate>
			</table>	
		 	
		</html:form>
	</body>
</html>