<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>


<%@page import="br.com.plusoft.csi.adm.vo.CsNatbFuncionariopublFupuVo"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%><html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title></title>
		  
		<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
		<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale" />/funcoes.js"></script>
			
			
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
		
		<script language="JavaScript">
			function inicio() {
				showError('<%=request.getAttribute("msgerro") %>');

				parent.showAguarde(false);
				try {
					parent.document.getElementById("checkAllAtivo").checked = true;
					parent.document.getElementById("checkAllReceptivo").checked = true;
					parent.document.getElementById("checkAllAdmin").checked = true;
					parent.document.getElementById("checkAllOperadores").checked = false;

					// Caso tenha ocorrido algum erro ao carregar a lista, n�o conseguir� carregar a pagina��o nem a lista, deve desabilitar todas as 
					// funcionalidades da tela.
					if(csNatbFuncionariopublFupuForm.viewStateSize) {
						parent.document.getElementById("divTotalViewState").innerText = csNatbFuncionariopublFupuForm.viewStateSize.value;
						parent.refreshPaginacao();
					} else {
						parent.desabilitaTela();
						parent.document.getElementById("divTotalViewState").innerText = "0";
					}
						
					if(csNatbFuncionariopublFupuForm.acao.value=="<%=Constantes.ACAO_GRAVAR%>") {
						parent.retornoGravar();
					}
				} catch(e) {
					alert(e.message);
				}
				
			}

		</script>	
		
		
	</head>
	<body class="principalBgrPageIFRM" onload="inicio();" style="margin: 0px;">
		<html:form styleId="csNatbFuncionariopublFupuForm" action="AdministracaoCsNatbFuncionariopublFupu.do" >
			<html:hidden property="fupuViewState" />
			<html:hidden property="funcViewState" />
			
			<html:hidden property="acao" />
			<html:hidden property="tela" />
			<html:hidden property="idPublCdPublico" />
			<html:hidden property="idCrpuCdCriteriospubl" />
			<html:hidden property="idFuncIncluir" />
			<html:hidden property="idAreaCdArea" />
			<html:hidden property="idEmprCdEmpresa" />
			<html:hidden property="idEmprCdEmpresaFunc" />
			<html:hidden property="funcNmFuncionario" />
			<html:hidden property="areaDsArea" />
			<input type="hidden" name="checkOption" value="" />
			<input type="hidden" name="checkAtributte" value="" />
			
			<input type="hidden" name="checkVisualizarAdministrador" value="" />
			<input type="hidden" name="checkVisualizarAtivo" value="" />
			<input type="hidden" name="checkVisualizarReceptivo" value="" />
			<input type="hidden" name="optVisualizacaoOperadorDisponivel" value="" />
			
			<logic:present name="fupuViewState">
			
			<html:hidden property="fupuRemoveOperadorViewState"/> <!-- Chamado: 90833 - Marco Costa - 30/09/2013 -->
			
			<table cellpadding="0" cellspacing="0" border="0" width="100%">	
			
			<bean:define id="regDe" name="csNatbFuncionariopublFupuForm" property="regDe" />
			<bean:define id="acao" name="csNatbFuncionariopublFupuForm" property="acao" />
			<bean:define id="regAte" name="csNatbFuncionariopublFupuForm" property="regAte" />
			<bean:define id="idAreaCdArea" name="csNatbFuncionariopublFupuForm" property="idAreaCdArea" />
			<bean:define id="funcNmFuncionario" name="csNatbFuncionariopublFupuForm" property="funcNmFuncionario" />
			
			<bean:define id="checkVisualizarAdministrador" name="csNatbFuncionariopublFupuForm" property="checkVisualizarAdministrador" />
			<bean:define id="checkVisualizarAtivo" name="csNatbFuncionariopublFupuForm" property="checkVisualizarAtivo" />
			<bean:define id="checkVisualizarReceptivo" name="csNatbFuncionariopublFupuForm" property="checkVisualizarReceptivo" />
			
			<%
			Vector fupuViewState = (Vector) request.getAttribute("fupuViewState");
			
			CsNatbFuncionariopublFupuVo fupuVo;
			long idFuncCdFuncionario = 0;
			int contador = 0;
			int ate = 0;
			if("".equals(regDe) || regDe == null) regDe = "0";
			if("".equals(regAte) || regDe == null) regAte = "0";
			if(fupuViewState==null) fupuViewState = new Vector();
			
			int de = Integer.parseInt(String.valueOf(regDe));
			
			if(de==0) de++;
			String regAteOriginal = String.valueOf(regAte);
			regAte = String.valueOf(Integer.parseInt(String.valueOf(regAte))+1);
			
			for(int j = 0; j < fupuViewState.size(); j++) {
				
				fupuVo = (CsNatbFuncionariopublFupuVo) fupuViewState.get(j);
				
				boolean exibirRegistro = false;
				
				if(checkVisualizarAdministrador.equals("S") && fupuVo.getFupuInAdmin())
				{
					exibirRegistro = true;
				}
				else if(checkVisualizarAtivo.equals("S") && fupuVo.getFupuInAtual())
				{
					exibirRegistro = true;
				}
				else if(checkVisualizarReceptivo.equals("S") && fupuVo.getFupuInReceptivo())
				{
					exibirRegistro = true;
				}
				else if(checkVisualizarAdministrador.equals("S") && fupuVo.getFupuInAdmin() && checkVisualizarAtivo.equals("S") && fupuVo.getFupuInAtual() && checkVisualizarReceptivo.equals("S") && fupuVo.getFupuInReceptivo())
				{
					exibirRegistro = true;
				}
				else if(checkVisualizarAdministrador.equals("N") && checkVisualizarAtivo.equals("N") && checkVisualizarReceptivo.equals("N"))
				{
					exibirRegistro = true;
				}
			   
			   //Chamado: 90833 - Marco Costa - 30/09/2013
				//if(idAreaCdArea.equals("") || idAreaCdArea.equals(String.valueOf(fupuVo.getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getIdAreaCdArea()))) {
					//if(funcNmFuncionario.equals("") || fupuVo.getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario().indexOf((String) funcNmFuncionario) > -1) {
						contador++;
						if(contador >= Integer.parseInt(String.valueOf(regDe)) && contador <= Integer.parseInt(String.valueOf(regAte))) {
								
							idFuncCdFuncionario = fupuVo.getCsCdtbFuncionarioFuncVo().getIdFuncCdFuncionario();
							 
							if(exibirRegistro)
							{
							%>
							<tr>
								<td class="principalLstPar" width="3%">
								
									<input type="checkbox" name="idFuncCdFuncionario" value="<%=idFuncCdFuncionario %>" />
									<input type="hidden" name="idFuncTela" value="<%=idFuncCdFuncionario %>" />
								</td>
								<td width="25%" class="principalLstPar" onclick="this.parentNode.childNodes[0].childNodes[0].click();">
									<%=acronymChar(fupuVo.getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getAreaDsArea(), 20)%> 
								</td>
								<td width="38%" class="principalLstPar" onclick="this.parentNode.childNodes[0].childNodes[0].click();">
									<%=acronymChar(fupuVo.getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario(), 37)%>
								</td>
								<td class="principalLstPar" width="7%" >
									<input type="checkbox" name="fupuInAdmin" value="<%=idFuncCdFuncionario %>" onclick="parent.dadosAlterados=true;" <%=(fupuVo.getFupuInAdmin()?"checked":"") %> />
								</td>
								<td class="principalLstPar" width="6%" >
									<!--   html:hidden name="fupuVo" property="fupuInAtual"  -->
									<input type="checkbox" name="fupuInAtual" value="<%=idFuncCdFuncionario %>" onclick="parent.dadosAlterados=true;" <%=(fupuVo.getFupuInAtual()?"checked":"") %> />
								</td>
								<td class="principalLstPar" width="5%">
									<!-- html:hidden name="fupuVo" property="fupuInReceptivo"  -->
									<input type="checkbox" name="fupuInReceptivo" value="<%=idFuncCdFuncionario %>" onclick="parent.dadosAlterados=true;" <%=(fupuVo.getFupuInReceptivo()?"checked":"") %>  />
								</td>
								
							</tr>
							<%
							  ate = j;
							}
						}
					//}
				//}
			    
			}
			fupuVo=null;
			if(contador==0) de = -1;
			
			%>
			</table>	
			
			<input type="hidden" name="regDe" value="<%=de %>" />
			<input type="hidden" name="regAte" value="<%=regAteOriginal %>" />
			<input type="hidden" name="regTotal" value="<%=contador %>" />
			<input type="hidden" name="viewStateSize" value="<%=fupuViewState.size() %>" />
			
			</logic:present>
		 	
		</html:form>
	</body>
</html>