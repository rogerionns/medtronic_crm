<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idIdioma=0;
if (session != null && session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
	idIdioma = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();
}

%>

<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title></title>
		  
		<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
		<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale" />/funcoes.js"></script>
		<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
			
			
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
		
		<script language="JavaScript">
			var dadosAlterados = false;
			var isLoading = true;
			
			function inicio() {
				showError('<%=request.getAttribute("msgerro") %>');

				isLoading=false;
				showAguarde(false);				
				
				//Chamado: 90833 - Marco Costa - 30/09/2013
				document.csNatbFuncionariopublFupuForm.idEmprCdEmpresa[1].selectedIndex = 0; //inicia sem nenhuma empresa selecionada.
				
				var url  = "AdministracaoCsNatbFuncionariopublFupu.do?tela=ifrmLstOperadores";
				    url += "&idPublCdPublico=<bean:write name="csNatbFuncionariopublFupuForm" property="idPublCdPublico" />";
				    url += "&idCrpuCdCriteriospubl=<bean:write name="csNatbFuncionariopublFupuForm" property="idCrpuCdCriteriospubl" />";

				    if(csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.checked)
					{
				    	url += "&checkVisualizarAdministrador=S";
					}
					else
					{
						url += "&checkVisualizarAdministrador=N";
					}

					if(csNatbFuncionariopublFupuForm.checkVisualizarAtivo.checked)
					{
						url += "&checkVisualizarAtivo=S";
					}
					else
					{
						url += "&checkVisualizarAtivo=N";
					}


					if(csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.checked)
					{
						url += "&checkVisualizarReceptivo=S";
					}
					else
					{
						url += "&checkVisualizarReceptivo=N";
					}
					
					//Chamado: 90833 - Marco Costa - 30/09/2013
					url += "&idEmprCdEmpresa="+csNatbFuncionariopublFupuForm.idEmprCdEmpresa[1].value;
				    
				$("ifrmLstOperadores").src = url;
				
			}

			function gravar() {
				if(!showAguarde(true)) return false;
				
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.acao.value="<%=Constantes.ACAO_GRAVAR%>";
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.submit();
			}

			function retornoGravar() {
				dadosAlterados = false;
				
				if(confirm('<bean:message key="prompt.dadosGravadosComSucessoDesejaSair"/>')) 
				{
					window.close();
				}
				else
				{
					filtrarOperadores();
				}
			}

			function sair() {
				if(!dadosAlterados || confirm('<bean:message key="prompt.desejaSairSemGravarAlteracoes"/>')) 
					window.close();
			}

			function carregaLstFuncionarios() {

				//Se a opcao de visualizar estiver selecionado a opcao Todos, então é obrigatorio preencher a area ou nome
				if(document.getElementsByName("optVisualizacaoOperadorDisponivel")[0].value == "T")
				{
					if(document.getElementsByName("idAreaCdArea")[0].value=="") {
						if(trim(document.getElementsByName("funcNmFuncionario")[0].value) == "")
						{
							alert("<bean:message key='prompt.selecioneAreaOuPreenchaNomeCliqueFiltrarExibirRegistros'/>.");
							return false;
						}					
					}
				}

				if(!showAguarde(true)) return false;
				
				var url = "AdministracaoCsNatbFuncionariopublFupu.do";
					url+= "?tela=ifrmLstFuncionarios";
					url+= "&idAreaCdArea="+document.getElementsByName("idAreaCdArea")[0].value;
					url+= "&funcNmFuncionario="+document.getElementsByName("funcNmFuncionario")[0].value.toUpperCase();
					url+= "&idPublCdPublico="+document.getElementsByName("idPublCdPublico")[0].value;
					url+= "&idCrpuCdCriteriospubl="+document.getElementsByName("idCrpuCdCriteriospubl")[0].value;
					url+= "&optVisualizacaoOperadorDisponivel="+document.getElementsByName("optVisualizacaoOperadorDisponivel")[0].value;
					url+= "&idEmprCdEmpresa="+document.csNatbFuncionariopublFupuForm.idEmprCdEmpresa[0].value

					//Chamado: 90833 - Marco Costa - 30/09/2013

				$("ifrmLstFuncionarios").src = url;
			}

			function marcarTodos(ifrm, name, t, tip) {								
				
				if(!showAguarde(true)) return false;

				if(name=="fupuInAtual" || name=="fupuInReceptivo" || name=="fupuInAdmin") {
					
					if(confirm("Deseja " + t.title + " os registros de todas as páginas como " + tip + "? \n\nClique em cancelar para marcar somente os registros dessa página")) {

						//Chamado: 80636 - Carlos Nunes - 24/12/2012
						if(csNatbFuncionariopublFupuForm.checkVisualizarAtivo.checked)
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAtivo.value = "S";
						}
						else
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAtivo.value = "N";
						}
						
						if(csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.checked)
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.value = "S";
						}
						else
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.value = "N";
						}
						
						if(csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.checked)
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.value = "S";
						}
						else
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.value = "N";
						}
						
						ifrmLstOperadores.csNatbFuncionariopublFupuForm.acao.value="CHECK_ALL";
						ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkOption.value=t.checked;
						ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkAtributte.value=name;
						ifrmLstOperadores.csNatbFuncionariopublFupuForm.submit();
												
					} 
					else
					{
						
						//t.checked = false;
						//showAguarde(false);
						//return false;
						
						if(csNatbFuncionariopublFupuForm.checkVisualizarAtivo.checked)
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAtivo.value = "S";
						}
						else
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAtivo.value = "N";
						}
						
						if(csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.checked)
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.value = "S";
						}
						else
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.value = "N";
						}
						
						if(csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.checked)
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.value = "S";
						}
						else
						{
							ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.value = "N";
						}
						
						ifrmLstOperadores.csNatbFuncionariopublFupuForm.acao.value="CHECK_PAGE";
						ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkOption.value=t.checked;
						ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkAtributte.value=name;
						ifrmLstOperadores.csNatbFuncionariopublFupuForm.submit();
						
					}
	
				}

				var checkObj = ifrm.document.getElementsByName(name);
				for(var i=0; i<checkObj.length; i++) {
					checkObj[i].checked=t.checked;
				}	
				
				if(t.checked) {
					t.title="<bean:message key="prompt.desmarcarTodos" />";
				} else {
					t.title="<bean:message key="prompt.marcarTodos" />";
				}
				if(tip) t.title += " - " + tip;


				showAguarde(false);								
				
				
				
			}

			function incluirOperadores() {
				
				//document.csNatbFuncionariopublFupuForm.idEmprCdEmpresa[1].selectedIndex = 0;
				//document.csNatbFuncionariopublFupuForm.idAreaCdArea[1].selectedIndex = 0;
				//filtrarOperadores();
				
				//Chamado: 90833 - Marco Costa - 30/09/2013
				
				dadosAlterados = true;
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.acao.value="<%=Constantes.ACAO_INCLUIR%>";
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.funcViewState.value=ifrmLstFuncionarios.document.forms[0].funcViewState.value;

				var idFuncIncluir = "";

				if(document.getElementById("checkAllFuncionarios").checked) {
					if(confirm("<bean:message key='prompt.desejaExecutarOperaçãoTodosOperadoresIncluir'/>")) {
						idFuncIncluir="TODOS;";
					}
				}

				var chk = ifrmLstFuncionarios.document.getElementsByName("idFuncCdFuncionario");
				if(idFuncIncluir=="") {
					for(var i=0; i<chk.length; i++) {
						if(chk[i].checked) {	
							idFuncIncluir += chk[i].value+";";
						}
					}
				}

				if(idFuncIncluir=="") {
					alert("<bean:message key='prompt.voceDeveSelecionarUmFuncionarioParaIncluirGrupo'/>");
					return false;
				}

				/*if(ifrmLstFuncionarios.document.forms[0].idAreaCdArea.value=="") {
					alert("<bean:message key='prompt.selecioneAreaCliqueFiltrarExibirRegistrosArea'/>.");
					return false;
				}*/

				if(!showAguarde(true)) return false;
				
				//Chamado: 90833 - Marco Costa - 30/09/2013
				
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.idFuncIncluir.value = idFuncIncluir;
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.idEmprCdEmpresaFunc.value = csNatbFuncionariopublFupuForm.idEmprCdEmpresa[0].value;
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.idAreaCdArea.value = csNatbFuncionariopublFupuForm.idAreaCdArea[0].value;
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.areaDsArea.value = csNatbFuncionariopublFupuForm.idAreaCdArea[0].options[csNatbFuncionariopublFupuForm.idAreaCdArea[0].selectedIndex].text;

				ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.value = "N";
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAtivo.value = "N";
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.value = "N";
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.optVisualizacaoOperadorDisponivel.value =  csNatbFuncionariopublFupuForm.optVisualizacaoOperadorDisponivel.value;
				
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.funcNmFuncionario.value = '';				
				
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.submit();

				
			}

			function removerOperadores() {
				if(!showAguarde(true)) return false;

				dadosAlterados = true;


				var checkOne = false;
				if(ifrmLstOperadores.csNatbFuncionariopublFupuForm.idFuncCdFuncionario) {
					for(var i=0; i<ifrmLstOperadores.document.getElementsByName("idFuncCdFuncionario").length; i++) {
						if(ifrmLstOperadores.document.getElementsByName("idFuncCdFuncionario")[i].checked) {	
							checkOne = true;
						}
					}
				}
				
				if(!checkOne) {
					alert("<bean:message key='prompt.voceDeveSelecionarPeloMenosUmOperadorParaRetirarGrupo'/>");
					showAguarde(false);
					return false;
				}

				ifrmLstOperadores.csNatbFuncionariopublFupuForm.acao.value="<%=Constantes.ACAO_EXCLUIR%>";
				if(document.getElementById("checkAllOperadores").checked) {
					if(confirm("<bean:message key='prompt.desejaExecutarOperaçãoTodosOperadores'/>")) {
						ifrmLstOperadores.csNatbFuncionariopublFupuForm.acao.value+="_TODOS";
					}
				}

				ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.value = "N";
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAtivo.value = "N";
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.value = "N";
				
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.submit();
			}

			function showAguarde(show) {
				if(isLoading) return;
				
				var obj = document.getElementById("LayerAguarde");

				if(show) {
					if(obj.style.visibility == 'visible') return false;
					obj.style.visibility = 'visible';
					
					document.body.style.cursor="wait";
				} else {
					if(obj.style.visibility == 'hidden') return false;
					obj.style.visibility = 'hidden';

					document.body.style.cursor="default";
				}
				
				desabilitaTela(show);

				return true;
			}

			function desabilitaTela(bDisable) {
				if(bDisable==undefined) bDisable = true;
				
				if(bDisable) {
					document.getElementById("imgAvancar").className = "geralImgDisable";
					document.getElementById("imgRemover").className = "geralImgDisable";
					document.getElementById("imgLupaFuncionario").className = "geralImgDisable";
					document.getElementById("imgLupaOperador").className = "geralImgDisable";
					document.getElementById("btnGravar").className = "geralImgDisable";
					
				} else {
					document.getElementById("imgAvancar").className = "geralCursoHand";
					document.getElementById("imgRemover").className = "geralCursoHand";
					document.getElementById("imgLupaFuncionario").className = "geralCursoHand";
					document.getElementById("imgLupaOperador").className = "geralCursoHand";
					document.getElementById("btnGravar").className = "geralCursoHand";
					
				}

				document.getElementById("imgAvancar").disabled = bDisable;
				document.getElementById("imgRemover").disabled = bDisable;
				document.getElementById("imgLupaFuncionario").disabled = bDisable;
				document.getElementById("imgLupaOperador").disabled = bDisable;
				document.getElementsByName("idAreaCdArea")[0].disabled = bDisable;
				document.getElementsByName("funcNmFuncionario")[0].disabled = bDisable;
				document.getElementsByName("idAreaCdArea")[1].disabled = bDisable;
				document.getElementsByName("funcNmFuncionario")[1].disabled = bDisable;
				document.getElementById("checkAllFuncionarios").disabled = bDisable;
				document.getElementById("checkAllOperadores").disabled = bDisable;
				document.getElementById("checkAllAtivo").disabled = bDisable;
				document.getElementById("checkAllAdmin").disabled = bDisable;
				document.getElementById("checkAllReceptivo").disabled = bDisable;
				document.getElementById("btnGravar").disabled = bDisable;

			}

			function paginarFuncionarios(opt) {
				//Chamado: 90833 - Marco Costa - 30/09/2013
				//Se a opcao de visualizar estiver selecionado a opcao Todos, então é obrigatorio preencher a area ou nome
				if(document.getElementsByName("optVisualizacaoOperadorDisponivel")[0].value == "T")
				{
					if(document.getElementsByName("idAreaCdArea")[0].value=="") {
						if(trim(document.getElementsByName("funcNmFuncionario")[0].value) == "")
						{
							alert("<bean:message key='prompt.selecioneAreaOuPreenchaNomeCliqueFiltrarExibirRegistros'/>.");
							return false;
						}					
					}
				}

				//if(!showAguarde(true)) return false;
												
				//Chamado: 90833 - Marco Costa - 30/09/2013
				ifrmLstFuncionarios.csNatbFuncionariopublFupuForm.tela.value = "ifrmLstFuncionarios";
			    ifrmLstFuncionarios.csNatbFuncionariopublFupuForm.idAreaCdArea.value = document.getElementsByName("idAreaCdArea")[0].value;				
				ifrmLstFuncionarios.csNatbFuncionariopublFupuForm.idPublCdPublico.value = document.getElementsByName("idPublCdPublico")[0].value;
				ifrmLstFuncionarios.csNatbFuncionariopublFupuForm.optVisualizacaoOperadorDisponivel.value = document.getElementsByName("optVisualizacaoOperadorDisponivel")[0].value;
				
				paginar(ifrmLstFuncionarios, opt);
			}

			
			function paginarOperadores(opt) {
				
				if(document.getElementById("checkAllOperadores").checked) {
					
					var checkObj = ifrmLstOperadores.document.getElementsByName('idFuncCdFuncionario');
					for(var i=0; i<checkObj.length; i++) {
						checkObj[i].checked = true;
					}
					
				}
				
				if(csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.checked)
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.value = "S";
				}
				else
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.value = "N";
				}

				if(csNatbFuncionariopublFupuForm.checkVisualizarAtivo.checked)
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAtivo.value = "S";
				}
				else
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAtivo.value = "N";
				}


				if(csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.checked)
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.value = "S";
				}
				else
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.value = "N";
				}
				
				paginar(ifrmLstOperadores, opt);
			}

			function paginar(ifrm, opt) {
				var regDe = new Number(ifrm.csNatbFuncionariopublFupuForm.regDe.value);
				var regAte = new Number(ifrm.csNatbFuncionariopublFupuForm.regAte.value);
				var regTotal = new Number(ifrm.csNatbFuncionariopublFupuForm.regTotal.value);
				
				if(opt=='A') {
					if(regDe <= 1) return false;
					ifrm.csNatbFuncionariopublFupuForm.acao.value = "PAGINAR_ANTES";
				} else {
					if((regAte+1) >= regTotal) return false;
					ifrm.csNatbFuncionariopublFupuForm.acao.value = "PAGINAR_DEPOIS";
				}

				showAguarde(true);
				
				ifrm.csNatbFuncionariopublFupuForm.submit();
			}

			function refreshPaginacao() {
				var regDe = new Number(ifrmLstOperadores.csNatbFuncionariopublFupuForm.regDe.value);
				var regAte = new Number(ifrmLstOperadores.csNatbFuncionariopublFupuForm.regAte.value);
				var regTotal = new Number(ifrmLstOperadores.csNatbFuncionariopublFupuForm.regTotal.value);

				if(regDe == -1){
					setValue($("vlMin"), regDe+1);
				}else{
					setValue($("vlMin"), regDe);
				}
				
				if(regAte < regTotal) {
					setValue($("vlMax"), (regAte+1));
				} else {
					setValue($("vlMax"), regTotal);
				}
				setValue($("nTotal"), regTotal);
			}

			function refreshPaginacaoFunc() {
				
				var regDe = new Number(ifrmLstFuncionarios.document.forms[0].regDe.value);
				var regAte = new Number(ifrmLstFuncionarios.document.forms[0].regAte.value);
				var regTotal = new Number(ifrmLstFuncionarios.document.forms[0].regTotal.value);

				if(regTotal == 0){
					setValue($("vlMinFunc"), (regDe));
				}else{
					setValue($("vlMinFunc"), (regDe+1));
				}
				
				if(regAte < regTotal) {
					setValue($("vlMaxFunc"), (regAte+1));
				} else {
					setValue($("vlMaxFunc"), regTotal);
				}
				setValue($("nTotalFunc"), regTotal);
			} 

			function filtrarOperadores() {
				if(!showAguarde(true)) return false;

				if(csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.checked)
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.value = "S";
				}
				else
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAdministrador.value = "N";
				}

				if(csNatbFuncionariopublFupuForm.checkVisualizarAtivo.checked)
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAtivo.value = "S";
				}
				else
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarAtivo.value = "N";
				}


				if(csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.checked)
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.value = "S";
				}
				else
				{
					ifrmLstOperadores.csNatbFuncionariopublFupuForm.checkVisualizarReceptivo.value = "N";
				}				
								
				//Chamado: 90833 - Marco Costa - 30/09/2013
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.idEmprCdEmpresa.value = csNatbFuncionariopublFupuForm.idEmprCdEmpresa[1].value;
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.idAreaCdArea.value = csNatbFuncionariopublFupuForm.idAreaCdArea[1].value;
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.funcNmFuncionario.value = csNatbFuncionariopublFupuForm.funcNmFuncionario[1].value.toUpperCase();
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.acao.value = "<%=Constantes.ACAO_CONSULTAR%>";
				ifrmLstOperadores.csNatbFuncionariopublFupuForm.submit();
			}


			function buscaAreaEmpresaDisp(){
				
				//Chamado: 90833 - Marco Costa - 30/09/2013
				
				var cTxt = '';

				cTxt += '<select id="idAreaCdArea" name="idAreaCdArea" class="principalObjForm" >';
				cTxt += '	<option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </option>';
				cTxt += '	<option value="0"> Carregando dados... </option>';
				cTxt += '</select>';

				document.getElementById("tdAreaDisp").innerHTML = cTxt;

				var ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/CS_CDTB_AREA_AREA.xml", "ConsultaBanco.do", true);	
				ajax.addField("id_idio_cd_idioma",'<%=idIdioma%>');
				ajax.addField("id_empr_cd_empresa",document.csNatbFuncionariopublFupuForm.idEmprCdEmpresa[0].value);

				ajax.executarConsulta(montaAreaEmpresaDisp, false, true);
			}
			
			function buscaAreaEmpresaSelec(){
				
				//Chamado: 90833 - Marco Costa - 30/09/2013
				
				var cTxt = '';

				cTxt += '<select id="idAreaCdArea" name="idAreaCdArea" class="principalObjForm" >';
				cTxt += '	<option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </option>';
				cTxt += '	<option value="0"> Carregando dados... </option>';
				cTxt += '</select>';

				document.getElementById("tdAreaSelec").innerHTML = cTxt;

				var ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/CS_CDTB_AREA_AREA.xml", "ConsultaBanco.do", true);	
				ajax.addField("id_idio_cd_idioma",'<%=idIdioma%>');
				ajax.addField("id_empr_cd_empresa",document.csNatbFuncionariopublFupuForm.idEmprCdEmpresa[1].value);

				ajax.executarConsulta(montaAreaEmpresaSelec, false, true);
			}

			function montaAreaEmpresaDisp(ajax){ 
				if(ajax.getMessage() != ''){
					alert(ajax.getMessage());
					return;
				}

				rs = ajax.getRecordset();
				var cTxt = '';

				//Chamado: 90833 - Marco Costa - 30/09/2013
				cTxt += '<select id="idAreaCdArea" name="idAreaCdArea" class="principalObjForm" onchange="carregaLstFuncionarios()">';
				cTxt += '	<option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </option>';
				while(rs.next()){
					cTxt += '	<option value="' + rs.get("id_area_cd_area") + '">' + rs.get("area_ds_area") + '</option>';
				}
				cTxt += '</select>';

				document.getElementById("tdAreaDisp").innerHTML = cTxt;
			}
			
			function montaAreaEmpresaSelec(ajax){ 
				if(ajax.getMessage() != ''){
					alert(ajax.getMessage());
					return;
				}

				rs = ajax.getRecordset();
				var cTxt = '';

				//Chamado: 90833 - Marco Costa - 30/09/2013
				cTxt += '<select id="idAreaCdArea" name="idAreaCdArea" class="principalObjForm" onchange="filtrarOperadores()">';
				cTxt += '	<option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </option>';
				while(rs.next()){
					cTxt += '	<option value="' + rs.get("id_area_cd_area") + '">' + rs.get("area_ds_area") + '</option>';
				}
				cTxt += '</select>';

				document.getElementById("tdAreaSelec").innerHTML = cTxt;
				
				filtrarOperadores();
				
			}
			
		</script>	
		

	</head>
	<body class="principalBgrPage" scroll="no" style="margin: 5px;" onload="inicio();">
		<html:form styleId="csNatbFuncionariopublFupuForm">
		
		<html:hidden property="idPublCdPublico"/>
		<html:hidden property="idCrpuCdCriteriospubl"/>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="100%" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.definirOperadores" /></td><td class="principalQuadroPstVazia" height="17">&nbsp;</td><td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td></tr></table></td></tr><tr><td class="principalBgrQuadro" height="520" valign="top" style="padding: 10px;">
		<!-- Page Content -->
		
		<div class="principalLabel" style="width: 140px; float: left; text-align: left;">
			<bean:message key="prompt.criterio" />&nbsp;
			<img src="/plusoft-resources/images/icones/setaAzul.gif" />&nbsp;
		</div> <!-- Chamado: 89652 - 19/07/2013 - Carlos Nunes -->
		<div class="principalLabelValorFixo" style="width: 600px; float: left; text-align: left;">
			<bean:write name="csNatbFuncionariopublFupuForm" property="publDsPublico"/>
		</div><br/>
		<div class="principalLabel" style="width: 140px; float: left; text-align: left;">
			Operadores Vinculados&nbsp;
			<img src="/plusoft-resources/images/icones/setaAzul.gif" />&nbsp;
		</div>
		<div class="principalLabelValorFixo" style="width: 150px; float: left; text-align: left;">
			<span id="divTotalViewState">&nbsp;</span>
		</div>
		<br/>
		
		<div id="principal" class="principalQuadroPst" style="float:left; height: 44px; width:300px; border: 1px solid; z-index:0" >

			<div id="title" style="float: left;">
				<div class="principalLstCab" style="width: 300px;text-align: left;height: 22px">&nbsp;<bean:message key="prompt.visualizacao"/></div>
			</div>
			
			<div style="float: left; margin-top: 2px; width: 100%;height: 22px">
				<html:select property="optVisualizacaoOperadorDisponivel" styleClass="principalObjForm"> 
					<html:option value="T"><bean:message key="prompt.todos"/></html:option>
					<html:option value="S"><bean:message key="prompt.subordinados"/></html:option>
					<html:option value="CA"><bean:message key="prompt.campanhaAdministrativa"/></html:option> 
				</html:select> 		       
			</div>
		</div>
		
		
		<div id="principal2" class="principalQuadroPst" style="float: left; margin-left: 78px; height: 44px; width:300px; border: 1px solid; z-index:0" >

			<div id="title" style="float: left; width:100%;"> 
				<div class="principalLstCab" style="width: 100%;text-align: left;height: 22px">&nbsp;<bean:message key="prompt.visualizacao"/></div>
			</div>
			
			<div style="float: left; margin-top: 2px; width: 100%px; height: 22px">
				<table>
					<tr>
						<td class="principalLabel">
				        	<input type="checkbox" name="checkVisualizarAdministrador"/> <bean:message key="prompt.administrador"/>
				        </td>
				        <td class="principalLabel">
				        	<input type="checkbox" name="checkVisualizarAtivo"/> <bean:message key="prompt.campanha.ativo"/>
				        </td>
				        <td class="principalLabel">
				        	<input type="checkbox" name="checkVisualizarReceptivo"/> <bean:message key="prompt.campanha.receptivo"/>
				        </td>				       
				     </tr>
				</table>
			</div>
		</div>
		<br/>
		<br/>
		<br/>

		<div style="float: left; width: 300px; ">
			<div class="principalLabel" style="clear: both; width: 300px; height: 100px;"> 
			
			<!-- Chamado: 90833 - Marco Costa - 30/09/2013 -->
				    <bean:message key="prompt.empresa.area"/><br />
				    <html:select property="idEmprCdEmpresa" styleClass="principalObjForm" onchange="buscaAreaEmpresaDisp()"> 
						<html:options collection="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" labelProperty="emprDsEmpresa"/> 
					</html:select> 
					
					<bean:message key="prompt.area" /><br />
				    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
				    	<tr>
				    		<td id="tdAreaDisp" name="tdAreaDisp">
								<html:select property="idAreaCdArea" styleClass="principalObjForm" onchange="carregaLstFuncionarios()">
									<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/> </html:option>
									
									<logic:present name="csCdtbAreaAreaVector">
										<html:options collection="csCdtbAreaAreaVector" labelProperty="areaDsArea" property="idAreaCdArea" />
									</logic:present>
								</html:select>
				    		</td>
				    	</tr>
				    </table>
					
					<bean:message key="prompt.nome" /><br />
					<html:text property="funcNmFuncionario" styleClass="principalObjForm" style="width: 280px;" />
					<img src="/plusoft-resources/images/botoes/lupa.gif" class="geralCursoHand" onclick="carregaLstFuncionarios()" id="imgLupaFuncionario" title="<bean:message key="prompt.filtrar" />"  />
			</div>
						
			<table cellpadding="0" cellspacing="0" border="0" style="margin-top: 5px;" width="300">	
				<tr>
					<td class="principalLstCab" width="3%">
						<input id="checkAllFuncionarios" type="checkbox" align="middle" onclick="marcarTodos(ifrmLstFuncionarios, 'idFuncCdFuncionario', this)" title="<bean:message key="prompt.marcarTodos" />" />
					</td>
					<td class="principalLstCab" onclick="this.parentNode.childNodes[0].childNodes[0].click();">
						 <bean:message key="prompt.operadoresDisponiveis"/> 
					</td>
				</tr>
	 		</table>
			
			<iframe id="ifrmLstFuncionarios" name="ifrmLstFuncionarios" src="AdministracaoCsNatbFuncionariopublFupu.do?tela=ifrmLstFuncionarios&idAreaCdArea=&idPublCdPublico=<bean:write name="csNatbFuncionariopublFupuForm" property="idPublCdPublico" />"
				frameborder="0" scrolling="auto" style="width: 298px; height: 300px; border: 1px solid #7EA5B8;" >
			</iframe>

			<div id="LayerPaginacaoFunc" style="position:relative; left:0px; top:5px; width:200px; height:21px; z-index:1">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="principalLabel" width="7%">
							<img id="imgAnt" src="webFiles/images/botoes/setaLeft.gif" width="21" height="18" title="<bean:message key='prompt.pagina.anterior'/>" class="geralCursoHand" onclick="paginarFuncionarios('A');">
						</td>
						<td class="principalLabel" width="30%" align="Center"><span id="vlMinFunc">&nbsp;</span></td>
						<td class="principalLabel" width="10%" align="Center">até</td>
						<td class="principalLabel" width="30%" align="Center"><span id="vlMaxFunc">&nbsp;</span></td>
						<td class="principalLabel" width="7%">
							<img id="imgProx" src="webFiles/images/botoes/setaRight.gif" width="21" height="18" title="<bean:message key='prompt.proxima.pagina'/>" class="geralCursoHand" onclick="paginarFuncionarios('P');">
						</td>
						<td class="principalLabel" width="3%" >&nbsp;</td>
						<td class="principalLabel" width="13%" align="center">Total:&nbsp;</td>
						<td class="principalLabel" width="30%" align="right"><span id="nTotalFunc">&nbsp;</span></td>
					</tr>
				</table>
			</div>			
		</div>
		
		<div style="float: left; position: relative; top: 150px; padding: 30px;">
			<img id="imgAvancar" src="webFiles/images/botoes/avancar_unico.gif" title="<bean:message key="prompt.adicionaraogrupo"/>" class=geralCursoHand onclick="incluirOperadores();" />
			<br/><br/>
			<img id="imgRemover" src="webFiles/images/botoes/voltar_unico.gif" title="<bean:message key="prompt.retirardogrupo"/>" class=geralCursoHand onclick="removerOperadores();" />
		</div>

		<div style="float: left; width: 490px; ">
			<!-- Chamado: 90833 - Marco Costa - 30/09/2013 -->
			<div class="principalLabel" style="clear: both; width: 100%; height: 100px;"> 			
				<bean:message key="prompt.empresa.area"/><br />
			    <html:select property="idEmprCdEmpresa" styleClass="principalObjForm" onchange="buscaAreaEmpresaSelec()">
			    	<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/> </html:option>
					<logic:present name="csCdtbEmpresaEmprVector">
						<html:options collection="csCdtbEmpresaEmprVector" labelProperty="emprDsEmpresa" property="idEmprCdEmpresa" />
					</logic:present>					
				</html:select> 
				
				<bean:message key="prompt.area" /><br />
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
				    	<tr>
				    		<td id="tdAreaSelec" name="tdAreaSelec">
								<html:select property="idAreaCdArea" styleClass="principalObjForm" onchange="filtrarOperadores()">
									<html:option value=""><bean:message key="prompt.selecioneUmaOpcao"/> </html:option>
									
									<logic:present name="csCdtbAreaAreaVector">
										<html:options collection="csCdtbAreaAreaVector" labelProperty="areaDsArea" property="idAreaCdArea" />
									</logic:present>
								</html:select>
				    		</td>
				    	</tr>
				    </table>				
				
				<bean:message key="prompt.nome" /><br />
				<html:text property="funcNmFuncionario" styleClass="principalObjForm" style="width: 450px;" />
				<img src="/plusoft-resources/images/botoes/lupa.gif" class="geralCursoHand" onclick="filtrarOperadores()" id="imgLupaOperador" title="<bean:message key="prompt.filtrar" />"  />
			</div>
			
			<!-- Chamado: 80636 - Carlos Nunes - 24/12/2012 -->
			<table cellpadding="0" cellspacing="0" border="0" style=" width: 100%;">	
				<tr>
					<td class="principalLstCab" colspan="2" height="20px" width="360">
						 &nbsp;<bean:message key="prompt.operadoresSelecionados"/>
					</td>
					<td id="checkAllAdmin" class="principalLstCab" width="40" style="cursor: pointer; " title="<bean:message key="prompt.marcarTodos" /> - Admin">
						Admin
					</td>
					<td id="checkAllAtivo" class="principalLstCab" width="35" style="cursor: pointer; " title="<bean:message key="prompt.marcarTodos" /> - Ativo">
						&nbsp;Ativo
					</td>
					<td id="checkAllReceptivo" class="principalLstCab" style="cursor: pointer; " title="<bean:message key="prompt.marcarTodos" /> - Receptivo">
						Recep
					</td>					
				</tr>
				<tr>
					<td class="principalLstCab" width="20">
						<input id="checkAllOperadores" type="checkbox" align="middle" onclick="marcarTodos(ifrmLstOperadores, 'idFuncCdFuncionario', this)" title="<bean:message key="prompt.marcarTodos" />" />
					</td>
					<td class="principalLstCab" width="340">
						 <table cellpadding="0" cellspacing="0" border="0">
						 	<tr>
						 		<td class="principalLstCab" width="140"><bean:message key="prompt.area"/></td>
						 		<td class="principalLstCab" width="200"><bean:message key="prompt.funcionario"/></td>
						 	</tr>
						 </table>
					</td>
					<td id="checkAllAdmin" class="principalLstCab" align="center" width="40" style="cursor: pointer; " title="<bean:message key="prompt.marcarTodos" /> - Admin">
						&nbsp;&nbsp;<input id="checkAllOperadoresAdmin" type="checkbox" align="middle" onclick="marcarTodos(ifrmLstOperadores, 'fupuInAdmin', this, 'Administrador');"" title="<bean:message key="prompt.marcarTodos" />" />
					</td>
					<td id="checkAllAtivo" class="principalLstCab" width="35" style="cursor: pointer; " title="<bean:message key="prompt.marcarTodos" /> - Ativo">
						&nbsp;&nbsp;&nbsp;<input id="checkAllOperadoresAtivos" type="checkbox" align="middle" onclick="marcarTodos(ifrmLstOperadores, 'fupuInAtual', this, 'Ativo');"" title="<bean:message key="prompt.marcarTodos" />" />
					</td>
					<td id="checkAllReceptivo" class="principalLstCab" style="cursor: pointer; " title="<bean:message key="prompt.marcarTodos" /> - Receptivo">
						&nbsp;&nbsp;&nbsp;<input id="checkAllOperadoresReceptivos" type="checkbox" align="middle" onclick="marcarTodos(ifrmLstOperadores, 'fupuInReceptivo', this, 'Receptivo');"" title="<bean:message key="prompt.marcarTodos" />" />
					</td>					
				</tr>
	 		</table>
			
			<iframe id="ifrmLstOperadores" name="ifrmLstOperadores" src=""
				frameborder="0" scrolling="auto" style="width: 488px; height: 285px; border: 1px solid #7EA5B8;" >
			</iframe>
			
			<div id="LayerPaginacao" style="position:relative; left:0px; top:5px; width:200px; height:21px; z-index:1">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="principalLabel" width="7%">
							<img id="imgAnt" src="webFiles/images/botoes/setaLeft.gif" width="21" height="18" title="<bean:message key='prompt.pagina.anterior'/>" class="geralCursoHand" onclick="paginarOperadores('A');">
						</td>
						<td class="principalLabel" width="30%" align="Center"><span id="vlMin">&nbsp;</span></td>
						<td class="principalLabel" width="10%" align="Center">até</td>
						<td class="principalLabel" width="30%" align="Center"><span id="vlMax">&nbsp;</span></td>
						<td class="principalLabel" width="7%">
							<img id="imgProx" src="webFiles/images/botoes/setaRight.gif" width="21" height="18" title="<bean:message key='prompt.proxima.pagina'/>" class="geralCursoHand" onclick="paginarOperadores('P');">
						</td>
						<td class="principalLabel" width="3%" >&nbsp;</td>
						<td class="principalLabel" width="13%" align="center">Total:&nbsp;</td>
						<td class="principalLabel" width="30%" align="right"><span id="nTotal">&nbsp;</span></td>
					</tr>
				</table>
			</div>
			
		</div>		
		
		<!-- End Content -->
		<img style="position: absolute; right: 25px; bottom: 50px; " id="btnGravar" src="/plusoft-resources/images/botoes/gravar.gif" <bean:message key="prompt.gravar" /> class="geralCursoHand" onclick="gravar();" />
		</td><td width="4" background="/plusoft-resources/images/linhas/VertSombra.gif"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="4" height="10"></td></tr><tr><td width="100%"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td><td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td></tr></table>
		<img style="position: absolute; bottom: 5px; right: 5px; " src="/plusoft-resources/images/botoes/out.gif" title="<bean:message key="prompt.sair" />" class="geralCursoHand" onclick="sair();" />


		<div id="LayerAguarde" style="position:absolute; left:380px; top:180px; width:199px; height:148px; z-index:1; visibility: visible; cursor: wait;"> 
		  	<div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
		</div>

		</html:form>
	</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>