<%@page contentType="text/plain; charset=ISO-8859-1" session="false" %>
<%@page import="br.com.plusoft.versao.configuracao.*"%>
<%
response.setContentType("text/plain");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

for(java.util.Iterator it = br.com.plusoft.csi.adm.action.generic.Empresas.empresas.iterator(); it.hasNext() ; ){
	br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo empresa = (br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo) it.next();
	
	java.util.List configPendente = new PlusoftConfigValidator().validatePlusoftConfig(empresa.getIdEmprCdEmpresa());
			
	out.println("-- ".concat(String.valueOf(empresa.getIdEmprCdEmpresa())).concat(" - ").concat(empresa.getEmprDsEmpresa()));
	
	for (java.util.Iterator iterator = configPendente.iterator(); iterator.hasNext();) {
		ConfiguracaoPendente configuracaoPendente = (ConfiguracaoPendente) iterator.next();
		
		out.println(configuracaoPendente.toSql().concat(";"));
	}
	if(configPendente.size()==0) out.println("-- Configurações OK");
	
	out.println("\n");
}
%>