<%@ page language="java" import="br.com.plusoft.csi.adm.helper.*" %>
<%@ page language="java" import="com.iberia.helper.*"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
	
	function atualizaManifestacao(){
		document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%= MAConstantes.TELA_CMB_MANIFESTACAO %>';
		document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = "<%=MAConstantes.ACAO_POPULACOMBO%>";
		document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmCmbManifestacao.name;
		document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	}
	
	function iniciaTela(){
		<logic:notEqual name="administracaoCsCdtbAtendpadraoAtpaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
			if(document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.length == 2){
				document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.selectedIndex = 1;
			}
		</logic:notEqual>
		atualizaManifestacao();
	}

	function mostraCampoBuscaProd(){
		document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = "<%= MAConstantes.TELA_CMB_VARIEDADE_MANIF %>";
		document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = "<%=Constantes.ACAO_CONSULTAR%>";
		document.administracaoCsCdtbAtendpadraoAtpaForm.target = this.name;
		document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	}
	
	function buscarProduto(){

		if (document.administracaoCsCdtbAtendpadraoAtpaForm['asn2DsAssuntonivel2'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}
		
		document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = "<%= MAConstantes.TELA_CMB_VARIEDADE_MANIF %>";
		document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = "<%=MAConstantes.ACAO_POPULACOMBO%>";
		document.administracaoCsCdtbAtendpadraoAtpaForm.target = this.name;
		document.administracaoCsCdtbAtendpadraoAtpaForm.submit();

	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}

</script>

<body class="principalBgrPageIFRM" text="#000000" onload="iniciaTela();">
<html:form action="/AdministracaoCsCdtbAtendpadraoAtpa.do" styleId="administracaoCsCdtbAtendpadraoAtpaForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idAsnCdAssuntoNivel" />
  <html:hidden property="idAsn1CdAssuntonivel1" />
  <html:hidden property="idMatpCdManiftipo" />
  <html:hidden property="idGrmaCdGrupomanifestacao" />
  <html:hidden property="idSugrCdSupergrupo" />
  <html:hidden property="idTpmaCdTpmanifestacao" />
  <html:hidden property="idLinhCdLinha" />
  	
	<logic:notEqual name="administracaoCsCdtbAtendpadraoAtpaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
			  <html:select property="idAsn2CdAssuntonivel2" styleClass="principalObjForm" disabled="disabled" onchange="atualizaManifestacao();">
				<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
			  	<html:options collection="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2"/>
			  </html:select>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
		  	</td>
	  	</tr>
	  </table>
  
	  <logic:present name="csCdtbProdutoAssuntoPrasVector">
		  <logic:iterate name="csCdtbProdutoAssuntoPrasVector" id="csCdtbProdutoAssuntoPrasVector">
			  <input type="hidden" name='txtLinha<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>' value='<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>'>
		  </logic:iterate>	  
	  </logic:present>

  </logic:notEqual>
	  
  <logic:equal name="administracaoCsCdtbAtendpadraoAtpaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
	  	<tr>
	  		<td width="95%">
	  			<html:text property="asn2DsAssuntonivel2" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
		  	</td>
		</tr> 	
	  </table>
  </logic:equal>

</html:form>
</body>
</html>
<logic:equal name="administracaoCsCdtbAtendpadraoAtpaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	<script>
		document.administracaoCsCdtbAtendpadraoAtpaForm['asn2DsAssuntonivel2'].focus();
	</script>
</logic:equal>