<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
var numLinha=0;

function inicio(){
	if(editCsAstbArqCargaArcaForm.laouInDelimitado.value == "true"){
		parent.document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.disabled=true;
		parent.document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.disabled=true;
	}else{
		parent.document.administracaoCsAstbArqCargaArcaForm.arcaNrInicio.disabled=false;
		parent.document.administracaoCsAstbArqCargaArcaForm.arcaNrTamanho.disabled=false;
	}

}

</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')" topmargin="0" leftmargin="0" rightmargin="0" >
<html:form styleId="editCsAstbArqCargaArcaForm" action="/AdministracaoCsAstbArqCargaArca.do"> 

<input type="hidden" name="laouInDelimitado" value="<bean:write name="administracaoCsAstbArqCargaArcaForm" property="laouInDelimitado" />">

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csAstbArqCargaArcaVector" indexId="sequencia">
  <script>numLinha++; </script>
  <tr> 
    <td class="geralCursoHand" width="4%"  >
    	 <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18" height="18" class="principalLstParMao" title="<bean:message key="prompt.excluir"/>" onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="csCdtbLayoutLaouVo.idLaouCdSequencial" />','<bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.idCaloCdSequencial" />')">
    </td>
    <td class="principalLstParMao" width="42%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbLayoutLaouVo.idLaouCdSequencial" />','<bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.idCaloCdSequencial" />')" 
      > 
      <%=acronymChar(((CsAstbArqCargaArcaVo)ccttrtVector).getCsCdtbLayoutLaouVo().getLaouDsLayout(),30)%>
    </td>
    <td class="principalLstParMao" width="31%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbLayoutLaouVo.idLaouCdSequencial" />','<bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.idCaloCdSequencial" />')" 
      > <input type="hidden" name="txtIdCalo" value='<bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.idCaloCdSequencial"/>'> <bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.caloDsDescricao" /> 
    </td>
    <td class="principalLstParMao" align="left" width="7%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbLayoutLaouVo.idLaouCdSequencial" />','<bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.idCaloCdSequencial" />')"
      > <input type="hidden" name="txtSequencia" value='<bean:write name="ccttrtVector" property="arcaNrSequencia"/>'> <bean:write name="ccttrtVector" property="arcaNrSequencia" /> 
    </td>   
    <td class="principalLstParMao" align="left" width="7%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbLayoutLaouVo.idLaouCdSequencial" />','<bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.idCaloCdSequencial" />')" 
      > <bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.caloInTipo" /> &nbsp;
    </td>
	<td class="principalLstParMao" align="left" width="7%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbLayoutLaouVo.idLaouCdSequencial" />','<bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.idCaloCdSequencial" />')" 
      > <bean:write name="ccttrtVector" property="arcaNrInicio" /> </td>
	<td class="principalLstParMao" align="left" width="7%" onclick="parent.parent.clearError();parent.parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbLayoutLaouVo.idLaouCdSequencial" />','<bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.idCaloCdSequencial" />')" 
      > <bean:write name="ccttrtVector" property="arcaNrTamanho" /> </td>
  </tr>
  </logic:iterate>
</table>
</html:form>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPOSDOLAYOUT_EXCLUSAO_CHAVE%>', editCsAstbArqCargaArcaForm.lixeira);
</script>


</body>
</html>