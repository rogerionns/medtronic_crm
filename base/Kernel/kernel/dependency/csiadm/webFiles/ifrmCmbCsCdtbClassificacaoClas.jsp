<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
function carregaDesc() {
	try {
		if (document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value == '') {
			parent.pessoa.style.visibility = 'hidden';
		} else {
			var inPessoa = eval('document.administracaoCsAstbMateriaDetalheMadtForm.clasInPessoa' + document.administracaoCsAstbMateriaDetalheMadtForm.idClasCdClassificacao.value + '.value');
			if (inPessoa == 'true') {
				parent.pessoa.style.visibility = 'visible';
			} else {
				parent.pessoa.style.visibility = 'hidden';
			}
		}
		parent.document.administracaoCsAstbMateriaDetalheMadtForm.idPessCdPessoa.value = '0';
		if (parent.cmbDescricao.document.administracaoCsAstbMateriaDetalheMadtForm.idDetcCdDetalheClass != null)
			parent.carregaDescricao();
	} catch (e) {}
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaDesc();">
<html:form action="/AdministracaoCsAstbMateriaDetalheMadt.do" styleId="administracaoCsAstbMateriaDetalheMadtForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idDetcCdDetalheClass" />
  
  <html:select property="idClasCdClassificacao" styleClass="principalObjForm" onchange="carregaDesc()">
    <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
    <html:options collection="csCdtbClassificacaoClasVector" property="idClasCdClassificacao" labelProperty="clasDsClassificacao" />
  </html:select>
  
  <logic:present name="csCdtbClassificacaoClasVector">
    <logic:iterate name="csCdtbClassificacaoClasVector" id="csCdtbClassificacaoClasVector">
      <input type="hidden" name="clasInPessoa<bean:write name="csCdtbClassificacaoClasVector" property="idClasCdClassificacao" />" value="<bean:write name="csCdtbClassificacaoClasVector" property="clasInPessoa" />">
    </logic:iterate>
  </logic:present>
</html:form>
</body>
</html>