<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language='javascript' src='webFiles/funcoes/TratarDados.js'></script>

<script>
 	function desabilitaCamposEmpresa(){
 		document.administracaoCsCdtbEmpresaEmprForm.idEmprCdEmpresa.disabled= true;
		document.administracaoCsCdtbEmpresaEmprForm.emprDsEmpresa.disabled= true;	
		document.administracaoCsCdtbEmpresaEmprForm.emprDhInativo.disabled= true;
	}	
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbEmpresaEmprForm" action="/AdministracaoCsCdtbEmpresaEmpr.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>		

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idEmprCdEmpresa" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2">
            <html:text style="height:20px;width:330px" property="emprDsEmpresa" styleClass="text" maxlength="60" /> 
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.codigoCorporativo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="43%" colspan="2">
          	<table width="100%" align="left" cellpading="0" cellspacing="0">
          		<tr>
          			<td width="40%">
          				<html:text property="emprDsCorporativo" styleClass="text" maxlength="20"/>
          			</td>
          			<td width="60%">
          				&nbsp;
          			</td>
          		</tr>
          	</table>
            
          </td>
          <td width="24%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.empresaLinkPlugin"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="43%" colspan="2">
          	<table width="100%" align="left" cellpading="0" cellspacing="0">
          		<tr>
          			<td width="10%">
          				<html:text property="emprDsLinkPlugin" styleClass="text" maxlength="255"/>
          			</td>          			
          		</tr>
          	</table>
            
          </td>
          <td width="24%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%" align="right" class="principalLabel"><bean:message key="prompt.tmaSegundos"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="43%" colspan="2">
          	<table width="100%" align="left" cellpading="0" cellspacing="0">
          		<tr>
          			<td width="10%">
          				<html:text property="emprNrTma" styleClass="text" maxlength="4" onkeydown="return ValidaTipo(this, 'N', event);"/>
          			</td>          			
          		</tr>
          	</table>
            
          </td>
          <td width="24%">&nbsp;</td>
        </tr>        
        <tr> 
          <td width="15%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="15%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="emprDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbEmpresaEmprForm.emprDsEmpresa.disabled= true;	
			document.administracaoCsCdtbEmpresaEmprForm.emprDhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_EMPRESA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbEmpresaEmprForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_EMPRESA_INCLUSAO_CHAVE%>')){
				desabilitaCamposEmpresa();
			}		
			document.administracaoCsCdtbEmpresaEmprForm.idEmprCdEmpresa.disabled= false;
			document.administracaoCsCdtbEmpresaEmprForm.idEmprCdEmpresa.value= '';
			document.administracaoCsCdtbEmpresaEmprForm.idEmprCdEmpresa.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_EMPRESA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_EMPRESA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbEmpresaEmprForm.imgGravar);	
				desabilitaCamposEmpresa();
			}
		</script>
</logic:equal>
</html>
<script>
	try{document.administracaoCsCdtbEmpresaEmprForm.emprDsEmpresa.focus();}
	catch(e){}	
</script>