<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

function MontaLista(){
	document.getElementById("lstArqCarga").src = "AdministracaoCsCdtbRespTabCaractProdRtcp.do?tela=administracaoLstCsCdtbRespTabCaractProdRtcp&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbTabelaCaractProdTacpVo.idTacpCdTabelaCaractProd=" + document.administracaoCsCdtbRespTabCaractProdRtcpForm['csCdtbTabelaCaractProdTacpVo.idTacpCdTabelaCaractProd'].value;
}

function desabilitaCamposRestabulada(){
	administracaoCsCdtbRespTabCaractProdRtcpForm.rtcpDsRespTabCaractProd.disabled= true;	
	administracaoCsCdtbRespTabCaractProdRtcpForm['csCdtbTabelaCaractProdTacpVo.idTacpCdTabelaCaractProd'].disabled= true;
	administracaoCsCdtbRespTabCaractProdRtcpForm.rtcpDhInativo.disabled = true; 
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(administracaoCsCdtbRespTabCaractProdRtcpForm.idRtcpRespTabCaractProd.value);
}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();MontaLista();">

<html:form styleId="administracaoCsCdtbRespTabCaractProdRtcpForm" action="/AdministracaoCsCdtbRespTabCaractProdRtcp.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> 
      <!-- ## -->
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="10%"> <html:text property="idRtcpRespTabCaractProd" styleClass="text" readonly="true" /> 
    </td>
    <td width="28%">&nbsp;</td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.tabela"/> 
      <!-- ## -->
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="csCdtbTabelaCaractProdTacpVo.idTacpCdTabelaCaractProd" styleClass="principalObjForm" onchange="MontaLista()"> 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbTabelaTabeVector" property="idTacpCdTabelaCaractProd" labelProperty="tacpDsTabelaCaractProd"/> 
      </html:select> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> 
      <!-- ## -->
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="rtcpDsRespTabCaractProd" styleClass="text" maxlength="60" /> 
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4">	
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr> 
	        <td align="right" width="83%"> 
	          <html:checkbox value="true" property="rtcpDhInativo"/>
	        </td>
	        <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
	        </td>
	      </tr>
	    </table>
    </td>
  </tr>
  <tr> 
    <td colspan="4" height="220"> <br>
      <table width="85%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="6%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="60%"> <bean:message key="prompt.descricao"/></td>
          <td class="principalLstCab" width="10%">&nbsp;</td>
          <td class="principalLstCab" width="13%"><bean:message key="prompt.inativo"/></td>
          <td class="principalLstCab" width="11%">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			//administracaoCsCdtbRespTabCaractProdRtcpForm.rtcpDsRespTabCaractProd.disabled= true;	
			//administracaoCsCdtbRespTabCaractProdRtcpForm.idTabeCdTabela.disabled= true;	
			
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
		
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RESP_TABULADA_CARACT_PROD_INCLUSAO_CHAVE%>', parent.administracaoCsCdtbRespTabCaractProdRtcpForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RESP_TABULADA_CARACT_PROD_INCLUSAO_CHAVE%>')){
				desabilitaCamposRestabulada();
			}
			
			
			//administracaoCsCdtbRespTabCaractProdRtcpForm.idRtcpRespTabCaractProd.disabled= false;
			//administracaoCsCdtbRespTabCaractProdRtcpForm.idRtcpRespTabCaractProd.value= '';
			//administracaoCsCdtbRespTabCaractProdRtcpForm.idRtcpRespTabCaractProd.disabled= true;
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RESP_TABULADA_CARACT_PROD_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_RESP_TABULADA_CARACT_PROD_ALTERACAO_CHAVE%>', parent.administracaoCsCdtbRespTabCaractProdRtcpForm.imgGravar);	
				desabilitaCamposRestabulada();
			}
			
		</script>
</logic:equal>

</html>

<script>
	try{administracaoCsCdtbRespTabCaractProdRtcpForm.idTabeCdTabela.focus();}
	catch(e){}
</script>