<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">

	function VerificaCampos(){
		if (document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrRodada.value == "0" ){
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrRodada.value = "" ;
		}
		if (document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrLigdia.value == "0" ){
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrLigdia.value = "" ;
		}
		if (document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrPergdia.value == "0" ){
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrPergdia.value = "" ;
		}
		if (document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrPergrod.value == "0" ){
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrPergrod.value = "" ;
		}
		if (document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrPergger.value == "0" ){
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrPergger.value = "" ;
		}
		if (document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrLigsemana.value == "0" ){
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrLigsemana.value = "" ;
		}								
	}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos()">

<html:form styleId="administracaoCsCdtbPcPesquisaPcpeForm" action="/AdministracaoCsCdtbPcPesquisaPcpe.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="26%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="10%"> <html:text property="idPcpeCdPcpesquisa" styleClass="text" disabled="true" /> 
    </td>
    <td width="40%">&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="pcpeDsPcPesquisa" styleClass="text" maxlength="60" /> 
    </td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%" align="right" class="principalLabel"><bean:message key="prompt.dataInicial"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><html:text property="pcpeDhInicio" styleClass="text" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" /> 
    </td>
    <td><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('administracaoCsCdtbPcPesquisaPcpeForm.pcpeDhInicio')" class="principalLstParMao">&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%" align="right" class="principalLabel"><bean:message key="prompt.dataFinal"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><html:text property="pcpeDhFinal" styleClass="text" maxlength="10" onkeypress="validaDigito(this, event)" /></td>
    <td><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('administracaoCsCdtbPcPesquisaPcpeForm.pcpeDhFinal')" class="principalLstParMao" >&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%" align="right" class="principalLabel"><bean:message key="prompt.qtdeRodadas"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><html:text property="pcpeNrRodada" styleClass="text" maxlength="5" /></td>
    <td>&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%" align="right" class="principalLabel"><bean:message key="prompt.qtdeLigacoesPorDia"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><html:text property="pcpeNrLigdia" styleClass="text" maxlength="5" /></td>
    <td>&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%" align="right" class="principalLabel"><bean:message key="prompt.qtdeLigacoesPorSemana"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><html:text property="pcpeNrLigsemana" styleClass="text" maxlength="5" /></td>
    <td>&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%" align="right" class="principalLabel"><bean:message key="prompt.qtdePerguntasPorDia"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><html:text property="pcpeNrPergdia" styleClass="text" maxlength="5" /></td>
    <td>&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%" align="right" class="principalLabel"><bean:message key="prompt.qtdePerguntasPorRodada"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><html:text property="pcpeNrPergrod" styleClass="text" maxlength="5" /></td>
    <td>&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%" align="right" class="principalLabel"><bean:message key="prompt.qtdeTotalPergunta"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><html:text property="pcpeNrPergger" styleClass="text" maxlength="5" /></td>
    <td>&nbsp;</td>
    <td width="24%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="26%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td class="principalLabel" width="40%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="83%"> <html:checkbox value="true" property="pcpeDhInativo"/></td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/></td>
        </tr>
      </table>
    </td>
    <td width="24%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeDsPcPesquisa.disabled= true;	
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeDhInativo.disabled= true;
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeDsPcPesquisa.disabled= true;
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeDhInicio.disabled= true;
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeDhFinal.disabled= true;
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrRodada.disabled= true;
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrLigdia.disabled= true;
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrPergdia.disabled= true;
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrPergrod.disabled= true;
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrPergger.disabled= true;
			document.administracaoCsCdtbPcPesquisaPcpeForm.pcpeNrLigsemana.disabled= true;			
			
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			document.administracaoCsCdtbPcPesquisaPcpeForm.idPcpeCdPcpesquisa.disabled= false;
			document.administracaoCsCdtbPcPesquisaPcpeForm.idPcpeCdPcpesquisa.value= '';
			document.administracaoCsCdtbPcPesquisaPcpeForm.idPcpeCdPcpesquisa.disabled= true;
		</script>
</logic:equal>
</html>