<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
</head>

<script language="JavaScript">
var cTela = "";
var cAcao = "";

	function atualizar(){
		
		if(administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value != ""){
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value +"@"+ administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value;
			<%}else{%>
				administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value +"@1";
				parent.document.forms[0].idAsn2CdAssuntonivel2.value = 1;
			<%}%>
		}
		else{
			administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = "0@"+ administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value;
		}
		parent.document.forms[0].idAsnCdAssuntoNivel.value = administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value;
		parent.document.forms[0].idAsn1CdAssuntonivel1.value = administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value;

		document.administracaoCsAstbComposicaoCompForm.acao.value = "<%=Constantes.ACAO_VISUALIZAR%>";
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			//document.administracaoCsAstbComposicaoCompForm.tela.value = '<%= MAConstantes.TELA_CMB_CS_CDTB_ASSUNTONIVEL2_ASN2 %>';
			parent.MontaLista(administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value);
		<%}else{%>
			//Chamado 78869 - Vinicius - Sempre levar em conta a PRAS e a PRMA
			parent.MontaLista(administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value);
			parent.MontaComboManifestacao();
		<%}%>
	}
	
	function iniciaTela(){
		<logic:notEqual name="administracaoCsAstbComposicaoCompForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
			try{
				if(document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.length == 2){
					document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.selectedIndex = 1;
				}
			}catch(e){}
		</logic:notEqual>
		atualizar();
	}

	function mostraCampoBuscaProd(){
		cTela = document.administracaoCsAstbComposicaoCompForm.tela.value;
		cAcao = document.administracaoCsAstbComposicaoCompForm.acao.value;
		document.administracaoCsAstbComposicaoCompForm.acao.value = "<%=Constantes.ACAO_CONSULTAR%>";
		document.administracaoCsAstbComposicaoCompForm.tela.value = "<%= MAConstantes.TELA_CMB_CS_CDTB_ASSUNTONIVEL1_ASN1 %>";
		administracaoCsAstbComposicaoCompForm.target = this.name;
		document.administracaoCsAstbComposicaoCompForm.submit();
		document.administracaoCsAstbComposicaoCompForm.tela.value = cTela;
		document.administracaoCsAstbComposicaoCompForm.acao.value = cAcao;
	}
	
	function buscarProduto(){

		if (document.administracaoCsAstbComposicaoCompForm['asn1DsAssuntoNivel1'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}

		document.administracaoCsAstbComposicaoCompForm.tela.value = "<%= MAConstantes.TELA_CMB_CS_CDTB_ASSUNTONIVEL1_ASN1 %>";
		document.administracaoCsAstbComposicaoCompForm.acao.value = "<%=Constantes.ACAO_VISUALIZAR%>";
		administracaoCsAstbComposicaoCompForm.target = this.name;
		document.administracaoCsAstbComposicaoCompForm.submit();

	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}

</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/AdministracaoCsAstbComposicaoComp.do" styleId="administracaoCsAstbComposicaoCompForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="idCompCdSequencial" />
	<html:hidden property="idTpinCdTipoinformacao" />
	<html:hidden property="idToinCdTopicoinformacao" />
	<html:hidden property="idLinhCdLinha" />
	<html:hidden property="idAsnCdAssuntoNivel" />
	<html:hidden property="idAsn2CdAssuntonivel2" />

	<logic:notEqual name="administracaoCsAstbComposicaoCompForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="95%">
					<html:select property="idAsn1CdAssuntonivel1" styleClass="principalObjForm" onchange="atualizar();">
						<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
						<html:options collection="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" labelProperty="csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1"/> 
					</html:select>                              
			 	</td>
			 	<td width="5%" valign="middle">
			 		<div align="right" id="divBotao"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
			 	</td>
			</tr>
		</table>
	</logic:notEqual>
	  
	<logic:equal name="administracaoCsAstbComposicaoCompForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
			<tr>
				<td width="95%">
					<html:text property="asn1DsAssuntoNivel1" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
				</td>
				<td width="5%" valign="middle">
					<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
				</td>
			</tr> 	
		</table>
		<html:hidden property="idAsn1CdAssuntonivel1" />
	</logic:equal>
</html:form>
</body>
</html>
<logic:equal name="administracaoCsAstbComposicaoCompForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	<script>
		document.administracaoCsAstbComposicaoCompForm['asn1DsAssuntoNivel1'].focus();
	</script>
</logic:equal>
