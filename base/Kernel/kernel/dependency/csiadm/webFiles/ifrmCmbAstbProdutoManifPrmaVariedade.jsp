<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>
function inicio() {
	try{
		//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 
		if (document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.length == 2){
			document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2[1].selected = true;
		}
	}catch(e){}
	parent.validaChkCombo();
	//atualizaManifestacao();
}	

function desabilitaLinha() {
	if (parent.ifrmCmbLinhaLinh.document.readyState != 'complete') {
		t = setTimeout ("desabilitaLinha()",100);
	}
	else {
		parent.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.disabled = true;
		try { clearTimeout(t); } catch(e){}
	}
}

//Atualiza o combo de manifesta��o
//function atualizaManifestacao(){
//	try{
//      parent.validaChkCombo();
//		administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
//		administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_MANIFESTACAO %>';
//		administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbManifestacao.name;
//		administracaoCsAstbProdutoManifPrmaForm.submit();
//	}
//	catch(x){
//		//Controle para evitar dar erro na tela por nao ter carregado os objetos dos outros iframes
//		setTimeout("atualizaManifestacao();", 5000);
//	}
//}

	function mostraCampoBuscaProd(){
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = "<%=Constantes.ACAO_CONSULTAR%>";
		document.administracaoCsAstbProdutoManifPrmaForm.submit();
	}
	
	function buscarProduto(){

		if (document.administracaoCsAstbProdutoManifPrmaForm['asn2DsAssuntoNivel2'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}
		
		document.administracaoCsAstbProdutoManifPrmaForm.tela.value = "<%= MAConstantes.TELA_CMB_PRMA_VARIEDADE %>";
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = "<%=MAConstantes.ACAO_POPULACOMBO%>";
		
		document.administracaoCsAstbProdutoManifPrmaForm.submit();

	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}
	
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); inicio();">
<html:form action="/AdministracaoCsAstbProdutoManifPrma.do" styleId="administracaoCsAstbProdutoManifPrmaForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="idLinhCdLinha" />	
	<html:hidden property="idMatpCdManifTipo" />
	<html:hidden property="idTpmaCdTpManifestacao" />
	<html:hidden property="idGrmaCdGrupoManifestacao" />
	<html:hidden property="idAsn1CdAssuntoNivel1" />
	
	<logic:notEqual name="administracaoCsAstbProdutoManifPrmaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
				<html:select property="idAsn2CdAssuntoNivel2" styleClass="principalObjForm" > 
					<html:option value="0"> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
					<html:options collection="combo" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2"/> 
				</html:select>                              
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
		  	</td>
	  	</tr>
	  </table>
	  
		  <logic:present name="combo">
			  <logic:iterate name="combo" id="csCdtbAssuntoNivel2Asn2Vector">
				  <input type="hidden" name='txtAsn2<bean:write name="csCdtbAssuntoNivel2Asn2Vector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>' value='<bean:write name="csCdtbAssuntoNivel2Asn2Vector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>'>
			  </logic:iterate>	  
		  </logic:present>
	  
	  </logic:notEqual>
	  
   	  <logic:equal name="administracaoCsAstbProdutoManifPrmaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
	  	<tr>
	  		<td width="95%">
	  			<html:text property="asn2DsAssuntoNivel2" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
		  	</td>
		</tr> 	
	  </table>
	  <script>
		//document.administracaoCsCdtbProdutoAssuntoPrasForm['idAsn2CdAssuntoNivel2'].select();
	  </script>
   	  </logic:equal>

</html:form>
	<script>
		if (parent.getFlagProduto() == 1){			
			//desabilitaLinha();
			try{
				window.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.disabled = true;
			}catch(e){}
		}
	</script>
</body>
</html>
<logic:equal name="administracaoCsAstbProdutoManifPrmaForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	<script>
		document.administracaoCsAstbProdutoManifPrmaForm['asn2DsAssuntoNivel2'].focus();
	</script>
</logic:equal>