<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>
</head>

<script>
function submeteExcluirEmail(idVirtualEmail, idMafoCdMarcaFornecedor,idEmmaCdEmailmarca, emmaDsEmail) {
	if(confirm('<bean:message key="prompt.Deseja_remover_esse_item" />'))
	{
		ifrmLstEmail.location.href = "AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_LST_EMAIL%>&acao=<%= Constantes.ACAO_EXCLUIR%>&idMafoCdMarcaFornecedor=" + idMafoCdMarcaFornecedor + "&idEmmaCdEmailmarca=" + idEmmaCdEmailmarca + "&idVirtualEmail=" + idVirtualEmail +"&emmaDsEmail="+ emmaDsEmail;
	}
}

function submeteFormEditEmail(idVirtualEmail, idMafoCdMarcaFornecedor, idEmmaCdEmailmarca, emmaDsEmail, sequencia) {
	
	administracaoCsCdtbMarcafornecedorMafoForm.idVirtualEmail.value = idVirtualEmail;
	administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value = idMafoCdMarcaFornecedor;
	administracaoCsCdtbMarcafornecedorMafoForm.idEmmaCdEmailmarca.value = idEmmaCdEmailmarca;
	administracaoCsCdtbMarcafornecedorMafoForm.emmaDsEmail.value = emmaDsEmail;
	
	if (ifrmLstEmail.contEmails==1){
		administracaoCsCdtbMarcafornecedorMafoForm.emmaTxObservacao.value = ifrmLstEmail.document.forms[0].txtObsevacao.value;
	}else{
		administracaoCsCdtbMarcafornecedorMafoForm.emmaTxObservacao.value = ifrmLstEmail.document.forms[0].txtObsevacao[sequencia].value;
	}
	
	var targetOrig = administracaoCsCdtbMarcafornecedorMafoForm.target;
	var telaOrig = administracaoCsCdtbMarcafornecedorMafoForm.tela.value;
	var acaoOrig = administracaoCsCdtbMarcafornecedorMafoForm.acao.value; 
	 
	administracaoCsCdtbMarcafornecedorMafoForm.target = "ifrmLstEmail";
	administracaoCsCdtbMarcafornecedorMafoForm.tela.value = '<%= MAConstantes.IFRM_LST_EMAIL%>';
	administracaoCsCdtbMarcafornecedorMafoForm.acao.value = '<%= Constantes.ACAO_EDITAR%>';
	
	administracaoCsCdtbMarcafornecedorMafoForm.submit();
	
	administracaoCsCdtbMarcafornecedorMafoForm.target = targetOrig;
	administracaoCsCdtbMarcafornecedorMafoForm.tela.value = telaOrig;
	administracaoCsCdtbMarcafornecedorMafoForm.acao.value = acaoOrig;
	
	//ifrmLstEmail.location.href = "AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<!%= MAConstantes.IFRM_LST_EMAIL%>&acao=<!%= Constantes.ACAO_EDITAR%>&idMafoCdMarcaFornecedor=" + idMafoCdMarcaFornecedor + "&idEmmaCdEmailmarca=" + idEmmaCdEmailmarca + "&emmaDsEmail=" + emmaDsEmail + "&idVirtualEmail=" + idVirtualEmail;
}

function limparTela()
{
	administracaoCsCdtbMarcafornecedorMafoForm.idEmmaCdEmailmarca.value = "0";
	administracaoCsCdtbMarcafornecedorMafoForm.emmaDsEmail.value = "";
	administracaoCsCdtbMarcafornecedorMafoForm.emmaTxObservacao.value = "";
	administracaoCsCdtbMarcafornecedorMafoForm.idVirtualEmail.value = "0";
}


function submeteIncluirEmail() {
	
	emmaDsEmail = administracaoCsCdtbMarcafornecedorMafoForm.emmaDsEmail.value;
	
	if(trim(emmaDsEmail)!= "")
	{
		idMafoCdMarcaFornecedor = parent.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value;
		idVirtualEmail = ifrmLstEmail.contEmails;
		idEmmaCdEmailmarca = administracaoCsCdtbMarcafornecedorMafoForm.idEmmaCdEmailmarca.value;	
		var emmaTxObservacao = administracaoCsCdtbMarcafornecedorMafoForm.emmaTxObservacao.value;
		
		emmaInPrincipal = "";
		
		if (administracaoCsCdtbMarcafornecedorMafoForm.idVirtualEmail.value == "0"){
			idVirtualEmail = ifrmLstEmail.contEmails;
			if(idVirtualEmail == 0)
			{					
				idVirtualEmail = 1;	
			}else
				idVirtualEmail++;
		}else{
			idVirtualEmail = administracaoCsCdtbMarcafornecedorMafoForm.idVirtualEmail.value;
		}		
		
		url  = "AdministracaoCsCdtbMarcafornecedorMafo.do";
		url += "?acao=<%= Constantes.ACAO_INCLUIR%>";
		url += "&tela=<%= MAConstantes.IFRM_LST_EMAIL%>";
		
		url += "&idMafoCdMarcaFornecedor=" + idMafoCdMarcaFornecedor;
		url += "&idEmmaCdEmailmarca=" + idEmmaCdEmailmarca;
		url += "&idVirtualEmail=" + idVirtualEmail;
		url += "&emmaDsEmail=" + emmaDsEmail;
		url += "&emmaTxObservacao=" + emmaTxObservacao;
		
		if(emmaInPrincipal != null)
		{
			url += "&emmaInPrincipal=" + emmaInPrincipal;
		}
		
		ifrmLstEmail.document.location.href = url;
		
		limparTela();
	}
	else
	{
		alert('<bean:message key="prompt.alert.E_necessario_preencher_email"/>');
	}
}


function validaEmail(email)
{
	if (email.value.search(/\S/) != -1) {
		regExp = /[A-Za-z0-9_]+@[A-Za-z0-9_-]{2,}\.[A-Za-z]{2,}/
		if (email.value.length < 7 || email.value.search(regExp) == -1){
			alert ("<bean:message key="prompt.Por_favor_preencha_corretamente_o_seu_E_Mail"/>.");
		    email.focus();
		    return false;
		}						
	}
	num1 = email.value.indexOf("@");
	num2 = email.value.lastIndexOf("@");
	if (num1 != num2){
	    alert ("<bean:message key="prompt.Por_favor_preencha_corretamente_o_seu_E_Mail"/>.");
	    email.focus();
		return false;
	}
}

function validaDigitoEmail(obj, evnt){
	if(evnt.keyCode == 32){
		evnt.returnValue = null;
		return null;
	}

	if(evnt.keyCode == 13) {
		document.getElementById('bt_adicionar').click();
				
		evnt.returnValue = null;
		return false;
	}
}

</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
	
<html:form action="/AdministracaoCsCdtbMarcafornecedorMafo.do" styleId="administracaoCsCdtbMarcafornecedorMafoForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	
	<html:hidden property="idMafoCdMarcaFornecedor" />
	<html:hidden property="idEmmaCdEmailmarca" />
	<html:hidden property="idVirtualEmail" />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr> 
             <td>&nbsp;</td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">  
         	<tr>        
             <td width="12%" class="principalLabel" align="right">
             		<bean:message key="prompt.eMail"/> 
             		<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> &nbsp;
             </td>
             <td width="45%"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr> 
                   <td width="94%" height="22"> 
                   	<html:text property="emmaDsEmail" styleClass="text" maxlength="100" onblur="validaEmail(this)" onkeydown="return validaDigitoEmail(this, event);" />
                   </td>
                   <td width="6%">
					  <img name="bt_adicionar" id="bt_adicionar" src="webFiles/images/botoes/setaDown.gif" title="<bean:message key="prompt.confirma"/>" width="21" height="18" class="geralCursoHand" onclick="submeteIncluirEmail();">
				   </td>
                 </tr>
               </table>
             </td>
             <td width="43%"></td>
           </tr>
                 <tr>
                 	<td width="12%">&nbsp;</td>
                 	<td width="45%" class="principalLabel"><bean:message key="prompt.observacao"/></td>
                 	<td width="43%">&nbsp;</td>
                 </tr>
                 <tr>
                 	<td width="12%">&nbsp;</td>
                 	<td width="45%" align="left">
                 		<html:textarea property="emmaTxObservacao" styleClass="text" rows="3" onkeyup="textCounter(this, 1000)" onblur="textCounter(this, 1000)" />
                 	</td>
                 	<td width="43%">&nbsp;</td>
                 </tr>
           <tr> 
             <td colspan="3" class="principalLabel" align="right">&nbsp; </td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
           <tr> 
           	 <td width="4%" class="principalLstCab">&nbsp;</td>
             <td width="96%" class="principalLstCab">E-mail</td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="160">
           <tr> 
             <td valign="top"> 
                 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                   <tr> 
                     <td height="160">
                     	<iframe id=ifrmLstEmail  name="ifrmLstEmail" src="AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_LST_EMAIL%>&acao=none" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidtd="0" marginheight="0" ></iframe>
                     </td>                     
                   </tr>
                 </table>
             </td>
           </tr>
         </table>
</html:form>
</body>
</html>