<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.produtoacaom" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
var wi = (window.dialogArguments?window.dialogArguments:window.opener);

function submeteSalvar() {
	wi.document.all('incluirIdAsnCdAssuntoNivel').value = '';
	if (document.all('incluirIdAsnCdAssuntoNivel') != null) {
		if (document.all('incluirIdAsnCdAssuntoNivel').length == undefined) {
			if (document.all('incluirIdAsnCdAssuntoNivel').checked)
				wi.document.all('incluirIdAsnCdAssuntoNivel').value = document.all('incluirIdAsnCdAssuntoNivel').value;
		} else {
			for (var i = 0; i < document.all('incluirIdAsnCdAssuntoNivel').length; i++) {
				if (document.all('incluirIdAsnCdAssuntoNivel')[i].checked)
					wi.document.all('incluirIdAsnCdAssuntoNivel').value += document.all('incluirIdAsnCdAssuntoNivel')[i].value + ",";
			}
		}
	}
	wi.document.all('excluirIdAsnCdAssuntoNivel').value = '';
	if (document.all('excluirIdAsnCdAssuntoNivel') != null) {
		if (document.all('excluirIdAsnCdAssuntoNivel').length == undefined) {
			if (document.all('excluirIdAsnCdAssuntoNivel').checked == false)
				wi.document.all('excluirIdAsnCdAssuntoNivel').value = document.all('excluirIdAsnCdAssuntoNivel').value;
		} else {
			for (var i = 0; i < document.all('excluirIdAsnCdAssuntoNivel').length; i++) {
				if (document.all('excluirIdAsnCdAssuntoNivel')[i].checked == false)
					wi.document.all('excluirIdAsnCdAssuntoNivel').value += document.all('excluirIdAsnCdAssuntoNivel')[i].value + ",";
			}
		}
	}
	window.close();
}

function marcaCheck() {
	if (wi.document.all('incluirIdAsnCdAssuntoNivel').value.length > 0) {
		var checks = wi.document.all('incluirIdAsnCdAssuntoNivel').value.split(',');
		for (var i = 0; i < checks.length - 1; i++) {
			checkProdutos(checks[i]);
		}
	}
	if (wi.document.all('excluirIdAsnCdAssuntoNivel').value.length > 0) {
		var checks = window.dialogArguments.document.all('excluirIdAsnCdAssuntoNivel').value.split(',');
		for (var i = 0; i < checks.length - 1; i++) {
			uncheckProdutos(checks[i]);
		}
	}
}

function checkProdutos(codigoPai) {
	if (document.all('incluirIdAsnCdAssuntoNivel') != null) {
		if (document.all('incluirIdAsnCdAssuntoNivel').length == undefined) {
			if (document.all('incluirIdAsnCdAssuntoNivel').value == codigoPai)
				document.all('incluirIdAsnCdAssuntoNivel').checked = true;
		} else {
			for (var i = 0; i < document.all('incluirIdAsnCdAssuntoNivel').length; i++) {
				if (document.all('incluirIdAsnCdAssuntoNivel')[i].value == codigoPai)
					document.all('incluirIdAsnCdAssuntoNivel')[i].checked = true;
			}
		}
	}
}

function uncheckProdutos(codigoPai) {
	if (document.all('excluirIdAsnCdAssuntoNivel') != null) {
		if (document.all('excluirIdAsnCdAssuntoNivel').length == undefined) {
			if (document.all('excluirIdAsnCdAssuntoNivel').value == codigoPai)
				document.all('excluirIdAsnCdAssuntoNivel').checked = false;
		} else {
			for (var i = 0; i < document.all('excluirIdAsnCdAssuntoNivel').length; i++) {
				if (document.all('excluirIdAsnCdAssuntoNivel')[i].value == codigoPai)
					document.all('excluirIdAsnCdAssuntoNivel')[i].checked = false;
			}
		}
	}
}

</script>
<script language="JavaScript">
nLinha = new Number(0);

function adicionarProd(){
	if (document.administracaoCsAstbAcaoProgramaAcpgForm.idAsnCdAssuntoNivel.value == "") {
		alert("<bean:message key="prompt.Por_favor_selecione_um_produto" />");
		document.administracaoCsAstbAcaoProgramaAcpgForm.idAsnCdAssuntoNivel.focus();
		return false;
	}

	addProd(document.administracaoCsAstbAcaoProgramaAcpgForm.idAsnCdAssuntoNivel.value, document.administracaoCsAstbAcaoProgramaAcpgForm.idAsnCdAssuntoNivel[document.administracaoCsAstbAcaoProgramaAcpgForm.idAsnCdAssuntoNivel.selectedIndex].text, false);
}

function addProd(cProduto, nProduto, banco) {
	if (comparaChave(cProduto)) {
		return false;
	}
	nLinha = nLinha + 1;
	
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
	strTxt += "	  <tr> ";
	if (!banco) {
		strTxt += "       <input type=\"hidden\" name=\"idPrasCdProdutoAssunto\" value=\"" + cProduto + "\" > ";
		strTxt += "       <input type=\"hidden\" name=\"prasDsProdutoAssunto\" value=\"" + nProduto + "\" > ";
		strTxt += "     <td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeProd(\"" + nLinha + "\")></td> ";
	} else {
		strTxt += "     <td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=\"removeProdBD('" + nLinha + "', '" + cProduto + "')\"></td> ";
	}
	strTxt += "     <td class=principalLstPar width=95%> ";
	strTxt += nProduto;
	strTxt += "     </td> ";
	strTxt += "	  </tr> ";
	strTxt += " </table> ";
	
	document.getElementById('lstProduto').innerHTML += strTxt;
}

function removeProd(nTblExcluir) {
	msg = '<bean:message key="prompt.Deseja_excluir_esse_produto" />';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstProduto.removeChild(objIdTbl);
	}
}

function removeProdBD(nTblExcluir, cProduto) {
	msg = '<bean:message key="prompt.Deseja_excluir_esse_produto" />';
	if (confirm(msg)) {
		objIdTbl = window.document.all.item(nTblExcluir);
		lstProduto.removeChild(objIdTbl);
		strTxt = "<input type=\"hidden\" name=\"produtosExcluidos\" value=\"" + cProduto + "\" > ";

		document.getElementById('lstProduto').innerHTML += strTxt;
	}
}

function comparaChave(cProduto) {
	try {
		if (document.administracaoCsAstbAcaoProgramaAcpgForm.idPrasCdProdutoAssunto.length != undefined) {
			for (var i = 0; i < document.administracaoCsAstbAcaoProgramaAcpgForm.idPrasCdProdutoAssunto.length; i++) {
				if (document.administracaoCsAstbAcaoProgramaAcpgForm.idPrasCdProdutoAssunto[i].value == cProduto) {
					alert('<bean:message key="prompt.Esse_produto_ja_existe" />');
					return true;
				}
			}
		} else {
			if (document.administracaoCsAstbAcaoProgramaAcpgForm.idPrasCdProdutoAssunto.value == cProduto) {
				alert('<bean:message key="prompt.Esse_produto_ja_existe" />');
				return true;
			}
		}
	} catch (e){}
	return false;
}

function submeteForm() {
	document.administracaoCsAstbAcaoProgramaAcpgForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	document.administracaoCsAstbAcaoProgramaAcpgForm.tela.value = '<%=MAConstantes.TELA_LST_CS_ASTB_PRODUTOACAO_PRAC%>';
	document.administracaoCsAstbAcaoProgramaAcpgForm.target = this.name = 'produtoAcao';
	document.administracaoCsAstbAcaoProgramaAcpgForm.submit();
	passaTempo();
}

function passaTempo() {
	try {
		for (var i = 0; i < 500000; i++) {
		}
	} catch(e) {}
}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="0" topmargin="5" onload="showError('<%=request.getAttribute("msgerro")%>');" onunload="submeteForm()">
<html:form styleId="administracaoCsAstbAcaoProgramaAcpgForm" action="/AdministracaoCsAstbAcaoProgramaAcpg.do">
<html:hidden property="acao" />
<html:hidden property="tela" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="250">
  <tr>
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.produtoacao" /> </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
    <br>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalLabel" valign="top" align="center" colspan="2">
      <table width="100%">
        <tr>
          <td width="10%" class="principalLabel">
            <bean:message key="prompt.produto" />
          </td>
          <td width="85%">
            <html:select property="idAsnCdAssuntoNivel" styleClass="principalObjForm">
              <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
              <logic:present name="csCdtbProdutoAssuntoPrasVector">
                <html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto" />
              </logic:present>
            </html:select>
          </td>
          <td width="5%">
            <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarProd()">
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="2" class="principalLabel"> 
      <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLstCab" width="100%">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.produto" />
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalLabel" valign="top" align="center"> 
      <div id="lstProduto" style="width:100%; height:170px; z-index:3; overflow: auto; visibility: visible">
          <logic:present name="csAstbProdutoAcaoPracBancoVector">
            <logic:iterate name="csAstbProdutoAcaoPracBancoVector" id="capapVector">
              <script>
                addProd('<bean:write name="capapVector" property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />', '<bean:write name="capapVector" property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" />', true);
              </script>
            </logic:iterate>
          </logic:present>
          <logic:present name="csAstbProdutoAcaoPracVector">
            <logic:iterate name="csAstbProdutoAcaoPracVector" id="capapVector">
              <script>
                addProd('<bean:write name="capapVector" property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />', '<bean:write name="capapVector" property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" />', false);
              </script>
            </logic:iterate>
          </logic:present>
      </div>
    </td>
  </tr>
  </table>
  </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="245"></td>
  </tr>
  <tr> 
    <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar" />" onClick="javascript:window.close()" class="geralCursoHand">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>