<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
    response.setHeader("Cache-Control","no-cache");

	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	

	long idEmpresa = empresaVo.getIdEmprCdEmpresa();
	
	final boolean CONF_SEMLINHA = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S");
	final boolean CONF_VARIEDADE = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S");
	
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function submitPaginacao(regDe,regAte){
	
	document.administracaoCsAstbComposicaoCompForm.regDe.value=regDe;
	document.administracaoCsAstbComposicaoCompForm.regAte.value=regAte;
	filtrar();
	
}

function filtrar(){

	window.top.document.getElementById("LayerAguarde").style.visibility = "visible";
	
	<%if (CONF_SEMLINHA) {%>
		document.administracaoCsAstbComposicaoCompForm.idLinhCdLinha.value = 1;
	<%}%>

	document.administracaoCsAstbComposicaoCompForm.target = admIframe.name;
	document.administracaoCsAstbComposicaoCompForm.acao.value ='filtrar';
	document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_COMPOSICAO_COMP%>';
	
	document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = ifrmCmbProdutoAssunto.document.getElementById("administracaoCsAstbComposicaoCompForm").idAsnCdAssuntoNivel.value;
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
	document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value = cmbVariedade.document.forms[0].idAsn2CdAssuntonivel2.value;
	<% } %>
	
	document.administracaoCsAstbComposicaoCompForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
	
	document.administracaoCsAstbComposicaoCompForm.regDe.value='0';
	document.administracaoCsAstbComposicaoCompForm.regAte.value='0';
	
	initPaginacao();
}

function limpaCampoFiltro(){
	//ifrmCmbProdutoAssunto.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = '';
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
		//cmbVariedade.document.forms[0].idAsn2CdAssuntonivel2.value = '';
	<% } %>
	
	<%if (!CONF_SEMLINHA) {%>
		//document.administracaoCsAstbComposicaoCompForm.idLinhCdLinha.value = '';
	<% } %>
	
	//document.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.value = '';
	//document.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.value = '';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsAstbComposicaoCompForm.target = editIframe.name;
	editIframe.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_COMPOSICAO_COMP%>';
	editIframe.document.administracaoCsAstbComposicaoCompForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsAstbComposicaoCompForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteExcluir(codigo) {
	
	tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.value = codigo;
	tab.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_COMPOSICAO_COMP%>';
	tab.document.administracaoCsAstbComposicaoCompForm.target = editIframe.name;
	tab.document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.disabled = false;
	tab.document.administracaoCsAstbComposicaoCompForm.submit();
	tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.disabled = true;
	//AtivarPasta(editIframe);
	//MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo){
	tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.value = codigo;
	tab.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_COMPOSICAO_COMP%>';
	tab.document.administracaoCsAstbComposicaoCompForm.target = editIframe.name;
	tab.document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.disabled = false;
	tab.document.administracaoCsAstbComposicaoCompForm.submit();
	tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.disabled = true;
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEditPras(idAsn1, idAsn2){
	tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.value = 0;
	tab.document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value = idAsn1;
	tab.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value = idAsn2;
	tab.document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = idAsn1 + "@" + idAsn2;
	tab.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_COMPOSICAO_COMP%>';
	tab.document.administracaoCsAstbComposicaoCompForm.target = editIframe.name;
	tab.document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsAstbComposicaoCompForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormInativar() {
	var check = false;
	if (editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idCompCdInativos != null) {		
		if (editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idCompCdInativos.length == undefined) {
			if (editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idCompCdInativos.checked) {
				check = true;
			}
		} else {
			for (var i = 0; i < editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idCompCdInativos.length; i++) {
				if (editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idCompCdInativos[i].checked)
					check = true;
			}
		}
		if (check == false) {
			alert("<bean:message key="prompt.Selecione_pelo_menos_um_item_para_inativa-lo"/>");
		} else {
			editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
			editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_LST_CS_ASTB_COMPOSICAO_COMP%>';
			editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = editIframe.document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value;
			editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value = editIframe.document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value;
			editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value = editIframe.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value;
			editIframe.lstArqCarga.document.administracaoCsAstbComposicaoCompForm.submit();
		}
	} else {
		alert("<bean:message key="prompt.Nao_existe_nenhum_item_para_inativar"/>");
	}
}

function submeteSalvar(){

	if(tab.document.administracaoCsAstbComposicaoCompForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_COMPOSICAO_COMP%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/>.');
	}else if(tab.document.administracaoCsAstbComposicaoCompForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_ASTB_COMPOSICAO_COMP%>'){
	
	<%if (!CONF_SEMLINHA) {%>
		if (tab.document.administracaoCsAstbComposicaoCompForm.idLinhCdLinha.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_uma_Linha"/>.");
			tab.document.administracaoCsAstbComposicaoCompForm.idLinhCdLinha.focus();
			return false;
		}
		if (!tab.document.getElementById("chkProdutosLinha").checked && tab.document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_um_produto_assunto"/>.");
			tab.document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.focus();
			return false;
		}
	<%}%>
		if (tab.ifrmCmbTpin.document.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.value == "" && !tab.administracaoCsAstbComposicaoCompForm.chkTiposInformacao.checked) {
			alert("<bean:message key="prompt.Por_favor_selecione_um_tipo_de_informacao"/>");
			tab.ifrmCmbTpin.document.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.focus();
			return false;
		}
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			<%if (!CONF_SEMLINHA) {%>
				if (!tab.document.getElementById("chkVariedadesProduto").checked && !tab.document.getElementById("chkProdutosLinha").checked && tab.cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value == "") {
			<%}else{%> 
				if (!tab.document.getElementById("chkVariedadesProduto").checked && tab.cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value == "") {
			<%}%>
					alert("<bean:message key="prompt.Por_favor_selecione_uma_variedade"/>.");
					tab.cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.focus();
					return false;
				}
		<%}%>
		
		if (tab.ifrmCmbToin.document.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.value == "" && !tab.administracaoCsAstbComposicaoCompForm.chkTopicosInformacao.checked) {
			alert("<bean:message key="prompt.Por_favor_selecione_um_topico_de_informacao"/>.");
			tab.ifrmCmbToin.document.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.focus();
			return false;
		}/*
		if (tab.document.administracaoCsAstbComposicaoCompForm.idTpmaCdTpManifestacao.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_um_tipo_de_manifestacao"/>.");
			tab.document.administracaoCsAstbComposicaoCompForm.idTpmaCdTpManifestacao.focus();
			return false;
		}*/
		
		//valdeci, limitar os checks para evitar que o processo fique muito grande
		
		var nCountEnabled = 0;

		<%if(!CONF_SEMLINHA){%>
			if(!tab.document.administracaoCsAstbComposicaoCompForm.idLinhCdLinha.disabled){
				nCountEnabled++;
			}
		<%}%>
		
		if(!tab.document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.disabled){
			nCountEnabled++;
		}
		
		<%if(CONF_VARIEDADE){%>
			if(!tab.cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.disabled){
				nCountEnabled++;
			}
		<%}%>
		
		if(!tab.ifrmCmbTpin.document.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.disabled){
			nCountEnabled++;
		}
		if(!tab.ifrmCmbToin.document.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.disabled){
			nCountEnabled++;
		}

		if(nCountEnabled < 2){
			alert("<bean:message key='prompt.selecioneOsCombos'/>.");
			return false;
		}
		
		if (tab.document.administracaoCsAstbComposicaoCompForm.compTxInformacao.value == "") {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			//tab.document.administracaoCsAstbComposicaoCompForm.compTxInformacao.focus();
			return false;
		}

		//if ((tab.document.administracaoCsAstbComposicaoCompForm.compTxInformacao.value != "") && (tab.document.administracaoCsAstbComposicaoCompForm.inarDsPatharquivo.value != "") && (tab.document.administracaoCsAstbComposicaoCompForm.infoArquivoExcluidos.value != "")){
			editIframe.PreencheLstPathArq();
		//}
		
		tab.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_COMPOSICAO_COMP%>';
		
		//Henrique (17/02/2006) Pois quando o usuario seleciona a linha o acao do sistema muda para visualizar
		if(tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.value > 0){
			tab.document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_EDITAR%>'
		}
		else{ 
			tab.document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>'
		}
		
		tab.document.administracaoCsAstbComposicaoCompForm.target = admIframe.name;
		tab.document.administracaoCsAstbComposicaoCompForm.idTpmaCdTpManifestacao.value = editIframe.cmbTpManifestacao.document.administracaoCsAstbComposicaoCompForm.idTpmaCdTpManifestacao.value;
		tab.document.administracaoCsAstbComposicaoCompForm.idGrmaCdGrupoManifestacao.value = editIframe.cmbGrupo.document.administracaoCsAstbComposicaoCompForm.idGrmaCdGrupoManifestacao.value;
		tab.document.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.value = editIframe.ifrmCmbTpin.document.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.value;
		tab.document.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.value = editIframe.ifrmCmbToin.document.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.value;

		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			tab.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value = editIframe.cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value;
		<%}%>
											      			
		
		//tab.document.administracaoCsAstbComposicaoCompForm.idMatpCdManifTipo.value = editIframe.document.administracaoCsAstbComposicaoCompForm.idMatpCdManifTipo.value;
		
		window.top.document.getElementById("LayerAguarde").style.visibility = "visible";
		disableEnable(tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial, false);
		
		tab.document.administracaoCsAstbComposicaoCompForm.submit();
		disableEnable(tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial, true);
		//cancel();
		setTimeout("editIframe.lstArqCarga.location.reload()", 2500);
		
		<%if (!CONF_SEMLINHA) {%>
		//	editIframe.document.administracaoCsAstbComposicaoCompForm.idLinhCdLinha.value= '';
		<%}%>
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		//	editIframe.cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value = '';
		<%}%>
		
		editIframe.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.value= '';
	//	editIframe.document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value='';
		editIframe.document.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.value= '';
		editIframe.document.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.value= '';
		editIframe.document.administracaoCsAstbComposicaoCompForm.idMatpCdManifTipo.value= '';

		editIframe.document.getElementById('Layer2').innerHTML="";
		editIframe.document.administracaoCsAstbComposicaoCompForm.compTxInformacao.value='';
		
		editIframe.document.administracaoCsAstbComposicaoCompForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value= '';		
	//	editIframe.cmbGrupo.document.administracaoCsAstbComposicaoCompForm.idGrmaCdGrupoManifestacao.value= '';
	//	editIframe.cmbTpManifestacao.document.administracaoCsAstbComposicaoCompForm.idTpmaCdTpManifestacao.value= '';
	//	editIframe.cmbGrupo.carregaTpManifestacao();
		editIframe.carregaGrupo();
		editIframe.document.administracaoCsAstbComposicaoCompForm.compTxInformacao.value= '';
		editIframe.document.administracaoCsAstbComposicaoCompForm.compDhInativo.checked= false;
		
		editIframe.document.administracaoCsAstbComposicaoCompForm.compDhElaboracao.value= '';
		editIframe.document.administracaoCsAstbComposicaoCompForm.compFonteRetirada.value= '';
		editIframe.document.administracaoCsAstbComposicaoCompForm.compFonteRetiradaOld.value= '';
		editIframe.document.administracaoCsAstbComposicaoCompForm.compDhElaboracao.value = '';
		
		tab.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_COMPOSICAO_COMP%>';
		
		editIframe.document.getElementById("lstPath").innerHTML = '';
	}
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_COMPOSICAO_COMP%>';
		editIframe.document.administracaoCsAstbComposicaoCompForm.target = admIframe.name;
		editIframe.document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial, false);
		editIframe.document.administracaoCsAstbComposicaoCompForm.submit();
	}
	
	disableEnable(tab.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial, true);
	setTimeout("editIframe.lstArqCarga.location.reload()", 2500);
	
	editIframe.document.administracaoCsAstbComposicaoCompForm.idCompCdSequencial.value= '';
//	editIframe.document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value='';
	editIframe.document.administracaoCsAstbComposicaoCompForm.idTpinCdTipoinformacao.value= '';
	editIframe.document.administracaoCsAstbComposicaoCompForm.idToinCdTopicoinformacao.value= '';
	editIframe.document.administracaoCsAstbComposicaoCompForm.idMatpCdManifTipo.value= '';

	editIframe.document.getElementById('Layer2').innerHTML="";
	editIframe.document.administracaoCsAstbComposicaoCompForm.compTxInformacao.value='';
	
	editIframe.document.administracaoCsAstbComposicaoCompForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value= '';
//	editIframe.cmbGrupo.document.administracaoCsAstbComposicaoCompForm.idGrmaCdGrupoManifestacao.value= '';
//	editIframe.cmbTpManifestacao.document.administracaoCsAstbComposicaoCompForm.idTpmaCdTpManifestacao.value= '';
//	editIframe.cmbGrupo.carregaTpManifestacao();
	editIframe.carregaGrupo();
	editIframe.document.administracaoCsAstbComposicaoCompForm.compTxInformacao.value= '';
	editIframe.document.administracaoCsAstbComposicaoCompForm.compDhInativo.checked= false;
	
	editIframe.document.administracaoCsAstbComposicaoCompForm.compDhElaboracao.value= '';
	editIframe.document.administracaoCsAstbComposicaoCompForm.compFonteRetirada.value= '';
	editIframe.document.administracaoCsAstbComposicaoCompForm.compFonteRetiradaOld.value= '';
	editIframe.document.administracaoCsAstbComposicaoCompForm.compDhElaboracao.value = '';
	
	tab.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_COMPOSICAO_COMP%>';
	
	editIframe.document.getElementById("lstPath").innerHTML = '';
}

function cancel(){

	editIframe.location = 'AdministracaoCsAstbComposicaoComp.do?tela=editCsAstbComposicaoComp&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsAstbComposicaoCompForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_COMPOSICAO_COMP%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsAstbComposicaoCompForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_COMPOSICAO_COMP%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			setaIdiomaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}


function MontaComboProduto(){	
	document.administracaoCsAstbComposicaoCompForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsAstbComposicaoCompForm.tela.value = 'cmbCsCdtbProdutoAssuntoPrasComp';
	document.administracaoCsAstbComposicaoCompForm.target = ifrmCmbProdutoAssunto.name;
	document.administracaoCsAstbComposicaoCompForm.submit();
}

function MontaLista(asn){
	
	//Acrescentado o carregamento da lista quando houver a TAG DE VARIEDADE
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
		cmbVariedade.location.href= "AdministracaoCsAstbComposicaoComp.do?tela=cmbCsCdtbAssuntoNivel2Asn2&acao=<%=Constantes.ACAO_VISUALIZAR%>&idAsnCdAssuntoNivel=" +  asn +"&idLinhCdLinha="+ document.administracaoCsAstbComposicaoCompForm.idLinhCdLinha.value; +"&idAsn1CdAssuntonivel1="+ asn;	
	<%}%>
   	
}

function inicio(){

	initPaginacao();
	
	//filtrar();
}


//Action Center: 15825 - 17/08/2012 - Carlos Nunes
//M�todo criado apenas para manter a compatibilidade do combo de produto dos filtros, com o combo da tela de edi��o, que utiliza a mesma jsp
function MontaComboManifestacao(){}

</script>

</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsAstbComposicaoCompForm"	action="/AdministracaoCsAstbComposicaoComp.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="idCompCdSequencial" />
	<html:hidden property="idAsnCdAssuntoNivel" />
	<html:hidden property="idAsn1CdAssuntonivel1" />
	<html:hidden property="idAsn2CdAssuntonivel2" />
	
	<html:hidden property="regDe" />
	<html:hidden property="regAte" />
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td class="principalBgrQuadro" valign="top" height="580">
			<br>
				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td valign="top" height="520">
						
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td class="principalPstQuadroLinkVazio">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalPstQuadroLinkSelecionado"
													id="tdDestinatario" name="tdDestinatario"
													onClick="AtivarPasta(admIframe);"><bean:message
														key="prompt.procurar" /></td>
												<td class="principalPstQuadroLinkNormalGrande"
													id="tdManifestacao" name="tdManifestacao"
													onClick="AtivarPasta(editIframe);"><bean:message
														key="prompt.composicaoInformacao" /></td>
											</tr>
										</table>
									</td>
									<td width="4"><img
										src="webFiles/images/separadores/pxTranp.gif" width="1"
										height="1"></td>
								</tr>
							</table>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td valign="top" class="principalBgrQuadro" height="490">
										<div name="Manifestacao" id="Manifestacao" style="position: relative; width: 100%; height: 400px; z-index: 6; display: none;">
											<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
												<tr>
													<td height="440">
														<iframe id=editIframe name="editIframe" src="AdministracaoCsAstbComposicaoComp.do?tela=editCsAstbComposicaoComp&acao=incluir" width="100%" height="440" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0"></iframe>
													</td>
												</tr>
											</table>
										</div>
										<div name="Destinatario" id="Destinatario" style="position: relative; width: 100%; height: 400px; z-index: 2;">
											<table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
												<%
												 String estiloLinha = "display:block; height: 25px;";
												 String estiloProduto = "";
												 
												 if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {
													 estiloLinha = "display:none";
													 estiloProduto = "height: 25px;";
												 }
												%>
												<tr style="<%=estiloLinha%>">
													<td width="30%" align="right" class="principalLabel"><%= getMessage("prompt.linha", request)%><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td>
														<!-- COMBO DE LINHA -->
														<html:select property="idLinhCdLinha" styleClass="principalObjForm" onchange="MontaComboProduto()"> 
															<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/> 
														</html:select>
													</td>
													<td width="19%">&nbsp;</td>
												</tr>
												
												<tr style="<%=estiloProduto%>">
													<td width="30%" align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel1", request)%><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td>
														<!-- PRODUTO ASSUNTO -->
														<iframe name="ifrmCmbProdutoAssunto" src="AdministracaoCsAstbComposicaoComp.do?acao=visualizar&tela=cmbCsCdtbProdutoAssuntoPrasComp" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
													</td>
													<td width="19%">&nbsp;</td>
												</tr>
												
												<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
												<tr> 
													<td width="30%" align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel2", request)%><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td>
													  <!-- VARIEDADE -->
													  <iframe name="cmbVariedade" src="AdministracaoCsAstbComposicaoComp.do?acao=visualizar&tela=cmbCsCdtbAssuntoNivel2Asn2" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
													</td>
													<td width="19%">&nbsp;</td>
												</tr>
											    <%}%>
											    
											    <tr>
													<td width="30%" align="right" class="principalLabel"><%= getMessage("prompt.tipoInformacao", request)%><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td>
														<html:select property="idTpinCdTipoinformacao" styleClass="principalObjForm" > 
															<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbTpinformacaoTpinVector" property="idTpinCdTipoinformacao" labelProperty="tpinDsTipoinformacao"/>
														</html:select> 
													</td>
													<td width="19%">&nbsp;</td>
												</tr>
												
												<tr>
													<td width="30%" align="right" class="principalLabel"><%= getMessage("prompt.topicoInformacao", request)%><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td>
														<html:select property="idToinCdTopicoinformacao" styleClass="principalObjForm" > 
													        <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbTopicoinformacToinVector" property="idToinCdTopicoinformacao" labelProperty="toinDsTopicoinformacao"/> 
													    </html:select> 
													</td>
													<td width="19%">
														&nbsp;<img
														src="webFiles/images/botoes/setaDown.gif" width="21"
														height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()"> 
													</td>
												</tr>
												
											</table>
											<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
												<tr>
													<td class="principalLstCab" width="10%" colspan="1">&nbsp;<bean:message key="prompt.codigo"/></td>
							                        <td class="principalLstCab" width="20%"><bean:message key="prompt.produtoAssunto"/></td>
							          				<td class="principalLstCab" width="30%"><bean:message key="prompt.topicoInformacao"/></td>
							         				<td class="principalLstCab" width="30%"><bean:message key="prompt.tipoInformacao"/></td>
							          				<td class="principalLstCab" width="10%"><bean:message key="prompt.inativo"/></td>
												</tr>
												<tr valign="top">
													<td colspan="5" height="300">
														<iframe id=admIframe name="admIframe" src="AdministracaoCsAstbComposicaoComp.do?acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="300" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0"></iframe>
													</td>
												</tr>
												<tr>
							                      	<td colspan="5" valign="top">
							                      		<table align="center">
							                      			<tr>
							                      				<td valign="top">
							                      					<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>
							                      				</td>
							                      			</tr>
							                      		</table>
							                      	</td>
												  </tr>
											</table>
										</div></td>
									<td width="4" height="490"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
								</tr>
								<tr>
									<td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
									<td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="1" height="3"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
					</tr>
				</table>
				<table border="0" cellspacing="0" cellpadding="4" align="right">
					<tr align="center">
						<td width="60" align="right"><img
							src="webFiles/images/botoes/new.gif" name="imgIncluir"
							width="14" height="16" class="geralCursoHand"
							title="<bean:message key='prompt.novo'/>"
							onclick="clearError();submeteFormIncluir()"></td>
						<td width="20"><img src="webFiles/images/botoes/gravar.gif"
							name="imgGravar" width="20" height="20" class="geralCursoHand"
							title="<bean:message key='prompt.gravar'/>"
							onclick="clearError();submeteSalvar();"></td>
						<td width="20"><img
							src="webFiles/images/botoes/cancelar.gif" width="20"
							height="20" class="geralCursoHand"
							title="<bean:message key='prompt.cancelar'/>"
							onclick="clearError();cancel();"></td>
					</tr>
				</table>
				<table align="center">
					<tr>
						<td><label id="error"> </label></td>
					</tr>
				</table></td>
			<td width="4" height="580"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="580"></td>
		</tr>
		<tr>
			<td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
			<td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		</tr>
	</table>
	
</html:form>
			
</body>

</html>

<script language="JavaScript">
	var tab = admIframe ;

	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_INCLUSAO_CHAVE%>', document.administracaoCsAstbComposicaoCompForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_INFORMACAO_COMPOSICAODEINFORMACAO_ALTERACAO_CHAVE%>')){
			document.administracaoCsAstbComposicaoCompForm.imgGravar.disabled=true;
			document.administracaoCsAstbComposicaoCompForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsAstbComposicaoCompForm.imgGravar.title='';
	}
	
	desabilitaListaEmpresas();

	setaArquivoXml("CS_ASTB_IDIOMACOMP_IDCO.xml");
	habilitaTelaIdioma();
	
</script>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>