<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>

<%@ page
	import="br.com.plusoft.csi.adm.form.*, br.com.plusoft.fw.app.Application"%>

<%@ include file="/webFiles/includes/funcoes.jsp"%>

<%
	String fileIncludeIdioma = "../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true" />

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

</head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript"
	src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>

<SCRIPT LANGUAGE="JavaScript">
function VerificaCampos(){
	if (document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.value == "0" ){
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.value = "" ;
	}
	
	if (document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrPrasoespecial.value == "0" ){
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrPrasoespecial.value = "" ;
	}
}	

var modalWin;

function MM_openBrWindow(theURL,winName,features) { //v2.0
	modalWin = window.open(theURL,winName,features);
	if (modalWin!=null && !modalWin.closed){
		//self.blur();
		modalWin.focus();
	}
}

function adicionarItem(){
	if (cmbFuncionarioCopiado.document.forms[0].idFuncCdFuncionario.value == "") {
		alert("<bean:message key="prompt.Por_favor_escolha_um_funcionario"/>");
		cmbFuncionarioCopiado.document.forms[0].idFuncCdFuncionario.focus();
		return false;
	}
	addItem(cmbFuncionarioCopiado.document.forms[0].idFuncCdFuncionario.value, cmbFuncionarioCopiado.document.forms[0].idFuncCdFuncionario[cmbFuncionarioCopiado.document.forms[0].idFuncCdFuncionario.selectedIndex].text);
}

function comparaChave(cFunc) {
	try {
		if (document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionarioCop.length != undefined) {
			for (var i = 0; i < document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionarioCop.length; i++) {
				if (document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionarioCop[i].value == cFunc) {
					alert('<bean:message key="prompt.Este_funcionario_ja_existe"/>');
					return true;
				}
			}
		} else {
			if (document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionarioCop.value == cFunc) {
				alert('<bean:message key="prompt.Este_funcionario_ja_existe"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}

nLinha = new Number(0);

function addItem(cFunc, nFunc) {
	if (comparaChave(cFunc)) {
		return false;
	}
	nLinha = nLinha + 1;

	strTxt = "";
	strTxt += "<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
    strTxt += "  <tr>";
	strTxt += "    <input type=\"hidden\" name=\"idFuncCdFuncionarioCop\" value=\"" + cFunc + "\" > ";
	strTxt += "    <td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif name=lixeira width=14 height=14 class=geralCursoHand title='<bean:message key="prompt.excluir"/>' onclick=removeItem(\"" + nLinha + "\")></td> ";
	strTxt += "    <td class=principalLstPar width=95%> ";
	if(nFunc == undefined){
		//strTxt += document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionarioCopCombo[document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionarioCopCombo.selectedIndex].text;
		strTxt += cmbFuncionarioCopiado.document.forms[0].idFuncCdFuncionario[cmbFuncionarioCopiado.document.forms[0].idFuncCdFuncionario.selectedIndex].text;
	}else{
		strTxt += nFunc;
	}
	strTxt += "    </td> ";
	strTxt += "	 </tr> ";
	strTxt += " </table> ";
	
	document.getElementById('lstFunc').innerHTML = document.getElementById('lstFunc').innerHTML + strTxt;
}

function removeItem(nTblExcluir) {
	msg = '<bean:message key="prompt.Deseja_excluir_esse_funcionario"/>';
	if (confirm(msg)) {
		objIdTbl = document.getElementById(nTblExcluir);
		document.getElementById('lstFunc').removeChild(objIdTbl);
	}
}

function carregaGrupo() {

	var acao = document.administracaoCsCdtbTpManifestacaoTpmaForm.acao.value;

<%if (Configuracoes.obterConfiguracao(
					ConfiguracaoConst.CONF_APL_SUPERGRUPO, request).equals("S")) {%>
	document.administracaoCsCdtbTpManifestacaoTpmaForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%=MAConstantes.TELA_CMB_SUPERGRUPO%>';
	document.administracaoCsCdtbTpManifestacaoTpmaForm.target = cmbSuperGrupoManif.name;
	document.administracaoCsCdtbTpManifestacaoTpmaForm.submit();
<%} else {%>	
	document.administracaoCsCdtbTpManifestacaoTpmaForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
	document.administracaoCsCdtbTpManifestacaoTpmaForm.target = cmbGrupoManifestacao.name;
	document.administracaoCsCdtbTpManifestacaoTpmaForm.submit();
<%}%>
	
	document.administracaoCsCdtbTpManifestacaoTpmaForm.acao.value = acao;
	document.administracaoCsCdtbTpManifestacaoTpmaForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_TPMANIFESTACAO_TPMA%>';
}

function adicionarBotao(){
	if (document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotao.value == "") {
		alert("<bean:message key="prompt.Por_favor_escolha_um_botao"/>");
		document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotao.focus();
		return false;
	}
	addBotao(document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotao.value, document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotao[document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotao.selectedIndex].text);
}

function comparaChaveBotao(cBotao) {
	try {
		if (document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotaoArray.length != undefined) {
			for (var i = 0; i < document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotaoArray.length; i++) {
				if (document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotaoArray[i].value == cBotao) {
					alert('<bean:message key="prompt.Este_botao_ja_existe"/>');
					return true;
				}
			}
		} else {
			if (document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotaoArray.value == cBotao) {
				alert('<bean:message key="prompt.Este_botao_ja_existe"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}

nLinha2 = new Number(0);

function addBotao(cBotao, nBotao) {
	if (comparaChaveBotao(cBotao)) {
		return false;
	}
	nLinha2 = nLinha2 + 1;
	
	strTxt = "";
	strTxt += "<table id=\"botao" + nLinha2 + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
    strTxt += "  <tr>";
	strTxt += "    <input type=\"hidden\" name=\"idBotaCdBotaoArray\" value=\"" + cBotao + "\" > ";
	strTxt += "    <td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif name=lixeira width=14 height=14 class=geralCursoHand title='<bean:message key="prompt.excluir"/>' onclick=removeBotao(\"" + nLinha2 + "\")></td> ";
	strTxt += "    <td class=principalLstPar width=95%> ";
	if(nBotao == undefined){
		strTxt +=        document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotao[document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotao.selectedIndex].text;
	}else{
		strTxt +=        nBotao;
	}	
	strTxt += "    </td> ";
	strTxt += "	 </tr> ";
	strTxt += " </table> ";
	
	document.getElementById('lstBotao').innerHTML += strTxt;
}

function removeBotao(nTblExcluir) {
	msg = '<bean:message key="prompt.Deseja_excluir_esse_botao"/>';
	if (confirm(msg)) {
		objIdTbl = document.getElementById("botao" + nTblExcluir);
		lstBotao.removeChild(objIdTbl);
	}
}

function setFunction(cRetorno, campo){
	if(campo=="document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxProcedimento"){
		Layer1.innerHTML = cRetorno;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxProcedimento.value=cRetorno;
	}else{
		Layer2.innerHTML = cRetorno;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxOrientacao.value=cRetorno;
	}
}

function desabilitaCamposTpManifestacao(){
	try{
		setPermissaoImageDisable('', document.administracaoCsCdtbTpManifestacaoTpmaForm.imgCriarCarta);
		setPermissaoImageDisable('', document.administracaoCsCdtbTpManifestacaoTpmaForm.imgSeta);
		document.administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao.disabled= true;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.idMatpCdManifTipo.disabled= true;	
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaDsTpManifestacao.disabled= true;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaCdCorporativo.disabled= true;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.disabled= true;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaInTempoResolucao.disabled= true;	
		document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionario.disabled= true;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.idTxpmCdTxpadraomanif.disabled= true;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.idClmaCdClassifmanif.disabled= true;	
		document.administracaoCsCdtbTpManifestacaoTpmaForm.idDocuCdDocumento.disabled= true;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaInConclui.disabled= true;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaInOperadorResp.disabled= true;	
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaDhInativo.disabled= true;	
		document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionarioCopCombo.disabled= true;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.idBotaCdBotao.disabled= true;
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxProcedimento.disabled= true;	
		document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxOrientacao.disabled= true;
		cmbGrupoManifestacao.document.administracaoCsCdtbTpManifestacaoTpmaForm.idGrmaCdGrupoManifestacao.disabled= true;		
	}
	catch(e){
		setTimeout("desabilitaCamposTpManifestacao();", 400);
	}
}

function carregaResponsavel(idArea){
	cmbFuncionarioResponsavel.location.href="AdministracaoCsCdtbTpManifestacaoTpma.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<%=MAConstantes.TELA_CMB_CS_CDTB_FUNCIONARIO_FUNC%>&idAreaCdArea=" + idArea + "&idFuncCdFuncionario=" + document.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionario.value;
	
}

function carregaCopiado(idArea){
	
	cmbFuncionarioCopiado.location.href="AdministracaoCsCdtbTpManifestacaoTpma.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<%=MAConstantes.TELA_CMB_CS_CDTB_FUNCIONARIO_FUNC%>&idAreaCdArea=" + idArea;
	
}

function inicio(){
	setaChavePrimaria(administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao.value);
}

var abaAtual = "abaTPMANIF";
var divAtual = "divTPMANIF";
function AtivarPasta(aba){
	document.getElementById(abaAtual).className = "principalPstQuadroLinkNormalGrande";
	document.getElementById(divAtual).style.display = "none";
	document.getElementById("aba"+ aba).className = "principalPstQuadroLinkSelecionadoGrande";
	document.getElementById("div"+ aba).style.display = "block";
	abaAtual = "aba"+ aba;
	divAtual = "div"+ aba;
}

// valdeci - 67296
function desenhoSelecionado(){
	if(administracaoCsCdtbTpManifestacaoTpmaForm.idDeprCdDesenhoProcesso.value != ""){
		administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.value="";
		administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.disabled=true;
		administracaoCsCdtbTpManifestacaoTpmaForm.tpmaInTempoResolucao.value="";
		administracaoCsCdtbTpManifestacaoTpmaForm.tpmaInTempoResolucao.disabled=true;
		administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrPrasoespecial.value="";
		administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrPrasoespecial.disabled=true;
		administracaoCsCdtbTpManifestacaoTpmaForm.idTpprCdTpprazoespecial.value="";
		administracaoCsCdtbTpManifestacaoTpmaForm.idTpprCdTpprazoespecial.disabled=true;
		administracaoCsCdtbTpManifestacaoTpmaForm.idAreaCdArea[0].value="";
		cmbFuncionarioResponsavel.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionario.value="";
		administracaoCsCdtbTpManifestacaoTpmaForm.idAreaCdArea[0].disabled=true;
		cmbFuncionarioResponsavel.administracaoCsCdtbTpManifestacaoTpmaForm.idFuncCdFuncionario.disabled=true;
	}else{
		administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrDiasResolucao.disabled=false;
		administracaoCsCdtbTpManifestacaoTpmaForm.tpmaInTempoResolucao.disabled=false;
		administracaoCsCdtbTpManifestacaoTpmaForm.tpmaNrPrasoespecial.disabled=false;
		administracaoCsCdtbTpManifestacaoTpmaForm.idTpprCdTpprazoespecial.disabled=false;
		administracaoCsCdtbTpManifestacaoTpmaForm.idAreaCdArea[0].disabled=false;
		cmbFuncionarioResponsavel.document.forms[0].idFuncCdFuncionario.disabled=false;
	}
}

</script>

<body class="principalBgrPageIFRM"
	onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos();carregaGrupo();carregaResponsavel(document.administracaoCsCdtbTpManifestacaoTpmaForm.idAreaCdArea[0].value);document.administracaoCsCdtbTpManifestacaoTpmaForm.idAreaCdArea[1].value='';inicio();desenhoSelecionado();">

	<html:form styleId="administracaoCsCdtbTpManifestacaoTpmaForm"
		action="/AdministracaoCsCdtbTpManifestacaoTpma.do">
		<input readonly type="hidden" name="remLen" size="3" maxlength="3"
			value="4000">
		<html:hidden property="modo" />
		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<html:hidden property="topicoId" />
		<html:hidden property="CDataInativo" />
		<html:hidden property="idGrmaCdGrupoManifestacao" />
		<html:hidden property="tpmaTxProcedimento" />
		<html:hidden property="tpmaTxOrientacao" />
		<html:hidden property="idFuncCdFuncionario" />
		<html:hidden property="idFuncCdFuncionarioCopCombo" />

		<br>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="principalPstQuadroLinkVazio">
			<tr>
				<td class="principalPstQuadroLinkSelecionadoGrande" id="abaTPMANIF"
					onClick="AtivarPasta('TPMANIF')"><bean:message
						key="prompt.tipoManifestacao" /></td>
				<td class="principalPstQuadroLinkNormalGrande" id="abaCOPIADOS"
					onClick="AtivarPasta('COPIADOS')"><bean:message
						key="prompt.funcionariosCopiados" /></td>
				<td class="principalPstQuadroLinkNormalGrande" id="abaBOTOES"
					onClick="AtivarPasta('BOTOES')"><bean:message
						key="prompt.funcoesextra" /></td>
				<td>&nbsp;</td>
			</tr>
		</table>

		<div id="divTPMANIF"
			style="display: block; border-width: 0px 1px 1px 1px;"
			class="principalBgrQuadro">






			<table width="99%" border="0" cellspacing="1" cellpadding="0"
				class="principalLabel">
				<tr>
					<td>
						<div align="right">
							<bean:message key="prompt.codigo" />
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td><html:text property="idTpmaCdTpManifestacao"
							styleClass="text" disabled="true" /></td>
				</tr>
				<tr>
					<td>
						<div align="right"><%=getMessage("prompt.manifestacao", request)%>
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td><html:select property="idMatpCdManifTipo"
							styleClass="principalObjForm" onchange="carregaGrupo()">
							<html:option value="">
								<bean:message key="prompt.selecione_uma_opcao" />
							</html:option>
							<html:options collection="csCdtbManifTipoMatpVector"
								property="idMatpCdManifTipo" labelProperty="matpDsManifTipo" />
						</html:select></td>
				</tr>
				<%
					if (Configuracoes.obterConfiguracao(
								ConfiguracaoConst.CONF_APL_SUPERGRUPO, request).equals(
								"S")) {
				%>
				<tr>
					<td>
						<div align="right">
							<bean:message key="prompt.Supergrupo" />
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td><iframe name="cmbSuperGrupoManif" src="" width="100%"
							height="20" scrolling="No" frameborder="0" marginwidth="0"
							marginheight="0"></iframe></td>
				</tr>
				<%
					}
				%>
				<tr>
					<td>
						<div align="right"><%=getMessage("prompt.grupomanif", request)%>
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td><iframe name="cmbGrupoManifestacao" src="" width="100%"
							height="20" scrolling="No" frameborder="0" marginwidth="0"
							marginheight="0"></iframe></td>
				</tr>

				<tr>
					<td align="right" class="principalLabel"><bean:message
							key="prompt.codigoCorporativo" /> <img
						src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td><html:text property="tpmaCdCorporativo" styleClass="text"
							maxlength="15" /></td>
				</tr>

				<tr>
					<td>
						<div align="right">
							<bean:message key="prompt.descricao" />
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td><html:text property="tpmaDsTpManifestacao"
							styleClass="text" maxlength="60" /></td>
				</tr>

				<tr>
					<td>
						<div align="right"><%=getMessage("prompt.DesenhoDoProcesso", request)%>
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td><html:select property="idDeprCdDesenhoProcesso"
							styleClass="principalObjForm" onchange="desenhoSelecionado()">
							<html:option value=''>
								<bean:message key="prompt.selecione_uma_opcao" />
							</html:option>
							<logic:present name="csCdtbDesenhoProcessoVector">
								<html:options collection="csCdtbDesenhoProcessoVector"
									property="field(id_depr_cd_desenhoprocesso)"
									labelProperty="field(depr_ds_desenhoprocesso)" />
							</logic:present>
						</html:select></td>
				</tr>

				<tr>
					<td>
						<div align="right">
							<bean:message key="prompt.diasParaResolucao" />
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
							<tbody>
								<tr>
									<td width="100"><html:text property="tpmaNrDiasResolucao"
											styleClass="text" maxlength="4"
											onkeypress="mascara(this,soNumeros)"
											onblur="mascara(this,soNumeros); return false;" /></td>
									<td width="80"><html:select property="tpmaInTempoResolucao"
											styleClass="principalObjForm"
											style="text-transform: uppercase">
											<html:option value=""></html:option>
											<html:option value="D">
												<bean:message key="prompt.dias" />
											</html:option>
											<html:option value="H">
												<bean:message key="prompt.horas" />
											</html:option>
											<html:option value="M">
												<bean:message key="prompt.minutos" />
											</html:option>
										</html:select></td>

									<td class="principalLabel">(<bean:message
											key="prompt.Normal" />)
									</td>
									<td width="100"><html:text property="tpmaNrPrasoespecial"
											styleClass="text" maxlength="4"
											onkeypress="mascara(this,soNumeros)"
											onblur="mascara(this,soNumeros); return false;" /></td>
									<td width="80"><html:select
											property="idTpprCdTpprazoespecial"
											styleClass="principalObjForm"
											style="text-transform: uppercase">
											<html:option value=''></html:option>
											<logic:present name="csDmtbTpprazoTpprVector">
												<html:options collection="csDmtbTpprazoTpprVector"
													property="field(id_tppr_cd_tpprazo)"
													labelProperty="field(tppr_ds_tpprazo)" />
											</logic:present>
										</html:select></td>
									<td width="60" class="principalLabel">(<bean:message
											key="prompt.Especial" />)
									</td>
								</tr>
							</tbody>
						</table>


					</td>
				</tr>

				<tr>
					<td>
						<div align="right">
							<bean:message key="prompt.responsavel" />
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td>
						<table width="100%">
							<tr>
								<td width="40%"><html:select property="idAreaCdArea"
										styleClass="principalObjForm"
										onchange="carregaResponsavel(this.value)">
										<html:option value="">
											<bean:message key="prompt.selecione_uma_opcao" />
										</html:option>
										<logic:present name="csCdtbAreaAreaVector">
											<html:options collection="csCdtbAreaAreaVector"
												property="idAreaCdArea" labelProperty="areaDsArea" />
										</logic:present>
									</html:select></td>
								<td width="60%"><iframe name="cmbFuncionarioResponsavel"
										src="AdministracaoCsCdtbTpManifestacaoTpma.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<%=MAConstantes.TELA_CMB_CS_CDTB_FUNCIONARIO_FUNC%>&idFuncCdFuncionario=<bean:write name='baseForm' property='idFuncCdFuncionario'/>"
										width="100%" height="20" scrolling="No" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<bean:message key="prompt.procedimento" />
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td height="50">
						<table>
							<tbody>
								<tr>
									<td><div id="Layer1"
											style="position: relative; width: 490; z-index: 1; overflow: auto; height: 50; background-color: #FFFFFF; border: 1px none #000000">
											<script>
				document.write(document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxProcedimento.value);
			   </script>
										</div></td>
									<td><img src="webFiles/images/botoes/bt_CriarCarta.gif"
										name="imgCriarCarta" width="25" height="22"
										class="geralCursoHand" border="0"
										title="<bean:message key="prompt.editor"/>"
										onClick="MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxProcedimento&carta=false&tamanho=3900','Documento','width=850,height=494,top=0,left=0')" /></td>
								</tr>
							</tbody>
						</table>

					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<bean:message key="prompt.orientacao" />
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td height="50">

						<table>
							<tbody>
								<tr>
									<td>
										<div id="Layer2"
											style="position: relative; width: 490; z-index: 1; overflow: auto; height: 50; background-color: #FFFFFF; border: 1px none #000000">
											<script language="JavaScript">
				document.write(document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxOrientacao.value);
			   </script>
										</div>
									</td>
									<td><img src="webFiles/images/botoes/bt_CriarCarta.gif"
										name="imgCriarCarta" width="25" height="22"
										class="geralCursoHand" border="0"
										title="<bean:message key="prompt.editor"/>"
										onClick="MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=document.administracaoCsCdtbTpManifestacaoTpmaForm.tpmaTxOrientacao&carta=false&tamanho=3900','Documento','width=850,height=494,top=0,left=0')" />
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							<bean:message key="prompt.textopadrao" />
							<img src="webFiles/images/icones/setaAzul.gif" name="imgSeta"
								width="7" height="7">
						</div>
					</td>
					<td><html:select property="idTxpmCdTxpadraomanif"
							styleClass="principalObjForm">
							<html:option value="">
								<bean:message key="prompt.selecione_uma_opcao" />
							</html:option>
							<html:options collection="csCdtbTxpadraomanifTxpmVector"
								property="idTxpmCdTxpadraomanif"
								labelProperty="txpmDsTxpadraomanif" />
						</html:select></td>
				</tr>

				<tr>
					<td>
						<div align="right">
							<bean:message key="prompt.classificacao" />
							<img src="webFiles/images/icones/setaAzul.gif" name="imgSeta"
								width="7" height="7">
						</div>
					</td>
					<td><html:select property="idClmaCdClassifmanif"
							styleClass="principalObjForm">
							<html:option value="">
								<bean:message key="prompt.selecione_uma_opcao" />
							</html:option> ***<html:options collection="csCdtbClassifmaniClmaVector"
								property="idClmaCdClassifmanif"
								labelProperty="clmaDsClassifmanif" />
						</html:select></td>
				</tr>

				<tr>
					<td>
						<div align="right">
							<bean:message key="prompt.respostaEMail" />
							<img src="webFiles/images/icones/setaAzul.gif" width="7"
								height="7">
						</div>
					</td>
					<td><html:select property="idDocuCdDocumento"
							styleClass="principalObjForm">
							<html:option value="">
								<bean:message key="prompt.selecione_uma_opcao" />
							</html:option>
							<html:options collection="csCdtbDocumentoDocuVector"
								property="idDocuCdDocumento" labelProperty="docuDsDocumento" />
						</html:select></td>
				</tr>

				<tr>
					<td width="23%">
						<div align="right"></div>
					</td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="principalLabel">

							<tr>
								<td width="50%"><html:checkbox value="true"
										property="tpmaInConclui" /> <bean:message
										key="prompt.concluirTempoAtendimento" /></td>
								<td width="50%"><html:checkbox value="true"
										property="tpmaInOperadorResp" /> <bean:message
										key="prompt.operadorResponsavel" /></td>
							</tr>

						</table>
					</td>
					
<tr>
	<td>
						<div align="right"></div>
					</td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="principalLabel">
							<tr>
								<td width="50%">
									<%
										// 91850 - 04/11/2013 - Jaider Alba
									%> <html:checkbox value="true" property="tpmaInWeb" /> <bean:message
						key="prompt.adm.web" />
								</td>
								<td width="50%"><html:checkbox value="true"
						property="tpmaDhInativo" /> <bean:message key="prompt.inativo" /></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<div id="divCOPIADOS"
			style="display: none; border-width: 0px 1px 1px 1px;"
			class="principalBgrQuadro">
			<br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="23%" class="principalLabel">
						<div align="right">
							<bean:message key="prompt.funcionariosCopiados" />
							<img src="webFiles/images/icones/setaAzul.gif" name="imgSeta"
								width="7" height="7">
						</div>
					</td>
					<td colspan="3">
						<table width="100%">
							<tr>
								<td width="40%"><html:select property="idAreaCdArea"
										styleClass="principalObjForm"
										onchange="carregaCopiado(this.value)">
										<html:option value="">
											<bean:message key="prompt.selecione_uma_opcao" />
										</html:option>
										<logic:present name="csCdtbAreaAreaVector">
											<html:options collection="csCdtbAreaAreaVector"
												property="idAreaCdArea" labelProperty="areaDsArea" />
										</logic:present>
									</html:select></td>
								<td width="60%"><iframe name="cmbFuncionarioCopiado"
										src="AdministracaoCsCdtbTpManifestacaoTpma.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<%=MAConstantes.TELA_CMB_CS_CDTB_FUNCIONARIO_FUNC%>"
										width="100%" height="20" scrolling="No" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
							</tr>
						</table>
					</td>
					<td width="12%"><img src="webFiles/images/botoes/setaDown.gif"
						title='<bean:message key="prompt.Adicionar"/>' name="imgSeta"
						width="21" height="18" class="geralCursoHand"
						onclick="adicionarItem()"></td>
				</tr>
				<tr>
					<td width="23%">&nbsp;</td>
					<td colspan="3">
						<div id="lstFunc"
							style="width: 497px; height: 340px; z-index: 1; overflow: auto;">
							<logic:present name="csCdtbFuncionarioFuncCopVector">
								<logic:iterate name="csCdtbFuncionarioFuncCopVector"
									id="ccffVector">
									<script>addItem('<bean:write name="ccffVector" property="idFuncCdFuncionario" />', '<bean:write name="ccffVector" property="funcNmFuncionario" />');</script>
								</logic:iterate>
							</logic:present>
						</div>
					</td>
					<td width="12%">&nbsp;</td>
				</tr>
			</table>
		</div>

		<div id="divBOTOES"
			style="display: none; border-width: 0px 1px 1px 1px;"
			class="principalBgrQuadro">
			<br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="23%" class="principalLabel">
						<div align="right">
							<bean:message key="prompt.botoes" />
							<img src="webFiles/images/icones/setaAzul.gif" name="imgSeta"
								width="7" height="7">
						</div>
					</td>
					<td colspan="3"><html:select property="idBotaCdBotao"
							styleClass="principalObjForm">
							<html:option value="">
								<bean:message key="prompt.selecione_uma_opcao" />
							</html:option>
							<logic:present name="csCdtbBotaoBotaVector">
								<html:options collection="csCdtbBotaoBotaVector"
									property="idBotaCdBotao" labelProperty="botaDsBotao" />
							</logic:present>
						</html:select></td>
					<td width="12%"><img src="webFiles/images/botoes/setaDown.gif"
						title='<bean:message key="prompt.Adicionar"/>' name="imgSeta"
						width="21" height="18" class="geralCursoHand"
						onclick="adicionarBotao()"></td>
				</tr>
				<tr>
					<td width="23%">&nbsp;</td>
					<td colspan="3">
						<div id="lstBotao"
							style="width: 497px; height: 350px; z-index: 0; overflow: auto">
							<logic:present name="csAstbTpManifBotaoTpmbVector">
								<logic:iterate name="csAstbTpManifBotaoTpmbVector"
									id="catmbtVector">
									<script>addBotao('<bean:write name="catmbtVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />', '<bean:write name="catmbtVector" property="csCdtbBotaoBotaVo.botaDsBotao" />');</script>
								</logic:iterate>
							</logic:present>
						</div>
					</td>
					<td width="12%">&nbsp;</td>
				</tr>
			</table>
		</div>
	</html:form>
</body>
<logic:equal name="baseForm" property="acao"
	value="<%=Constantes.ACAO_EXCLUIR%>">
	<script>
			desabilitaCamposTpManifestacao();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao"
	value="<%=Constantes.ACAO_INCLUIR%>">
	<script>
			setPermissaoImageEnable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbTpManifestacaoTpmaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposTpManifestacao();
			}else{
				document.administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao.disabled= false;
				document.administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao.value= '';
				document.administracaoCsCdtbTpManifestacaoTpmaForm.idTpmaCdTpManifestacao.disabled= true;
			}
		</script>
					</logic:equal>

					<logic:equal name="baseForm" property="acao"
						value="<%=Constantes.ACAO_EDITAR%>">
						<script>
			if (!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbTpManifestacaoTpmaForm.imgGravar);	
				desabilitaCamposTpManifestacao();
			}
			if (!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_EXCLUSAO_CHAVE%>')){
				setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_EXCLUSAO_CHAVE%>
		',
					document.administracaoCsCdtbTpManifestacaoTpmaForm.lixeira);
		}
	</script>
					</logic:equal>
</html>

<script>
	try {
		document.administracaoCsCdtbTpManifestacaoTpmaForm.idMatpCdManifTipo
				.focus();
	} catch (e) {
	}
</script>