<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
 	function desabilitaCamposArea(){
 		administracaoCsCdtbFornprodutoFoprForm.idFoprCdFornproduto.disabled= true;
 		administracaoCsCdtbFornprodutoFoprForm.foprDsCorporativo.disabled= true;
		administracaoCsCdtbFornprodutoFoprForm.foprDsFornproduto.disabled= true;	
		administracaoCsCdtbFornprodutoFoprForm.foprDhInativo.disabled= true;
	}
	
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbFornprodutoFoprForm.idFoprCdFornproduto.value);
	}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbFornprodutoFoprForm" action="/AdministracaoCsCdtbFornprodutoFopr.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>		

	<br><!--Jonathan | Adequa��o para o IE 10-->
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idFoprCdFornproduto" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.codigoCorporativo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="foprDsCorporativo" styleClass="text" maxlength="10"/>
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text style="height:20px;width:350px" property="foprDsFornproduto" styleClass="principalObjForm" maxlength="60"/> 
          </td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td class="principalLabel"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="foprDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td>&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			administracaoCsCdtbFornprodutoFoprForm.foprDsFornproduto.disabled= true;	
			administracaoCsCdtbFornprodutoFoprForm.foprDhInativo.disabled= true;
			administracaoCsCdtbFornprodutoFoprForm.foprDsCorporativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
		/*	setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_INCLUSAO_CHAVE%>', parent.administracaoCsCdtbFornprodutoFoprForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_INCLUSAO_CHAVE%>')){
				desabilitaCamposArea();
			}		
			administracaoCsCdtbFornprodutoFoprForm.idFoprCdFornproduto.disabled= false;
			administracaoCsCdtbFornprodutoFoprForm.idFoprCdFornproduto.value= '';
			administracaoCsCdtbFornprodutoFoprForm.idFoprCdFornproduto.disabled= true;
			*/
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
		 /*
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_ALTERACAO_CHAVE%>', parent.administracaoCsCdtbFornprodutoFoprForm.imgGravar);	
				desabilitaCamposArea();
			}
			*/
		</script>
</logic:equal>
</html>
<script>
	try{administracaoCsCdtbFornprodutoFoprForm.foprDsFornproduto.focus();}
	catch(e){}	
</script>