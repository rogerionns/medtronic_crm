<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>

<script language="JavaScript">

function iniciarTela(){
	//parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';	
	parent.ifrmArqPras.location.href = "AdministracaoCsCdtbProdutoImagemPrim.do?tela=<%=MAConstantes.TELA_IMG_PRAS%>";

}

function excluirArquivo(idAsn1CdAssuntonivel1,idAsn2CdAssuntonivel2,idPrimCdProdutoImagem){

	if(!confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>'))
		return false;

	//parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';	
	
	administracaoCsCdtbProdutoImagemPrimForm.tela.value = '<%=MAConstantes.TELA_LST_IMAGEM_PRAS%>';
	administracaoCsCdtbProdutoImagemPrimForm.acao.value = '<%=Constantes.ACAO_EXCLUIR%>';
	
	administracaoCsCdtbProdutoImagemPrimForm.idPrimCdProdutoImagem.value = idPrimCdProdutoImagem;
	administracaoCsCdtbProdutoImagemPrimForm.idAsn1CdAssuntonivel1.value = idAsn1CdAssuntonivel1;
	administracaoCsCdtbProdutoImagemPrimForm.idAsn2CdAssuntonivel2.value = idAsn2CdAssuntonivel2;
	
	
	administracaoCsCdtbProdutoImagemPrimForm.submit();		
}

var obj;
function downLoadArquivo(idAsn1CdAssuntonivel1,idAsn2CdAssuntonivel2,idPrimCdProdutoImagem){
	
	var url="";
	url = "AdministracaoCsCdtbProdutoImagemPrim.do?tela=<%=MAConstantes.TELA_IFRM_DOWNLOAD_PRASARQUIVO%>";
	url = url + "&idAsn1CdAssuntonivel1=" + idAsn1CdAssuntonivel1;
	url = url + "&idAsn2CdAssuntonivel2=" + idAsn2CdAssuntonivel2;
	url = url + "&idPrimCdProdutoImagem=" + idPrimCdProdutoImagem;
	
	ifrmDownloadManifArquivo.location = url;
	
	//obj = window.open(url,'Documento','width=5,height=3,top=2000,left=2000');	
	
	//setTimeout("fecharJanela();", 1000);
}

function fecharJanela(){
	if(!obj.closed){
		obj.close();
		setTimeout("fecharJanela();", 100);
	}
}

</script>


<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();">

<html:form styleId="administracaoCsCdtbProdutoImagemPrimForm" action="/AdministracaoCsCdtbProdutoImagemPrim.do">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="idPrimCdProdutoImagem" />
<html:hidden property="idAsn1CdAssuntonivel1" />
<html:hidden property="idAsn2CdAssuntonivel2" />


<div id="Layer1" style="width:100%; height:50px; z-index:1; overflow: auto">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<logic:present name="prasArqVector">
		  <logic:iterate id="prasArqVector" name="prasArqVector">
		  <tr> 
		    <td class="principalLstPar" width="5%">&nbsp;<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" title='<bean:message key="prompt.excluir"/>' class="geralCursoHand" onclick="excluirArquivo('<bean:write name="prasArqVector" property="idAsn1CdAssuntonivel1"/>','<bean:write name="prasArqVector" property="idAsn2CdAssuntonivel2"/>','<bean:write name="prasArqVector" property="idPrimCdProdutoImagem"/>');"></td>
		    <td class="principalLstPar" width="60%">&nbsp;<span class="geralCursoHand" onclick="downLoadArquivo('<bean:write name="prasArqVector" property="idAsn1CdAssuntonivel1"/>','<bean:write name="prasArqVector" property="idAsn2CdAssuntonivel2"/>','<bean:write name="prasArqVector" property="idPrimCdProdutoImagem"/>');"><bean:write name="prasArqVector" property="primNmFile"/></span>&nbsp;</td>
		    <td class="principalLstPar" width="30%">&nbsp;</td>
		    <td class="principalLstPar" width="5%">&nbsp;</td>
		  </tr>
		  </logic:iterate>
		</logic:present>  
		
	</table>
</div>

<iframe name="ifrmDownloadManifArquivo" src="" width="0" height="0" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>


</html:form>
</body>
</html>