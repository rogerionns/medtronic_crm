<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
	parent.parent.parent.parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
	
	function inicio(){
		try{
			parent.editIframe.filtrar();
		}
		catch(x){}
	}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form styleId="editCsAstbProdutoManifPrmaForm" action="/AdministracaoCsAstbProdutoManifPrma.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idAsn1CdAssuntoNivel1"/>
	<html:hidden property="idAsn2CdAssuntoNivel2"/>
	<html:hidden property="idTpmaCdTpManifestacao"/>

<script>var possuiRegistros=false;</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="cagaaVector" name="csAstbProdutoManifPrmaVector">
  <script>possuiRegistros=true;</script>
  <tr> 
       
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
	    <td width="11%" class="principalLstPar"> 
      		&nbsp;<bean:write name="cagaaVector" property="idAsn1CdAssuntoNivel1" /> 
    	</td> 
		<td class="principalLstParMao" align="left" width="40%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="idAsn1CdAssuntoNivel1" />','<bean:write name="cagaaVector" property="idAsn2CdAssuntoNivel2" />','<bean:write name="cagaaVector" property="idTpmaCdTpManifestacao" />')"> 
			<bean:write name="cagaaVector" property="linhDsLinha" />
		</td>
		<td class="principalLstParMao" width="49%" align="left" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="idAsn1CdAssuntoNivel1" />','<bean:write name="cagaaVector" property="idAsn2CdAssuntoNivel2" />','<bean:write name="cagaaVector" property="idTpmaCdTpManifestacao" />')"> 
			<bean:write name="cagaaVector" property="asn1DsAssuntoNivel1" /> 
		</td>
	<%} else {%>
		<td width="11%" class="principalLstPar"> 
      		&nbsp;<bean:write name="cagaaVector" property="idAsn1CdAssuntoNivel1" /> 
    	</td>
		<td class="principalLstParMao" width="89%" align="left" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="idAsn1CdAssuntoNivel1" />','<bean:write name="cagaaVector" property="idAsn2CdAssuntoNivel2" />','<bean:write name="cagaaVector" property="idTpmaCdTpManifestacao" />')"> 
			<bean:write name="cagaaVector" property="asn1DsAssuntoNivel1" /> 
		</td>
	<%}%>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>

<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>