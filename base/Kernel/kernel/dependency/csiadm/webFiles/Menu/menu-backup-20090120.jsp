
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<html>
<head>
<title>Menu Vertical</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="webFiles/funcoes/funcoes.js"></script>
<script src="webFiles/Menu/menu.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<link name="lnkCss" id="lnkCss" rel="stylesheet" href="webFiles/Menu/css/mvert_Azul.css" type="text/css">
<script language="JavaScript">
	var itensMenuArray = new Array();
	var seqItensMenuArray = 0;
	
	
    // Tratamento para Itens de Menu *********************************************
	function itemMenuTagChanged(strTipo, nItemId, nItemTag, strOldTag, strTag) {
		if (strTipo=='MENU') {				
			if (nItemTag==1)
				parent.ifrmConteudo.frmTeste.document.txtTag1Menu.value = strTag	
							
			if (nItemTag==2) {								
				parent.ifrmConteudo.frmTeste.document.txtTag2Menu.value = strTag
			}
		}
		else {				
			if (nItemTag==1)		
				parent.ifrmConteudo.frmTeste.document.txtTag1ItemMenu.value = strTag
				
			if (nItemTag==2)		
				parent.ifrmConteudo.frmTeste.document.txtTag2ItemMenu.value = strTag
		}
	
	}
	
	function itemMenuVisibleChanged(strTipo, nItemId, bolOldVisible, bolVisible) {
    	// Este evento ? disparado pelo M?todo setItemMenuVisible() do objeto Menu
		
	}
	
	function itemMenuEnabledChanged(strTipo, nItemId, bolOldEnabled, bolEnabled) {
		// Este evento ? disparado pelo M?todo setItemMenuEnabed() do objeto Menu
		
	}
	
	function itemMenuImagesPathChanged(nItemId, strOldPathEnabled, strOldPathDisabled, strPathEnabled, strPathDisabled) {
		// Este evento ? disparado pelo M?todo setItemMenuCaption() do objeto Menu
		parent.ifrmConteudo.frmTeste.document.txtImagemEnabled.value = strPathEnabled
		parent.ifrmConteudo.frmTeste.document.txtImagemDisabled.value = strPathDisabled
		
	}
	
	function itemMenuCaptionChanged(strTipo, nItemId, strOldCaption, strNewCaption) {
		// Este evento ? disparado pelo M?todo setItemMenuCaption() do objeto Menu
		if (strTipo=='MENU') {	
			parent.ifrmConteudo.frmTeste.document.txtExemplo.value = strNewCaption
		}
		else {
			parent.ifrmConteudo.frmTeste.document.txtItemMenuCaption.value = strNewCaption			
		}
		
	}
	
	function itemMenuToolTipChanged(strTipo, nItemId, strOldToolTip, strNewToolTip) {
		// Este evento ? disparado pelo M?todo setCaption() do objeto Menu
		if (strTipo == 'MENU') {
			parent.ifrmConteudo.frmTeste.document.txtToolTipMenu.value = strNewToolTip
		}
		else {
			parent.ifrmConteudo.frmTeste.document.txtToolTipItemMenu.value = strNewToolTip
		}
		
	}
	
	function cliqueMenuVertical(nId) {
	    
		// Construa aqui o c?digo para o tratamento do clique em um bot?o do menu vertical
		if (parent.ifrmConteudo.frmTeste) {
			parent.ifrmConteudo.frmTeste.document.txtExemplo.value = colMenu[nId].Caption
			parent.ifrmConteudo.frmTeste.document.txtMenuId.value = colMenu[nId].MenuIndex		
			parent.ifrmConteudo.frmTeste.document.txtToolTipMenu.value = colMenu[nId].ToolTip			
			parent.ifrmConteudo.frmTeste.document.txtTag1Menu.value = colMenu[nId].Tag1
			parent.ifrmConteudo.frmTeste.document.txtTag2Menu.value = colMenu[nId].Tag2
		}
		
		return false
	}
	
	function cliqueItemMenuVertical(nItemId) {
		for(var i = 0; i < itensMenuArray.length;i++){
			if (itensMenuArray[i][0] == nItemId){
				url = itensMenuArray[i][1];
				
				if(url.indexOf("?") > -1)
					url += "&idFuncCdFuncionario=<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>";
				else
					url += "?idFuncCdFuncionario=<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>";
				
				if(url.indexOf("http") > -1)
		  			parent.document.getElementById("ifrmConteudo").src = url;
		  		else
		  			parent.document.getElementById("ifrmConteudo").src = "../"+ url;
		  		
		  		return;
			}
		}
		return false;
	}
	
	var nLoad = 0;
	function onLoad() {
/*		try {
			criaMenu();
		}catch(x){
			nLoad++
			if(nLoad < 50){
				setTimeout("onLoad()", 100);
			}
			else{
				alert(x);
			}
		}*/
	}
	
	
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body name='bodyMenu' id='bodyMenu' bgcolor="#FFFFFF" text="#000000" onload="onLoad()">
<div class="menuvertBackColor" name="divMenuCont" id="divMenuCont" style="position:absolute; left:0; top:0; width:100%; height:650; z-index:2; overflow: hidden"> 
  <div style='left:0; top:0; width:99%; height:23; z-index:1; overflow: hidden; visibility: visible'>
		
    <!--table name='tabBotao@INDICE@' id='tabBotao@INDICE@' class='objetoOpcaoVisualizar' width='99%' border='0' cellspacing='0' cellpadding='0' >
      <tr>         
        <td align='center' width="20%" onclick="showMenuCss()">
          <div align="left" class="cursorHand">Visualizar:&nbsp;</div>
        </td>
        <td align='center' width="4%"> 
          <div align="left"><img src="images/mvert/btn_grande_baixo.gif" name="imgBigIcos" id="imgBigIcos" width="19" height="19" class="cursorHand" title="?cones Grandes" onclick="setIconesPequenos(false)"></div>
        </td>
        <td align='center' width="69%"> 
          <div align="left"><img src="images/mvert/btn_pequeno_alto.gif" name="imgSmallIcos" id="imgSmallIcos" width="19" height="19" class="cursorHand" title="?cones Pequenos" onclick="setIconesPequenos(true)"></div>
        </td>
      </tr>
    </table-->
    <table class='objetoOpcaoVisualizar' width='99%' border='0' cellspacing='0' cellpadding='0' >
      <tr>         
        <td align='center' >
          <div>PLUSOFT&nbsp;</div>
        </td>
      </tr>
    </table>    
</div>

</div>

</body>
</html>
<script language="JavaScript">	
    
   divMenuCont.style.height = 650;
	setIconesPequenos(true);   
	
	
	function criaMenu(){
		var primeiroItemMenu = true;
		var sequenciaGrupoMenu = -1;
		var sequenciaItemMenu = 0;
	  
		  <!-- MENUS -->    
		  <logic:iterate id="menusVector" name="menusVector" indexId="menu">
				  primeiroItemMenu = true;
								
				  <!-- ITENS DE MENUS -->    
				  <logic:iterate id="itensMenu" name="menusVector" property="itensMenu" indexId="itemmenu">
					  
					  //Verifica se o usuario tem permissao para acessar uma determinada tela ou se o item nao obriga permissao
					  if (getPermissao('<bean:write name="itensMenu" property="permissiondepends" />') || '<bean:write name="itensMenu" property="permissiondepends" />' == ''){
						if (primeiroItemMenu){
							sequenciaGrupoMenu = sequenciaGrupoMenu + 1;
							sequenciaItemMenu = 0;
	
							objMenu = addMenu(sequenciaGrupoMenu, '<bean:write name="menusVector" property="label" />', '<bean:write name="menusVector" property="label" />' );
							primeiroItemMenu = false;
							
						}
						
						sobjItMenu = addItemMenu(sequenciaGrupoMenu + "" + sequenciaItemMenu , sequenciaGrupoMenu, '<bean:write name="itensMenu" property="label" />', '<bean:write name="itensMenu" property="label" />', '<bean:write name="itensMenu" property="image" />', '<bean:write name="itensMenu" property="imageDisabled" />');
						
						//Aramazena o menu no array para o controle do click
						var item = new Array(2)
						item[0] = sequenciaGrupoMenu + "" + sequenciaItemMenu;
						item[1] = "<bean:write name="itensMenu" property="link" filter="false" />"					
						itensMenuArray[seqItensMenuArray] = item;
						seqItensMenuArray = seqItensMenuArray + 1;
						item = null;
						
						sequenciaItemMenu = sequenciaItemMenu + 1;
						
						
					}
				  </logic:iterate>
				  <!-- ITENS DE MENUS -->    
		  </logic:iterate>
		  <!-- MENUS -->
		  
		  setItemMenuAtivo('MENU', 0, 650);
	}    

	function inicio(){
		try {
			criaMenu();
		}catch(x){
			nLoad++
			if(nLoad < 30){
				setTimeout("onLoad()", 500);
			}
			else{
				alert(x);
			}
		}
	}
	
	inicio();
</script>	 