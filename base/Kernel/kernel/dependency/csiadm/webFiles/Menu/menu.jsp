<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" 
		import="java.util.*,br.com.plusoft.fw.util.*,br.com.plusoft.csi.adm.vo.MenuItensVo,org.apache.commons.lang.StringUtils,br.com.plusoft.fw.base64.mVertImages" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<script>
    //Chamado: 90471 - 16/09/2013 - Carlos Nunes
	var idFuncCdFuncionario='<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>';

	function getLink(url){
		if(url.indexOf("idFuncCdFuncionario") < 0)
		{
			if(url.indexOf("?") > -1)
			{
					url += "&idFuncCdFuncionario=" + idFuncCdFuncionario;
			}
			else
			{
				url += "?idFuncCdFuncionario=" + idFuncCdFuncionario;
			}
		}
		return url;
	}
</script>

<html>
	<head>
		<title>Menu Vertical</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="expires" content="0">

		<link rel="stylesheet" href="/plusoft-resources/css/menu-lite.css" type="text/css">
		<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	</head>
	<body>
		<logic:present name="menusVector">
		<ul>
			<li class="plusoft">
				PLUSOFT
			</li>
		<logic:iterate id="menu" name="menusVector" indexId="menuId">
			<li class="listmenu">
				<a href="#" class="menu" menuid="<bean:write name="menuId" />"><bean:write name="menu" property="label" />
				</a><ul class="submenu" menuid="<bean:write name="menuId" />">
			<logic:iterate id="item" name="menu" property="itensMenu" indexId="itemId">
				<li permission="<bean:write name="item" property="permissiondepends" />"><a href="<bean:write name="item" property="link" />" class="menuitem">
					&nbsp;
					<img src="<bean:write name="item" property="image" />" />&nbsp;
					<bean:write name="item" property="label" />
				</a></li> 
			</logic:iterate>
				</ul></li>
		</logic:iterate>
		</ul>
		</logic:present>
		<logic:notPresent name="menusVector">
			N�o foi poss�vel carregar o menu.
		</logic:notPresent>
		
		<script type="text/javascript">
			/**
			  * Novo mVert 
			  * Desenvolvido para acelerar o tempo de carregamento de menu atrav�s de HTML, e n�o via javascript
			  * 
			  * @author jvarandas
			  * @since 21/01/2011
			  */
			$(document).ready(function() {
				// Clique dos menus, para abrir os itens do menu selecionado
				$(".menu").click(function(event) {
					event.preventDefault();

					var s = $("ul[menuid="+this.getAttribute("menuId")+"]").css("display");

					if(s!="block") {
						$(".submenu").hide();

						$("ul[menuid="+this.getAttribute("menuId")+"]").show();
					}
				});

				// Clique dos itens de menu para carregar o item respectivo no ifrmConteudo e marca ele como selected
				$(".menuitem").click(function(event) {
					event.preventDefault();

					$(".menuitem").removeClass("selected");
					$(this).addClass("selected");
					//Chamado: 90471 - 16/09/2013 - Carlos Nunes
					window.top.ifrmConteudo.location.href = getLink(this.href);
				});


				// Varre os menus verificando a permiss�o e exibindo
				var visibleMenus = 0;
				$(".listmenu").each(function() {
					var hasPermission = false;
					var itemPermission = "";
	
					// Varre cada item do menu
					for(var i = 0; i<$(this).find("li").length; i++) {
						itemPermission = $(this).find("li").get(i).getAttribute("permission");
	
						// Se o item for permissionado, verificado permiss�o
						if(itemPermission!="") {
							if(window.top.getPermissao(itemPermission)) {
								hasPermission = true;
							} else {
							// Se n�o tiver permiss�o, esconde o item
								$(this).find("li").get(i).style.display="none";
							}
						} else {
							hasPermission = true;
						}
					}
	
					// Se algum item tiver permissao, mostra o menu
					if(hasPermission) { 
						$(this).show();
						visibleMenus++;
					}
				});

				// Chamado: 85553 - 31/12/2012 - Carlos Nunes
				$(".submenu").css("height", (600-(visibleMenus*24))+"px");
				$(".submenu").css("overflow", "auto");

				$(".menu[menuid=0]").click();
			});
		</script>
	</body>
</html>	 