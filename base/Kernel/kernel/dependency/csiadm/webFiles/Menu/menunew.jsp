<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head>
<title>Menu Vertical</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<link name="lnkCss" id="lnkCss" rel="stylesheet" href="webFiles/css/mvert_Azul.css" type="text/css">

	
	
<script language="JavaScript">
    // Tratamento para Itens de Menu *********************************************
	function itemMenuTagChanged(strTipo, nItemId, nItemTag, strOldTag, strTag) {
		if (strTipo=='MENU') {				
			if (nItemTag==1)
				window.parent.item('ifrmConteudo').frmTeste.txtTag1Menu.value = strTag	
							
			if (nItemTag==2) {								
				window.parent.item('ifrmConteudo').frmTeste.txtTag2Menu.value = strTag
			}
		}
		else {				
			if (nItemTag==1)		
				window.parent.item('ifrmConteudo').frmTeste.txtTag1ItemMenu.value = strTag
				
			if (nItemTag==2)		
				window.parent.item('ifrmConteudo').frmTeste.txtTag2ItemMenu.value = strTag
		}
	
	}
	
	function itemMenuVisibleChanged(strTipo, nItemId, bolOldVisible, bolVisible) {
    	// Este evento ? disparado pelo M?todo setItemMenuVisible() do objeto Menu
		
	}
	
	function itemMenuEnabledChanged(strTipo, nItemId, bolOldEnabled, bolEnabled) {
		// Este evento ? disparado pelo M?todo setItemMenuEnabed() do objeto Menu
		
	}
	
	function itemMenuImagesPathChanged(nItemId, strOldPathEnabled, strOldPathDisabled, strPathEnabled, strPathDisabled) {
		// Este evento ? disparado pelo M?todo setItemMenuCaption() do objeto Menu
		window.parent.item('ifrmConteudo').frmTeste.txtImagemEnabled.value = strPathEnabled
		window.parent.item('ifrmConteudo').frmTeste.txtImagemDisabled.value = strPathDisabled
		
	}
	
	function itemMenuCaptionChanged(strTipo, nItemId, strOldCaption, strNewCaption) {
		// Este evento ? disparado pelo M?todo setItemMenuCaption() do objeto Menu
		if (strTipo=='MENU') {	
			window.parent.item('ifrmConteudo').frmTeste.txtExemplo.value = strNewCaption
		}
		else {
			window.parent.item('ifrmConteudo').frmTeste.txtItemMenuCaption.value = strNewCaption			
		}
		
	}
	
	function itemMenuToolTipChanged(strTipo, nItemId, strOldToolTip, strNewToolTip) {
		// Este evento ? disparado pelo M?todo setCaption() do objeto Menu
		if (strTipo == 'MENU') {
			window.parent.item('ifrmConteudo').frmTeste.txtToolTipMenu.value = strNewToolTip
		}
		else {
			window.parent.item('ifrmConteudo').frmTeste.txtToolTipItemMenu.value = strNewToolTip
		}
		
	}
	
	function cliqueMenuVertical(nId) {
	    
		// Construa aqui o c?digo para o tratamento do clique em um bot?o do menu vertical
		if (window.parent.item('ifrmConteudo').frmTeste) {
			window.parent.item('ifrmConteudo').frmTeste.txtExemplo.value = colMenu[nId].Caption
			window.parent.item('ifrmConteudo').frmTeste.txtMenuId.value = colMenu[nId].MenuIndex		
			window.parent.item('ifrmConteudo').frmTeste.txtToolTipMenu.value = colMenu[nId].ToolTip			
			window.parent.item('ifrmConteudo').frmTeste.txtTag1Menu.value = colMenu[nId].Tag1
			window.parent.item('ifrmConteudo').frmTeste.txtTag2Menu.value = colMenu[nId].Tag2
		}
		
		return false
	}
	
	function cliqueItemMenuVertical(nItemId) {
		if(nItemId == 0)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbAreaArea.do";
		else if(nItemId == 1)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbFuncionarioFunc.do";
		else if(nItemId == 3)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTipoRelacaoTpre.do";
		else if(nItemId == 4)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTipoDiversoTpdi.do";
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {%>
			else if(nItemId == 8)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTpProgramaTppg.do";	
			else if(nItemId == 9)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbAcaoAcao.do";			
			else if(nItemId == 10)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsAstbAcaoProgramaAcpg.do";			
			else if(nItemId == 11)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsAstbProdutoProgramaPrpr.do";			
			else if(nItemId == 12)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsAstbProgramaPerfilPgpe.do";
			else if(nItemId == 13)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbMaterialPromoMapr.do";	
			else if(nItemId == 14)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbMrOrigemMror.do";	
		<%}%>
		else if(nItemId == 81)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbSupergrupoSugr.do";
		else if(nItemId == 15)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbGrupoManifestacaoGrma.do";
		else if(nItemId == 16)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTpManifestacaoTpma.do";
		else if(nItemId == 83)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsAstbProdutoManifPrma.do";
		else if(nItemId == 82)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsAstbSuperGrupoManifSugm.do";
		else if(nItemId == 17)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbGrauSatisfacaoGrsa.do";
		else if(nItemId == 19)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbDocumentoDocu.do";
		else if(nItemId == 20)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbAnexoMailAnma.do";		
		else if(nItemId == 21)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCatbDocumentoAnexoDoan.do";		
		else if(nItemId == 22)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbEventoFollowUpEvfu.do";
		else if(nItemId == 23)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbBotaoBota.do";

		else if(nItemId == 24)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbClassifmaniClma.do";
		else if(nItemId == 25)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbStatusManifStma.do";
		else if(nItemId == 26)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTxpadraomanifTxpm.do";

/*
		else if(nItemId == 27)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbAssuntoNivel1Asn1.do";
		else if(nItemId == 28)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbAssuntoNivel2Asn2.do";
*/
		else if(nItemId == 27)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbLinhaLinh.do";
		else if(nItemId == 28)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbProdutoAssuntoPras.do";
		else if(nItemId == 85)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbDestinoprodutoDepr.do";
		else if(nItemId == 29)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbPesquisaPesq.do";
		else if(nItemId == 30)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbQuestaoQues.do";
		else if(nItemId == 31)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbAlternativaAlte.do";
		else if(nItemId == 32)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCatbPesqquestaoPque.do";
		else if(nItemId == 33)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCatbQuestalternativaQual.do";
		else if(nItemId == 91)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbJustificativaJust.do";	
		else if(nItemId == 92)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbResultadoResu.do";	
		else if(nItemId == 34)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbPerfilPerf.do";		
		else if(nItemId == 35)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbDetperfilDtpe.do";		
		else if(nItemId == 36)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTabelaTabe.do";		
		else if(nItemId == 37)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbResptabuladaReta.do";		
		else if(nItemId == 38)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbComoLocalizouColo.do";
		else if(nItemId == 39)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbEstadoAnimoEsan.do";
		else if(nItemId == 40)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbFormaContatoFoco.do";
		else if(nItemId == 41)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbMidiaMidi.do";			
		else if(nItemId == 42)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTipoRetornoTpre.do";
		else if(nItemId == 43)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTipoPublicoTppu.do";
		else if(nItemId == 84)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbEstadoCivilEsci.do";
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
			else if(nItemId == 46)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbConstatacaoCpro.do";
			else if(nItemId == 47)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbJustiflaudoJula.do";
			else if(nItemId == 48)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbOrigemProblemaOrip.do";	
			else if(nItemId == 49)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbPrestadorServicoPrse.do";
			else if(nItemId == 50)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTpTercReclTptr.do";	
			else if(nItemId == 51)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbFabricaFabr.do";			
			else if(nItemId == 52)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbExposicaoExpo.do";
			else if(nItemId == 53)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTipoRessarciTpre.do";
			
			else if(nItemId == 86)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbMotivoloteMolo.do";
			else if(nItemId == 87)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbMotivotrocaMotr.do";
			else if(nItemId == 88)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbProcedenteProc.do";
			else if(nItemId == 89)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbResultadoanalRean.do";
			else if(nItemId == 90)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbLaudopadraoLapa.do";
			
		<%}%>
		else if(nItemId == 54)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsAstbComposicaoComp.do";	
		else if(nItemId == 55)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTpinformacaoTpin.do";	
		else if(nItemId == 56)
			window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbTopicoinformacToin.do";	
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_EMAIL,request).equals("S")) {%>		
			else if(nItemId == 57)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbGrupoFuncGrfu.do";	
			else if(nItemId == 58)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbServicoServ.do";	
			else if(nItemId == 59)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbCaixaPostalCapo.do";	
			else if(nItemId == 60)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbAcaoRegraAcre.do";	
			else if(nItemId == 61)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbAssuntoMailAsme.do";	
			else if(nItemId == 62)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbRegraRegr.do";	
			else if(nItemId == 63)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbItemRegraItre.do";	
			else if(nItemId == 64)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsAstbGrupoAssuntoAsgr.do";	
			else if(nItemId == 65)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCatbFuncGrfuFugr.do";	
		<%}%>
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CAMPANHA,request).equals("S")) {%>		
			//else if(nItemId == 66)
			//	window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbCamposLayoutCalo.do";				
			else if(nItemId == 69)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbLayoutLaou.do";				
			else if(nItemId == 70)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsAstbArqCargaArca.do";				
			else if(nItemId == 71)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbCampanhaCamp.do";				
			else if(nItemId == 72)
				window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCatbAnexoCampAnca.do";				
			else if(nItemId == 93)
					window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbMotivoencerraMoen.do";
			else if(nItemId == 94)
					window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbPublicoPubl.do";
			else if(nItemId == 95)
					window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbCategoriaprodCapr.do";
			else if(nItemId == 96)
					window.parent.item('ifrmConteudo').location.href="../../AdministracaoCsCdtbProdutovendaProd.do";
				
		<%}%>
		

		return false;
	}
	
</script>

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body name='bodyMenu' id='bodyMenu' bgcolor="#FFFFFF" text="#000000">
<div class="menuvertBackColor" name="divMenuCont" id="divMenuCont" style="position:absolute; left:0; top:0; width:100%; height:650; z-index:2; overflow: hidden"> 
  <div style='left:0; top:0; width:99%; height:23; z-index:1; overflow: hidden; visibility: visible'>
		<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>">
    <!--table name='tabBotao@INDICE@' id='tabBotao@INDICE@' class='objetoOpcaoVisualizar' width='99%' border='0' cellspacing='0' cellpadding='0' >
      <tr>         
        <td align='center' width="20%" onclick="showMenuCss()">
          <div align="left" class="cursorHand">Visualizar:&nbsp;</div>
        </td>
        <td align='center' width="4%"> 
          <div align="left"><img src="images/mvert/btn_grande_baixo.gif" name="imgBigIcos" id="imgBigIcos" width="19" height="19" class="cursorHand" title="?cones Grandes" onclick="setIconesPequenos(false)"></div>
        </td>
        <td align='center' width="69%"> 
          <div align="left"><img src="images/mvert/btn_pequeno_alto.gif" name="imgSmallIcos" id="imgSmallIcos" width="19" height="19" class="cursorHand" title="?cones Pequenos" onclick="setIconesPequenos(true)"></div>
        </td>
      </tr>
    </table-->
    <table class='objetoOpcaoVisualizar' width='99%' border='0' cellspacing='0' cellpadding='0' >
      <tr>         
        <td align='center' >
          <div>PLUSOFT&nbsp;</div>
        </td>
      </tr>
    </table>    
</div>

</div>

</body>
</html>
<script language="JavaScript">	
    
    divMenuCont.height = 650
	   
	  
	  		
	  <!-- MENUS -->    
	  <logic:iterate id="menusVector" name="menusVector" indexId="menu">
	  		objMenu = addMenu(<%= menu%>, '<bean:write name="menusVector" property="label" />', '<bean:write name="menusVector" property="label" />' );
	  		
	  		<!-- ITENS DE MENUS -->    
		  	<logic:iterate id="itensMenu" name="menusVector" property="itensMenu" indexId="itemmenu">
				  sobjItMenu = addItemMenu("<%= menu%><%= itemmenu%>", <%= menu%>, '<bean:write name="itensMenu" property="label" />', '<bean:write name="itensMenu" property="label" />', '<bean:write name="itensMenu" property="image" />', '<bean:write name="itensMenu" property="imageDisabled" />');
		  	</logic:iterate>
		  	<!-- ITENS DE MENUS -->    
	  </logic:iterate>
	  <!-- MENUS -->
    

	//objMenu = addMenu(ID_GRUPO_EMPRESA, '<bean:message key="prompt.empresa"/>', '<bean:message key="prompt.empresa"/>' );
	
	//sobjItMenu = addItemMenu(71, ID_GRUPO_CAMPANHA, '<bean:message key="prompt.campanha"/>', '<bean:message key="prompt.campanha"/>', 'images/mvert/Campanha.gif', 'images/mvert/Campanha.gif');
	    
	
	setItemMenuAtivo('MENU', 0, 650);
	
</script>