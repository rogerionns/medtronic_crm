<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	
function adicionarParametro(){
		
		var paboDsNomeinterno;
		var valorCheck;
		
		if (document.administracaoCsCdtbBotaoBotaForm['idPaboCdParametrobotao'].value == ""){
			alert("<bean:message key='prompt.porFavorEscolhaUmResultado' />.");
			document.administracaoCsCdtbBotaoBotaForm['idPaboCdParametrobotao'].focus();
			return false;
		}
		
		objEMail = document.administracaoCsCdtbBotaoBotaForm['idPaboDsNomeinterno'];
		for (nNode=0;nNode<objEMail.length;nNode++) {
	  		if (objEMail[nNode].value == document.administracaoCsCdtbBotaoBotaForm['idPaboCdParametrobotao'].value) {
		  		paboDsNomeinterno= document.administracaoCsCdtbBotaoBotaForm.paboDsNomeinterno[nNode].value;
		  	}
		}
		addParam(document.administracaoCsCdtbBotaoBotaForm.idPaboCdParametrobotao.value, document.administracaoCsCdtbBotaoBotaForm.idPaboCdParametrobotao[document.administracaoCsCdtbBotaoBotaForm.idPaboCdParametrobotao.selectedIndex].text,paboDsNomeinterno,document.administracaoCsCdtbBotaoBotaForm['paboInObrigatorio'].checked ); 
		document.administracaoCsCdtbBotaoBotaForm['paboInObrigatorio'].checked = false;
}

nLinha = new Number(0);
estilo = new Number(0);

function addParam(idPaboCdParametrobotao, paboDsParametrobotao, paboDsNomeinterno,valorCheck) {
	
	nLinha = nLinha + 1;
	estilo++;
	objEMail = document.administracaoCsCdtbBotaoBotaForm.idPaboCdParametrobotaoLista;
	for (nNode=0;nNode<objEMail.length;nNode++) {
	  if (objEMail[nNode].value == idPaboCdParametrobotao) {
		  idPaboCdParametrobotao=0;
		  }
	}
	
	if (valorCheck == true){
		statusCheck=1;
	}
	else{
		statusCheck=0;
	}

	if (idPaboCdParametrobotao > 0) { 
		strTxt = "";
		strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr class='intercalaLst" + (estilo-1)%2 + "'> ";
		strTxt += "     	<td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeParametro(\"" + nLinha + "\") title=\"<bean:message key='prompt.excluir'/>\"></td> ";
		strTxt += "       	<input type=\"hidden\" name=\"idPaboCdParametrobotaoLista\" value=\"" + idPaboCdParametrobotao + "\" > ";
		strTxt += "     	<td class=principalLstPar width=40%> " + paboDsParametrobotao ;
		strTxt += "     	<td class=principalLstPar width=43%> " + paboDsNomeinterno ;
		if (valorCheck == true){
			strTxt += "     	<td class=principalLstPar width=10% align=\"left\"><input type=\"checkbox\" name=\"paboInObrigatorioLista\" value=\"" + idPaboCdParametrobotao + "\" + checked=\"True\"></td> ";
		}
		else{
			strTxt += "     	<td class=principalLstPar width=10%  align=\"left\"><input type=\"checkbox\" name=\"paboInObrigatorioLista\" value=\"" + idPaboCdParametrobotao + "\"></td> ";
		}
		strTxt += "     	</td> ";
		strTxt += "		</tr> ";
		strTxt += " </table> ";

		lstParametros.innerHTML = lstParametros.innerHTML + strTxt;
	}else{
		alert('<bean:message key="prompt.selecionarParametroNaoRepetido" />');
	}
}


function removeParametro(nTblExcluir) {
	msg = '<bean:message key="prompt.desejaExcluirEsseParametro"/>';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstParametros.removeChild(objIdTbl);
		estilo--;
	}
}
	
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsCdtbBotaoBotaForm" action="/AdministracaoCsCdtbBotaoBota.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="idBotaCdBotao" />
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td height="110" align="left"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>	
						<td width="15%" align="right" class="principalLabel"><bean:message key="prompt.parametros"/><!-- ## --> 
							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
						</td>
						<td width="60%">
							  <html:select property="idPaboCdParametrobotao" styleClass="principalObjForm" > 
								<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
								<html:options collection="CsDmtbParametrobotaoPaboVector" property="idPaboCdParametrobotao" labelProperty="paboDsParametrobotao"/> 
							   </html:select> 
							   
							<logic:iterate id="ccttrtVector" name="CsDmtbParametrobotaoPaboVector" indexId="sequencia">
								 <input type="hidden" name="paboDsNomeinterno" value="<bean:write name="ccttrtVector" property="paboDsNomeinterno" />" />
								 <input type="hidden" name="idPaboDsNomeinterno" value="<bean:write name="ccttrtVector" property="idPaboCdParametrobotao" />" />
							</logic:iterate>
						</td>
						<td width="15%" align="left">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left"> 
								<tr>
									<td width="20%" align="right"><input type="checkbox" name="paboInObrigatorio" value="true"></td>
									<td class="principalLabel" width="80%">&nbsp;<bean:message key="prompt.obrigatorio"/>
								</tr>
							</table>
						</td>  
						<td width="10%" align="left"><img src="webFiles/images/botoes/setaDown.gif" title='<bean:message key="prompt.Adicionar"/>' width="21" height="18" class="geralCursoHand" onclick="adicionarParametro()"></td>                         
					</tr>
					<tr>
						<td colspan="4" height="8">&nbsp;</td> 
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td width="15%">&nbsp;</td> 
						<td width="60%"> 
							 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr> 
									  <td class="principalLstCab" height="1">&nbsp;</td>
									  <td class="principalLstCab" width="40%"> <bean:message key="prompt.parametro"/></td>
									  <td class="principalLstCab" width="40%"><bean:message key="prompt.nomeInterno"/></td>
									  <td class="principalLstCab" width="20%"><bean:message key="prompt.obrigatorio"/></td>
								</tr>
							  </table>
						</td>
						<td width="25%" colspan="2">&nbsp;</td> 
					</tr>
					<tr>
						<td width="15%">&nbsp;</td> 
						<td width="60%" height="50" valign="top"> 
							  <div id="lstParametros" style="position:absolute; width:360px; height:70px; z-index:4; overflow:auto"> 
								  <input type="hidden" name="idPaboCdParametrobotaoLista" value="0">
								<!--Inicio Lista Parametros -->
								 <logic:present name="listParametrosVector">
									  <logic:iterate id="cdppVector" name="listParametrosVector">
										<script language="JavaScript">
											var inObrigatorio
											
											inObrigatorio = '<bean:write name="cdppVector" property="paboInObrigatorio" />';
											
											if(inObrigatorio=="S"){
												inObrigatorio = true;
											}else{
												inObrigatorio = false;}
											
											  addParam('<bean:write name="cdppVector" property="idPaboCdParametrobotao" />',
													  '<bean:write name="cdppVector" property="paboDsParametrobotao" />',
													  '<bean:write name="cdppVector" property="paboDsNomeinterno" />',
													  inObrigatorio);
													  
										</script>
									 </logic:iterate>
								</logic:present>
							  </div>
						</td>
						<td width="25%" colspan="2">&nbsp;</td> 
					  </tr>
				</table>
			</td>
		</tr>
</table>
</html:form>
</body>
</html>