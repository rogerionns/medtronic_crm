<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<%@ page import="
		br.com.plusoft.csi.adm.helper.*,
		br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%>

<%
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

	String caminhoLogoTipo = "";
	try{
		long idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
		caminhoLogoTipo = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CAMINHO_LOGOTIPO_CLIENTE,idEmprCdEmpresa);
	}catch(Exception e){}
		
	if(caminhoLogoTipo == null || caminhoLogoTipo.equals("")){
		caminhoLogoTipo = "";
	}
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../webFiles/css/global.css" type="text/css">
<style type="text/css">
</style>

<script language="JavaScript">

function inicio(){
	parent.carregarLogo("<%=caminhoLogoTipo%>");
}

</script>
</head>

<body class="superiorBgrPage" text="#000000" onload="inicio();">

</body>

</html>
