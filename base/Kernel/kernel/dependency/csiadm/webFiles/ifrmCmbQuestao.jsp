<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
	function MontaCmb(){
		if(window.parent.document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.value > 0 || window.parent.document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.value != ""){
			window.parent.cmbQuestao.location.href= "AdministracaoCsCatbQuestalternativaQual.do?tela=cmbQuestao&acao=<%=Constantes.ACAO_CONSULTAR%>&idPesqCdPesquisa=" + window.parent.document.administracaoCsCatbQuestalternativaQualForm.idPesqCdPesquisa.value + "&idQuesCdQuestao=<bean:write name="administracaoCsCatbQuestalternativaQualForm" property="idQuesCdQuestao"/>";
		}
	}
	
	function PopulaHiddenIdQuest(){
		window.parent.document.administracaoCsCatbQuestalternativaQualForm.idQuesCdQuestao.value = document.administracaoCsCatbQuestalternativaQualForm.idQuesCdQuestao.value
	}
</script>
</head>
<body class="principalBgrPageIFRM" leftmargin="0" topmargin="0" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsCatbQuestalternativaQualForm" action="/AdministracaoCsCatbQuestalternativaQual.do">
	<html:select property="idQuesCdQuestao" styleId="idQuesCdQuestao" styleClass="principalObjForm" onchange="PopulaHiddenIdQuest()" > 
     <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
     <html:options collection="csCdtbQuestaoQuesVector" property="idCodigo" labelProperty="quesDsQuestao"/> 
    </html:select> 
</html:form>
</body>
</html>