<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsCdtbEventoFollowUpEvfuForm.target = admIframe.name;
	document.administracaoCsCdtbEventoFollowUpEvfuForm.acao.value ='filtrar';
	document.administracaoCsCdtbEventoFollowUpEvfuForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbEventoFollowUpEvfuForm.filtro.value = '';
}

function submeteFormIncluir() {
	editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.idFuncCdFuncionario.value = 0;
	editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_EVENTOFOLLOWUP_EVFU%>';
	editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo, idFuncionario){
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp.value = codigo;
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.idFuncCdFuncionario.value = idFuncionario;
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_EVENTOFOLLOWUP_EVFU%>';
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.target = editIframe.name;
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){

	if(tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_EVENTOFOLLOWUP_EVFU%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_EVENTOFOLLOWUP_EVFU%>'){
		
		tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuDsEventoFollowUp.value = trim(tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuDsEventoFollowUp.value);
		if (tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuDsEventoFollowUp.value != "") {

			if(tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuNrTemporesolucao.value != ""){
				if (tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuNrTemporesolucao.value.search(/[^\d]/) != -1) {
					alert ("<bean:message key="prompt.Por_favor_digite_apenas_numeros"/>.");
					tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuNrTemporesolucao.focus();
					return false;
				}
				
				if(tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuInTemporesolucao.value == ""){
					alert("<bean:message key="prompt.Por_favor_informe_se_o_tempo_sera_em_dias_ou_horas"/>.");
					tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuInTemporesolucao.focus();
					return false;
				}	
			}
			
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
			if((tab.document.getElementById("ckTodosSuperGrupos").checked || 
				tab.document.getElementById("ckTodosGrupos").checked || 
				tab.document.getElementById("ckTodosTipos").checked) && 
				tab.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].selectedIndex == 0){
				alert("<bean:message key="prompt.Por_favor_selecione_uma_manifestacao" />");
				tab.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].focus();
				return false;
			}
			else 
			if((tab.document.getElementById("ckTodosGrupos").checked || tab.document.getElementById("ckTodosTipos").checked) && 
				tab.ifrmCmbSuperGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["idSugrCdSupergrupo"].selectedIndex == 0){
				alert("<bean:message key="prompt.Por_favor_selecione_um_segmento" />");
				tab.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].focus();
				return false;
			}
<%} else {%>
			if((tab.document.getElementById("ckTodosGrupos").checked || tab.document.getElementById("ckTodosTipos").checked) && tab.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].selectedIndex == 0){
				alert("<bean:message key="prompt.Por_favor_selecione_uma_manifestacao" />");
				tab.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].focus();
				return false;
			}
<%}%>
			else if(tab.document.getElementById("ckTodosTipos").checked && tab.ifrmCmbGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].selectedIndex == 0){
				alert("<bean:message key="prompt.Por_favor_selecione_um_grupo_de_manifestacao" />");
				tab.ifrmCmbGrupomanifestacao.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].focus();
				return false;
			}

			tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_EVENTOFOLLOWUP_EVFU%>';
			tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.target = admIframe.name;
			disableEnable(tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp, false);
			tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.submit();
			disableEnable(tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp, true);
			cancel();
		}
		else {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.evfuDsEventoFollowUp.focus();
		}
	}
}

function submeteExcluir(codigo) {
	
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp.value = codigo;
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_EVENTOFOLLOWUP_EVFU%>';
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.target = editIframe.name;
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_EVENTOFOLLOWUP_EVFU%>';
		editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp, false);
		editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbEventoFollowUpEvfuForm.idEvfuCdEventoFollowUp, true);
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbEventoFollowUpEvfu.do?tela=editCsCdtbEventoFollowUpEvfu&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbEventoFollowUpEvfu.do?tela=editCsCdtbEventoFollowUpEvfu&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsCdtbEventoFollowUpEvfuForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_EVENTOFOLLOWUP_EVFU%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			setaListaBloqueia();
			setaIdiomaBloqueia();
			document.administracaoCsCdtbEventoFollowUpEvfuForm.imgLixeira.style.display = 'none';
			break;
		case editIframe:
			tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_EVENTOFOLLOWUP_EVFU%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');
			setaListaHabilita();
			setaIdiomaHabilita();
			document.administracaoCsCdtbEventoFollowUpEvfuForm.imgLixeira.style.display = 'block';
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function submeteFormExcluir(){
	if(tab.document.administracaoCsCdtbEventoFollowUpEvfuForm.tela.value != '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_EVENTOFOLLOWUP_EVFU%>'){
		editIframe.excluirTpma();
	}
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbEventoFollowUpEvfuForm"	action="/AdministracaoCsCdtbEventoFollowUpEvfu.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/><!-- ## --></td>
									<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.eventoFollowUp"/><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="position: absolute; width: 97%; height: 225px; z-index: 6; visibility: hidden"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsCdtbEventoFollowUpEvfu.do?tela=editCsCdtbEventoFollowUpEvfu&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="position: absolute; width: 97%; height: 199px; z-index: 2; visibility: visible"> 
                    		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalLabel" width="15%"><bean:message key="prompt.descricao"/></td>
									<td class="principalLabel" width="65%">&nbsp;</td>
								</tr>

								<tr>
									<td width="65%">
									<table border="0" cellspacing="0" cellpadding="0">
									<tr>
									<td width="25%">
									<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
									</td>
									<td width="05%">
									&nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
									</td>
									</tr>	
									</table>
									</td>
								</tr>
								<tr>
									<td class="principalLabel" width="15%">&nbsp;</td>
									<td class="principalLabel" width="64%">&nbsp;</td>
								</tr>
							</table>
							<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalLstCab" width="18%">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td class="principalLstCab" width="25%">
														&nbsp;
													</td>
													<td class="principalLstCab" width="75%">
														&nbsp;&nbsp;<bean:message key="prompt.codigo"/>
													</td>
												</tr>
											</table>
									</td>
									<td class="principalLstCab" width="61%"><bean:message key="prompt.descricao"/></td>
									<td class="principalLstCab" align="left" width="20%">&nbsp;&nbsp;<bean:message key="prompt.inativo"/> </td>
								</tr>
								
								<tr valign="top">
									
                        <td colspan="3" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsCdtbEventoFollowUpEvfu.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                </td>
							<td width="4" height="400"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="40" align="right">
							<img src="webFiles/images/botoes/lixeira18x18.gif" name="imgLixeira" width="18" height="18" class="geralCursoHand" title="<bean:message key='prompt.excluir'/>" onclick="submeteFormExcluir()" style="display:none" >
					</td>
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="540px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="540px"></td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_EVENTOFOLLOWUP_INCLUSAO_CHAVE%>', document.administracaoCsCdtbEventoFollowUpEvfuForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_EVENTOFOLLOWUP_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_EVENTOFOLLOWUP_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbEventoFollowUpEvfuForm.imgGravar.disabled=true;
			document.administracaoCsCdtbEventoFollowUpEvfuForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbEventoFollowUpEvfuForm.imgGravar.title='';
	}
	
	desabilitaComboListaEmpresa();
	
	setaArquivoXml("CS_ASTB_IDIOMAEVFU_IDEF.xml");
	habilitaTelaIdioma();
	desabilitaListaEmpresas();
	
	
</script>



</body>
</html>
