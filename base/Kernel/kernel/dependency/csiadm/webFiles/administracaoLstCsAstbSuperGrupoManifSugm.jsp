<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>				

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>
<body class="principalBgrPageIFRM" style="margin: 0px;" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsAstbSuperGrupoManifSugmForm" action="/AdministracaoCsAstbSuperGrupoManifSugm.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csAstbSuperGrupoManifSugmVector" indexId="sequencia"> 
  <tr> 
    <td width="5%" class="principalLstPar"> 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" />','<bean:write name="ccttrtVector" property="csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />')" > 
    </td>
    <td  class="principalLstPar" width="40%"  >
     <plusoft:acronym name="ccttrtVector" property="csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.matpDsManifTipo" length="20" /> 
    </td>
    <td width="45%" class="principalLstPar"  >
     <plusoft:acronym name="ccttrtVector" property="csCdtbGrupoManifestacaoGrmaVo.grmaDsGrupoManifestacao" length="30" /> 
    </td>
  </tr>
  </logic:iterate>
</table>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_SEGMENTOGRUPO_EXCLUSAO_CHAVE%>', editCsAstbSuperGrupoManifSugmForm.lixeira);
</script>	

</html:form>
</body>
</html>