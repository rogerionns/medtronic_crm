<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page
	import="br.com.plusoft.csi.adm.form.*, br.com.plusoft.fw.app.Application"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
%>

<html>
<head>
<title>Parâmetro Manifestação</title>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<SCRIPT LANGUAGE="JavaScript">
function VerificaCampos(){
	if (administracaoCsAstbProdutoManifPrmaForm.tpmaNrDiasResolucao.value == "0" ){
		administracaoCsAstbProdutoManifPrmaForm.tpmaNrDiasResolucao.value = "" ;
	}
	//administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop.multiple = true;
	
	try{
	
		administracaoCsAstbProdutoManifPrmaForm.tpmaTxProcedimento.value = getDialogArguments().document.getElementById("tpmaTxProcedimento").value;
		administracaoCsAstbProdutoManifPrmaForm.tpmaTxOrientacao.value = getDialogArguments().document.getElementById("tpmaTxOrientacao").value;
		document.getElementById("Layer1").innerHTML = administracaoCsAstbProdutoManifPrmaForm.tpmaTxProcedimento.value;
		document.getElementById("Layer2").innerHTML = administracaoCsAstbProdutoManifPrmaForm.tpmaTxOrientacao.value;
	
		administracaoCsAstbProdutoManifPrmaForm.tpmaNrDiasResolucao.value = getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["tpmaNrDiasResolucao"].value;
		administracaoCsAstbProdutoManifPrmaForm.tpmaInTempoResolucao.value = getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["tpmaInTempoResolucao"].value;
		
		if(getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["idFuncCdFuncionario"].value > 0){
			administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionario.value = getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["idFuncCdFuncionario"].value;
		}
		
		if(getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["idTxpmCdTxpadraomanif"].value > 0){
			administracaoCsAstbProdutoManifPrmaForm.idTxpmCdTxpadraomanif.value = getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["idTxpmCdTxpadraomanif"].value;
		}
		
		if(getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["idClmaCdClassifmanif"].value > 0){
			administracaoCsAstbProdutoManifPrmaForm.idClmaCdClassifmanif.value = getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["idClmaCdClassifmanif"].value;
		}
		
		if(getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["idDocuCdDocumento"].value > 0){
			administracaoCsAstbProdutoManifPrmaForm.idDocuCdDocumento.value = getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["idDocuCdDocumento"].value;	
		}
		
		if(getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["idDeprCdDesenhoProcesso"].value > 0){
			administracaoCsAstbProdutoManifPrmaForm.idDeprCdDesenhoProcesso.value = getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["idDeprCdDesenhoProcesso"].value;
		}
		
		administracaoCsAstbProdutoManifPrmaForm.tpmaInConclui.checked = getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["tpmaInConclui"].value == "true";
		administracaoCsAstbProdutoManifPrmaForm.tpmaInOperadorResp.checked = getDialogArguments().document.administracaoCsAstbProdutoManifPrmaForm["tpmaInOperadorResp"].value == "true";
	}
	catch(x){}
}	

var modalWin;

function MM_openBrWindow(theURL,winName,features) { //v2.0
	modalWin = window.open(theURL,winName,features);
	if (modalWin!=null && !modalWin.closed){
		//self.blur();
		modalWin.focus();
	}
}

function adicionarItem(){
	if (administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.value == "") {
		alert("<bean:message key="prompt.Por_favor_escolha_um_funcionario"/>");
		administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.focus();
		return false;
	}
	
	if(administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.value == administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionario.value){
		alert("<bean:message key='prompt.esteFuncionárioFoiSelecionadoComoResponsavel'/>");
		return false;
	}
	
	addItem(administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.value, administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo[administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.selectedIndex].text);
}

function comparaChave(cFunc) {
	try {
		if (administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop.length != undefined) {
			for (var i = 0; i < administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop.length; i++) {
				if (administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop[i].value == cFunc) {
					alert('<bean:message key="prompt.Este_funcionario_ja_existe"/>');
					return true;
				}
			}
		} else {
			if (administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop.value == cFunc) {
				alert('<bean:message key="prompt.Este_funcionario_ja_existe"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}

nLinha = new Number(0);

function addItem(cFunc, nFunc) {
	if (comparaChave(cFunc)) {
		return false;
	}
	nLinha = nLinha + 1;
	
	strTxt = "";
	strTxt += "<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
    strTxt += "  <tr>";
	strTxt += "    <input type=\"hidden\" id=\"idFuncCdFuncionarioCop\" name=\"idFuncCdFuncionarioCop\" value=\"" + cFunc + "\" > ";
	strTxt += "    <td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif name=lixeira width=14 height=14 class=geralCursoHand onclick=removeItem(\"" + nLinha + "\")></td> ";
	strTxt += "    <td class=principalLstPar width=95%> ";
	strTxt +=        nFunc;
	strTxt += "    </td> ";
	strTxt += "	 </tr> ";
	strTxt += " </table> ";
	
	document.getElementsByName("lstFunc").innerHTML += strTxt;
}

function removeItem(nTblExcluir) {
	msg = '<bean:message key="prompt.Deseja_excluir_esse_funcionario"/>';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstFunc.removeChild(objIdTbl);
		nLinha = nLinha - 1;
	}
}


nLinha2 = new Number(0);

function setFunction(cRetorno, campo){
	if(campo=="administracaoCsAstbProdutoManifPrmaForm.tpmaTxProcedimento"){
		document.getElementById("Layer1").innerHTML = cRetorno;
		document.administracaoCsAstbProdutoManifPrmaForm.tpmaTxProcedimento.value=cRetorno;
	}else{
		document.getElementById("Layer2").innerHTML = cRetorno;
		document.administracaoCsAstbProdutoManifPrmaForm.tpmaTxOrientacao.value=cRetorno;
	}
}

function desabilitaCamposTpManifestacao(){
	try{
		setPermissaoImageDisable('', administracaoCsAstbProdutoManifPrmaForm.imgCriarCarta);
		setPermissaoImageDisable('', administracaoCsAstbProdutoManifPrmaForm.imgSeta);
		administracaoCsAstbProdutoManifPrmaForm.tpmaNrDiasResolucao.disabled= true;
		administracaoCsAstbProdutoManifPrmaForm.tpmaInTempoResolucao.disabled= true;	
		administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionario.disabled= true;
		administracaoCsAstbProdutoManifPrmaForm.idTxpmCdTxpadraomanif.disabled= true;
		administracaoCsAstbProdutoManifPrmaForm.idClmaCdClassifmanif.disabled= true;	
		administracaoCsAstbProdutoManifPrmaForm.idDocuCdDocumento.disabled= true;
		administracaoCsAstbProdutoManifPrmaForm.tpmaInConclui.disabled= true;
		administracaoCsAstbProdutoManifPrmaForm.tpmaInOperadorResp.disabled= true;	
		//administracaoCsAstbProdutoManifPrmaForm.tpmaDhInativo.disabled= true;	
		//administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.disabled= true;
		administracaoCsAstbProdutoManifPrmaForm.tpmaTxProcedimento.disabled= true;	
		administracaoCsAstbProdutoManifPrmaForm.tpmaTxOrientacao.disabled= true;
		administracaoCsAstbProdutoManifPrmaForm.idDeprCdDesenhoProcesso.disabled= true;
	}
	catch(e){
		setTimeout("desabilitaCamposTpManifestacao();", 400);
	}
}

function submeteSalvar(){

	getDialogArguments().document.getElementById("parametrosManifestacao").innerHTML = "";

	strTxt = "";
	strTxt += "    <input type=\"hidden\" name=\"tpmaNrDiasResolucao\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.tpmaNrDiasResolucao.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"tpmaInTempoResolucao\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.tpmaInTempoResolucao.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"idFuncCdFuncionario\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionario.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"idTxpmCdTxpadraomanif\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idTxpmCdTxpadraomanif.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"idClmaCdClassifmanif\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idClmaCdClassifmanif.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"idDocuCdDocumento\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idDocuCdDocumento.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"idDeprCdDesenhoProcesso\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idDeprCdDesenhoProcesso.value + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"tpmaInConclui\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.tpmaInConclui.checked + "\" > ";
	strTxt += "    <input type=\"hidden\" name=\"tpmaInOperadorResp\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.tpmaInOperadorResp.checked + "\" > ";
	//strTxt += "    <input type=\"hidden\" name=\"tpmaDhInativo\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.tpmaDhInativo.value + "\" > ";
	//strTxt += "    <input type=\"hidden\" name=\"idFuncCdFuncionarioCopCombo\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.value + "\" > ";
	strTxt += "    <input type=\"hidden\" id=\"tpmaTxProcedimento\" name=\"tpmaTxProcedimento\" value=\"" + "\" > ";
	strTxt += "    <input type=\"hidden\" id=\"tpmaTxOrientacao\" name=\"tpmaTxOrientacao\" value=\"" + "\" > ";
	
	if (nLinha > 0){
		if (nLinha == 1){
			strTxt += "    <input type=\"hidden\" id=\"idFuncCdFuncionarioCop\" name=\"idFuncCdFuncionarioCop\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop.value + "\" > ";
		}else{
			for(i=0;i<administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop.length;i++){
				strTxt += "    <input type=\"hidden\" id=\"idFuncCdFuncionarioCop\" name=\"idFuncCdFuncionarioCop\" value=\"" + administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop[i].value + "\" > ";
			}	
		}
	}
	else{
		strTxt += "    <input type=\"hidden\" id=\"idFuncCdFuncionarioCop\" name=\"idFuncCdFuncionarioCop\" value=\"\" > ";
	}
	
	getDialogArguments().document.getElementById("parametrosManifestacao").innerHTML += strTxt;
	
	getDialogArguments().document.getElementById("tpmaTxProcedimento").value = administracaoCsAstbProdutoManifPrmaForm.tpmaTxProcedimento.value;
	getDialogArguments().document.getElementById("tpmaTxOrientacao").value = administracaoCsAstbProdutoManifPrmaForm.tpmaTxOrientacao.value;
	
	window.close();

}


function cancel(){

		administracaoCsAstbProdutoManifPrmaForm.tpmaNrDiasResolucao.value = "";
		administracaoCsAstbProdutoManifPrmaForm.tpmaInTempoResolucao.value = "";
		administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionario.value = "";
		administracaoCsAstbProdutoManifPrmaForm.idTxpmCdTxpadraomanif.value = "";
		administracaoCsAstbProdutoManifPrmaForm.idClmaCdClassifmanif.value = "";
		administracaoCsAstbProdutoManifPrmaForm.idDocuCdDocumento.value = "";
		administracaoCsAstbProdutoManifPrmaForm.idDeprCdDesenhoProcesso.value = "";
		administracaoCsAstbProdutoManifPrmaForm.tpmaInConclui.checked = false;
		administracaoCsAstbProdutoManifPrmaForm.tpmaInOperadorResp.checked = false;
		//administracaoCsAstbProdutoManifPrmaForm.tpmaDhInativo.checked = false;
		//administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.value = "";
		administracaoCsAstbProdutoManifPrmaForm.tpmaTxProcedimento.value = "";
		administracaoCsAstbProdutoManifPrmaForm.tpmaTxOrientacao.value = "";
		document.getElementById("Layer1").innerHTML = "";
		document.getElementById("Layer2").innerHTML = "";
}

function sair(){
	if(confirm("<bean:message key='prompt.osDadosNaoForamSalvosDesejaConfirmarSaidaTela' />")){
		window.close();
	}
	
}

function adicionarItem(){
	if (document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.value == "") {
		alert("<bean:message key="prompt.Por_favor_escolha_um_funcionario"/>");
		document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.focus();
		return false;
	}
	addItem(document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.value, document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo[document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.selectedIndex].text);
}

function addItem(cFunc, nFunc) {
	if (comparaChave(cFunc)) {
		return false;
	}
	nLinha = nLinha + 1;

	strTxt = "";
	strTxt += "<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
    strTxt += "  <tr>";
	strTxt += "    <input type=\"hidden\" name=\"idFuncCdFuncionarioCop\" value=\"" + cFunc + "\" > ";
	strTxt += "    <td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif name=lixeira width=14 height=14 class=geralCursoHand onclick=removeItem(\"" + nLinha + "\")></td> ";
	strTxt += "    <td class=principalLstPar width=95%> ";
	if(nFunc == undefined){
		strTxt +=        document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo[document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCopCombo.selectedIndex].text;
	}else{
		strTxt +=        nFunc;
	}
	strTxt += "    </td> ";
	strTxt += "	 </tr> ";
	strTxt += " </table> ";
	
	document.getElementById('lstFunc').innerHTML = document.getElementById('lstFunc').innerHTML + strTxt;
}

function removeItem(nTblExcluir) {
	msg = '<bean:message key="prompt.Deseja_excluir_esse_funcionario"/>';
	if (confirm(msg)) {
		objIdTbl = document.getElementById(nTblExcluir);
		document.getElementById('lstFunc').removeChild(objIdTbl);
		nLinha = nLinha - 1;
	}
}

function comparaChave(cFunc) {
	try {
		if (document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop.length != undefined) {
			for (var i = 0; i < document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop.length; i++) {
				if (document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop[i].value == cFunc) {
					alert('<bean:message key="prompt.Este_funcionario_ja_existe"/>');
					return true;
				}
			}
		} else {
			if (document.administracaoCsAstbProdutoManifPrmaForm.idFuncCdFuncionarioCop.value == cFunc) {
				alert('<bean:message key="prompt.Este_funcionario_ja_existe"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}



</script>
<script language="JavaScript"
	src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>

<body class="principalBgrPageIFRM"
	onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos();">

	<html:form styleId="administracaoCsAstbProdutoManifPrmaForm"
		action="/AdministracaoCsAstbProdutoManifPrma.do">
		<input readonly type="hidden" name="remLen" size="3" maxlength="3"
			value="4000">
		<html:hidden property="modo" />
		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<html:hidden property="topicoId" />
		<html:hidden property="CDataInativo" />
		<html:hidden property="idGrmaCdGrupoManifestacao" />
		<html:hidden property="tpmaTxProcedimento" />
		<html:hidden property="tpmaTxOrientacao" />

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="1" cellpadding="0" class="principalLabel">
						<tr>
							<td style="width: 150px;">
								<div align="right">
									<bean:message key="prompt.diasParaResolucao" />
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
								</div>
							</td>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td style="width: 20%"><html:text property="tpmaNrDiasResolucao" styleClass="text" maxlength="4" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" /></td>
											<td style="width: 20%"><html:select property="tpmaInTempoResolucao" styleClass="principalObjForm">
													<html:option value=""></html:option>
													<html:option value="D">
														<bean:message key="prompt.dias" />
													</html:option>
													<html:option value="H">
														<bean:message key="prompt.horas" />
													</html:option>
												</html:select></td>
											<td>&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td style="width: 30px;">&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<bean:message key="prompt.responsavel" />
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
								</div>
							</td>
							<td><html:select property="idFuncCdFuncionario"
									styleClass="principalObjForm">
									<html:option value="">
										<bean:message key="prompt.selecione_uma_opcao" />
									</html:option>
									<html:options collection="csCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario" />
								</html:select></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<bean:message key="prompt.funcionariosCopiados" />
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
								</div>
							</td>
							<td>
								<html:select property="idFuncCdFuncionarioCopCombo" styleClass="principalObjForm">
									<html:option value="">
										<bean:message key="prompt.selecione_uma_opcao" />
									</html:option>
									<html:options collection="csCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario" />
								</html:select>
							</td>
							<td><img src="webFiles/images/botoes/setaDown.gif" name="imgSeta" width="21" height="18" class="geralCursoHand" onclick="adicionarItem()" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>
								<div id="lstFunc" style="width: 100%; height: 45px; z-index: 1; overflow: auto">
									<logic:present name="csCdtbFuncionarioFuncCopVector">
										<logic:iterate name="csCdtbFuncionarioFuncCopVector" id="ccffVector">
											<script>addItem('<bean:write name="ccffVector" property="idFuncCdFuncionario" />', '<bean:write name="ccffVector" property="funcNmFuncionario" />');</script>
										</logic:iterate>
									</logic:present>
								</div>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<bean:message key="prompt.procedimento" />
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
								</div>
							</td>
							<td>
								<div id="Layer1" name="Layer1" style="position: relative; width: 100%; z-index: 1; overflow: auto; height: 60; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000">
									<script>
										document.write(document.administracaoCsAstbProdutoManifPrmaForm.tpmaTxProcedimento.value);
									</script>
								</div>
							</td>
							<td><img src="webFiles/images/botoes/bt_CriarCarta.gif" name="imgCriarCarta" width="25" height="22" class="geralCursoHand" border="0" title="<bean:message key="prompt.editor"/>" onClick="MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=administracaoCsAstbProdutoManifPrmaForm.tpmaTxProcedimento&carta=false&tamanho=3900','Documento','width=850,height=494,top=0,left=0')" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<bean:message key="prompt.orientacao" />
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
								</div>
							</td>
							<td>
								<div id="Layer2" name="Layer2" style="position: relative; width: 100%; z-index: 1; overflow: auto; height: 60; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000">
									<script>
										document.write(document.administracaoCsAstbProdutoManifPrmaForm.tpmaTxOrientacao.value);
									</script>
								</div>
							</td>
							<td><img src="webFiles/images/botoes/bt_CriarCarta.gif" name="imgCriarCarta" width="25" height="22" class="geralCursoHand" border="0" title="<bean:message key="prompt.editor"/>" onClick="MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=administracaoCsAstbProdutoManifPrmaForm.tpmaTxOrientacao&carta=false&tamanho=3900','Documento','width=850,height=494,top=0,left=0')" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<bean:message key="prompt.DesenhoDoProcesso" />
									<img src="webFiles/images/icones/setaAzul.gif" name="imgSeta" width="7" height="7">
								</div>
							</td>
							<td><html:select property="idDeprCdDesenhoProcesso" styleClass="principalObjForm">
									<html:option value="">
										<bean:message key="prompt.selecione_uma_opcao" />
									</html:option>
									<html:options collection="csCdtbDesenhoProcessoDeprVector" property="field(id_depr_cd_desenhoprocesso)" labelProperty="field(depr_ds_desenhoprocesso)" />
								</html:select></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<bean:message key="prompt.textopadrao" />
									<img src="webFiles/images/icones/setaAzul.gif" name="imgSeta" width="7" height="7">
								</div>
							</td>
							<td><html:select property="idTxpmCdTxpadraomanif" styleClass="principalObjForm">
									<html:option value="">
										<bean:message key="prompt.selecione_uma_opcao" />
									</html:option>
									<html:options collection="csCdtbTxpadraomanifTxpmVector" property="idTxpmCdTxpadraomanif" labelProperty="txpmDsTxpadraomanif" />
								</html:select></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<bean:message key="prompt.classificacao" />
									<img src="webFiles/images/icones/setaAzul.gif" name="imgSeta" width="7" height="7">
								</div>
							</td>
							<td><html:select property="idClmaCdClassifmanif" styleClass="principalObjForm">
									<html:option value="">
										<bean:message key="prompt.selecione_uma_opcao" />
									</html:option> ***<html:options collection="csCdtbClassifmaniClmaVector" property="idClmaCdClassifmanif" labelProperty="clmaDsClassifmanif" />
								</html:select></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<bean:message key="prompt.respostaEMail" />
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
								</div>
							</td>
							<td><html:select property="idDocuCdDocumento" styleClass="principalObjForm">
									<html:option value="">
										<bean:message key="prompt.selecione_uma_opcao" />
									</html:option>
									<html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento" />
								</html:select></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div align="right"></div>
							</td>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
									<tr>
										<td width="50%"><html:checkbox value="true" property="tpmaInConclui" /> <bean:message key="prompt.concluirTempoAtendimento" /></td>
										<td width="50%"><html:checkbox value="true" property="tpmaInOperadorResp" /> <bean:message key="prompt.operadorResponsavel" /></td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="4" align="right">
			<tr align="center">
				<td width="20"><img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="submeteSalvar();"></td>
				<td width="20"><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.limpar'/>" onclick="cancel();"></td>
				<td width="20"><img src="webFiles/images/botoes/out.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.sair'/>" onclick="sair()"></td>
			</tr>
		</table>
	</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%=Constantes.ACAO_EXCLUIR%>">
	<script>
		desabilitaCamposTpManifestacao();
		confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
		parent.setConfirm(confirmacao);
	</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%=Constantes.ACAO_INCLUIR%>">
	<script>
		setPermissaoImageEnable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_INCLUSAO_CHAVE%>', parent.administracaoCsAstbProdutoManifPrmaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
		if (!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_INCLUSAO_CHAVE%>')){
			desabilitaCamposTpManifestacao();
		}
	</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%=Constantes.ACAO_EDITAR%>">
	<script>
		if (!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_ALTERACAO_CHAVE%>', parent.administracaoCsAstbProdutoManifPrmaForm.imgGravar);	
			desabilitaCamposTpManifestacao();
		}
		if (!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_EXCLUSAO_CHAVE%>')){
			setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_TIPODAMANIFESTACAO_EXCLUSAO_CHAVE%>',administracaoCsAstbProdutoManifPrmaForm.lixeira);
		}
	</script>
</logic:equal>

</html>

