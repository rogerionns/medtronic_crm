<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function EhBissexto (ano){
					if (isNaN(ano)) 
					   return false;
					if ((ano - 1900) % 4 == 0)
					   return true
					else 
					   return false;
}

function filtrar(){
	
	document.administracaoCsCdtbCampanhaCampForm.target = admIframe.name;
	document.administracaoCsCdtbCampanhaCampForm.acao.value ='filtrar';
	document.administracaoCsCdtbCampanhaCampForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbCampanhaCampForm.filtro.value = '';
}

function submeteFormIncluir() {
	editIframe.document.administracaoCsCdtbCampanhaCampForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbCampanhaCampForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_CAMPANHA_CAMP%>';
	editIframe.document.administracaoCsCdtbCampanhaCampForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbCampanhaCampForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo){
	tab.document.administracaoCsCdtbCampanhaCampForm.idCampCdCampanha.value = codigo;
	tab.document.administracaoCsCdtbCampanhaCampForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_CAMPANHA_CAMP%>';
	tab.document.administracaoCsCdtbCampanhaCampForm.target = editIframe.name;
	tab.document.administracaoCsCdtbCampanhaCampForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbCampanhaCampForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function comparaData(ini,fim){	
	if (ini.value != "" && fim.value != ""){
		if(!validaPeriodo(ini.value,fim.value)){
			alert ('<bean:message key="prompt.Periodo_de_data_invalido"/>.');
			fim.focus();
			return false;
		}
	}
	
	return true;	
}

function submeteSalvar(){

	if(tab.document.administracaoCsCdtbCampanhaCampForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_CAMPANHA_CAMP%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbCampanhaCampForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_CAMPANHA_CAMP%>'){
		if (!comparaData(tab.document.administracaoCsCdtbCampanhaCampForm.campDhInicial,tab.document.administracaoCsCdtbCampanhaCampForm.campDhFinal)){
		  return false;
		}

		if (trim(tab.document.administracaoCsCdtbCampanhaCampForm.campDsCampanha.value) == "") {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbCampanhaCampForm.campDsCampanha.focus();
			return false;
		}
	
		
/*		
		if (tab.document.administracaoCsCdtbCampanhaCampForm.idOrigCdOrigem.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_o_campo_origem"/>.");
			tab.document.administracaoCsCdtbCampanhaCampForm.idOrigCdOrigem.focus();
			return false;
		}

		if (tab.document.administracaoCsCdtbCampanhaCampForm.idPesqCdPesquisa.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_o_campo_pesquisa"/>.");
			tab.document.administracaoCsCdtbCampanhaCampForm.idPesqCdPesquisa.focus();
			return false;
		}		

		if (tab.document.administracaoCsCdtbCampanhaCampForm.idDocuCdEmailBody.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_o_campo_Email_Body"/>.");
			tab.document.administracaoCsCdtbCampanhaCampForm.idDocuCdEmailBody.focus();
			return false;
		}		

		if (tab.document.administracaoCsCdtbCampanhaCampForm.idDocuCdDocumento.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_o_campo_documento"/>.");
			tab.document.administracaoCsCdtbCampanhaCampForm.idDocuCdDocumento.focus();
			return false;
		}		

		if (tab.document.administracaoCsCdtbCampanhaCampForm.idFuncCdFuncionario.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_um_funcionario"/>.");
			tab.document.administracaoCsCdtbCampanhaCampForm.idFuncCdFuncionario.focus();
			return false;
		}		
*/
		if (tab.document.administracaoCsCdtbCampanhaCampForm.campDhInicial.value == "") {
			alert("<bean:message key="prompt.O_campo_data_inicial_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbCampanhaCampForm.campDhInicial.focus();
			return false;
		}		
		
	    strCampo = "tab.document.administracaoCsCdtbCampanhaCampForm.campDhInicial"  + ".value"
	    strAcao = "tab.document.administracaoCsCdtbCampanhaCampForm.campDhInicial.value='';tab.document.administracaoCsCdtbCampanhaCampForm.campDhInicial"  + ".select()"
	    sData = new String(eval(strCampo));
		if ((sData.length != 10) && (sData.length != 0)){
			alert ("<bean:message key="prompt.Data_invalida_Favor_inseri-la_no_formato_DD_MM_AAAA"/>");
			eval(strAcao);
		    return false;
		}
				
		re = /\//;
		b1 = sData.search(re);
		b2 = (sData.substring(b1+1, sData.length)).search(re) +  b1 + 1;
				
		dia = sData.substring(0, b1);
		if (isNaN(dia)) {
			alert ("<bean:message key="prompt.Dia_invalido_Favor_corrigi-lo"/>.");
			eval(strAcao);
		    return false;
		}
		if (dia > 31) {
			alert ("<bean:message key="prompt.Dia_invalido_Favor_corrigir"/>.");
			eval(strAcao);
		    return false;
		}
				
		mes = sData.substring(b1+1, b2);
		if (isNaN(mes)) {
			alert ("<bean:message key="prompt.Mes_invalido_Favor_corrigi-lo"/>.");
			eval(strAcao);
		    return false;
		}
		if (mes > 12) {
			alert ("<bean:message key="prompt.Mes_invalido_Favor_corrigir"/>");
			eval(strAcao);
		    return false;
		}
				
		ano = sData.substring(b2+1, sData.length);
		if (isNaN(ano)) {
			alert ("<bean:message key="prompt.Ano_invalido_Favor_corrigi-lo"/>.");
			eval(strAcao);
		    return false;
		}
		if (ano < 1900){
			alert("<bean:message key="prompt.Ano_invalido"/>.");
			eval(strAcao);	   
			return false;
		}		
		if (strCampo == 0 ) { 
			if (ano.length != 4) {
				alert ("<bean:message key="prompt.Favor_digitar_o_ano_com_quatro_digitos"/>.");
				eval(strAcao);
			    return false;
			}}

		if (mes == 2){
		    if (dia >= 30){
				alert ("<bean:message key="prompt.Data_invalida"/>.");
				eval(strAcao);
	 	        return false;
		    }
			if (dia == 29)
				if (EhBissexto (ano) == 0){
					alert ("<bean:message key="prompt.O_ano_de"/> + ano + <bean:message key="prompt.nao_e_bissexto_portanto_a_data_e_invalida_Favor_corrigi-la"/>.");
					eval(strAcao);
				    return false;
				}
		}
		if ((mes == 4) && (dia == 31)){
			alert ("<bean:message key="prompt.O_mes_de_abril_so_tem30dias_favor_corrigir_a_data"/>.");
			eval(strAcao);
			return false;
		}
		if ((mes == 6) && (dia == 31)){
			alert ("<bean:message key="prompt.O_mes_de_junho_so_tem30dias_favor_corrigir_a_data"/>.");
			eval(strAcao);
			return false;
		}
		if ((mes == 9) && (dia == 31)){
			alert ("<bean:message key="prompt.O_mes_de_setembro_so_tem30dias_favor_corrigir_a_data"/>.");
			eval(strAcao);
			return false;
		}
		if ((mes == 11) && (dia == 31)){
			alert ("<bean:message key="prompt.O_mes_de_novembro_so_tem30dias_favor_corrigir_a_data"/>.");
			eval(strAcao);
			return false;
		}

		data = new Date(ano, mes-1, dia);
		if (isNaN(data)) {
			alert ("<bean:message key="prompt.Data_invalida_Favor_corrigi-la"/>.");
			eval(strAcao);
		    return false;
		}

		if (tab.document.administracaoCsCdtbCampanhaCampForm.campDhFinal.value == "") {
			alert("<bean:message key="prompt.O_campo_data_Final_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbCampanhaCampForm.campDhFinal.focus();
			return false;
		}
		
	    strCampo = "tab.document.administracaoCsCdtbCampanhaCampForm.campDhFinal"  + ".value"
	    strAcao = "tab.document.administracaoCsCdtbCampanhaCampForm.campDhFinal.value='';tab.document.administracaoCsCdtbCampanhaCampForm.campDhFinal"  + ".select()"
	    sData = new String(eval(strCampo));
		if ((sData.length != 10) && (sData.length != 0)){
			alert ("<bean:message key="prompt.Data_invalida_Favor_inseri-la_no_formato_DD_MM_AAAA"/>");
			eval(strAcao);
		    return false;
		}
				
		re = /\//;
		b1 = sData.search(re);
		b2 = (sData.substring(b1+1, sData.length)).search(re) +  b1 + 1;
				
		dia = sData.substring(0, b1);
		if (isNaN(dia)) {
			alert ("<bean:message key="prompt.Dia_invalido_Favor_corrigi-lo"/>.");
			eval(strAcao);
		    return false;
		}
		if (dia > 31) {
			alert ("<bean:message key="prompt.Dia_invalido_Favor_corrigir"/>.");
			eval(strAcao);
		    return false;
		}
				
		mes = sData.substring(b1+1, b2);
		if (isNaN(mes)) {
			alert ("<bean:message key="prompt.Mes_invalido_Favor_corrigi-lo"/>.");
			eval(strAcao);
		    return false;
		}
		if (mes > 12) {
			alert ("<bean:message key="prompt.Mes_invalido_Favor_corrigir"/>");
			eval(strAcao);
		    return false;
		}
				
		ano = sData.substring(b2+1, sData.length);
		if (isNaN(ano)) {
			alert ("<bean:message key="prompt.Ano_invalido_Favor_corrigi-lo"/>.");
			eval(strAcao);
		    return false;
		}
		if (ano < 1900){
			alert("<bean:message key="prompt.Ano_invalido"/>.");
			eval(strAcao);	   
			return false;
		}		
		if (strCampo == 0 ) { 
			if (ano.length != 4) {
				alert ("<bean:message key="prompt.Favor_digitar_o_ano_com_quatro_digitos"/>.");
				eval(strAcao);
			    return false;
			}}

		if (mes == 2){
		    if (dia >= 30){
				alert ("<bean:message key="prompt.Data_invalida"/>.");
				eval(strAcao);
	 	        return false;
		    }
			if (dia == 29)
				if (EhBissexto (ano) == 0){
					alert ("<bean:message key="prompt.O_ano_de"/> + ano + <bean:message key="prompt.nao_e_bissexto_portanto_a_data_e_invalida_Favor_corrigi-la"/>.");
					eval(strAcao);
				    return false;
				}
		}
		if ((mes == 4) && (dia == 31)){
			alert ("<bean:message key="prompt.O_mes_de_abril_so_tem30dias_favor_corrigir_a_data"/>.");
			eval(strAcao);
			return false;
		}
		if ((mes == 6) && (dia == 31)){
			alert ("<bean:message key="prompt.O_mes_de_junho_so_tem30dias_favor_corrigir_a_data"/>.");
			eval(strAcao);
			return false;
		}
		if ((mes == 9) && (dia == 31)){
			alert ("<bean:message key="prompt.O_mes_de_setembro_so_tem30dias_favor_corrigir_a_data"/>.");
			eval(strAcao);
			return false;
		}
		if ((mes == 11) && (dia == 31)){
			alert ("<bean:message key="prompt.O_mes_de_novembro_so_tem30dias_favor_corrigir_a_data"/>.");
			eval(strAcao);
			return false;
		}

		data = new Date(ano, mes-1, dia);
		if (isNaN(data)) {
			alert ("<bean:message key="prompt.Data_invalida_Favor_corrigi-la"/>.");
			eval(strAcao);
		    return false;
		}


		if (tab.document.forms[0].campInDistribuicao.checked && tab.validateSum() != 100) {
			if(!confirm("A soma dos percentuais de distribui��o � menor/maior que 100% ou alguma Sub-Campanha que esta associada esta inativa.\nSer� necess�rio corrigir a distribui��o antes da execu��o da estrat�gia.\n\nDeseja gravar mesmo assim?")) {
				return false;
			}
		}
		
		var t = editIframe.document.getElementsByName("publNrPercdistrib");

		if(t.length>0){
			for(var i = 0; i < t.length; i++) {
				t[i].disabled = false;
			}
		}
		
		tab.document.administracaoCsCdtbCampanhaCampForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_CAMPANHA_CAMP%>';
		tab.document.administracaoCsCdtbCampanhaCampForm.target = admIframe.name;
		disableEnable(tab.document.administracaoCsCdtbCampanhaCampForm.idCampCdCampanha, false);
		tab.document.administracaoCsCdtbCampanhaCampForm.submit();
		disableEnable(tab.document.administracaoCsCdtbCampanhaCampForm.idCampCdCampanha, true);
		cancel();
		
		
	}
}

function submeteExcluir(codigo) {
	
	tab.document.administracaoCsCdtbCampanhaCampForm.idCampCdCampanha.value = codigo;
	tab.document.administracaoCsCdtbCampanhaCampForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_CAMPANHA_CAMP%>';
	tab.document.administracaoCsCdtbCampanhaCampForm.target = editIframe.name;
	tab.document.administracaoCsCdtbCampanhaCampForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbCampanhaCampForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbCampanhaCampForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_CAMPANHA_CAMP%>';
		editIframe.document.administracaoCsCdtbCampanhaCampForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbCampanhaCampForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbCampanhaCampForm.idCampCdCampanha, false);
		editIframe.document.administracaoCsCdtbCampanhaCampForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbCampanhaCampForm.idCampCdCampanha, true);
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbCampanhaCamp.do?tela=editCsCdtbCampanhaCamp&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbCampanhaCamp.do?tela=editCsCdtbCampanhaCamp&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsCdtbCampanhaCampForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbCampanhaCampForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_CAMPANHA_CAMP%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormal');
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbCampanhaCampForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_CAMPANHA_CAMP%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionado');	
			setaIdiomaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbCampanhaCampForm"	action="/AdministracaoCsCdtbCampanhaCamp.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/><!-- ## --></td>
									<td class="principalPstQuadroLinkNormal" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.campanha"/> <!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="width: 97%; height: 225px;display: none"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsCdtbCampanhaCamp.do?tela=editCsCdtbCampanhaCamp&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="width: 97%; height: 199px;display: block"> 
                    		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalLabel" width="15%"><bean:message key="prompt.descricao"/></td>
									<td class="principalLabel" width="65%">&nbsp;</td>
								</tr>

								<tr>
									<td width="65%">
									<table border="0" cellspacing="0" cellpadding="0">
									<tr>
									<td width="25%">
									<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
									</td>
									<td width="05%">
									&nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
									</td>
									</tr>	
									</table>
									</td>
								</tr>
								<tr>
									<td class="principalLabel" width="15%">&nbsp;</td>
									<td class="principalLabel" width="64%">&nbsp;</td>
								</tr>
							</table>
							<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalLstCab" width="18%">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalLstCab" width="25%">
													&nbsp;
												</td>
												<td class="principalLstCab" width="75%">
													&nbsp;&nbsp;<bean:message key="prompt.codigo"/>
												</td>
											</tr>
										</table>
									</td>
									<td class="principalLstCab" width="61%"><bean:message key="prompt.descricao"/></td>
									<td class="principalLstCab" align="left" width="20%">&nbsp;&nbsp;<bean:message key="prompt.inativo"/> </td>
								</tr>
								
								<tr valign="top">
									
                        <td colspan="3" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsCdtbCampanhaCamp.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                </td>
							<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" style='background: url("webFiles/images/linhas/VertSombra.gif") repeat-y 0px 0px;'>&nbsp;</td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPANHA_INCLUSAO_CHAVE%>', document.administracaoCsCdtbCampanhaCampForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPANHA_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPANHA_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbCampanhaCampForm.imgGravar.disabled=true;
			document.administracaoCsCdtbCampanhaCampForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbCampanhaCampForm.imgGravar.title='';
	}
	
	desabilitaListaEmpresas();

	setaArquivoXml("CS_ASTB_IDIOMACAMP_IDCP.xml");
	habilitaTelaIdioma();
	
</script>



</body>
</html>