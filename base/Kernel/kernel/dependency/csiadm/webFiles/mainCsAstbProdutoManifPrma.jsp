<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.util.Geral"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>
<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
fileInclude = Geral.getActionProperty("funcoesJSAdm", empresaVo.getIdEmprCdEmpresa()) + "/funcoes/funcoesTelaManifProduto.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>' />
<bean:write name="funcoesPessoa" filter="html"/>
</script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsAstbProdutoManifPrmaForm.target = admIframe.name;
	document.administracaoCsAstbProdutoManifPrmaForm.acao.value ='filtrar';
	document.administracaoCsAstbProdutoManifPrmaForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsAstbProdutoManifPrmaForm.filtro.value = '';
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
	document.administracaoCsAstbProdutoManifPrmaForm.filtro2.value = '';
<%}%>
}

function submeteFormIncluir() {
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.target = editIframe.name;
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_PRODUTOMANIF_PRMA%>';
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo1,codigo2,codigo3){
	tab.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value = codigo1;	
	tab.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value = codigo2;
	tab.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value = codigo3;
	tab.document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_PRODUTOMANIF_PRMA%>';
	tab.document.administracaoCsAstbProdutoManifPrmaForm.target = editIframe.name;
	tab.document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsAstbProdutoManifPrmaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){
	bUsaCheks = false;
	/*
	if(tab.document.administracaoCsAstbProdutoManifPrmaForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_PRODUTOMANIF_PRMA%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsAstbProdutoManifPrmaForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_ASTB_PRODUTOMANIF_PRMA%>'){
	*/
	
	
	if(window.document.all.item("Manifestacao").style.visibility == "hidden"){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else{
	
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
			editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value = 1;
		<%}else{%>	
			getidLinhCdLinha();
		<%}%>
		
		getidAsn1CdAssuntoNivel1();
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			getidAsn2CdAssuntoNivel2();
		<%}%>
		getidMatpCdManifTipo();
		getidGrmaCdGrupoManifestacao();
		getidTpmaCdTpManifestacao();
		getidDeprCdDestinoproduto();
		getidPesqCdPesquisa();

		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			if(tab.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value=="0"){
				alert("<bean:message key="prompt.Por_favor_selecione_uma_Linha"/>.");
				tab.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.focus();
				return false;
			}
			
			if(tab.administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.checked){
				bUsaCheks = true;
				if(tab.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value=="0"){
					alert("<bean:message key="prompt.Por_favor_selecione_uma_Linha"/>.");
					tab.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.focus();
					return false;
				}
			}
		<%}%>
			
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
			if(tab.administracaoCsAstbProdutoManifPrmaForm.chkProdutoTipo.checked){
				bUsaCheks = true;
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
					if(tab.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value=="0"){
						alert("<bean:message key="prompt.Por_favor_selecione_uma_Linha"/>.");
						tab.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.focus();
						return false;
					}
				<%}%>
				if (tab.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value == "0") {
					alert("<bean:message key="prompt.Por_favor_selecione_um_produto"/>.");
					tab.ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.focus();
					return false;
				}
			}
		<%}%>
		
		if(tab.administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.checked){
			bUsaCheks = true;
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
				if(tab.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value=="0"){
					alert("<bean:message key="prompt.Por_favor_selecione_uma_Linha"/>.");
					tab.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.focus();
					return false;
				}
			<%}%>
			if (tab.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value == "0") {
				alert("<bean:message key="prompt.Por_favor_selecione_um_produto"/>.");
				tab.ifrmCmbLinhaProduto.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.focus();
				return false;
			}

			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				if (tab.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value == "0" || tab.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value == "") {
					alert("<bean:message key="prompt.Por_favor_selecione_uma_variedade"/>.");
					tab.ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.focus();
					return false;
				}
			<%}%>
		} 
		else if (tab.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value == "0" && !tab.administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.checked) {
			alert("<bean:message key="prompt.Por_favor_selecione_um_produto"/>.");
			tab.ifrmCmbLinhaProduto.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.focus();
			return false;
		}
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			else if (tab.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value == "0" && tab.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value == "" && !tab.administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.checked && !tab.administracaoCsAstbProdutoManifPrmaForm.chkProdutoTipo.checked) {
				alert("<bean:message key="prompt.Por_favor_selecione_uma_variedade"/>.");
				tab.ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.focus();
				return false;
			}
		<%}%>

		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
			if(tab.administracaoCsAstbProdutoManifPrmaForm.chkSuperGrupoManif.checked){
				bUsaCheks = true;		
				if (tab.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value == "0") {
					alert("<bean:message key="prompt.Por_favor_selecione_uma_manifestacao"/>");
					tab.ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.focus();
					return false;
				}
			} else if(tab.administracaoCsAstbProdutoManifPrmaForm.chkGrupoManifestacao.checked){
				bUsaCheks = true;		
				if (ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.value == "0") {
					alert("<bean:message key="prompt.Por_favor_selecione_um_segmento"/>");
					tab.ifrmCmbSuperGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idSugrCdSupergrupo.focus();
					return false;
				}
			}else if(tab.administracaoCsAstbProdutoManifPrmaForm.chkGrupo.checked){
				bUsaCheks = true;
				if (tab.ifrmCmbGrupoManif.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value == "0") {
					alert("<bean:message key="prompt.Por_favor_selecione_um_grupo_manifestacao"/>");
					tab.ifrmCmbGrupoManif.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.focus();
					return false;
				}
			}else if (!tab.administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.checked && editIframe.ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value == "0") {
				alert("<bean:message key="prompt.Por_favor_selecione_uma_manifestacao"/>");
				editIframe.ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.focus();
				return false;
			}
		<%} else {%>
			if(tab.administracaoCsAstbProdutoManifPrmaForm.chkGrupoManifestacao.checked){
				bUsaCheks = true;		
				if (tab.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value == "0") {
					alert("<bean:message key="prompt.Por_favor_selecione_uma_manifestacao"/>");
					tab.ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.focus();
					return false;
				}
			}else if(tab.administracaoCsAstbProdutoManifPrmaForm.chkGrupo.checked){
				bUsaCheks = true;
				if (tab.ifrmCmbGrupoManif.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value == "0") {
					alert("<bean:message key="prompt.Por_favor_selecione_um_grupo_manifestacao"/>");
					tab.ifrmCmbGrupoManif.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.focus();
					return false;
				}
			}else if (!tab.administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.checked && editIframe.ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value == "0") {
				alert("<bean:message key="prompt.Por_favor_selecione_uma_manifestacao"/>");
				editIframe.ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.focus();
				return false;
			}
		<%}%>
		
		if(!bUsaCheks){
			if (tab.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value == "0") {
				alert("<bean:message key="prompt.Por_favor_selecione_um_produto"/>.");
				tab.ifrmCmbLinhaProduto.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.focus();
				return false;
			}
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				if (tab.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value == "0") {
					alert("<bean:message key="prompt.Por_favor_selecione_uma_variedade"/>.");
					tab.ifrmCmbLinhaProdutoVariedade.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.focus();
					return false;
				}
			<%}%>
			if (tab.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value == "0") {
				alert("<bean:message key="prompt.Por_favor_selecione_uma_manifestacao"/>");
				tab.ifrmCmbManifestacao.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.focus();
				return false;
			}
		}

		//Verificando valida��es especiais dependendo do cliente
		if (validarFormEspec()){
			
			tab.document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_PRODUTOMANIF_PRMA%>';
			tab.document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= Constantes.ACAO_INCLUIR %>';
			
			tab.document.administracaoCsAstbProdutoManifPrmaForm.target = admIframe.name;
	//		disableEnable(tab.ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1, false);
	
			tab.administracaoCsAstbProdutoManifPrmaForm.optProduto.disabled = false;
			tab.administracaoCsAstbProdutoManifPrmaForm.optAssunto.disabled = false;
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
				tab.administracaoCsAstbProdutoManifPrmaForm.chkTipoLinha.disabled = false;
			<%}%>
			tab.administracaoCsAstbProdutoManifPrmaForm.chkManifestacao.disabled =  false;
		
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
				tab.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.disabled = false;
			<%}%>
			
			tab.ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled = false;
			
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
				tab.ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].disabled =  false;			
				tab.administracaoCsAstbProdutoManifPrmaForm.chkProdutoTipo.disabled =  false;			
			<%}%>
			tab.ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.disabled = false;
			tab.ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.disabled = false;
			tab.ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.disabled = false;
	
			window.top.document.all.item('LayerAguarde').style.visibility = 'visible';

			tab.document.administracaoCsAstbProdutoManifPrmaForm.submit();
	//		disableEnable(tab.ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1, true);
			//cancel();
	
			//setTimeout("editIframe.lstArqCarga.location.reload()", 2500);
		/*	document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= Constantes.ACAO_VISUALIZAR %>';
			document.administracaoCsAstbProdutoManifPrmaForm.tela.value = 'administracaoLstCsAstbProdutoManifPrma';
			document.administracaoCsAstbProdutoManifPrmaForm.target = tab.lstArqCarga.name;
			document.administracaoCsAstbProdutoManifPrmaForm.submit();*/
			
			tab.ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value = "0";
			tab.ifrmCmbDestprod.document.administracaoCsAstbProdutoManifPrmaForm.idDeprCdDestinoproduto.value = "0";
			tab.ifrmCmbPesq.document.administracaoCsAstbProdutoManifPrmaForm.idPesqCdPesquisa.value=0;
			tab.ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value=0;
			
			// zerando os selects de manifestacao
			document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
			document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_GRUPOMANIF %>';
			document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value = "0"; 
			document.administracaoCsAstbProdutoManifPrmaForm.target = tab.ifrmCmbGrupoManif.name;
			document.administracaoCsAstbProdutoManifPrmaForm.submit();
			
			document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value = "0";
			document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_TIPOMANIF %>';
			document.administracaoCsAstbProdutoManifPrmaForm.target = tab.ifrmCmbLinhaTipoManif.name;
			document.administracaoCsAstbProdutoManifPrmaForm.submit();
			
			tab.administracaoCsAstbProdutoManifPrmaForm.receptiva.checked = false;
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
			tab.administracaoCsAstbProdutoManifPrmaForm.ativa.checked = false;
			<%} %>			
			tab.administracaoCsAstbProdutoManifPrmaForm.questionario.checked = false;

			tab.administracaoCsAstbProdutoManifPrmaForm.prmaInWeb.checked = false;

			window.top.document.all.item('LayerAguarde').style.visibility = 'visible';
		}
	//	editIframe.filtrar();
	}
}
			
/*			<!-- Diego - Habilita/Desabilita campos da feature TAG_PRODUTO_ATIVO -->
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
            	tab.document.administracaoCsAstbProdutoManifPrmaForm.ativa.checked = false;
			<%} %>			
			
			tab.document.administracaoCsAstbProdutoManifPrmaForm.questionario.checked = false;
		}
	}
}*/

function submeteFormExcluir() {
	var bExcluir = false;
	
	if(tab.lstArqCarga.administracaoCsAstbProdutoManifPrmaForm.listaExcluir.length != undefined){
		for(i=0; i < tab.lstArqCarga.administracaoCsAstbProdutoManifPrmaForm.listaExcluir.length;i++){
			if(tab.lstArqCarga.administracaoCsAstbProdutoManifPrmaForm.listaExcluir[i].checked){
				bExcluir = true
			}
		}
	}else{
		if(tab.lstArqCarga.administracaoCsAstbProdutoManifPrmaForm.listaExcluir != undefined){
			if(tab.lstArqCarga.administracaoCsAstbProdutoManifPrmaForm.listaExcluir.checked){
			 	bExcluir = true
			}
		}
	}

	//tab.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_PRODUTOMANIF_PRMA%>';
	//tab.administracaoCsAstbProdutoManifPrmaForm.target = editIframe.name;
	//tab.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	//tab.administracaoCsAstbProdutoManifPrmaForm.submit();
	
	if(bExcluir){
		if(tab.lstArqCarga != undefined){
			if(confirm("<bean:message key="prompt.alert.temCertezaQueDesejaExcluirSelecionados" />")){
			tab.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value = tab.ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value;
			tab.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value = tab.ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value;
			tab.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value = tab.ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value;
			tab.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value = tab.ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value;
			
		    <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
		    	tab.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value = tab.ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm["idAsn2CdAssuntoNivel2"].value;
		    <%}%>
			
			window.top.document.all.item('LayerAguarde').style.visibility = 'visible';
			tab.lstArqCarga.submeteFormExcluir();
		    
		    setTimeout("editIframe.filtrar()",1000);
		    
			}	
		}
		
	}else{
		alert("<bean:message key='prompt.naoExisteNenhumaAssociacaoSelecionada'/>");
	}
}


function submeteExcluir(codigo1,codigo2,codigo3) {

	tab.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value = codigo1;
	tab.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value = codigo2;	
	tab.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value = codigo3;

	tab.document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_PRODUTOMANIF_PRMA%>';
	tab.document.administracaoCsAstbProdutoManifPrmaForm.target = editIframe.name;
	tab.document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsAstbProdutoManifPrmaForm.submit();
	
	loadingIframe();	
}

function loadingIframe(){
	var a;
	if (editIframe.document.readyState != 'complete'){
		a = setTimeout('loadingIframe()', 100);
	}else{
		setConfirm(true);
		clearTimeout(a);
	}
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_PRODUTOMANIF_PRMA%>';
		editIframe.document.administracaoCsAstbProdutoManifPrmaForm.target = admIframe.name;
		editIframe.document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		//disableEnable(editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1, false);
		editIframe.document.administracaoCsAstbProdutoManifPrmaForm.submit();
		//disableEnable(editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1, true);

		/////////////////////////////////
		//AtivarPasta(admIframe);
		//MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		//editIframe.location = 'AdministracaoCsAstbProdutoManifPrma.do?tela=editCsAstbProdutoManifPrma&acao=incluir';
		/////////////////////////////////
	
		tab.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value = "";
		setTimeout("editIframe.lstArqCarga.location.reload()", 2000);
	}else{
		//cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsAstbProdutoManifPrma.do?tela=editCsAstbProdutoManifPrma&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsAstbProdutoManifPrmaForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function getidAsn1CdAssuntoNivel1() {
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value = editIframe.ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value;
	document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value = editIframe.ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value;
}

function getidAsn2CdAssuntoNivel2() {
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value = editIframe.ifrmCmbLinhaProdutoVariedade.document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value;
}


function getidTpmaCdTpManifestacao() {
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value = editIframe.ifrmCmbLinhaTipoManif.document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value;
}

function getidMatpCdManifTipo() {
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value = editIframe.ifrmCmbManifestacao.document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value;
}

function getidDeprCdDestinoproduto() {
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idDeprCdDestinoproduto.value = editIframe.ifrmCmbDestprod.document.administracaoCsAstbProdutoManifPrmaForm.idDeprCdDestinoproduto.value;
}

function getidPesqCdPesquisa() {
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idPesqCdPesquisa.value = editIframe.ifrmCmbPesq.document.administracaoCsAstbProdutoManifPrmaForm.idPesqCdPesquisa.value;
}

function getidGrmaCdGrupoManifestacao() {
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value = editIframe.ifrmCmbGrupoManif.document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value;
}

function getidLinhCdLinha() {
	editIframe.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value = editIframe.ifrmCmbLinhaLinh.document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value;
}


function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_PRODUTOMANIF_PRMA%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			document.administracaoCsAstbProdutoManifPrmaForm.imgLixeira.style.display='none'; 	
			break;
		case editIframe:
			tab.document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_PRODUTOMANIF_PRMA%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');
			document.administracaoCsAstbProdutoManifPrmaForm.imgLixeira.style.display='block'; 	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsAstbProdutoManifPrmaForm"	action="/AdministracaoCsAstbProdutoManifPrma.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="idLinhCdLinha" />
	<html:hidden property="idAsn1CdAssuntoNivel1" />
	<html:hidden property="idAsn2CdAssuntoNivel2" />	
	<html:hidden property="idTpmaCdTpManifestacao" />
	<html:hidden property="idDeprCdDestinoproduto" />	
	<html:hidden property="idPesqCdPesquisa" />
	<html:hidden property="idMatpCdManifTipo" />	
	<html:hidden property="idGrmaCdGrupoManifestacao" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">

					</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/><!-- ## --></td>
									<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.AssuntoGrupo"/><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0" height="450" align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="450"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="position: absolute; width: 97%; height: 400px; z-index: 6; visibility: hidden"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="425" valign="top"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsAstbProdutoManifPrma.do?tela=editCsAstbProdutoManifPrma&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="position: absolute; width: 97%; height: 400px; z-index: 2; visibility: visible"> 
                    		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									
									<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
											<td class="principalLabel" width="30%"><%= getMessage("prompt.assuntoNivel1", request)%></td>
											<td class="principalLabel" width="70%"  colspan="2"><%= getMessage("prompt.assuntoNivel2", request)%></td>		
									<%}else{%>
											<td class="principalLabel" width="100%"><bean:message key="prompt.produtoAssunto"/></td>
									<%}%>

								</tr>

								<tr>
										<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
												<td width="25%">
													<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
												</td>
												<td width="25%">
													<html:text property="filtro2" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
												</td>		
										<%}else{%>
												<td width="50%">
													<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
												</td>
										<%}%>

									<td width="05%">
									&nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
									</td>

								</tr>
								<tr>
									
                        <td class="principalLabel" colspan="3">&nbsp;</td>
						</tr>
						<tr>
						
						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
							<td colspan="3">			
	                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	                              <tr>
			                          <td class="principalLstCab" width="10%">&nbsp;<bean:message key="prompt.codigo"/></td>
			                          <td class="principalLstCab" width="40%">&nbsp;&nbsp;<%= getMessage("prompt.linha", request)%> </td>
			                          <td class="principalLstCab" align="left" width="50%"> <%= getMessage("prompt.Produto", request)%></td>
			                      </tr>
	                          </table>	
	                        </td>
						<%}else{%>			
                       		<td colspan="3">			
	                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	                              <tr>
			                           <td class="principalLstCab" width="11%">&nbsp;<bean:message key="prompt.codigo"/></td>
			                          <td class="principalLstCab" align="left" width="89%"> <%= getMessage("prompt.Produto", request)%></td>
			                      </tr>
	                          </table>	
	                        </td>
                       <%}%>	
									
								</tr>
								
								<tr valign="top">
									
                        <td colspan="3" height="360"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsAstbProdutoManifPrma.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                </td>
							<td width="4" height="100%"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="40" align="right">
							<img src="webFiles/images/botoes/lixeira18x18.gif" name="imgLixeira" width="18" height="18" class="geralCursoHand" title="<bean:message key='prompt.excluir'/>" onclick="submeteFormExcluir()"  style="display:none" >
					</td>
					<td width="20" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="540px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="540px"></td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFESTACAOPRODUTO_INCLUSAO_CHAVE%>', document.administracaoCsAstbProdutoManifPrmaForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFESTACAOPRODUTO_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFESTACAOPRODUTO_ALTERACAO_CHAVE%>')){
			administracaoCsAstbProdutoManifPrmaForm.imgGravar.disabled=true;
			administracaoCsAstbProdutoManifPrmaForm.imgGravar.className = 'geralImgDisable';
			administracaoCsAstbProdutoManifPrmaForm.imgGravar.title='';
	}
	
	desabilitaListaEmpresas();
	desabilitaTelaIdioma();
	
</script>



</body>
</html>