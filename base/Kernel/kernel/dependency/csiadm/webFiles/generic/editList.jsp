<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.form.GenericAdmForm"%>
<%@ page import="br.com.plusoft.fw.entity.*"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.Vector"%>

<% 
String fileInclude="/webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="/webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	

StringBuffer objAuxiliarLista = new StringBuffer("");

Entity entity = null;
entity = EntityInstance.getEntity(request.getParameter("entityName"));

%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@page import="java.util.Iterator"%>
<%@page import="org.apache.struts.taglib.TagUtils"%>

<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script type="text/javascript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script type="text/javascript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script type="text/javascript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script type="text/javascript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script type="text/javascript" src="webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript" src="webFiles/funcoes/number.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>

<script>
	
	function MM_openBrWindow(theURL,winName,features) { //v2.0
		modalWin = window.open(theURL,winName,features);
		if (modalWin!=null && !modalWin.closed){
			//self.blur();
			modalWin.focus();
		}
	}
 	function habilitaCampos(disabled){
		for ( var i=0; i< document.forms[0].elements.length; i++){
			var e = document.forms[0].elements[i];
			if (e.field == "true"){
				e.disabled = disabled;
			}
		}
	}	
	
	function setFunction(cRetorno, campo){		
		document.getElementById(campo+"_div").innerHTML = cRetorno;
		if(document.getElementById(campo+"_div").innerText == ''){	
			document.getElementById(campo+"_div").innerHTML = "";
			eval(campo).value = '';		
		}else{
			eval(campo).value = cRetorno;		
		}		
		
	}
	
	var pkValue = "";
	function onLoad(){
		if(document.getElementById("list") != undefined){
			telaAntiga = document.forms[0].tela.value;
			acaoAntiga = document.forms[0].acao.value;
			document.forms[0].tela.value = 'list';
			document.forms[0].acao.value = 'consultar';
			document.forms[0].target = 'list';
			document.forms[0].submit();
			document.forms[0].tela.value = telaAntiga;
			document.forms[0].acao.value = acaoAntiga;
			document.forms[0].target = 'edit';	
		}
		
		showError('<%=request.getAttribute("msgerro")%>');
		parent.setFocusFirstObj();
		
		<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		
			habilitaCampos(true);
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.confirm_remove(confirmacao);
			
			setTimeout('recarregarLista()',300);
		</logic:equal>
		
		setaAssociacaoMultiEmpresa();
		
		if(pkValue != ";")
			setaChavePrimaria(pkValue);
		else
			setaChavePrimaria("");
	}

	function recarregarLista(){
		if(document.getElementById("list") != undefined){
			telaAntiga = document.forms[0].tela.value;
			acaoAntiga = document.forms[0].acao.value;
			document.forms[0].tela.value = 'list';
			document.forms[0].acao.value = 'consultar';
			document.forms[0].target = 'list';
			document.forms[0].submit();
			document.forms[0].tela.value = telaAntiga;
			document.forms[0].acao.value = acaoAntiga;
			document.forms[0].target = 'edit';	
		}
	}


	function txtFiltro_onKeyDown(e, idCombo, keyCombo, descCombo) {
		if(e.keyCode==13) {
			preencheCombo(idCombo, keyCombo, descCombo);
			return false;
		}

		if(e.keyCode==27) {
			cancelaFiltrar(idCombo);
			return false;
		}

		return true;
	}

	function filtrarCombo(idCombo) {
		$("divCmb"+idCombo).style.display="none";
		$("divFiltro"+idCombo).style.display="block";

		$("txtFiltro"+idCombo).value="";
		$("txtFiltro"+idCombo).focus();
		
	}

	function cancelaFiltrar(idCombo) {
		$("divCmb"+idCombo).style.display="block";
		$("divFiltro"+idCombo).style.display="none";

		$(idCombo).focus();
	}

	var ajax;
	function preencheCombo(idCombo, keyCombo, descCombo) {
		ajax = new ConsultaBanco("","GenericAdm.do");	
		
		ajax.addField("tela", "filtrarCombo");	
		ajax.addField("acao", "incluir");

		ajax.addField("field", idCombo);
		ajax.addField("entityName", "<%=request.getParameter("entityName")%>");
		ajax.addField("temMultiempresa", "<%=request.getParameter("temMultiempresa")%>");
		ajax.addField("temMultiIdioma", "<%=request.getParameter("temMultiIdioma")%>");
		
		ajax.addField("filtro", $("txtFiltro"+idCombo).value.toUpperCase());
			

		ajax.executarConsulta(
			function() { 
				ajax.popularCombo($(idCombo), keyCombo.toLowerCase(), descCombo.toLowerCase(), "", true, "");
				ajax = null;

				$("divCmb"+idCombo).style.display="block";
				$("divFiltro"+idCombo).style.display="none";


				$(idCombo).focus();
			});
	}

</script>

<body class= "principalBgrPageIFRM" onload="onLoad();">

<html:form styleId="genericAdmForm" enctype="multipart/form-data" action="/GenericAdm.do">

	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="CDataInativo"/>
	<html:hidden property="entityName"/>
	<html:hidden property="entityEmpresaName"/>
	<html:hidden property="entityIdiomaName"/>
	<html:hidden property="temMultiempresa"/>
	<html:hidden property="temMultiIdioma"/>
	<html:hidden property="idIdioma"/>
	
	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" >
		<%
//		Enumeration lista = entity.getFields().keys();
		Vector lista = entity.getFieldByOrder();
		GenericAdmForm genericAdmForm = (GenericAdmForm)request.getAttribute("genericAdmForm");
		Vo voRequest = genericAdmForm.getVo();		
		
		String objStyle = "";
		String objName = "";
		String objType = "";
		int objMaxlenght = 0;
		String objSize = "0";
		String objDisabled = "";
		String objClassname = "";
		String valor = "";
		String onClick = "";
		String checked = "";
		String objFunctions = new String("");
		String objOldType = "";

		String sizeView = "315";
		
		StringBuffer objAuxiliar = null;
		
		//percorre todos os fields da entidade
		for (int i = 0; i < lista.size(); i++){
//		while(lista.hasMoreElements()){
			//String key = (String)lista.nextElement();
			//Field field = (Field)entity.getFields().get(key);
			Field field = (Field)lista.get(i);
			objName = field.getName();
			objStyle = field.getStyle();
			View view = field.getViewByName("edit.jsp");
			objMaxlenght = field.getLen();
			objOldType = "";
			
			//Caso o campo o nao seja um campo fisico ou esteja configurado na view que o atributo nao pode ser visualizado deve-se pular esse campo
			if ((view != null && !view.getVisible())){continue;}
			
			boolean fieldInativo = false;
			
			valor = "";
			if (voRequest!=null){
				valor = voRequest.getFieldAsString(objName);
				
				if(field.getType()!=null && field.getType().equalsIgnoreCase("Datetime")) {
					if(view!=null && !"".equals(view.getFormat())) {
					    String format = view.getFormat();
					    
						valor = voRequest.getFieldByFormat(field.getName(), view.getFormat()) + "";
					
						if (valor==null || valor.equals("null")){
							valor = "";
						}
					}
				}
			}
			
			objAuxiliar = new StringBuffer("");

			objAuxiliarLista.append(" <td style=\"width:20%\" class=\"principalLstCab\"> " + getMessage(field.getLabel(), request) + "</td>");
			objFunctions = "";
			if(view != null){
				if(view.getScripts() != null){
					Vector scriptFunction = (Vector)view.getScripts();					
					for (int i2 = 0; i2 < scriptFunction.size(); i2++) {
						View functions = (View)scriptFunction.get(i2);
						if(functions.getNameFunction() != null && !functions.getNameFunction().equals("") && functions.getCodeFunction() != null && ! functions.getCodeFunction().equals("")) 
							objFunctions = objFunctions + functions.getNameFunction() + "="  + functions.getCodeFunction();
					}
				}

			}
			
			if (field.getName().indexOf("dh_inativo") != -1){
				if (voRequest!=null){
					valor = String.valueOf( voRequest.getFieldByFormat(field.getName(), "DD/MM/YYYY HH:MI:SS"));
				}
				if (valor==null || valor.equals("null")){
					valor = "";
				}
				
				objType = "hidden";	
				objClassname = "";
				fieldInativo = true;
				
				//Coloca a constante de data atual
				if (valor==null || valor.equalsIgnoreCase("")){
					//valor = "#SYSDATE#";
				}
				
				objAuxiliar.append( "<input type='checkbox' name='" + objName + "_checkbox' ");
				objAuxiliar.append( "onclick=\"if (this.checked){genericAdmForm." + field.getName() + ".value = '"+ valor + "';}else{genericAdmForm." + field.getName() + ".value = '';}\"");
				objAuxiliar.append( "/>");
				
				objAuxiliar.append( "<script>");
				objAuxiliar.append( "if (genericAdmForm." + field.getName() + ".value != ''){genericAdmForm." + objName + "_checkbox.checked = true;}");
				
				objAuxiliar.append( "</script>");
			}else if(field.hasFk()){
				
				if(view != null && view.isIframe()){
					objAuxiliar.append("<input type='hidden' name='" + objName + "' value='" + valor + "' scriptValidate=\"" + field.getScriptValidate("edit.jsp") + "\" />");	
					objAuxiliar.append("<iframe name='" + objName + "_iframe' id='" + objName + "_iframe' src='GenericAdm.do?acao=incluir&tela=combo&field=" + objName +"&entityName="+ request.getParameter("entityName") +"&entityEmpresaName="+ request.getParameter("entityEmpresaName") +"&entityIdiomaName="+ request.getParameter("entityIdiomaName") +"&temMultiempresa="+ request.getParameter("temMultiempresa") +"&temMultiIdioma="+ request.getParameter("temMultiIdioma") +"' width='100%' height='20px' scrolling='No' frameborder='0' marginwidth='0' marginheight='0' ></iframe>");
					
					if(!valor.equals("")){
						objAuxiliar.append( "<script>");
						objAuxiliar.append(" function carregaCombo_" + objName + "() {");			
						objAuxiliar.append(objName + "_iframe.genericAdmForm." + objName + ".value = " + valor + "");
						objAuxiliar.append(" }");					
						objAuxiliar.append( "</script>  ");					
					}
				}else{		
				    //Chamado: 106926 - 23/02/2016 - Carlos Nunes		
					String objRetornoName = field.getFk().getEntity().getName().toLowerCase();
					
					String objKeyName = field.getFk().getKey();
					
					if(field.getFk().getKey().toLowerCase().equalsIgnoreCase("id_mech_cd_maxesperafila") ||
					   field.getFk().getKey().toLowerCase().equalsIgnoreCase("id_mech_cd_forahorario") ||
					   field.getFk().getKey().toLowerCase().equalsIgnoreCase("id_mech_cd_motencercliente") ||
					   field.getFk().getKey().toLowerCase().equalsIgnoreCase("id_mech_cd_motencerabandono")){
						
						objRetornoName = field.getFk().getKey().toLowerCase() + "_vector";
						objKeyName = "id_mech_cd_motencerchat";
					}
					
					if(request.getAttribute(objRetornoName)!=null){					
						// jvarandas
						// Alterado para exibir a lupa para filtrar combo devido ao limite de resultados
						
						objAuxiliar.append( "<div id='divCmb"+objName+"' style=\"width: 360px;\">");
						
						if(view != null && view.getSizeview() != null && !view.getSizeview().equalsIgnoreCase("")){
							sizeView = view.getSizeview();
						}
						
						objAuxiliar.append( "<select name='" + objName + "' class='principalObjForm' style='width: "+sizeView+"px; float: left' " + objFunctions + " scriptValidate=\"" + field.getScriptValidate("edit.jsp") + "\">");
						objAuxiliar.append( "	<option value=''>" + getMessage("prompt.selecione_uma_opcao",request) + "</option>");
						
						for(Iterator it = ((Vector)request.getAttribute(objRetornoName)).iterator() ; it.hasNext();){
							Vo vo = (Vo)it.next();
							objAuxiliar.append( "  		<option value='" + vo.getFieldAsString(objKeyName) + "'>" + vo.getFieldAsString(field.getFk().getDescription()) + "</option>");
						}		

						objAuxiliar.append( "</select>   ");

						if(request.getAttribute("exibirLupa"+objName)!=null) {
								
							objAuxiliar.append("<img src='webFiles/images/botoes/lupa.gif' align='absmiddle' class='geralCursoHand' title='" + getMessage("prompt.filtrar",request) + "' ");
							objAuxiliar.append("onclick='filtrarCombo(\""+objName+"\")' />");
							
							objAuxiliar.append( "</div>");
							
							objAuxiliar.append( "<div id='divFiltro"+objName+"' style='display:none;'>");
							
							objAuxiliar.append( "<input type='text' id='txtFiltro" + objName + "' name='txtFiltro" + objName + "' style='width: "+sizeView+"px; float: left' class='principalObjForm' onkeydown=\"return txtFiltro_onKeyDown(event, '"+objName+"', '"+field.getFk().getKey()+"', '"+field.getFk().getDescription()+"');\"  />");
							objAuxiliar.append("<img src='webFiles/images/icones/check.gif' align='absmiddle' class='geralCursoHand' title='" + getMessage("prompt.pesquisar",request) + "' ");
							objAuxiliar.append("onclick=\"preencheCombo('"+objName+"', '"+field.getFk().getKey()+"', '"+field.getFk().getDescription()+"');\" />");
						
						}
						
						
						objAuxiliar.append( "</div>");
						
						
						
						objAuxiliar.append( "<script>document.forms[0]." + objName + ".value = '" + valor + "'</script>  ");	
						
						if(valor != null && !valor.equals("") && !objFunctions.equals("")){
							objAuxiliar.append( "<script>setTimeout(" + objFunctions + ",300)</script>  ");
						}
						
					}else{
						objAuxiliar.append( "<select name='excluir_" + objName + "' class='" + objClassname + "' scriptValidate=\"" + field.getScriptValidate("edit.jsp") + "\">");
						objAuxiliar.append( "	<option value=''>" + getMessage("prompt.selecione_uma_opcao",request) + "</option>");
						objAuxiliar.append( "</select>   ");
						objAuxiliar.append("<input type='hidden' name='" + objName + "' value='" + valor + "' />");
					}
				}		      	
			}else{
				
				if (view != null){
					objType = view.getObjectType();
					objClassname = view.getClassName();
					if (objType== null       || objType.equals(""))      objType      = "text";	
					if (objClassname == null || objClassname.equals("")) objClassname = "text";
					
					if(view != null && view.getSizeview() != null && !view.getSizeview().equalsIgnoreCase("")){
						sizeView = view.getSizeview();
					}
					
					//Se o objeto for do tipo combo					
					//------------------------------------------------------------------------------------------------
					if (objType!=null && objType.equalsIgnoreCase("combobox")){
						objAuxiliar.append( "<select style='width: "+sizeView+"px; float: left' name='" + objName + "' class='" + objClassname + "' scriptValidate=\"" + field.getScriptValidate("edit.jsp") + "\">");
						
						
						objAuxiliar.append( "	<option value=''>" + getMessage("prompt.selecione_uma_opcao",request) + "</option>");
						if (field.getDomains()!=null){
							for(int y = 0; y < field.getDomains().size(); y++){
								FieldDomain fieldDomain = (FieldDomain)field.getDomains().get(y);
								objAuxiliar.append( "  		<option value='" + fieldDomain.getValue() + "'>" + getMessage(fieldDomain.getValueDescription(),request) + "</option>");								
								fieldDomain = null;
							}
						}
						objAuxiliar.append( "</select>   ");
						objAuxiliar.append( "<script>document.forms[0]." + objName + ".value = '" + valor + "'</script>  ");
					//------------------------------------------------------------------------------------------------
					
					}else if (objType!=null && objType.equalsIgnoreCase("checkbox")){
						objOldType = objType;
						objType = "hidden";
						
						objAuxiliar.append( "<input type='checkbox' name='" + objName + "_checkbox' ");
						objAuxiliar.append( "onclick=\"if (this.checked){genericAdmForm." + field.getName() + ".value = '"+ field.getTrueValue() + "';}else{genericAdmForm." + field.getName() + ".value = '" + field.getFalseValue() +  "';}\"");
						objAuxiliar.append( "/>");
						
						objAuxiliar.append( "<script>");
						objAuxiliar.append( "if (genericAdmForm." + field.getName() + ".value == '" + field.getTrueValue() + "'){genericAdmForm." + objName + "_checkbox.checked = true;}");
						
						objAuxiliar.append( "document.forms[0]."  + objName + "_checkbox.onclick(); ");
						
						objAuxiliar.append( "</script>");
						
						
					}else if (objType!=null && objType.equalsIgnoreCase("editor")){
						objType = "hidden";
						//objAuxiliar.append( " <table><tr><td><div id='document.forms[0]."  + objName + "_div' style='position:relative; width:400; z-index:1; overflow: auto; height:" + objMaxlenght + "; background-color: #FFFFFF; border: 1px none #000000'> ");
						//objAuxiliar.append( valor );
						//objAuxiliar.append( "</div> <script>");
						////objAuxiliar.append( " document.write(\"" + valor + "\");");
						//objAuxiliar.append( " document.forms[0]." + objName + ".value = document.getElementById('document.forms[0]." + objName + "_div').innerHTML;");
						//valor="";
						//objAuxiliar.append( " </script> ");
						//objAuxiliar.append( "  </td>");				

						objAuxiliar.append( " <table><tr><td><div id='document.forms[0]."  + objName + "_div' style='position:relative; width:400; z-index:1; overflow: auto; height:" + objMaxlenght + "; background-color: #FFFFFF; border: 1px none #000000'> ");
						
						objAuxiliar.append("<input type='hidden' name='" + objName + "HD' value='" + valor + "'" +  "/>");

						objAuxiliar.append( "<script>");

						objAuxiliar.append( " document.write(document.forms[0]." + objName + "HD.value);");
						
						objAuxiliar.append( "</script>");
						
						//objAuxiliar.append( " document.write(\"" + valor + "\");");
						
						
						objAuxiliar.append( "</div> <script>");
						//objAuxiliar.append( " document.write(\"" + valor + "\");");
						objAuxiliar.append( " document.forms[0]." + objName + ".value = document.forms[0]." + objName + "HD.value");
						valor="";
						objAuxiliar.append( " </script> ");
						objAuxiliar.append( "  </td>");
						
						objAuxiliar.append( " <td><img src='webFiles/images/botoes/bt_CriarCarta.gif' name='imgCriarCarta' width='25' height='22' class='geralCursoHand' border='0' title='" + getMessage("prompt.editor",request) + "' onClick=\"MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=document.forms[0]."  + objName + "&carta=false&tamanho=3900','Documento','width=850,height=494,top=0,left=0')\" > </td></tr></table>");
			
					}else if(objType!=null && objType.equalsIgnoreCase("upload-icone")) {
						valor = "";
						
						objAuxiliar.append("<input type=\"hidden\" name=\"fieldUpload\" value=\""+objName+"\" />");
						objAuxiliar.append("<span id=\"preview-icone\" style=\"line-height: 20px; vertical-align: middle; \">");
						
						if(voRequest!=null && voRequest.getField(field.getName())!=null) {
							if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) {
								String url = "GenericAdmShowImage.do?entityName="+request.getParameter("entityName");
								for(int j = 0; j < entity.getPkFields().size(); j++) {
									Field f = (Field) entity.getPkFields().get(j);
									url += "&" + f.getName().toLowerCase() + "=" + voRequest.getFieldAsString(f.getName());
								}
								
								objAuxiliar.append("<img src=\""+url+"\" style=\"height: 16px; width: 16px; \" />");
							} else {
								String base64 = new String(org.apache.commons.codec.binary.Base64.encodeBase64((byte[]) voRequest.getField(field.getName())));
								
								objAuxiliar.append("<img src=\"data:image/png;base64,"+base64+"\" style=\"height: 16px; \" />");
							}
						}
						objAuxiliar.append("</span>");
						
						objType="file";
						objStyle = "width: 290px;";
						valor = "";
						objName = "fileUpload";
					}
				}

				if (objType== null       || objType.equals(""))      objType      = "text";	
				if (objClassname == null || objClassname.equals("")) objClassname = "text";
			}
			
			objSize = field.getSizeview();
			if (field.isPk() && field.getType().equalsIgnoreCase("integer")){
				objDisabled = "disabled";
				
			%>	<script>pkValue += "<%= valor +";"%>";</script> <%
			}else{
				objDisabled = "";
			}
			
						
		%>				
			<tr> 
		<%
			if(!objType.equalsIgnoreCase("hidden") || field.getName().indexOf("dh_inativo") > -1 || !objType.equals("")) {
		%>
			  <td width="25%" align="right" class="principalLabel"><%= getMessage(field.getLabel(), request)%><!-- ## --> 
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		<%
			} else {
		%>
		 	  <td width="25%" align="right" class="principalLabel">&nbsp;</td>
		<%
			} 
		%>
			<td
			  <%
			  if (field.isPk() && !field.hasFk() && field.getType().equalsIgnoreCase("integer"))	{
			  %>	 	
			  	colspan="1" width="15%"
			  <%} else{%>
			  	colspan="2" 
			  <%}%>
			  	> 
			  		
			  	<%if(!field.hasFk() && !"combobox".equalsIgnoreCase(objType) ){ 
			  			if(!objType.equalsIgnoreCase("textarea")) {
			  				boolean isDatePicker=false;
			  				
			  				if(objType.equalsIgnoreCase("datepicker")) { 
			  					isDatePicker=true;
			  					objType = "text";
			  					objStyle = "width: 70px;";
			  					objFunctions += "onkeydown=\"return validaDigito(this, event)\" onblur=\"return verificaData(this); \" ";
			  				} 
			  				
			  				if (field.isPk() && !field.hasFk() && field.getType().equalsIgnoreCase("integer") && "".equals(objStyle)) 	{
			  					objStyle = "width: 70px;";
			  				}
			  				
			  				if ((objType==null || "text".equalsIgnoreCase(objType)) && "".equals(objStyle)) objStyle = "width: 315px; ";
			  				%> 
			  				
						  	<input type="<%= objType%>" 
						  		   name="<%= objName%>" 
						  		   class="<%= objClassname%>" 
						  		   maxlength="<%= objMaxlenght%>" 
						  		   size="<%= objSize%>" 
						  		   pk="<%= field.isPk()%>"
							  	   allowNull=<%= String.valueOf( field.getAllowNull())%>
						  		   defaultValue=<%= field.getDefaultValue()%>
						  		   field="true"
						  		   valorRequest="<%= valor%>"
						  		   value="<%= valor%>"
							  	   scriptValidate="<%=field.getScriptValidate("edit.jsp") %>"
						  		   style="<%= objStyle%>"
						  		   <%= objDisabled%> 
						  		   <%= checked %>
						  		   <%= objFunctions %>
						  		   
						  	/>
						  	<% if(isDatePicker) { %>
						  	<img id="calendar<%= objName%>" src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" 
						  		onClick="show_calendar('genericAdmForm.<%= objName%>')" 
						  		title="<bean:message key="prompt.calendario" />">
						  	<% } %>
						  	
						  <%}else{ 
							  int rows = view.getLen();
							  
							  if(rows == 0){
								  rows = objMaxlenght;
							  }
						  %>
						  		<textarea 
							 	   name="<%= objName%>" 
						  		   class="<%= objClassname%>" 
						  		   maxlength="<%= objMaxlenght%>"
						  		   rows="<%= rows%>"  
						  		   size="<%= objSize%>" 
						  		   pk="<%= field.isPk()%>"
							  	   allowNull=<%= String.valueOf( field.getAllowNull())%>
						  		   defaultValue=<%= field.getDefaultValue()%>
						  		   field="true"
						  		   valorRequest="<%= valor%>"			  		   
							  	   scriptValidate="<%=field.getScriptValidate("edit.jsp") %>"							  	   
							  	   <%= objDisabled%> 
							  	   <%= checked %>
							  	   <%= objFunctions %>
							  	><%= valor%></textarea>
						  <%} %>
			  				 	
				 <%} %>
			  	
			  	<% TagUtils.getInstance().write(pageContext,objAuxiliar.toString()); %>
			  </td>
			  <td width="30%"
			  <%
			  if (field.isPk() && !field.hasFk())	{
			  %>	 	
			  	colspan="1">&nbsp;</td>
			  	<td
			  <%} else{%>
			  	colspan="1"
			  <%}%>
			  >&nbsp;</td>
			</tr>
		<%	
		}
		%>
		

      </table>

		<%
		Vector listByViewList = entity.getFieldByView("list.jsp");		
		if(listByViewList.size() > 0){
		%>

		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-top:10px;">
			<tr>		   
			    <td class="principalLstCab" width="5%"></td>
			    
			    
			<%
			
				Vector listaLabel = entity.getFieldByOrder();		
				//percorre todos os fields da entidade
				for (int i = 0; i < listaLabel.size(); i++){
					Field field = (Field)listaLabel.get(i);
					View view = field.getViewByName("list.jsp");
					
					//Caso o campo o nao seja um campo fisico ou esteja configurado na view que o atributo nao pode ser visualizado deve-se pular esse campo
					if (view != null && !view.getVisible()){continue;}
					
			%>				
					<td class="principalLstCab" width="<%= field.getSizeview()%>"><%= getMessage(field.getLabel(), request)%></td>	
			<%	
					field = null;
					view = null;
				}								
				listaLabel = null;									
			%>
			</tr>
		</table>
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <tr> 
		    <td height="210" valign="top" width="700px"> 
		    	<iframe id="list" name="list" src="" width="100%" height="210px" frameborder="0" marginwidth="0" marginheight="0" scrolling="auto"></iframe>
		    </td>
		  </tr>  
		</table>
		<%} %>
</html:form>
</body>
</html>

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>

<script>
var $a = jQuery.noConflict()
<!-- 91032 - 20/09/2013 - Jaider Alba -->
<%
Permission permission = (Permission)entity.getPermissions().get("inclusao");
String inclusao = permission.getKey();
permission = (Permission)entity.getPermissions().get("alteracao");
String alteracao = permission.getKey();	
%>

if ( (genericAdmForm.acao.value == '<%=Constantes.ACAO_INCLUIR %>' && parent.document.getElementById("imgIncluir").disabled) ||
     (genericAdmForm.acao.value == '<%=Constantes.ACAO_EDITAR %>' && '<%= alteracao%>'!='' && !getPermissao('<%= alteracao%>') ) 
   )
{
	for (x = 0;  x < genericAdmForm.elements.length;  x++) {
		Campo = genericAdmForm.elements[x];
		if(Campo.pk!="true" && Campo.type != 'hidden')
		{
			Campo.disabled = true;
            //Codigo acrescentado, porque em alguns browsers o comando acima de desabilitar n�o funciona
			$a(Campo).attr("disabled", true);
			$a(Campo).attr("readonly", true);
		}
	}
}
else
{
	if(genericAdmForm.acao.value == '<%=Constantes.ACAO_EDITAR %>')
	{
		for (x = 0;  x < genericAdmForm.elements.length;  x++) {
			Campo = genericAdmForm.elements[x];
			if(Campo.pk!="true" && Campo.type != 'hidden')
			{
				Campo.disabled = false;
				//Codigo acrescentado, porque em alguns browsers o comando acima de desabilitar n�o funciona
				$a(Campo).attr("disabled", false);
				$a(Campo).attr("readonly", false);
			}
		}
	}		
}

//Codigo acrescentado, porque em alguns browsers o comando acima de desabilitar n�o funciona
for (x = 0;  x < genericAdmForm.elements.length;  x++) {	
	Campo = genericAdmForm.elements[x];
	if(Campo.pk=="true" || $a(Campo).attr("pk")=="true")
	{
		Campo.disabled = true;

		$a(Campo).attr("disabled", true);
		$a(Campo).attr("readonly", true);		
	}
}

</script>