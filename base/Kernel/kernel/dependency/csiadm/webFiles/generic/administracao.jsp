<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="br.com.plusoft.fw.entity.*"%>

<%@ page import="java.util.Vector"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

Entity entity = null;
entity = EntityInstance.getEntity(request.getParameter("entityName"));
%>

<% 
String fileIncludeIdioma="/webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<%@page import="br.com.plusoft.fw.webapp.RequestHeaderHelper"%><html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
</script>
<script>var inicioLixeira="";</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');resizeContentDiv();" onresize="resizeContentDiv();">
<html:form styleId="genericAdmForm" action="/GenericAdm.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="entityName"/>
	<html:hidden property="entityEmpresaName"/>
	<html:hidden property="entityIdiomaName"/>
	<html:hidden property="temMultiempresa"/>
	<html:hidden property="temMultiIdioma"/>

<script>var possuiRegistros=false;</script>
<!--Chamado: 98441 - 02/01/2015 - Leonardo M. Facchini-->
<div style="width: 100%;overflow: auto;height: 100%;">
<div id="contentdiv" style="float: left;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="sortable">
 
		<%	   	
		Vector listaFields = entity.getFieldByOrder();
		
		Vector listaFieldsPK = entity.getPkFields();
		
		//Obtem todos os registros do banco de dados
		Vector vectorBean = (Vector)request.getAttribute("vetorBean");
		
		String pkName = "";
		String pkValue = "";
		String camposHidden = "";
		
		Vector listByViewList = entity.getFieldByView("list.jsp");	
		boolean temLista = false;
		if(listByViewList.size() > 0){
			temLista = true;
		}
		
		//Percorre todos os registros retornados pelo banco de dados
		if (vectorBean!=null){
			for (int loopVetorBean = 0; loopVetorBean < vectorBean.size(); loopVetorBean++){
				Vo vo = (Vo)vectorBean.get(loopVetorBean);	
				
				pkName = "";
				pkValue = "";
				camposHidden ="";
				
				//Os campos como pk para executar o dubmit quando o usuario clicar sobre o item.				
				StringBuffer javascriptPk = new  StringBuffer();
				javascriptPk.append("parent.setPK(");
				for (int loopPk = 0; loopPk < listaFieldsPK.size(); loopPk++){
					Field fieldPK = (Field)listaFieldsPK.get(loopPk);					
					String value = vo.getFieldAsString(fieldPK.getName());					
					
					pkName += fieldPK.getName() +";";
					pkValue += value +";";
					camposHidden += "<input type=\'hidden\' name=\'" + fieldPK.getName() + "\'></input>";
					
					if (loopPk!=0)
						javascriptPk.append(", ");	
					javascriptPk.append("'" + fieldPK.getName() + "','" + value + "'");
					fieldPK = null;
				}
				javascriptPk.append(");");
							
				
			%>	
				<tr>
				<script>possuiRegistros=true;</script>
			    <td width="3%" class="principalLstPar" valign="middle">
			    	<img src="webFiles/images/botoes/lixeira.gif" <%if(temLista){%> style="display:none;" <%}%> name="lixeira" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();<%=javascriptPk%>;parent.submit_remove()">&nbsp;
			    </td>
			
			<% if(request.getParameter("entityIdiomaName") != null && !"".equals(request.getParameter("entityIdiomaName"))){ %>
			    <td width="3%" class="principalLstPar">
			    	<!-- Globo de multiidioma -->
			    	<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('<%=request.getParameter("entityIdiomaName") %>', '<%= pkValue%>')">
			    </td>
			<% } else { %>
				<td width="3%" class="principalLstPar">&nbsp;</td>
			<% } %>
				
			<%
				//Percorre todos os campos disponiveis para esse pagina
				for (int loopFields = 0; loopFields < listaFields.size(); loopFields++){
					Field field = (Field)listaFields.get(loopFields);
					String objectType = "text";
					String sizeView = null;
					
					View view = field.getViewByName("administracao.jsp");
					
					//Caso o campo o nao seja um campo fisico ou esteja configurado na view que o atributo nao pode ser visualizado deve-se pular esse campo
					if (view != null && !view.getVisible()) {continue;}
					
					if (view !=null && view.getObjectType()!=null) 
						objectType = view.getObjectType();
					
					String valor = String.valueOf( vo.getFieldByFormat(field.getName(), field.getFormat("administracao.jsp")));
					//String valor = vo.getFieldAsString(field.getName());
					if (valor == null || valor.equals("") || valor.equalsIgnoreCase("null")){
						valor = "&nbsp;";
					}
					%>
						<td class="principalLstParMao" width="<%= field.getSizeview()%>" onclick="parent.clearError();<%=javascriptPk%>;parent.submit_edit()">
						 <%
						 if(objectType.equals("thumbnail")) {
							 if(vo.getField(field.getName())!=null) {
							 
								if(!RequestHeaderHelper.isW3CBrowser(request)) {
									String url = "GenericAdmShowImage.do?entityName="+request.getParameter("entityName");
									for(int j = 0; j < entity.getPkFields().size(); j++) {
										Field f = (Field) entity.getPkFields().get(j);
										url += "&" + f.getName().toLowerCase() + "=" + vo.getFieldAsString(f.getName());
									}
									
									out.println("<img src=\""+url+"\" style=\"height: 16px; \" />");
								} else {
									String base64 = new String(org.apache.commons.codec.binary.Base64.encodeBase64((byte[]) vo.getField(field.getName())));
									
									out.println("<img src=\"data:"+ br.com.plusoft.fw.util.BinaryDataHelper.getBinaryImageMimetype((byte[]) vo.getField(field.getName()))  +";base64,"+base64+"\" style=\"height: 16px; \" />");
								}
							
							 } else {
								 out.println("&nbsp;");
							 }
							 
							 
						 } else {
						  	if(field.getAcronymlen() > 0){
						  		out.println(acronymChar(valor,field.getAcronymlen()));
						  	}else{
						  		out.println(valor);
						  	}
						 }
						 %>

						</td>
					<%
					field = null;
					view = null;
				}
				vo = null;
			%>
				<td class="principalLstParMao" width="3%">&nbsp;</td>
				</tr>
			<%
			}
		}
		vectorBean = null;
		listaFields = null;		
		%>				
		
		<%=camposHidden%>
		
		<tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
</div>
</div>
<div id="divErro"  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
//Chamado: 98441 - 02/01/2015 - Leonardo M. Facchini
function resizeContentDiv(){
	var parentNodeOffsetWidth = document.getElementById('contentdiv').parentNode.offsetWidth;
	document.getElementById('contentdiv').style.width = parentNodeOffsetWidth - (parentNodeOffsetWidth * 0.03);
	document.getElementById('contentdiv').parentNode.style.height = document.getElementById('contentdiv').parentNode.parentNode.parentNode.offsetHeight;
}
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}	
	if('<%= ((Permission)entity.getPermissions().get("exclusao")).getKey()%>'!='')
		setPermissaoImageDisable('<%= ((Permission)entity.getPermissions().get("exclusao")).getKey()%>', genericAdmForm.lixeira);	


</script>
</html:form>
</body>
</html>