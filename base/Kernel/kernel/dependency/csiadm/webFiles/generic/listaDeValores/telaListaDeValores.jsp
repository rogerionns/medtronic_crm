<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>-- CRM -- Plusoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
 
function busca(){
	document.forms[0].target = popupLstRazaoSocial.name = 'teste';
	document.forms[0].action = 'FiltrarListaDeValores.do';
	document.forms[0].submit();
}
</script>
</head>

<body class="principalBgrPage" text="#000000" scroll="no" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="AbrirListaDeValores">

<html:hidden property="entityName"/>
<html:hidden property="idEmpresa"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
		    <td width="1007" colspan="2"> 
		      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tr> 
		          <td class="principalPstQuadro" height="17" width="166"><bean:message name="entity" property="label"/></td>
		          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
		          <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		        </tr>
		      </table>
		    </td>
		  </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>      
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left: 10px">
        <tr>   
          <bean:define id="fields" name="entity" property="fieldByOrder"/>
          <logic:iterate name="fields" id="field">
          	<logic:present name="field" property="viewByName(lovfiltros)">
          		<bean:define id="view" name="field" property="viewByName(lovfiltros)"/>
          		<logic:equal value="true" name="view" property="filter">
          			<td class="principalLabel" height="17" width="<bean:write name="view" property="sizeview"/>">
          				<bean:message name="field" property="label"/>
          			</td>
          		</logic:equal>          		
          	</logic:present>
          </logic:iterate>	
          <td width="10%">&nbsp;</td>
        </tr>          
        
        <tr>   
          <logic:iterate name="fields" id="field">
          	<logic:present name="field" property="viewByName(lovfiltros)">
          		<bean:define id="view" name="field" property="viewByName(lovfiltros)"/>
          		<logic:equal value="true" name="view" property="filter">
          			<td width="<bean:write name="view" property="sizeview"/>">
			         <input type="text" name="<bean:write name="field" property="name"/>" class="principalObjForm" style="width: 100%" onkeydown="if(event.keyCode==13)busca();" maxlength="<bean:write name="field" property="len"/>"/>
			        </td>          			
          		</logic:equal>          		
          	</logic:present>
          </logic:iterate>	
               
          
          <td width="10%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" title="<bean:message key='prompt.pesquisar'/>" onclick="busca()"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td valign="top" align="center"> 
           <iframe name="popupLstRazaoSocial" src="FiltrarListaDeValores.do?entityName=<bean:write name="listaDeValoresForm" property="entityName"/>&idEmpresa=<bean:write name="listaDeValoresForm" property="idEmpresa"/>" width="100%" scrolling="No" height="250" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="30" border="0" cellspacing="0" cellpadding="0" align="right">
  <tr> 
    <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()"></td>
  </tr>
</table>
</html:form>
</body>
</html>
