<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.form.GenericAdmForm"%>
<%@ page import="br.com.plusoft.fw.entity.*"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.Vector"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@page import="java.util.Iterator"%>
<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
 	function habilitaCampos(disabled){
		for ( var i=0; i< document.forms[0].elements.length; i++){
			var e = document.forms[0].elements[i];
			if (e.field == "true"){
				e.disabled = disabled;
			}
		}
	}	
	
	function onLoad(){
		showError('<%=request.getAttribute("msgerro")%>');
		parent.setFocusFirstObj();
		
	}
</script>

<body class= "principalBgrPageIFRM" onload="onLoad();">

<html:form styleId="genericAdmForm" action="/GenericAdm.do">

	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="CDataInativo"/>	
	<html:hidden property="entityName"/>	
	
	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
		<%
	   	Entity entity = null;
		entity = EntityInstance.getEntity(request.getParameter("entityName"));
		
//		Enumeration lista = entity.getFields().keys();
		Vector lista = entity.getFieldByOrder();
		GenericAdmForm genericAdmForm = (GenericAdmForm)request.getAttribute("genericAdmForm");
		Vo voRequest = genericAdmForm.getVo();		
		
		String objName = "";
		String objType = "";
		int objMaxlenght = 0;
		String objSize = "0";
		String objDisabled = "";
		String objClassname = "";
		String valor = "";
		String onClick = "";
		String checked = "";

		StringBuffer objAuxiliar = null;
				
		//percorre todos os fields da entidade
		for (int i = 0; i < lista.size(); i++){
//		while(lista.hasMoreElements()){
			//String key = (String)lista.nextElement();
			//Field field = (Field)entity.getFields().get(key);
			Field field = (Field)lista.get(i);
			objName = field.getName();
			View view = field.getViewByName("edit.jsp");

			
			//Caso o campo o nao seja um campo fisico ou esteja configurado na view que o atributo nao pode ser visualizado deve-se pular esse campo
			if ((view != null && !view.getVisible())){continue;}
			
			boolean fieldInativo = false;
			
			valor = "";
			if (voRequest!=null){
				valor = voRequest.getFieldAsString(objName);
			}
			
			objAuxiliar = new StringBuffer("");
			
			if (field.getName().indexOf("dh_inativo") != -1){
				if (voRequest!=null){
					valor = String.valueOf( voRequest.getFieldByFormat(field.getName(), "DD/MM/YYYY HH:MI:SS"));
				}
				if (valor==null || valor.equals("null")){
					valor = "";
				}
				
				objType = "hidden";	
				objClassname = "";
				fieldInativo = true;
				
				//Coloca a constante de data atual
				if (valor==null || valor.equalsIgnoreCase("")){
					//valor = "#SYSDATE#";
				}
				
				objAuxiliar.append( "<input type='checkbox' name='" + objName + "_checkbox' ");
				objAuxiliar.append( "onclick=\"if (this.checked){genericAdmForm." + field.getName() + ".value = '"+ valor + "';}else{genericAdmForm." + field.getName() + ".value = '';}\"");
				objAuxiliar.append( "/>");
				
				objAuxiliar.append( "<script>");
				objAuxiliar.append( "if (genericAdmForm." + field.getName() + ".value != ''){genericAdmForm." + objName + "_checkbox.checked = true;}");
				
				objAuxiliar.append( "</script>");
			}else if(field.hasFk()){
				
				if(view != null && view.isIframe()){
					objAuxiliar.append("<input type='hidden' name='" + objName + "' value='" + valor + "' />");	
					objAuxiliar.append("<iframe name='" + objName + "_iframe' id='" + objName + "_iframe' src='GenericAdm.do?acao=incluir&tela=combo&field=" + objName +"&entityName="+ request.getParameter("entityName") + "' width='100%' height='20px' scrolling='No' frameborder='0' marginwidth='0' marginheight='0' ></iframe>");
					
					if(!valor.equals("")){
						objAuxiliar.append( "<script>");
						objAuxiliar.append(" function carregaCombo" + objName + "() {");					
						objAuxiliar.append(objName + "_iframe.genericAdmForm." + objName + ".value = " + valor + "");
						objAuxiliar.append(" }");					
						objAuxiliar.append( "</script>  ");					
					}
				}else{
					objAuxiliar.append( "<select name='" + objName + "' class='" + objClassname + "' scriptValidate=\"" + field.getScriptValidate("edit.jsp") + "\">");
					objAuxiliar.append( "	<option value=''>" + getMessage("prompt.selecione_uma_opcao",request) + "</option>");
					if(request.getAttribute(field.getFk().getEntity().getName().toLowerCase())!=null){
						for(Iterator it = ((Vector)request.getAttribute(field.getFk().getEntity().getName().toLowerCase())).iterator() ; it.hasNext();){
							Vo vo = (Vo)it.next();
							objAuxiliar.append( "  		<option value='" + vo.getFieldAsString(field.getFk().getKey()) + "'>" + vo.getFieldAsString(field.getFk().getDescription()) + "</option>");
						}		
					}
					objAuxiliar.append( "</select>   ");
					objAuxiliar.append( "<script>document.forms[0]." + objName + ".value = '" + valor + "'</script>  ");
				}		      	
			}else{
				
				if (view != null){
					objType = view.getObjectType();
					objClassname = view.getClassName();
					if (objType== null       || objType.equals(""))      objType      = "text";	
					if (objClassname == null || objClassname.equals("")) objClassname = "text";
					
					//Se o objeto for do tipo combo					
					//------------------------------------------------------------------------------------------------
					if (objType!=null && objType.equalsIgnoreCase("combobox")){
						objAuxiliar.append( "<select name='" + objName + "' class='" + objClassname + "' scriptValidate=\"" + field.getScriptValidate("edit.jsp") + "\">");
						
						
						objAuxiliar.append( "	<option value=''>" + getMessage("prompt.selecione_uma_opcao",request) + "</option>");
						if (field.getDomains()!=null){
							for(int y = 0; y < field.getDomains().size(); y++){
								FieldDomain fieldDomain = (FieldDomain)field.getDomains().get(y);
								objAuxiliar.append( "  		<option value='" + fieldDomain.getValue() + "'>" + getMessage(fieldDomain.getValueDescription(),request) + "</option>");								
								fieldDomain = null;
							}
						}
						objAuxiliar.append( "</select>   ");
						objAuxiliar.append( "<script>document.forms[0]." + objName + ".value = '" + valor + "'</script>  ");
					//------------------------------------------------------------------------------------------------
					
					}else if (objType!=null && objType.equalsIgnoreCase("checkbox")){
						objType = "hidden";
						
						objAuxiliar.append( "<input type='checkbox' name='" + objName + "_checkbox' ");
						objAuxiliar.append( "onclick=\"if (this.checked){genericAdmForm." + field.getName() + ".value = '"+ field.getTrueValue() + "';}else{genericAdmForm." + field.getName() + ".value = '" + field.getFalseValue() +  "';}\"");
						objAuxiliar.append( "/>");
						
						objAuxiliar.append( "<script>");
						objAuxiliar.append( "if (genericAdmForm." + field.getName() + ".value == '" + field.getTrueValue() + "'){genericAdmForm." + objName + "_checkbox.checked = true;}");
						
						objAuxiliar.append( "document.forms[0]."  + objName + "_checkbox.onclick(); ");
						
						objAuxiliar.append( "</script>");
						
						
					}else if(objType!=null && objType.equalsIgnoreCase("upload-icone")) {
						objType="file";
						//objAuxiliar.append( "<input type=\"file\" name=\"" + objName + "_checkbox\" ");
						
						
					}
						
					}
				}

				if (objType== null       || objType.equals(""))      objType      = "text";	
				if (objClassname == null || objClassname.equals("")) objClassname = "text";
			}
			
			objMaxlenght = field.getLen();
			objSize = field.getSizeview();
			if (field.isPk()){
				objDisabled = "disabled";
			}else{
				objDisabled = "";
			}
			
						
		%>				
			<tr> 
			  <td width="17%" align="right" class="principalLabel"><%= getMessage(field.getLabel(), request)%><!-- ## --> 
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
			  <td 		
			  
			  <%
			  if (field.isPk())	{
			  %>	 	
			  	colspan="1" width="15%"
			  <%} else{%>
			  	colspan="2" 
			  <%}%>
			  	> 
			  		
			  	<%if(!field.hasFk()){ %>
			  
				  	<input type="<%= objType%>" 
				  		   name="<%= objName%>" 
				  		   class="<%= objClassname%>" 
				  		   maxlength="<%= objMaxlenght%>" 
				  		   size="<%= objSize%>" 
				  		   pk="<%= field.isPk()%>"
					  	   allowNull=<%= String.valueOf( field.getAllowNull())%>
				  		   defaultValue=<%= field.getDefaultValue()%>
				  		   field="true"
				  		   valorRequest="<%= valor%>"
				  		   value="<%= valor%>"
					  	   scriptValidate="<%=field.getScriptValidate("edit.jsp") %>"
				  		   <%= objDisabled%> 
				  		   <%= checked %>
				  	/>
			  	
				 <%} %>
			  	
			  	<%= objAuxiliar.toString()%>
			  </td>
			  <td width="30%"
			  <%
			  if (field.isPk())	{
			  %>	 	
			  	colspan="1">&nbsp</td>
			  	<td
			  <%} else{%>
			  	colspan="1"
			  <%}%>
			  >&nbsp;</td>
			</tr>
		<%	
		}
		%>
		

      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			habilitaCampos(true);
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.confirm_remove(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			/*
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_INCLUSAO_CHAVE%>', parent.genericAdmForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_INCLUSAO_CHAVE%>')){
				habilitaCampos(true);
			}
			*/		
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			/*
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_ALTERACAO_CHAVE%>', parent.genericAdmForm.imgGravar);	
				habilitaCampos(true);
			}
			*/
		</script>
</logic:equal>
</html>
<script>
	//try{genericAdmForm.areaDsArea.focus();}
	//catch(e){}	
</script>