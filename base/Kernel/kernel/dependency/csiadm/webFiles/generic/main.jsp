<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.entity.*"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.lang.String"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<%@ include file = "/webFiles/includes/multiempresa.jsp" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String entityIdioma = request.getParameter("entityIdiomaName");
String entityEmpresa = request.getParameter("entityEmpresaName");

if(entityIdioma == null) entityIdioma = "";
if(entityEmpresa == null) entityEmpresa = "";
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

Entity entity = null;
entity = EntityInstance.getEntity(request.getParameter("entityName"), empresaVo.getIdEmprCdEmpresa());
String newTela = new String("");
int tamanhoCampoDesc = 0;

if(request.getParameter("telaEdit") != null){
	newTela = (String)request.getParameter("telaEdit").toString();
}

if(newTela == null || newTela.equals("")){
	newTela = "edit";
}

%>

<% String fileInclude="/webFiles/includes/multiempresa.jsp"; %>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% String fileIncludeIdioma="/webFiles/includes/idioma.jsp"; %>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>
<title>Plusoft CRM - Cadastro</title>
<!--main.jsp-->
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script type="text/javascript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script type="text/javascript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script type="text/javascript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script type="text/javascript" src="webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript" src="webFiles/funcoes/sorttable.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script type="text/javascript">
telaEdit = '<%=newTela%>';
var tamanhoCampoDesc = 0;
function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){	
	error.innerHTML =  '';
}

function filtrar(){
	
	genericAdmForm.target = admIframe.name;
	genericAdmForm.acao.value ='filtrar';
	genericAdmForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	genericAdmForm.filtro.value = '';
}

function submeteFormIncluir() {
	var editIframeAux = document.getElementById('editIframe');
	editIframeAux = (editIframeAux.contentWindow || editIframeAux.contentDocument);
	
	editIframeAux.document.forms[0].target = editIframeAux.name;
	editIframeAux.document.forms[0].tela.value = '<%=newTela%>';
	editIframeAux.document.forms[0].acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	
	editIframeAux.document.forms[0].submit();
	AtivarPasta('editIframe');
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

/*
<!--
function submeteFormEdit(){
	var arg = submeteFormEdit.arguments;
	for (var i = 0;  i < arg.length; i++){
		tab.document.forms[0].
		alert(arg[i]);
	}

	tab.document.forms[0].idAreaCdArea.value = codigo;
	tab.document.forms[0].tela.value = '<%=newTela%>';
	tab.document.forms[0].target = editIframe.name;
	tab.document.forms[0].acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.forms[0].submit();
	AtivarPasta('editIframe');
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}
-->
*/

/*
Este metodo tem como objetivo setar os valores da pks nos respectivos campos para 
a execucao do seu submit
Ex: setPK('id_area_cd_area', '1', 'area_ds_area', 'nome da area', 'area_dh_inativo', '01/01/2000');
@author Henrique Pinheiro
@date 05/07/2006
*/
function setPK(){		
	var args = setPK.arguments;
	for (var i = 0;  i < args.length; i++){
		if ((i % 2) != 0){						
			var obj = eval("tab.document.forms[0]." + args[i-1]);
			obj.value = args[i];
			
			// Inclu�do para que possa editar um item que n�o est� na combo (por estar inativo)
			if(obj.value != args[i] && obj.options!=undefined) {
				addOptionComboItem(obj, args[i], 'Aguarde', null);
				obj.value = args[i];	
			}

					
			disableEnable(obj, false); //habilita os campos para serem enviados para o servidor
			obj = null;	
		}
	}
}

/*
Este metodo tem como objetivo enviar as informacoes de edicao para o servidor de app
normalmente esse metodo deve ser chamado logo apos o metodo setPK()
@author Henrique Pinheiro
@date 24/07/2006
*/
function submit_edit(){

	tab.document.forms[0].tela.value = '<%=newTela%>';
	tab.document.forms[0].target = editIframe.name;
	tab.document.forms[0].acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.forms[0].submit();

	//editIframe.genericAdmForm.tela.value = '<%=newTela%>';
	//editIframe.genericAdmForm.target = editIframe.name;
	//editIframe.genericAdmForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';	
	//editIframe.genericAdmForm.submit();
	AtivarPasta('editIframe');
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submit_editList(){
	editIframe.genericAdmForm.tela.value = '<%=newTela%>';
	editIframe.genericAdmForm.target = editIframe.name;
	editIframe.genericAdmForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	editIframe.genericAdmForm.submit();
	editIframe.genericAdmForm.acao.value = '<%=Constantes.ACAO_INCLUIR %>';	
	AtivarPasta('editIframe');
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

/*
Este metodo tem como objetivo enviar as informacoes de edicao para o servidor de app
normalmente esse metodo deve ser chamado logo apos o metodo setPK()
@author Henrique Pinheiro
@date 24/07/2006
*/
function submit_remove(codigo) {
	//editIframe.genericAdmForm.tela.value = '<%=newTela%>';
	//editIframe.genericAdmForm.target = editIframe.name;
	//editIframe.genericAdmForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	//editIframe.genericAdmForm.submit();
	
	tab.document.forms[0].tela.value = '<%=newTela%>';
	tab.document.forms[0].target = editIframe.name;
	tab.document.forms[0].acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.forms[0].submit();
	
	AtivarPasta('editIframe');
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function enable_disable_obj(disabled){
	var editIframeAux = document.getElementById('editIframe');
	editIframeAux = (editIframeAux.contentWindow || editIframeAux.contentDocument);
	
	for ( var i=0; i< editIframeAux.document.forms[0].elements.length; i++){
		var e = editIframeAux.document.forms[0].elements[i];
		try{
		e.setAttribute("disabledAnterior", e.disabled);
		e.disabled = disabled;
		}catch(e){}
	}
}

function set_disable_anterior(){
	var editIframeAux = document.getElementById('editIframe');
	editIframeAux = (editIframeAux.contentWindow || editIframeAux.contentDocument);
	
	for ( var i=0; i< editIframeAux.document.forms[0].elements.length; i++){
		var e = editIframeAux.document.forms[0].elements[i];
		try{
			e.disabled = e.getAttribute("disabledAnterior");
		}catch(e){}
	}
}

function confirm_remove(confirmacao){
	var editIframeAux = document.getElementById('editIframe');
	editIframeAux = (editIframeAux.contentWindow || editIframeAux.contentDocument);
	
	if (confirmacao == true){		
		editIframeAux.document.forms[0].tela.value = 'administracao';
		editIframeAux.document.forms[0].target = admIframe.name;
		editIframeAux.document.forms[0].acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		enable_disable_obj(false);
		editIframeAux.document.forms[0].submit();
		enable_disable_obj(true);
		
		if(editIframe.list != undefined){
			enable_disable_obj(false);
		}else{
			AtivarPasta('admIframe');
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			editIframe.location = 'GenericAdm.do?tela=<%=newTela%>&acao=incluir&entityName=' + genericAdmForm.entityName.value + '&entityEmpresaName=' + genericAdmForm.entityEmpresaName.value + '&entityIdiomaName=' + genericAdmForm.entityIdiomaName.value + '&temMultiempresa=' + genericAdmForm.temMultiempresa.value + '&temMultiIdioma=' + genericAdmForm.temMultiIdioma.value;
		}
	}else{
		cancel();	
	}
}

function cancel(){
	editIframe.location = 'GenericAdm.do?tela=<%=newTela%>&acao=incluir&entityName=' + genericAdmForm.entityName.value + '&entityEmpresaName=' + genericAdmForm.entityEmpresaName.value + '&entityIdiomaName=' + genericAdmForm.entityIdiomaName.value + '&temMultiempresa=' + genericAdmForm.temMultiempresa.value + '&temMultiIdioma=' + genericAdmForm.temMultiIdioma.value;
	AtivarPasta('admIframe');
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


/*
@author Henrique Pinheiro
@date 14/08/2006

Executa a validacao de gravacao do registro
*/
function submeteSalvar(){
	if(tab.document.forms[0].tela.value == 'administracao'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.forms[0].tela.value == '<%=newTela%>'){
		var bGravar = false;

		bGravar = validateFields();

		if (bGravar && (genericAdmForm.entityEmpresaName.value!=null) && (genericAdmForm.entityEmpresaName.value!='')) {
			bGravar = podeGravarAssociacao();
		}

		if (bGravar){

			enable_disable_obj(false);
			
			tab.document.forms[0].tela.value = 'administracao';
			tab.document.forms[0].target = admIframe.name;
		
			tab.document.forms[0].submit();
			
			if(editIframe.list != undefined){
				if(editIframe.list != undefined){
					editIframe.list.document.forms[0].tela.value = 'list';
					editIframe.list.document.forms[0].acao.value = 'consultar';
					editIframe.list.document.forms[0].entityEmpresaName.value = document.forms[0].entityEmpresaName.value;
					editIframe.list.document.forms[0].temMultiempresa.value = document.forms[0].temMultiempresa.value;
					editIframe.list.document.forms[0].temMultiIdioma.value = document.forms[0].temMultiIdioma.value;
					editIframe.list.document.forms[0].entityIdiomaName.value = document.forms[0].entityIdiomaName.value;
					setTimeout('editIframe.list.document.forms[0].submit();', 1500);
					
				}
			}else
				cancel();
				
		}else{
			//set_disable_anterior();
			setFocusFirstObj();
		}
	}
}


/*
@author Henrique Pinheiro
@date 14/08/2006

Percorre todos os componentes da tela e seta o focus no primeiro objeto habilitado
*/
function setFocusFirstObj(){
	var edit = document.getElementById("editIframe");
	edit = (edit.contentWindow || edit.contentDocument);

	for (var i = 0; i < edit.document.forms[0].length; i++) { 
		if (edit.document.forms[0].elements[i].type != 'hidden' && !edit.document.forms[0].elements[i].disabled){
			try{
				edit.document.forms[0].elements[i].focus();
			}catch(e){}
			break;
		}
	}
}

/*
@author Henrique Pinheiro
@date 14/08/2006

Percorre todos os campos da tela e executao o script de validacao
*/
function validateFields(){
	var editIframeAux = document.getElementById('editIframe');
	editIframeAux = (editIframeAux.contentWindow || editIframeAux.contentDocument);
	
	var message = "";
	var ojb = null;
	for (var i = 0; i < editIframeAux.document.forms[0].length; i++) { 
		ojb = editIframeAux.document.forms[0].elements[i];
		if (ojb.type=='file' || (ojb.type != 'hidden' && ojb.type == 'text') || (ojb.type != 'hidden' && ojb.type == 'select-one') || (ojb.type != 'hidden' && ojb.type == 'textarea')  || editIframe.document.getElementById(ojb.name + "_iframe") != null
		   ){
			var scriptValidate = ojb.getAttribute('scriptValidate');
			
			if (eval(scriptValidate)!=null && trim(eval(scriptValidate))!=""){				
				message = message + eval(scriptValidate) + "\n\r";				
			}
			scriptValidate = null;
		}else{
			if(ojb.type == 'hidden'){
				try{
					if(eval("editIframe." + ojb.name + "_iframe.genericAdmForm." + ojb.name + ".value") != undefined){
						eval("editIframeAux.document.forms[0]." + ojb.name + ".value = editIframe." + ojb.name + "_iframe.genericAdmForm." + ojb.name + ".value");
					}
				}catch(e){}
			}
		}
	}
	ojb = null;
	
	if (message!=""){
		message = "<bean:message key='prompt.paraGravarDadosNecessario' /> " + "\n\r" + "\n\r" + message;
		alert(message);
		return false;
	}else{
		return true; // todos os campos estao preenchidos
	}
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.genericAdmForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = document.getElementById(pasta);
	tab = (tab.contentWindow || tab.contentDocument);
	switch (pasta){
		case 'admIframe':
			tab.document.forms[0].tela.value = 'administracao';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			setaListaBloqueia();
			setaIdiomaBloqueia();
			
			break;
		case 'editIframe':
			tab.document.forms[0].tela.value = '<%=newTela%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');
			setaListaHabilita();
			setaIdiomaHabilita();
			
			break;
	}
  }catch(e){
	alert(e.message);
  }
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function teste(){
}

</script>
</head>

<body ondblclick="" class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">

<html:form styleId="genericAdmForm"	action="/GenericAdm.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="entityName"/>
	<html:hidden property="entityEmpresaName"/>
	<html:hidden property="entityIdiomaName"/>
	<html:hidden property="temMultiempresa"/>
	<html:hidden property="temMultiIdioma"/>
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="100%" width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta('admIframe');">
									<bean:message key="prompt.procurar" /><!-- ## --></td>
									<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta('editIframe');">
									<%= getMessage(entity.getLabel(), request)%><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" align="center"><br>
				<table width="95%" border="0" cellspacing="0" cellpadding="0" height="400">
				<tr>
				<td valign="top">
                  <div name="Manifestacao" id="Manifestacao" style="width: 97%; height: 225px; z-index: 6; display: none"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id="editIframe" name="editIframe"
										src="GenericAdm.do?tela=<%=newTela%>&acao=incluir&entityName=<%=request.getParameter("entityName") %>&entityEmpresaName=<%=entityEmpresa%>&entityIdiomaName=<%=entityIdioma%>&temMultiempresa=<%=request.getParameter("temMultiempresa")%>&temMultiIdioma=<%=request.getParameter("temMultiIdioma")%>"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario" style="width: 100%; height: 199px; z-index: 2; display: block"> 
                    		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalLabel" width="15%"><bean:message key="prompt.descricao"/></td>
									<td class="principalLabel" width="65%">&nbsp;</td>
								</tr>

								<tr>
									<td width="65%">
									<table width="" border="0" cellspacing="0" cellpadding="0">
									<tr>
									<td width="25%">
									<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar(); textCounter(this, tamanhoCampoDesc)"/>
									</td>
									<td width="05%">
									&nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
									</td>
									</tr>	
									</table>
									</td>
								</tr>
								<tr>
									<td class="principalLabel" width="15%">&nbsp;</td>
									<td class="principalLabel" width="64%">&nbsp;</td>
								</tr>
							</table>	
							<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<!-- Obtem todos os campos que devem aparecer na lista de administracao -->
								<%							   	
								Vector lista = entity.getFieldByOrder();
								%>
								
								<tr>
								<td>
								<table border="0" cellspacing="0" cellpadding="0" width="97%" class="sortable" style="cursor: pointer;">
								<!-- Chamado: 81180 - Carlos Nunes - 12/03/2012 -->
								<td width="3%" class="principalLstCab">&nbsp;</td>
								<td width="3%" class="principalLstCab">&nbsp;</td>
								
								<%
									//percorre todos os fields da entidade
									for (int i = 0; i < lista.size(); i++){
										Field field = (Field)lista.get(i);
										View view = field.getViewByName("administracao.jsp");
										
										if (tamanhoCampoDesc == 0 && field.getType().equalsIgnoreCase("string")) {tamanhoCampoDesc = field.getLen();}
										
										if (tamanhoCampoDesc == 0 && field.getType().equalsIgnoreCase("string")) {tamanhoCampoDesc = field.getLen();}
										
										//Caso o campo o nao seja um campo fisico ou esteja configurado na view que o atributo nao pode ser visualizado deve-se pular esse campo
										if (view != null && !view.getVisible()){continue;}
										
								%>				
										<td class="principalLstCab" width="<%= field.getSizeview()%>"><%= getMessage(field.getLabel(), request)%></td>	
								<%	
										field = null;
										view = null;
									}								
								lista = null;									
								%>
								<td class="principalLstCab" width="3%">&nbsp;</td>
								</tr>
								</table>
								
								<%if(tamanhoCampoDesc > 0){ %>
								<script>tamanhoCampoDesc = <%=tamanhoCampoDesc%></script>
								<%} %>
								
								</td>
								<!-- Obtem todos os campos que devem aparecer na lista de administracao -->								
								
								<tr valign="top">
									
                        <td height="320"> 
<!--                         Chamado: 98441 - 30/12/2014 - Leonardo Marquini Facchini -->
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="GenericAdm.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&entityName=<%=request.getParameter("entityName") %>&entityEmpresaName=<%=entityEmpresa%>&entityIdiomaName=<%=entityIdioma%>&temMultiempresa=<%=request.getParameter("temMultiempresa")%>&temMultiIdioma=<%=request.getParameter("temMultiIdioma")%>"
										width="100%" height="100%" scrolling="no" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>
						</td>
						</tr>
						</table>
                </td>
							<td width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
						</tr>
						<tr>
							<td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
							<td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<!-- 91032 - 20/09/2013 - Jaider Alba -->
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" id="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
		</tr>
		<tr>
			<td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
			<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
</script>

<script language="JavaScript">
	
	<%
		Permission permission = (Permission)entity.getPermissions().get("inclusao");
		String inclusao = permission.getKey();
		permission = (Permission)entity.getPermissions().get("alteracao");
		String alteracao = permission.getKey();	
	%>
	
	if('<%= inclusao%>' != '')
		setPermissaoImageDisable('<%= inclusao%>', genericAdmForm.imgIncluir);	
	
	if ('<%= inclusao%>'!='' && '<%=  alteracao%>'!='' && !getPermissao('<%= inclusao%>') &&
	   !getPermissao('<%=  alteracao%>')){
			genericAdmForm.imgGravar.disabled=true;
			genericAdmForm.imgGravar.className = 'geralImgDisable';
			genericAdmForm.imgGravar.title='';
	 }
	
	if ((genericAdmForm.entityEmpresaName.value!=null) && (genericAdmForm.entityEmpresaName.value!='')) {
		habilitaListaEmpresas();
	} else {
		desabilitaListaEmpresas();
	}
	
	setaArquivoXml(genericAdmForm.entityIdiomaName.value);
	
	if ((genericAdmForm.entityIdiomaName.value!=null) && (genericAdmForm.entityIdiomaName.value!='')) {
		habilitaTelaIdioma();
	} else {
		desabilitaTelaIdioma();
	}
	
</script>


</body>
</html>
