<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.form.GenericAdmForm"%>
<%@ page import="br.com.plusoft.fw.entity.*"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Iterator"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>

<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>

</head>

<script>
nomecombo = "";
function carregaCombo() {
	try{		
		eval('parent.carregaCombo_' + nomecombo + '();');	
			
	}catch(e){}
	 
}

	function txtFiltro_onKeyDown(e, idCombo, keyCombo, descCombo) {
		if(e.keyCode==13) {
			preencheCombo(idCombo, keyCombo, descCombo);
			return false;
		}
	
		if(e.keyCode==27) {
			cancelaFiltrar(idCombo);
			return false;
		}
	
		return true;
	}
	
	function filtrarCombo(idCombo) {
		$("divCmb"+idCombo).style.display="none";
		$("divFiltro"+idCombo).style.display="block";

		$("txtFiltro"+idCombo).value="";
		$("txtFiltro"+idCombo).focus();
	}
	
	function cancelaFiltrar(idCombo) {
		$("divCmb"+idCombo).style.display="block";
		$("divFiltro"+idCombo).style.display="none";
	
		$(idCombo).focus();
	}
	
	var ajax;
	function preencheCombo(idCombo, keyCombo, descCombo) {
		ajax = new ConsultaBanco("","GenericAdm.do");	
		
		ajax.addField("tela", "filtrarCombo");	
		ajax.addField("acao", "incluir");
		ajax.addField("field", idCombo);
		ajax.addField("entityName", "<%=request.getParameter("entityName")%>");
		ajax.addField("temMultiempresa", "<%=request.getParameter("temMultiempresa")%>");
		ajax.addField("temMultiIdioma", "<%=request.getParameter("temMultiIdioma")%>");
		ajax.addField("filtro", $("txtFiltro"+idCombo).value);
		<%
			String ajaxConditions = "";
			String requestCondition = (String) request.getAttribute("requestCondition");
			
			if(requestCondition!=null) {
				String requestConditions[] = {""};
				if(requestCondition.indexOf("&")>-1) 
					requestConditions = requestCondition.split("&");
				else
					requestConditions[0] = requestCondition;
				
				
				for(int i=0;i<requestConditions.length;i++) {
					String cond[] = requestConditions[i].split("=", 2);
					
					
					ajaxConditions+= "ajax.addField(\"" + cond[0] + "\", \"" + cond[1] + "\");\n" ;
				}
			}
		
		
		%>
		<%=ajaxConditions %>
		
		ajax.executarConsulta(
			function() { 
				ajax.popularCombo($(idCombo), keyCombo.toLowerCase(), descCombo.toLowerCase(), "", true, "");
				ajax = null;
	
				$("divCmb"+idCombo).style.display="block";
				$("divFiltro"+idCombo).style.display="none";
				$(idCombo).focus();
			});
	}
</script>

<body class="principalBgrPage" text="#000000" onload="carregaCombo()">

<html:form styleId="genericAdmForm"	action="/GenericAdm.do">
	
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="entityName"/>
<html:hidden property="entityEmpresaName"/>
<html:hidden property="entityIdiomaName"/>
<html:hidden property="temMultiempresa"/>
<html:hidden property="temMultiIdioma"/>

<%

Entity entity = EntityInstance.getEntity(request.getParameter("entityName"));
Vector fields = entity.getFieldByOrder();
String objFunctions = new String("");
StringBuffer objAuxiliar = new StringBuffer("");

for(Iterator it = fields.iterator();it.hasNext();){
	Field field = (Field)it.next();
	if(field.getName().equals(request.getParameter("field"))){
		if(field.getFk()!= null && !field.getFk().getEntityName().equals("")){
			
			View newView = (View)field.getViewByName("edit.jsp");
			if(newView != null){
				if(newView.getScripts() != null){
					Vector scriptFunction = (Vector)newView.getScripts();

					for (int i = 0; i < scriptFunction.size(); i++) {
						View functions = (View)scriptFunction.get(i);
						objFunctions = objFunctions + functions.getNameFunction() + "="  + functions.getCodeFunction();
					}
				}

			}
			%>
				<script>nomecombo='<%=field.getName()%>'</script>
			<%
			
			String objName = field.getName();
			
			objAuxiliar.append( "<div id='divCmb"+objName+"' style='display:block;'>");
			//Chamado: 91643 - 20/12/2013 - Carlos Nunes
			String widthCmb = "100%";
			
			if(request.getAttribute("exibirLupa"+objName)!=null) {
				widthCmb = "90%";
			}
			
			objAuxiliar.append( "<select name='" + field.getName() + "' style='width: " + widthCmb + ";' class='principalObjForm' " + objFunctions + ">");
			
			objAuxiliar.append( "	<option value=''>" + getMessage("prompt.selecione_uma_opcao",request) + "</option>");
			if(request.getAttribute("genericVector")!=null){
				for(Iterator itVector = ((Vector)request.getAttribute("genericVector")).iterator() ; itVector.hasNext();){
					Vo vo = (Vo)itVector.next();
					objAuxiliar.append( "  		<option value='" + vo.getFieldAsString(field.getFk().getKey()) + "'>" + vo.getFieldAsString(field.getFk().getDescription()) + "</option>");
				}		
			}
			objAuxiliar.append( "</select>   ");

			
			if(request.getAttribute("exibirLupa"+objName)!=null) {
				objAuxiliar.append("<img src='webFiles/images/botoes/lupa.gif' id='imgLupa" + objName + "' align='absmiddle' class='geralCursoHand' title='" + getMessage("prompt.filtrar",request) + "' ");
				objAuxiliar.append("onclick='filtrarCombo(\""+objName+"\")' />");
				
				objAuxiliar.append( "</div>");
				
				objAuxiliar.append( "<div id='divFiltro"+objName+"' style='display:none;'>");
				
				objAuxiliar.append( "<input type='text' id='txtFiltro" + objName + "' name='txtFiltro" + objName + "' class='principalObjForm' onkeydown=\"return txtFiltro_onKeyDown(event, '"+objName+"', '"+field.getFk().getKey()+"', '"+field.getFk().getDescription()+"');\" style='width: 90%;' />");
				objAuxiliar.append("<img src='webFiles/images/icones/check.gif' align='absmiddle' class='geralCursoHand' title='" + getMessage("prompt.pesquisar",request) + "' ");
				objAuxiliar.append("onclick=\"preencheCombo('"+objName+"', '"+field.getFk().getKey()+"', '"+field.getFk().getDescription()+"');\" />");
			}
			
			objAuxiliar.append( "</div>");
			
			if(!objFunctions.equals("")){				
				objAuxiliar.append( "<script>setTimeout(" + objFunctions + ",500)</script>  ");
			}
		}
	}
}
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr> 
     <td colspan="2"> 
     	<%= objAuxiliar.toString()%>
     </td>
   </tr>
</table>   
                
</html:form>
</body>
</html>