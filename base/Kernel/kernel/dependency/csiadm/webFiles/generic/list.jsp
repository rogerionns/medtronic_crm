<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.form.GenericAdmForm"%>
<%@ page import="br.com.plusoft.fw.entity.*"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Iterator"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


Entity entity = null;
entity = EntityInstance.getEntity(request.getParameter("entityName"));

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src="<bean:message key="prompt.funcoes"/>/TratarDados.js"></script>
</head>

<script>
function removeParametro(nTblExcluir) {
	msg = 'Deseja excluir esse Parametro?';
	if (confirm(msg)) {
		objIdTbl = window.document.all.item(nTblExcluir);
		lstParametros.removeChild(objIdTbl);
		estilo--;
	}
}

function load(){
	parent.parent.tab.document.forms[0].tela.value = parent.parent.telaEdit;
}

</script>

<body class="principalBgrPageIFRM" text="#000000" onload="load();">

<html:form styleId="genericAdmForm"	action="/GenericAdm.do">
	
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="entityName"/>
<html:hidden property="entityEmpresaName"/>
<html:hidden property="entityIdiomaName"/>
<html:hidden property="temMultiempresa"/>
<html:hidden property="temMultiIdioma"/>

<%

Enumeration enu = request.getParameterNames();
while(enu.hasMoreElements()){
	String name = (String)enu.nextElement();
	String valor = request.getParameter(name);	
	%>
		<input type="hidden" name="<%=name %>" value="<%=valor %>"/>
	<%
}


StringBuffer objAuxiliar = new StringBuffer("");
Vector listaFields = entity.getFieldByOrder();
Vector listaFieldsPK = entity.getPkFields();
String listExcluir = new String("");


if(request.getAttribute("listVector")!=null){
	
	
	for(int i = 0; i < ((Vector)request.getAttribute("listVector")).size(); i++){
		Vo vo = (Vo)((Vector)request.getAttribute("listVector")).get(i);
		
		objAuxiliar.append("	<table id=\"" + i + "\" width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ");		
		
		objAuxiliar.append("		<tr valign=\"top\"> ");
		
		
		
		//Os campos como pk para executar o dubmit quando o usuario clicar sobre o item.				

		StringBuffer javascriptPk = new  StringBuffer();
		javascriptPk.append("parent.parent.setPK(");
		for (int loopPk = 0; loopPk < listaFieldsPK.size(); loopPk++){
			Field fieldPK = (Field)listaFieldsPK.get(loopPk);					
			String value = vo.getFieldAsString(fieldPK.getName());					
			
			if (loopPk!=0)
				javascriptPk.append(", ");	
			javascriptPk.append("'" + fieldPK.getName() + "','" + value + "'");
			fieldPK = null;
		}

		javascriptPk.append(");");
		
		objAuxiliar.append("			<td width=\"5%\" class=\"principalLstParMao\" >  ");
		objAuxiliar.append("				<img src=\"webFiles/images/botoes/lixeira18x18.gif\" name=\"lixeira\" id=\"lixeira\" width=\"18\" height=\"18\" title='" + getMessage("prompt.excluir",request) + "' onclick=\"" + javascriptPk + "parent.parent.submit_remove()\">");
		objAuxiliar.append("			</td> ");
		
		
//		Percorre todos os campos disponiveis para esse pagina
		for (int loopFields = 0; loopFields < listaFields.size(); loopFields++){
			Field field = (Field)listaFields.get(loopFields);
			
			View view = field.getViewByName("list.jsp");
			
			//Caso o campo o nao seja um campo fisico ou esteja configurado na view que o atributo nao pode ser visualizado deve-se pular esse campo
			if (view != null && !view.getVisible()){continue;}
			
			objAuxiliar.append("<script>" + field.getScriptValidate("list.jsp") + "</script>");
			
			String valor = String.valueOf( vo.getFieldByFormat(field.getName(), field.getFormat("list.jsp")));
			//String valor = vo.getFieldAsString(field.getName());
			if (valor == null || valor.equals("") || valor.equalsIgnoreCase("null")){
				valor = "&nbsp;";
			}
		
			objAuxiliar.append("		<td class=\"principalLstParMao\" width=\"" + field.getSizeview() + "\" onclick=\"" + javascriptPk + "parent.parent.submit_edit()\">  ");
			objAuxiliar.append("		 " + valor);
			objAuxiliar.append("		</td> ");
		
			field = null;
			view = null;
		}
		vo = null;

		objAuxiliar.append("		</tr> ");		
		objAuxiliar.append("	</table> ");		
	}		
}




%>

	<%= objAuxiliar.toString()%>

          
  <script>
  //Chamado: 84704 - 09/10/2012 - Carlos Nunes
	  if('<%= ((Permission)entity.getPermissions().get("exclusao")).getKey()%>'!='')
	  { 
			parent.parent.setPermissaoImageDisable('<%= ((Permission)entity.getPermissions().get("exclusao")).getKey()%>', genericAdmForm.lixeira);
	  }
  </script>      
</html:form>
</body>
</html>