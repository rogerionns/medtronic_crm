<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>

<script language="JavaScript">
	var possuiResponsavel = false;
	parent.administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInResponsavel'].checked = false;
	parent.administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInEditar'].checked = false;
	parent.administracaoCsAstbFuncareavendaFuavForm['csAstbFuncareavendaFuavVo.fuavInVer'].checked = false;
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsAstbFuncareavendaFuavForm" action="/AdministracaoCsAstbFuncareavendaFuav.do"> 
<html:hidden property="tela" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csAstbFuncareavendaFuavVector" indexId="sequencia"> 
  <tr> 
    <td width="5%" > 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18" height="18" class="principalLstParMao" title="<bean:message key="prompt.excluir"/>" onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="ccttrtVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />','<bean:write name="ccttrtVector" property="csCdtbFuncVendedorFuncVo.idFuncCdFuncionario" />')"> 
    </td>
    <td  class="principalLstPar" width="50%">
     <bean:write name="ccttrtVector" property="csCdtbFuncVendedorFuncVo.funcNmFuncionario" /> 
    </td>
    <td  class="principalLstPar" width="10%" align="center">
  &nbsp;&nbsp;<logic:equal property="fuavInVer" name="ccttrtVector" value="false">
				  N
			  </logic:equal>
			  <logic:notEqual property="fuavInVer" name="ccttrtVector" value="false">
				  S
			  </logic:notEqual>
    </td>

    <td  class="principalLstPar" width="10%" align="center">
  &nbsp;&nbsp;<logic:equal property="fuavInEditar" name="ccttrtVector" value="false">
				  N
			  </logic:equal>
			  <logic:notEqual property="fuavInEditar" name="ccttrtVector" value="false">
				  S
			  </logic:notEqual>
    </td>
    <td  class="principalLstPar" width="10%" align="center">
  &nbsp;&nbsp;<logic:equal property="fuavInResponsavel" name="ccttrtVector" value="false">
				  N
			  </logic:equal>
			  <logic:notEqual property="fuavInResponsavel" name="ccttrtVector" value="false">
				  S
				<script>possuiResponsavel = true;</script>
			  </logic:notEqual>
    </td>
    <td  class="principalLstPar" width="15%">&nbsp;</td>

    
  </tr>
  </logic:iterate>
</table>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_SFA_FUNCIONARIOVENDEDOR_EXCLUSAO_CHAVE%>', editCsAstbFuncareavendaFuavForm.lixeira);
</script>

</html:form>
</body>
</html>