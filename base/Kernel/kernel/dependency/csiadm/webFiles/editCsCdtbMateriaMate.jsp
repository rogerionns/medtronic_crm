<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
 	function desabilitaCampos(){
 		administracaoCsCdtbMateriaMateForm.idMateCdMateria.disabled= true;
		administracaoCsCdtbMateriaMateForm.mateCdCodMsd.disabled= true;	
		administracaoCsCdtbMateriaMateForm.mateDsMateria.disabled= true;
		administracaoCsCdtbMateriaMateForm.mateDsMathCode.disabled= true;
		administracaoCsCdtbMateriaMateForm.mateDsResumo.disabled= true;
		administracaoCsCdtbMateriaMateForm.mateTxMateria.disabled= true;
		administracaoCsCdtbMateriaMateForm.mateDhCadastro.disabled= true;
		administracaoCsCdtbMateriaMateForm.mateDhAtualizacao.disabled= true;
		administracaoCsCdtbMateriaMateForm.mateDhInativo.disabled= true;
	}	
	
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbMateriaMateForm.idMateCdMateria.value);
	}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>'); inicio();">
<html:form styleId="administracaoCsCdtbMateriaMateForm" action="/AdministracaoCsCdtbMateriaMate.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>		

	<!-- Lst Local -->
	<div id="camposHidden">
	</div>
	
	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.codigo" />
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="30%"> 
            <html:text property="idMateCdMateria" styleClass="text" disabled="true" />
          </td>
          <td width="20%" align="right" class="principalLabel">
            <bean:message key="prompt.identificacao" />
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
          </td>
          <td width="30%">
            <html:text property="mateCdCodMsd" styleClass="text" maxlength="10" />
          </td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.titulo" />
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="3"> 
            <html:text property="mateDsMateria" styleClass="text" maxlength="250" /> 
          </td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.matchCode" />
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="3"> 
            <html:text property="mateDsMathCode" styleClass="text" maxlength="250" readonly="true" /> 
          </td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.resumoMateria" />
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="3"> 
            <html:textarea property="mateDsResumo" styleClass="text" rows="3" onkeyup="textCounter(this, 200)" onblur="textCounter(this, 200)" /> 
          </td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.textoMateria" />
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="3"> 
            <html:textarea property="mateTxMateria" styleClass="text" rows="4" onkeyup="textCounter(this, 2000)" onblur="textCounter(this, 2000)"/> 
          </td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.localizacaoMaterias" />
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="3" height="30">
            <iframe name="local" src="AdministracaoCsCdtbMateriaMate.do?acao=visualizar&tela=localizacaoMateria" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          </td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3" height="75"> 
            <iframe name="lstLocal" src="AdministracaoCsCdtbMateriaMate.do?acao=visualizar&tela=lstLocalizacaoMateria&idMateCdMateria=<bean:write name='administracaoCsCdtbMateriaMateForm' property='idMateCdMateria' />" width="100%" height="100%" scrolling="Yes" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          </td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.dtCadastro" />
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="32%"> 
            <html:text property="mateDhCadastro" styleClass="text" readonly="true" />
          </td>
          <td width="20%" align="right" class="principalLabel">
            <bean:message key="prompt.dtAtualizacao" />
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
          </td>
          <td width="35%">
            <html:text property="mateDhAtualizacao" styleClass="text" readonly="true" />
          </td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="32%">&nbsp;</td>
          <td width="20%">&nbsp;</td>
          <td class="principalLabel" width="35%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="mateDhInativo"/></td>
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo" />
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			administracaoCsCdtbMateriaMateForm.mateDsMateria.disabled= true;
			administracaoCsCdtbMateriaMateForm.mateDsMathCode.disabled= true;
			administracaoCsCdtbMateriaMateForm.mateTxMateria.disabled= true;
			administracaoCsCdtbMateriaMateForm.mateDsResumo.disabled= true;
			administracaoCsCdtbMateriaMateForm.mateDhCadastro.disabled= true;
			administracaoCsCdtbMateriaMateForm.mateDhAtualizacao.disabled= true;
			administracaoCsCdtbMateriaMateForm.mateCdCodMsd.disabled= true;
			administracaoCsCdtbMateriaMateForm.mateDhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_MATERIA_INCLUSAO_CHAVE%>', parent.administracaoCsCdtbMateriaMateForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_MATERIA_INCLUSAO_CHAVE%>')){
				desabilitaCampos();
			}		
		
			administracaoCsCdtbMateriaMateForm.idMateCdMateria.disabled= false;
			administracaoCsCdtbMateriaMateForm.idMateCdMateria.value= '';
			administracaoCsCdtbMateriaMateForm.mateDsMateria.disabled= false;
			administracaoCsCdtbMateriaMateForm.mateDsMathCode.disabled= false;
			administracaoCsCdtbMateriaMateForm.mateTxMateria.disabled= false;
			administracaoCsCdtbMateriaMateForm.mateDsResumo.disabled= false;
			administracaoCsCdtbMateriaMateForm.mateDhCadastro.disabled= false;
			administracaoCsCdtbMateriaMateForm.mateDhAtualizacao.disabled= false;
			administracaoCsCdtbMateriaMateForm.mateCdCodMsd.disabled= false;
			administracaoCsCdtbMateriaMateForm.idMateCdMateria.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_MATERIA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_MATERIA_ALTERACAO_CHAVE%>', parent.administracaoCsCdtbMateriaMateForm.imgGravar);	
				desabilitaCampos();
			}
		</script>
</logic:equal>
</html>
<script>
	try{administracaoCsCdtbMateriaMateForm.mateDsMateria.focus();}
	catch(e){}	
</script>