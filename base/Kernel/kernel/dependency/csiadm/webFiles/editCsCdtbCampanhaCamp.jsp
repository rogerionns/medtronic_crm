<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) 
	  	if ((obj=MM_findObj(args[i]))!=null) { 
	  		v=args[i+2];
			if (obj.style) { 
				obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; 
			}
			obj.display=v; 
		}
	}
	
	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function SetClassFolder(pasta, estilo) {
	 	stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
	 	eval(stracao);
	} 
	
	function VerificaCalendarioInicial(){
		if(document.administracaoCsCdtbCampanhaCampForm.campDhInicial.disabled==false){
			show_calendar('administracaoCsCdtbCampanhaCampForm.campDhInicial');
		}
	}
	
	function VerificaCalendarioFinal(){
		if(document.administracaoCsCdtbCampanhaCampForm.campDhFinal.disabled==false){
			show_calendar('administracaoCsCdtbCampanhaCampForm.campDhFinal');
		}
	}

	
	function AtivarPasta(pasta, obj)
	{

		SetClassFolder('tdCarta','principalPstQuadroLinkNormal');
		SetClassFolder('tdEmail','principalPstQuadroLinkNormal');
		SetClassFolder('tdAtivo','principalPstQuadroLinkNormal');
		SetClassFolder('tdSms','principalPstQuadroLinkNormal');
		SetClassFolder('tdDistribuicao','principalPstQuadroLinkNormal');

		MM_showHideLayers('Carta','','hide','Email','','hide', 'Ativo','','hide','SMS','','hide', 'Distribuicao', '', 'hide', pasta, '', 'show');
		SetClassFolder(obj.id, 'principalPstQuadroLinkSelecionado');
	}
	
	function desabilitaCamposCampanha(){
		document.administracaoCsCdtbCampanhaCampForm.campDsCampanha.disabled= true;	
		document.administracaoCsCdtbCampanhaCampForm.campDhInativo.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.idDocuCdEmailBody.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.idDocuCdDocumento.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campDhInicial.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campDhFinal.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campInEmail.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campInCartaemail.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campInCartaImpressa.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campInEtiqueta.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campInRegistro.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campInDistribuicao.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campInTelefone.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campNrValmediotel.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campNrValmediocarta.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campNrValmedioemail.disabled= true;
		document.administracaoCsCdtbCampanhaCampForm.campInSms.disabled= true;
	}

	function marcaDistribuicao() {
		document.getElementById("tdDistribuicao").style.display = (document.forms[0].campInDistribuicao.checked?"":"none");

		if(!document.forms[0].campInDistribuicao.checked && document.getElementById("Distribuicao").style.display!="none") document.getElementById("tdCarta").click();
		
	}
	
	function marcaRegistro(){
		if(document.administracaoCsCdtbCampanhaCampForm.campInRegistro.checked==true){
			if(confirm("<bean:message key='prompt.aoSelecionarEstaOpcaoInformacoesCartaImpressaEmailAtivoSeraoApagadas' />")){
				document.administracaoCsCdtbCampanhaCampForm.idDocuCdEmailBody.disabled= true;
				document.administracaoCsCdtbCampanhaCampForm.idDocuCdDocumento.disabled= true;
				document.administracaoCsCdtbCampanhaCampForm.campInEmail.disabled= true;
				document.administracaoCsCdtbCampanhaCampForm.campInCartaemail.disabled= true;
				document.administracaoCsCdtbCampanhaCampForm.campInCartaImpressa.disabled= true;
				document.administracaoCsCdtbCampanhaCampForm.campInEtiqueta.disabled= true;
				document.administracaoCsCdtbCampanhaCampForm.campInTelefone.disabled= true;
				document.administracaoCsCdtbCampanhaCampForm.campNrValmediotel.disabled= true;
				document.administracaoCsCdtbCampanhaCampForm.campNrValmediocarta.disabled= true;
				document.administracaoCsCdtbCampanhaCampForm.campNrValmedioemail.disabled= true;

				document.administracaoCsCdtbCampanhaCampForm.idDocuCdEmailBody.value="";
				document.administracaoCsCdtbCampanhaCampForm.idDocuCdDocumento.value="";
				document.administracaoCsCdtbCampanhaCampForm.campInEmail.checked= false;
				document.administracaoCsCdtbCampanhaCampForm.campInCartaemail.checked= false;
				document.administracaoCsCdtbCampanhaCampForm.campInCartaImpressa.checked= false;
				document.administracaoCsCdtbCampanhaCampForm.campInEtiqueta.checked= false;
				document.administracaoCsCdtbCampanhaCampForm.campInTelefone.checked= false;
				document.administracaoCsCdtbCampanhaCampForm.campNrValmediotel.value="";
				document.administracaoCsCdtbCampanhaCampForm.campNrValmediocarta.value="";
				document.administracaoCsCdtbCampanhaCampForm.campNrValmedioemail.value="";

			}else{
				document.administracaoCsCdtbCampanhaCampForm.campInRegistro.checked = false;
			}
		}else{
				document.administracaoCsCdtbCampanhaCampForm.idDocuCdEmailBody.disabled= false;
				document.administracaoCsCdtbCampanhaCampForm.idDocuCdDocumento.disabled= false;
				document.administracaoCsCdtbCampanhaCampForm.campInEmail.disabled= false;
				document.administracaoCsCdtbCampanhaCampForm.campInCartaemail.disabled= false;
				document.administracaoCsCdtbCampanhaCampForm.campInCartaImpressa.disabled= false;
				document.administracaoCsCdtbCampanhaCampForm.campInEtiqueta.disabled= false;
				document.administracaoCsCdtbCampanhaCampForm.campInTelefone.disabled= false;
				document.administracaoCsCdtbCampanhaCampForm.campNrValmediotel.disabled= false;
				document.administracaoCsCdtbCampanhaCampForm.campNrValmediocarta.disabled= false;
				document.administracaoCsCdtbCampanhaCampForm.campNrValmedioemail.disabled= false;
		}
	}
	
	function verificarRegistro(){
		if(document.administracaoCsCdtbCampanhaCampForm.campInRegistro.checked==true){
			document.administracaoCsCdtbCampanhaCampForm.idDocuCdEmailBody.disabled= true;
			document.administracaoCsCdtbCampanhaCampForm.idDocuCdDocumento.disabled= true;
			document.administracaoCsCdtbCampanhaCampForm.campInEmail.disabled= true;
			document.administracaoCsCdtbCampanhaCampForm.campInCartaemail.disabled= true;
			document.administracaoCsCdtbCampanhaCampForm.campInCartaImpressa.disabled= true;
			document.administracaoCsCdtbCampanhaCampForm.campInEtiqueta.disabled= true;
			document.administracaoCsCdtbCampanhaCampForm.campInTelefone.disabled= true;
			document.administracaoCsCdtbCampanhaCampForm.campNrValmediotel.disabled= true;
			document.administracaoCsCdtbCampanhaCampForm.campNrValmediocarta.disabled= true;
			document.administracaoCsCdtbCampanhaCampForm.campNrValmedioemail.disabled= true;
		}
	}

	function inicio(){
		//setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbCampanhaCampForm.idCampCdCampanha.value);
	}	

	function validatePerc(obj, event) {
		numberValidate(obj, 0, '.', ',', event);
		
		var n = new Number(obj.value);
		if(n > 100) n = 100;
		if(n < 0) n = 0;

		obj.value = n;
		validateSum();

		return true;
	}

	function validateSum() {
		var t = document.getElementsByName("publNrPercdistrib");

		if(t.length==0) return 100;
		
		var s = new Number(0);
		
		for(var i = 0; i < t.length; i++) {
			if(s >= 100) {
				t[i].value = 0;
			} else {
				if (s+(new Number(t[i].value)) >= 100) {
					t[i].value = (100-s);
				} 
			}
			
			if(!t[i].disabled)
				s += new Number(t[i].value);
		}

		return s;
	}
	
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();verificarRegistro();marcaDistribuicao();" style="overflow: hidden;">

<html:form styleId="administracaoCsCdtbCampanhaCampForm" action="/AdministracaoCsCdtbCampanhaCamp.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />

	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
  <tr> 
    <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="13%"> <html:text property="idCampCdCampanha" styleClass="text" disabled="true" /> 
    </td>
    <td colspan="2">&nbsp;</td>
    <td width="35%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="3"><html:text property="campDsCampanha" styleClass="text" maxlength="60" /> 
    </td>
    <td width="35%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.dataInicial"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><html:text property="campDhInicial" styleClass="text" maxlength="10" onblur="verificaData(this)"  onkeydown="return validaDigito(this, event)"/> 
    </td>
    <td width="10%"><img src="webFiles/images/botoes/calendar.gif" title='<bean:message key="prompt.calendario"/>' width="16" height="15" onclick="VerificaCalendarioInicial()" class="principalLstParMao" ></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="35%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%" align="right" class="principalLabel"><bean:message key="prompt.dataFinal"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td><html:text property="campDhFinal" styleClass="text" maxlength="10" onblur="verificaData(this)" onkeydown="return validaDigito(this, event)" /> 
    </td>
    <td width="10%"><img src="webFiles/images/botoes/calendar.gif" title='<bean:message key="prompt.calendario"/>' width="16" height="15" onclick="VerificaCalendarioFinal()" class="principalLstParMao" ></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="35%">&nbsp;</td>
  </tr>
  <tr> 
	<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.registro"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	<td width="13%"><html:checkbox value="true" property="campInRegistro" onclick="marcaRegistro()"/></td>
	<td width="17%">&nbsp;</td>
	<td width="17%">&nbsp;</td>
	<td width="35%"></td>
 </tr>  
  <tr> 
	<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.distribuicao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	<td width="13%"><html:checkbox value="true" property="campInDistribuicao" onclick="marcaDistribuicao();" /></td>
	<td width="17%">&nbsp;</td>
	<td width="17%">&nbsp;</td>
	<td width="35%"></td>
 </tr>  
  
  <tr> 
    <td width="18%" align="right" class="principalLabel">&nbsp;</td>
    <td width="13%">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td width="35%">&nbsp;</td>
  </tr>
  
  <tr>
  	<td colspan="5" class="principalLabel">&nbsp;</td>
  </tr>
 
  <tr>
  	<!--
  	<td width="18%" align="right" class="principalLabel">&nbsp;</td>
  	-->
  		
  	<td colspan="5">
	    <!-- TITULOS DAS ABAS -->
		<table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
		  <tr> 
			<td class="principalPstQuadroLinkSelecionado" align="center" id="tdCarta" name="tdCarta" onclick="AtivarPasta('Carta', this)"><bean:message key="prompt.cartaImpressora"/></td>
			<td class="principalPstQuadroLinkNormal" align="center" id="tdEmail" name="tdEmail" onclick="AtivarPasta('Email', this)"><bean:message key="prompt.eMail"/></td>
			<td class="principalPstQuadroLinkNormal" align="center" id="tdAtivo" name="tdAtivo" onclick="AtivarPasta('Ativo', this)"><bean:message key="prompt.telefone"/></td>
			<td class="principalPstQuadroLinkNormal" align="center" id="tdSms" name="tdSms" onclick="AtivarPasta('SMS', this)"><bean:message key="prompt.SMS"/></td>
			<td class="principalPstQuadroLinkNormal" align="center" id="tdDistribuicao" name="tdDistribuicao" onclick="AtivarPasta('Distribuicao', this)"><bean:message key="prompt.distribuicao"/></td>
			
			<td>&nbsp;</td>
		  </tr>
		</table>
		<!-- TITULOS DAS ABAS -->
		  
		<table width="90%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" height="100">
		  <tr class="principalBordaQuadro">
		  		<!-- ABA CARTA-->
				<td valign="top"> 
				  <div id="Carta" style="width:95%; height:130px; z-index:3; display: block"> 				  	
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
					  <tr> 
						<td colspan="5" class="principalEspacoPqn">&nbsp;</td>
					  </tr>
					  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.cartaImpressora"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td width="13%"><html:checkbox value="true" property="campInCartaImpressa"/></td>
							<td width="17%"> 
							  <div align="right"><bean:message key="prompt.etiqueta"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
							</td>
							<td width="17%"><html:checkbox value="true" property="campInEtiqueta"/></td>
							<td width="35%"></td>
						  </tr>
						  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.documento"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td colspan="3"> <html:select property="idDocuCdDocumento" styleClass="principalObjForm" > 
							  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
								  <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/> 
							  </html:select> </td>
						  </tr>
						  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.campanha.valorMedio"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td><html:text property="campNrValmediocarta" styleClass="text" maxlength="10" onblur="return numberValidate(this, 2, '.', ',', event);"/> 
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td width="35%">&nbsp;</td>
						  </tr>
						  
						  
					  
					</table>
				  </div>
		  		<!-- ABA CARTA-->

		  		<!-- ABA EMAIL-->
				  <div id="Email" name="Email" style="width:95%; height:130px; z-index:3; display: none"> 
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
					  <tr> 
						<td colspan="5" class="principalEspacoPqn">&nbsp;</td>
					   </tr>
					   
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.eMail"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td width="13%"><html:checkbox value="true" property="campInEmail"/></td>
							<td width="19%"> 
							  <div align="right"><bean:message key="prompt.cartaEmail"/> 
							  	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							  	</div>
							</td>
							<td width="15%"><html:checkbox value="true" property="campInCartaemail"/></td>
							<td width="35%"></td>
						  </tr>
						  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.emailBody"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td colspan="3"> <html:select property="idDocuCdEmailBody" styleClass="principalObjForm" > 
							  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/> 
							  </html:select> </td>
						  </tr>
						  
						  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.campanha.valorMedio"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td><html:text property="campNrValmedioemail" styleClass="text" maxlength="10" onblur="return numberValidate(this, 2, '.', ',', event);"/> 
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td width="35%">&nbsp;</td>
						  </tr>
						
					</table>
				  </div>
		  		<!-- ABA EMAIL-->

		  		<!-- ABA ATIVO-->
				<div id="Ativo" name="Ativo" style="width:95%; height:130px; z-index:3; display: none"> 
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
					  <tr> 
						<td colspan="5" class="principalEspacoPqn">&nbsp;</td>
					  </tr>
					  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.telefone"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td width="13%"><html:checkbox value="true" property="campInTelefone"/></td>
							<td width="17%">&nbsp;</td>
							<td width="17%">&nbsp;</td>
							<td width="35%"></td>
						  </tr>
						  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.campanha.valorMedio"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td><html:text property="campNrValmediotel" styleClass="text" maxlength="10" onblur="return numberValidate(this, 2, '.', ',', event);"/> 
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td width="35%">&nbsp;</td>
						  </tr>
						  
					  
					</table>
				</div>
		  		<!-- ABA ATIVO-->
		  		
		  		<!-- ABA SMS-->
		  		<div id="SMS" style="width:95%; height:130px; z-index:3; display: none"> 				  	
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
					  <tr> 
						<td colspan="5" class="principalEspacoPqn">&nbsp;</td>
					  </tr>
					  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.SMS"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td width="13%"><html:checkbox value="true" property="campInSms"/></td>
							<td>&nbsp;</td>
						  </tr>
						  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.classeImplementacao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td colspan="2"> <html:select property="idIpchCdImplementchat" styleClass="principalObjForm" > 
							<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
							<html:options collection="csDmtbImplementchatIpchVector" property="field(id_ipch_cd_implementchat)" labelProperty="field(ipch_ds_implementchat)"/> 
						</html:select> </td>
						  </tr>
						  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.documento"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td colspan="2"> <html:select property="idDocuCdSms" styleClass="principalObjForm" > 
							  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
								  <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/> 
							  </html:select> </td>
						  </tr>
						  
						  <tr> 
							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.campanha.valorMedio"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
							<td><html:text property="campNrValmediosms" styleClass="text" maxlength="10" onblur="return numberValidate(this, 2, '.', ',', event);"/> 
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td width="35%">&nbsp;</td>
						  </tr>
						  
						  
					  
					</table>
				  </div>
		  		<!-- ABA SMS-->

		  		<!-- ABA DITRIBUIÇÃO -->
				<div id="Distribuicao" style="width:95%; height:130px; z-index:4; display: none; ">
					<table width="610" border="0" cellspacing="0" cellpadding="0" align="left" style="margin: 5px;">
						<tr>
							<td width="80%" align="left" class="principalLstCab"><bean:message key="prompt.subcampanha" /></td>
							<td width="10%" align="left" class="principalLstCab" title="<bean:message key="prompt.percentualDistribuicao" />"><bean:message key="prompt.percDist" /></td>
						</tr>
						
						<tr>
							<td colspan="2">	
								<div style="overflow: auto; width: 610px; height: 100px; border: 1px solid #e0e0e0;">
								<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
									<logic:present name="csCdtbPublicoPublDist">
									<logic:iterate id="publ" name="csCdtbPublicoPublDist">
									<tr>
										<td width="80%" align="left" class="principalLstPar"><bean:write name="publ" property="field(publ_ds_publico)" /></td>
										<td width="10%" align="left" class="principalLstPar">
											<input type="hidden" name="idPublCdPublicoDistrib" value="<bean:write name="publ" property="field(id_publ_cd_publico)" />" />
											<input type="hidden" name="publDsPublicoDistrib" value="<bean:write name="publ" property="field(publ_ds_publico)" />" />
											
											<input class="principalObjForm" type="text" maxlength="3" style="text-align: center;" name="publNrPercdistrib" <logic:notEqual name="publ" property="field(publ_dh_inativo)" value="">disabled</logic:notEqual> value="<bean:write name="publ" property="field(publ_nr_percdistrib)" />"onblur="return validatePerc(this, event);" />
										</td>
									</tr>
									</logic:iterate>
									</logic:present>
								</table>
								</div>
							</td>
						</tr>
						
					</table>
					</div>
		  		<!-- ABA DITRIBUIÇÃO-->

				
				</td>
		  </tr>
		  
		</table>
  	</td>
  </tr>
  
  <tr> 
    <td width="18%" align="right" class="principalLabel">&nbsp;</td>
    <td width="13%">&nbsp;</td>
    <td width="17%"> 
      <div align="right"><bean:message key="prompt.inativo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td width="17%"><html:checkbox value="true" property="campDhInativo"/></td>
    <td width="35%"></td>
  </tr>

  <tr> 
    <td width="18%" align="right" class="principalLabel">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
    <td width="35%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%" align="right" class="principalLabel">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
    <td width="35%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%">&nbsp;</td>
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr> 
    <td width="18%">&nbsp;</td>
    <td width="13%">&nbsp;</td>
    <td class="principalLabel" colspan="2">&nbsp; </td>
    <td width="35%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposCampanha();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPANHA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbCampanhaCampForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPANHA_INCLUSAO_CHAVE%>')){
				desabilitaCamposCampanha();
			}else{
				document.administracaoCsCdtbCampanhaCampForm.idCampCdCampanha.disabled= false;
				document.administracaoCsCdtbCampanhaCampForm.idCampCdCampanha.value= '';
				document.administracaoCsCdtbCampanhaCampForm.idCampCdCampanha.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPANHA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_CAMPANHA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbCampanhaCampForm.imgGravar);	
				desabilitaCamposCampanha();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbCampanhaCampForm.campDsCampanha.focus();}
	catch(e){}
</script>