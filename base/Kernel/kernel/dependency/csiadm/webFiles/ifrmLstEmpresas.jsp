<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*, java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, 
				 br.com.plusoft.fw.app.Application, 
				 br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo,
				 br.com.plusoft.csi.adm.vo.MultiEmpresaVo,
				 br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<%
	boolean mostrarPrincipal =false;
	if(request.getSession().getAttribute("tabela") != null && ((String)request.getSession().getAttribute("tabela")).equalsIgnoreCase(MAConstantes.TABLE_CS_ASTB_EMPRESAFUNC_EMFU)){
		mostrarPrincipal =true;
	}
	long idEmprCdEmpresaPrincipal = 0;
	if(request.getSession().getAttribute("emprInPrincipal")!= null){
		idEmprCdEmpresaPrincipal = Long.parseLong((String)request.getSession().getAttribute("emprInPrincipal"));
	}
	
%>

<html:html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/pt/funcoes.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>

<script>

var cmbNive = '';

	cmbNive += '<select name="<<NIVE_NAME>>" id="<<NIVE_ID>>" class="principalObjForm" disabled=true >';
	cmbNive += '	<option value="0"><bean:message key="prompt.selecione_uma_opcao"/></option>';
	<logic:present name="csCdtbNivelAcessoNiveVector">
		<logic:iterate id="ccniveVector" name="csCdtbNivelAcessoNiveVector" indexId="sequencia">
			cmbNive += '	<option value="<bean:write name="ccniveVector" property="idNiveCdNivelAcesso" />">';
			cmbNive += '<bean:write name="ccniveVector" property="niveDsNivelacesso" />';
			cmbNive += '</option>';
		</logic:iterate>
	</logic:present>
	cmbNive += '</select>';

	function verificaPrincipal(objCheck,nLinha){
		
		<%if(mostrarPrincipal){%>
		
		var obj = document.getElementsByName("emprInPrincipal");
		var nodePrincipal = 0;
		var existePrincipal = false;
		var empresaPrincipal = 0;
		var totalSel = 0;
		
		if(obj.length != null && obj.length != undefined){
			for (nNode=0;nNode<obj.length;nNode++) {
				if(obj[nNode].checked){
					existePrincipal = true;
					nodePrincipal = nNode;
					empresaPrincipal = document.getElementsByName("listaIdEmprCdEmpresa")[nNode].value;
					totalSel++;
				}
			}
		}else{
			if(obj.checked == true){
				existePrincipal = true;
				nodePrincipal = -1;
				totalSel++;
			}
		}
		
		if(objCheck.checked == true){
			if(existePrincipal == false){
				if(nodePrincipal > -1){
					document.getElementById("emprInPrincipal" + objCheck.value).checked=true;
				}else{
					document.getElementById("emprInPrincipal").checked=true;
				}
			} else {
				//Procura quem est� marcado como principal e pega o valor da �rea
				document.getElementById("listaIdNiveCdNivelacesso" + objCheck.value).value=document.getElementById("listaIdNiveCdNivelacesso"+empresaPrincipal).value;
			}
		}else{
			
			if(nodePrincipal > -1){
				document.getElementById("emprInPrincipal" + objCheck.value).checked=false;
				document.getElementById("listaIdNiveCdNivelacesso" + objCheck.value).value="0";
			}else{
				document.getElementById("emprInPrincipal").checked=false;
				document.getElementById("listaIdNiveCdNivelacesso".value).value="0";
			}

		}
		
		<%}%>
	}
	
	//Est� fun��o para o Google Ghrome 
	
	function sair(){
		window.close();	
	}
	
	
	function salvar(){
		
		var existePrincipal = false;		
		var semNivelAcesso = false;
		var newTxt = "";
		var obj = document.getElementsByName("listaIdEmprCdEmpresa");
		var obj2 = document.getElementsByName("descEmpresa");
		
		var listaIdEmprCdEmpresaAux = new Array();
		
		<%if(mostrarPrincipal){%>
			var obj3 = document.getElementsByName("emprInPrincipal");
			var obj4 = document.getElementsByName("listaIdNiveCdNivelacesso");
		<%}%>
		
		if(obj != null && obj != undefined){
			if(obj.length != null && obj.length != undefined){
						
				/* for(i = 0; i < obj.length; i++ ) {
					obj[i].disabled = false;
				}*/
			
				for (nNode=0;nNode<obj.length;nNode++) {
					if(obj[nNode].checked){
						//Adiciona no ultimo elemento
						listaIdEmprCdEmpresaAux[listaIdEmprCdEmpresaAux.length] = obj[nNode].value;
						
						newTxt+="<input type=hidden name=empresas value=\'" + obj2[nNode].value + "\'></input>";
						<%if(mostrarPrincipal){%>
							if (obj4[nNode].value=="" || obj4[nNode].value=="0") {
								semNivelAcesso = true;
							}
							if(obj3[nNode].checked){
								existePrincipal = true;
							}
						<%}%>
					}
				}
			}else{
				if(obj.checked == true){
					newTxt+="<input type=hidden name=empresas value=\'" + obj2.value + "\'></input>";
					<%if(mostrarPrincipal){%>
						if(obj3.checked){
							existePrincipal = true;
						}
					<%}%>
				}
			}
		}
		window.dialogArguments.top.document.getElementById("divEmpresas").innerHTML=newTxt;
		
		<%if(mostrarPrincipal){%>
			if(newTxt != ''){
				if(existePrincipal==false){
					alert('<bean:message key="prompt.selecionarEmpresaPrincipal" />');
					return false;
				}
				if (semNivelAcesso) {
					alert('<bean:message key="prompt.todasEmpresasSelecionadasDevemTerTipoFuncionarioIndicado" />.');
					return false;
				}
			}
		<%}%>
		
		if(obj != null && obj != undefined){
			if(obj.length != null && obj.length != undefined){
				for(i = 0; i < obj.length; i++ ) {
					obj[i].disabled = false;
				}
			}
		}
		
		<%if(mostrarPrincipal){%>
			if(obj3 != null && obj3 != undefined){
				if(obj3.length != null && obj3.length != undefined){
					for(i = 0; i < obj3.length; i++ ) {
						obj3[i].disabled = false;
					}
				}
			}
			
			if(obj4 != null && obj4 != undefined){
				if(obj4.length != null && obj4.length != undefined){
					for(i = 0; i < obj4.length; i++ ) {
						obj4[i].disabled = false;
					}
				}
			}
		<%}%>
		
		<% if (mostrarPrincipal) { %>
			//Action Center: 16402 - 25/03/2013 - Carlos Nunes
	        var idEmpresaFuncionarioEmEdicao = window.dialogArguments.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.value;
	
			if(obj != null && obj != undefined){
				if (obj.length != null && obj.length != undefined){
					for(i = 0; i < obj.length; i++ ) {
						if (obj[i].checked && obj[i].value == idEmpresaFuncionarioEmEdicao) {
							window.dialogArguments.ifrmConteudo.editIframe.document.getElementById("idNiveCdNivelAcesso").value = obj4[i].value;
						}
					}
				}
			}
		<% } %>
		
		
		try{
			window.dialogArguments.ifrmConteudo.editIframe.document.getElementById("listaIdEmprCdEmpresa").value = listaIdEmprCdEmpresaAux;
		}catch(e){
			//alert(e + listaIdEmprCdEmpresaAux);
		}
			
		

		document.administracaoEmpresaForm.acao.value="gravar";
		document.administracaoEmpresaForm.tela.value="ifrmResultEmpresa";
		document.administracaoEmpresaForm.target = ifrmResultEmpresa.name;
		document.administracaoEmpresaForm.submit();
	}

	nLinhaC = new Number(100);

	function addEmpresa(codEmpresa,descEmpresa) {

		nLinhaC = nLinhaC + 1;
	
		var strTxt = "";
		strTxt += "	<table id=\"" + nLinhaC + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr>";
		strTxt += "	        <td class=principalLstParMao width=10%><input name=listaIdEmprCdEmpresa id=listaIdEmprCdEmpresa" + codEmpresa + " onclick=verificaPrincipal(this,nLinhaC) type=checkbox disabled=true value=" + codEmpresa + "></input></td>";
		strTxt += "     	<td class=principalLstPar width=14%>&nbsp;" + codEmpresa + "</td>";
		strTxt += "	        <td class=principalLstPar width=32%>&nbsp;" + descEmpresa + "</td><input name=descEmpresa id=descEmpresa" + nLinhaC + " type=hidden value=\'" + descEmpresa + "\'></input>";
		<%if(mostrarPrincipal){%>
		strTxt += "	        <td class=principalLstParMao align=center width=30%>";
		strTxt += "	        " + cmbNive.replace('<<NIVE_NAME>>','listaIdNiveCdNivelacesso').replace('<<NIVE_ID>>','listaIdNiveCdNivelacesso'+codEmpresa);
		strTxt += "	        </td>";
		strTxt += "	        <td class=principalLstParMao align=center width=14%><input name=emprInPrincipal id=emprInPrincipal" + codEmpresa + " type=radio disabled=true value=" + codEmpresa + "></input></td>";
		<%}%>
		strTxt += "		</tr>";
		strTxt += " </table>";
		
		document.getElementById("lstEmpresas").innerHTML += strTxt;

	}
	
	function inicio(){
		
		var obj = document.getElementsByName("listaIdEmprCdEmpresa");
		var nQuant = 0;
		
		if(obj != null && obj != undefined){
			if(obj.length != null && obj.length != undefined){
				for (nNode=0;nNode<obj.length;nNode++) {
					if(obj[nNode].checked){
						nQuant++;
					}
				}
			}else{
				if(obj.checked == true){
					nQuant++;
				}
			}
		}
		
		if(nQuant == 0){
			if(window.dialogArguments.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.value > 0 ){
				document.getElementById("listaIdEmprCdEmpresa" + window.dialogArguments.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.value).checked=true;
				<%if(mostrarPrincipal){%>
					document.getElementById("emprInPrincipal" + window.dialogArguments.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.value).checked=true;
				<%}%>
			}
		}

		<% if (mostrarPrincipal) { %>
			var objCheck = document.getElementsByName("listaIdEmprCdEmpresa");
			var objPrinc = document.getElementsByName("emprInPrincipal");
			var objNivel = document.getElementsByName("listaIdNiveCdNivelacesso");

			//Action Center: 16402 - 25/03/2013 - Carlos Nunes
	        var idEmpresaFuncionarioEmEdicao = window.dialogArguments.ifrmCmbEmpresa.document.administracaoEmpresaForm.idEmprCdEmpresa.value;
	        
	        var openerWindow = (window.opener) ? window.opener : window.dialogArguments;
			
			if(objCheck != null && objCheck != undefined){
				if(objCheck.length != null && objCheck.length != undefined){
					for(i = 0; i < objCheck.length; i++ ) {
						if (objCheck[i].checked && objCheck[i].value == idEmpresaFuncionarioEmEdicao) {
							objNivel[i].value = window.dialogArguments.ifrmConteudo.editIframe.document.getElementById("idNiveCdNivelAcesso").value;
						}
					}
				}
			}
		<% } %>
		
		
		
		var listaIdEmprCdEmpresaAux;
		try{
			var listaIdEmprCdEmpresaAux = window.dialogArguments.ifrmConteudo.editIframe.document.getElementById("listaIdEmprCdEmpresa").value;
			
			//Corre��o para o Habibs
			if(listaIdEmprCdEmpresaAux.indexOf('[')>-1){
				listaIdEmprCdEmpresaAux = eval(listaIdEmprCdEmpresaAux);
			}else{

				listaIdEmprCdEmpresaAux = listaIdEmprCdEmpresaAux.split(',');
			}
			
			if (listaIdEmprCdEmpresaAux!=null && listaIdEmprCdEmpresaAux!= undefined){
				
				//Se possuir espec, deve desmarcar as op��es do kernel, e setar as que vieram no espec
				var obj = document.getElementsByName("listaIdEmprCdEmpresa");
				
				if(obj != null && obj != undefined){
					if(obj.length != null && obj.length != undefined){
								
						for (nNode=0;nNode<obj.length;nNode++) {
							obj[nNode].checked=false;
						}
					}
				}
				
				for (var i = 0; i < listaIdEmprCdEmpresaAux.length; i++){
					try{
						document.getElementById("listaIdEmprCdEmpresa" + listaIdEmprCdEmpresaAux[i]).checked = true;
					}catch(e){}				
				}
			}			
		}catch(e){
			//alert(e + listaIdEmprCdEmpresaAux);
		}		
		
		

	}
	
</script>


</head>

<body class="principalBgrPage" onload="inicio()" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">

<html:form styleId="administracaoEmpresaForm" action="/AdministracaoEmpresa.do">

	<html:hidden property="tela"/>
	<html:hidden property="acao"/>
		
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	
    <tr> 
      	<td width="1007" colspan="2"> 
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          		<tr> 
            		<td class="principalPstQuadroGiant" height="17" width="166">&nbsp;<bean:message key="prompt.empresasCadastradas"/></td>
            		<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            		<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          		</tr>
        	</table>
      	</td>
    </tr>
    
    <tr> 
      	<td width="100%" align="center" class="principalBgrQuadro" valign="top" height="200"> 
      	
			<table width="99%" border="0" cellspacing="0" cellpadding="0">
		     	<tr>
                 	<td class="EspacoPqn">&nbsp;</td>
              	</tr>
		 		<tr>
		 			<td class="principalLstCab" width="10%">&nbsp;<bean:message key="prompt.associar"/></td>
					<td class="principalLstCab" width="14%">&nbsp;<bean:message key="prompt.codigo"/></td>
					<td class="principalLstCab" width="32%">&nbsp;<bean:message key="prompt.empresa"/></td>
					<%if(mostrarPrincipal){%>
					<td class="principalLstCab" width="30%">&nbsp;<bean:message key="prompt.tipo_de_funcionario"/></td>
					<td class="principalLstCab" width="14%" align="center">&nbsp;<bean:message key="prompt.principal"/></td>
					<%}%>
		  		</tr>
			</table>				  
			<table width="720px" border="0" cellspacing="0" cellpadding="0" height="200" class="principalBordaQuadro">
				<tr> 
					<td valign="top"> 
						<div id="lstEmpresas" style="position:absolute; width:715px; height:195px; z-index:4; overflow:auto">
								
							<logic:present name="csCdtbEmpresaEmprVector">
								<logic:iterate id="ccttrtVector" name="csCdtbEmpresaEmprVector" indexId="sequencia">
									<script language="JavaScript">
										addEmpresa('<bean:write name="ccttrtVector" property="idEmprCdEmpresa" />','<bean:write name="ccttrtVector" property="emprDsEmpresa" />'); 
									</script>
								</logic:iterate>
							</logic:present>
							
						</div> 
					</td>					
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalLabel">&nbsp;</td>
				</tr>
			</table>
			
		</td>
		
      	<td width="4" height="250"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="250"></td>
      		
	</tr>
    
	<tr>
		<td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr>
	
</table>

<table border="0" cellspacing="0" cellpadding="4" align="right">
	<tr> 
		<td> 
			<div align="right"></div>
			<img src="webFiles/images/botoes/confirmaEdicao.gif" width="20" height="20" border="0" title="<bean:message key='prompt.confirm'/>" onClick="salvar()" class="geralCursoHand">
			<img src="webFiles/images/botoes/out.gif" width="20" height="20" border="0" title="<bean:message key='prompt.sair'/>" onClick="javascript:window.close()" class="geralCursoHand">
		</td>
    </tr>
</table>

<iframe id=ifrmResult name="ifrmResultEmpresa" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="0" height="0"></iframe>


<logic:present name="csCdtbEmpresaEmprAssocVector">
	<logic:iterate id="ccttrtVector" name="csCdtbEmpresaEmprAssocVector" indexId="sequencia">
		<script language="JavaScript">
			try{
				document.getElementById("listaIdEmprCdEmpresa" + '<bean:write name="ccttrtVector" property="idEmprCdEmpresa" />').checked = true;
			}catch(e){}
		</script>
	</logic:iterate>
</logic:present>

<logic:present name="csCdtbEmpresaEmprPermVector">
	<logic:iterate id="ccttrtVector" name="csCdtbEmpresaEmprPermVector" indexId="sequencia">
		<script language="JavaScript">
			document.getElementById("listaIdEmprCdEmpresa" + '<bean:write name="ccttrtVector" property="idEmprCdEmpresa" />').disabled = false;
			<%if(mostrarPrincipal){%>
				document.getElementById("listaIdNiveCdNivelacesso" + '<bean:write name="ccttrtVector" property="idEmprCdEmpresa" />').disabled = false;
				document.getElementById("emprInPrincipal" + '<bean:write name="ccttrtVector" property="idEmprCdEmpresa" />').disabled = false;
			<%}%>
		</script>
	</logic:iterate>
</logic:present>

<%
	if(mostrarPrincipal){
		if(idEmprCdEmpresaPrincipal>0){
%>

	<script>
		try{
			document.getElementById("emprInPrincipal" + '<%=idEmprCdEmpresaPrincipal%>').checked = true;
		}catch(e){}
	</script>

<% } } %>

<% 	if (mostrarPrincipal) { 
		if(request.getSession().getAttribute("listaIdNiveCdNivelacesso")!= null){
			try {
				long[] arrayNivelacesso = (long[])request.getSession().getAttribute("listaIdNiveCdNivelacesso");
				Vector csCdtbEmpresaEmprAssocVector = (Vector)request.getSession().getAttribute("csCdtbEmpresaEmprAssocVector");
				if (arrayNivelacesso.length>0) {
					for (int i = 0; i<csCdtbEmpresaEmprAssocVector.size(); i++) {
						long idEmprVec = ((MultiEmpresaVo)csCdtbEmpresaEmprAssocVector.get(i)).getIdEmprCdEmpresa();
						%><script>
						     if(document.getElementById("listaIdNiveCdNivelacesso<%=idEmprVec%>") != null)
						     {
						        document.getElementById("listaIdNiveCdNivelacesso<%=idEmprVec%>").value = '<%=arrayNivelacesso[i]%>';
						     }
						  </script><%
					}
				}
			}catch(Exception e){}
		}
	}
%>

</html:form>
</body>
</html:html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>