<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%> 

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 


// Funcoes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case ifrmParametrosBotao:
			MM_showHideLayers('ifrmParametros','','show','ifrmTexto','','hide');
			SetClassFolder('tdParametros','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdTexto','principalPstQuadroLinkNormal');
			break;
		case ifrmTextoBotao:
			MM_showHideLayers('ifrmTexto','','show','ifrmParametros','','hide');
			SetClassFolder('tdParametros','principalPstQuadroLinkNormal');
			SetClassFolder('tdTexto','principalPstQuadroLinkSelecionado');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function habilitaDimensao(bHabilita) {
	if (bHabilita) {
		document.administracaoCsCdtbBotaoBotaForm.botaDsDimensao.disabled = false;
	}
	else {
		document.administracaoCsCdtbBotaoBotaForm.botaDsDimensao.value = "";
		document.administracaoCsCdtbBotaoBotaForm.botaDsDimensao.disabled = true;
	}
}	

function habilitaNonModal() {

	if ((document.administracaoCsCdtbBotaoBotaForm.botaInLocal.value=='F') || (document.administracaoCsCdtbBotaoBotaForm.botaInLocal.value=='S')) {
		document.getElementById('tdNonModal').style.visibility = 'visible';
	} else {
		document.getElementById('tdNonModal').style.visibility = 'hidden';
	}

}

function desabilitaCamposBotao(){
	document.administracaoCsCdtbBotaoBotaForm.botaDsBotao.disabled= true;	
	document.administracaoCsCdtbBotaoBotaForm.botaDsIcone.disabled= true;
	document.administracaoCsCdtbBotaoBotaForm.botaDsLink.disabled= true;	
	document.administracaoCsCdtbBotaoBotaForm.botaDhInativo.disabled= true;
	document.administracaoCsCdtbBotaoBotaForm.botaInModal.disabled= true;
	document.administracaoCsCdtbBotaoBotaForm.botaDsDimensao.disabled= true;
	document.administracaoCsCdtbBotaoBotaForm.botaInLocal.disabled= true;
}

function obtemListaDosParametros(){

	objEMail = ifrmParametrosBotao.document.administracaoCsCdtbBotaoBotaForm.idPaboCdParametrobotaoLista;
	var strTxt="";
	if(objEMail.length != undefined){
		for (nNode=0;nNode<objEMail.length;nNode++) {
		  		strTxt += "  <input type=\"hidden\" name=\"idPaboCdParametrobotaoLista\" value=\"" + objEMail[nNode].value + "\" > ";
		}
	}else{
		strTxt += "  <input type=\"hidden\" name=\"idPaboCdParametrobotaoLista\" value=\"" + "0" + "\" > ";
	}
	
	objEMail2 = ifrmParametrosBotao.document.administracaoCsCdtbBotaoBotaForm.paboInObrigatorioLista;
	if(objEMail2 != undefined){ 
	
		/*	Caso a lista tenha apenas um par�metro e obtido apenas o valor deste par�metro, 
			caso contr�rio ele percorre a lista.
		*/
		if(ifrmParametrosBotao.document.administracaoCsCdtbBotaoBotaForm.paboInObrigatorioLista.length == undefined){
			if(objEMail2.checked){
				strTxt += "  <input type=\"hidden\" name=\"paboInObrigatorioLista\" value=\"" + objEMail2.value + "\" > ";
			}
		}
		else
		{
			for (nNode=0;nNode<objEMail2.length;nNode++) {
				if(objEMail2[nNode].checked){
					  strTxt += "  <input type=\"hidden\" name=\"paboInObrigatorioLista\" value=\"" + objEMail2[nNode].value + "\" > ";
				}
			}
		}
	}else{
	 	strTxt += "  <input type=\"hidden\" name=\"paboInObrigatorioLista\" value=\"" + "0" + "\" > ";
	}
	
	lstParametros.innerHTML = lstParametros.innerHTML + strTxt;
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	//Chamado: 86366 - Carlos Nunes - 17/01/2013
	setaChavePrimaria('<bean:write name="administracaoCsCdtbBotaoBotaForm" property="idBotaCdBotao" />');
}

		

</script>

	
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbBotaoBotaForm" action="/AdministracaoCsCdtbBotaoBota.do" enctype="multipart/form-data">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>
	<html:hidden property="botaDsIconeAntigo"/>	
	<html:hidden property="idBotaCdBotao"/>
	<html:hidden property="botaTxTexto"/>
	<input type="hidden" name="limparSessao" value="false"></input>
	
	<div id="lstParametros"></div>
	
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idBotaCdBotao" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="25%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="botaDsBotao" styleClass="text" maxlength="60" /> 
          </td>
          <td width="25%">&nbsp;</td>
        </tr>
        
        <!--Verifica se o campo sequencia deve ser mostrado na tela -->
        
        	<tr> 
          		<td width="20%" align="right" class="principalLabel"><bean:message key="prompt.sequencia"/><!-- ## --> 
            		<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          		<td width="10%"> 
            		<html:text property="botaNrSequencia" styleClass="text" maxlength="3" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;" />
          		</td>
          		<td width="28%">&nbsp;</td>
          		<td width="25%">&nbsp;</td>
        	</tr>
        
     
        <tr> 
          <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.icone"/> (25kb max)<!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:file property="botaDsIcone" styleClass="text" maxlength="60" />
          </td>
          <td width="25%">
            &nbsp;
               <img src="AdministracaoCsCdtbBotaoBota.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%=MAConstantes.TELA_EDIT_CS_CDTB_BOTAO_IMAGE%>&idBotaCdBotao=<bean:write name="administracaoCsCdtbBotaoBotaForm" property="idBotaCdBotao" />" width="22" height="22">
          </td>
        </tr>
        <tr> 
          <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.link"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="botaDsLink" styleClass="text" style="width:352px" maxlength="2000" />
          </td>
          <td width="25%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.local"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2" class="principalLabel"> 
          	
			<html:select property="botaInLocal" styleClass="principalObjForm" onchange="habilitaNonModal();"> 
				
			  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option>
			  <html:option value="M"> <bean:message key="prompt.manifestacaofixo"/> </html:option>
			  <html:option value="F"> <bean:message key="prompt.funcoesextra"/> </html:option>
			  <html:option value="P"> <bean:message key="prompt.pessoa"/> </html:option>
			  <html:option value="A"> <bean:message key="prompt.cadastro"/> </html:option>
			  <html:option value="R"> <bean:message key="prompt.ressarcimento"/> </html:option>
			  <html:option value="T"> <bean:message key="prompt.produto"/> </html:option>
			  
			  <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SFA,request).equals("S")) {	%>
				  <html:option value="S"> <bean:message key="prompt.funcoesextraSfa"/> </html:option>  
				  <html:option value="C"> <bean:message key="prompt.pessoaCliente"/> </html:option>
				  <html:option value="L"> <bean:message key="prompt.pessoaLead"/> </html:option>
				  <html:option value="O"> <bean:message key="prompt.oportunidade"/> </html:option>
			  <%} %>
			  
			  <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MOBILE,request).equals("S")) {	%>
				  <html:option value="1"> <bean:message key="prompt.MobileFuncoesExtra"/> </html:option>  
				  <html:option value="2"> <bean:message key="prompt.MobilePessoa"/> </html:option>
			  <%} %>
			</html:select>     
          	
          </td>
          <td width="25%">&nbsp;</td>
        </tr>
        
         
        	<tr>
        	 	<td width="20%" align="right" class="principalLabel"><bean:message key="prompt.atendimentoPadrao"/><!-- ## --> 
            	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          		<td colspan="2" class="principalLabel"> 
          			<html:select property="atendimentoPadrao" styleClass="principalObjForm">
          		  		<html:option value=""><bean:message key="prompt.selecione_uma_opcao"/></html:option>
                  		<logic:present name="vectorAtendimento">
                  			<html:options collection="vectorAtendimento" property="idAtpdCdAtendpadrao" labelProperty="atpdDsAtendpadrao"/>
                  		</logic:present>
            		</html:select>     
          		</td>
          		<td width="25%">&nbsp;</td>
        	</tr>
        	<!-- Chamado 91034 - 19/09/2013 - Jaider Alba -->
        	<tr>
        	 	<td width="20%" align="right" class="principalLabel"><bean:message key="prompt.grupoBotao"/> 
            	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          		<td colspan="2" class="principalLabel"> 
          			<html:select property="idGrboCdGrupoBotao" styleClass="principalObjForm">
          		  		<html:option value=""><bean:message key="prompt.selecione_uma_opcao"/></html:option>
                  		<logic:present name="vectorGrupoBotao">
                  			<html:options collection="vectorGrupoBotao" 
                  						  property="field(ID_GRBO_CD_GRUPOBOTAO)" 
                  						  labelProperty="field(GRBO_DS_GRUPOBOTAO)" />
                  		</logic:present>
            		</html:select>     
          		</td>
          		<td width="25%">&nbsp;</td>
        	</tr>
         

		<tr> 
			<td width="20%" align="right" class="principalLabel"><bean:message key="prompt.tipo"/><!-- ## --> 
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
			</td>
			<td colspan="2" class="principalLabel"> 

				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
		        	<tr> 
	          			<td width="20%" align="left" class="principalLabel"> 
					        <html:radio value="N" property="botaInModal" onclick="habilitaDimensao(false);"/> 
							<bean:message key="prompt.abas"/>
						</td>
	          			<td width="20%" align="left" class="principalLabel"> 
				    	    <html:radio value="S" property="botaInModal" onclick="habilitaDimensao(true);"/>
							<bean:message key="prompt.modal"/>
						</td>
	          			<td width="60%" align="left" class="principalLabel" id="tdNonModal" name="tdNonModal"> 
				        	<html:radio value="O" property="botaInModal" onclick="habilitaDimensao(true);"/>
							<bean:message key="prompt.nonModal"/>
						</td>
					</tr>
				</table>

			</td>
          	<td width="25%">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.dimensao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
          	<logic:notEqual name="administracaoCsCdtbBotaoBotaForm" property="botaInModal" value="N">
              <html:text property="botaDsDimensao" styleClass="text" style="width: 320px" maxlength="1000" />
            </logic:notEqual>
            <logic:equal name="administracaoCsCdtbBotaoBotaForm" property="botaInModal" value="N">
              <html:text property="botaDsDimensao" styleClass="text" style="width: 320px" maxlength="1000" disabled="true"/> 
            </logic:equal>
		  </td>
          <td width="25%">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="20%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="botaDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
                
              </tr>
            </table>
          </td>
          <td width="25%">&nbsp;</td>
        </tr>
        
		<tr>
			
			<td colspan="8">
			<table width="650px" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="principalPstQuadroLinkSelecionado"
							id="tdParametros" name="tdParametros"
							onClick="AtivarPasta(ifrmParametrosBotao);">
						<bean:message key="prompt.parametros" /><!-- ## --></td>
						<td class="principalPstQuadroLinkNormal" id="tdTexto"
							name="tdTexto"
							onClick="AtivarPasta(ifrmTextoBotao);">
						<bean:message key="prompt.procedimento" /><!-- ## --></td>
					</tr>
				</table>
			</td>
			</tr>
			<tr>
			<td class="principalBordaQuadro" height="135px" valign="top">
       			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
					  <td>
						 <div name="ifrmParametros" id="ifrmParametros" style="position: absolute; width: 640; height: 130px; z-index: 8; visibility: visible"> 
								<iframe id=ifrmParametrosBotao name="ifrmParametrosBotao"
											src="AdministracaoCsCdtbBotaoBota.do?tela=ifrmParametrosBotao&acao=visualizar&idBotaCdBotao=<bean:write name="baseForm" property="idBotaCdBotao" />"
											width="635px" height="130px" scrolling="no" frameborder="0"
											marginwidth="0" marginheight="0"></iframe>
						</div>
						 <div name="ifrmTexto" id="ifrmTexto" style="position: absolute; width: 640px; height: 130px; z-index: 7; visibility: visible"> 
								<iframe id=ifrmTextoBotao name="ifrmTextoBotao"
											src="AdministracaoCsCdtbBotaoBota.do?tela=ifrmTextoBotao&acao=visualizar&idBotaCdBotao=<bean:write name="baseForm" property="idBotaCdBotao" />"
											width="635px" height="130px" scrolling="no" frameborder="0"
											marginwidth="0" marginheight="0"></iframe>
						</div>
					</td>					
				</tr>		        
     		</table>
     		</td>
     		</tr>
     		</table>
		</td>
		<td width="100" height="5" colspan="3">&nbsp;</td>
	</tr>
</table>



</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposBotao();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_BOTAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbBotaoBotaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_BOTAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposBotao();
			}
			document.administracaoCsCdtbBotaoBotaForm.idBotaCdBotao.disabled= false;
			document.administracaoCsCdtbBotaoBotaForm.idBotaCdBotao.value= '';
			document.administracaoCsCdtbBotaoBotaForm.idBotaCdBotao.disabled= true;
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_BOTAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_BOTAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbBotaoBotaForm.imgGravar);	
				desabilitaCamposBotao();
			}
		</script>
</logic:equal>

</html>

<script>
	habilitaNonModal();
	try{document.administracaoCsCdtbBotaoBotaForm.botaDsBotao.focus();}
	catch(e){}
</script>