<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<%
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	

	long idEmpresa = empresaVo.getIdEmprCdEmpresa();
	
	String maxRegistros = "";
	//try{
	//	maxRegistros = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_LIMITE_LINHAS_RESULTADO,idEmpresa);
	//}catch(Exception e){}
		
	//if(maxRegistros == null || maxRegistros.equals("")){
	//	maxRegistros = "100";
	//}
	
	//pagina��o****************************************
	long numRegTotal=0;
	if (request.getAttribute("csAstbComposicaoCompVector")!=null){
		Vector v = ((java.util.Vector)request.getAttribute("csAstbComposicaoCompVector"));
		if (v.size() > 0){
			numRegTotal = ((CsAstbComposicaoCompVo)v.get(0)).getNumRegTotal();
		}
	}

	long regDe=0;
	long regAte = 0;

	if (request.getParameter("regDe") != null)
		regDe = Long.parseLong((String)request.getParameter("regDe"));
	if (request.getParameter("regAte") != null)
		regAte  = Long.parseLong((String)request.getParameter("regAte"));
	//***************************************
	
%>

<%@page import="java.util.Vector"%>
<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
</script>
</head>

<script language="JavaScript">

	function setaDeAte(){

		if(parent.document.administracaoCsAstbComposicaoCompForm.regDe.value == "0" && parent.document.administracaoCsAstbComposicaoCompForm.regAte.value == "0"){			
			
			parent.document.administracaoCsAstbComposicaoCompForm.regDe.value='<%=regDe%>';
			parent.document.administracaoCsAstbComposicaoCompForm.regAte.value='<%=regAte%>';
			parent.initPaginacao();
		}	
	
	}
	
	function inicio(){
		
		window.top.document.getElementById("LayerAguarde").style.visibility = "hidden";

		try{
			if(parent.editIframe.ifrmCmbProduto.document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.selectedIndex == 0 || 
				parent.editIframe.cmbVariedade.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.selectedIndex == 0){
				
				parent.cancel();
			}
		
			parent.editIframe.lstArqCarga.document.location.href = "AdministracaoCsAstbComposicaoComp.do?tela=administracaoLstCsAstbComposicaoComp&acao=<%=Constantes.ACAO_VISUALIZAR%>"+
				"&idAsnCdAssuntoNivel="+ parent.editIframe.document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value +
				"&idAsn1CdAssuntonivel1="+ parent.editIframe.document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value +
				"&idAsn2CdAssuntonivel2="+ parent.editIframe.document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value;
		
			parent.AtivarPasta(parent.admIframe);
		}
		catch(x){}
		
		parent.setPaginacao(<%=regDe%>,<%=regAte%>);
		parent.atualizaPaginacao(<%=numRegTotal%>);
			
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form styleId="editCsAstbComposicaoCompForm" action="/AdministracaoCsAstbComposicaoComp.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="erro"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idCompCdSequencial"/>
	<html:hidden property="idAsn1CdAssuntonivel1" />
	<html:hidden property="idAsn2CdAssuntonivel2" />
	<html:hidden property="idAsnCdAssuntoNivel" />
	<html:hidden property="compDhElaboracao" />
	<html:hidden property="compFonteRetirada"/>
	<html:hidden property="compFonteRetiradaOld"/>

<script>
	var possuiRegistros=false;
	var totalRegistros=0;
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csAstbComposicaoCompVector" indexId="sequencia">
  <script>
  		possuiRegistros=true;
  		totalRegistros=<%=sequencia%>;
  </script>
  <tr> 
  
    <td width="3%" class="principalLstParMao"> 
    	<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMACOMP_IDCO.xml','<bean:write name="ccttrtVector" property="idCompCdSequencial" />')">
    </td>
  
    <td class="principalLstParMao" width="6%" colspan="1" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idCompCdSequencial" />')"> 
      &nbsp;<bean:write name="ccttrtVector" property="idCompCdSequencial" /> 
    </td>
    
    <td class="principalLstParMao" width="20%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idCompCdSequencial" />')"> 
      &nbsp;<script>acronym('<bean:write name="ccttrtVector" property="prasDsProdutoAssunto" />', 20);</script></td>
    <td class="principalLstParMao" align="left" width="30%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idCompCdSequencial" />')"> 
      &nbsp;&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="toinDsTopicoinformacao" />', 20);</script> </td>
	<td class="principalLstParMao" align="left" width="30%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idCompCdSequencial" />')"> 
      &nbsp;&nbsp;&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="tpinDsTipoinformacao" />', 20);</script> </td>
	<td class="principalLstParMao" align="left" width="10%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idCompCdSequencial" />')"> 
      &nbsp;&nbsp;&nbsp;&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="dhInativo" />', 10);</script> </td>
    </td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		if(parent.error != undefined && parent.error != null)
			parent.printError(divErro.innerHTML);
	}
	//if((totalRegistros+1)>=<%=maxRegistros%>){
	//	alert('<bean:message key="prompt.alert_limiteRegistros"/>');
	//}	
</script>
</html:form>
</body>
</html>