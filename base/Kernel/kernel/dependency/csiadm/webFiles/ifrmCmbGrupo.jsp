<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>
function atualizaTipoManif() {

	//Chamado 70392 - Vinicius - Incluido check e hidden tpmaDhInativo para filtrar TPMA inativos
	if(parent.document.getElementById("chkTipoManifInativo").checked){
		parent.ifrmCmbTipoManif.administracaoCsCdtbAtendpadraoAtpaForm.tpmaDhInativo.value = true;
		administracaoCsCdtbAtendpadraoAtpaForm.tpmaDhInativo.value = true;
	}else{
		parent.ifrmCmbTipoManif.administracaoCsCdtbAtendpadraoAtpaForm.tpmaDhInativo.value = false;
		administracaoCsCdtbAtendpadraoAtpaForm.tpmaDhInativo.value = false;
	}
	
	document.administracaoCsCdtbAtendpadraoAtpaForm.target = parent.ifrmCmbTipoManif.name;
	document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%= MAConstantes.TELA_CMB_TIPOMANIF %>';
	document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
}

function iniciaTela(){
	
	if(document.administracaoCsCdtbAtendpadraoAtpaForm.idGrmaCdGrupomanifestacao.length == 2){
		document.administracaoCsCdtbAtendpadraoAtpaForm.idGrmaCdGrupomanifestacao.selectedIndex = 1;
	}
	
	setTimeout("atualizaTipoManif()",500);
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">

<html:form action="/AdministracaoCsCdtbAtendpadraoAtpa.do" styleId="administracaoCsCdtbAtendpadraoAtpaForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="idMatpCdManiftipo" />
	<html:hidden property="idAsnCdAssuntoNivel" />
    <html:hidden property="idAsn2CdAssuntonivel2" />
	<html:hidden property="idTpmaCdTpmanifestacao" />
	
	<html:hidden property="tpmaDhInativo"/>
	
	<html:select property="idGrmaCdGrupomanifestacao" styleClass="principalObjForm" onchange="atualizaTipoManif();">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		<html:options collection="combo" property="idGrmaCdGrupoManifestacao" labelProperty="grmaDsGrupoManifestacao"/>
	</html:select>
</html:form>
</body>
</html>
