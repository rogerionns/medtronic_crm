<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.form.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";

String LIMITE_CARACTERES="300000";

%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<html>
<head>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function setFunction(cRetorno, campo){
	if(campo=="document.administracaoCsCdtbDocumentoDocuForm.docuTxDocumento"){
		document.getElementById("divConteudoDocumento").innerHTML = cRetorno;
		document.administracaoCsCdtbDocumentoDocuForm.docuTxDocumento.value=cRetorno;
	}
}

function desabilitaCamposDocumento(){
	setPermissaoImageDisable('', document.administracaoCsCdtbDocumentoDocuForm.imgEditor);	
	document.administracaoCsCdtbDocumentoDocuForm.docuDsDocumento.disabled= true;
	document.administracaoCsCdtbDocumentoDocuForm.docuDhInativo.disabled= true;	
	document.administracaoCsCdtbDocumentoDocuForm.docuTxDocumento.disabled= true;
	document.administracaoCsCdtbDocumentoDocuForm.docuInTipoDocumento[0].disabled=true;
	document.administracaoCsCdtbDocumentoDocuForm.docuInTipoDocumento[1].disabled=true;
	document.administracaoCsCdtbDocumentoDocuForm.docuInTipoDocumento[2].disabled=true;
	document.administracaoCsCdtbDocumentoDocuForm.docuInTipoDocumento[3].disabled=true;
}

function controlaTxtLivre(){
	if(document.getElementById("chkTextoLivre").checked){
		document.getElementById("divConteudoDocumento").style.display = "none";
		document.getElementById("divTextoLivre").style.display = "block";
		document.getElementsByName("txtDescricaoLivre")[0].value = document.administracaoCsCdtbDocumentoDocuForm.docuTxDocumento.value;
	}else{
		document.getElementById("divTextoLivre").style.display = "none";
		document.getElementById("divVisualizacao").style.display = "none";
		document.getElementById("divConteudoDocumento").style.display = "block";
		document.getElementById("divConteudoDocumento").innerHTML = document.getElementsByName("txtDescricaoLivre")[0].value;

		
		document.administracaoCsCdtbDocumentoDocuForm.docuTxDocumento.value=document.getElementsByName("txtDescricaoLivre")[0].value;
	}
}

function visualizaHtml(){
	document.getElementById("divVisuzalizaHtml").innerHTML = document.getElementsByName("txtDescricaoLivre")[0].value;
	document.getElementById("divTextoLivre").style.display = "none";
	document.getElementById("divVisualizacao").style.display = "block";

	trataLinks(document.getElementById("divVisuzalizaHtml"));
}


// jvarandas - 16/07/2010
// Fun��o criada para tratar os links do documento exibido na edi��o.
// Se o link possui campos especiais, ele n�o pode ser exibido, pois poderia ser aberto com erros
function trataLinks(obj) {
	var links = obj.getElementsByTagName("a");
	for(var i=0; i<links.length; i++) {
		if(links[i].href.indexOf("[[") > -1) {
			links[i].href = "javascript:linkNaoDisponivel()";
		}	
	}
}

function linkNaoDisponivel() {
	alert("<bean:message key="prompt.documento.linkNaoDisponivel" />");
}

function voltar(){
	if(document.getElementById("chkTextoLivre").checked){
		document.getElementById("divTextoLivre").style.display = "block";
		document.getElementById("divVisualizacao").style.display = "none";
	}else{
		document.getElementById("divVisualizacao").style.display = "none";
		document.getElementById("divTextoLivre").style.display = "none";
		document.getElementById("divConteudoDocumento").style.display = "block";
	}
}

function inicio(){
	document.getElementsByName("txtDescricaoLivre")[0].value = administracaoCsCdtbDocumentoDocuForm.docuTxDocumento.value;
	setaChavePrimaria(administracaoCsCdtbDocumentoDocuForm.idDocuCdDocumento.value);
}

</script>

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')" style="overflow: hidden;">

<html:form styleId="administracaoCsCdtbDocumentoDocuForm" action="/AdministracaoCsCdtbDocumentoDocu.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<html:hidden property="docuTxDocumento"  />
	<br>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="13%"> <html:text property="idDocuCdDocumento" styleClass="text" disabled="true" /> 
    </td>
    <td colspan="2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        <html:radio value="P" property="docuInTipoDocumento"/> 
        <bean:message key="prompt.padrao"/> 
        <html:radio value="C" property="docuInTipoDocumento"/> 
        <bean:message key="prompt.comeco"/> 
        <html:radio value="M" property="docuInTipoDocumento"/> 
        <bean:message key="prompt.meio"/> 
        <html:radio value="F" property="docuInTipoDocumento"/> 
        <bean:message key="prompt.fim"/> 
	</td>
  </tr>
  <tr> 
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="docuDsDocumento" styleClass="text" maxlength="60" style="width: 473px;" /> 
    </td>
  </tr>
  
  <tr height="17">
  	<td width="17%" >&nbsp;</td>
  	<td class="principalLabel">
  		&nbsp;
  	</td>
  	<td align="right"><input type="checkbox" name="chkTextoLivre" id="chkTextoLivre" onclick="controlaTxtLivre()"><bean:message key="prompt.textoLivre"/></input></td>
  </tr>
  
  <tr>
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.grupoDocumento"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
		<html:select property="csCdtbGrupoDocumentoGrdoVo.idGrdoCdGrupoDocumento" styleClass="principalObjForm">
			<html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
			<logic:present name="csCdtbGrupoDocumentoGrdoVector">
				<html:options collection="csCdtbGrupoDocumentoGrdoVector" property="idGrdoCdGrupoDocumento" labelProperty="grdoDsGrupoDocumento"/>
			</logic:present>
		</html:select>
    </td>
  </tr>
  
  <tr>
    <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.AssuntoTitulo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
		<html:text property="docuDsTitulo" styleClass="principalObjForm" maxlength="200" style="width: 473px;" /> 
    </td>
  </tr>
  
  <tr> 
    <td width="17%" align="right" class="principalLabel" height="220"><bean:message key="prompt.texto"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2" height="200" valign="top"> <br>
      <div id="divConteudoDocumento" style="overflow: auto; width:460px; height: 200px; background-color: #FFFFFF; border: 1px none #000000; display: block;"> 
        <script>
    	document.write(document.administracaoCsCdtbDocumentoDocuForm.docuTxDocumento.value);

    	trataLinks(document.getElementById('divConteudoDocumento'));
       </script>
      </div>
      
      
      
		<div id="divTextoLivre" class= "principalBgrPageIFRM" style="width:80px; height:200px; display:none;" >
			<table width="100%">
				<tr>
					<td width="94%">
						<textarea filter="false" style="width:440px;" id="txtDescricaoLivre" name="txtDescricaoLivre" styleClass="principalObjForm3D" rows="10" cols="60"></textarea>   	
					</td>
					
					<td width="6%">
						<img src="webFiles/images/botoes/lupa.gif" name="imgEditor" width="21" height="20" class="geralCursoHand" border="0" title="<bean:message key='prompt.visualizarComoHtml'/>" onClick="visualizaHtml()"> 
					</td>
				</tr>
			</table>
		</div>
		
		<div id="divVisualizacao" style="display: none;">
			<div id="divVisuzalizaHtml" style="float: left; width:440px; height:190px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; overflow:auto;" >
		
			</div><br/><br/><br/><br/><br/><br/><br/><br/>
			<div id="divBotaoRetornar" class="principalBgrPageIFRM" style="float: left;">
				<img src="webFiles/images/botoes/bt_voltar.gif" name="imgEditor" width="60" height="20" class="geralCursoHand" align="middle;" border="0" title="<bean:message key='prompt.voltar'/>" onClick="voltar()"> 
			</div>
		</div>
    </td>
    <td width="5%" height="220">&nbsp;</td>
    <td width="5%"> 
      <img src="webFiles/images/botoes/bt_CriarCarta.gif" name="imgEditor" width="25" height="22" class="geralCursoHand" border="0" title="<bean:message key="prompt.tituloCorresp"/>" onClick="MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=document.administracaoCsCdtbDocumentoDocuForm.docuTxDocumento&carta=true&tamanho=<%=LIMITE_CARACTERES%>','Documento','width=850,height=494,top=0,left=0')"> 
    </td>	
  </tr>
  <tr> 
    <td width="17%">&nbsp;</td>
    <td colspan="3">&nbsp; </td>
  </tr>
  <tr> 
    <td width="17%">&nbsp;</td>
    <td width="13%">&nbsp;</td>
    <td class="principalLabel" width="57%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="83%"> <html:checkbox value="true" property="docuDhInativo"/> 
            <!-- @@ -->
          </td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/> 
            <!-- ## -->
          </td>
        </tr>
      </table>
    </td>
    <td width="13%">&nbsp;</td>
  </tr>
</table>

<script>
	document.administracaoCsCdtbDocumentoDocuForm.docuTxDocumento.readOnly = true;
</script>
</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposDocumento();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DOCUMENTO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbDocumentoDocuForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DOCUMENTO_INCLUSAO_CHAVE%>')){
				desabilitaCamposDocumento();
			}
			document.administracaoCsCdtbDocumentoDocuForm.idDocuCdDocumento.disabled= false;
			document.administracaoCsCdtbDocumentoDocuForm.idDocuCdDocumento.value= '';
			document.administracaoCsCdtbDocumentoDocuForm.idDocuCdDocumento.disabled= true;
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DOCUMENTO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_DOCUMENTO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbDocumentoDocuForm.imgGravar);	
				desabilitaCamposDocumento();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbDocumentoDocuForm.docuDsDocumento.focus();}
	catch(e){}
</script>