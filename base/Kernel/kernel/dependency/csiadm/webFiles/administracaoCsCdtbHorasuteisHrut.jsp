<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsCdtbHorasuteisHrutForm" action="/AdministracaoCsCdtbHorasuteisHrut.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="csCdtbHorasuteisHrutVo.idHrutCdHorasuteis"/>
	<html:hidden property="csCdtbHorasuteisHrutVo.hrutNrSequencia" />

<script>var possuiRegistros=false;</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="cagaaVector" name="csCdtbHorasuteisHrutVector">
  <script>possuiRegistros=true;</script>
  <tr> 
  
    <td width="5%" class="principalLstPar"> 
      &nbsp; &nbsp; <bean:write name="cagaaVector" property="idHrutCdHorasuteis" /> 
    </td>
    
    <td class="principalLstParMao" width="3%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="idHrutCdHorasuteis" />',0)"> 
       &nbsp;
    </td>
    
    <td class="principalLstParMao" align="left" width="38%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="idHrutCdHorasuteis" />',0)"> 
     &nbsp;
      <logic:equal name="cagaaVector" property="idHrutCdHorasuteis" value="1">
      	<bean:message key="prompt.domingo" />
      </logic:equal>
      <logic:equal name="cagaaVector" property="idHrutCdHorasuteis" value="2">
      	<bean:message key="prompt.segunda" />
      </logic:equal>
      <logic:equal name="cagaaVector" property="idHrutCdHorasuteis" value="3">
      	<bean:message key="prompt.terca" />
      </logic:equal>
      <logic:equal name="cagaaVector" property="idHrutCdHorasuteis" value="4">
      	<bean:message key="prompt.quarta" />
      </logic:equal>
      <logic:equal name="cagaaVector" property="idHrutCdHorasuteis" value="5">
      	<bean:message key="prompt.quinta" />
      </logic:equal>
      <logic:equal name="cagaaVector" property="idHrutCdHorasuteis" value="6">
      	<bean:message key="prompt.sexta" />
      </logic:equal>
      <logic:equal name="cagaaVector" property="idHrutCdHorasuteis" value="7">
      	<bean:message key="prompt.sabado" />
      </logic:equal>
      
    </td>
    
    <td class="principalLstParMao" align="left" width="38%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="idHrutCdHorasuteis" />',0)"> 
     &nbsp;
     	<logic:equal name="cagaaVector" property="hrutInTipo" value="M">
     		<bean:message key="prompt.manifestacao"/>
     	</logic:equal>
        <logic:equal name="cagaaVector" property="hrutInTipo" value="C">
     		<bean:message key="prompt.chat"/>
     	</logic:equal>
     	<logic:equal name="cagaaVector" property="hrutInTipo" value="P">
     		<bean:message key="prompt.campanha"/>
     	</logic:equal>
    </td>
    
	<td class="principalLstParMao" align="left" width="23%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="cagaaVector" property="idHrutCdHorasuteis" />',0)"> 
      &nbsp;
    </td>
    
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>