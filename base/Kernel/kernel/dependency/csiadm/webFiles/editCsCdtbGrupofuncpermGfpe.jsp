<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	
function inicio(){
	showError('<%=request.getAttribute("msgerro")%>');
	validarPermissao();
	setaChavePrimaria(administracaoCsCdtbGrupofuncpermGfpeForm['csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm'].value);
}	

function validarPermissao(){
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_GRUPOFUNCIONARIO_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_GRUPOFUNCIONARIO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbGrupofuncpermGfpeForm.imgGravar);
			desabilitaCamposGrupofuncperm();
		}
	</logic:equal>
}

function desabilitaCamposGrupofuncperm(){
	try{
		setPermissaoImageDisable('', document.administracaoCsCdtbGrupofuncpermGfpeForm.imgAvancar);
		setPermissaoImageDisable('', document.administracaoCsCdtbGrupofuncpermGfpeForm.imgVoltar);
		document.administracaoCsCdtbGrupofuncpermGfpeForm["csCdtbGrupofuncpermGfpeVo.gfpeDsGrupofuncperm"].disabled= true;	
		document.administracaoCsCdtbGrupofuncpermGfpeForm["csCdtbFuncionarioFuncVo.funcNmFuncionario"].disabled=true;
		document.administracaoCsCdtbGrupofuncpermGfpeForm["csCdtbAreaAreaVo.idAreaCdArea"].disabled=true;
		document.administracaoCsCdtbGrupofuncpermGfpeForm.inInativo.disabled= true;
		//lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.disabled=true;
		lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.todos.disabled=true;
		//lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.disabled=true;
		lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.todos.disabled=true;
	}
	catch(e){
		setTimeout("desabilitaCamposGrupofuncperm();", 300);
	}
}


function atualizaListaFuncionarios(bOrigemClick) {
	
	if(bOrigemClick){
		document.administracaoCsCdtbGrupofuncpermGfpeForm["csCdtbFuncionarioFuncVo.funcNmFuncionario"].value="";
	}
	
	oldAcao = document.administracaoCsCdtbGrupofuncpermGfpeForm.acao.value;
	oldTela = document.administracaoCsCdtbGrupofuncpermGfpeForm.tela.value;
	
	document.administracaoCsCdtbGrupofuncpermGfpeForm.acao.value = '<%= Constantes.ACAO_CONSULTAR %>';
	document.administracaoCsCdtbGrupofuncpermGfpeForm.tela.value = '<%= MAConstantes.TELA_IFRM_LST_FUNCIONARIOS %>';
	document.administracaoCsCdtbGrupofuncpermGfpeForm.target = lstFuncionarios.name;
	document.administracaoCsCdtbGrupofuncpermGfpeForm.submit();
	
	document.administracaoCsCdtbGrupofuncpermGfpeForm.acao.value = oldAcao;
	document.administracaoCsCdtbGrupofuncpermGfpeForm.tela.value = oldTela;
}	

function moveToRight() {
	selecionou = 0;
	if (lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.length == 0) {
		alert('<bean:message key="prompt.nao_existem_funcionarios_na_lista_para_serem_selecionados"/>');
		return false;
	}
	for (i = 0; i < lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.length; i++) {
		if (lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.options[i] != undefined && lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.options[i] != null && lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.options[i].selected) { 
			selecionou = 1;
			if (!(verificaExistencia(lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.options[i].value, lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo))) {
				alert('<bean:message key="prompt.o_funcionario"/> ' + lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.options[i].text + ' <bean:message key="prompt.ja_foi_selecionado_para_este_grupo"/>');
			}
			else {
				lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.options[lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.length] = new Option(lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.options[i].text, lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.options[i].value);											
				lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.options[i] = null;
				i--;
			}
		}
	}
	lstFuncionarios.atualizaTotal(lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.length);
	lstFuncionariosGrupo.atualizaTotal(lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.length);
	if (selecionou == 0)
		alert('<bean:message key="prompt.Por_favor_selecione_um_funcionario"/>');
}

function moveToLeft() {
	selecionou = 0;
	if (lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.length == 0) {
		alert('<bean:message key="prompt.nao_existem_funcionarios_na_lista_para_serem_selecionados"/>');
		return false;
	}
	for (i = 0; i < lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.length; i++) {
		if (lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.options[i].selected) { 
			selecionou = 1;
			if ((verificaExistencia(lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.options[i].value, lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario))) {
				lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.options[lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.length] = new Option(lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.options[i].text, lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.options[i].value);											
			}
			lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.options[i] = null;
			i--;
		}
	}
	lstFuncionarios.atualizaTotal(lstFuncionarios.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionario.length);
	lstFuncionariosGrupo.atualizaTotal(lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.length);
	if (selecionou == 0)
		alert('<bean:message key="prompt.Por_favor_selecione_um_funcionario"/>');
}

// Verifica se o elemento ja foi copiado
function verificaExistencia(valor, obj) {
	for (j = 0; j < obj.length; j++) {
		if (obj.options[j].value == valor)
			return false;	
	}
	return true;
}

function filtrar(event) {
	if (event.keyCode == 13)
		atualizaListaFuncionarios(false);
}

function getListaFuncionariosGrupo() {
	var html = "";
	for (i = 0; i < lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.length; i++) {
		html += "<input type=\"hidden\" name=\"idFuncCdFuncionarioGrupo\" value=\"" + lstFuncionariosGrupo.document.administracaoCsCdtbGrupofuncpermGfpeForm.idFuncCdFuncionarioGrupo.options[i].value + "\">";
	}
	document.getElementById("listaFuncionarios").innerHTML = html;
}
</script>
<body class= "principalBgrPageIFRM" onload="inicio()">

<html:form styleId="administracaoCsCdtbGrupofuncpermGfpeForm" action="/AdministracaoCsCdtbGrupofuncpermGfpe.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>	
	<html:hidden property="idEmpresa"/>	
	
	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="csCdtbGrupofuncpermGfpeVo.gfpeDsGrupofuncperm" styleClass="text" maxlength="60" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="inInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        
        <!-- INICIO ABA DE FUNCIONARIO -->
        <tr>
        	<td colspan="4">
	        	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td height="254">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"	align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
								<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado" id="funcionario" name="funcionario">
										<bean:message key="prompt.funcionario" />
									</td>
								</tr>
								</table>
							</td>
							<td width="4">
								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
							</td>
						</tr>
						</table>

						<table width="100%" border="0" cellspacing="0" cellpadding="0"	align="center">
						<tr>
							<td valign="top" class="principalBgrQuadro" height="70">
								
								<!-- TABELA DE FORA -->
								<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
								<tr>
									<!-- LISTA DE FUNCIONARIOS -->
									<td width="48%" height="30">
										<table border="0" cellpadding="0" cellspacing="4" width="100%" height="100%">
											<tr>
												<td width="50%" class="principalLabel" height="15">
													<bean:message key="prompt.area"/><br>
													<html:select property="csCdtbAreaAreaVo.idAreaCdArea" styleClass="principalObjForm" onchange="atualizaListaFuncionarios(true);">
														<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
														<logic:present name="vectorArea">
															<html:options collection="vectorArea" property="idAreaCdArea" labelProperty="areaDsArea"/>
														</logic:present>
													</html:select>
												</td>	
												<td width="50%" class="principalLabel" height="15">
													<bean:message key="prompt.nome"/><br>
													<html:text property="csCdtbFuncionarioFuncVo.funcNmFuncionario" styleClass="text" maxlength="60" onkeypress="filtrar(event);"/> 
												</td>	
											</tr>
											<tr>
												<td class="principalLabel" colspan="2" valign="top">
													<iframe name="lstFuncionarios" id="lstFuncionarios"	src="AdministracaoCsCdtbGrupofuncpermGfpe.do?tela=<%= MAConstantes.TELA_IFRM_LST_FUNCIONARIOS %>&acao=<%= Constantes.ACAO_CONSULTAR %>"	width="100%" height="175" scrolling="no" frameborder="0"	marginwidth="0" marginheight="0"></iframe>
												</td>	
											</tr>
										</table>	
									</td>
									<!-- LISTA DE FUNCIONARIOS -->
									
									<!-- BOTOES DE MOVIMENTACAO -->
									<!--Jonathan | Adequa��o para o IE 10-->
									<td valign="top" width="4%" height="100%">
										<table cellpadding="0" cellspacing="0" width="100%" height="80%" border="0" align="center">
										<tr><td height="35%"></td></tr>
			  							<tr>
			  								<td valign="middle" align="center"><img src="webFiles/images/botoes/avancar_unico.gif" name="imgAvancar" width=21px height=18px class=geralCursoHand title="<bean:message key="prompt.adicionaraogrupo"/>" onclick="moveToRight();"></td>
			  							</tr>
			  				  			<tr>
			  								<td valign="middle" align="center"><img src="webFiles/images/botoes/voltar_unico.gif" name="imgVoltar" width=21px height=18px class=geralCursoHand title="<bean:message key="prompt.retirardogrupo"/>" onclick="moveToLeft();"></td>
							  			</tr>
			  							</table>
									</td>
									<!-- BOTOES DE MOVIMENTACAO -->
									
									<!-- LISTA DE FUNCIONARIOS DO GRUPO CADASTRADO -->
									<td height="100">
										<table border="0" cellpadding="0" cellspacing="4" width="100%" height="100%">
										<tr>
											<td class="principalLabel" height="34">
												&nbsp;
											</td>	
										</tr>
										<tr>
											<td class="principalLabel" colspan="2" valign="top">
												<iframe name="lstFuncionariosGrupo" id="lstFuncionariosGrupo" src="AdministracaoCsCdtbGrupofuncpermGfpe.do?tela=<%= MAConstantes.TELA_IFRM_LST_FUNCIONARIOS_GRUPO %>&acao=<%= Constantes.ACAO_CONSULTAR %>&csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm=<bean:write name="administracaoCsCdtbGrupofuncpermGfpeForm" property="csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"/>"	width="100%" height="175" scrolling="no" frameborder="0"	marginwidth="0" marginheight="0"></iframe>
											</td>	
										</tr>
										</table>	
									</td>
									<!-- LISTA DE FUNCIONARIOS DO GRUPO CADASTRADO -->
									
								</tr>
								</table>
								<!-- FINAL DA TABELA DE FORA -->
								
							</td>
							<td width="4" height="230"><img	src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>						
						</tr>
						<tr>
							<td width="100%" height="8" valign="top"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
							<td width="4" height="8" valign="top"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
        <!-- FINAL ABA DE FUNCIONARIO -->
        
      </table>

      <div id="listaFuncionarios"></div>
</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbGrupofuncpermGfpeForm["csCdtbGrupofuncpermGfpeVo.gfpeDsGrupofuncperm"].disabled= true;	
			document.administracaoCsCdtbGrupofuncpermGfpeForm.inInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_GRUPOFUNCIONARIO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbGrupofuncpermGfpeForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_GRUPOFUNCIONARIO_INCLUSAO_CHAVE%>')){
				desabilitaCamposGrupofuncperm();
			}else{
				document.administracaoCsCdtbGrupofuncpermGfpeForm["csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].disabled= false;
				document.administracaoCsCdtbGrupofuncpermGfpeForm["csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].value= '';
				document.administracaoCsCdtbGrupofuncpermGfpeForm["csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].disabled= true;
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbGrupofuncpermGfpeForm["csCdtbGrupofuncpermGfpeVo.gfpeDsGrupofuncperm"].focus();}
	catch(e){}
</script>
