<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script>
function selectAll(obj) {
	for (i = 0; i < administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.length; i++) {
		administracaoCsCatbLinhaCaractProdLcprForm.idCaprCdCaracteristica.options[i].selected = obj.checked;
	}
}	

function atualizaTotal(qtde) {
	document.getElementById("totalCaracteristica").innerHTML = "<bean:message key="prompt.total"/>: " + qtde;
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">

<html:form styleId="administracaoCsCatbLinhaCaractProdLcprForm" action="/AdministracaoCsCatbLinhaCaractProdLcpr.do">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="3%">
			<input name="todos" type="checkbox" onclick="javascript: selectAll(this);">		
		</td>	
		<td class="principalLabel">
			<bean:message key="prompt.todos"/>	
		</td>
	</tr>	
	<tr>
		<td colspan="2">
			<html:select property="idCaprCdCaracteristica" styleClass="principalObjForm" multiple="true" size="9">
				<logic:present name="caracteristicaVector">
					<html:options collection="caracteristicaVector" property="idCaprCdCaracteristicaprod" labelProperty="caprDsCaracteristicaprod"/>
				</logic:present>
			</html:select>
		</td>	
	</tr>
	<tr>
		<td colspan="2" class="principalLabel" id="totalCaracteristica">
			<logic:present name="caracteristicaVector">
				<bean:message key="prompt.total"/>: <%= ((java.util.Vector)request.getAttribute("caracteristicaVector")).size() %>
			</logic:present>
		</td>	
	</tr>
	</table>
	
</html:form>
</body>
</html>
