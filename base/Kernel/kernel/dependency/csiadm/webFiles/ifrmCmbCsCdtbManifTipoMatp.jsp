<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes, com.iberia.helper.Constantes,br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript">
function carregaGrupo() {
	document.administracaoCsAstbMateriaProdutoMaprForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	document.administracaoCsAstbMateriaProdutoMaprForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
	document.administracaoCsAstbMateriaProdutoMaprForm.target = cmbGrupo.name;
	document.administracaoCsAstbMateriaProdutoMaprForm.submit();
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/AdministracaoCsAstbMateriaProdutoMapr.do" styleId="administracaoCsAstbComposicaoCompForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
  <br>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="50%" class="principalLabel">
        <bean:message key="prompt.manifestacaoTipo" />
      </td>
      <td width="50%" class="principalLabel">
        <bean:message key="prompt.grupoManifestacao" />
      </td>
    </tr>
    <tr>
      <td width="50%">
        <html:select property="idPrasCdProdutoAssunto" styleClass="principalObjForm">
          <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
          <html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto" />
        </html:select>
      </td>
      <td width="50%">
        <iframe name="cmbGrupo" src="CsAstbComposicaoComp.do?acao=showAll&tela=cmbCsCdtbGrupoManifestacaoGrma" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </td>
    </tr>
  </table>
</html:form>
</body>
</html>