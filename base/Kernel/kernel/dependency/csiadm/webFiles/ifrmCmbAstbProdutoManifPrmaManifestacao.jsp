<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
	function atualizaGrupoManif() {
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
		document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_SUPERGRUPOMANIF %>';
		document.administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbSuperGrupoManif.name;
		document.administracaoCsAstbProdutoManifPrmaForm.submit();
	<%} else {%>
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>';
		document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_GRUPOMANIF %>';
		document.administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbGrupoManif.name;
		document.administracaoCsAstbProdutoManifPrmaForm.submit();
	<%}%>
	
	}
	
	function iniciaTela(){
		if(document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.length == 2){
			document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.selectedIndex = 1;
		}
	
		atualizaGrupoManif();
		parent.validaChkCombo();
	}

	
	var c_desabilitaProdutoAsn1 = 0;
	function desabilitaProdutoAsn1() {
		try {
			if (parent.getFlagProduto() == 1) {
				parent.ifrmCmbLinhaProduto.document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled = true;
			}

			
			c_desabilitaProdutoAsn1 = 0;
		} catch(e) {
			if(c_desabilitaProdutoAsn1 > 10) return;

			setTimeout('desabilitaProdutoAsn1()', 100);
			c_desabilitaProdutoAsn1++;
		}
	}
	
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); iniciaTela();">
	<html:form action="/AdministracaoCsAstbProdutoManifPrma.do" styleId="administracaoCsAstbProdutoManifPrmaForm">
		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<html:hidden property="idGrmaCdGrupoManifestacao" />
		<html:hidden property="idTpmaCdTpManifestacao" />
		<html:hidden property="idAsn1CdAssuntoNivel1" />
		<html:hidden property="idAsn2CdAssuntoNivel2" />
		<html:hidden property="idSugrCdSupergrupo" />
		
		<html:select property="idMatpCdManifTipo" styleClass="principalObjForm" onchange="atualizaGrupoManif()">
			<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
			<html:options collection="combo" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo"/>
		</html:select>  
		
	</html:form>
	</body>
	<script>
		desabilitaProdutoAsn1();
	</script>
</html>