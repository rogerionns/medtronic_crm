<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<%
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	

	long idEmpresa = empresaVo.getIdEmprCdEmpresa();
	
	String maxRegistros = "";
	try{
		maxRegistros = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_LIMITE_LINHAS_RESULTADO,idEmpresa);
	}catch(Exception e){}
		
	if(maxRegistros == null || maxRegistros.equals("")){
		maxRegistros = "100";
	}
	
	//pagina��o****************************************
	long numRegTotal=0;
	if (request.getAttribute("csCdtbProdutoAssuntoPrasVector")!=null){
		Vector v = ((java.util.Vector)request.getAttribute("csCdtbProdutoAssuntoPrasVector"));
		if (v.size() > 0){
			numRegTotal = ((CsCdtbProdutoAssuntoPrasVo)v.get(0)).getNumRegTotal();
		}
	}

	long regDe=0;
	long regAte = 0;

	if (request.getParameter("regDe") != null)
		regDe = Long.parseLong((String)request.getParameter("regDe"));
	if (request.getParameter("regAte") != null)
		regAte  = Long.parseLong((String)request.getParameter("regAte"));
	//***************************************
	
%>


<%@page import="java.util.Vector"%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

	function setaDeAte(){
	
		if(parent.document.editCsCdtbProdutoAssuntoPrasForm.regDe.value == "0" && parent.document.editCsCdtbProdutoAssuntoPrasForm.regAte.value == "0"){			
			
			parent.document.editCsCdtbProdutoAssuntoPrasForm.regDe.value='<%=regDe%>';
			parent.document.editCsCdtbProdutoAssuntoPrasForm.regAte.value='<%=regAte%>';
			parent.initPaginacao();
		}	
	
	}
	
	function inicio(){
		parent.setPaginacao(<%=regDe%>,<%=regAte%>);
		parent.atualizaPaginacao(<%=numRegTotal%>);
			
	}

</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form styleId="editCsCdtbProdutoAssuntoPrasForm" action="/AdministracaoCsCdtbProdutoAssuntoPras.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="erro"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idAsn1CdAssuntoNivel1"/>
	<html:hidden property="idAsn2CdAssuntoNivel2"/>

<script>
	var possuiRegistros=false;
	var totalRegistros=0;
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="csCdtbProdutoAssuntoPrasVector" indexId="sequencia"> 
  <script>
  		possuiRegistros=true;
  		totalRegistros=<%=sequencia%>;
  </script>
  <tr> 
    <td width="3%" class="principalLstPar"> <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />','<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')"> 
    </td>

    <td width="3%" class="principalLstParMao"> 
    	<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMAPRAS_IDPR.xml','<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />' + ';' + '<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')">
    </td>

	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
	    <td class="principalLstParMao" align="left" width="20%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />','<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')"> 
	      <div align="left">
				&nbsp;<bean:write name="ccttrtVector" property="csCdtbLinhaLinhVo.linhDsLinha" /> 
			</div>
	    </td>

	    <td class="principalLstParMao" align="left" width="26%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />','<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')"> 
	      <div align="left">&nbsp;  
	        <bean:write name="ccttrtVector" property="prasDsProdutoAssunto" />
	      </div>
	    </td>
	<%}else{%>
	
	    <td class="principalLstParMao" colspan="2" align="left" width="46%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />','<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')"> 
	      <div align="left">
				&nbsp;<bean:write name="ccttrtVector" property="prasDsProdutoAssunto" />
			</div>
	    </td>
	<%}%>
	    
    <td class="principalLstParMao" align="center" width="18%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />','<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')"> 
      <div align="left">
        &nbsp;<bean:write name="ccttrtVector" property="codProd" />
      </div>
    </td>
    <td class="principalLstParMao" align="center" width="11%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />','<bean:write name="ccttrtVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')"> 
      <div align="left">
        &nbsp;<bean:write name="ccttrtVector" property="prasDhInativo" />
      </div>
    </td>
  </tr>
  </logic:iterate> 
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_PRODUTOASSUNTO_EXCLUSAO_CHAVE%>', editCsCdtbProdutoAssuntoPrasForm.lixeira);
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>