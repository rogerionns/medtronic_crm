<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");
%>
	
<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsCdtbPublicoPublForm" action="/AdministracaoCsCdtbPublicoPubl.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />

<script language="JavaScript">

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
</script>
<script language="JavaScript">
nLinha = new Number(0);
estilo = new Number(0);

function adicionarResu(){
		if (document.administracaoCsCdtbPublicoPublForm['idResuCdResultado'].value == ""){
			alert("<bean:message key='prompt.porFavorEscolhaUmResultado' />");
			document.administracaoCsCdtbPublicoPublForm['idResuCdResultado'].focus();
			return false;
		}
		addResu(document.administracaoCsCdtbPublicoPublForm.idResuCdResultado[document.administracaoCsCdtbPublicoPublForm.idResuCdResultado.selectedIndex].text, document.administracaoCsCdtbPublicoPublForm.idResuCdResultado.value); 
}

function addResu(cResultado, nResultado) {
	nLinha = nLinha + 1;
	estilo++;

	objEMail = document.administracaoCsCdtbPublicoPublForm.idResu;

	for (nNode=0;nNode<objEMail.length;nNode++) {
	  if (objEMail[nNode].value == nResultado) {
		  nResultado=0;
		  }
	}
	if (nResultado > 0) { 
		strTxt = "";
		strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr class='intercalaLst" + (estilo-1)%2 + "'> ";
		strTxt += "     	<td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeResu(\"" + nLinha + "\")></td> ";
		strTxt += "       	<input type=\"hidden\" name=\"idResu\" value=\"" + nResultado + "\" > ";
		strTxt += "     	<td class=principalLstPar width=55%> " + cResultado ;
		strTxt += "     	</td> ";
		strTxt += "		</tr> ";
		strTxt += " </table> ";
		document.getElementsByName("lstResultado").innerHTML += strTxt;
	}else{
		alert('<bean:message key="prompt.selecionarResultadoNaoRepetido" />');
	}
}

function removeResu(nTblExcluir) {
	msg = '<bean:message key="prompt.desejaExcluirEsseResultado" />';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstResultado.removeChild(objIdTbl);
		estilo--;
	}
}

function preencheHidden(){

	if (document.administracaoCsCdtbPublicoPublForm['pucoLimite'].value > 0 && document.administracaoCsCdtbPublicoPublForm['pucoLimite'].value != ""){
		document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.pucoNrLimitedistrib'].value=document.administracaoCsCdtbPublicoPublForm['pucoLimite'].value;
	}

}

function HabilitaLimite(chk) {

	if (chk.checked==false) {
		document.administracaoCsCdtbPublicoPublForm['pucoLimite'].disabled=false;
	}else{
		document.administracaoCsCdtbPublicoPublForm['pucoLimite'].disabled=true;
		document.administracaoCsCdtbPublicoPublForm['pucoLimite'].value='';
		document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.pucoNrLimitedistrib'].value='-1';
	}
}

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 
  
function AtivarPasta(pasta)

{

switch (pasta)

{

case 'RESULTADO':
	SetClassFolder('tdResultado','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEncerramento','principalPstQuadroLinkNormal');
	SetClassFolder('tdOperadores','principalPstQuadroLinkNormal');
	MM_showHideLayers('Resultado','','show','lstResultado','','show','Encerramento','','hide', 'Operadores','','hide');

	break;
	
case 'ENCERRAMENTO':
	SetClassFolder('tdResultado','principalPstQuadroLinkNormal');
	SetClassFolder('tdEncerramento','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdOperadores','principalPstQuadroLinkNormal');
	MM_showHideLayers('Resultado','','hide','lstResultado','','hide','Encerramento','','show', 'Operadores','','hide');
	
	break;

case 'OPERADOR':
	SetClassFolder('tdResultado','principalPstQuadroLinkNormal');
	SetClassFolder('tdEncerramento','principalPstQuadroLinkNormal');
	SetClassFolder('tdOperadores','principalPstQuadroLinkSelecionado');
	MM_showHideLayers('Resultado','','hide','lstResultado','','hide','Encerramento','','hide', 'Operadores','','show');
	
	break;
	
}
}	

	//Esta funcao insere novos options na combo que passarem pelo objeto
	function preencheCombo(Objeto,Valor,Texto){
		var Evento = document.createElement("OPTION");
			
		Evento.value = Valor;
		Evento.text = Texto;
		Objeto.add(Evento);
	}
	
	//Esta funcao remove o option de um combo passado pelo objeto
	function removeCombo(Objeto,Item){
		Objeto.remove(Item);
	}


	//Esta funcao faz todos os movimentos entre as duas combos de sele��o
	function tranferir(tudo, origem, destino){
		if (tudo){
			for (var i= 0; i < origem.length ; i++){
				preencheCombo(destino, origem.options(i).value, origem.options(i).text);
			}
			
			for (var i= origem.length; i >=  0 ; i--){
				removeCombo(origem,i);
			}
		}
		
		else{
		
			if(origem.selectedIndex == -1){
				alert('<bean:message key="prompt.alert.E_necessario_informar_no_minimo_um_item"/>');
			}
			else{
				for (var i= 0; i < origem.length ; i++){
					if (origem.options(i).selected){
						preencheCombo(destino, origem.options(i).value, origem.options(i).text);
					}
				}
				
				for (var i= origem.length -1; i >=  0 ; i--){
					if (origem.options(i).selected){
						removeCombo(origem,i);
					}
				}		
			}
		}
	}

	
</script>	
	
<body name='bodyMenu' id='bodyMenu' bgcolor="#FFFFFF" text="#000000" leftmargin="0" class= "principalBgrPageIFRM" topmargin="0" onload="parent.ifrmConsultaCampanha.MontaCmbHidden()">
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
  <tr> 
    <td class="principalPstQuadroLinkSelecionado" align="center" id="tdResultado" name="tdResultado" onclick="AtivarPasta('RESULTADO')"><bean:message key="prompt.definirresultado"/></td>
    <td class="principalPstQuadroLinkNormal" align="center" id="tdEncerramento" name="tdEncerramento" onclick="AtivarPasta('ENCERRAMENTO')"><bean:message key="prompt.encerramento"/></td>
    <td class="principalPstQuadroLinkNormal" align="center" id="tdOperadores" name="tdOperadores" onclick="AtivarPasta('OPERADOR')"><bean:message key="prompt.operador"/></td>
    
    <td>&nbsp;</td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" height="150">
  <tr> 
    <td valign="top"> 
      <div id="Resultado" style="position:absolute; width:99%; height:130px; z-index:3; visibility: visible"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td class="principalEspacoPqn">&nbsp;</td>
          </tr>
          <tr> 
            <td class="principalLabel"><bean:message key="prompt.resultado"/></td>
          </tr>
          <tr> 
            <td> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="91%"> 
					  <html:select property="idResuCdResultado" styleClass="principalObjForm"> 
					  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
					  <html:options collection="csCdtbResultadoResuVector" property="idResuCdResultado" labelProperty="resuDsResultado"/>
					  </html:select>
                  </td>
                  <td width="9%" align="center"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarResu()"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td class="principalEspacoPqn">&nbsp;</td>
          </tr>
          <tr> 
            <td height="60" valign="top"> 
              <div id="lstResultado" style="position:absolute; width:99%; height:50px; z-index:4; visibility: visible"> 
              	<input type="hidden" name="idResu" value="0">

				<!--Inicio Lista Resultado -->
			 	<logic:present name="listResultadoVector">
				  <logic:iterate id="cnrrVector" name="listResultadoVector">
					<script language="JavaScript">
					  addResu('<bean:write name="cnrrVector" property="resuDsResultado" />',
							  '<bean:write name="cnrrVector" property="idResuCdResultado" />');
					</script>
				  </logic:iterate>
				</logic:present>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div id="Encerramento" style="position:absolute; width:99%; height:130px; z-index:3; visibility: hidden"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td class="principalEspacoPqn">&nbsp;</td>
          </tr>
          <tr> 
            <td class="principalLabel"><bean:message key="prompt.motivoEncerramentoDesc"/></td>
          </tr>
          <tr> 
            <td> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
					  <html:select property="idMoenCdMotivoEncerra" styleClass="principalObjForm"> 
					  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
					  <html:options collection="csCdtbMotivoEncerraMoenVector" property="idMoenCdMotivoEncerra" labelProperty="moenDsMotivoEncerra"/>
					  </html:select>
                  </td>
                </tr>
                <tr> 
                  <td class="principalLabel"><bean:message key="prompt.data"/></td>
                </tr>
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="27%">
                        	<html:text property="publDhEncerramento" styleClass="text" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                        </td>
                       	<td width="73%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('administracaoCsCdtbPublicoPublForm.publDhEncerramento')" class="principalLstParMao" ></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
      <div id="Operadores" style="position:absolute; width:99%; height:130px; z-index:3; visibility: hidden"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td colspan="3" class="principalEspacoPqn">&nbsp;</td>
          </tr>
          
          <tr>
			  <td width="50%" class="principalLabel"><bean:message key="prompt.operadoresSelecionados"/></td>
			  <td width="4%">&nbsp;</td>	
			  <td width="50%" class="principalLabel"><bean:message key="prompt.operadoresDisponiveis"/></td>
          </tr>
          
          <tr>
			  <td width="50%">
				<html:select property="idOperadoresDisponiveis" size="7" multiple="true" styleClass="principalObjForm"> 
					 <html:options collection="csCdtbFuncionarioFuncDispVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>
				</html:select>
			  </td>	

			  <td width="4%">
			  		<table>
			  			<tr>
			  				<td><img src="webFiles/images/botoes/avancar_unico.gif" width=21 height=18 class=geralCursoHand onclick="javascript:tranferir(false, idOperadoresDisponiveis, idOperadoresSelecionados)"></td>
			  			</tr>
			  			<tr>
			  				<td><img src="webFiles/images/botoes/setaRight.gif" width=21 height=18 class=geralCursoHand onclick="javascript:tranferir(true, idOperadoresDisponiveis, idOperadoresSelecionados)"></td>
			  			</tr>
			  			<tr>
			  				<td><img src="webFiles/images/botoes/setaLeft.gif" width=21 height=18 class=geralCursoHand onclick="javascript:tranferir(true, idOperadoresSelecionados, idOperadoresDisponiveis)"></td>
			  			</tr>
			  			<tr>
			  				<td><img src="webFiles/images/botoes/voltar_unico.gif" width=21 height=18 class=geralCursoHand onclick="javascript:tranferir(false, idOperadoresSelecionados, idOperadoresDisponiveis)"></td>
			  			</tr>
			  		</table>
			  </td>	
				
			  <td width="50%">
				<html:select property="idOperadoresSelecionados" size="7" multiple="true" styleClass="principalObjForm"> 
					 <html:options collection="csCdtbFuncionarioFuncSelVector" property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario" labelProperty="csCdtbFuncionarioFuncVo.funcNmFuncionario"/>
				</html:select>
			  </td>	
          </tr>
          
          
        </table>
      </div>
      
      
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
