<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
</head>

<script language="JavaScript">
	function atualizaListaComp(){
		
		if(parent.lstArqCarga != undefined){
		
			parent.lstArqCarga.location.href = "AdministracaoCsAstbComposicaoComp.do?tela=administracaoLstCsAstbComposicaoComp&acao=<%=Constantes.ACAO_VISUALIZAR%>"+
				"&idAsnCdAssuntoNivel="+ parent.document.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value +
				"&idAsn1CdAssuntonivel1="+ parent.document.administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value +
				"&idAsn2CdAssuntonivel2="+ administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value;
		}

		if(administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value != ""){
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value +"@"+ administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.value;
			<%}else{%>
				administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value +"@1";
			<%}%>
		}
		else{
			administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = administracaoCsAstbComposicaoCompForm.idAsn1CdAssuntonivel1.value +"@0";
		}
		
		//parent.administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value = administracaoCsAstbComposicaoCompForm.idAsnCdAssuntoNivel.value;
		//Chamado 78869 - Vinicius - Sempre levar em conta a PRAS e a PRMA
		parent.MontaComboManifestacao();
	}
	
	function inicio(){
		try{
			<logic:notEqual name="administracaoCsAstbComposicaoCompForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
				try{
					if(document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.length == 2){
						document.administracaoCsAstbComposicaoCompForm.idAsn2CdAssuntonivel2.selectedIndex = 1;
						atualizaListaComp();
					}
				}catch(e){}
			</logic:notEqual>
			parent.validaChk();
		}catch(e){}
	}

	function mostraCampoBuscaProd(){
		document.administracaoCsAstbComposicaoCompForm.acao.value = "<%=Constantes.ACAO_CONSULTAR%>";
		document.administracaoCsAstbComposicaoCompForm.submit();
	}
	
	function buscarProduto(){

		if (document.administracaoCsAstbComposicaoCompForm['asn2DsAssuntoNivel2'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}
		
		document.administracaoCsAstbComposicaoCompForm.tela.value = "cmbCsCdtbAssuntoNivel2Asn2";
		document.administracaoCsAstbComposicaoCompForm.acao.value = "<%=Constantes.ACAO_VISUALIZAR%>";
		
		document.administracaoCsAstbComposicaoCompForm.submit();

	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}
		
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/AdministracaoCsAstbComposicaoComp.do" styleId="administracaoCsAstbComposicaoCompForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idAsn1CdAssuntonivel1" />
  <html:hidden property="idAsnCdAssuntoNivel" />
  <html:hidden property="idLinhCdLinha" />
  
  
  <logic:notEqual name="administracaoCsAstbComposicaoCompForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
				  <html:select property="idAsn2CdAssuntonivel2" styleClass="principalObjForm" onchange="atualizaListaComp()">
				    <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
				    <html:options collection="csCdtbAssuntoNivel2Asn2Vector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2" />
				  </html:select>
				                             
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right" id="divBotao"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
		  	</td>
	  	</tr>
	  </table>
	  
		  <logic:present name="combo">
			  <logic:iterate name="csCdtbAssuntoNivel2Asn2Vector" id="csCdtbAssuntoNivel2Asn2Vector">
				  <input type="hidden" name='txtAsn2<bean:write name="csCdtbAssuntoNivel2Asn2Vector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>' value='<bean:write name="csCdtbAssuntoNivel2Asn2Vector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>'>
			  </logic:iterate>	  
		  </logic:present>
	  
	  </logic:notEqual>
	  
   	  <logic:equal name="administracaoCsAstbComposicaoCompForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
	  	<tr>
	  		<td width="95%">
	  			<html:text property="asn2DsAssuntoNivel2" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
		  	</td>
		</tr> 	
	  </table>
	  <script>
		//document.administracaoCsCdtbProdutoAssuntoPrasForm['idAsn2CdAssuntoNivel2'].select();
	  </script>
   	  </logic:equal>
   	  
</html:form>  
</body>
</html>
<logic:equal name="administracaoCsAstbComposicaoCompForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	<script>
		document.administracaoCsAstbComposicaoCompForm['asn2DsAssuntoNivel2'].focus();
	</script>
</logic:equal>