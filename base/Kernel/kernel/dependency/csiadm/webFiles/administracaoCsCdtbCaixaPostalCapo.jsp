<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
</script>
</head>
<!--Chamado: 98441 - 02/01/2015 - Leonardo M. Facchini-->
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');resizeContentDiv();" onresize="resizeContentDiv();">
<html:form styleId="editCsCdtbCaixaPostalCapoForm" action="/AdministracaoCsCdtbCaixaPostalCapo.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="capoNrSequencia"/>
	<html:hidden property="idServCdServico"/>

<script>var possuiRegistros=false;</script>
<div style="width: 100%;overflow: auto;height: 95%;">
<div id="contentdiv" style="float: left;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			
			<logic:iterate id="ccttrtVector" name="CsCdtbCaixaPostalCapoVector">
  <script>possuiRegistros=true;</script>
  <tr> 
    <td width="3%" class="principalLstPar"> <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="csCdtbServicoServVo.idServCdServico" />', '<bean:write name="ccttrtVector" property="capoNrSequencia" />')"></td>
    <td class="principalLstParMao" width="10%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbServicoServVo.idServCdServico" />', '<bean:write name="ccttrtVector" property="capoNrSequencia" />')"> 
      <bean:write name="ccttrtVector" property="capoNrSequencia" />&nbsp; </td>
    <td class="principalLstParMao" align="left" width="36%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbServicoServVo.idServCdServico" />', '<bean:write name="ccttrtVector" property="capoNrSequencia" />')"> 
      <script>acronym ('<bean:write name="ccttrtVector" property="csCdtbServicoServVo.servDsServico" /> ', 35);</script></td>
    <!-- td class="principalLstParMao" align="left" width="23%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbServicoServVo.idServCdServico" />', '<bean:write name="ccttrtVector" property="capoNrSequencia" />')"> 
      <bean:write name="ccttrtVector" property="capoDsServidor" /></td-->
    <td width="39%" class="principalLstParMao" align="left" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbServicoServVo.idServCdServico" />', '<bean:write name="ccttrtVector" property="capoNrSequencia" />')"> 
     <script>acronym ('<bean:write name="ccttrtVector" property="csCdtbAssuntoMailAsmeVo.asmeDsAssuntoMail" />', 15);</script> </td>
	<td class="principalLstParMao" align="left" width="12%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="csCdtbServicoServVo.idServCdServico" />', '<bean:write name="ccttrtVector" property="capoNrSequencia" />')"> 
      <bean:write name="ccttrtVector" property="capoDhInativo" />&nbsp;</td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td colspan="5" height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
</div>
</div>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
//Chamado: 98441 - 02/01/2015 - Leonardo M. Facchini
function resizeContentDiv(){
	var parentNodeOffsetWidth = document.getElementById('contentdiv').parentNode.offsetWidth;
	document.getElementById('contentdiv').style.width = parentNodeOffsetWidth - (parentNodeOffsetWidth * 0.03);
	document.getElementById('contentdiv').parentNode.style.height = parseInt(document.getElementById('contentdiv').parentNode.parentNode.parentNode.offsetHeight);
}
if('<%=request.getAttribute("gravou")%>' == 'true' || '<%=request.getAttribute("excluiu")%>' == 'true'){
	parent.cancel();
}

	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_CAIXAPOSTAL_EXCLUSAO_CHAVE%>', editCsCdtbCaixaPostalCapoForm.lixeira);	
	
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>