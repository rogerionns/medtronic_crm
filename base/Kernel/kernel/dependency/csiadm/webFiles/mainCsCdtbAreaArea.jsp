<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsCdtbAreaAreaForm.target = admIframe.name;
	document.administracaoCsCdtbAreaAreaForm.acao.value ='filtrar';
	document.administracaoCsCdtbAreaAreaForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbAreaAreaForm.filtro.value = '';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsCdtbAreaAreaForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbAreaAreaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_AREA_AREA%>';
	editIframe.document.administracaoCsCdtbAreaAreaForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbAreaAreaForm.submit();
	AtivarPasta(editIframe);
	//MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
	document.getElementById('Destinatario').style.display='none';
	document.getElementById('Manifestacao').style.display='block';
}

function submeteFormEdit(codigo){
	tab.document.administracaoCsCdtbAreaAreaForm.idAreaCdArea.value = codigo;
	tab.document.administracaoCsCdtbAreaAreaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_AREA_AREA%>';
	tab.document.administracaoCsCdtbAreaAreaForm.target = editIframe.name;
	tab.document.administracaoCsCdtbAreaAreaForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbAreaAreaForm.submit();
	AtivarPasta(editIframe);
	//MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
	document.getElementById('Destinatario').style.display='none';
	document.getElementById('Manifestacao').style.display='block';
}

function submeteSalvar(){

	if(tab.document.administracaoCsCdtbAreaAreaForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_AREA_AREA%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbAreaAreaForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_AREA_AREA%>'){
		if (trim(tab.document.administracaoCsCdtbAreaAreaForm.areaDsArea.value).length > 0) {
			tab.document.administracaoCsCdtbAreaAreaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_AREA_AREA%>';
			tab.document.administracaoCsCdtbAreaAreaForm.target = admIframe.name;
			disableEnable(tab.document.administracaoCsCdtbAreaAreaForm.idAreaCdArea, false);
			tab.document.administracaoCsCdtbAreaAreaForm.submit();
			disableEnable(tab.document.administracaoCsCdtbAreaAreaForm.idAreaCdArea, true);
			cancel();
		} else {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbAreaAreaForm.areaDsArea.focus();
		}
	}
}

function submeteExcluir(codigo) {
	
	tab.document.administracaoCsCdtbAreaAreaForm.idAreaCdArea.value = codigo;
	tab.document.administracaoCsCdtbAreaAreaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_AREA_AREA%>';
	tab.document.administracaoCsCdtbAreaAreaForm.target = editIframe.name;
	tab.document.administracaoCsCdtbAreaAreaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbAreaAreaForm.submit();
	AtivarPasta(editIframe);
	//MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
	document.getElementById('Destinatario').style.display='none';
	document.getElementById('Manifestacao').style.display='block';
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbAreaAreaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_AREA_AREA%>';
		editIframe.document.administracaoCsCdtbAreaAreaForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbAreaAreaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbAreaAreaForm.idAreaCdArea, false);
		editIframe.document.administracaoCsCdtbAreaAreaForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbAreaAreaForm.idAreaCdArea, true);
		AtivarPasta(admIframe);
		//MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		document.getElementById('Destinatario').style.display='block';
		document.getElementById('Manifestacao').style.display='none';
		editIframe.location = 'AdministracaoCsCdtbAreaArea.do?tela=editCsCdtbAreaArea&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbAreaArea.do?tela=editCsCdtbAreaArea&acao=incluir';
	AtivarPasta(admIframe);
	//MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
	document.getElementById('Destinatario').style.display='block';
	document.getElementById('Manifestacao').style.display='none';
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsCdtbAreaAreaForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbAreaAreaForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_AREA_AREA%>';
			//MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			document.getElementById('Destinatario').style.display='block';
			document.getElementById('Manifestacao').style.display='none';
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormal');
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbAreaAreaForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_AREA_AREA%>';
			//MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			document.getElementById('Destinatario').style.display='none';
			document.getElementById('Manifestacao').style.display='block';
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionado');	
			setaIdiomaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbAreaAreaForm"	action="/AdministracaoCsCdtbAreaArea.do">

<html:hidden property="modo" />
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="topicoId" />

<body class="principalBgrPage" text="#000000">

			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="principalBgrQuadro" valign="top" height="580">
					<br>
						<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td valign="top" height="520">
								
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										align="center">
										<tr>
											<td class="principalPstQuadroLinkVazio">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="principalPstQuadroLinkSelecionado"
															id="tdDestinatario" name="tdDestinatario"
															onClick="AtivarPasta(admIframe);"><bean:message
																key="prompt.procurar" /></td>
														<td class="principalPstQuadroLinkNormal"
															id="tdManifestacao" name="tdManifestacao"
															onClick="AtivarPasta(editIframe);"><bean:message
																key="prompt.area" /></td>
													</tr>
												</table>
											</td>
											<td width="4"><img
												src="webFiles/images/separadores/pxTranp.gif" width="1"
												height="1"></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td valign="top" class="principalBgrQuadro" height="490">
												<div name="Manifestacao" id="Manifestacao" style="position: relative; width: 100%; height: 400px; z-index: 6; display: none;">
													<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td height="400">
																<iframe id=editIframe name="editIframe" src="AdministracaoCsCdtbAreaArea.do?tela=editCsCdtbAreaArea&acao=incluir" width="100%" height="400" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0"></iframe>
															</td>
														</tr>
													</table>
												</div>
												<div name="Destinatario" id="Destinatario" style="position: relative; width: 100%; height: 400px; z-index: 2;">
													<table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
														<tr>
															<td><div class="principalLabel"><bean:message key="prompt.descricao" /></div></td>
														</tr>
														<tr>
															<td>
																<table border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td style="width: 400px;"><html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();" /></td>
																		<td><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()"></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td style="width: 140px;">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td style="width: 50px;"><div class="principalLstCab">&nbsp;</div></td>
																		<td><div class="principalLstCab"><bean:message key="prompt.codigo" /></div></td>
																	</tr>
																</table>
															</td>
															<td style="width: 500px;"><div class="principalLstCab"><bean:message key="prompt.area" /></div></td>
															<td align="left"><div class="principalLstCab">&nbsp;&nbsp;<bean:message key="prompt.inativo" /></div></td>
														</tr>
														<tr valign="top">
															<td colspan="3" height="440">
																<iframe id=admIframe name="admIframe" src="AdministracaoCsCdtbAreaArea.do?acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="440" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0"></iframe>
															</td>
														</tr>
													</table>
												</div></td>
											<td width="4" height="490"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
										</tr>
										<tr>
											<td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
											<td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="1" height="3"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
							</tr>
						</table>
						<table border="0" cellspacing="0" cellpadding="4" align="right">
							<tr align="center">
								<td width="60" align="right"><img
									src="webFiles/images/botoes/new.gif" name="imgIncluir"
									width="14" height="16" class="geralCursoHand"
									title="<bean:message key='prompt.novo'/>"
									onclick="clearError();submeteFormIncluir()"></td>
								<td width="20"><img src="webFiles/images/botoes/gravar.gif"
									name="imgGravar" width="20" height="20" class="geralCursoHand"
									title="<bean:message key='prompt.gravar'/>"
									onclick="clearError();submeteSalvar();"></td>
								<td width="20"><img
									src="webFiles/images/botoes/cancelar.gif" width="20"
									height="20" class="geralCursoHand"
									title="<bean:message key='prompt.cancelar'/>"
									onclick="clearError();cancel();"></td>
							</tr>
						</table>
						<table align="center">
							<tr>
								<td><label id="error"> </label></td>
							</tr>
						</table></td>
					<td width="4" height="580"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="580"></td>
				</tr>
				<tr>
					<td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
					<td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
				</tr>
			</table>
		</html:form>

<script language="JavaScript">
	var tab = admIframe ;
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_INCLUSAO_CHAVE%>', document.administracaoCsCdtbAreaAreaForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_AREA_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbAreaAreaForm.imgGravar.disabled=true;
			document.administracaoCsCdtbAreaAreaForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbAreaAreaForm.imgGravar.title='';
	}
	   
	desabilitaListaEmpresas();
	
	setaArquivoXml("CS_ASTB_IDIOMAAREA_IDAR.xml");
	habilitaTelaIdioma();
	   
</script>


</body>
</html>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>