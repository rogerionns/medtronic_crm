<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>

<script language="JavaScript">

function anexaArquivo(){

	if (administracaoCsCdtbProdutoImagemPrimForm.pathArquivoImg.value == ""){
		alert ('<bean:message key="prompt.Por_favor_escolha_um_arquivo"/>');
		return false;
	}

	parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
	
	var idAsn1 = parent.parent.document.getElementById("administracaoCsCdtbProdutoAssuntoPrasForm").idAsn1CdAssuntoNivel1.value;
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		var idAsn2 = parent.parent.ifrmCmbAssuntoNivel2Pras.document.getElementById("administracaoCsCdtbProdutoAssuntoPrasForm").idAsn2CdAssuntoNivel2.value;
	<%}else{%>
		var idAsn2 = parent.parent.document.getElementById("administracaoCsCdtbProdutoAssuntoPrasForm").idAsn2CdAssuntoNivel2.value;
	<%}%>

	document.administracaoCsCdtbProdutoImagemPrimForm.idAsn1CdAssuntonivel1.value = idAsn1;
	document.administracaoCsCdtbProdutoImagemPrimForm.idAsn2CdAssuntonivel2.value = idAsn2;
	
	document.administracaoCsCdtbProdutoImagemPrimForm.tela.value="<%=MAConstantes.TELA_LST_IMAGEM_PRAS%>";
	document.administracaoCsCdtbProdutoImagemPrimForm.acao.value="<%=Constantes.ACAO_INCLUIR%>";
	document.administracaoCsCdtbProdutoImagemPrimForm.target="ifrmLstArquivoPras";
	
	document.administracaoCsCdtbProdutoImagemPrimForm.submit();
	
}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');">

<html:form styleId="administracaoCsCdtbProdutoImagemPrimForm" enctype="multipart/form-data" action="/AdministracaoCsCdtbProdutoImagemPrim.do">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="idAsn1CdAssuntonivel1" />
<html:hidden property="idAsn2CdAssuntonivel2" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><html:file property="pathArquivoImg" styleClass="principalObjForm"/></td>
	</tr>
</table>	

</html:form>
</body>
</html>