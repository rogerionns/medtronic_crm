<%@ page language="java" import="com.iberia.helper.Constantes"%>
<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
var result = 0;


function carregaRegistro(plus1,plus2,plus3,plus4,plus5,plus6,plus7,plus8){

	parent.document.cadastroLicencaForm['csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft1'].value = plus1;
	parent.document.cadastroLicencaForm['csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft2'].value = plus2;
	parent.document.cadastroLicencaForm['csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft3'].value = plus3;
	parent.document.cadastroLicencaForm['txtNSerie'].value = plus3;
	parent.document.cadastroLicencaForm['csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft4'].value = plus4;
	parent.document.cadastroLicencaForm['csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft5'].value = plus5;
	parent.document.cadastroLicencaForm['txtAplicacao'].value = plus5;
	parent.document.cadastroLicencaForm['csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft6'].value = plus6;
	parent.document.cadastroLicencaForm['txtModulo'].value = plus6;
	parent.document.cadastroLicencaForm['csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft7'].value = plus7;
	parent.document.cadastroLicencaForm['txtLicenca'].value = plus7;
	parent.document.cadastroLicencaForm['csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft8'].value = plus8;
	parent.document.cadastroLicencaForm['txtDias'].value = plus8;
	
}

function submeteExcluir(nrSerie){
	if(confirm("<bean:message key="prompt.Deseja_excluir_esse_registro"/>")){
		parent.document.cadastroLicencaForm['csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft3'].value = nrSerie;
		parent.document.cadastroLicencaForm.tela.value = '<%=MAConstantes.TELA_LST_CADASTRO_LICENCA%>';
		parent.document.cadastroLicencaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR%>';
		
		parent.document.cadastroLicencaForm.target = parent.ifrmLstCadastroLicenca.name;
		
		parent.document.cadastroLicencaForm.submit();
	}
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0">
<html:form action="/CadastroLicenca.do" styleId="cadastroLicencaForm">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td height="70" valign="top">
	      <div id="Layer1" style="position:absolute; height:70; z-index:1; visibility: visible; width: 100%"> 
	        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
	        	<logic:present name="licNewVector">
		        	<logic:iterate id="licNewVector" name="licNewVector" indexId="numero">	
		        		<script>
		        			result++;
		        		</script>
					    <tr>
					    	<td width="3%" class="principalLstPar">
							  <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="submeteExcluir('<bean:write name="licNewVector" property="idPsf1CdPlusoft3" />')">
							</td>
						    <td width="30%" class="principalLstPar" onclick="carregaRegistro('<bean:write name="licNewVector" property="idPsf1CdPlusoft1"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft2"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft3"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft4"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft5"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft6"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft7"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft8"/>')">&nbsp;<bean:write name="licNewVector" property="idPsf1CdPlusoft5"/></td> 
					    	<td width="30%" class="principalLstPar" onclick="carregaRegistro('<bean:write name="licNewVector" property="idPsf1CdPlusoft1"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft2"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft3"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft4"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft5"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft6"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft7"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft8"/>')"><bean:write name="licNewVector" property="idPsf1CdPlusoft6"/>&nbsp;</td> 
					    	<td width="20%" class="principalLstPar" onclick="carregaRegistro('<bean:write name="licNewVector" property="idPsf1CdPlusoft1"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft2"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft3"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft4"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft5"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft6"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft7"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft8"/>')"><bean:write name="licNewVector" property="idPsf1CdPlusoft3"/>&nbsp;</td> 
					    	<td width="18%" class="principalLstPar" onclick="carregaRegistro('<bean:write name="licNewVector" property="idPsf1CdPlusoft1"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft2"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft3"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft4"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft5"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft6"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft7"/>','<bean:write name="licNewVector" property="idPsf1CdPlusoft8"/>')"><bean:write name="licNewVector" property="idPsf1CdPlusoft7"/>&nbsp;</td> 					    	
					    </tr>
					</logic:iterate>
				</logic:present>
				<script>
				  if (result == 0)
				    document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="430" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
				</script>
	        </table>
	      </div>
	    </td>
	  </tr>
	</table>
</body>
</html:form>
</html>
