<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>
</head>

<script>
function submeteExcluirSite(idVirtualSite, idMafoCdMarcaFornecedor,idSimaCdSitemarca, simaDsSitemarca) {
	if(confirm('<bean:message key="prompt.Deseja_remover_esse_item" />'))
	{
		ifrmLstSite.location.href = "AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_LST_SITE%>&acao=<%= Constantes.ACAO_EXCLUIR%>&idMafoCdMarcaFornecedor=" + idMafoCdMarcaFornecedor + "&idSimaCdSitemarca=" + idSimaCdSitemarca + "&idVirtualSite=" + idVirtualSite + "&simaDsSitemarca=" + simaDsSitemarca;
	}
}

function submeteFormEditSite(idVirtualSite, idMafoCdMarcaFornecedor, idSimaCdSitemarca, simaDsSitemarca, sequencial) {
	
	administracaoCsCdtbMarcafornecedorMafoForm.idVirtualSite.value = idVirtualSite;
	administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value = idMafoCdMarcaFornecedor;
	administracaoCsCdtbMarcafornecedorMafoForm.idSimaCdSitemarca.value = idSimaCdSitemarca;
	administracaoCsCdtbMarcafornecedorMafoForm.simaDsSitemarca.value = simaDsSitemarca;
	
	if (ifrmLstSite.contSites==1){
		administracaoCsCdtbMarcafornecedorMafoForm.simaTxObservacao.value = ifrmLstSite.document.forms[0].txtObsevacao.value;
	}else{
		administracaoCsCdtbMarcafornecedorMafoForm.simaTxObservacao.value = ifrmLstSite.document.forms[0].txtObsevacao[sequencial].value;
	}
	
	var targetOrig = administracaoCsCdtbMarcafornecedorMafoForm.target;
	var telaOrig = administracaoCsCdtbMarcafornecedorMafoForm.tela.value;
	var acaoOrig = administracaoCsCdtbMarcafornecedorMafoForm.acao.value; 
	 
	administracaoCsCdtbMarcafornecedorMafoForm.target = "ifrmLstSite";
	administracaoCsCdtbMarcafornecedorMafoForm.tela.value = '<%= MAConstantes.IFRM_LST_SITE%>';
	administracaoCsCdtbMarcafornecedorMafoForm.acao.value = '<%= Constantes.ACAO_EDITAR%>';
	
	administracaoCsCdtbMarcafornecedorMafoForm.submit();
	
	administracaoCsCdtbMarcafornecedorMafoForm.target = targetOrig;
	administracaoCsCdtbMarcafornecedorMafoForm.tela.value = telaOrig;
	administracaoCsCdtbMarcafornecedorMafoForm.acao.value = acaoOrig;
	
	//ifrmLstSite.location.href = "AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<!%= MAConstantes.IFRM_LST_SITE%>&acao=<!%= Constantes.ACAO_EDITAR%>&idMafoCdMarcaFornecedor=" + idMafoCdMarcaFornecedor + "&idSimaCdSitemarca=" + idSimaCdSitemarca + "&simaDsSitemarca=" + simaDsSitemarca + "&idVirtualSite=" + idVirtualSite;
}

function limparTela()
{
	administracaoCsCdtbMarcafornecedorMafoForm.idSimaCdSitemarca.value = "0";
	administracaoCsCdtbMarcafornecedorMafoForm.simaDsSitemarca.value = "";		
	administracaoCsCdtbMarcafornecedorMafoForm.simaTxObservacao.value = "";
	administracaoCsCdtbMarcafornecedorMafoForm.idVirtualSite.value = "0";
}


function submeteIncluirSite() {
	
	simaDsSitemarca = administracaoCsCdtbMarcafornecedorMafoForm.simaDsSitemarca.value;
	
	if(trim(simaDsSitemarca)!= "")
	{
		idMafoCdMarcaFornecedor = parent.document.administracaoCsCdtbMarcafornecedorMafoForm.idMafoCdMarcaFornecedor.value;
		idSimaCdSitemarca = administracaoCsCdtbMarcafornecedorMafoForm.idSimaCdSitemarca.value;	
		var simaTxObservacao = administracaoCsCdtbMarcafornecedorMafoForm.simaTxObservacao.value;
		simaInPrincipal = "";

		if (administracaoCsCdtbMarcafornecedorMafoForm.idVirtualSite.value == "0"){
			idVirtualSite = ifrmLstSite.contSites;
			if(idVirtualSite == 0)
			{					
				idVirtualSite = 1;	
			}else
				idVirtualSite++;
		}else{
			idVirtualSite = administracaoCsCdtbMarcafornecedorMafoForm.idVirtualSite.value;
		}
		
		url  = "AdministracaoCsCdtbMarcafornecedorMafo.do";
		url += "?acao=<%= Constantes.ACAO_INCLUIR%>";
		url += "&tela=<%= MAConstantes.IFRM_LST_SITE%>";
		
		url += "&idMafoCdMarcaFornecedor=" + idMafoCdMarcaFornecedor;
		url += "&idSimaCdSitemarca=" + idSimaCdSitemarca;
		url += "&idVirtualSite=" + idVirtualSite;
		url += "&simaDsSitemarca=" + simaDsSitemarca;
		url += "&simaTxObservacao=" + simaTxObservacao;
		
		if(simaInPrincipal != null)
		{
			url += "&simaInPrincipal=" + simaInPrincipal;
		}
		
		ifrmLstSite.document.location.href = url;
		
		limparTela();
	}
	else
	{
		alert('<bean:message key="prompt.alert.E_necessario_preencher_site" />');
	}
}


</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
	
<html:form action="/AdministracaoCsCdtbMarcafornecedorMafo.do" styleId="administracaoCsCdtbMarcafornecedorMafoForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	
	<html:hidden property="idMafoCdMarcaFornecedor" />
	<html:hidden property="idSimaCdSitemarca" />
	<html:hidden property="idVirtualSite" />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr> 
             <td>&nbsp;</td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">  
         	<tr>        
             <td width="12%" class="principalLabel" align="right">
             		<bean:message key="prompt.site"/> 
             		<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> &nbsp;
             </td>
             <td width="45%"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr> 
                   <td width="94%" height="22"> 
                   	<html:text property="simaDsSitemarca" styleClass="text" maxlength="100"/>
                   </td>
                   <td width="6%">
					  <img src="webFiles/images/botoes/setaDown.gif" title="<bean:message key="prompt.confirma"/>"  width="21" height="18" class="geralCursoHand" onclick="submeteIncluirSite();">
				   </td>
                 </tr>
               </table>
             </td>
             <td width="43%"></td>
           </tr>
           <tr>
           		<td width="12%">&nbsp;</td>
  		       	<td width="45%" align="left" class="principalLabel"><bean:message key="prompt.observacao"/></td>
  		       	<td width="43%">&nbsp;</td>
           </tr>
           <tr>
           		<td width="12%">&nbsp;</td>
	           	<td width="45%" align="left">
	           		<html:textarea property="simaTxObservacao" styleClass="text" rows="3" onkeyup="textCounter(this, 1000)" onblur="textCounter(this, 1000)" />
	           	</td>
	           	<td width="43%">&nbsp;</td>
           </tr>
           <tr> 
             <td colspan="3" class="principalLabel" align="right">&nbsp; </td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
           <tr> 
           	 <td width="4%" class="principalLstCab">&nbsp;</td>
             <td width="96%" class="principalLstCab">Site</td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="160">
           <tr> 
             <td valign="top"> 
                 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                   <tr> 
                     <td height="160">
                     	<iframe id=ifrmLstSite  name="ifrmLstSite" src="AdministracaoCsCdtbMarcafornecedorMafo.do?tela=<%= MAConstantes.IFRM_LST_SITE%>&acao=none" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidtd="0" marginheight="0" ></iframe>
                     </td>                     
                   </tr>
                 </table>
             </td>
           </tr>
         </table>
</html:form>
</body>
</html>