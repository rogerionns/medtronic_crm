<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
	
<html>
<head>
<title>CSI - PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">	
<!--ifrmLstSite.jsp-->	

var contSites = 0;

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form styleId="administracaoCsCdtbMarcafornecedorMafoForm" action="/AdministracaoCsCdtbMarcafornecedorMafo.do" >
<html:hidden property="acao" />
<html:hidden property="tela" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLstPar">
		<logic:present name="csCdtbSitemarcaSimaVector">
		  <logic:iterate id="ccttrtVector" name="csCdtbSitemarcaSimaVector" indexId="sequencia"> 
		  	  <script>contSites++;</script>	
			  <tr> 
			    <td width="4%" class="principalLstPar"> <img src="webFiles/images/botoes/lixeira18x18.gif"  name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.submeteExcluirSite('<%=sequencia.intValue()+1 %>','<bean:write name="ccttrtVector" property="idMafoCdMarcafornecedor" />', '<bean:write name="ccttrtVector" property="idSimaCdSitemarca" />','<bean:write name="ccttrtVector" property="simaDsSitemarca" />')">
			    </td>
			    <td class="principalLstParMao" align="left" width="96%" onclick="parent.submeteFormEditSite('<%=sequencia.intValue()+1 %>','<bean:write name="ccttrtVector" property="idMafoCdMarcafornecedor" />','<bean:write name="ccttrtVector" property="idSimaCdSitemarca" />','<bean:write name="ccttrtVector" property="simaDsSitemarca" />','<%=sequencia%>')">     	
			    	<bean:write name="ccttrtVector" property="simaDsSitemarca" />
			    	<input type="hidden" name="txtObsevacao" value='<bean:write name="ccttrtVector" property="simaTxObservacao" />'>   
			    </td>
			  </tr>
		  </logic:iterate>
		 </logic:present>
	</table>
</html:form>
</body>
</html>