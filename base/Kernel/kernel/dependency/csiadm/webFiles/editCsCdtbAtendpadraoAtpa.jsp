<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<%
//Chamado: 81098 - 24/09/2012 - Carlos Nunes
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
var divChkTipoManifInativoDisplay = "";

function inicio(){
	showError('<%=request.getAttribute("msgerro")%>');
	try{
		validarPermissao();
	
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
			ifrmCmbCsCdtbLinhaLinh.document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.value = 1;
			verificaCmbProdutoManifJaCarregou();
		<%}%>	
	} catch(e){
		setTimeout("inicio();",100);
	}

	//Chamado: 81098 - 24/09/2012 - Carlos Nunes
	setaChavePrimaria(administracaoCsCdtbAtendpadraoAtpaForm.idAtpdCdAtendpadrao.value);
}	

function verificaCmbProdutoManifJaCarregou() {
	if(ifrmCmbProdutoManif.document.readyState == 'complete') {
		if(ifrmCmbProdutoManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn1CdAssuntonivel1.value<=0){
			ifrmCmbCsCdtbLinhaLinh.atualizaProduto();
		}
	} else {
		setTimeout('verificaCmbProdutoManifJaCarregou()', 200);
	}
}

function validarPermissao(){
	<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_ATENDIMENTOPADRAO_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_ATENDIMENTOPADRAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbAtendpadraoAtpaForm.imgGravar);	
			desabilitaCamposAtendpadrao();
		}
	</logic:equal>		
}

function desabilitaCamposAtendpadrao(){
	try{
		ifrmCmbComoLocalizou.document.administracaoCsCdtbAtendpadraoAtpaForm.idColoCdComolocalizou.disabled=true;
		ifrmCmbMidia.document.administracaoCsCdtbAtendpadraoAtpaForm.idMidiCdMidia.disabled=true;
		ifrmCmbFormaContato.document.administracaoCsCdtbAtendpadraoAtpaForm.idFocoCdFormacontato.disabled=true;
		ifrmCmbTipoRetorno.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpreCdTiporetorno.disabled=true;
		ifrmCmbEstadoAnimo.document.administracaoCsCdtbAtendpadraoAtpaForm.idEsanCdEstadoanimo.disabled=true;
		ifrmCmbAtendente.document.administracaoCsCdtbAtendpadraoAtpaForm.idFuncCdAtendente.disabled=true;
		ifrmCmbCsCdtbLinhaLinh.document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.disabled=true;
		ifrmCmbProdutoManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn1CdAssuntonivel1.disabled=true;
		ifrmCmbManifestacao.document.administracaoCsCdtbAtendpadraoAtpaForm.idMatpCdManiftipo.disabled=true;
		ifrmCmbGrupo.document.administracaoCsCdtbAtendpadraoAtpaForm.idGrmaCdGrupomanifestacao.disabled=true;
		ifrmCmbTipoManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpmaCdTpmanifestacao.disabled=true;
	//	ifrmCmbArea.document.administracaoCsCdtbAtendpadraoAtpaForm.idAreaCdArea.disabled=true;
	//	ifrmCmbResponsavel.document.administracaoCsCdtbAtendpadraoAtpaForm.idFuncCdResponsavel.disabled=true;
		ifrmCmbStatusManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idStmaCdStatusmanif.disabled=true;
		ifrmCmbGrauSatisf.document.administracaoCsCdtbAtendpadraoAtpaForm.idGrsaCdGrausatisfacao.disabled=true;
		ifrmCmbTipo.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpinCdTipoinformacao.disabled=true;
		ifrmCmbTopico.document.administracaoCsCdtbAtendpadraoAtpaForm.idToinCdTopicoinformacao.disabled=true;
		
		for (var i = 0; i < document.administracaoCsCdtbAtendpadraoAtpaForm.length; i++) { 
			if (document.administracaoCsCdtbAtendpadraoAtpaForm.elements[i].type != 'hidden')
				document.administracaoCsCdtbAtendpadraoAtpaForm.elements[i].disabled= true;	
		}
	}
	catch(e){
		setTimeout("desabilitaCamposAtendpadrao();",2000);
	}
}


function ativaDetalhe() {
	// caso esteja selecionado manifestacao
	if (document.administracaoCsCdtbAtendpadraoAtpaForm.atpaInTpatendimento[0].checked) {
		manifestacao.style.display = 'block';
		informacao.style.display = 'none';
		divChkTipoManifInativo.style.display = 'block';
	}
	// caso esteja selecionado informacao
	else {
		manifestacao.style.display = 'none';
		informacao.style.display = 'block';
		divChkTipoManifInativo.style.display = 'none';
	}
}

function carregaProdAssunto(){
	<%//if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
	if (document.administracaoCsCdtbAtendpadraoAtpaForm.chkAssunto.checked == true)
		ifrmCmbCsCdtbLinhaLinh.document.administracaoCsCdtbAtendpadraoAtpaForm.prasInProdutoassunto.value = "N";
	else{
		ifrmCmbCsCdtbLinhaLinh.document.administracaoCsCdtbAtendpadraoAtpaForm.prasInProdutoassunto.value = "S";
	}	
	<%//}%>

	ifrmCmbCsCdtbLinhaLinh.document.administracaoCsCdtbAtendpadraoAtpaForm.target = "ifrmCmbCsCdtbLinhaLinh";
	ifrmCmbCsCdtbLinhaLinh.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = "<%= MAConstantes.TELA_CMB_LINHA %>";
	ifrmCmbCsCdtbLinhaLinh.document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = "<%= MAConstantes.ACAO_POPULACOMBO %>";
	ifrmCmbCsCdtbLinhaLinh.document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.selectedIndex = 0;
	ifrmCmbCsCdtbLinhaLinh.document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
}

function carregaProdAssuntoInfo(){
	<%//if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
	if (document.administracaoCsCdtbAtendpadraoAtpaForm.chkAssuntoInfo.checked == true)
		ifrmCmbLinhaInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.prasInProdutoassunto.value = "N";
	else{
		ifrmCmbLinhaInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.prasInProdutoassunto.value = "S";
	}	
	<%//}%>

/*<iframe id="ifrmCmbLinhaInfo" name="ifrmCmbLinhaInfo" src="AdministracaoCsCdtbAtendpadraoAtpa.do?
acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_LINHA_INFO %>
&idToinCdTopicoinformacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idToinCdTopicoinformacao"/>
&idTpinCdTipoinformacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idTpinCdTipoinformacao"/>
&idAsnCdAssuntoNivel=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsnCdAssuntoNivel"/>
&idAsn1CdAssuntonivel1=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsn1CdAssuntonivel1"/>
&idAsn2CdAssuntonivel2=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsn2CdAssuntonivel2"/>
&idLinhCdLinha=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idLinhCdLinha"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
*/

	ifrmCmbLinhaInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.target = "ifrmCmbLinhaInfo";
	ifrmCmbLinhaInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = "<%= MAConstantes.TELA_CMB_LINHA_INFO %>";
	ifrmCmbLinhaInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = "<%= MAConstantes.ACAO_POPULACOMBO %>";
	ifrmCmbLinhaInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.selectedIndex = 0;
	ifrmCmbLinhaInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
}

</script>
<body class= "principalBgrPageIFRM" onload="inicio();">

<html:form styleId="administracaoCsCdtbAtendpadraoAtpaForm" action="/AdministracaoCsCdtbAtendpadraoAtpa.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo"/>		
	<html:hidden property="idColoCdComolocalizou"/>
	<html:hidden property="idMidiCdMidia"/>
	<html:hidden property="idFocoCdFormacontato"/>
	<html:hidden property="idEsanCdEstadoanimo"/>
	<html:hidden property="idTpreCdTiporetorno"/>
	<html:hidden property="idFuncCdAtendente"/>
	<html:hidden property="idLinhCdLinha"/>
	<html:hidden property="idAsnCdAssuntoNivel"/>
	<html:hidden property="idMatpCdManiftipo"/>
	<html:hidden property="idSugrCdSupergrupo"/>
	<html:hidden property="idGrmaCdGrupomanifestacao"/>
	<html:hidden property="idTpmaCdTpmanifestacao"/>
	<html:hidden property="idAreaCdArea"/>
	<html:hidden property="idFuncCdResponsavel"/>
	<html:hidden property="idStmaCdStatusmanif"/>
	<html:hidden property="idTpinCdTipoinformacao"/>
	<html:hidden property="idToinCdTopicoinformacao"/>
	<html:hidden property="idCompCdSequencial"/>
	<html:hidden property="idGrsaCdGrausatisfacao"/>
	<html:hidden property="idAsn1CdAssuntonivel1"/>
	<html:hidden property="idAsn2CdAssuntonivel2"/>
	<html:hidden property="prasInProdutoassunto"/>
	
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr valign="top"> <!--Jonathan | Adequa��o para o IE 10-->
          <td align="right" class="principalLabel" width="150"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td> 
          	<table width="100%" align="center" cellspacing="0"
						cellpadding="0">
						<tbody>
							<tr>
								<td><html:text
										property="idAtpdCdAtendpadrao" styleClass="text"
										disabled="true" style="width:120px" />
										</td> 
										<td align="right" class="principalLabel"><bean:message key="prompt.descricao" /> <img
									src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
								</td>
								<td><html:text property="atpdDsAtendpadrao"
										styleClass="text" maxlength="60" style="width: 228px;" /></td>
							</tr>
						</tbody>
					</table>
				</td>
        </tr>
        <!-- Chamado 78931 - Vinicius - Inclusao do grupo de atendimento padrao -->
         <tr> 
	    	<td align="right" class="principalLabel"><bean:message key="prompt.grupoAtendPadrao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    <td><!--Jonathan | Adequa��o para o IE 10-->
		    	<table width="100%" align="center" cellspacing="0" cellpadding="0">
			    	<tr>
			    		<td width="30%">
				    		<html:select property="idGrapCdGrupoatpa" styleClass="principalObjForm">
							<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
							    <logic:present name="csCdtbGrupoatpaGrapVector">
								  <html:options collection="csCdtbGrupoatpaGrapVector" property="field(id_grap_cd_grupoatpa)" labelProperty="field(grap_ds_grupoatpa)"/>
								</logic:present>
						 	</html:select>
			    		</td>
			    		<% // 91850 - 28/10/2013 - Jaider Alba - Cod. Corp. %>
			    		<td width="23%" align="right" class="principalLabel">
			    			<bean:message key="prompt.codigoCorporativo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
		    			</td>
			    		<td width="30%">
			    			<html:text property="atpaDsCodCorporativo" styleClass="text" />
			    		</td>
			    		<td>&nbsp;</td>
			    	</tr>
		    	</table>
		    </td>
		</tr>
        
        <tr> 
	    	<td align="right" class="principalLabel"><bean:message key="prompt.observacao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    <td>
		    	<table width="100%" align="center" cellspacing="0" cellpadding="0">
          		<tr>
          		<td class="principalLabel" width="450" align="left">
		    		<html:textarea property="atpdTxObservacao" style="width:372px" styleClass="text" rows="2" cols="50" onkeypress="textCounter(this, 1000)" onkeyup="textCounter(this, 1000)"/>
		    	</td>
		    	<td>&nbsp;</td>
		    	</tr>
		    	</table>
		    </td>
		</tr>
		
		<!-- Como localizou  & Midia -->
		<tr> 
	    	<td align="right" class="principalLabel"><bean:message key="prompt.comoLocalizou"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    <td> <!--Jonathan | Adequa��o para o IE 10-->
		    	<table width="100%" align="center" cellspacing="0" cellpadding="0">
		    	<tr>
		    		<td width="30%"><iframe id="ifrmCmbComoLocalizou" name="ifrmCmbComoLocalizou" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_COMOLOCALIZOU %>&idColoCdComolocalizou=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idColoCdComolocalizou"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px"></iframe></td>
		    		<td width="23%" align="right" class="principalLabel"><bean:message key="prompt.midia"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    		<td width="30%"><iframe id="ifrmCmbMidia" name="ifrmCmbMidia" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_MIDIA %>&idMidiCdMidia=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idMidiCdMidia"/>&idColoCdComolocalizou=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idColoCdComolocalizou"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe></td>
		    		<td>&nbsp;</td>
		    	</tr>
		    	</table>
		    </td>
		</tr>
		
		<!-- Forma contato & Tipo retorno -->
		<tr> 
	    	<td align="right" class="principalLabel"><bean:message key="prompt.formaContato"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    <td>
		    	<table width="100%" align="center" cellspacing="0" cellpadding="0">
		    	<tr>
		    		<td width="30%"><iframe id="ifrmCmbFormaContato" name="ifrmCmbFormaContato" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_FORMACONTATO %>&idFocoCdFormacontato=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idFocoCdFormacontato"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe></td>
		    		<td width="23%" align="right" class="principalLabel"><bean:message key="prompt.tipoRetorno"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    		<td width="30%"><iframe id="ifrmCmbTipoRetorno" name="ifrmCmbTipoRetorno" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_TIPORETORNO %>&idTpreCdTiporetorno=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idTpreCdTiporetorno"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe></td>
		    		<td>&nbsp;</td>
		    	</tr>
		    	</table>
		    </td>
		</tr>

		<!-- Estado de animo & Atendente -->
		<tr> 
	    	<td align="right" class="principalLabel"><bean:message key="prompt.estadoAnimo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    <td>
		    	<table width="100%" align="center" cellspacing="0" cellpadding="0">
		    	<tr>
		    		<td width="30%"><iframe id="ifrmCmbEstadoAnimo" name="ifrmCmbEstadoAnimo" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_ESTADOANIMO %>&idEsanCdEstadoanimo=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idEsanCdEstadoanimo"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe></td>
		    		<td width="23%" align="right" class="principalLabel"><bean:message key="prompt.atendente"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    		<td width="30%"><iframe id="ifrmCmbAtendente" name="ifrmCmbAtendente" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_ATENDENTE %>&idFuncCdAtendente=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idFuncCdAtendente"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe></td>
		    		<td>&nbsp;</td>
		    	</tr>
		    	</table>
		    </td>
		</tr>
		
		<tr> 
          <td align="right" class="principalLabel"><bean:message key="prompt.tempoAtendimento"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td>
          	<table>
          		<tbody>
          			<tr>
          				<td><html:text property="atpaNrTempoatendimento" styleClass="text" style="width:125px" maxlength="2" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/></td>
          <td class="principalLabel" align="center"></td>
          <td width="237.5" align="right" class="principalLabel">
          <html:checkbox value="true" property="atpaDhInativo"/>
				&nbsp;<bean:message key="prompt.inativo"/>
          </td>
          			</tr>
          		</tbody>
          	</table>
          	</td>
          
        </tr>
        
        <!-- Tipo de atendimento -->
         <tr>
        	<td align="right" class="principalLabel"><bean:message key="prompt.tipoAtendimento"/>
            	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
        	</td>
        	<td> 
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td class="principalLabel">
				        	<html:radio value="M" property="atpaInTpatendimento" onclick="ativaDetalhe();" />
				        </td>
				        <td class="principalLabel">
				        	<bean:message key="prompt.manifestacaofixo"/>
				        </td>
				        <td class="principalLabel">
				        	<html:radio value="I" property="atpaInTpatendimento" onclick="ativaDetalhe();"/>
				        </td>
				        <td class="principalLabel">
				        	<bean:message key="prompt.informacao"/>
				        </td>
				        <td class="principalLabel" align="right" width="200">
	        	<% // 91850 - 28/10/2013 - Jaider Alba - Web %>
	        	<html:checkbox value="true" property="atpaInWeb"/>&nbsp;<bean:message key="prompt.adm.web"/>
	        </td>
				     </tr>
				</table>
    	    </td>
	        
        </tr>

		<tr>
			<td valign="top" colspan="2">
				<table width="100%" border="0" cellspacing="5" cellpadding="0" align="center">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr>
								<td>
								<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionadoResultAnalise" id="detalhe" name="detalhe">
										<bean:message key="prompt.detalheAtendimento" />
									</td>
								</tr>
								</table>
							</td>
						</tr>
						</table>	
					
						<table height="100%" width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
								<td valign="top" height="190px" class="principalBordaQuadro">
							<!-- MANIFESTACAO -->
							<logic:equal name="administracaoCsCdtbAtendpadraoAtpaForm" property="atpaInTpatendimento" value="M">
								<script>divChkTipoManifInativoDisplay = 'block';</script>						
								&nbsp;<div name="manifestacao" id="manifestacao" style="width: 95%; height: 150px;display: block"> 
							</logic:equal>
							<logic:notEqual name="administracaoCsCdtbAtendpadraoAtpaForm" property="atpaInTpatendimento" value="M">
								<script>divChkTipoManifInativoDisplay = 'none';</script>							
								&nbsp;<div name="manifestacao" id="manifestacao" style="width: 95%; height: 150px;display: none"> 
							</logic:notEqual>							
							
								<table width="96%" border="0" cellspacing="0" cellpadding="0" align="left">
								
								<!-- Linha  & Produto -->
								<tr id="trChkAssunto">
									<td colspan=2 class="principalLabel" align="right">
	  									<div>
	  									<input type="checkbox" name="chkAssunto" onclick="carregaProdAssunto();" />
					          			<bean:message key="prompt.assunto"/>
            							</div>
									</td>
									<td colspan="3"></td>
								</tr>
								<tr> 
					          		<td id="tdLblLinha" align="right" class="principalLabel">
	  									<div>
					          			<%= getMessage("prompt.linha", request)%>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
            							</div>
            						</td>
          							<td id="tdObjLinha" > 
	  									<div>
            							<iframe id="ifrmCmbCsCdtbLinhaLinh" name="ifrmCmbCsCdtbLinhaLinh" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_LINHA %>&idLinhCdLinha=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idLinhCdLinha"/>&idAsnCdAssuntoNivel=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsnCdAssuntoNivel"/>&idAsn1CdAssuntonivel1=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsn1CdAssuntonivel1"/>&idAsn2CdAssuntonivel2=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsn2CdAssuntonivel2"/>&idMatpCdManiftipo=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idMatpCdManiftipo"/>&idGrmaCdGrupomanifestacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idGrmaCdGrupomanifestacao"/>&idSugrCdSupergrupo=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idSugrCdSupergrupo"/>&idTpmaCdTpmanifestacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idTpmaCdTpmanifestacao"/>&prasInProdutoassunto=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="prasInProdutoassunto"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
          								</div>
          							</td>
          							<td align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel1", request)%>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
            						</td>
          							<td colspan="2"> 
            							<iframe id="ifrmCmbProdutoManif" name="ifrmCmbProdutoManif" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
          							</td>
          						</tr>
									
									<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
										<script>
											//document.getElementById("trChkAssunto").style.display = "none";
											document.getElementById("tdLblLinha").style.display = "none";
											document.getElementById("tdObjLinha").style.display = "none";
										</script>
									<%}%>
									
          						<!-- Variedade -->
          						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
								<tr>
									<td align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel2", request)%>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td colspan="4"> 
										<iframe id="ifrmCmbVariedadeManif" name="ifrmCmbVariedadeManif" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>          							
									</td>
          						</tr>
          						<%}%>
          						<!-- Manifestacao & Supergrupo -->
								<tr> 
					          		<td width="19%" align="right" class="principalLabel"><%= getMessage("prompt.manifestacao", request)%>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
            						</td>
          							<td 
          							<%if (!Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
          								width="66%" colspan="3"
          							<%} else { %>
          							 width="25%" 
          							 <%}%>> 
            							<iframe id="ifrmCmbManifestacao" name="ifrmCmbManifestacao" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
<!--           							</td> -->

									<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
									</td>
          							<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.Supergrupo"/>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
           							</td>
          							<td width="25%" > 
            							<iframe id="ifrmCmbSupergrupo" name="ifrmCmbSupergrupo" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
          							</td>
          							<% } %>
          							<td width="15%">
          							<%//Chamado 70392 - Vinicius - Incluido check chkTipoManifInativo para filtrar TPMA inativos 
									  //AdequacaoSafari - 10/12/2013 - Jaider Alba: Movido por quest�es de layout %>
										<div name="divChkTipoManifInativo" id="divChkTipoManifInativo" class="principalLabel" style="width: 65px; height: 20px; display: block">
											<input type="checkbox" id="chkTipoManifInativo" name="chkTipoManifInativo" onclick="ifrmCmbGrupo.atualizaTipoManif();">&nbsp;<bean:message key="prompt.inativo"/>
										</div>
									</td>
          						</tr>          						
          						<!-- Grupo & Tipo de manifestacao -->
								<tr> 
					          		<td align="right" class="principalLabel"><%= getMessage("prompt.grupomanif", request)%>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td colspan="4"> 
          								<iframe id="ifrmCmbGrupo" name="ifrmCmbGrupo" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
            						</td>
          						</tr>
          						
          						<tr>
									<td align="right" class="principalLabel"><%= getMessage("prompt.tipomanif", request)%>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td colspan="4" > 
            							<iframe id="ifrmCmbTipoManif" name="ifrmCmbTipoManif" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
          							</td>
          						</tr>
          						
          						<!-- Area & Responsavel -->
								<!-- tr> 
					          		<td width="12%" align="right" class="principalLabel"><bean:message key="prompt.area"/>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td width="33%"> 
            							<iframe id="ifrmCmbArea" name="ifrmCmbArea" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_AREA %>&idAreaCdArea=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAreaCdArea"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20" ></iframe>
          							</td>

									<td width="12%" align="right" class="principalLabel"><bean:message key="prompt.responsavel"/>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td width="33%" colspan="2"> 
            							<iframe id="ifrmCmbResponsavel" name="ifrmCmbResponsavel" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_RESPONSAVEL %>&idFuncCdResponsavel=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idFuncCdResponsavel"/>&idAreaCdArea=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAreaCdArea"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20" ></iframe>
          							</td>
          						</tr-->
          						
          						<!-- Status da manifestacao & Grau de satisfacao -->
								<tr> 
					          		<td width="19%" align="right" class="principalLabel"><bean:message key="prompt.status"/>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td width="32%"> 
            							<iframe id="ifrmCmbStatusManif" name="ifrmCmbStatusManif" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_STATUSMANIF %>&idStmaCdStatusmanif=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idStmaCdStatusmanif"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
          							</td>

									<td width="18%" align="right" class="principalLabel"><bean:message key="prompt.grauSatisfacao"/>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td	 colspan="2"> 
            							<iframe id="ifrmCmbGrauSatisf" name="ifrmCmbGrauSatisf" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_GRAUSATISF %>&idGrsaCdGrausatisfacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idGrsaCdGrausatisfacao"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
          							</td>
          						</tr>
          						
								
								<tr> 
					          		<td align="center" class="principalLabel" colspan="5">
										<html:checkbox value="true" property="atpaInManifconcluir"/>
            							<bean:message key="prompt.concluirTempoAtendimento"/>
          							</td>
          						</tr>
									 <tr>
									 	<td colspan="5">&nbsp;</td>
									 </tr>         									
								</table>							
        					</div>
							
							<!-- INFORMACAO -->
							<logic:equal name="administracaoCsCdtbAtendpadraoAtpaForm" property="atpaInTpatendimento" value="I">							
						   		<div name="informacao" id="informacao" style="width: 95%; height: 150px;display: block"> 
					   		</logic:equal>
					   		<logic:notEqual name="administracaoCsCdtbAtendpadraoAtpaForm" property="atpaInTpatendimento" value="I">
						   		<div name="informacao" id="informacao" style="width: 95%; height: 150px;display: none"> 
					   		</logic:notEqual>
					   			<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center"><br>
								
								<!-- Linha  & Produto -->
								<tr id="trChkAssuntoInfo">
									<td colspan=2 width="3%" class="principalLabel" align="right">
	  									<div>
	  									<input type="checkbox" name="chkAssuntoInfo" onclick="carregaProdAssuntoInfo();" />
					          			<bean:message key="prompt.assunto"/>
            							</div>
									</td>
									<td></td>
								</tr>
								<tr> 
					          		<td id="tdLblLinhaInfo" width="10%" align="right" class="principalLabel">
	  									<div>
					          			<%= getMessage("prompt.linha", request)%>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
            								</div>
          							<td id="tdObjLinhaInfo" width="35%"> 
	  									<div>
            							<iframe id="ifrmCmbLinhaInfo" name="ifrmCmbLinhaInfo" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_LINHA_INFO %>&idToinCdTopicoinformacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idToinCdTopicoinformacao"/>&idTpinCdTipoinformacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idTpinCdTipoinformacao"/>&idAsnCdAssuntoNivel=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsnCdAssuntoNivel"/>&idAsn1CdAssuntonivel1=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsn1CdAssuntonivel1"/>&idAsn2CdAssuntonivel2=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsn2CdAssuntonivel2"/>&idLinhCdLinha=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idLinhCdLinha"/>&prasInProdutoassunto=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="prasInProdutoassunto"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
          								</div>
          							</td>
          							<td width="15%" align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel1", request)%>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td width="35%"> 
            							<iframe id="ifrmCmbProdutoInfo" name="ifrmCmbProdutoInfo" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_PRODUTO_INFO %>&idAsnCdAssuntoNivel=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsnCdAssuntoNivel"/>&idAsn1CdAssuntonivel1=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsn1CdAssuntonivel1"/>&idLinhCdLinha=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idLinhCdLinha"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
          							</td>
          						</tr>

								<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
									<script>
										document.getElementById("trChkAssuntoInfo").style.dislay = "none";
										document.getElementById("tdLblLinhaInfo").style.display = "none";
										document.getElementById("tdObjLinhaInfo").style.display = "none";
									</script>
								<%}%>

         						<!-- Variedade -->
          						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
								<tr>
									<td width="12%" align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel2", request)%>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td width="33%"> 
										<iframe id="ifrmCmbVariedadeInfo" name="ifrmCmbVariedadeInfo" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>          							
									</td>
          						</tr>
          						<%}%>
								
								<!-- Tipo  & Topico -->
								<tr> 
					          		<td width="10%" align="right" class="principalLabel"><bean:message key="prompt.tipo"/>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td width="35%"> 
            							<iframe id="ifrmCmbTipo" name="ifrmCmbTipo" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_TIPO %>&idTpinCdTipoinformacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idTpinCdTipoinformacao"/>&idAsnCdAssuntoNivel=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsnCdAssuntoNivel"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
          							</td>
          							<td width="15%" align="right" class="principalLabel"><bean:message key="prompt.topico"/>
            							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          							<td width="35%"> 
            							<iframe id="ifrmCmbTopico" name="ifrmCmbTopico" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_CMB_TOPICO %>&idToinCdTopicoinformacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idToinCdTopicoinformacao"/>&idTpinCdTipoinformacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idTpinCdTipoinformacao"/>&idAsnCdAssuntoNivel=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsnCdAssuntoNivel"/>" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20px" ></iframe>
          							</td>
          						</tr>
								
          						<!-- Texto informacao -->
								<tr>
									<td colspan="4">
									<table align="center" width="100%">
									<tr>
										<td width="5%">
											<div align="center"><img src="webFiles/images/icones/informacao.gif" width="22" height="22"></div>
										</td>	
										<td class="principalTituloBlocoVerde">
											<bean:message key="prompt.informacao"/>
										</td>
									</tr>
									</table>
									</td>
								</tr>			    
								<tr> 
					          		<td colspan="4" class="principalLabel"> 
											<iframe id="ifrmTextoInformacao" name="ifrmTextoInformacao" src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%= MAConstantes.ACAO_POPULACOMBO %>&tela=<%= MAConstantes.TELA_TEXTO_INFORMACAO %>&idAsn1CdAssuntonivel1=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsn1CdAssuntonivel1"/>&idAsn2CdAssuntonivel2=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idAsn2CdAssuntonivel2"/>&idTpinCdTipoinformacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idTpinCdTipoinformacao"/>&idToinCdTopicoinformacao=<bean:write name="administracaoCsCdtbAtendpadraoAtpaForm" property="idToinCdTopicoinformacao"/>" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="50px"></iframe>
          							</td>
          						</tr>

          						</table>
							</div>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
	  </table>
	  <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
          		<tr>
		    			<td colspan="3">&nbsp;</td>
		    		</tr>
		    		<tr>
		    			<td class="principalLabel" width="150px" align="right">
					    	<bean:message key="prompt.textoAtendimento"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					    </td>
		          		<td class="principalLabel" align="left" valign="top">
		    		<html:textarea property="atpaTxTexto" style="height: 33px; width: 500px" styleClass="text" rows="3" cols="50" onkeypress="textCounter(this, 4000)" onkeyup="textCounter(this, 4000)"/>
		    	</td>
		    	<td>&nbsp;</td>
		    	</tr>
		    	</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			for (var i = 0; i < document.administracaoCsCdtbAtendpadraoAtpaForm.length; i++) { 
				if (document.administracaoCsCdtbAtendpadraoAtpaForm.elements[i].type != 'hidden')
					document.administracaoCsCdtbAtendpadraoAtpaForm.elements[i].disabled= true;	
			}
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_ATENDIMENTOPADRAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbAtendpadraoAtpaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_ATENDIMENTOPADRAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposAtendpadrao();
			}
			document.administracaoCsCdtbAtendpadraoAtpaForm.idAtpdCdAtendpadrao.disabled= false;
			document.administracaoCsCdtbAtendpadraoAtpaForm.idAtpdCdAtendpadrao.value= '';
			document.administracaoCsCdtbAtendpadraoAtpaForm.idAtpdCdAtendpadrao.disabled= true;
			document.administracaoCsCdtbAtendpadraoAtpaForm.atpaNrTempoatendimento.value = '';
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbAtendpadraoAtpaForm.atpdDsAtendpadrao.focus();}
	catch(e){}
	divChkTipoManifInativo.style.display = divChkTipoManifInativoDisplay;
</script>