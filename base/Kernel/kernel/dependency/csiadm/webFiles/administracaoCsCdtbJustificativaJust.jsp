<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsCdtbJustificativaJustForm" action="/AdministracaoCsCdtbJustificativaJust.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="erro"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idJustCdJustificativa"/>

<script>var possuiRegistros=false;</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"  class="sortable">
  <logic:iterate id="ccttrtVector" name="csCdtbJustificativaJustVector" indexId="sequencia">
  <script>possuiRegistros=true;</script>
  <tr> 
    <td width="3%" class="principalLstParMao">
      <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="idJustCdJustificativa" />')">
    </td>

    <td width="3%" class="principalLstParMao"> 
    	<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMAJUST_IDJU.xml','<bean:write name="ccttrtVector" property="idJustCdJustificativa" />')">
    </td>
    
    <td class="principalLstParMao" width="13%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idJustCdJustificativa" />')">
      <bean:write name="ccttrtVector" property="idJustCdJustificativa" />
    </td>
    <td class="principalLstParMao" align="left" width="32%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idJustCdJustificativa" />')">
      <%=acronymChar(((br.com.plusoft.csi.adm.vo.CsCdtbJustificativaJustVo)ccttrtVector).getResuDsResultado(), 29)%>
	</td>    
    <td class="principalLstParMao" align="left" width="30%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idJustCdJustificativa" />')">
      <%=acronymChar(((br.com.plusoft.csi.adm.vo.CsCdtbJustificativaJustVo)ccttrtVector).getJustDsJustificativa(), 29)%>
	</td>
	<td sorttable_customkey="<plusoft:format name="ccttrtVector" property="justDhInativo" inputformat="DD/MM/YYYY HH:mm:SS" outputformat="YYYYMMDDHHmmSS" />" class="principalLstParMao" align="center" width="25%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idJustCdJustificativa" />')">
	&nbsp;
      <bean:write name="ccttrtVector" property="justDhInativo" />
       <script>
      	if('<bean:write name="ccttrtVector" property="justDhInativo" />' == '')
      	{
          	document.write('<font color="#f4f4f4">00/00/0000</font>');
      	}
      </script>
	</td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_JUSTIFICATIVA_EXCLUSAO_CHAVE%>', editCsCdtbJustificativaJustForm.lixeira);
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>