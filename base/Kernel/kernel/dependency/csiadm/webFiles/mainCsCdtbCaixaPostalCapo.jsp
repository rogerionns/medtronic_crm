<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true" />

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript"
	src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsCdtbCaixaPostalCapoForm.target = admIframe.name;
	document.administracaoCsCdtbCaixaPostalCapoForm.acao.value ='filtrar';
	document.administracaoCsCdtbCaixaPostalCapoForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbCaixaPostalCapoForm.filtro.value = '';
}

function submeteFormIncluir() {
	editIframe.document.administracaoCsCdtbCaixaPostalCapoForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbCaixaPostalCapoForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_CAIXAPOSTAL_CAPO%>';
	editIframe.document.administracaoCsCdtbCaixaPostalCapoForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbCaixaPostalCapoForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigoServico, codigo){
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.idServCdServico.value = codigoServico;
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoNrSequencia.value = codigo;
	
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_CAIXAPOSTAL_CAPO%>';
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.target = editIframe.name;
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){

	if(tab.document.administracaoCsCdtbCaixaPostalCapoForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_CAIXAPOSTAL_CAPO%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbCaixaPostalCapoForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_CAIXAPOSTAL_CAPO%>'){

		if (tab.document.administracaoCsCdtbCaixaPostalCapoForm.idServCdServico.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_um_servico"/>.");
			tab.document.administracaoCsCdtbCaixaPostalCapoForm.idServCdServico.focus();
			return false;
		}

		//Chamado: 86104 - 26/12/2012 - Carlos Nunes
		if (tab.document.administracaoCsCdtbCaixaPostalCapoForm.idAsmeCdAssuntoMail.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_um_assunto_de_E-mail"/>.");
			tab.document.administracaoCsCdtbCaixaPostalCapoForm.idAsmeCdAssuntoMail.focus();
			return false;
		}
		
		if (trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPoolRec.value) == "") {
			 if (tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsServidorRec.value == "") {
				alert("<bean:message key="prompt.capo.O_campo_servidor_e_obrigatorio"/>.");
				tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsServidorRec.focus();
				return false;
			}else{
				if (trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPortaRec.value) == "") {
					alert("<bean:message key="prompt.capo.O_campo_porta_e_obrigatorio"/>.");
					tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPortaRec.focus();
					return false;
				}
				
				if (trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsProtocoloRec.value) == "") {
					alert("<bean:message key="prompt.capo.O_campo_protocolo_e_obrigatorio"/>.");
					tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsProtocoloRec.focus();
					return false;
				}
				
				if (trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsUsuarioRec.value) == "") {
					alert("<bean:message key="prompt.capo.O_campo_usuario_e_obrigatorio"/>.");
					tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsUsuarioRec.focus();
					return false;
				}
			}
		}
		
		if (trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPoolEnv.value) == "") {
			if (trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsServidorEnv.value) != "") {
				if (trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPortaEnv.value) == ""
						|| trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsProtocoloEnv.value) == "") {
					alert("<bean:message key="prompt.capo.mensagem.envio"/>");
					return false;
				}
			}else if(trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPortaEnv.value) != "") {
				if (trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsServidorEnv.value) == ""
						|| trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsProtocoloEnv.value) == "") {
					alert("<bean:message key="prompt.capo.mensagem.envio"/>");
					return false;
				}
			}else if(trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsProtocoloEnv.value) != "") {
				if (trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsServidorEnv.value) == ""
						|| trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPortaEnv.value) == "") {
					alert("<bean:message key="prompt.capo.mensagem.envio"/>");
					return false;
				}
			}else if(trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsEmailresposta.value) != "") {
				if (trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsServidorEnv.value) == ""
						|| trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsPortaEnv.value) == ""
						|| trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsProtocoloEnv.value) == "") {
					alert("<bean:message key="prompt.capo.mensagem.envio"/>");
					return false;
				}
			}

			if(trim(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoDsEmailresposta.value) == ""){
				if(!confirm("<bean:message key='prompt.email.resposta.vazio'/>"))
				{
					return false;
				}
			}
		}
		
		tab.document.administracaoCsCdtbCaixaPostalCapoForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_CAIXAPOSTAL_CAPO%>';
		if(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoNrSequencia.value > 0){
			tab.document.administracaoCsCdtbCaixaPostalCapoForm.acao.value = '<%= Constantes.ACAO_EDITAR%>';
		}else{
			tab.document.administracaoCsCdtbCaixaPostalCapoForm.acao.value = '<%= Constantes.ACAO_INCLUIR%>';
		}
		disableEnable(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoNrSequencia, false);
		tab.document.administracaoCsCdtbCaixaPostalCapoForm.submit();
		disableEnable(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoNrSequencia, true);
		
		/* tab.document.administracaoCsCdtbCaixaPostalCapoForm.target = admIframe.name;
		disableEnable(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoNrSequencia, false);
		tab.document.administracaoCsCdtbCaixaPostalCapoForm.submit();
		disableEnable(tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoNrSequencia, true); */
		
		
		//cancel();

	}
}

function submeteExcluir(codigoServico, codigo) {
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.idServCdServico.value = codigoServico;
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.capoNrSequencia.value = codigo;
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_CAIXAPOSTAL_CAPO%>';
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.target = editIframe.name;
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbCaixaPostalCapoForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbCaixaPostalCapoForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_CAIXAPOSTAL_CAPO%>';
		editIframe.document.administracaoCsCdtbCaixaPostalCapoForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbCaixaPostalCapoForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbCaixaPostalCapoForm.capoNrSequencia, false);
		editIframe.document.administracaoCsCdtbCaixaPostalCapoForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbCaixaPostalCapoForm.capoNrSequencia, true);
	}else{
		cancel();	
	}
}

function cancel(){

	admIframe.location = 'AdministracaoCsCdtbCaixaPostalCapo.do?acao=visualizar';
	editIframe.location = 'AdministracaoCsCdtbCaixaPostalCapo.do?tela=editCsCdtbCaixaPostalCapo&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
//Chamado item 58 - 26/11/2014 - Leonardo Marquini Facchini
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsCdtbCaixaPostalCapoForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbCaixaPostalCapoForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_CAIXAPOSTAL_CAPO%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormal');
			break;
		case editIframe:
			tab.document.administracaoCsCdtbCaixaPostalCapoForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_CAIXAPOSTAL_CAPO%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionado');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000"
	onload="showError('<%=request.getAttribute("msgerro")%>')">

	<html:form styleId="administracaoCsCdtbCaixaPostalCapoForm"
		action="/AdministracaoCsCdtbCaixaPostalCapo.do">

		<html:hidden property="modo" />
		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<html:hidden property="topicoId" />

		<body class="principalBgrPage" text="#000000">
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td width="100%" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							height="100%" align="center">
							<tr>
								<td class="principalQuadroPst" height="100%">&nbsp;</td>
								<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
								<td height="100%" width="4"
									style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="principalBgrQuadro" valign="top"><br>
						<table width="99%" border="0" cellspacing="0" cellpadding="0"
							align="center">
							<tr>
								<td height="254">
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										align="center">
										<tr>
											<td class="principalPstQuadroLinkVazio">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="principalPstQuadroLinkSelecionado"
															id="tdDestinatario" name="tdDestinatario"
															onClick="AtivarPasta(admIframe);"><bean:message
																key="prompt.procurar" />
															<!-- ## --></td>
														<td class="principalPstQuadroLinkNormal"
															id="tdManifestacao" name="tdManifestacao"
															onClick="AtivarPasta(editIframe);"><bean:message
																key="prompt.caixaPostal" />
															<!-- ## --></td>
													</tr>
												</table>
											</td>
											<td width="4"><img
												src="webFiles/images/separadores/pxTranp.gif" width="1"
												height="1"></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										align="center">
										<tr>
											<td valign="top" class="principalBgrQuadro" height="400"><br>
												<!--                 Chamado item 58 - 26/11/2014 - Leonardo Marquini Facchini -->
												<div name="Manifestacao" id="Manifestacao"
													style="width: 100%; height: 400px; display: none">
													<table width="99%" border="0" cellspacing="0"
														cellpadding="0" align="center">
														
														<tr>
															<td height="380">
																<!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
																<iframe id=editIframe name="editIframe"
																	src="AdministracaoCsCdtbCaixaPostalCapo.do?tela=editCsCdtbCaixaPostalCapo&acao=incluir"
																	width="100%" height="100%" scrolling="Default"
																	frameborder="0" marginwidth="0" marginheight="0"></iframe>
															</td>
														</tr>
													</table>
												</div> <!--                 Chamado item 58 - 26/11/2014 - Leonardo Marquini Facchini -->
												<div name="Destinatario" id="Destinatario"
													style="width: 99%; height: 400px; display: block">
													<table width="99%" border="0" cellspacing="0"
														cellpadding="0" align="center">
														<tr>
															<td class="principalLabel" width="11%"><bean:message
																	key="prompt.servidor" /></td>
															<td class="principalLabel" colspan="2">&nbsp;</td>
														</tr>
														<tr>
															<td width="65%">
																<table border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="25%"><html:text property="filtro"
																				styleClass="principalObjForm"
																				onkeydown="if(event.keyCode==13) filtrar();" /></td>
																		<td width="05%">&nbsp;<img
																			src="webFiles/images/botoes/setaDown.gif" width="21"
																			height="18" class="geralCursoHand"
																			onclick="filtrar()"
																			title='<bean:message key="prompt.aplicarFiltro"/>'>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class="principalLabel" width="11%">&nbsp;</td>
															<td class="principalLabel" colspan="2">&nbsp;</td>
														</tr>
													</table>
													<table width="97%" border="0" cellspacing="0"
														cellpadding="0" align="center">
														<tr>
															<td>
																<table width="97%" border="0" cellspacing="0"
														cellpadding="0">
																	<thead>
																		<tr>
																			<th class="principalLstCab" width="3%">&nbsp;</th>
																			<th align="left" class="principalLstCab" width="10%"><bean:message
																					key="prompt.codigo" /></th>
																			<th align="left" class="principalLstCab" width="36%"><bean:message
																					key="prompt.servico" /></th>
																			<!-- td class="principalLstCab" width="21%"><bean:message key="prompt.servidor"/></td-->
																			<th align="left" class="principalLstCab" width="39%"><bean:message
																					key="prompt.assuntoEMail" /></th>
																			<th align="left" class="principalLstCab" align="left"
																				width="12%"><bean:message key="prompt.inativo" /></th>
																		</tr>
																	</thead>
																</table>
															</td>
														</tr>
														<tr valign="top">
															<td height="320">
																<!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
																<iframe id=admIframe name="admIframe"
																	src="AdministracaoCsCdtbCaixaPostalCapo.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
																	width="100%" height="98%" scrolling="no"
																	frameborder="0" marginwidth="0" marginheight="0"></iframe>
															</td>
														</tr>
													</table>
												</div></td>
											<td width="4"
												style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
										</tr>
										<tr>
											<td width="100%" height="4"><img
												src="webFiles/images/linhas/horSombra.gif" width="100%"
												height="4"></td>
											<td width="4" height="4"><img
												src="webFiles/images/linhas/cntInferiorDireito.gif"
												width="4" height="4"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="webFiles/images/separadores/pxTranp.gif"
									width="1" height="3"></td>
							</tr>
						</table>
						<table border="0" cellspacing="0" cellpadding="4" align="right">
							<tr align="center">
								<td width="60" align="right"><img
									src="webFiles/images/botoes/new.gif" name="imgIncluir"
									width="14" height="16" class="geralCursoHand"
									title="<bean:message key='prompt.novo'/>"
									onclick="clearError();submeteFormIncluir()"></td>
								<td width="20"><img src="webFiles/images/botoes/gravar.gif"
									name="imgGravar" width="20" height="20" class="geralCursoHand"
									title="<bean:message key='prompt.gravar'/>"
									onclick="clearError();submeteSalvar();"></td>


								<td width="20"><img
									src="webFiles/images/botoes/cancelar.gif" width="20"
									height="20" class="geralCursoHand"
									title="<bean:message key='prompt.cancelar'/>"
									onclick="clearError();cancel();"></td>

							</tr>
						</table>
						<table align="center">
							<tr>
								<td><label id="error"> </label></td>
							</tr>
						</table></td>
					<td width="4" height="100%"
						style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%"><img
						src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
					<td width="4"><img
						src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
						height="4"></td>
				</tr>
			</table>
	</html:form>

	<script language="JavaScript">
	var tab = admIframe ;
	
	desabilitaListaEmpresas();
	
</script>

	<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_CAIXAPOSTAL_INCLUSAO_CHAVE%>', document.administracaoCsCdtbCaixaPostalCapoForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_CAIXAPOSTAL_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_CAIXAPOSTAL_ALTERACAO_CHAVE%>
		')) {
			document.administracaoCsCdtbCaixaPostalCapoForm.imgGravar.disabled = true;
			document.administracaoCsCdtbCaixaPostalCapoForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbCaixaPostalCapoForm.imgGravar.title = '';
		}
	</script>

</body>
</html>
