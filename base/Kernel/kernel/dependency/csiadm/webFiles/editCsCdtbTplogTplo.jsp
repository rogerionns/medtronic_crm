<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>


<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>


<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposTplog(){
		document.administracaoCsCdtbTplogTploForm.tploDsTplog.disabled= true;	
		document.administracaoCsCdtbTplogTploForm.tploDhInativo.disabled= true;
		document.administracaoCsCdtbTplogTploForm.idTploCdTplog.disabled= true;
		document.administracaoCsCdtbTplogTploForm.idResuCdResultado.disabled= true;
		document.administracaoCsCdtbTplogTploForm.tploNrReagendar.disabled= true;
		document.administracaoCsCdtbTplogTploForm.tploInLivre.disabled= true;
		document.administracaoCsCdtbTplogTploForm.tploInResultado.disabled= true;
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEM_AGENDAMENTO_RETORNO,request).equals("N")) {%>
		document.administracaoCsCdtbTplogTploForm.tploInAgendRetorno.disabled= true;
		<%}%>
	}
	
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbTplogTploForm.idTploCdTplog.value);
	}
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbTplogTploForm" action="/AdministracaoCsCdtbTplogTplo.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />
	<input type="hidden" name="limparSessao" value="false"></input>

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="14%"> <html:text property="idTploCdTplog" styleClass="text" disabled="true" /> 
    </td>
    <td width="40%">&nbsp;</td>
    <td width="26%">&nbsp;</td>
  </tr>
  <tr>
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.tpresultado"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      	<html:select property="idResuCdResultado" styleClass="principalObjForm" > 
          <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
          <logic:present name="resultadoList">
          	<html:options collection="resultadoList" property="idResuCdResultado" labelProperty="resuDsResultado"/> 
          </logic:present>
        </html:select>       
    </td>
    <td width="26%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="tploDsTplog" styleClass="text" maxlength="60" /> 
    </td>
    <td width="26%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.camapanha.nrReagendar"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="tploNrReagendar" styleClass="text" maxlength="5" onkeypress="mascara(this,soNumeros)" onblur="mascara(this,soNumeros); return false;"/> 
    </td>
    <td width="26%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.codigoCorporativo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="tploDsCodcorporativo" styleClass="text" maxlength="20"/> 
    </td>
    <td width="26%">&nbsp;</td>
  </tr>
  
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.codTelefonia"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="tploDsCodtelefonia" styleClass="text" maxlength="20"/> 
    </td>
    <td width="26%">&nbsp;</td>
  </tr>
  
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.comissao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:checkbox value="true" property="tploInComissao"/> 
    </td>
    <td width="26%">&nbsp;</td>
  </tr>
  
  <tr> 
    <td width="20%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  
  <tr> 
    <td width="20%">&nbsp;</td>
    <td width="14%">&nbsp;</td>
    <td class="principalLabel" width="40%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right"> <html:checkbox value="true" property="tploInLivre"/></td>
          <td class="principalLabel">&nbsp;<bean:message key="prompt.camapanha.inLivre"/></td>
        </tr>
      	
        <tr> 
          <td align="right"> <html:checkbox value="true" property="tploInResultado"/></td>
          <td class="principalLabel">&nbsp;<bean:message key="prompt.visivelEmResultado"/></td>
        </tr>
        
        <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEM_AGENDAMENTO_RETORNO,request).equals("N")) {%>
        <tr> 
          <td align="right"> <html:checkbox value="true" property="tploInAgendRetorno"/></td>
          <td class="principalLabel">&nbsp;<bean:message key="prompt.visivelEmAgendamentoRetorno"/></td>
        </tr>
        <%}%>
        <tr> 
          <td align="right"> <html:checkbox value="true" property="tploDhInativo"/></td>
          <td class="principalLabel">&nbsp;<bean:message key="prompt.inativo"/></td>
        </tr>
        
      </table>
    </td>
    <td width="26%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposTplog();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_TIPOLOG_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbTplogTploForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_TIPOLOG_INCLUSAO_CHAVE%>')){
				desabilitaCamposTplog();
			}else{
				document.administracaoCsCdtbTplogTploForm.idTploCdTplog.disabled= false;
				document.administracaoCsCdtbTplogTploForm.idTploCdTplog.value= '';
				document.administracaoCsCdtbTplogTploForm.tploNrReagendar.value= '';
				document.administracaoCsCdtbTplogTploForm.idTploCdTplog.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_TIPOLOG_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_TIPOLOG_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbTplogTploForm.imgGravar);	
				desabilitaCamposTplog();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbTplogTploForm.idResuCdResultado.focus();}
	catch(e){}
</script>