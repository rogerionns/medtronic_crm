<%@page import="br.com.plusoft.licenca.helper.ModuloHelper"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.Geral" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<%
	String caminhoLogoTipo = "";
	CsCdtbEmpresaEmprVo empresaVo = null;
	CsCdtbFuncionarioFuncVo funcionarioVo = null;
	long idEmprCdEmpresa = 0;
	long idFuncCdFuncionario = 0;
	String funcDsLoginname = "";
	String funcNmFuncionario = "";
	String idPsf2CdPlusoft3 = "";
	
	try{
		if(request.getSession()!= null && request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) != null) {
			empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	
			idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
		}
		
		if(request.getSession()!= null && request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null) {
			funcionarioVo = (CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			idFuncCdFuncionario = funcionarioVo.getIdFuncCdFuncionario();
			funcDsLoginname = funcionarioVo.getFuncDsLoginname();
			funcNmFuncionario = funcionarioVo.getFuncNmFuncionario();
			if(funcionarioVo.getIdPsf2CdPlusoft3()!=null) idPsf2CdPlusoft3 = funcionarioVo.getIdPsf2CdPlusoft3();
		}
		caminhoLogoTipo = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CAMINHO_LOGOTIPO_CLIENTE,idEmprCdEmpresa);
	}catch(Exception e){}
		
	if(caminhoLogoTipo == null || caminhoLogoTipo.equals("")){
		caminhoLogoTipo = "";
	}
	
	request.getSession(true).setAttribute("idModuCdModulo", String.valueOf(ModuloHelper.CODIGO_MODULO_CADASTRO));
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<html>
<head>
<title><bean:message key="prompt.moduloDeCadastroMSD"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/global.css" type="text/css">
<script language="JavaScript" src="funcoes/variaveis.js"></script>
<script language="JavaScript" src="funcoes/funcoes.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script language="JavaScript">

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function FechaJanela(){
	if(confirm("<bean:message key="prompt.Tem_certeza_que_finalizar_o_sistema"/>")){
		podeFecharSistema=true;
		window.close();
	}
}

var podeFecharSistema = false;

function encerrarSistema(){
	if(!podeFecharSistema && '<%=funcDsLoginname%>'!='') {
		var dt = new Date();
		var dd = ("0"+dt.getDate());
		if(dd.length>2) 
			dd=dd.substring(1, 3);
		
		var mm = ("0"+(new Number(dt.getMonth())+1));
		if(mm.length>2) 
			mm=mm.substring(1, 3);
		
		var yy = dt.getFullYear();
		var hh = ("0"+dt.getHours());
		if(hh.length>2) 
			hh=hh.substring(1, 3);
		var mi = ("0"+dt.getMinutes());
		if(mi.length>2) 
			mi=mi.substring(1, 3);
		var ss = ("0"+dt.getSeconds());
		if(ss.length>2) 
			ss=ss.substring(1, 3);
		
		//alert("O sistema est� sendo finalizado.\n\n);
		//return false;
		var avs = "<plusoft:message key="prompt.confirm.encerraSistema" />";
			avs+= "[<%=funcDsLoginname%>] - ";
			avs+= dd +"/"+ mm +"/"+ yy +" "+ hh +":"+ mi +":"+ ss;
		return avs;
	}
}

//Chamado: 79676 - Carlos Nunes - 20/03/2012
/* window.onbeforeunload = function (e) {
  e = e || window.event;

 // For IE and Firefox prior to version 4
  if (e) {
	 e.returnValue = encerrarSistema();
  }

  unloadSistema();
  
  // For Safari
  return  encerrarSistema();
}; */

var unloading = false;
window.onbeforeunload = function (evt) {
	unloading = true;
	if (typeof evt == 'undefined') {
		evt = window.event;
	}

	if (evt) {
		evt.returnValue = encerrarSistema();
	}

	return encerrarSistema();
};

setInterval(function(){
    if(unloading){
        unloading = false;
        setTimeout(function(){}, 1000);
    }
}, 400);

window.onunload = function(){
	unloadSistema();
};

function unloadSistema(){
	var wnd = window.open("", "logoutadm", "top=190,left=250,status=no,width=300,height=50,center=yes");

	wnd.document.open();
	wnd.document.write("<html><head><title><bean:message key="prompt.moduloDeCadastroMSD"/></title></head><body>");
	wnd.document.write("<table width=\"100%\"><tr><td width=\"10%\"><img src=\"/plusoft-resources/images/plusoft-logo-128.png\" style=\"width: 40px; \" /></td><td style=\"font-family: Arial,sans-serif; font-size: 11px;\"><plusoft:message key="prompt.aguarde.finaliza" /></td></tr></table>");
	wnd.document.write("<form action=\"/csiadm/Logout.do\" name=\"logoutForm\" method=\"POST\">");
	wnd.document.write("<input type=\"hidden\" name=\"l\" value=\"<%=idPsf2CdPlusoft3 %>\" />");
	wnd.document.write("</form>");
	wnd.document.write("</body></html>");
	wnd.document.close();

	wnd.document.forms[0].submit();
	wnd.setTimeout("window.close()", 3000);

	podeFecharSistema = true;
}


</script>

<script>
	
var desabilitado = true;
var desabilitadoIdioma = true;
var arquivoXml = "";
var chavePrimaria = "";

function inicio(){
	if("<%=caminhoLogoTipo%>" != ""){
		document.getElementById("divLogo").innerHTML = "<img id='imgLogo' src='../" + "<%=caminhoLogoTipo%>" +  "'></img>";
	}
}

function abrirModalMultiempresa(){
	
	if(desabilitado==true){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item"/>');
		return false;
	}
	
	var url = '../AdministracaoEmpresa.do?acao=consultar&tela=ifrmLstEmpresas';
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:340px');

}

function abrirModalInternacional(){

	if(desabilitadoIdioma==true){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item"/>');
		return false;
	}
	
	var url = '../Idioma.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=ifrmIdioma&arquivoXml=' + arquivoXml + '&chavePrimaria=' + chavePrimaria;
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:340px');

}

var recarregaIfrmConteudoCount = 0;
function recarregaIfrmConteudo(){
	// Aguarda o iFrame de Permiss�o carregar antes de carregar o menu
	if(ifrmPermissao.permissaoReady==undefined || ifrmPermissao.permissaoReady==false) {
		setTimeout("recarregaIfrmConteudo();", 100);
		return false;
	}
	
	<%//Chamado: 99716 KERNEL-939 - 11/03/2015 - Marcos Donato%>
//	console.log('indexFrame.jsp.before');
//	console.log('indexFrame.jsp.count=' + recarregaIfrmConteudoCount);
	try {
		if( recarregaIfrmConteudoCount < 20 ) {
			if( !ifrmCmbEmpresa ) {
				recarregaIfrmConteudoCount++;
//				console.log('indexFrame.jsp.ifrmCmbEmpresa');
				setTimeout("recarregaIfrmConteudo();", 100);
				return false;
			} else {
				if(ifrmCmbEmpresa.urlEspec != '' ) {
					if( !ifrmCmbEmpresa.ifrmSessaoEspec ){
						recarregaIfrmConteudoCount++;
//						console.log('indexFrame.jsp.ifrmCmbEmpresa.ifrmSessaoEspec');
						setTimeout("recarregaIfrmConteudo();", 100);
						return false;
					} else {
						if ( !ifrmCmbEmpresa.ifrmSessaoEspec.document.readyState || ifrmCmbEmpresa.ifrmSessaoEspec.document.readyState != 'complete' ) {
							recarregaIfrmConteudoCount++;
//							console.log('indexFrame.jsp.ifrmCmbEmpresa.ifrmSessaoEspec.readyState');
							setTimeout("recarregaIfrmConteudo();", 100);
							return false;
						}
					}
				}
			}
		}
	} catch (e) {
		alert(e);
	}
//	console.log('indexFrame.jsp.after');
	<%//Chamado: 99716 KERNEL-939 - 11/03/2015 - Marcos Donato - FIM%>
	
	ifrmMenu.location.href="/csiadm/Menu.do?acao=consultar&resource=AdmWEBmenu.xml";
	ifrmConteudo.location.reload();
	
}

function carregarLogo(caminho){

	if(caminho != ""){
		document.getElementById("divLogo").innerHTML = "<img id='imgLogo' src='../" + caminho +  "'></img>";
	}
}

function recarregarLogoTipo(){

	ifrmRecarregaLogotipo.location = "webFiles/ifrmRecarregarLogotipo.jsp"

}

var listaIdEmprCdEmpresaAux;
var listaIdNiveCdNivelacessoAux;
var emprInPrincipalAux;
var csCdtbEmpresaEmprAssocVectorAux;
var listaIdEmprCdEmpresaOriginalAux;

function setDadosEmpresa(listaIdEmprCdEmpresa, listaIdNiveCdNivelacesso, emprInPrincipal, csCdtbEmpresaEmprAssocVector){
	
	try{
		
		listaIdEmprCdEmpresaAux = listaIdEmprCdEmpresa;
		listaIdNiveCdNivelacessoAux = listaIdNiveCdNivelacesso;
		emprInPrincipalAux = emprInPrincipal;
		csCdtbEmpresaEmprAssocVectorAux = csCdtbEmpresaEmprAssocVector;
			
		
		}catch(e){}
} 

</script>

</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" class="principalBgrPage" onload="inicio();" onbeforeunload="return encerrarSistema();" onunload="unloadSistema();" style="overflow: hidden;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="18%" height="15"><img src="images/logo/banner_new_adm.gif" width="179" height="47"></td>
    <td width="82%" height="15" valign="bottom" background="images/logo/bgr_superior.jpg"> 
      <table width="59%" border="0" cellspacing="0" cellpadding="0" height="27">
        <tr> 
          <td height="25">
          	
          	<div id="divEmpresa" style="text-align:right; position:absolute; left:150; top:8; width:505px; height:30; z-index:1; visibility:visible"> 
				  <table border="0" width="100%">
					  <tr>
						  <td width="18%" class="principalLabel" align="right">
							   <bean:message key="prompt.empresa"/>:
						   </td>
						  <td width="50%">
						  	
						  	<iframe id=ifrmCmbEmpresa name="ifrmCmbEmpresa" src="../AdministracaoEmpresa.do?acao=consultar&tela=ifrmCmbEmpresa" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="20"></iframe>
						  	
						   </td>
						   <td width="8%" valign="top" title="<bean:message key="prompt.empresasCadastradas"/>">
							   <div id="divDesabilita" style="position:absolute; width:35px; height:35px; z-index:1; background-color: F4F4F4; layer-background-color: F4F4F4; visibility: visible" class="desabilitado" title="<bean:message key="prompt.empresasCadastradas"/>">&nbsp;</div>
							   	<img id="imgLstEmpresas" class="geralCursoHand" src="images/botoes/textfolder.gif" width="28" height="23" onClick="abrirModalMultiempresa()" title="<bean:message key="prompt.empresasCadastradas"/>">
						   </td>
						     <td width="8%" valign="top" title="<bean:message key="prompt.idiomas"/>">
							   <div id="divInternacional" style="position:absolute; width:35px; height:35px; z-index:1; background-color: F4F4F4; layer-background-color: F4F4F4; visibility: visible" class="desabilitado" title="<bean:message key="prompt.idiomas"/>">&nbsp;</div>
							   	<img id="imgInternacional" class="geralCursoHand" src="images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="28" height="23" onClick="abrirModalInternacional()">
						   </td>
						    <td width="16%" valign="top">
						    	&nbsp;
						    </td>
					   </tr>
				   </table>				   
       		</div>
       		
          	<div id="divLogo" style="text-align:right; position:absolute; left:835; top:1; width:105px; height:30; z-index:100; visibility:visible"></div>
            <div id="Layer1" style="position:absolute; left:955px; top:2px; width:53px; height:43px; z-index:1"><img src="/plusoft-resources/images/plusoft-logo-128.png" style="width:40px;"></div>
             <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
            <div id="Layer2" style="position:absolute; left:644px; top:695px; width:412px; height:42px; z-index:2"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="22">
                <tr> 
                  <td background="images/background/inferiorMarquee.gif">&nbsp;</td>
                </tr>
              </table>
            </div>
            <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
            <div id="LayerNomeUsuario" style="position:absolute; left:14px; top:685px; width:522px; height:17px; z-index:3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalLabel"><font color="#FFFFFF"><bean:message key="prompt.usuario"/> 
                    : <%=funcNmFuncionario %>
                 </font></td>
                </tr>
              </table>
            </div>
            <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
            <div id="Layer3" style="position:absolute; left:987px; top:652px; width:24px; height:35px; z-index:4" class="geralCursoHand" onClick="FechaJanela()" title="<bean:message key='prompt.sairSistema'/>"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td><img src="images/botoes/out.gif" width="25" height="25" border="0"></td>
                </tr>
                <tr> 
                  <td style="font-family: Arial, Helvetica, sans-serif; font-size: xx-small; text-align: center; "> 
                    <bean:message key="prompt.sair"/>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td height="7" bgcolor="#2E5494"></td>
    <td height="7" bgcolor="#2E5494"></td>
  </tr>
  <tr> 
  	<!-- Menu/menu.jsp -->
    <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
    <td valign="top" width="183px"><iframe id="ifrmMenu" name="ifrmMenu" src="" width="99%" height="631" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" ></iframe></td>
    <td valign="top" width="805px"><iframe id="ifrmConteudo" name="ifrmConteudo" src="centro.jsp" width="100%" height="590" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe></td>
  </tr>
  <tr> 
    <td valign="top" colspan="2"><img src="images/background/inferiorStatusBar.gif" width="645" height="30"></td>
  </tr>
</table>

<!--Permissao -->
<div id="permissaoDiv" style="position:absolute; width:99%; z-index:3; height: 0px; visibility: hidden">
	<iframe id="ifrmPermissao" 
			name="ifrmPermissao" 
			src="../AdministracaoPermissionamento.do?tela=<%= Geral.getActionProperty("permissaoFrame", idEmprCdEmpresa)%>&acao=<%= Constantes.ACAO_CONSULTAR%>&idModuCdModulo=<%= PermissaoConst.MODULO_CODIGO_CADASTRO%>" 
			width="0" 
			height="0" 
			scrolling="no" 
			marginwidth="0" 
			marginheight="0" 
			frameborder="0">
	</iframe>
</div>
<!--Permissao -->

<div id="LayerAguarde" style="position:absolute; left:450px; top:200px; width:199px; height:148px; z-index:1; visibility: hidden"> 
  	<div align="center"><iframe src="aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

<div id="divEmpresas" style="position:absolute; left:450px; top:200px; width:199px; height:148px; z-index:1; visibility: hidden"> 
  	
</div>

<!-- IFRM responsavel pelo reload de verifica��o de licen�as. -->
<iframe name="ifrmLicenca" id="ifrmLicenca" src="../Licenca.do?tela=ifrmLicenca&idPsf2CdPlusoft3=<%=idPsf2CdPlusoft3.replaceAll("#", "%23") %>" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
<!-- -->

<iframe id="ifrmRecarregaLogotipo" name="ifrmRecarregaLogotipo" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>


</body>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="/csiadm/webFiles/funcoes/funcoesMozilla.js"></script>