<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>

<script language="JavaScript">
	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbAssuntoNivel2Asn2Form.idAsn2CdAssuntoNivel2.value);
	}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form styleId="administracaoCsCdtbAssuntoNivel2Asn2Form" action="/AdministracaoCsCdtbAssuntoNivel2Asn2.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />	

	<br><!--Jonathan | Adequa��o para o IE 10-->
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="17%"> 
            <html:text property="idAsn2CdAssuntoNivel2" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.codigoCorporativo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="17%"> 
            <html:text property="asn2CdCorporativo" styleClass="text" maxlength="15" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="29%">&nbsp;</td>
        </tr>        
        <tr> 
          <td width="17%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="asn2DsAssuntoNivel2" styleClass="text" maxlength="60" style="width: 335px;" /> 
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="17%">&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td width="17%">&nbsp;</td>
          <td width="12%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="asn2DhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="29%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbAssuntoNivel2Asn2Form.asn2DsAssuntoNivel2.disabled= true;	
			document.administracaoCsCdtbAssuntoNivel2Asn2Form.asn2CdCorporativo.disabled= true;	
			document.administracaoCsCdtbAssuntoNivel2Asn2Form.asn2DhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			document.administracaoCsCdtbAssuntoNivel2Asn2Form.idAsn2CdAssuntoNivel2.disabled= false;
			document.administracaoCsCdtbAssuntoNivel2Asn2Form.idAsn2CdAssuntoNivel2.value= '';
			document.administracaoCsCdtbAssuntoNivel2Asn2Form.idAsn2CdAssuntoNivel2.disabled= true;
		</script>
</logic:equal>
</html>