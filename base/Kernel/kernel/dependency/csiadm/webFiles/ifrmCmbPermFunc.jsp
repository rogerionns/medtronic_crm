<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
function atualizaTreeView() {
	parent.parent.parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.administracaoPermissionamentoForm.acao.value = '<%= Constantes.ACAO_CONSULTAR %>'
	document.administracaoPermissionamentoForm.tela.value = '<%= MAConstantes.TELA_IFRM_TREE_VIEW %>';
	document.administracaoPermissionamentoForm.target = parent.ifrmTreeView.name;
	document.administracaoPermissionamentoForm.submit();
	limpaComboGrupo();
}	

function limpaComboGrupo() {
	document.administracaoPermissionamentoForm.acao.value = '<%= Constantes.ACAO_VISUALIZAR %>'
	document.administracaoPermissionamentoForm.tela.value = '<%= MAConstantes.TELA_IFRM_CMB_GRUPOFUNCIONARIO %>';
	document.administracaoPermissionamentoForm.target = parent.ifrmCmbGrupofuncionario.name;
	document.administracaoPermissionamentoForm.submit();
}

function inicio(){
	if(!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_PERMISSIONAMENTO_FUNCIONARIO_CHAVE%>')){
		document.administracaoPermissionamentoForm["csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].disabled = true;
	}else{
		document.administracaoPermissionamentoForm["csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].disabled = false;
	}
}
</script>

</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form action="/AdministracaoPermissionamento.do" styleId="administracaoPermissionamentoForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
		
	<html:select property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario" styleClass="principalObjForm" onchange="atualizaTreeView()">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		<html:options collection="funcionariosVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>
	</html:select>
</html:form>
</body>
</html>
