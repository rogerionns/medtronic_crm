<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="editCsAstbMateriaDetalheMadtForm" action="/AdministracaoCsAstbMateriaDetalheMadt.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="camdmVector" name="csAstbMateriaDetalheMadtVector"> 
  <tr> 
    <td class="principalLstParMao" width="4%" onclick="parent.parent.clearError();parent.parent.submeteExcluir('<bean:write name="camdmVector" property="csCdtbMateriaMateVo.idMateCdMateria" />', '<bean:write name="camdmVector" property="csCdtbDetalheClassDetcVo.idDetcCdDetalheClass" />', '<bean:write name="camdmVector" property="csCdtbDetalheClassDetcVo.csCdtbClassificacaoClasVo.idClasCdClassificacao" />', '<bean:write name="camdmVector" property="csCdtbDetalheClassDetcVo.csCdtbClassificacaoClasVo.csCdtbTpClassificacaoTpclVo.idTpclCdTpClassificacao" />')"> 
    	<img src="webFiles/images/botoes/lixeira18x18.gif" width="18" height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>"  > 
    </td>
    <td width="32%" class="principalLstPar">
       &nbsp;<bean:write name="camdmVector" property="csCdtbDetalheClassDetcVo.csCdtbClassificacaoClasVo.csCdtbTpClassificacaoTpclVo.tpclDsTpClassificacao" />
    </td>
    <td width="32%" class="principalLstPar">
       <bean:write name="camdmVector" property="csCdtbDetalheClassDetcVo.csCdtbClassificacaoClasVo.clasDsClassificacao" />
    </td>
    <td width="32%" class="principalLstPar">
       &nbsp;<bean:write name="camdmVector" property="csCdtbDetalheClassDetcVo.detcDsDetalheClass" />
    </td>
  </tr>
  </logic:iterate>
</table>
</html:form>
</body>
</html>