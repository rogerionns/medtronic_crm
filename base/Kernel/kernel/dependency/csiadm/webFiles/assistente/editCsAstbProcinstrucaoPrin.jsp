<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbProcedimentoProcVo"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

function MontaLista(){
	lstArqCarga.location.href= "AdministracaoCsAstbProcinstrucaoPrin.do?tela=administracaoLstCsAstbProcinstrucaoPrin&acao=<%=Constantes.ACAO_VISUALIZAR%>&csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao=" + administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].value;
	checaProcedimento();
}

function desabilitaCamposProcinstrucao(){
	administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].disabled= true;
	administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].disabled= true;
}

function checaProcedimento() {
	if( administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].value > 0 ) {
		administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.prinInTipo'][0].checked = true;
	}
}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista();">
<html:form styleId="administracaoCsAstbProcinstrucaoPrinForm" action="/AdministracaoCsAstbProcinstrucaoPrin.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.assistenteInstrucao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="3">
    	 <html:select property="csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao" styleClass="principalObjForm" onchange="MontaLista()">
			<html:option value=''><bean:message key="prompt.selecione_uma_opcao"/></html:option>
			<logic:present name="csCdtbInstrucaoInstVector">
				<html:options collection="csCdtbInstrucaoInstVector" property="field(id_inst_cd_instrucao)" labelProperty="field(inst_ds_instrucao)"/>
			</logic:present>
		  </html:select>
	 </td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.assistenteProcedimento"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="3">
    	<html:select property="csAstbProcinstrucaoPrinVo.idProcCdProcedimento" styleClass="principalObjForm"  > 
      	<html:option value=""><bean:message key="prompt.selecione_uma_opcao"/></html:option> 
			<logic:present name="csCdtbProcedimentoProcVector">
			  <html:options collection="csCdtbProcedimentoProcVector" property="field(id_proc_cd_procedimento)" labelProperty="field(proc_ds_procedimento)"/> 
			</logic:present>
       </html:select>
    </td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td class="principalLabel" width="28%"> </td>
    <td width="31%" class="principalLabel">
        <html:radio value="V" property="csAstbProcinstrucaoPrinVo.prinInTipo"/> 
        <bean:message key="prompt.tipoTextoVerificar"/> 
        <html:radio value="I" property="csAstbProcinstrucaoPrinVo.prinInTipo"/> 
        <bean:message key="prompt.tipoTextoInstrucao"/> 
    </td>
  </tr>
  <tr> 
    <td colspan="4" height="220"> 
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="6%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="36%"> <bean:message key="prompt.assistenteProcedimento"/></td>
          <td class="principalLstCab" width="33%">&nbsp;</td>
          <td class="principalLstCab" width="11%">&nbsp;</td>
          <td class="principalLstCab" width="14%">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
	      	administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].value=<bean:write name="baseForm" property="csAstbProcinstrucaoPrinVo.idProcCdProcedimento"/>;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
			administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].disabled= false;
			administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].disabled= false;
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_PROCEDIMENTOINSTRUCAO_INCLUSAO_CHAVE%>', parent.administracaoCsAstbProcinstrucaoPrinForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_PROCEDIMENTOINSTRUCAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposProcinstrucao();
			}else{
				administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].disabled= false;
				administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].disabled= false;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_PROCEDIMENTOINSTRUCAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_PROCEDIMENTOINSTRUCAO_ALTERACAO_CHAVE%>', parent.administracaoCsAstbProcinstrucaoPrinForm.imgGravar);	
				desabilitaCamposProcinstrucao();
			}
		</script>
</logic:equal>

</html>

<script>
	try{administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].focus();}
	catch(e){}
</script>