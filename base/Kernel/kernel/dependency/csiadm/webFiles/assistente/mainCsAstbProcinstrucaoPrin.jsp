<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	administracaoCsAstbProcinstrucaoPrinForm.target = admIframe.name;
	administracaoCsAstbProcinstrucaoPrinForm.acao.value ='filtrar';
	administracaoCsAstbProcinstrucaoPrinForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	administracaoCsAstbProcinstrucaoPrinForm.filtro.value = '';
}

function submeteFormIncluir() {
	editIframe.administracaoCsAstbProcinstrucaoPrinForm.target = editIframe.name;
	editIframe.administracaoCsAstbProcinstrucaoPrinForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_PROCINSTRUCAO_PRIN%>';
	editIframe.administracaoCsAstbProcinstrucaoPrinForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.administracaoCsAstbProcinstrucaoPrinForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo2,codigo3){
	//tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].value = codigo1;
	tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].value = codigo2;
	//tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].value = codigo3;
	tab.administracaoCsAstbProcinstrucaoPrinForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_PROCINSTRUCAO_PRIN%>';
	tab.administracaoCsAstbProcinstrucaoPrinForm.target = editIframe.name;
	tab.administracaoCsAstbProcinstrucaoPrinForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.administracaoCsAstbProcinstrucaoPrinForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){
	/*
	if(tab.administracaoCsAstbProcinstrucaoPrinForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_PROCINSTRUCAO_PRIN%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.administracaoCsAstbProcinstrucaoPrinForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_ASTB_PROCINSTRUCAO_PRIN%>'){
	*/

	if(window.document.all.item("Manifestacao").style.display == "none"){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else{	
		if (tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].value == "") {
			alert("<bean:message key="prompt.O_campo_Instrucao_e_obrigatorio"/>");
			tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].focus();
			return false;
		}
		else if(tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].value == "") {
			alert("<bean:message key="prompt.O_campo_Procedimento_e_obrigatorio"/>");
			tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].focus();
			return false;
		}
		
		tab.administracaoCsAstbProcinstrucaoPrinForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_PROCINSTRUCAO_PRIN%>';
		tab.administracaoCsAstbProcinstrucaoPrinForm.target = admIframe.name;
		disableEnable(tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'], false);
		tab.administracaoCsAstbProcinstrucaoPrinForm.submit();
		//disableEnable(tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'], true);
		//cancel();

		setTimeout("editIframe.lstArqCarga.location.reload()", 2500);
		tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].value = ""; 
		
	}
}

function submeteExcluir(codigo1,codigo2) {

	tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].value = codigo2;
	tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].value = codigo1;

	tab.administracaoCsAstbProcinstrucaoPrinForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_ASTB_PROCINSTRUCAO_PRIN%>';
	tab.administracaoCsAstbProcinstrucaoPrinForm.target = editIframe.name;
	tab.administracaoCsAstbProcinstrucaoPrinForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.administracaoCsAstbProcinstrucaoPrinForm.submit();

	/////////////////////////////////////
	//AtivarPasta(editIframe);
	//MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
	/////////////////////////////////////	
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.administracaoCsAstbProcinstrucaoPrinForm.tela.value = '<%= MAConstantes.TELA_EXCLUIR_CS_ASTB_PROCINSTRUCAO_PRIN%>';
		editIframe.administracaoCsAstbProcinstrucaoPrinForm.target = admIframe.name;
		editIframe.administracaoCsAstbProcinstrucaoPrinForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		//disableEnable(editIframe.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'], false);
		editIframe.administracaoCsAstbProcinstrucaoPrinForm.submit();
		//disableEnable(editIframe.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'], true);

		/////////////////////////////////
		//AtivarPasta(admIframe);
		//MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		//editIframe.location = 'AdministracaoCsAstbProcinstrucaoPrin.do?tela=editCsAstbProcinstrucaoPrin&acao=incluir';
		/////////////////////////////////
	
		//tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.csCdtbInstrucaoInstVo.idInstCdInstrucao'].value = "";
		tab.administracaoCsAstbProcinstrucaoPrinForm['csAstbProcinstrucaoPrinVo.idProcCdProcedimento'].value = "";
		setTimeout("editIframe.lstArqCarga.location.reload()", 2000);
			
	}else{
		//cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsAstbProcinstrucaoPrin.do?tela=editCsAstbProcinstrucaoPrin&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsAstbProcinstrucaoPrinForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.administracaoCsAstbProcinstrucaoPrinForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_ASTB_PROCINSTRUCAO_PRIN%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			break;
		case editIframe:
			tab.administracaoCsAstbProcinstrucaoPrinForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_ASTB_PROCINSTRUCAO_PRIN%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsAstbProcinstrucaoPrinForm"	action="/AdministracaoCsAstbProcinstrucaoPrin.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td class="principalBgrQuadro" valign="top" height="580">
			<br>
				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td valign="top" height="520">
						
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td class="principalPstQuadroLinkVazio">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalPstQuadroLinkSelecionado"
													id="tdDestinatario" name="tdDestinatario"
													onClick="AtivarPasta(admIframe);"><bean:message
														key="prompt.procurar" /></td>
												<td class="principalPstQuadroLinkNormalGrande"
													id="tdManifestacao" name="tdManifestacao"
													onClick="AtivarPasta(editIframe);"><bean:message
														key="prompt.assistenteProcInstrucao" /></td>
											</tr>
										</table>
									</td>
									<td width="4"><img
										src="webFiles/images/separadores/pxTranp.gif" width="1"
										height="1"></td>
								</tr>
							</table>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td valign="top" class="principalBgrQuadro" height="490">
										<div name="Manifestacao" id="Manifestacao" style="position: relative; width: 100%; height: 400px; z-index: 6; display: none;">
											<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
												<tr>
													<td height="440">
														<iframe id=editIframe name="editIframe" src="AdministracaoCsAstbProcinstrucaoPrin.do?tela=editCsAstbProcinstrucaoPrin&acao=incluir" width="100%" height="440" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0"></iframe>
													</td>
												</tr>
											</table>
										</div>
										<div name="Destinatario" id="Destinatario" style="position: relative; width: 100%; height: 400px; z-index: 2;">
											<table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
											    <tr><td>&nbsp;</td></tr>
												<tr>
													<td width="20%" align="right" class="principalLabel"><bean:message key="prompt.assistenteProcInstrucao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td>
														<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
													</td>
													<td width="19%">
														&nbsp;<img
														src="webFiles/images/botoes/setaDown.gif" width="21"
														height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()"> 
													</td>
												</tr>
												<tr><td>&nbsp;</td></tr>
											</table>
											<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
												<tr>
													<td class="principalLstCab" width="8%">&nbsp;<bean:message key="prompt.codigo"/></td>
							                        <td class="principalLstCab" width="92%"><bean:message key="prompt.assistenteProcInstrucao"/> </td>
												</tr>
												<tr valign="top">
													<td colspan="5" height="300">
														<iframe id=admIframe name="admIframe" src="AdministracaoCsAstbProcinstrucaoPrin.do?acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="300" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0"></iframe>
													</td>
												</tr>
											</table>
										</div></td>
									<td width="4" height="490"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
								</tr>
								<tr>
									<td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
									<td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="1" height="3"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
					</tr>
				</table>
				<table border="0" cellspacing="0" cellpadding="4" align="right">
					<tr align="center">
						<td width="60" align="right"><img
							src="webFiles/images/botoes/new.gif" name="imgIncluir"
							width="14" height="16" class="geralCursoHand"
							title="<bean:message key='prompt.novo'/>"
							onclick="clearError();submeteFormIncluir()"></td>
						<td width="20"><img src="webFiles/images/botoes/gravar.gif"
							name="imgGravar" width="20" height="20" class="geralCursoHand"
							title="<bean:message key='prompt.gravar'/>"
							onclick="clearError();submeteSalvar();"></td>
						<td width="20"><img
							src="webFiles/images/botoes/cancelar.gif" width="20"
							height="20" class="geralCursoHand"
							title="<bean:message key='prompt.cancelar'/>"
							onclick="clearError();cancel();"></td>
					</tr>
				</table>
				<table align="center">
					<tr>
						<td><label id="error"> </label></td>
					</tr>
				</table></td>
			<td width="4" height="580"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="580"></td>
		</tr>
		<tr>
			<td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
			<td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		</tr>
	</table>
	
</html:form>
			
</body>

</html>

<script language="JavaScript">
	var tab = admIframe ;
	
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_PROCEDIMENTOINSTRUCAO_INCLUSAO_CHAVE%>', administracaoCsAstbProcinstrucaoPrinForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_PROCEDIMENTOINSTRUCAO_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_PROCEDIMENTOINSTRUCAO_ALTERACAO_CHAVE%>')){
			administracaoCsAstbProcinstrucaoPrinForm.imgGravar.disabled=true;
			administracaoCsAstbProcinstrucaoPrinForm.imgGravar.className = 'geralImgDisable';
			administracaoCsAstbProcinstrucaoPrinForm.imgGravar.title='';
	   }
</script>