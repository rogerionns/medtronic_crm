<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>
<script>
	
	var vExpandir = new Array();
	var nAltera = 0; 
	var nLoop = 0;
	var nIndex = 0;
	
	function executaExpandir(){
		try{
			if(vExpandir[nLoop] != undefined){
				if(nLoop == 0){
					expandir(vExpandir[nLoop][0], vExpandir[nLoop][1]);
					nLoop++;
				}
				else{
					if(document.getElementById(vExpandir[nLoop - 1][0] +"_Conteudo").carregou == "true"){
						expandir(vExpandir[nLoop][0], vExpandir[nLoop][1]);
						nLoop++;
					}
				}
			}
		}
		catch(e){
			alert(e.description);
		}

		if(vExpandir.length >= nLoop)
			setTimeout("executaExpandir();", 500);
	}
	
	function addProcedimento(nIdProcedimento, cDescProcedimento) {
	
	strTxt = "";
	
	strTxt += "		<div class=\"principalLabel\" id=\"" + nIdProcedimento + "\" onClick=\"expandir('" + nIdProcedimento + "','" + nIdProcedimento + "')\"> ";
	strTxt += "			    <img src=\"webFiles/images/icones/setaBola.gif\" width=\"12\" height=\"12\"> ";
	strTxt += "				<span class=\"geralCursoHand\">" + cDescProcedimento + "</span> ";
	strTxt += "				<img src=\"webFiles/images/botoes/new.gif\" name=\"imgIncluir\" width=\"13\" height=\"13\" class=\"geralCursoHand\"  title=\"<bean:message key='prompt.novoProcedimento'/>\" onclick=\"novo(" + nIdProcedimento + ")\"> ";
	strTxt += "		</div> ";
	strTxt += "		<div><img src=\"webFiles/images/separadores/pxTranp.gif\" width=\"0\" height=\"6\"></div>";
	strTxt += "		<div id=\"" + nIdProcedimento + "_Conteudo\" style=\"display:none\" carregou=\"false\" caminho=\"" + nIdProcedimento + "\">";
	strTxt += "			<div id=\"" + nIdProcedimento + "_Conteudo_TD\" class=\"principalLabel\" height=\"25\" colspan=\"2\"><img id=\"imgTranspAg\" name=\"imgTranspAg\" src=\"webFiles/images/separadores/pxTranp.gif\" width=\"20\" height=\"1\"><img src=\"webFiles/images/icones/setaBola.gif\" width=\"12\" height=\"12\"> ";
	strTxt += "				... ";
	strTxt += "			</div> ";
	strTxt += "		</div> ";
	
	document.getElementById("lstProcedimento").innerHTML += strTxt;
	validarPermissao();
	}
	
	var statusTr = new Array();
	statusTr[false] = "none";
	statusTr[true] = "block";
	
	function expandir(nId, nIdAtual){
		
		document.getElementById(nId+"_Conteudo").style.display = statusTr[document.getElementById(nId+"_Conteudo").style.display == "none"];
		administracaoCsCdtbReferenciaRefeForm.assistenteJunto.value = document.getElementById(nId+"_Conteudo").caminho;

		ifrmTree.administracaoCsCdtbReferenciaRefeForm.tela.value = '<%=MAConstantes.TELA_MAIN_LST_TREE%>';
		ifrmTree.administracaoCsCdtbReferenciaRefeForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
		ifrmTree.administracaoCsCdtbReferenciaRefeForm.assistenteJunto.value = nId;
		ifrmTree.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbProcedimentoProcEntradaVo.idProcCdProcedimento'].value = nIdAtual;
		ifrmTree.administracaoCsCdtbReferenciaRefeForm.caminhoAssistente.value = document.getElementById(nId+"_Conteudo").caminho;
		ifrmTree.administracaoCsCdtbReferenciaRefeForm.submit();
		//ifrmTree.location = "AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_LST_TREE%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&assistenteJunto=" + nId + "&csCdtbReferenciaRefeVo.csCdtbProcedimentoProcEntradaVo.idProcCdProcedimento=" + nIdAtual + "&caminhoAssistente=" + document.getElementById(nId+"_Conteudo").caminho;
		validarPermissao();
		if(nAltera=0){
			parent.ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.altera.value="false";
		}else{
			nAltera=1;
		}
		nAltera=0
	}

	function novo(nIdProcedimento){
		if(confirm("<bean:message key='prompt.desejaCriarNovoProcedimento'/>")){
			parent.administracaoCsCdtbReferenciaRefeForm.campoGravacao.value = nIdProcedimento;
			parent.ifrmCmbProcedimento.location.href = "AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_CMB_PROCEDIMENTO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbGrupoassistenteGrasVo.idGrasCdGrupoassistente=" + parent.ifrmCmbGrupoAssistente.administracaoCsCdtbReferenciaRefeForm['csCdtbGrupoassistenteGrasVo.idGrasCdGrupoassistente'].value + "&campoGravacao=" + nIdProcedimento;
			enableOpcoes();
			parent.ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.altera.value="false";
		}
	}
	
	function disableOpcoes(){
		parent.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.refeInUltimoprocedimento'].disabled = true;
		parent.ifrmCmbProcedimento.administracaoCsCdtbReferenciaRefeForm['csCdtbProcedimentoProcVo.idProcCdProcedimento'].disabled = true;
		parent.ifrmCmbAtendPadrao.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao'].disabled = true;
	}
	
	function enableOpcoes(){
		parent.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.refeInUltimoprocedimento'].disabled = false;
		parent.ifrmCmbProcedimento.administracaoCsCdtbReferenciaRefeForm['csCdtbProcedimentoProcVo.idProcCdProcedimento'].disabled = false;
		parent.ifrmCmbAtendPadrao.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao'].disabled = false;
	}
	
	function altera(idRef,Filho){
		if(confirm("<bean:message key='prompt.desejaRealmenteAlterarProcedimento'/>")){
			parent.administracaoCsCdtbReferenciaRefeForm.campoGravacao.value = Filho;
			parent.ifrmCmbProcedimento.location.href = "AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_CMB_PROCEDIMENTO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbGrupoassistenteGrasVo.idGrasCdGrupoassistente=" + parent.ifrmCmbGrupoAssistente.administracaoCsCdtbReferenciaRefeForm['csCdtbGrupoassistenteGrasVo.idGrasCdGrupoassistente'].value;
			parent.ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.idRefeCdReferencia'].value=idRef;
			enableOpcoes();
			parent.ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.altera.value="true";
			nAltera=1;
		}
	}

	function excluir(nIdReferencia){
		if(confirm("<bean:message key='prompt.desejaExcluirProcedimento'/>")){
			parent.ifrmGravarReferencia.location.href = "AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_GRAVAR%>&acao=<%=Constantes.ACAO_EXCLUIR%>&csCdtbReferenciaRefeVo.idRefeCdReferencia=" + nIdReferencia;
			administracaoCsCdtbReferenciaRefeForm.target=this.name="teste";
			document.location.reload();
		}
	}
	
	
	function validarPermissao(){
	try{
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_REFERENCIA_EXCLUSAO_CHAVE%>', administracaoCsCdtbReferenciaRefeForm.lixeira);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_REFERENCIA_INCLUSAO_CHAVE%>', administracaoCsCdtbReferenciaRefeForm.imgIncluir);	
	}
	catch(e){
		setTimeout("validarPermissao();",100);
	}
}
	
	
</script>
<body class="principalBgrPageIFRM" text="#000000" >
<html:form action="/AdministracaoCsCdtbReferenciaRefe.do" styleId="administracaoCsCdtbReferenciaRefeForm">
 <html:hidden property="acao" />
 <html:hidden property="tela" />
 <html:hidden property="assistenteJunto" />
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
   <tr> 
     <td height="290" valign="top"> 

		<!-- DIV DE PROCEDIMENTOS - TREE DE PROCEDIMENTOS -->

			<div id="lstProcedimento" style="width:100%; height:100%; overflow: auto; visibility: visible"> 	

			</div>

		<!-- DIV DE PROCEDIMENTOS - TREE DE PROCEDIMENTOS -->

     </td>
   </tr>
 </table>
 <table width="99%" border="0" cellspacing="0" cellpadding="0">
   <td><iframe id=ifrmTree name="ifrmTree" src="AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_LST_TREE%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="0" height="0"></iframe></td>
   
   
	  <logic:present name="csCdtbProcedimentoProcVector">
		<logic:iterate id="csCdtbProcedimentoProcVector" name="csCdtbProcedimentoProcVector">
		   <script>
			   	addProcedimento('<bean:write name="csCdtbProcedimentoProcVector" property="field(id_proc_cd_procedimento)"/>','<bean:write name="csCdtbProcedimentoProcVector" property="field(proc_ds_procedimento)"/>');
		   </script>
		</logic:iterate>
	  </logic:present>
   
 </table>
</html:form>
</body>
</html>

