<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


function carregaGrupo()
{
	var nIdAssunto;
	nIdAssunto = document.administracaoCsCdtbReferenciaRefeForm['csCdtbAssuntoassistenteAsasVo.idAsasCdAssuntoassistente'].value;
	if (nIdAssunto > 0 ){
		ifrmCmbGrupoAssistente.location.href = "AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_CMB_GRUPOASSISTENTE%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbGrupoassistenteGrasVo.csCdtbAssuntoassistenteAsasVo.idAsasCdAssuntoassistente=" + nIdAssunto;
	}
}

function gravar(){

	if(confirm("<bean:message key='prompt.desejaGravarProcedimentoSelecionado'/>")){
		var cGravar = "";
		if(administracaoCsCdtbReferenciaRefeForm.campoGravacao.value > 0){
			if(ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.altera.value!="true"){
				if(ifrmCmbProcedimento.administracaoCsCdtbReferenciaRefeForm['csCdtbProcedimentoProcVo.idProcCdProcedimento'].value > 0){
					cGravar = administracaoCsCdtbReferenciaRefeForm.campoGravacao.value + "_" + ifrmCmbProcedimento.administracaoCsCdtbReferenciaRefeForm['csCdtbProcedimentoProcVo.idProcCdProcedimento'].value;
	
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.tela.value = '<%=MAConstantes.TELA_MAIN_GRAVAR%>';
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao'].value = ifrmCmbAtendPadrao.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao'].value;
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.campoGravacao.value = cGravar;
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.refeInUltimoprocedimento'].value =administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.refeInUltimoprocedimento'].checked;
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.submit();
					
				}
			}else{
				if(ifrmCmbProcedimento.administracaoCsCdtbReferenciaRefeForm['csCdtbProcedimentoProcVo.idProcCdProcedimento'].value > 0){
					cGravar = administracaoCsCdtbReferenciaRefeForm.campoGravacao.value + "_" + ifrmCmbProcedimento.administracaoCsCdtbReferenciaRefeForm['csCdtbProcedimentoProcVo.idProcCdProcedimento'].value;
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.tela.value = '<%=MAConstantes.TELA_MAIN_GRAVAR%>';
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao'].value = ifrmCmbAtendPadrao.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao'].value;
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.campoGravacao.value = cGravar;
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.refeInUltimoprocedimento'].value =administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.refeInUltimoprocedimento'].checked;
					ifrmGravarReferencia.administracaoCsCdtbReferenciaRefeForm.submit();
					
				}
			}
		}
		
		administracaoCsCdtbReferenciaRefeForm.campoGravacao.value = "";
		ifrmCmbProcedimento.administracaoCsCdtbReferenciaRefeForm['csCdtbProcedimentoProcVo.idProcCdProcedimento'].value = "";
		ifrmCmbAtendPadrao.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao'].value = "";
		administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.refeInUltimoprocedimento'].checked = false;

		ifrmCmbProcedimento.administracaoCsCdtbReferenciaRefeForm['csCdtbProcedimentoProcVo.idProcCdProcedimento'].disabled = true;
		ifrmCmbAtendPadrao.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao'].disabled = true;
		administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.refeInUltimoprocedimento'].disabled = true;
		
	}

}

function cancelar(){

	administracaoCsCdtbReferenciaRefeForm.campoGravacao.value = "";
	ifrmCmbProcedimento.administracaoCsCdtbReferenciaRefeForm['csCdtbProcedimentoProcVo.idProcCdProcedimento'].value = "";
	ifrmCmbAtendPadrao.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao'].value = "";
	administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.refeInUltimoprocedimento'].checked = false;
	
	ifrmCmbProcedimento.administracaoCsCdtbReferenciaRefeForm['csCdtbProcedimentoProcVo.idProcCdProcedimento'].disabled = true;
	ifrmCmbAtendPadrao.administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao'].disabled = true;
	administracaoCsCdtbReferenciaRefeForm['csCdtbReferenciaRefeVo.refeInUltimoprocedimento'].disabled = true;
	

}

function validarPermissao(){

	try{
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_REFERENCIA_EXCLUSAO_CHAVE%>', administracaoCsCdtbReferenciaRefeForm.lixeira);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_REFERENCIA_INCLUSAO_CHAVE%>', ifrmLstAssistente.administracaoCsCdtbReferenciaRefeForm.imgIncluir);	
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_REFERENCIA_INCLUSAO_CHAVE%>') &&
	    	!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_ASSISTENTE_REFERENCIA_ALTERACAO_CHAVE%>')){
				administracaoCsCdtbReferenciaRefeForm.imgGravar.disabled=true;
				administracaoCsCdtbReferenciaRefeForm.imgGravar.className = 'geralImgDisable';
				administracaoCsCdtbReferenciaRefeForm.imgGravar.title='';
	   	}
	}
	catch(e){
		setTimeout("validarPermissao();",100);
	}
}


</script>
</head>
	
<body class="principalBgrPage" text="#000000" leftmargin="0" topmargin="0" onload="validarPermissao();" >

<html:form styleId="administracaoCsCdtbReferenciaRefeForm"	action="/AdministracaoCsCdtbReferenciaRefe.do">
<html:hidden property="modo" />
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="campoGravacao" /> 

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalBgrQuadro" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLabel">&nbsp;</td>
        </tr>
      </table>
      <table width="99%" border="0" cellspacing="0" cellpadding="0"	align="center">
        <tr> 
          <td height="254" valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkVazio"> 
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadroLinkSelecionadoGrande" valign="top"> Cadastro 
                        do Assistente</td>
                    </tr>
                  </table>
                </td>
                <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td valign="top" class="principalBgrQuadro" align="center" style="height:400px"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
                    <tr>
                      <td>&nbsp;</td>
						<tr> 
						  <td width="50%" class="principalLabel">Assunto</td>
						  <td width="50%" class="principalLabel">Grupo</td>
						</tr>
						<tr> 
						  <td class="principalLabel" valign="top" style="height:25px">
						  <html:select property="csCdtbAssuntoassistenteAsasVo.idAsasCdAssuntoassistente" styleClass="principalObjForm" onchange="carregaGrupo()">
							<html:option value="0"><bean:message key="prompt.selecione_uma_opcao"/></html:option>
							  <logic:present name="csCdtbAssuntoassistenteAsasVector">
								  <html:options collection="csCdtbAssuntoassistenteAsasVector" property="field(id_asas_cd_assuntoassistente)" labelProperty="field(asas_ds_assuntoassistente)" />  
							  </logic:present>
						  </html:select>
						  </td>
						  <td class="principalLabel" style="height:25px" valign="top"><iframe id=ifrmCmbGrupoAssistente name="ifrmCmbGrupoAssistente" src="AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_CMB_GRUPOASSISTENTE%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="25px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
						</tr>
                    </tr>
                  </table>
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" height="330">
                    <tr>
                      <td valign="top">
                        <iframe id=ifrmLstAssistente name="ifrmLstAssistente" src="AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_LST_ASSISTENTE%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="330px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                      </td>
                    </tr>
                  </table>
                  <table width="98%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" colspan="2" valign="top">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td colspan="2" class="principalLabel" valign="top">Procedimento</td>
                    </tr>
                    <tr> 
                      <td colspan="2" class="principalLabel" valign="top"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="78%" hstyle="height:25px" valign="top"><iframe id=ifrmCmbProcedimento name="ifrmCmbProcedimento" src="AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_CMB_PROCEDIMENTO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="25px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                            <td width="22%" class="principalLabel" valign="top"> 
                              <html:checkbox property="csCdtbReferenciaRefeVo.refeInUltimoprocedimento" disabled="true" />
                              &Uacute;ltimo Procedimento</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td colspan="2" class="principalLabel" valign="top">Atendimento Padr&atilde;o</td>
                    </tr>
                    <tr> 
                      <td colspan="2" class="principalLabel" width="78%" style="height:25px" valign="top">
						<iframe id=ifrmCmbAtendPadrao name="ifrmCmbAtendPadrao" src="AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_CMB_ATENDPADRAO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="25px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
					  </td>
                    </tr>
                    <tr>
                      <td colspan="2" class="principalLabel" valign="top">&nbsp;</td>
                    </tr>
                  </table>
                </td>
                <td width="4" height="500"><img	src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
              </tr>
              <tr> 
                <td width="100%" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
        </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td width="20"> <img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" onclick="gravar()"></td>
          <td width="20"> <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="cancelar()"></td>
        </tr>
      </table>
    </td>
    <td width="4" height="570"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
   	<iframe id=ifrmGravarReferencia name="ifrmGravarReferencia" src="AdministracaoCsCdtbReferenciaRefe.do?tela=<%=MAConstantes.TELA_MAIN_GRAVAR%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
  </tr>
  <tr> 
    <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>

</html:form>
</body>
</html>

<script>
	try{administracaoCsCdtbReferenciaRefeForm["csCdtbAssuntoassistenteAsasVo.idAsasCdAssuntoassistente"].focus();}
	catch(e){}
</script>