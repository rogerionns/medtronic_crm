<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<%
	long idEmpresa = Long.parseLong((String)request.getSession().getAttribute("idEmprCdEmpresa"));
	
	String maxRegistros = "";
	try{
		maxRegistros = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_LIMITE_LINHAS_RESULTADO,idEmpresa);
	}catch(Exception e){}
		
	if(maxRegistros == null || maxRegistros.equals("")){
		maxRegistros = "100";
	}
	
	
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="editCsCdtbAssuntoNivel2Asn2Form" action="/AdministracaoCsCdtbAssuntoNivel2Asn2.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="erro"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idAsn2CdAssuntoNivel2"/>

<script>
	var possuiRegistros=false;
	var totalRegistros=0;
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="CsCdtbAssuntoNivel2Asn2Vector" indexId="sequencia">
  <script>
  		possuiRegistros=true;
  		totalRegistros=<%=sequencia%>;
  </script>
  <tr> 
    <td width="3%" class="principalLstPar">
      <img src="webFiles/images/botoes/lixeira18x18.gif" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2" />')">
    </td>

    <td width="3%" class="principalLstParMao"> 
    	<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMAASN2_IDA2.xml','<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2" />')">
    </td>
    
    <td class="principalLstParMao" width="12%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2" />')">
      <bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2" />
    </td>
    <td class="principalLstParMao" align="left" width="58%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2" />')">
      <script>acronym('<bean:write name="ccttrtVector" property="asn2DsAssuntoNivel2" />', 50);</script>
	</td>
	<td class="principalLstParMao" align="center" width="25%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2" />')">
	&nbsp;
      <bean:write name="ccttrtVector" property="asn2DhInativo" />
	</td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
</table>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
	if((totalRegistros+1)>=<%=maxRegistros%>){
		alert('<bean:message key="prompt.alert_limiteRegistros"/>');
	}
</script>
</html:form>
</body>
</html>