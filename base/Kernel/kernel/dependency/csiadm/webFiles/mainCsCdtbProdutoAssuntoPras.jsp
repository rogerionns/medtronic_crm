<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	

long idEmpresa = empresaVo.getIdEmprCdEmpresa();

%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>

<script language="Javascript">

function submitPaginacao(regDe,regAte){
	
	document.administracaoCsCdtbProdutoAssuntoPrasForm.regDe.value=regDe;
	document.administracaoCsCdtbProdutoAssuntoPrasForm.regAte.value=regAte;
	if(regDe > -1 && regAte > -1)
		filtrar();
}

function inicio(){
	initPaginacao();
	filtrar();
}

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	//if(trim(administracaoCsCdtbProdutoAssuntoPrasForm.filtro.value).length > 2)
	//{
		document.administracaoCsCdtbProdutoAssuntoPrasForm.target = admIframe.name;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value ='filtrar';
		document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();
		setTimeout('limpaCampoFiltro()', 10);
	//}
	//else if(trim(administracaoCsCdtbProdutoAssuntoPrasForm.codProd.value).length > 2)
	//{
	//	document.administracaoCsCdtbProdutoAssuntoPrasForm.target = admIframe.name;
	//	document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value ='filtrar';
	//	document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();
	//	setTimeout('limpaCampoFiltro()', 10);		
	//}
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
	//else if(trim(administracaoCsCdtbProdutoAssuntoPrasForm.filtro2.value).length > 2)
	//{
	//	document.administracaoCsCdtbProdutoAssuntoPrasForm.target = admIframe.name;
	//	document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value ='filtrar';
	//	document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();
	//	setTimeout('limpaCampoFiltro()', 10);		
	//}	
<%}%>
	//else
	//{
	//}

	document.administracaoCsCdtbProdutoAssuntoPrasForm.regDe.value='0';
	document.administracaoCsCdtbProdutoAssuntoPrasForm.regAte.value='0';
	
	initPaginacao();
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbProdutoAssuntoPrasForm.filtro.value = '';
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		document.administracaoCsCdtbProdutoAssuntoPrasForm.filtro2.value = '';
	<%}%>
	document.administracaoCsCdtbProdutoAssuntoPrasForm.codProd.value = '';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_PRODUTOASSUNTO_PRAS%>';
	editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo1,codigo2){
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value = codigo1;
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value = codigo2;
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_PRODUTOASSUNTO_PRAS%>';
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.target = editIframe.name;
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){

	if(tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_PRODUTOASSUNTO_PRAS%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_PRODUTOASSUNTO_PRAS%>'){

		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		if(tab.ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1 == undefined || tab.ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value == ""){
			alert("<%= getMessage("prompt.Por_favor_selecione_um_Assunto_Nivel1", request)%>.");
			try{
				tab.ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.focus();
			}catch(e){}
			return false;
		}
		if(tab.ifrmCmbAssuntoNivel2Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2 == undefined || tab.ifrmCmbAssuntoNivel2Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value == ""){
			alert("<%= getMessage("prompt.Por_favor_selecione_um_Assunto_Nivel2", request)%>.");
			try{
				tab.ifrmCmbAssuntoNivel2Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.focus();
			}catch(e){}
			return false;
		}
		<%}%>
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			if(tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idLinhCdLinha.value == ""){
				alert("<%= getMessage("prompt.Por_favor_selecione_uma_Linha", request)%>.");
				tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idLinhCdLinha.focus();
				return false;
			}
		<%}%>
		/*
		if(tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAreaCdArea.value == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_uma_Area"/>.");
			tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAreaCdArea.focus();
			return false;
		}*/

<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>

		tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDsProdutoAssunto.value = 
		tab.ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.options[tab.ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.selectedIndex].text + " - " + 
		tab.ifrmCmbAssuntoNivel2Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.options[tab.ifrmCmbAssuntoNivel2Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.selectedIndex].text

<%}else{%>

		tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDsProdutoAssunto.value = trim(tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDsProdutoAssunto.value);
		if(tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDsProdutoAssunto.value == ""){
			alert("<bean:message key="prompt.Por_favor_digite_uma_descricao"/>.");
			tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDsProdutoAssunto.focus();
			return false;
		}

<%}%>

		if (tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDsProdutoAssunto.value.length > 0) {

			var cTela = "";
			var cTarget = "";
			
			if (editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDhInativo.checked){
				if (confirm("<bean:message key="prompt.Deseja_inativar_todas_as_composicoes_de_informacao_que_possuem_este_produto"/>?"))
					tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.inativarComp.value = true;
				else
					tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.inativarComp.value = false;
			}
			cTela = tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value;
			cTarget = tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.target;
			tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_PRODUTOASSUNTO_PRAS%>';
			tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.target = admIframe.name;

			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				disableEnable(tab.ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1, false);
			<%}%>
			tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idPesqCdPesquisa.value = editIframe.ifrmCmbPesq.document.administracaoCsCdtbProdutoAssuntoPrasForm.idPesqCdPesquisa.value;
			
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value = tab.ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value;
				tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value = tab.ifrmCmbAssuntoNivel2Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value;
			<%}%>
			
			tab.montaDadosCaracteristica();

			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value = tab.ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value;
				tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value = tab.ifrmCmbAssuntoNivel2Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value;
			<%}%>
			
			if(podeGravarAssociacao()==false){
				tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value=cTela;
				tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.target=cTarget;
				return false;
			}
			tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				disableEnable(tab.ifrmCmbAssuntoNivel1Pras.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1, true);
			<%}%>

			setTimeout("try{filtrar();}catch(e){}",1000);
			
			cancel();
		} else {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.prasDsProdutoAssunto.focus();
		}
	}
}

function submeteExcluir(codigo1,codigo2) {
	
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1.value = codigo1;
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2.value = codigo2;
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_PRODUTOASSUNTO_PRAS%>';
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.target = editIframe.name;
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		disableEnable(editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1, false);
		disableEnable(editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn2CdAssuntoNivel2, false);
		editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_PRODUTOASSUNTO_PRAS%>';
		editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			disableEnable(editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.idAsn1CdAssuntoNivel1, false);
		<%}%>
		editIframe.document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbProdutoAssuntoPras.do?tela=editCsCdtbProdutoAssuntoPras&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbProdutoAssuntoPras.do?tela=editCsCdtbProdutoAssuntoPras&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsCdtbProdutoAssuntoPrasForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_PRODUTOASSUNTO_PRAS%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			setaListaBloqueia();
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_PRODUTOASSUNTO_PRAS%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');
			setaListaHabilita();	
			setaIdiomaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbProdutoAssuntoPrasForm"	action="/AdministracaoCsCdtbProdutoAssuntoPras.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="regDe" />
	<html:hidden property="regAte" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top">
				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td height="254" valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalPstQuadroLinkVazio">
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalPstQuadroLinkSelecionado" id="tdDestinatario" name="tdDestinatario" onClick="AtivarPasta(admIframe);">
													<bean:message key="prompt.procurar"/>
												</td>
							                    <td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao" name="tdManifestacao" onClick="AtivarPasta(editIframe);"> 
							                        <bean:message key="prompt.produtoAssunto"/>
							                    </td>
											</tr>
										</table>
									</td>
									<td width="4">
										<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
									</td>
								</tr>
							</table>
							<table width="100%" height="400px" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
							        <td valign="top" height="400px" class="principalBgrQuadro"> 
					                  &nbsp;<div name="Manifestacao" id="Manifestacao" style="position: absolute; width: 97%; height: 225px; z-index: 6; visibility: hidden"> 
					                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					                      <tr>
                    					    <td height="380px" valign="top"> 
					                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                    						  <iframe id=editIframe name="editIframe" src="AdministracaoCsCdtbProdutoAssuntoPras.do?tela=editCsCdtbProdutoAssuntoPras&acao=incluir"	width="97%" height="380px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                    						</td>
										  </tr>
									    </table>
		  							  </div>
					                  <div name="Destinatario" id="Destinatario" style="position: absolute; width: 97%; height: 197px; z-index: 2; visibility: visible"> 
					                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					                      <tr> 
                    					    <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
													<td class="principalLabel" width="30%"><%= getMessage("prompt.assuntoNivel1", request)%></td>
													<td class="principalLabel" width="30%"><%= getMessage("prompt.assuntoNivel2", request)%></td>
                    					    		<td class="principalLabel" width="30%"><bean:message key="prompt.Cod_Prod"/></td>
					                        		<td class="principalLabel"  width="7%">&nbsp;</td>	
											<%}else{%>
													<td class="principalLabel" colspan="3"><bean:message key="prompt.descricao"/></td>
                    					    		<td class="principalLabel" colspan="1"><bean:message key="prompt.Cod_Prod"/></td>
					                        		<td class="principalLabel" width="17%">&nbsp;</td>
											<%}%>
					                      </tr>
                    					  <tr> 
					                        <td colspan="5"> 
                    					      <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                            <tr width="100%"> 
													<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
															<td width="30%"> 
																<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
					                              			</td>
															<td width="30%"> 
																<html:text property="filtro2" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
					                              			</td>
					                              			<td width="30%"> 
					                              				<html:text property="codProd" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
					                              			</td>
                    					          			<td width="10%"> &nbsp;
																<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
															</td>		
													<%}else{%>
															<td width="35%"> 
																<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
					                              			</td>
					                              			<td width="30%"> 
					                              				<html:text property="codProd" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
					                              			</td>
                    					          			<td width="35%"> &nbsp;
																<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
															</td>
													<%}%>
												</tr>
                    					      </table>
					                        </td>
                    					  </tr>
					                      <tr> 
                    					    <td class="principalLabel" colspan="5">&nbsp;</td>
					                      </tr>
					                      
					                      	</table>
										  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
 
                    					  <tr> 
					                        <td class="principalLstCab" width="8%">&nbsp;</td>
											<td class="principalLstCab" width="15%">
												<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
													<%= getMessage("prompt.linha", request)%>
												<%}else{%>	
													<bean:message key="prompt.descricao"/>
												<%}%>
                    					    </td>
					                        <td class="principalLstCab" width="9%"></td>
                    					    <td class="principalLstCab" width="30%">
												<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
													<bean:message key="prompt.descricao"/>
												<%}else{%>	
													&nbsp;
												<%}%>
                    					    </td>
                    					    <td class="principalLstCab" width="22%">
                    					   		<bean:message key="prompt.Cod_Prod"/>
                    					    </td>
					                        <td class="principalLstCab" align="left" width="17%"><bean:message key="prompt.inativo"/></td>
					                      </tr>
                    					  <tr valign="top"> 
					                        <td colspan="6" height="303px"> <!--Jonathan | Adequa��o para o IE 10-->
                    					      <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
					                          <iframe id=admIframe name="admIframe" src="AdministracaoCsCdtbProdutoAssuntoPras.do?acao=<%=Constantes.ACAO_VISUALIZAR%>" width="98%" height="303px" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0"></iframe></td>
					                      </tr>
					                 
					                      <tr valign="top"> 
					                      	<td colspan="6" valign="top"> 
					                      		<table cellpadding="0" cellspacing="0" align="center" border="0">
					                      			<tr valign="top">
					                      				<td valign="top">
					                      					<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>
					                      				</td>
					                      			</tr>
					                      		</table>
					                      	</td>
										  </tr>
										  
                    					</table>
									   </div>
						             </td>
									 <td width="4" height="410px"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="410px"></td>
								 </tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
							</table>
						</td>
					</tr>
				
				</table>
				<table border="0" cellspacing="0" cellpadding="4" align="right">
					<tr align="center">
						<td width="60" align="right">
								<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
						</td>
						<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
						</td>
						<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
						</td>
					</tr>
				</table>
				<table align="center" >
					<tr>
						<td>
							<label id="error">
												
							</label>
						</td>
					</tr>
				</table>
			</td>
			<td width="4" height="550px" valign="top"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="550px"></td>
		</tr>
		<tr>
			<td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="3"></td>
			<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="3"></td>
		</tr>
	</table>
</html:form>
<script language="JavaScript">
	var tab = admIframe ;
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_PRODUTOASSUNTO_INCLUSAO_CHAVE%>', document.administracaoCsCdtbProdutoAssuntoPrasForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_PRODUTOASSUNTO_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUTO_PRODUTOASSUNTO_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbProdutoAssuntoPrasForm.imgGravar.disabled=true;
			document.administracaoCsCdtbProdutoAssuntoPrasForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbProdutoAssuntoPrasForm.imgGravar.title='';
	}
	
	habilitaListaEmpresas();

	setaArquivoXml("CS_ASTB_IDIOMAPRAS_IDPR.xml");
	habilitaTelaIdioma();
	
</script>




</body>
</html>
