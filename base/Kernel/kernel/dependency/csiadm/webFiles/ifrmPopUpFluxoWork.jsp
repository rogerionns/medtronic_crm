<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Page-Enter" content="blendTrans(Duration=1)">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript">
<!--ifrmPopUpFluxoWork.jsp-->
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'PROCURAR':
	MM_showHideLayers('procurar','','show','conteudo','','hide','processo','','hide', 'lstRespostaPadrao','','hide')
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionadoGRANDE');
	SetClassFolder('tdProcesso','principalPstQuadroLinkNormalGRANDE');
	SetClassFolder('tdConteudo','principalPstQuadroLinkNormalGRANDE');
	ifrmDetalhes.marcarEtapaFinal(ifrmDetalhes.document.forms[0].etpr_in_ultimaetapa);
	ifrmDetalhes.exibirTdsTitulos('visible');
	break;

case 'PROCESSO':
	MM_showHideLayers('procurar','','hide','conteudo','','hide','processo','','show', 'lstRespostaPadrao','','show')
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormalGRANDE');
	SetClassFolder('tdProcesso','principalPstQuadroLinkSelecionadoGRANDE');
	SetClassFolder('tdConteudo','principalPstQuadroLinkNormalGRANDE');
	ifrmDetalhes.exibirEtapaFinal('hidden');
	ifrmDetalhes.exibirTdsTitulos('hidden');
	break;

case 'CONTEUDO':
	MM_showHideLayers('procurar','','hide','conteudo','','show','processo','','hide', 'lstRespostaPadrao','','hide')
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormalGRANDE');
	SetClassFolder('tdProcesso','principalPstQuadroLinkNormalGRANDE');
	SetClassFolder('tdConteudo','principalPstQuadroLinkSelecionadoGRANDE');
	ifrmDetalhes.exibirEtapaFinal('hidden');
	ifrmDetalhes.exibirTdsTitulos('hidden');
	break;
}
 eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
  if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
  obj.display=v; }
}

var arrayTreeView = new Array();
var countEtapa = new Number(0);
var criandoEtapa = false;

function carregaCmbFuncionario() {
	ifrmCmbFuncionario.location.href = 'CarregaCmbFuncionarioFluxoWorkflow.do?id_area_cd_area=' + fluxoWorkflowForm.id_area_cd_area.value;
}

function adicionaFuncionariosCopiados() {
	if(fluxoWorkflowForm.id_area_cd_area.value == '') {
		alert('<bean:message key="prompt.OCampoAreaEObrigatorio" />');
	} else if(ifrmCmbFuncionario.document.forms[0].id_func_cd_funcionario.value == '') {
		alert('<bean:message key="prompt.OCampoFuncionarioEObrigatorio" />');
	} else {
		adicionaFuncionariosCopiados2(fluxoWorkflowForm.id_area_cd_area.value,
									  ifrmCmbFuncionario.document.forms[0].id_func_cd_funcionario.value,
									  fluxoWorkflowForm.id_area_cd_area[fluxoWorkflowForm.id_area_cd_area.selectedIndex].text,
									  ifrmCmbFuncionario.document.forms[0].id_func_cd_funcionario[ifrmCmbFuncionario.document.forms[0].id_func_cd_funcionario.selectedIndex].text);
	}
}

nLinhaFuncCopiado = new Number(0);
function adicionaFuncionariosCopiados2(codArea, codFunc, descArea, descFunc) { 
	if(!podeAdicionarNaListaFuncionariosCopiados(codArea, codFunc)) {
		return false;
	}

	var strTxt = "";
	
	//valdeci - 67296 - lixeira para remover funcionario
	strTxt += "<table id=\"func" + nLinhaFuncCopiado + "\" width=710px border=0 cellspacing=0 cellpadding=0>";
    strTxt += "   <tr>"; 
    strTxt += "     <td class=principalLstPar width=5%>&nbsp;<img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand title=<bean:message key='prompt.excluir'/> onclick=removeCopiado(\"" + nLinhaFuncCopiado + "\") ></td>";
    strTxt += "     <td class=principalLstPar width=50%>&nbsp;" + acronymLst(descArea, 30) + "</td>";
    strTxt += "     <td class=principalLstPar width=45%>&nbsp;" + descFunc + "</td>";
    strTxt += "	    <input type=hidden name=idFuncCdFuncionarioArray value=\"" + codFunc + "\" />";
    strTxt += "	    <input type=hidden name=idAreaCdAreaArray value=\"" + codArea + "\" />";
    strTxt += "   </tr>";
    strTxt += " </table>";
	
	nLinhaFuncCopiado++;   

    document.getElementById("lstFuncionariosCopiados").innerHTML += strTxt;
}

//valdeci - 67296
function removeCopiado(linha) {
	if (confirm('<bean:message key="prompt.DesejaRemoverORegistro" />')) {
		var objIdTbl = window.document.getElementById("func" +linha);
		lstFuncionariosCopiados.removeChild(objIdTbl);
		nLinhaFuncCopiado--;
	}
}

function podeAdicionarNaListaFuncionariosCopiados(codArea, codFunc) {
	//Verifica se o item j� existe na lista
	if(fluxoWorkflowForm.idAreaCdAreaArray != null && fluxoWorkflowForm.idAreaCdAreaArray.length != undefined) {
		for(i = 0; i < fluxoWorkflowForm.idAreaCdAreaArray.length; i++) {
			if(fluxoWorkflowForm.idAreaCdAreaArray[i].value == codArea && fluxoWorkflowForm.idFuncCdFuncionarioArray[i].value == codFunc) {
				alert('<bean:message key="prompt.EsteRegistroJaExisteNaLista"/>');
				return false;
			}
		}
	} else if(fluxoWorkflowForm.idAreaCdAreaArray != null) {
		if(fluxoWorkflowForm.idAreaCdAreaArray.value == codArea && fluxoWorkflowForm.idFuncCdFuncionarioArray.value == codFunc) {
				alert('<bean:message key="prompt.EsteRegistroJaExisteNaLista"/>');
				return false;
			}
	}	
	return true;
}

function adicionaRespostaPadrao() {
	if(fluxoWorkflowForm.id_repa_cd_respostapadrao.value == '') {
		alert('<bean:message key="prompt.OCampoRespostaPadraoEObrigatorio" />');
	} else {
		adicionaRespostaPadrao2(fluxoWorkflowForm.id_repa_cd_respostapadrao.value,
							    fluxoWorkflowForm.id_repa_cd_respostapadrao[fluxoWorkflowForm.id_repa_cd_respostapadrao.selectedIndex].text);
	}
}

nLinhaRespPadrao = new Number(0);
function adicionaRespostaPadrao2(codResp, descResp) { 
	if(!podeAdicionarNaListaRespostaPadrao(codResp)) {
		return false;
	}

	var strTxt = "";

	strTxt += "<table id=\"" + nLinhaRespPadrao + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
    strTxt += "   <tr>"; 
    strTxt += "     <td class=principalLstPar width=6%>&nbsp;<img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand title=<bean:message key='prompt.excluir'/> onclick=removeRespostaPadrao(\"" + nLinhaRespPadrao + "\") ></td>";
    strTxt += "     <td class=principalLstPar width=94%>&nbsp;" + descResp + "</td>";
    strTxt += "	    <input type=hidden name=idRepaCdRespostapadraoArray value=\"" + codResp + "\" />";
    strTxt += "   </tr>";
    strTxt += " </table>";
	
	nLinhaRespPadrao++;   

    document.getElementById("lstRespostaPadrao").innerHTML += strTxt;
}

function podeAdicionarNaListaRespostaPadrao(codResp) {
	//Verifica se o item j� existe na lista
	if(fluxoWorkflowForm.idRepaCdRespostapadraoArray != null && fluxoWorkflowForm.idRepaCdRespostapadraoArray.length != undefined) {
		for(i = 0; i < fluxoWorkflowForm.idRepaCdRespostapadraoArray.length; i++) {
			if(fluxoWorkflowForm.idRepaCdRespostapadraoArray[i].value == codResp) {
				alert('<bean:message key="prompt.EsteRegistroJaExisteNaLista"/>');
				return false;
			}
		}
	} else if(fluxoWorkflowForm.idRepaCdRespostapadraoArray != null) {
		if(fluxoWorkflowForm.idRepaCdRespostapadraoArray.value == codResp) {
				alert('<bean:message key="prompt.EsteRegistroJaExisteNaLista" />');
				return false;
			}
	}	
	return true;
}

function removeRespostaPadrao(linha) {
	if (confirm('<bean:message key="prompt.DesejaRemoverORegistro" />')) {
		var objIdTbl = window.document.getElementById(linha);
		lstRespostaPadrao.removeChild(objIdTbl);
		nLinhaRespPadrao--;
	}
}

function inicio() {
	//Desabilita todos os campos da tela
	habilitaDesabilitaTodosCampos(true);
	
	//Posiciona no check correspondente
	if(document.forms[0].filtroTreeviewSelecionado.value == '' || document.forms[0].filtroTreeviewSelecionado.value == 'D') {
		document.forms[0].filtroTreeview[0].checked = true;
	} else if(document.forms[0].filtroTreeviewSelecionado.value == 'T') {
		document.forms[0].filtroTreeview[1].checked = true;
	}
	
	document.forms[0].filtroTreeview[0].disabled = false;
	document.forms[0].filtroTreeview[1].disabled = false;
		
	//Desabilitando as imagens da tela
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btAdicionaRespostaPadrao, true);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btAdcionaFuncCopiado, true);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btGravar, true);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btCancelar, true);
	
	//Carrega a treeView
	carregaTreeView();
	
	//Verifica se � altera��o da primeira etapa
	if(fluxoWorkflowForm.alteracaoPrimeiraEtapa.value == 'true') {
		alteraPrimeiraEtapa();
		fluxoWorkflowForm.editando.value = 'true';
		//fluxoWorkflowForm.alteracaoPrimeiraEtapa.value = false;
	}
	
	//Verifica se � altera��o das demais etapas
	if(fluxoWorkflowForm.alteracaoDemaisEtapas.value == 'true') {
		alteraDemaisEtapas();
		fluxoWorkflowForm.editando.value = 'true';
		//fluxoWorkflowForm.alteracaoDemaisEtapas.value = false;
	}
		
	document.all.item('aguarde').style.display = 'none';

	//Marca a linha em edicao
	if(fluxoWorkflowForm.alteracaoPrimeiraEtapa.value  == 'true' || fluxoWorkflowForm.alteracaoDemaisEtapas.value  == 'true') {
		for(i = 0; i < arrayTreeView.length; i++) {
			if(arrayTreeView[i].idEtapaProcesso == fluxoWorkflowForm.id_etpr_cd_etapaprocesso.value){
				ifrmTreeView.pintaLinha(arrayTreeView[i].idFilho);
			}
		}
	}
	
	AtivarPasta('CONTEUDO');
	AtivarPasta('PROCURAR');
}

function carregaTreeView() {
	var descricao = '';
	
	for(i = 0; i < arrayTreeView.length; i++) {
		if(document.forms[0].filtroTreeview[0].checked) {
			descricao = arrayTreeView[i].descFilho;
		} else {
			descricao = arrayTreeView[i].tituloEtapa + ' (' + arrayTreeView[i].encaminharParaSomenteNome + ')';
		}

		ifrmTreeView.adicionaFilho(arrayTreeView[i].idPai, 
								   arrayTreeView[i].idFilho, 
								   descricao, 
								   arrayTreeView[i].textoFuncao, 
								   arrayTreeView[i].etapaOk,
								   arrayTreeView[i].idEtapaProcesso);
		countEtapa++;
	}

	//Expande a treeView
	ifrmTreeView.expandirTodos();
}

function habilitaDesabilitaTodosCampos(valor) {
	for(i = 0; i < document.forms[0].elements.length; i++) {
		document.forms[0].elements[i].disabled = valor;
	}
	
	for(i = 0; i < ifrmDetalhes.document.forms[0].elements.length; i++) {
		ifrmDetalhes.document.forms[0].elements[i].disabled = valor;
	}
	
	ifrmCmbFuncionario.document.forms[0].id_func_cd_funcionario.disabled = valor;
	ifrmDetalhes.ifrmCmbFuncionario.document.forms[0].id_func_cd_funcionario_etapa.disabled = valor;
}

function habilitaDesabilitaImagem(obj, valor) {
	if (obj != undefined){
		if(valor) {
			obj.disabled=valor;
			obj.className = 'geralImgDisable';
		} else {
			obj.disabled=valor;
			obj.className = 'geralCursoHand';
		}
	}
}

function habilitaDesabilitaText(nome, valor) {
	var nomeDesabilitado = nome + 'Desabilitado';
	if(valor) {
		document.getElementById(nome).style.display = 'none';
		document.getElementById(nomeDesabilitado).style.display = 'block';
	} else {
		document.getElementById(nome).style.display = 'block';
		document.getElementById(nomeDesabilitado).style.display = 'none';
	}
}

function criarEtapa() {
	if(countEtapa == 0)
		novaEtapa();
	else if(countEtapa > 0)
		demaisEtapas();
}

function novaEtapa() {
	//Desabilita os botoes Nova Etapa e Excluir Etapa
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btNovaEtapa, true);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btExcluirEtapa, true);
	habilitaDesabilitaText('textNovaEtapa', true);
	habilitaDesabilitaText('textExcluirEtapa', true);
	
	//Habilita os botoes Gravar e Cancelar
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btGravar, false);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btCancelar, false);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btAdicionaRespostaPadrao, false);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btAdcionaFuncCopiado, false);
	
	//Habilita todos os campos da tela
	habilitaDesabilitaTodosCampos(false);
	
	//Desabilita os campos do cabecalho
	fluxoWorkflowForm.etpr_nr_prazonormal_cabecalho.disabled = true;
	fluxoWorkflowForm.etpr_nr_prazoespecial_cabecalho.disabled = true;
	
	ifrmDetalhes.document.forms[0].etpr_nr_etapa.value = '1';
	
	if(ifrmDetalhes.document.forms[0].etpr_nr_etapapredecessora.value == '')
		ifrmDetalhes.document.forms[0].etpr_nr_etapapredecessora.value = '0';
		
	if(countEtapa == 0) { //Se for a primeira etapa desabilita os campos abaixo
		ifrmDetalhes.document.forms[0].etpr_in_etapainicial[0].disabled = true;
		ifrmDetalhes.document.forms[0].etpr_in_etapainicial[1].disabled = true;
		ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[0].disabled = true;
		ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[1].disabled = true;
		ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[2].disabled = true;
	}
	
	ifrmDetalhes.document.forms[0].etpr_in_etapainicial[0].checked = true;
	ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[0].checked = true;
	ifrmDetalhes.document.forms[0].etpr_in_encaminharemail.checked = true;	

	criandoEtapa= true;
}

function demaisEtapas() {
	if(countEtapa > 0 && ifrmTreeView.etapaSelecionada == 0) {
		alert('<bean:message key="prompt.ENecessarioSelecionarUmaEtapa" />');
		return false;
	}
	
	for(i = 0; i < arrayTreeView.length; i++) {
		if(arrayTreeView[i].idFilho == ifrmTreeView.etapaSelecionada){
			if(arrayTreeView[i].ultimaEtapa == "S"){
				alert("<bean:message key="prompt.alert.NaoEPossivelCriarEtapaAbaixoDeEtapaFinal" />");
				return false;
			}
		}
	}
	
	//Desabilita os botoes Nova Etapa e Excluir Etapa
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btNovaEtapa, true);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btExcluirEtapa, true);
	habilitaDesabilitaText('textNovaEtapa', true);
	habilitaDesabilitaText('textExcluirEtapa', true);
	
	//Habilita os botoes Gravar e Cancelar
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btGravar, false);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btCancelar, false);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btAdicionaRespostaPadrao, false);
	habilitaDesabilitaImagem(document.fluxoWorkflowForm.btAdcionaFuncCopiado, false);
	
	//Habilita todos os campos da tela
	habilitaDesabilitaTodosCampos(false);

	//Verifica qual � o maior n�mero de etapa utilizado e acrescenta + 1
	var maior = new Number(0);
	for(i = 0; i < arrayTreeView.length; i++) {
		maior = Math.max(maior, new Number(arrayTreeView[i].idFilho));
	}
	ifrmDetalhes.document.forms[0].etpr_nr_etapa.value = maior + 1;
	
	//Bloqueia o campo etapa predecessora
	ifrmDetalhes.document.forms[0].etpr_nr_etapapredecessora.disabled = true;
	
	//Prenche o titulo do campo Etapa Inicial com o valo da pr�xima pergunta
	for(i = 0; i < arrayTreeView.length; i++) {
		if(arrayTreeView[i].idEtapaProcesso == fluxoWorkflowForm.id_etpr_cd_etapaprocesso.value) {
			for(j = 0; j < arrayTreeView.length; j++) {
				if((arrayTreeView[i].idPai == arrayTreeView[j].idPai) && (arrayTreeView[i].idEtapaProcesso == arrayTreeView[j].idEtapaProcesso)) {
					ifrmDetalhes.document.forms[0].etapaInicialLabel.value = arrayTreeView[j].perguntaProxEtapa;
					break;
				}
			}
			break;
		}
	}
	
	//Checando o campo Encaminhar Para: �rea / Funcion�rio
	ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[0].checked = true;
	
	//Checando o campoNotificar �rea respons�vel por e-mail
	ifrmDetalhes.document.forms[0].etpr_in_encaminharemail.checked = true;	

	criandoEtapa= true;
}

function gravar() {
	//Verifica se deve continuar com a gravacao quando for uma alteracao
	if(fluxoWorkflowForm.alteracaoPrimeiraEtapa.value  == 'true' || fluxoWorkflowForm.alteracaoDemaisEtapas.value  == 'true') {
		if(!confirm('<bean:message key="prompt.ConfirmaAlteracaoDaEtapa" /> ' + fluxoWorkflowForm.descricaoEtapa.value + '?'))
			return false;
	}

	AtivarPasta('PROCURAR');
	
	if(ifrmDetalhes.document.forms[0].etpr_nr_etapa.value == '' || ifrmDetalhes.document.forms[0].etpr_nr_etapa.value == '0') {
		alert('<bean:message key="prompt.OCampoNumEtapaDeveSerMaiorQueZero" />');
		ifrmDetalhes.document.forms[0].etpr_nr_etapa.focus();
		return false;
	}
	
	//Verifica se j� existe uma etapa criada com este valor
	for(i = 0; i < arrayTreeView.length; i++) {
		if(arrayTreeView[i].idFilho == ifrmDetalhes.document.forms[0].etpr_nr_etapa.value && arrayTreeView[i].idEtapaProcesso != fluxoWorkflowForm.id_etpr_cd_etapaprocesso.value) {
			alert('<bean:message key="prompt.JaExisteUmaEtapaComEsteValor" />');
			ifrmDetalhes.document.forms[0].etpr_nr_etapa.focus();
			return false;
		}
	}
	
	//Verifica a obrigatoriedade do campo etpa inicial
	if(!ifrmDetalhes.document.forms[0].etpr_in_etapainicial[0].checked && !ifrmDetalhes.document.forms[0].etpr_in_etapainicial[1].checked && !ifrmDetalhes.document.forms[0].etpr_in_ultimaetapa.checked) {
		alert('<bean:message key="prompt.OCampo" /> ' + ifrmDetalhes.document.forms[0].etapaInicialLabel.value + ' <bean:message key="prompt.EObrigatorio" />');
		return false;
	} else { //Uma etapa pai s� pode ter um filho sim e um filho n�o, se o usu�rio quiser criar outro filho sim ou n�o, os filhos sim e n�o j� existentes ser�o remanejados para este novo filho.
		if(fluxoWorkflowForm.editando.value != 'true') { //Se n� estiver editando
			var etapaSim = false;
			var etapaNao = false;
			
			for(i = 0; i < arrayTreeView.length; i++) {
				if(ifrmDetalhes.document.forms[0].etpr_nr_etapapredecessora.value == arrayTreeView[i].idPai) {				
					if(arrayTreeView[i].etapaOk) {
						etapaSim = arrayTreeView[i].etapaOk;
					}
					else {
						etapaNao = arrayTreeView[i].etapaOk;
					}
				}
			}
			
			if((etapaSim && etapaNao) || (etapaSim && ifrmDetalhes.document.forms[0].etpr_in_etapainicial[0].checked) || (etapaNao && ifrmDetalhes.document.forms[0].etpr_in_etapainicial[1].checked)) { // Se o pai j� tiver um filho sim e um filho nao
				if(confirm('<bean:message key="prompt.JaExisteUmaEtapaFilhaDoFluxoSelecionado" />')) {
					fluxoWorkflowForm.remanejar.value = 'true';
				} else {
					return false;
				}
			}	
		}		
	}
	
	if(!ifrmDetalhes.document.forms[0].etpr_in_ultimaetapa.checked && ifrmDetalhes.document.forms[0].etpr_ds_perguntafluxo.value == '' && !ifrmDetalhes.fluxoWorkflowForm.etpr_in_encaminharpara[1].checked) {
		alert('<bean:message key="prompt.OCampoPerguntaParaDefiniciaoEObrigatorio" />');
		ifrmDetalhes.document.forms[0].etpr_ds_perguntafluxo.focus();
		return false;
	}
	
	//Verifica a obrigatoriedade do campo Encaminhar Para
	if(ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[0].checked) {
		if(ifrmDetalhes.ifrmCmbFuncionario.document.forms[0].id_func_cd_funcionario_etapa.value == '') {
			alert('<bean:message key="prompt.OCampoFuncionarioEObrigatorio" />');
			ifrmDetalhes.AtivarPasta('AREA');
			ifrmDetalhes.ifrmCmbFuncionario.document.forms[0].id_func_cd_funcionario_etapa.focus();
			return false;
		}
		
		if(ifrmDetalhes.document.forms[0].etpr_nr_prazonormal.value == '') {
			alert('<bean:message key="prompt.OCampoProcessoNormalEObrigatorio" />');
			ifrmDetalhes.AtivarPasta('AREA');
			ifrmDetalhes.document.forms[0].etpr_nr_prazonormal.focus();
			return false;
		}
		
		if(ifrmDetalhes.document.forms[0].id_tppr_cd_prazonormal.value == '') {
			alert('<bean:message key="prompt.OCampoTipoPrazoProcessoNormalEObrigatorio" />');
			ifrmDetalhes.AtivarPasta('AREA');
			ifrmDetalhes.document.forms[0].id_tppr_cd_prazonormal.focus();
			return false;
		}
	} else if(ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[1].checked) {
		if(ifrmDetalhes.document.forms[0].etpr_nr_encaminharetapa.value == '') {
			alert('<bean:message key="prompt.NumEtapa" />');
			ifrmDetalhes.AtivarPasta('ETAPA');
			ifrmDetalhes.document.forms[0].etpr_nr_encaminharetapa.focus();
			return false;
		}
	} else if(ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[2].checked) {
		if(ifrmDetalhes.document.forms[0].etpr_nr_prazonormal.value == '') {
			alert('<bean:message key="prompt.OCampoProcessoNormalEObrigatorio" />');
			ifrmDetalhes.document.forms[0].etpr_nr_prazonormal.focus();
			return false;
		}
		
		if(ifrmDetalhes.document.forms[0].id_tppr_cd_prazonormal.value == '') {
			alert('<bean:message key="prompt.OCampoTipoPrazoProcessoNormalEObrigatorio" />');
			ifrmDetalhes.document.forms[0].id_tppr_cd_prazonormal.focus();
			return false;
		}
	} else {
		if(!ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[2].checked) {
			alert('<bean:message key="prompt.EncaminharParaEObrigatorio" />');
			return false;			
		}
	}
	
	if(fluxoWorkflowForm.editando.value != 'true') { //Se n� estiver editando
		if(countEtapa >= 1 && ( ifrmDetalhes.document.forms[0].etpr_nr_etapapredecessora.value == '0' || ifrmDetalhes.document.forms[0].etpr_nr_etapapredecessora.value == '')) {
			alert('<bean:message key="prompt.SelecioneUmaEtapaPredecessoraValida" />');
			return false;
		}
	}
	
	document.all.item('aguarde').style.display = 'block';
	
	//Habilita todos os campos da tela para que possam ser enviados via submit
	habilitaDesabilitaTodosCampos(false);
	
	//Armazenando os campos que est�o em iframes nos hiddens para serem enviados no submit
	fluxoWorkflowForm.id_func_cd_funcionario.value = ifrmCmbFuncionario.document.forms[0].id_func_cd_funcionario.value; 
	fluxoWorkflowForm.id_func_cd_funcionario_etapa.value = ifrmDetalhes.ifrmCmbFuncionario.document.forms[0].id_func_cd_funcionario_etapa.value;
	fluxoWorkflowForm.etpr_nr_etapa.value = ifrmDetalhes.document.forms[0].etpr_nr_etapa.value;
	fluxoWorkflowForm.etpr_nr_etapapredecessora.value = ifrmDetalhes.document.forms[0].etpr_nr_etapapredecessora.value;
	fluxoWorkflowForm.etpr_ds_perguntafluxo.value = ifrmDetalhes.document.forms[0].etpr_ds_perguntafluxo.value;
	fluxoWorkflowForm.etpr_nr_encaminharetapa.value = ifrmDetalhes.document.forms[0].etpr_nr_encaminharetapa.value;
	//fluxoWorkflowForm.id_etpr_cd_predecessora.value = ifrmDetalhes.document.forms[0].id_etpr_cd_predecessora.value;
	fluxoWorkflowForm.id_tppr_cd_prazonormal.value = ifrmDetalhes.document.forms[0].id_tppr_cd_prazonormal.value;
	fluxoWorkflowForm.id_tppr_cd_tpprazoespecial.value = ifrmDetalhes.document.forms[0].id_tppr_cd_tpprazoespecial.value;
	fluxoWorkflowForm.etpr_nr_prazonormal.value = ifrmDetalhes.document.forms[0].etpr_nr_prazonormal.value;

	<%
	/**
	* Chamado 75490 - Vinicius - Quando n�o vier valor gravar "0"(zero) 
	*/
	%>
	if (ifrmDetalhes.document.forms[0].etpr_nr_prazoespecial.value != "") {
			fluxoWorkflowForm.etpr_nr_prazoespecial.value = ifrmDetalhes.document.forms[0].etpr_nr_prazoespecial.value;
		} else {
			fluxoWorkflowForm.etpr_nr_prazoespecial.value = "0";
		}

		fluxoWorkflowForm.etpr_ds_tituloetapa.value = ifrmDetalhes.document.forms[0].etpr_ds_tituloetapa.value;

		if (ifrmDetalhes.document.forms[0].etpr_in_etapainicial[0].checked) {
			fluxoWorkflowForm.etpr_in_etapainicial.value = ifrmDetalhes.document.forms[0].etpr_in_etapainicial[0].value;
		} else if (ifrmDetalhes.document.forms[0].etpr_in_etapainicial[1].checked) {
			fluxoWorkflowForm.etpr_in_etapainicial.value = ifrmDetalhes.document.forms[0].etpr_in_etapainicial[1].value;
		}

		if (ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[0].checked) {
			fluxoWorkflowForm.etpr_in_encaminharpara.value = ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[0].value;
		} else if (ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[1].checked) {
			fluxoWorkflowForm.etpr_in_encaminharpara.value = ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[1].value;
		} else if (ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[2].checked) {
			fluxoWorkflowForm.etpr_in_encaminharpara.value = ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[2].value;
			if (ifrmDetalhes.document.forms[0].etpr_in_retorarearesol.checked) {
				fluxoWorkflowForm.etpr_in_retorarearesol.value = 'S';
			} else {
				fluxoWorkflowForm.etpr_in_retorarearesol.value = 'N';
			}
		}

		if (ifrmDetalhes.document.forms[0].etpr_in_encaminharemail.checked) {
			fluxoWorkflowForm.etpr_in_encaminharemail.value = 'S';
		} else {
			fluxoWorkflowForm.etpr_in_encaminharemail.value = 'N';
		}

		if (ifrmDetalhes.document.forms[0].etpr_in_ultimaetapa.checked) {
			fluxoWorkflowForm.etpr_in_ultimaetapa.value = 'S';
		} else {
			fluxoWorkflowForm.etpr_in_ultimaetapa.value = 'N';
		}

		document.fluxoWorkflowForm.target = this.name = 'popupFluxoWorkflow';

		if (fluxoWorkflowForm.editando.value != 'true') {
			document.fluxoWorkflowForm.action = 'GravaFluxoWorkflow.do';
		} else {
			fluxoWorkflowForm.gravaEdicao.value = 'true';
			document.fluxoWorkflowForm.action = 'AlteraEtapaFluxoWorkflow.do';
		}

		document.fluxoWorkflowForm.submit();
	}

	function submitAlteraEtapa(idEtapaProcesso, descEtapa, ePrimeiraEtapa) {
		document.all.item('aguarde').style.display = 'block';

		fluxoWorkflowForm.descricaoEtapa.value = descEtapa;
		fluxoWorkflowForm.id_etpr_cd_etapaprocesso.value = idEtapaProcesso;

		//Habilita todos os campos da tela para que possam ser enviados via submit
		habilitaDesabilitaTodosCampos(false);

		if (ePrimeiraEtapa) {
			fluxoWorkflowForm.alteracaoPrimeiraEtapa.value = 'true';
			fluxoWorkflowForm.alteracaoDemaisEtapas.value = 'false';
		} else {
			fluxoWorkflowForm.alteracaoPrimeiraEtapa.value = 'false';
			fluxoWorkflowForm.alteracaoDemaisEtapas.value = 'true';
		}

		document.fluxoWorkflowForm.target = this.name = 'popupFluxoWorkflow';
		document.fluxoWorkflowForm.action = 'AlteraEtapaFluxoWorkflow.do';
		document.fluxoWorkflowForm.submit();
	}

	function alteraPrimeiraEtapa() {
		//Desabilita os botoes Nova Etapa e Excluir Etapa
		habilitaDesabilitaImagem(document.fluxoWorkflowForm.btNovaEtapa, true);
		habilitaDesabilitaImagem(document.fluxoWorkflowForm.btExcluirEtapa,
				true);
		habilitaDesabilitaText('textNovaEtapa', true);
		habilitaDesabilitaText('textExcluirEtapa', true);

		//Habilita os botoes Gravar e Cancelar
		habilitaDesabilitaImagem(document.fluxoWorkflowForm.btGravar, false);
		habilitaDesabilitaImagem(document.fluxoWorkflowForm.btCancelar, false);
		habilitaDesabilitaImagem(
				document.fluxoWorkflowForm.btAdicionaRespostaPadrao, false);
		habilitaDesabilitaImagem(
				document.fluxoWorkflowForm.btAdcionaFuncCopiado, false);

		//Habilita todos os campos da tela
		habilitaDesabilitaTodosCampos(false);

		//Desabilitando os campos N� Etapa, N� Etapa Predecessora, Etapa Inicial e Encaminhar
		ifrmDetalhes.document.forms[0].etpr_nr_etapa.disabled = true;
		ifrmDetalhes.document.forms[0].etpr_nr_etapapredecessora.disabled = true;
		ifrmDetalhes.document.forms[0].etpr_in_etapainicial[0].disabled = true;
		ifrmDetalhes.document.forms[0].etpr_in_etapainicial[1].disabled = true;
		ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[0].disabled = true;
		ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[1].disabled = true;
		ifrmDetalhes.document.forms[0].etpr_in_encaminharpara[2].disabled = true;
	}

	function alteraDemaisEtapas() {
		//Desabilita os botoes Nova Etapa e Excluir Etapa
		habilitaDesabilitaImagem(document.fluxoWorkflowForm.btNovaEtapa, true);
		habilitaDesabilitaImagem(document.fluxoWorkflowForm.btExcluirEtapa,
				true);
		habilitaDesabilitaText('textNovaEtapa', true);
		habilitaDesabilitaText('textExcluirEtapa', true);

		//Habilita os botoes Gravar e Cancelar
		habilitaDesabilitaImagem(document.fluxoWorkflowForm.btGravar, false);
		habilitaDesabilitaImagem(document.fluxoWorkflowForm.btCancelar, false);
		habilitaDesabilitaImagem(
				document.fluxoWorkflowForm.btAdicionaRespostaPadrao, false);
		habilitaDesabilitaImagem(
				document.fluxoWorkflowForm.btAdcionaFuncCopiado, false);

		//Habilita todos os campos da tela
		habilitaDesabilitaTodosCampos(false);

		//Desabilitando os campos N� Etapa, N� Etapa Predecessora, Etapa Inicial e Encaminhar
		ifrmDetalhes.document.forms[0].etpr_nr_etapa.disabled = true;
		ifrmDetalhes.document.forms[0].etpr_nr_etapapredecessora.disabled = true;

		//Prenche o titulo do campo Etapa Inicial com o valor da pr�xima pergunta
		for (var i = 0; i < arrayTreeView.length; i++) {
			if (i > 0) {// Diferente da Etapa Inicial
				if (arrayTreeView[i].idPai == ifrmDetalhes.fluxoWorkflowForm.etpr_nr_etapapredecessora.value) {
					ifrmDetalhes.fluxoWorkflowForm.etapaInicialLabel.value = arrayTreeView[i - 1].perguntaProxEtapa;
					break;
				}
			}
		}
	}

	function excluirEtapa() {
		var podeExcluir = false;

		if (countEtapa > 0 && ifrmTreeView.etapaSelecionada == 0) {
			alert('<bean:message key="prompt.ENecessarioSelecionarUmaEtapa" />');
			return false;
		}

		if (confirm('<bean:message key="prompt.ConfirmaAExclusaoDaEtapa" />')) {
			//Verifica se a etapa esta sendo refernciada por outras etapas
			var estaSendoReferenciadaPorOutraEtapa = false;
			for (i = 0; i < arrayTreeView.length; i++) {
				if (fluxoWorkflowForm.etpr_nr_etapa.value == arrayTreeView[i].encaminharEtapa) {
					estaSendoReferenciadaPorOutraEtapa = true;
					break;
				}
			}
			if (estaSendoReferenciadaPorOutraEtapa) {
				alert('<bean:message key="prompt.NaoEPossivelExcluirAEtapaSelecionadaReferenciada" />');
				return false;
			}

			//Verifica se a etapa selecionada possui filhos
			var possuiFilhos = false;
			for (i = 0; i < arrayTreeView.length; i++) {
				if (fluxoWorkflowForm.etpr_nr_etapa.value == arrayTreeView[i].idPai) {
					possuiFilhos = true;
					break;
				}
			}
			if (possuiFilhos) {
				if (!confirm('<bean:message key="prompt.EstaEtapaPossuiOutrasEtapasEncadeadas" />'))
					return false;
				else
					podeExcluir = true;
			} else {
				podeExcluir = true;
			}

			if (podeExcluir) {
				//Habilita todos os campos da tela
				habilitaDesabilitaTodosCampos(false);

				document.fluxoWorkflowForm.target = this.name = 'popupFluxoWorkflow';
				document.fluxoWorkflowForm.action = 'ExcluirFluxoWorkflow.do';
				document.fluxoWorkflowForm.submit();
			}
		}
	}

	function cancelar() {
		fluxoWorkflowForm.alteracaoPrimeiraEtapa.value = 'false';
		fluxoWorkflowForm.alteracaoDemaisEtapas.value = 'false';
		fluxoWorkflowForm.editando.value = 'false';
		fluxoWorkflowForm.remanejar.value = 'false';
		document.fluxoWorkflowForm.target = this.name = 'popupFluxoWorkflow';
		document.fluxoWorkflowForm.action = 'AbrePopupFluxoWorkflow.do';
		document.fluxoWorkflowForm.submit();
	}

	function fechaJanela() {
		var janela = null;

		if (parent.window.dialogArguments != undefined) {
			janela = parent.window.dialogArguments;
		} else {
			janela = parent.window.opener;
		}

		janela.document.forms[0].depr_nr_prazonormal.value = fluxoWorkflowForm.etpr_nr_prazonormal_cabecalho.value;
		janela.document.forms[0].depr_nr_prazoespecial.value = fluxoWorkflowForm.etpr_nr_prazoespecial_cabecalho.value;

		if ("<bean:write name='fluxoWorkflowForm' property='menorPrazoNormal' />" == "DIAS") {
			janela.document.forms[0].id_tppr_cd_tpprazonormal.value = '1';
		} else if ("<bean:write name='fluxoWorkflowForm' property='menorPrazoNormal' />" == "HORAS") {
			janela.document.forms[0].id_tppr_cd_tpprazonormal.value = '2';
		} else if ("<bean:write name='fluxoWorkflowForm' property='menorPrazoNormal' />" == "MINUTOS") {
			janela.document.forms[0].id_tppr_cd_tpprazonormal.value = '3';
		}

		if ("<bean:write name='fluxoWorkflowForm' property='menorPrazoEspecial' />" == "DIAS") {
			janela.document.forms[0].id_tppr_cd_tpprazoespecial.value = '1';
		} else if ("<bean:write name='fluxoWorkflowForm' property='menorPrazoEspecial' />" == "HORAS") {
			janela.document.forms[0].id_tppr_cd_tpprazoespecial.value = '2';
		} else if ("<bean:write name='fluxoWorkflowForm' property='menorPrazoEspecial' />" == "MINUTOS") {
			janela.document.forms[0].id_tppr_cd_tpprazoespecial.value = '3';
		}

		janela.calculaPrazoEmHorasDias();
	}

	function imprimir() {
		//window.open("CarregaTreeViewInfo.do?impressao=S","Impressao","width=950,height=600,top=50,left=50");
		showModalDialog(
				'CarregaTreeViewInfo.do?impressao=S',
				window,
				'help:no;scroll:no;Status:NO;dialogWidth:950px;dialogHeight:600px,dialogTop:50px,dialogLeft:50px');
	}

	function verificaFiltroTreeview() {
		ifrmTreeView.location.href = 'CarregaTreeViewInfo.do';
		verificaIfrmTreeViewJaCarregou();
	}

	function verificaIfrmTreeViewJaCarregou() {
		if (ifrmTreeView.document.readyState == 'complete') {
			carregaTreeView();
		} else {
			setTimeout('verificaIfrmTreeViewJaCarregou()', 300);
		}
	}
	
</script>
</head>

<script language="JavaScript">
	
</script>

<body class="principalBgrPage" text="#000000" leftmargin="5"
	topmargin="5" marginwidth="5" marginheight="5"
	onLoad="showError('<%=request.getAttribute("msgerro")%>');inicio();"
	onbeforeunload="fechaJanela();">
	<div style="overflow: auto;height: 730px;width: 840px;">
	<html:form styleId="fluxoWorkflowForm"
		action="/AbrePopupFluxoWorkflow.do">
		<html:hidden property="id_etpr_cd_etapaprocesso" />
		<html:hidden property="id_depr_cd_desenhoprocesso" />
		<html:hidden property="id_func_cd_funcionario" />
		<html:hidden property="id_func_cd_funcionario_etapa" />
		<html:hidden property="etpr_nr_etapa" />
		<html:hidden property="etpr_nr_etapapredecessora" />
		<html:hidden property="etpr_in_etapainicial" />
		<html:hidden property="etpr_in_etapasucesso" />
		<html:hidden property="etpr_ds_perguntafluxo" />
		<html:hidden property="etpr_in_encaminharpara" />
		<html:hidden property="etpr_nr_encaminharetapa" />
		<html:hidden property="etpr_in_encaminharemail" />
		<html:hidden property="id_etpr_cd_predecessora" />
		<html:hidden property="id_tppr_cd_prazonormal" />
		<html:hidden property="id_tppr_cd_tpprazoespecial" />
		<html:hidden property="etpr_nr_prazonormal" />
		<html:hidden property="etpr_nr_prazoespecial" />
		<html:hidden property="etpr_in_ultimaetapa" />
		<html:hidden property="etpr_ds_tituloetapa" />
		<html:hidden property="etpr_in_retorarearesol" />

		<!--<html:hidden property="countEtapa"/>-->
		<html:hidden property="alteracaoPrimeiraEtapa" />
		<html:hidden property="alteracaoDemaisEtapas" />
		<html:hidden property="editando" />
		<html:hidden property="gravaEdicao" />
		<html:hidden property="remanejar" />
		<html:hidden property="descricaoEtapa" />
		<html:hidden property="filtroTreeviewSelecionado" />
		<table width="99%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="1007" colspan="2">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="espacoPqn">
						<tr>
							<td>&nbsp;</td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="principalPstQuadro" height="17" width="166"><bean:message
									key="prompt.DesenhoDoProcesso" /></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
							<td height="100%" width="4"
								style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="principalBgrQuadro" valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						height="100%">
						<tr>
							<td valign="top" align="center">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0"
									align="center">
									<tr>
										<td class="principalLabel" align="center" width="4%"><img
											src="webFiles/images/botoes/abrir_tudo.gif"
											title="<bean:message key="prompt.ExpandirTodos"/>" width="30"
											height="30" class="geralCursoHand"
											onclick="ifrmTreeView.expandirTodos()"></td>
										<td class="principalLabel" width="10%"><span
											class="geralCursoHand" onclick="ifrmTreeView.expandirTodos()"><bean:message
													key="prompt.ExpandirTodos" /></span></td>
										<td class="principalLabel" width="4%" align="center"><img
											src="webFiles/images/botoes/fechar_tudo.gif"
											title="<bean:message key="prompt.FecharTodos"/>" width="30"
											height="30" class="geralCursoHand"
											onclick="ifrmTreeView.fecharTodos()"></td>
										<td class="principalLabel" width="10%"><span
											class="geralCursoHand" onclick="ifrmTreeView.fecharTodos()"><bean:message
													key="prompt.FecharTodos" /></span></td>
										<td class="principalLabel" width="10%"><html:radio
												property="filtroTreeview" value="D"
												onclick="verificaFiltroTreeview()" />&nbsp;<bean:message
												key="prompt.destinatario" /></td>
										<td class="principalLabel" width="10%"><html:radio
												property="filtroTreeview" value="T"
												onclick="verificaFiltroTreeview()" />&nbsp;<bean:message
												key="prompt.titulo" /></td>
										<td class="principalLabel" width="4%" align="center"><img
											src="webFiles/images/icones/impressora.gif"
											alt="<bean:message key="prompt.Imprimir"/>" width="30"
											height="30" class="geralCursoHand" onclick="imprimir()"></td>
										<td class="principalLabel" width="52%"><span
											class="geralCursoHand" onclick="imprimir()"><bean:message
													key="prompt.Imprimir" /></span></td>
									</tr>
								</table>

								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0"
									align="center">
									<tr>
										<td class="espacoPqn"><img
											src="webFiles/images/linhas/horSombra.gif" width="100%"
											height="2"></td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0"
									align="center" height="260px">
									<tr>
										<td valign="top"><iframe id="ifrmTreeView"
												name="ifrmTreeView" style="border: 1px solid black;"
												src="CarregaTreeViewInfo.do" width="100%" height="260px"
												scrolling="auto" frameborder="0" marginwidth="0"
												marginheight="0"></iframe></td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<table>
												<tbody>
													<tr>
														<td valign="top"><div id="botoes"
																style="left: 624px; top: 313px; width: 190px; height: 35px;">
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td class="principalLabel" align="center" width="7%"><img
																			src="webFiles/images/botoes/new.gif" id="btNovaEtapa"
																			title="<bean:message key="prompt.NovaEtapa"/>"
																			width="14" height="16" class="geralCursoHand"
																			onclick="criarEtapa()"></td>
																		<td class="principalLabel" width="44%"><span
																			class="geralCursoHand" id="textNovaEtapa"
																			style="display: block" onclick="criarEtapa()">&nbsp;<bean:message
																					key="prompt.NovaEtapa" /></span> <span
																			class="geralImgDisable"
																			id="textNovaEtapaDesabilitado" style="display: none">&nbsp;<bean:message
																					key="prompt.NovaEtapa" /></span></td>
																		<td class="principalLabel" align="center" width="8%"><img
																			src="webFiles/images/botoes/lixeira18x18.gif"
																			id="btExcluirEtapa"
																			title="<bean:message key="prompt.ExcluirEtapa"/>"
																			width="18" height="18" class="geralCursoHand"
																			onclick="excluirEtapa()"></td>
																		<td class="principalLabel" width="41%"><span
																			class="geralCursoHand" id="textExcluirEtapa"
																			style="display: block" onclick="excluirEtapa()">&nbsp;<bean:message
																					key="prompt.ExcluirEtapa" /></span> <span
																			class="geralImgDisable"
																			id="textExcluirEtapaDesabilitado"
																			style="display: none">&nbsp;<bean:message
																					key="prompt.ExcluirEtapa" /></span></td>
																	</tr>
																</table>
															</div></td>
														<td><div id="botao"
																style="left: 535px; top: 21px; width: 293px; height: 23px;">
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td class="principalLabelValorFixo" align="right"
																			width="42%"><bean:message
																				key="prompt.PrazoTotalDoFluxo" /></td>
																		<td class="principalLabel" align="right" width="23%"><bean:message
																				key="prompt.Normal" /> <img
																			src="webFiles/images/icones/setaAzul.gif" width="7"
																			height="7"></td>
																		<td class="principalLabel" width="20%"><html:text
																				property="etpr_nr_prazonormal_cabecalho"
																				maxlength="4"
																				styleClass="principalObjFormCentralizado"
																				onkeypress="isDigito(this)" disabled="true" /></td>
																		<td class="principalLabel" id="tdLblPrazoNormal"
																			width="15%">&nbsp;<bean:write
																				name="fluxoWorkflowForm" property="menorPrazoNormal" /></td>
																	</tr>
																	<tr>
																		<td class="principalLabel" align="right" width="42%">&nbsp;</td>
																		<td class="principalLabel" align="right" width="23%"><bean:message
																				key="prompt.Especial" /> <img
																			src="webFiles/images/icones/setaAzul.gif" width="7"
																			height="7"></td>
																		<td class="principalLabel" width="20%"><html:text
																				property="etpr_nr_prazoespecial_cabecalho"
																				maxlength="4"
																				styleClass="principalObjFormCentralizado"
																				onkeypress="isDigito(this)" disabled="true" /></td>
																		<td class="principalLabel" id="tdLblPrazoEspecial"
																			width="15%">&nbsp;<bean:write
																				name="fluxoWorkflowForm"
																				property="menorPrazoEspecial" />
																		</td>
																	</tr>
																</table>
															</div></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td valign="top">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="espacoPqn">&nbsp;</td>
												</tr>
											</table>

											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="espacoPqn">&nbsp;</td>
												</tr>
											</table>
											<table width="99%" border="0" cellspacing="0" cellpadding="0"
												align="center">
												<tr>
													<td class="principalPstQuadroLinkSelecionadoGrande"
														name="tdProcurar" id="tdProcurar"
														onClick="AtivarPasta('PROCURAR')"><bean:message
															key="prompt.Etapa" /></td>
													<td class="principalPstQuadroLinkNormalGrande"
														name="tdProcesso" id="tdProcesso"
														onClick="AtivarPasta('PROCESSO')"><bean:message
															key="prompt.DetalheDoProcesso" /></td>
													<td class="principalPstQuadroLinkNormalGrande"
														name="tdConteudo" id="tdConteudo"
														onClick="AtivarPasta('CONTEUDO')"><bean:message
															key="prompt.funcionariosCopiados" /></td>
													<td class="principalLabel">&nbsp;</td>
												</tr>
											</table>
											<table width="99%" border="0" cellspacing="0" cellpadding="0"
												align="center" class="principalBordaQuadro"
												style="height: 245px">
												<tr>
													<td valign="top">
														<div id="procurar"
															style="width: 100%; z-index: 2; height: 100%; display: block">
															<iframe name="ifrmDetalhes"
																src="CarregaDetalheProcesso.do?id_depr_cd_desenhoprocesso=<bean:write name='fluxoWorkflowForm' property='id_depr_cd_desenhoprocesso' />&alteracaoPrimeiraEtapa=<bean:write name='fluxoWorkflowForm' property='alteracaoPrimeiraEtapa' />&alteracaoDemaisEtapas=<bean:write name='fluxoWorkflowForm' property='alteracaoDemaisEtapas' />&id_etpr_cd_etapaprocesso=<bean:write name='fluxoWorkflowForm' property='id_etpr_cd_etapaprocesso' />"
																width="100%" scrolling="Default" frameborder="0"
																style="height: 100%"></iframe>
														</div>
														<div id="processo"
															style="width: 100%; height: 100%; display: none">
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td>&nbsp;</td>
																</tr>
															</table>
															<table width="98%" border="0" cellspacing="0"
																cellpadding="0" align="center">
																<tr>
																	<td class="principalLabelValorFixo">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0">
																			<tr valign="top">
																				<td width="51%">
																					<table width="99%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td class="principalLabel" align="right"
																								width="26%"><bean:message
																									key="prompt.Checklist" /> <img
																								src="webFiles/images/icones/setaAzul.gif"
																								width="7" height="7"></td>
																							<td class="principalLabel" width="74%"><html:select
																									property="id_chli_cd_checklist"
																									styleClass="principalObjForm">
																									<html:option value="">
																										<bean:message key="prompt.selecione_uma_opcao" />
																									</html:option>
																									<logic:present name="csCdtbChecklistChliVector">
																										<html:options
																											collection="csCdtbChecklistChliVector"
																											property="field(id_chli_cd_checklist)"
																											labelProperty="field(chli_ds_checklist)" />
																									</logic:present>
																								</html:select></td>
																						</tr>
																						<tr>
																							<td class="principalLabel" align="right"
																								width="26%">&nbsp;</td>
																							<td class="principalLabel" width="74%">&nbsp;</td>
																						</tr>
																						<tr>
																							<td class="principalLabel" align="right"
																								width="26%"><bean:message
																									key="prompt.GerarIntegracao" /><img
																								src="webFiles/images/icones/setaAzul.gif"
																								width="7" height="7"></td>
																							<td class="principalLabel" width="74%"><html:select
																									property="id_inte_cd_integracao"
																									styleClass="principalObjForm">
																									<html:option value="">
																										<bean:message key="prompt.selecione_uma_opcao" />
																									</html:option>
																									<logic:present
																										name="csCdtbIntegracaoInteVector">
																										<html:options
																											collection="csCdtbIntegracaoInteVector"
																											property="field(id_inte_cd_integracao)"
																											labelProperty="field(inte_ds_integracao)" />
																									</logic:present>
																								</html:select></td>
																						</tr>
																						<tr>
																							<td class="espacoPqn" align="right" width="26%">&nbsp;</td>
																							<td class="espacoPqn" width="74%">&nbsp;</td>
																						</tr>
																						<tr>
																							<td class="principalLabel" align="right"
																								width="26%">&nbsp;</td>
																							<td class="principalLabel" width="74%">&nbsp;
																							</td>
																						</tr>
																					</table>
																				</td>
																				<td width="2%">&nbsp;</td>
																				<td width="47%">
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td class="principalLabel" align="right"
																								width="27%"><bean:message
																									key="prompt.RespostaPadrao" /><img
																								src="webFiles/images/icones/setaAzul.gif"
																								width="7" height="7"></td>
																							<td class="principalLabel" width="68%"><html:select
																									property="id_repa_cd_respostapadrao"
																									styleClass="principalObjForm">
																									<html:option value="">
																										<bean:message key="prompt.selecione_uma_opcao" />
																									</html:option>
																									<logic:present
																										name="csCdtbRespostapadraoRepaVector">
																										<html:options
																											collection="csCdtbRespostapadraoRepaVector"
																											property="field(id_repa_cd_respostapadrao)"
																											labelProperty="field(repa_ds_respostapadrao)" />
																									</logic:present>
																								</html:select></td>
																							<td class="principalLabel" width="5%"><img
																								src="webFiles/images/icones/setaDown.gif"
																								id="btAdicionaRespostaPadrao"
																								title="<bean:message key="prompt.Adicionar" />"
																								width="21" height="18" class="geralCursoHand"
																								onclick="adicionaRespostaPadrao()"></td>
																						</tr>
																					</table>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td class="espacoPqn">&nbsp;</td>
																						</tr>
																					</table>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td valign="top">
																								<div id="lstRespostaPadrao"
																									style="width: 100%; height: 100px; display: none; overflow: auto"></div>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>

															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td class="espacoPqn">&nbsp;</td>
																</tr>
															</table>
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td class="espacoPqn">&nbsp;</td>
																</tr>
															</table>
														</div>
														<div id="conteudo"
															style="width: 100%; display: none; height: 100%;">
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td>&nbsp;</td>
																</tr>
															</table>
															<table width="98%" border="0" cellspacing="0"
																cellpadding="0" align="center">
																<tr>
																	<td class="principalLabel" align="right" width="6%"><bean:message
																			key="prompt.area" /> <img
																		src="webFiles/images/icones/setaAzul.gif" width="7"
																		height="7"></td>
																	<td class="principalLabel" width="38%"><html:select
																			property="id_area_cd_area"
																			styleClass="principalObjForm"
																			onchange="carregaCmbFuncionario()">
																			<html:option value="">
																				<bean:message key="prompt.selecione_uma_opcao" />
																			</html:option>
																			<logic:present name="csCdtbAreaAreaVector">
																				<html:options collection="csCdtbAreaAreaVector"
																					property="idAreaCdArea" labelProperty="areaDsArea" />
																			</logic:present>
																		</html:select></td>
																	<td class="principalLabel" width="15%" align="right"><bean:message
																			key="prompt.funcionario" /> <img
																		src="webFiles/images/icones/setaAzul.gif" width="7"
																		height="7"></td>
																	<td class="principalLabel" width="37%"><iframe
																			name="ifrmCmbFuncionario"
																			src="CarregaCmbFuncionarioFluxoWorkflow.do"
																			width="100%" height="20" scrolling="No"
																			frameborder="0" marginwidth="0" marginheight="0"></iframe>
																	</td>
																	<td class="principalLabel" width="4%"><img
																		src="webFiles/images/botoes/setaDown.gif"
																		id="btAdcionaFuncCopiado"
																		title="<bean:message key="prompt.Adicionar" />"
																		width="21" height="18" class="geralCursoHand"
																		onclick="adicionaFuncionariosCopiados()"></td>
																</tr>
															</table>
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td class="espacoPqn">&nbsp;</td>
																</tr>
															</table>
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td width="7%">&nbsp;</td>
																	<td valign="top" width="88%">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0" class="principalLstCab"
																			align="center">
																			<tr>
																				<td width="5%">&nbsp;</td>
																				<td width="50%">&nbsp;<bean:message
																						key="prompt.area" /></td>
																				<td width="45%"><bean:message
																						key="prompt.funcionario" /></td>
																			</tr>
																		</table>
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0" align="center">
																			<tr>
																				<td valign="top">
																					<div id="lstFuncionariosCopiados"
																						style="width: 100%; height: 105px; overflow: auto"></div>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width="5%">&nbsp;</td>
																</tr>
															</table>

															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td class="espacoPqn">&nbsp;</td>
																</tr>
															</table>
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td class="espacoPqn">&nbsp;</td>
																</tr>
															</table>
														</div>
													</td>
												</tr>
											</table>

										</td>
									</tr>
								</table>

								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td align="right" width="97%"><img
											src="webFiles/images/botoes/gravar.gif" id="btGravar"
											width="20" height="20" class="geralCursoHand"
											title="<bean:message key="prompt.gravar" />"
											onclick="gravar()"></td>
										<td width="3%" align="center"><img
											src="webFiles/images/botoes/cancelar.gif" id="btCancelar"
											width="20" height="20" class="geralCursoHand"
											title="<bean:message key="prompt.cancelar" />"
											onclick="cancelar()"></td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td width="4" height="100%"
					style="background: url(webFiles/images/linhas/VertSombra.gif) 0 0 repeat-y;">&nbsp;</td>
			</tr>
			<tr>
				<td width="100%"><img
					src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img
					src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
					height="4"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="espacoPqn">&nbsp;</td>
			</tr>
		</table>
		<table width="30" border="0" cellspacing="0" cellpadding="0"
			align="right">
			<tr>
				<td><img src="webFiles/images/botoes/out.gif" width="25"
					height="25" class="geralCursoHand"
					title="<bean:message key="prompt.sair"/>"
					onClick="javascript:window.close()"></td>
			</tr>
		</table>

		<script>
			//MONTA A TREEVIEW COM AS ETAPAS DO DESENHO DO PROCESSO
			<logic:present name="treeViewVector">
			var indice = new Number(0);

			<logic:iterate name="treeViewVector" id="treeViewVector">
			var etapaOk = false;
			if ('<bean:write name="treeViewVector" property="field(etpr_in_etapainicial)" />' == 'S')
				etapaOk = true;

			var encaminharPara = '<bean:write name="treeViewVector" property="field(etpr_in_encaminharpara)" />';
			var encaminharParaSomenteNome = '';
			if (encaminharPara == 'A') {
				//encaminharPara =  '<bean:write name="treeViewVector" property="field(etpr_nr_etapa)" />' + ' - ' + '<bean:write name="treeViewVector" property="field(etpr_ds_tituloetapa)" /> (<bean:write name="treeViewVector" property="field(func_nm_funcionario)" />)';
				encaminharPara = '<bean:write name="treeViewVector" property="field(etpr_nr_etapa)" />'
						+ ' - '
						+ '<bean:write name="treeViewVector" property="field(func_nm_funcionario)" />';
				encaminharParaSomenteNome = '<bean:write name="treeViewVector" property="field(func_nm_funcionario)" />';
			} else if (encaminharPara == 'E') {
				encaminharPara = '<bean:write name="treeViewVector" property="field(etpr_nr_etapa)" />'
						+ ' - Etapa '
						+ '<bean:write name="treeViewVector" property="field(etpr_nr_encaminharetapa)" />';
				encaminharParaSomenteNome = 'Etapa '
						+ '<bean:write name="treeViewVector" property="field(etpr_nr_encaminharetapa)" />';
			} else if (encaminharPara == 'M') {
				encaminharPara = '<bean:write name="treeViewVector" property="field(etpr_nr_etapa)" />'
						+ ' - Gerador Manifesta��o';
				encaminharParaSomenteNome = 'Gerador Manifesta��o';
			}

			arrayTreeView[indice] = new Object();
			arrayTreeView[indice].idPai = '<bean:write name="treeViewVector" property="field(etpr_nr_etapapredecessora)" />';
			arrayTreeView[indice].idFilho = '<bean:write name="treeViewVector" property="field(etpr_nr_etapa)" />';
			arrayTreeView[indice].descFilho = encaminharPara;
			arrayTreeView[indice].textoFuncao = ''
			arrayTreeView[indice].etapaOk = etapaOk;
			arrayTreeView[indice].prazoNormal = '<bean:write name="treeViewVector" property="field(etpr_nr_prazonormal)" />';
			arrayTreeView[indice].prazoEspecial = '<bean:write name="treeViewVector" property="field(etpr_nr_prazoespecial)" />';
			arrayTreeView[indice].idEtapaProcesso = '<bean:write name="treeViewVector" property="field(id_etpr_cd_etapaprocesso)" />';
			arrayTreeView[indice].perguntaProxEtapa = '<bean:write name="treeViewVector" property="field(etpr_ds_perguntafluxo)" />';
			arrayTreeView[indice].encaminharEtapa = '<bean:write name="treeViewVector" property="field(etpr_nr_encaminharetapa)" />';
			arrayTreeView[indice].ultimaEtapa = '<bean:write name="treeViewVector" property="field(etpr_in_ultimaetapa)" />';
			arrayTreeView[indice].tituloEtapa = '<bean:write name="treeViewVector" property="field(etpr_ds_tituloetapa)" />';
			arrayTreeView[indice].encaminharParaSomenteNome = encaminharParaSomenteNome;
			indice++;
			</logic:iterate>
			</logic:present>

			//MONTA A LISTA DE FUNCIONARIOS COPIADOS
			<logic:present name="vectorFuncCopiados">
			<logic:iterate name="vectorFuncCopiados" id="vectorFuncCopiados">
			adicionaFuncionariosCopiados2(
					'<bean:write name="vectorFuncCopiados" property="field(id_area_cd_area)" />',
					'<bean:write name="vectorFuncCopiados" property="field(id_func_cd_funcionario)" />',
					'<bean:write name="vectorFuncCopiados" property="field(area_ds_area)" />',
					'<bean:write name="vectorFuncCopiados" property="field(func_nm_funcionario)" />');
			</logic:iterate>
			</logic:present>

			//MONTA A LISTA DE RESPOSTA PADR�O
			<logic:present name="vectorDetalheProcesso">
			<logic:iterate name="vectorDetalheProcesso" id="vectorDetalheProcesso">
			adicionaRespostaPadrao2(
					'<bean:write name="vectorDetalheProcesso" property="field(id_repa_cd_respostapadrao)" />',
					'<bean:write name="vectorDetalheProcesso" property="field(repa_ds_respostapadrao)" />');
			</logic:iterate>
			</logic:present>
		</script>

	</html:form>
	<div id="aguarde"
		style="left: 340px; top: 270px; width: 199px; height: 148px; display: none">
		<div align="center">
			<iframe src="webFiles/aguarde.jsp" width="100%" height="100%"
				scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
		</div>
	</div>
</div>
</body>
</html>
