<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsNatbFuncionariopublFupuVo"%>
<%@ page import="java.util.Vector"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");
%>
	
<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key='prompt.funcoes'/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key='prompt.funcoes'/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="<bean:message key='prompt.funcoes'/>/date-picker.js"></script>


<body name='bodyMenu' id='bodyMenu' bgcolor="#FFFFFF" text="#000000" leftmargin="0" class="principalBgrPageIFRM" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsCdtbPublicoPublForm" action="/AdministracaoCsCdtbPublicoPubl.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	
	<input type="hidden" name="qtdFuncDisp" value="0"/>
	<input type="hidden" name="qtdFuncSel" value="0"/>
	<input type="hidden" name="qtdFunc" value="0"/>

<script language="JavaScript">

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
</script>

	
				<div style="
					width: 310;
					height: 80px;
					border-top: #CDCDCD 1px solid;
					border-left: #CDCDCD 1px solid;
					border-right: #CDCDCD 1px solid;
					border-bottom: #CDCDCD 1px solid;
					background-color: #FFFFFF;
					overflow: auto">
					
						 <logic:present name="csCdtbFuncionarioFuncVector">
							  <div id="trFuncDispZero"
								style="visibility: visible; display: block; height: 1px; border:0px; padding:0px; margin:0px; width:1px; font-size: 1px;">
							  </div>
							  <logic:iterate id="funcDispVector" name="csCdtbFuncionarioFuncVector" indexId="numero">
									<script language="JavaScript">
										document.administracaoCsCdtbPublicoPublForm.qtdFunc.value="<%=numero.intValue()+1%>";
									</script>
									<div id="trFuncDisp<%= numero%>"
										idfunc="<bean:write name="funcDispVector" property="idFuncCdFuncionario" />"
										idArea="<bean:write name="funcDispVector" property="csCdtbAreaAreaVo.idAreaCdArea" />"  
										name="trFuncDispName<bean:write name="funcDispVector" property="idFuncCdFuncionario" />"
										suprimido="N" 
										insercao=""
										ativacao=""
										status=""
										style=" visibility: visible">
										<table cellpadding="0" cellspacing="0" border="0">	
											<tr>
												<td class="principalLstPar" width=5>
													<input type="hidden" id="idArea<%= numero%>" value="<bean:write name="funcDispVector" property="csCdtbAreaAreaVo.idAreaCdArea" />">
													<input name="chkOperadorSelecionado" type="checkbox" id="chkFuncDisp<%= numero %>">
												</td>
												<td class="principalLstPar" width=300>
													<label style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-color: black black #CCCCCC; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; " id="lblDisp<%= numero%>">
													<!-- bean:write name="funcDispVector" property="funcNmFuncionario" / -->
													<%=acronymChar(((CsCdtbFuncionarioFuncVo)funcDispVector).getFuncNmFuncionario(), 37)%>
													</label>
												</td>
											</tr>
										</table>	
									</div>
									<!-- tr colspan="2" id="trfuncdispname<%= numero%>" 
										idfunc="<bean:write name="funcDispVector" property="idFuncCdFuncionario" />"
										idArea="<bean:write name="funcDispVector" property="csCdtbAreaAreaVo.idAreaCdArea" />"  
										name="trfuncdispname<%= numero%>"
										suprimido="N" 
										insercao=""
										ativacao=""
										status=""
										style="display: none">
										<input type="hidden" id="idArea<%=numero%>" value="<bean:write name="funcDispVector" property="csCdtbAreaAreaVo.idAreaCdArea" />">

										<td class="principalLstPar" width=5><input name="chkOperadorSelecionado" type="checkbox" id="chkFuncDisp<%= numero %>"></td>
										<td class="principalLstPar"><bean:write name="funcDispVector" property="funcNmFuncionario" /></td>
									</tr-->
						 		</logic:iterate>
						 </logic:present>
				</div>          	
</html:form>
</body>
</html>
