<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>

<script language="JavaScript">

var nCountIniciar = 0;

function iniciarTela(){
	try{
	
		var idAsn1 = parent.document.getElementById("administracaoCsCdtbProdutoAssuntoPrasForm").idAsn1CdAssuntoNivel1.value;
		var idAsn2 = parent.ifrmCmbAssuntoNivel2Pras.document.getElementById("administracaoCsCdtbProdutoAssuntoPrasForm").idAsn2CdAssuntoNivel2.value;
	
		document.administracaoCsCdtbProdutoImagemPrimForm.idAsn1CdAssuntonivel1.value = idAsn1;
		document.administracaoCsCdtbProdutoImagemPrimForm.idAsn2CdAssuntonivel2.value = idAsn2;
	
		document.administracaoCsCdtbProdutoImagemPrimForm.tela.value="<%=MAConstantes.TELA_LST_IMAGEM_PRAS%>";
		document.administracaoCsCdtbProdutoImagemPrimForm.acao.value="<%=Constantes.ACAO_CONSULTAR%>";
		document.administracaoCsCdtbProdutoImagemPrimForm.target="ifrmLstArquivoPras";
		document.administracaoCsCdtbProdutoImagemPrimForm.submit();
		
	}catch(e){
		if(nCountIniciar<5){
			setTimeout('iniciarTela()',200);
			nCountIniciar++;
		}
	}
}

function anexaArquivo(){
	ifrmArqPras.anexaArquivo();
}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();">

<html:form styleId="administracaoCsCdtbProdutoImagemPrimForm" action="/AdministracaoCsCdtbProdutoImagemPrim.do">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="idAsn1CdAssuntonivel1" />
<html:hidden property="idAsn2CdAssuntonivel2" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="espacoPqn">&nbsp;</td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalLabel" width="10%"><bean:message key="prompt.img"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td class="principalLabel" width="50%">
        <iframe name="ifrmArqPras" src="AdministracaoCsCdtbProdutoImagemPrim.do?tela=<%=MAConstantes.TELA_IMG_PRAS%>" height="20" width="100%" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>
    </td>
    <td class="principalLabel" width="40%">&nbsp;<span class="geralCursoHand"><img src="webFiles/images/icones/arquivos.gif" width="25" height="24" onclick="anexaArquivo();" title='<bean:message key="prompt.anexar_imagem"/>'></span></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
  	<td class="espacoPqn">&nbsp;</td>
  </tr>
  <tr> 
    <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td class="principalLstCab" width="5%">&nbsp;</td>
				<td class="principalLstCab" width="60%">Nome</td>
				<td class="principalLstCab" width="30%">&nbsp</td>
				<td class="principalLstCab" width="5%">&nbsp;</td>
			</tr>		
		</table> 
    </td>
  </tr>
  <tr>
	<td>
		<iframe name="ifrmLstArquivoPras" src="AdministracaoCsCdtbProdutoImagemPrim.do?tela=<%=MAConstantes.TELA_LST_IMAGEM_PRAS%>" width="100%" height="150" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>
	</td>
  </tr>
</table>
</html:form>
</body>
</html>