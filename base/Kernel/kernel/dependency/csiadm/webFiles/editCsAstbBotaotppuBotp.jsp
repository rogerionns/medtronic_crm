<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

function MontaLista(){
	ifmlstCsAstbBotaotppuBotp.location.href= "AdministracaoCsAstbBotaotppuBotp.do?tela=ifrmLstCsAstbBotaotppuBotp&acao=<%=Constantes.ACAO_VISUALIZAR%>&idTppuCdTipopublico=" + document.administracaoCsAstbBotaotppuBotpForm.idTppuCdTipopublico.value;
}

function desabilitaCamposBotaotppu(){
	document.administracaoCsAstbBotaotppuBotpForm.idTppuCdTipopublico.disabled= true;
	document.administracaoCsAstbBotaotppuBotpForm.idBotaCdBotao.disabled= true;
}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista();">
<html:form styleId="administracaoCsAstbBotaotppuBotpForm" action="/AdministracaoCsAstbBotaotppuBotp.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.tipoPublico"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idTppuCdTipopublico" styleClass="principalObjForm" onchange="MontaLista()"> 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbTipopublicoTppuVector" property="idTppuCdTipoPublico" labelProperty="tppuDsTipoPublico"/> 
      </html:select> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.botao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idBotaCdBotao" styleClass="principalObjForm"  > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbBotaoBotaVector" property="idBotaCdBotao" labelProperty="botaDsBotao"/> 
      </html:select> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td class="principalLabel" width="28%"> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="220"> 
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="6%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="36%"> <bean:message key="prompt.botao"/></td>
          <td class="principalLstCab" width="33%">&nbsp;</td>
          <td class="principalLstCab" width="11%">&nbsp;</td>
          <td class="principalLstCab" width="14%">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <iframe id="ifmlstCsAstbBotaotppuBotp" name="ifmlstCsAstbBotaotppuBotp" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script language="JavaScript">
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICOBOTAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsAstbBotaotppuBotpForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICOBOTAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposBotaotppu();
			}else{
				document.administracaoCsAstbBotaotppuBotpForm.idTppuCdTipopublico.disabled= false;
				document.administracaoCsAstbBotaotppuBotpForm.idBotaCdBotao.disabled= false;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script language="JavaScript">
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICOBOTAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CONTATOCLIENTE_TIPODEPUBLICOBOTAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsAstbBotaotppuBotpForm.imgGravar);	
				desabilitaCamposBotaotppu();
			}
		</script>
</logic:equal>

</html>

<script language="JavaScript">
	try{document.administracaoCsAstbBotaotppuBotpForm.idTppuCdTipopublico.focus();}
	catch(e){}
</script>