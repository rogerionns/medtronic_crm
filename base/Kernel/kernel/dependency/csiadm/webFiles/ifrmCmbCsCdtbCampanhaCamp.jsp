<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.afvpj.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript">

	function MontaCmb(){
		if(window.parent.document.administracaoCsCdtbPublicoPublForm.idCampCdCampanha.value > 0 ){
			window.parent.ifrmConsultaCampanha.location.href= "AdministracaoCsCdtbPublicoPubl.do?tela=<%=AdmConstantes.TABLE_IFRM_CONSULTA_CAMPANHA%>&acao=<%=Constantes.ACAO_CONSULTAR%>&idCampCdCampanha=" + window.parent.document.administracaoCsCdtbPublicoPublForm.idCampCdCampanha.value;
		}
	}

	function MontaCmbHidden(){
		if (parent.ifrmCsCdtbPublicoPublSubAbas != undefined){
			if (parent.ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm != undefined){
				if (parent.ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.idTpsgCdTpseguro'] != undefined){
					if (document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.idTpsgCdTpseguro'].value>0){
						parent.ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.idTpsgCdTpseguro'].value=document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.idTpsgCdTpseguro'].value; 
						parent.ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.idTpsgCdTpseguro'].disabled=true;
						parent.ifrmCsCdtbPublicoPublSubAbas.cmbFamiliaseguro.MontaCmb();
					}else{
						parent.ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.idTpsgCdTpseguro'].value=''; 
						parent.ifrmCsCdtbPublicoPublSubAbas.document.administracaoCsCdtbPublicoPublForm['csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.idTpsgCdTpseguro'].disabled=false;
					}
				}
			}
		}
	}
	
</script>
</head>
<body class="principalBgrPageIFRM" leftmargin="0" topmargin="0" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');MontaCmbHidden()">
<html:form styleId="administracaoCsCdtbPublicoPublForm" action="/AdministracaoCsCdtbPublicoPubl.do">
<table width="100%" height="20"  border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td width="33%" class="principalLabel"> 
	<html:text property="publDsNatureza" styleClass="text" disabled="true" />
  </td>
  <td width="33%" class="principalLabel">
  	<html:text property="publDsCanal" styleClass="text" disabled="true" />
  </td>
  <td width="33%" class="principalLabel"> 
	<html:text property="publDsSegmento" styleClass="text" disabled="true" />  
  </td> 
 </tr>
 <html:hidden property="csCdtbPublicoPublVo.csCdtbPublicoComplPucoVo.idTpsgCdTpseguro"/>
</table>
</html:form>
</body>
</html>