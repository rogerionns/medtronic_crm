<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
	function desabilitaCamposJustificativa(){
		document.administracaoCsCdtbJustificativaJustForm.justDsJustificativa.disabled= true;	
		document.administracaoCsCdtbJustificativaJustForm.justDhInativo.disabled= true;
		document.administracaoCsCdtbJustificativaJustForm.idResuCdResultado.disabled= true;
		document.administracaoCsCdtbJustificativaJustForm.justDsCodcorporativo.disabled= true;
		document.administracaoCsCdtbJustificativaJustForm.justDsCodtelefonia.disabled = true;
		document.administracaoCsCdtbJustificativaJustForm.justInComissao.disabled = true;
	}

	function inicio(){
		setaAssociacaoMultiEmpresa();
		setaChavePrimaria(administracaoCsCdtbJustificativaJustForm.idJustCdJustificativa.value);
	}	
</script>

<body class= "principalBgrPageIFRM" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbJustificativaJustForm" action="/AdministracaoCsCdtbJustificativaJust.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="erro" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />	
	<input type="hidden" name="limparSessao" value="false"></input>

	<br>
	 <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><!-- ## --> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td width="10%"> 
            <html:text property="idJustCdJustificativa" styleClass="text" disabled="true" />
          </td>
          <td width="28%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="justDsJustificativa" styleClass="text" maxlength="40" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.resultado"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> <html:select property="idResuCdResultado" styleClass="principalObjForm" >
 	        <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
 	        	<html:options collection="csCdtbResultadoResuVector" property="idResuCdResultado" labelProperty="resuDsResultado"/> 
		    </html:select> </td>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codigoCorporativo"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="justDsCodcorporativo" styleClass="text" maxlength="20" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
        
        <tr> 
          <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.codTelefonia"/>
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
          <td colspan="2"> 
            <html:text property="justDsCodtelefonia" styleClass="text" maxlength="20" /> 
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
              
        <tr> 
          <td width="13%" align="right" class="principalLabel">&nbsp;<bean:message key="prompt.comissao"/>
          	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
          </td>
          <td colspan="2"><html:checkbox value="true" property="justInComissao"/></td>
        </tr>
        
        
        <tr> 
          <td width="13%">&nbsp;</td>
          <td width="10%">&nbsp;</td>
          <td class="principalLabel" width="28%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="right" width="83%"> 
                  <html:checkbox value="true" property="justDhInativo"/><!-- @@ --></td>
            
                <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/><!-- ## --> 
                </td>
              </tr>
            </table>
          </td>
          <td width="31%">&nbsp;</td>
        </tr>
      </table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			desabilitaCamposJustificativa();
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_JUSTIFICATIVA_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbJustificativaJustForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_JUSTIFICATIVA_INCLUSAO_CHAVE%>')){
				desabilitaCamposJustificativa();
			}else{
				document.administracaoCsCdtbJustificativaJustForm.idJustCdJustificativa.disabled= false;
				document.administracaoCsCdtbJustificativaJustForm.idJustCdJustificativa.value= '';
				document.administracaoCsCdtbJustificativaJustForm.idJustCdJustificativa.disabled= true;
			}
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_JUSTIFICATIVA_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDECAMPANHA_JUSTIFICATIVA_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbJustificativaJustForm.imgGravar);	
				desabilitaCamposJustificativa();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbJustificativaJustForm.justDsJustificativa.focus();}
	catch(e){}
</script>