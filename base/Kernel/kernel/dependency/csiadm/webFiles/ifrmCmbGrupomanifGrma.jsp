<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

	function submeteForm(){
		parent.document.administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value = administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value;
	
		administracaoCsCdtbEventoFollowUpEvfuForm.tela.value = "<%=MAConstantes.TELA_CMB_TPMANIFESTACAO%>";
		administracaoCsCdtbEventoFollowUpEvfuForm.target = parent.ifrmCmbTpmanifestacao.name;
		
		if(administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].length == 2)
			administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].selectedIndex = 1;
		
		administracaoCsCdtbEventoFollowUpEvfuForm.submit();
		
		if(parent.document.administracaoCsCdtbEventoFollowUpEvfuForm.todosGrupos.checked
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
			|| parent.document.administracaoCsCdtbEventoFollowUpEvfuForm.todosSuperGrupos.checked 
<%}%>
			){
			administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].selectedIndex = 0;
			administracaoCsCdtbEventoFollowUpEvfuForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].disabled = true;
		}
	}
	
	function inicio(){
		submeteForm();
	}

</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0>

<html:form styleId="administracaoCsCdtbEventoFollowUpEvfuForm" action="/AdministracaoCsCdtbEventoFollowUpEvfu.do">

	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
	
	<html:select property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" styleClass="principalObjForm" onchange="submeteForm();">
		<html:option value="">
			<bean:message key="prompt.selecione_uma_opcao"/></html:option>
		<html:options collection="csCdtbGrupomanifestacaoGrmaVector" property="idGrmaCdGrupoManifestacao" labelProperty="grmaDsGrupoManifestacao"/>
	</html:select>

</html:form>
</body>
</html>