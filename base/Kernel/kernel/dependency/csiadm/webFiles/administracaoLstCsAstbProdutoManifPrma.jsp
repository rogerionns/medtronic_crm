<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>


<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	

long idEmpresa = empresaVo.getIdEmprCdEmpresa();

String maxRegistros = "";
try{
	maxRegistros = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_LIMITE_LINHAS_RESULTADO,idEmpresa);
}catch(Exception e){}
	
if(maxRegistros == null || maxRegistros.equals("")){
	maxRegistros = "100";
}

%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

parent.parent.parent.parent.parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
	
function excluir(codigo1,codigo2,codigo3) {
	confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>');
	if(confirmacao == true){
		parent.parent.submeteExcluir(codigo1,codigo2,codigo3);
	}else{
	}
}
	
function submeteFormExcluir() {

	administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_LST_CS_ASTB_PRODUTOMANIF_PRMA%>';
	administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	administracaoCsAstbProdutoManifPrmaForm.submit();

}

	
function edit(idLinha, idProduto,idVariedade, idManif, idGrupo, idTipo, idPesq, idDest, prasInProdutoassunto, prmaInWeb) {

	parent.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= Constantes.ACAO_EDITAR %>';
	
	document.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.value = idLinha;
	document.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.value = idProduto;
	document.administracaoCsAstbProdutoManifPrmaForm.idAsn2CdAssuntoNivel2.value = idVariedade;	
	document.administracaoCsAstbProdutoManifPrmaForm.idMatpCdManifTipo.value = idManif;
	document.administracaoCsAstbProdutoManifPrmaForm.idGrmaCdGrupoManifestacao.value = idGrupo;
	document.administracaoCsAstbProdutoManifPrmaForm.idTpmaCdTpManifestacao.value = idTipo;
	document.administracaoCsAstbProdutoManifPrmaForm.idPesqCdPesquisa.value = idPesq;
	document.administracaoCsAstbProdutoManifPrmaForm.idDeprCdDestinoproduto.value = idDest;
	document.administracaoCsAstbProdutoManifPrmaForm.tipoLinha.value = prasInProdutoassunto;
	parent.setFlagProduto(1);

	if(prmaInWeb == "S")
	{
		parent.administracaoCsAstbProdutoManifPrmaForm.prmaInWeb.checked = true;
	}
	else
	{
		parent.administracaoCsAstbProdutoManifPrmaForm.prmaInWeb.checked = false;
	}
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>'
		document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_LINHA %>';
		document.administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbLinhaLinh.name;
		document.administracaoCsAstbProdutoManifPrmaForm.submit();
	<%}else{%>
		document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= MAConstantes.ACAO_POPULACOMBO %>'
		administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_PRODUTO %>';
		administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbLinhaProduto.name;
		administracaoCsAstbProdutoManifPrmaForm.submit();
	<%}%>
	
/*	administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_PRODUTO %>';
	administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbLinhaProduto.name;
	administracaoCsAstbProdutoManifPrmaForm.submit();

	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>
		administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_VARIEDADE %>';
		administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbLinhaProdutoVariedade.name;
		administracaoCsAstbProdutoManifPrmaForm.submit();
	<%}%>
*/
	
	document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_MANIFESTACAO %>';
	document.administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbManifestacao.name;
	document.administracaoCsAstbProdutoManifPrmaForm.submit();
	
	/*
	administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_GRUPOMANIF %>';
	administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbGrupoManif.name;
	administracaoCsAstbProdutoManifPrmaForm.submit();
	
	administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_TIPOMANIF %>';
	administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbLinhaTipoManif.name;
	administracaoCsAstbProdutoManifPrmaForm.submit();
	*/
	
	document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_PESQ %>';
	document.administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbPesq.name;
	document.administracaoCsAstbProdutoManifPrmaForm.submit();
	
	document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CMB_PRMA_DESTPROD %>';
	document.administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCmbDestprod.name;
	document.administracaoCsAstbProdutoManifPrmaForm.submit();
	
	document.administracaoCsAstbProdutoManifPrmaForm.tela.value = '<%= MAConstantes.TELA_CAMPOS_MANIFESTACAO %>';
	document.administracaoCsAstbProdutoManifPrmaForm.acao.value = '<%= Constantes.ACAO_EDITAR %>';
	document.administracaoCsAstbProdutoManifPrmaForm.target = parent.ifrmCamposManif.name;
	
	parent.desabilita = true;
	
	document.administracaoCsAstbProdutoManifPrmaForm.submit();

}
</script>



</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0" scroll="no">
<html:form styleId="administracaoCsAstbProdutoManifPrmaForm" action="/AdministracaoCsAstbProdutoManifPrma.do"> 
	
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="idLinhCdLinha" />
	<html:hidden property="idAsn1CdAssuntoNivel1" />
	<html:hidden property="idAsn2CdAssuntoNivel2" />	
	<html:hidden property="idTpmaCdTpManifestacao" />
	<html:hidden property="idDeprCdDestinoproduto" />	
	<html:hidden property="idPesqCdPesquisa" />
	<html:hidden property="idMatpCdManifTipo" />	
	<html:hidden property="idGrmaCdGrupoManifestacao" />
	<html:hidden property="tipoLinha" />
	<script>
		var possuiRegistros=false;
		var totalRegistros=0;
	</script>
	
	<div id="lstManifProd" style="overflow: auto; height: 150; width: 708; visibility: visible" onScroll="parent.document.getElementById('manifProduto').scrollLeft=this.scrollLeft;">	
		<logic:iterate id="ccttrtVector" name="csAstbProdutoManifPrmaVector" indexId="sequencia"> 
			<script>
				possuiRegistros=true;
				totalRegistros=<%=sequencia%>;
			</script>

			<table width="810px" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
				    <td width="3%" align="left" class="principalLstParMao" > 
				    	<input type="checkbox" name="listaExcluir" id="listaExcluir" value="<bean:write name="ccttrtVector" property="idAsn1CdAssuntoNivel1"/>|<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2"/>|<bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />">
				    </td>
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
				    <td class="principalLstParMao" width="8%" align="left" onclick="edit(<bean:write name="ccttrtVector" property="idLinhCdLinha"/>, <bean:write name="ccttrtVector" property="idAsn1CdAssuntoNivel1"/>,<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2"/>, <bean:write name="ccttrtVector" property="idMatpCdManifTipo" />, <bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />, <bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />, <bean:write name="ccttrtVector" property="idPesqCdPesquisa" />, <bean:write name="ccttrtVector" property="idDeprCdDestinoproduto" />, '<bean:write name="ccttrtVector" property="prasInProdutoAssunto" />', '<bean:write name="ccttrtVector" property="prmaInWeb" />');">
				    	<script>acronym('<bean:write name="ccttrtVector" property="linhDsLinha" />', 8);</script>&nbsp;
				    </td>
				    <td class="principalLstParMao" width="10%" align="left" onclick="edit(<bean:write name="ccttrtVector" property="idLinhCdLinha"/>, <bean:write name="ccttrtVector" property="idAsn1CdAssuntoNivel1"/>,<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2"/>, <bean:write name="ccttrtVector" property="idMatpCdManifTipo" />, <bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />, <bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />, <bean:write name="ccttrtVector" property="idPesqCdPesquisa" />, <bean:write name="ccttrtVector" property="idDeprCdDestinoproduto" />, '<bean:write name="ccttrtVector" property="prasInProdutoAssunto" />', '<bean:write name="ccttrtVector" property="prmaInWeb" />');">
						<script>acronym('<bean:write name="ccttrtVector" property="asn1DsAssuntoNivel1" />', 14);</script>&nbsp;
				    </td>
				<%} else {%>
				    <td class="principalLstParMao" width="22%" align="left" onclick="edit(<bean:write name="ccttrtVector" property="idLinhCdLinha"/>, <bean:write name="ccttrtVector" property="idAsn1CdAssuntoNivel1"/>,<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2"/>, <bean:write name="ccttrtVector" property="idMatpCdManifTipo" />, <bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />, <bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />, <bean:write name="ccttrtVector" property="idPesqCdPesquisa" />, <bean:write name="ccttrtVector" property="idDeprCdDestinoproduto" />, '<bean:write name="ccttrtVector" property="prasInProdutoAssunto" />', '<bean:write name="ccttrtVector" property="prmaInWeb" />');">
						<script>acronym('<bean:write name="ccttrtVector" property="asn1DsAssuntoNivel1" />', 25);</script>&nbsp;
				    </td>
				<%}%>
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")){%>				
					<td class="principalLstParMao" width="11%" align="left" onclick="edit(<bean:write name="ccttrtVector" property="idLinhCdLinha"/>, <bean:write name="ccttrtVector" property="idAsn1CdAssuntoNivel1"/>,<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2"/>, <bean:write name="ccttrtVector" property="idMatpCdManifTipo" />, <bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />, <bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />, <bean:write name="ccttrtVector" property="idPesqCdPesquisa" />, <bean:write name="ccttrtVector" property="idDeprCdDestinoproduto" />, '<bean:write name="ccttrtVector" property="prasInProdutoAssunto" />', '<bean:write name="ccttrtVector" property="prmaInWeb" />');">
						<script>acronym('<bean:write name="ccttrtVector" property="asn2DsAssuntoNivel2" />', 12);</script>&nbsp;	
				    </td>
				    <td class="principalLstParMao" width="16%" align="left" onclick="edit(<bean:write name="ccttrtVector" property="idLinhCdLinha"/>, <bean:write name="ccttrtVector" property="idAsn1CdAssuntoNivel1"/>,<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2"/>, <bean:write name="ccttrtVector" property="idMatpCdManifTipo" />, <bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />, <bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />, <bean:write name="ccttrtVector" property="idPesqCdPesquisa" />, <bean:write name="ccttrtVector" property="idDeprCdDestinoproduto" />, '<bean:write name="ccttrtVector" property="prasInProdutoAssunto" />', '<bean:write name="ccttrtVector" property="prmaInWeb" />');">
						&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="matpDsManifTipo" />', 13);</script>&nbsp;
				    </td>
				<%} else {%>
				    <td class="principalLstParMao" width="23%" align="left" onclick="edit(<bean:write name="ccttrtVector" property="idLinhCdLinha"/>, <bean:write name="ccttrtVector" property="idAsn1CdAssuntoNivel1"/>,<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2"/>, <bean:write name="ccttrtVector" property="idMatpCdManifTipo" />, <bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />, <bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />, <bean:write name="ccttrtVector" property="idPesqCdPesquisa" />, <bean:write name="ccttrtVector" property="idDeprCdDestinoproduto" />, '<bean:write name="ccttrtVector" property="prasInProdutoAssunto" />', '<bean:write name="ccttrtVector" property="prmaInWeb" />');">
						<script>acronym('<bean:write name="ccttrtVector" property="matpDsManifTipo" />', 25);</script>&nbsp;
				    </td>
				<%}%>
				    <td class="principalLstParMao" width="15%" align="left" onclick="edit(<bean:write name="ccttrtVector" property="idLinhCdLinha"/>, <bean:write name="ccttrtVector" property="idAsn1CdAssuntoNivel1"/>,<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2"/>, <bean:write name="ccttrtVector" property="idMatpCdManifTipo" />, <bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />, <bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />, <bean:write name="ccttrtVector" property="idPesqCdPesquisa" />, <bean:write name="ccttrtVector" property="idDeprCdDestinoproduto" />, '<bean:write name="ccttrtVector" property="prasInProdutoAssunto" />', '<bean:write name="ccttrtVector" property="prmaInWeb" />');">
				    	<script>acronym('<bean:write name="ccttrtVector" property="grmaDsGrupoManifestacao" />', 15);</script>&nbsp;
				    </td>
				    <td  class="principalLstParMao" width="18%" align="left" onclick="edit(<bean:write name="ccttrtVector" property="idLinhCdLinha"/>, <bean:write name="ccttrtVector" property="idAsn1CdAssuntoNivel1"/>,<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2"/>, <bean:write name="ccttrtVector" property="idMatpCdManifTipo" />, <bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />, <bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />, <bean:write name="ccttrtVector" property="idPesqCdPesquisa" />, <bean:write name="ccttrtVector" property="idDeprCdDestinoproduto" />, '<bean:write name="ccttrtVector" property="prasInProdutoAssunto" />', '<bean:write name="ccttrtVector" property="prmaInWeb" />');">
				    	<script>acronym('<bean:write name="ccttrtVector" property="tpmaDsTpManifestacao" />', 22);</script>&nbsp;
				    </td>
				    <td class="principalLstParMao" width="19%" align="left" onclick="edit(<bean:write name="ccttrtVector" property="idLinhCdLinha"/>, <bean:write name="ccttrtVector" property="idAsn1CdAssuntoNivel1"/>,<bean:write name="ccttrtVector" property="idAsn2CdAssuntoNivel2"/>, <bean:write name="ccttrtVector" property="idMatpCdManifTipo" />, <bean:write name="ccttrtVector" property="idGrmaCdGrupoManifestacao" />, <bean:write name="ccttrtVector" property="idTpmaCdTpManifestacao" />, <bean:write name="ccttrtVector" property="idPesqCdPesquisa" />, <bean:write name="ccttrtVector" property="idDeprCdDestinoproduto" />, '<bean:write name="ccttrtVector" property="prasInProdutoAssunto" />', '<bean:write name="ccttrtVector" property="prmaInWeb" />');">
						<script>acronym('<bean:write name="ccttrtVector" property="pesqDsPesquisa" />', 18);</script>&nbsp;
				    </td>
				</tr>
			</table>
		</logic:iterate>
	</div>

	<script>
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_MANIFESTACAOPRODUTO_EXCLUSAO_CHAVE%>', document.administracaoCsAstbProdutoManifPrmaForm.lixeira);
/*	if (parent.ifrmCmbLinhaProduto.administracaoCsAstbProdutoManifPrmaForm.idAsn1CdAssuntoNivel1.disabled)
		parent.ifrmCmbLinhaLinh.administracaoCsAstbProdutoManifPrmaForm.idLinhCdLinha.disabled = true;*/

		if((totalRegistros+1)>=<%=maxRegistros%>){
			alert('<bean:message key="prompt.alert_limiteRegistros"/>');
		}	
	</script>
	<iframe id="ifrmExcluir" name="ifrmExcluir" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="0%" height="0"></iframe>
</html:form>
</body>
</html>