<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function changeFiltro(opcao) {
	if (opcao == 'G') {
		document.getElementById("buscaGrupo").style.visibility = 'visible';
		document.getElementById("buscaFuncionario").style.visibility = 'hidden';
	}
	else {
		document.getElementById("buscaGrupo").style.visibility = 'hidden';
		document.getElementById("buscaFuncionario").style.visibility = 'visible';
	}
}

function cancel() {
	document.administracaoPermissionamentoForm.submit();
}


</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoPermissionamentoForm" action="/AdministracaoPermissionamento.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
				<table border="0" cellpadding="0" cellspacing="0" width="99%" align="center">
					<tr>
						<td class="principalLabel" width="20%">
							<input type="radio" name="opcao" value="G" checked="true" onclick="changeFiltro('G');"><bean:message key="prompt.grupo"/>&nbsp;
							<input type="radio" name="opcao" value="F" onclick="changeFiltro('F');"><bean:message key="prompt.funcionario"/>
						</td>
						<td class="principalLabel" width="99%">
							<div id="buscaGrupo" style="width:99%; visibility: visible; height: 20px;">
								<iframe name="ifrmCmbGrupofuncionario" id="ifrmCmbGrupofuncionario" src="AdministracaoPermissionamento.do?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MAConstantes.TELA_IFRM_CMB_GRUPOFUNCIONARIO %>" width="80%" height="22" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
							</div>							
							<div id="buscaFuncionario" style="position: absolute; width:99%; visibility: hidden; height: 20px; top: 34px;">
								<table border="0" cellpadding="0" cellspacing="0" width="100%" height="22">
									<tr>
										<td class="principalLabel" width="45%">
											<iframe name="ifrmCmbPermArea" id="ifrmCmbPermArea" src="AdministracaoPermissionamento.do?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MAConstantes.TELA_IFRM_CMB_PERMAREA %>" width="100%" height="22" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
										</td>	
										<td class="principalLabel" width="27%">
											<iframe name="ifrmCmbPermFunc" id="ifrmCmbPermFunc" src="AdministracaoPermissionamento.do?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MAConstantes.TELA_IFRM_CMB_PERMFUNC %>" width="100%" height="22" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
										</td>
										<td class="principalLabel" width="23%">&nbsp;</td>
									</tr>
								</table>
							</div>	
						</td>
					</tr>	
				</table>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="240">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>&nbsp;</td>
									<!--td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar" /><!-- ## --><!--/td>
									<!--td class="principalPstQuadroLinkNormal" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.area" /><!-- ## --><!--/td-->

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400">
					<table border="0" cellpadding="0" cellspacing="0" width="98%" height="15" align="center">
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td width="30%" class="principalLstPar">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" height="15" align="center">
							<tr>
								<td class="principalLabel" align="left" width="15%" onclick="ifrmTreeView.Expand();" style="cursor:pointer">
									<img src="webFiles/images/botoes/abrir_tudo.gif">
								</td>
								<td class="principalLabel" align="left" width="35%" onclick="ifrmTreeView.Expand();" style="cursor:pointer">
									<bean:message key="prompt.expandir"/>
								</td>	
								<td class="principalLabel" align="left" width="15%" onclick="ifrmTreeView.Collapse();" style="cursor:pointer">
									<img src="webFiles/images/botoes/fechar_tudo.gif">
								</td>
								<td class="principalLabel" align="left" onclick="ifrmTreeView.Collapse();" style="cursor:pointer">
									<bean:message key="prompt.fecharTodos"/>
								</td>	
							</tr>	
							</table>
						</td>
						<td class="principalLstPar">&nbsp;</td>
					</tr>
					</table>
				  <iframe name="ifrmTreeView" id="ifrmTreeView" src="AdministracaoPermissionamento.do?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MAConstantes.TELA_IFRM_TREE_VIEW %>" width="100%" height="400" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0"></iframe>

							
                </td>
							<td width="4" height="100%" valign="top"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>
						<tr>
							<td width="100%" height="8" valign="top"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="8" valign="top"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError(); ifrmTreeView.submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError(); cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="557px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="557px"></td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_PERMISSIONAMENTO_ALTERACAO_CHAVE%>')){
			document.administracaoPermissionamentoForm.imgGravar.disabled=true;
			document.administracaoPermissionamentoForm.imgGravar.className = 'geralImgDisable';
			document.administracaoPermissionamentoForm.imgGravar.title='';
	}

	if(!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_PERMISSIONAMENTO_GRUPO_CHAVE%>')){
		document.getElementsByName("opcao")[0].disabled = true;
		document.getElementsByName("opcao")[1].checked = true;
		changeFiltro('F');
	}else{
		document.getElementsByName("opcao")[0].disabled = false;
	}

	if(!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_PERMISSIONAMENTO_FUNCIONARIO_CHAVE%>')){
		document.getElementsByName("opcao")[1].disabled = true;
		document.getElementsByName("opcao")[0].checked = true;
		changeFiltro('G');
	}else{
		document.getElementsByName("opcao")[1].disabled = false;
	}
	
	desabilitaListaEmpresas();
	
</script>


</body>
</html>
