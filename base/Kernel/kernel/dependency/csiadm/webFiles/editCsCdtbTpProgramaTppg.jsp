<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<SCRIPT LANGUAGE="JavaScript">
	function textCounter(field, countfield, maxlimit) {
		if (field.value.length > maxlimit) 
			field.value = field.value.substring(0, maxlimit);
		else 
			countfield.value = maxlimit - field.value.length;
	}
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="administracaoCsCdtbTpProgramaTppgForm" action="/AdministracaoCsCdtbTpProgramaTppg.do">
	<input readonly type=hidden name=remLen size=3 maxlength=3 value="4000"/> 
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabel">
  <tr> 
    <td width="14%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="13%"> <html:text property="idTppgCdTipoPrograma" styleClass="text" disabled="true" /> 
    </td>
    <td colspan="3">&nbsp;</td>
    <td width="32%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="14%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="4"> <html:text property="tppgDsTipoPrograma" styleClass="text" maxlength="60" /> 
    </td>
    <td width="32%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="14%"> 
      <div align="right"><bean:message key="prompt.observacao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td colspan="5"> <html:textarea property="tppgTxObservacao" styleClass="text" rows="10" onkeypress="textCounter(this.form.tppgTxObservacao,this.form.remLen,4000)" onkeyup="textCounter(this.form.tppgTxObservacao,this.form.remLen,4000)" /> 
    </td>
  </tr>
  <tr> 
    <td width="14%"> 
      <div align="right"><bean:message key="prompt.dataInicial"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td width="13%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="85%"> <html:text property="tppgDhInicio" styleClass="text" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" /> 
          </td>
          <td width="15%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('administracaoCsCdtbTpProgramaTppgForm.tppgDhInicio')" class="principalLstParMao"></td>
        </tr>
      </table>

    </td>
    <td class="principalLabel" width="13%"> 
      <div align="right"><bean:message key="prompt.dataTermino"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
    </td>
    <td class="principalLabel" width="14%">
      <table width="96%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="85%"> <html:text property="tppgDhTermino" styleClass="text" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"  /> 
          </td>
          <td width="15%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('administracaoCsCdtbTpProgramaTppgForm.tppgDhTermino')" class="principalLstParMao" ></td>
        </tr>
      </table>

    </td>
    <td class="principalLabel" width="14%">&nbsp;</td>
    <td width="32%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="14%"> 
      <div align="right"></div>
    </td>
    <td width="13%" > </td>
    <td class="principalLabel" colspan="3">&nbsp;</td>
    <td width="32%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="14%"> 
      <div align="right"></div>
    </td>
    <td width="13%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="13%"> <html:checkbox value="true" property="tppgDhInativo"/></td>
          <td class="principalLabel" width="87%">&nbsp;<bean:message key="prompt.inativo"/></td>
        </tr>
      </table>
    </td>
    <td class="principalLabel" colspan="3">&nbsp;</td>
    <td width="32%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbTpProgramaTppgForm.tppgDsTipoPrograma.disabled= true;	
			document.administracaoCsCdtbTpProgramaTppgForm.tppgDhInativo.disabled= true;
			document.administracaoCsCdtbTpProgramaTppgForm.tppgDhInicio.disabled= true;
			document.administracaoCsCdtbTpProgramaTppgForm.tppgDhTermino.disabled= true;
			document.administracaoCsCdtbTpProgramaTppgForm.tppgTxObservacao.disabled= true;						

			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			document.administracaoCsCdtbTpProgramaTppgForm.idTppgCdTipoPrograma.disabled= false;
			document.administracaoCsCdtbTpProgramaTppgForm.idTppgCdTipoPrograma.value= '';
			document.administracaoCsCdtbTpProgramaTppgForm.idTppgCdTipoPrograma.disabled= true;
		</script>
</logic:equal>
</html>