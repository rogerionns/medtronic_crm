<%@ page language="java" import="br.com.plusoft.csi.adm.helper.MAConstantes, 
								com.iberia.helper.Constantes,
								br.com.plusoft.fw.app.Application" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
	function inicio(){
		//showError('<%=request.getAttribute("msgerro")%>');

		//try{
			//if(parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value != "0" && parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value != ""){
				//try{
					//manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;
					
					//if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].selectedIndex == -1){
					//	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = "";
					//}
					
				//}
				//catch(x){}
			//}
		//}
		//catch(x){}
		
		habilitaBotaoBuscaProd();
		
		
		try{
			//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 
			if (document.administracaoCsCdtbProdutoAssuntoPrasForm['idAsn2CdAssuntoNivel2'].length == 2){
				document.administracaoCsCdtbProdutoAssuntoPrasForm['idAsn2CdAssuntoNivel2'][1].selected = true;
				carregaLinha();
			}
		}catch(e){}		
		
		submeteForm();
	}

	function habilitaBotaoBuscaProd(){
		try{
			//if (parent.cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].length > 1){
				window.document.all.item('botaoPesqProd').disabled = false;
				window.document.all.item('botaoPesqProd').className = "geralCursoHand";
			//}else{
				//window.document.all.item('botaoPesqProd').disabled = true;
				//window.document.all.item('botaoPesqProd').className = "geralImgDisable";
			//}
		}catch(e){}	
	}

	function desabilitarBotaoBuscaProd(){
		try{
			window.document.all.item('botaoPesqProd').disabled = true;
			window.document.all.item('botaoPesqProd').className = "geralImgDisable";
		}catch(e){}	
	}
	
	function mostraCampoBuscaProd(){
		window.location.href = "AdministracaoCsCdtbProdutoAssuntoPras.do?tela=<%= MAConstantes.TELA_CMB_ASSUNTONIVEL2_PRAS %>&acao=<%=Constantes.ACAO_CONSULTAR%>";	
	}
	
	function buscarProduto(){

		if (document.administracaoCsCdtbProdutoAssuntoPrasForm['asn2DsAssuntoNivel2'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}
	
		var idAsn1CdAssuntoNivel1;
		document.administracaoCsCdtbProdutoAssuntoPrasForm.tela.value = "<%= MAConstantes.TELA_CMB_ASSUNTONIVEL2_PRAS %>";
		document.administracaoCsCdtbProdutoAssuntoPrasForm.acao.value = "<%=Constantes.ACAO_FITRAR%>";

		idAsn1CdAssuntoNivel1 = parent.document.administracaoCsCdtbProdutoAssuntoPrasForm['idAsn1CdAssuntoNivel1'].value;
		
		if (idAsn1CdAssuntoNivel1 == "")
			idAsn1CdAssuntoNivel1 = 0;
			 
		document.administracaoCsCdtbProdutoAssuntoPrasForm['idAsn1CdAssuntoNivel1'].value = idAsn1CdAssuntoNivel1;
		
		document.administracaoCsCdtbProdutoAssuntoPrasForm.submit();

	}
	
	function carregaLinha(){
	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}
	
	function submeteInformacao(){
	}
	
	function submeteForm(){
	}
	
	function verificarCancelar(evnt){
		if(evnt.keyCode == 27 && administracaoCsCdtbProdutoAssuntoPrasForm.acao.value == "<%=Constantes.ACAO_CONSULTAR%>"){
			parent.carregarCmbVariedade();
			return false;
		}
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="inicio();" onkeydown="return verificarCancelar(event);">
<html:form action="/AdministracaoCsCdtbProdutoAssuntoPras.do" styleId="administracaoCsCdtbProdutoAssuntoPrasForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idAsn1CdAssuntoNivel1" />
  
  	  <logic:notEqual name="administracaoCsCdtbProdutoAssuntoPrasForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
				<html:select property="idAsn2CdAssuntoNivel2" styleClass="principalObjForm" > 
					<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> 
					<html:options collection="csCdtbAssuntoNivel2Asn2Vector" property="idAsn2CdAssuntoNivel2" labelProperty="asn2DsAssuntoNivel2"/> 
				</html:select>                              
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
		  	</td>
	  	</tr>
	  </table>
	  
		  <logic:present name="csCdtbAssuntoNivel2Asn2Vector">
			  <logic:iterate name="csCdtbAssuntoNivel2Asn2Vector" id="csCdtbAssuntoNivel2Asn2Vector">
				  <input type="hidden" name='txtAsn2<bean:write name="csCdtbAssuntoNivel2Asn2Vector" property="idAsn2CdAssuntoNivel2"/>' value='<bean:write name="csCdtbAssuntoNivel2Asn2Vector" property="idAsn2CdAssuntoNivel2"/>'>
			  </logic:iterate>	  
		  </logic:present>
	  
	  </logic:notEqual>
	  
   	  <logic:equal name="administracaoCsCdtbProdutoAssuntoPrasForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
	  	<tr>
	  		<td width="95%">
	  			<html:text property="asn2DsAssuntoNivel2" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
		  	</td>
		</tr> 	
	  </table>
	  <script>
		//document.administracaoCsCdtbProdutoAssuntoPrasForm['idAsn2CdAssuntoNivel2'].select();
	  </script>
   	  </logic:equal>
  	
</html:form>
</body>
</html>
<logic:equal name="administracaoCsCdtbProdutoAssuntoPrasForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	<script>
		document.administracaoCsCdtbProdutoAssuntoPrasForm['asn2DsAssuntoNivel2'].focus();
	</script>
</logic:equal>