<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<%  
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>


<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsCdtbAtendpadraoAtpaForm.target = admIframe.name;
	document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value ='filtrar';
	document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbAtendpadraoAtpaForm.filtro.value = '';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsCdtbAtendpadraoAtpaForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_ATENDPADRAO_ATPA%>';
	editIframe.document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo){
	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idAtpdCdAtendpadrao.value = codigo;
	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_ATENDPADRAO_ATPA%>';
	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.target = editIframe.name;
	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){
	
	if(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_ATENDPADRAO_ATPA%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_ATENDPADRAO_ATPA%>'){
		
		if (trim(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpdDsAtendpadrao.value).length > 0){
			
			if (
			(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpaInTpatendimento[0].checked && 
			 tab.ifrmCmbProdutoManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn1CdAssuntonivel1.value == 0) || 
			(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpaInTpatendimento[1].checked &&
			 tab.ifrmCmbProdutoInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.value == 0)){
				
				alert("<bean:message key="prompt.O_campo_produto_e_obrigatorio"/>.");
				if (tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpaInTpatendimento[0].checked){
					tab.ifrmCmbProdutoManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn1CdAssuntonivel1.focus();}
				else{
					tab.ifrmCmbProdutoInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.focus();}
					
			}else{
			
			/*	if(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpaInTpatendimento[0].checked && tab.ifrmCmbResponsavel.document.administracaoCsCdtbAtendpadraoAtpaForm.idFuncCdResponsavel.value <=0){
					alert("<bean:message key="prompt.O_campo_responsavel_e_obrigatorio"/>");
					return false;
				}*/

				if(tab.ifrmCmbAtendente.document.administracaoCsCdtbAtendpadraoAtpaForm.idFuncCdAtendente.value <=0){
					alert("<bean:message key="prompt.atendente_obrigatorio"/>");
					return false;
				}
                //Corre��o Action Center
				if(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpaInTpatendimento[1].checked && tab.ifrmCmbTipo.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpinCdTipoinformacao.value == 0){
					alert("<bean:message key="prompt.Por_favor_selecione_um_tipo_de_informacao"/>.");
					tab.ifrmCmbTipo.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpinCdTipoinformacao.focus();
					return false;
				}

                //Chamado 105770 - Victor Godinho 06/01/2016
				if (trim(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpaTxTexto.value).length <= 0){
					alert("<bean:message key="prompt.Por_favor_informe_o_texto_atendimento"/>.");
					tab.ifrmCmbTipo.document.administracaoCsCdtbAtendpadraoAtpaForm.atpaTxTexto.focus();
					return false;
				}

				if(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpaInTpatendimento[1].checked && tab.ifrmCmbTopico.document.administracaoCsCdtbAtendpadraoAtpaForm.idToinCdTopicoinformacao.value == 0){
					alert("<bean:message key="prompt.Por_favor_selecione_um_topico_de_informacao"/>.");
					tab.ifrmCmbTopico.document.administracaoCsCdtbAtendpadraoAtpaForm.idToinCdTopicoinformacao.focus();
					return false;
				}

				if(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpaInTpatendimento[0].checked && tab.ifrmCmbTipoManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpmaCdTpmanifestacao.value == 0){
					alert("<bean:message key="prompt.O_campo_tipo_de_manifestacao_e_obrigatorio"/>.");
					tab.ifrmCmbTipoManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpmaCdTpmanifestacao.focus();
				}
				else{
					
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_ATENDPADRAO_ATPA%>';
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.target = admIframe.name;
					disableEnable(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idAtpdCdAtendpadrao, false);
				
					// copiando os valores dos combos para o formulario principal
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idColoCdComolocalizou.value = tab.ifrmCmbComoLocalizou.document.administracaoCsCdtbAtendpadraoAtpaForm.idColoCdComolocalizou.value;
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idMidiCdMidia.value = tab.ifrmCmbMidia.document.administracaoCsCdtbAtendpadraoAtpaForm.idMidiCdMidia.value;
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idFocoCdFormacontato.value = tab.ifrmCmbFormaContato.document.administracaoCsCdtbAtendpadraoAtpaForm.idFocoCdFormacontato.value;
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idEsanCdEstadoanimo.value = tab.ifrmCmbEstadoAnimo.document.administracaoCsCdtbAtendpadraoAtpaForm.idEsanCdEstadoanimo.value;
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpreCdTiporetorno.value = tab.ifrmCmbTipoRetorno.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpreCdTiporetorno.value;
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idFuncCdAtendente.value = tab.ifrmCmbAtendente.document.administracaoCsCdtbAtendpadraoAtpaForm.idFuncCdAtendente.value;
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idMatpCdManiftipo.value = tab.ifrmCmbManifestacao.document.administracaoCsCdtbAtendpadraoAtpaForm.idMatpCdManiftipo.value;
	
					<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idSugrCdSupergrupo.value = tab.ifrmCmbSupergrupo.document.administracaoCsCdtbAtendpadraoAtpaForm.idSugrCdSupergrupo.value;
					<% } %>
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idGrmaCdGrupomanifestacao.value = tab.ifrmCmbGrupo.document.administracaoCsCdtbAtendpadraoAtpaForm.idGrmaCdGrupomanifestacao.value;
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpmaCdTpmanifestacao.value = tab.ifrmCmbTipoManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idTpmaCdTpmanifestacao.value;
				//	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idAreaCdArea.value = tab.ifrmCmbArea.document.administracaoCsCdtbAtendpadraoAtpaForm.idAreaCdArea.value;
				//	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idFuncCdResponsavel.value = tab.ifrmCmbResponsavel.document.administracaoCsCdtbAtendpadraoAtpaForm.idFuncCdResponsavel.value;			
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idStmaCdStatusmanif.value = tab.ifrmCmbStatusManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idStmaCdStatusmanif.value;
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idCompCdSequencial.value = tab.ifrmTextoInformacao.document.administracaoCsCdtbAtendpadraoAtpaForm.idCompCdSequencial.value;
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idGrsaCdGrausatisfacao.value = tab.ifrmCmbGrauSatisf.document.administracaoCsCdtbAtendpadraoAtpaForm.idGrsaCdGrausatisfacao.value;
					// para pegar o produto correto (manifestacao ou informacao
					if (tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpaInTpatendimento[0].checked) { // Manifestacao
					
						tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.value = tab.ifrmCmbCsCdtbLinhaLinh.document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.value;
						
						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
							tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.value = tab.ifrmCmbProdutoManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn1CdAssuntonivel1.value +"@"+ tab.ifrmCmbVariedadeManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.value;
						<%}else{%>
							tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.value = tab.ifrmCmbProdutoManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn1CdAssuntonivel1.value +"@1";
						<%}%>
							
						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
							tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.value = tab.ifrmCmbVariedadeManif.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.value;
						<%}%>
					}
					else { // Informacao
						
						tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.value = tab.ifrmCmbLinhaInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.idLinhCdLinha.value;
						
						tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.value = tab.ifrmCmbProdutoInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsnCdAssuntoNivel.value;
						
						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
							tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.value = tab.ifrmCmbVariedadeInfo.document.administracaoCsCdtbAtendpadraoAtpaForm.idAsn2CdAssuntonivel2.value;
						<%}%>
					}
				
					tab.document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
					disableEnable(tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idAtpdCdAtendpadrao, true);
					cancel();
				}
			}
			
		} else {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbAtendpadraoAtpaForm.atpdDsAtendpadrao.focus();
		}
	}
}

function submeteExcluir(codigo) {
	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.idAtpdCdAtendpadrao.value = codigo;
	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_ATENDPADRAO_ATPA%>';
	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.target = editIframe.name;
	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_ATENDPADRAO_ATPA%>';
		editIframe.document.administracaoCsCdtbAtendpadraoAtpaForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbAtendpadraoAtpaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbAtendpadraoAtpaForm.idAtpdCdAtendpadrao, false);
		editIframe.document.administracaoCsCdtbAtendpadraoAtpaForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbAtendpadraoAtpaForm.idAtpdCdAtendpadrao, true);
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbAtendpadraoAtpa.do?tela=editCsCdtbAtendpadraoAtpa&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbAtendpadraoAtpa.do?tela=editCsCdtbAtendpadraoAtpa&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
     obj.display=v; }
}

function  Reset(){
	document.administracaoCsCdtbAtendpadraoAtpaForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_ATENDPADRAO_ATPA%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalResultAnalise');
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbAtendpadraoAtpaForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_ATENDPADRAO_ATPA%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoResultAnalise');	
			setaIdiomaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbAtendpadraoAtpaForm"	action="/AdministracaoCsCdtbAtendpadraoAtpa.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="500" valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar" /><!-- ## --></td>
									<td class="principalPstQuadroLinkNormalResultAnalise" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.atendimentoPadrao" /><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="480"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="width: 100%; height: 225px;display: none"> 
                    <table width="99%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="450"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsCdtbAtendpadraoAtpa.do?tela=editCsCdtbAtendpadraoAtpa&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="width: 97%; height: 199px;display: block"> 
                    		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalLabel" width="15%"><bean:message key="prompt.descricao"/></td>
									<td class="principalLabel" width="65%">&nbsp;</td>
								</tr>

								<tr>
									<td width="65%">
									<table border="0" cellspacing="0" cellpadding="0">
									<tr>
									<td width="25%">
									<html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/>
									</td>
									<td width="05%">
									&nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()">
									</td>
									</tr>	
									</table>
									</td>
								</tr>
								<tr>
									<td class="principalLabel" width="15%">&nbsp;</td>
									<td class="principalLabel" width="64%">&nbsp;</td>
								</tr>
							</table>
							<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="principalLstCab" width="18%">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalLstCab" width="25%">
													&nbsp;
												</td>
												<td class="principalLstCab" width="75%">
													&nbsp;&nbsp;<bean:message key="prompt.codigo"/>
												</td>
											</tr>
										</table>
									</td>
									<td class="principalLstCab" width="61%"><bean:message key="prompt.descricao"/></td>
									<td class="principalLstCab" align="left" width="20%">&nbsp;&nbsp;<bean:message key="prompt.inativo"/> </td>
								</tr>
								
								<tr valign="top">
									
                        <td colspan="3" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsCdtbAtendpadraoAtpa.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                </td>
							<td width="4" height="480px"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="480px"></td>
						</tr>
						<tr>
							<td width="100%" height="8" valign="top"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="8" valign="top"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="545px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="545px"></td>
		</tr>
		<tr>
			<td width="100%" valign="top"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4" valign="top"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_ATENDIMENTOPADRAO_INCLUSAO_CHAVE%>', document.administracaoCsCdtbAtendpadraoAtpaForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_ATENDIMENTOPADRAO_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAMADO_ATENDIMENTOPADRAO_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbAtendpadraoAtpaForm.imgGravar.disabled=true;
			document.administracaoCsCdtbAtendpadraoAtpaForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbAtendpadraoAtpaForm.imgGravar.title='';
	}
	
	desabilitaListaEmpresas();

	setaArquivoXml("CS_ASTB_IDIOMAATENDPAD_IDAP.xml");
	habilitaTelaIdioma();
	
</script>


</body>
</html>
