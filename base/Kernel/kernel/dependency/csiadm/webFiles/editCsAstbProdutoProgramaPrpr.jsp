<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>

function MontaLista(){
	lstArqCarga.location.href= "AdministracaoCsAstbProdutoProgramaPrpr.do?tela=administracaoLstCsAstbProdutoProgramaPrpr&acao=<%=Constantes.ACAO_VISUALIZAR%>&idTppgCdTipoPrograma=" + document.administracaoCsAstbProdutoProgramaPrprForm.idTppgCdTipoPrograma.value;
}


</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista()">

<html:form styleId="administracaoCsAstbProdutoProgramaPrprForm" action="/AdministracaoCsAstbProdutoProgramaPrpr.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="idAsn1CdAssuntoNivel1"/>
	<html:hidden property="idAsn2CdAssuntoNivel2"/>	

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.tipoPrograma"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
    	<html:select property="idTppgCdTipoPrograma" styleClass="principalObjForm" onchange="MontaLista()"> 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbTpProgramaTppgVector" property="idTppgCdTipoPrograma" labelProperty="tppgDsTipoPrograma"/> 
      </html:select> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel"><bean:message key="prompt.produtoAssunto"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idAsnCdAssuntoNivel" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/> 
      </html:select> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td class="principalLabel" width="28%"> </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="220"> 
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="6%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="36%"> <bean:message key="prompt.produtoAssunto"/></td>
          <td class="principalLstCab" width="33%">&nbsp;</td>
          <td class="principalLstCab" width="11%">&nbsp;</td>
          <td class="principalLstCab" width="14%">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsAstbProdutoProgramaPrprForm.idTppgCdTipoPrograma.disabled= true;
			document.administracaoCsAstbProdutoProgramaPrprForm.idAsnCdAssuntoNivel.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			document.administracaoCsAstbProdutoProgramaPrprForm.idTppgCdTipoPrograma.disabled= false;
			document.administracaoCsAstbProdutoProgramaPrprForm.idAsnCdAssuntoNivel.disabled= false;
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			document.administracaoCsAstbProdutoProgramaPrprForm.idTppgCdTipoPrograma.disabled= false;
			document.administracaoCsAstbProdutoProgramaPrprForm.idAsnCdAssuntoNivel.disabled= false;
		</script>
</logic:equal>
</html>