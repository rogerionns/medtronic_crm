<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

 function desabilitaCampos(){
 	administracaoCsAstbMateriaProdutoMaprForm.idMateCdMateria.disabled= true;
	administracaoCsAstbMateriaProdutoMaprForm.idLinhCdLinha.disabled= true;	
	setTimeout('cmbProduto.administracaoCsAstbMateriaProdutoMaprForm.idPrasCdProdutoAssunto.disabled= true;',500);
	setTimeout('cmbVariedade.administracaoCsAstbMateriaProdutoMaprForm.idAsn2CdAssuntoNivel2.disabled= true;',500);
}	

function carregaProduto() {
	administracaoCsAstbMateriaProdutoMaprForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	administracaoCsAstbMateriaProdutoMaprForm.tela.value = '<%=MAConstantes.TELA_CMB_CS_CDTB_PRODUTOASSUNTO_PRAS%>';
	administracaoCsAstbMateriaProdutoMaprForm.target = cmbProduto.name;
	administracaoCsAstbMateriaProdutoMaprForm.submit();
}

function MontaLista(){
	lstArqCarga.location.href= "AdministracaoCsAstbMateriaProdutoMapr.do?tela=administracaoLstCsAstbMateriaProdutoMapr&acao=<%=Constantes.ACAO_VISUALIZAR%>&idMateCdMateria=" + administracaoCsAstbMateriaProdutoMaprForm.idMateCdMateria.value;
}

</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista();">
<html:form styleId="administracaoCsAstbMateriaProdutoMaprForm" action="/AdministracaoCsAstbMateriaProdutoMapr.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="idPrasCdProdutoAssunto" />
	<html:hidden property="idAsn1CdAssuntoNivel1" />
	<html:hidden property="idAsn2CdAssuntoNivel2" />
	<br>

<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.materia" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      <html:select property="idMateCdMateria" styleClass="principalObjForm" onchange="MontaLista()">
        <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
        <html:options collection="csCdtbMateriaMateVector" property="idMateCdMateria" labelProperty="mateDsMateria" />
      </html:select>
    </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.linha" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      <html:select property="idLinhCdLinha" onchange="carregaProduto()" styleClass="principalObjForm" > 
        <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option>
        <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/> 
      </html:select> </td>
    <td width="18%">&nbsp;</td>
  </tr>
 
  	<tr> 
	    <td width="24%" align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel1", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	    <td colspan="2">
	      <iframe name="cmbProduto" src="AdministracaoCsAstbMateriaProdutoMapr.do?acao=<%=Constantes.ACAO_RECUSAR%>&tela=<%=MAConstantes.TELA_CMB_CS_CDTB_PRODUTOASSUNTO_PRAS%>" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	    </td>
	    <td width="18%">&nbsp;</td>
  	</tr>
  	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
	  	<tr> 
		    <td width="24%" align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel2", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    <td colspan="2">
		      <iframe name="cmbVariedade" src="AdministracaoCsAstbMateriaProdutoMapr.do?acao=<%=Constantes.ACAO_RECUSAR%>&tela=<%=MAConstantes.TELA_CMB_CS_CDTB_ASSUNTONIVEL2_ASN2_MAPR%>" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
		    </td>
		    <td width="18%">&nbsp;</td>
	  	</tr>	  	
  	<%}else{%>
	  	<tr style="display:none"> 
		    <td width="24%" align="right" class="principalLabel"><%= getMessage("prompt.assuntoNivel2", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		    <td colspan="2">
		      <iframe name="cmbVariedade" src="AdministracaoCsAstbMateriaProdutoMapr.do?acao=<%=Constantes.ACAO_RECUSAR%>&tela=<%=MAConstantes.TELA_CMB_CS_CDTB_ASSUNTONIVEL2_ASN2_MAPR%>" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
		    </td>
		    <td width="18%">&nbsp;</td>
	  	</tr>
  	<%}%>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="220">
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="4%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="96%">&nbsp;<bean:message key="prompt.produto" /></td>
        </tr>
        <tr valign="top"> 
          <td colspan="2" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			administracaoCsAstbMateriaProdutoMaprForm.idMateCdMateria.disabled= true;	
			administracaoCsAstbMateriaProdutoMaprForm.idLinhCdLinha.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_MATERIAPRODUTO_INCLUSAO_CHAVE%>', parent.administracaoCsAstbMateriaProdutoMaprForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_MATERIAPRODUTO_INCLUSAO_CHAVE%>')){
				desabilitaCampos();
			}		
			/*administracaoCsAstbMateriaProdutoMaprForm.idMateCdMateria.disabled= false;
			administracaoCsAstbMateriaProdutoMaprForm.idMateCdMateria.value= '';
			administracaoCsAstbMateriaProdutoMaprForm.idMateCdMateria.disabled= true;*/
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_MATERIAPRODUTO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PRODUCAOCIENTIFICA_MATERIAPRODUTO_ALTERACAO_CHAVE%>', parent.administracaoCsAstbMateriaProdutoMaprForm.imgGravar);	
				desabilitaCampos();
			}
		</script>
</logic:equal>
</html>
<script>
	try{administracaoCsAstbMateriaProdutoMaprForm.idMateCdMateria.focus();}
	catch(e){}	
</script>