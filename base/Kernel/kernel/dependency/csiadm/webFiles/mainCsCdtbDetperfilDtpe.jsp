<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsCdtbDetperfilDtpeForm.target = admIframe.name;
	document.administracaoCsCdtbDetperfilDtpeForm.acao.value ='filtrar';
	document.administracaoCsCdtbDetperfilDtpeForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbDetperfilDtpeForm.filtro.value = '';
}

function submeteFormIncluir() {
	
	editIframe.document.administracaoCsCdtbDetperfilDtpeForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbDetperfilDtpeForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_DETPERFIL_DTPE%>';
	editIframe.document.administracaoCsCdtbDetperfilDtpeForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbDetperfilDtpeForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo){
	tab.document.administracaoCsCdtbDetperfilDtpeForm.idDtpeCdDetperfil.value = codigo;
	tab.document.administracaoCsCdtbDetperfilDtpeForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_DETPERFIL_DTPE%>';
	tab.document.administracaoCsCdtbDetperfilDtpeForm.target = editIframe.name;
	tab.document.administracaoCsCdtbDetperfilDtpeForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbDetperfilDtpeForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){

	if(tab.document.administracaoCsCdtbDetperfilDtpeForm.tela.value == '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_DETPERFIL_DTPE%>'){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else if(tab.document.administracaoCsCdtbDetperfilDtpeForm.tela.value == '<%=MAConstantes.TELA_EDIT_CS_CDTB_DETPERFIL_DTPE%>'){
		if (tab.document.administracaoCsCdtbDetperfilDtpeForm.idPerfCdPerfil.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_um_perfil"/>");
			tab.document.administracaoCsCdtbDetperfilDtpeForm.idPerfCdPerfil.focus();
			return false;
		}
		if ((tab.document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrRespostas[0].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrRespostas[1].checked == false ))
		{
			alert("<bean:message key="prompt.Por_favor_selecione_o_nivel_de_resposta"/>.");
			return false;
		}
		if (tab.document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrSequencia.value.search(/[^\d]/) != -1) {
			alert ("<bean:message key="prompt.Digite_apenas_numeros_neste_campo"/>.");
			tab.document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrSequencia.focus();
			return false;
		}
		
		tab.document.administracaoCsCdtbDetperfilDtpeForm.dtpeDsDetperfil.value = trim(tab.document.administracaoCsCdtbDetperfilDtpeForm.dtpeDsDetperfil.value);
		if (tab.document.administracaoCsCdtbDetperfilDtpeForm.dtpeDsDetperfil.value == "") {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbDetperfilDtpeForm.dtpeDsDetperfil.focus();
			return false;
		}
		if (tab.document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrRespostas[0].checked == true){
			//////////////////// PRIMEIRO CAMPO /////////////////////////		
			if (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreDsTitulo'].value == ""){
				alert("<bean:message key="prompt.Por_favor_digite_o_titulo_para_o_campo_da_primeira_resposta"/>.");
				tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreDsTitulo'].focus();
				return false;
			}
			if ((tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].value == "") && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.idTabeCdTabela'].value == "")){
				alert("<bean:message key="prompt.Por_favor_digite_a_quantidade_maxima_de_carateres_para_o_campo_da_primeira_resposta"/>.");
				tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].focus();
				return false;
			}
			if (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].value.search(/[^\d]/) != -1) {
				alert ("<bean:message key="prompt.Digite_apenas_numeros_neste_campo"/>.");
				tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].focus();
				return false;
			}
			if((tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInObrigatorio'][0].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInObrigatorio'][1].checked == false)){
				alert("<bean:message key="prompt.Por_favor_selecione_se_a_primeira_resposta_sera_obrigatoria"/>.");
				return false;
			}
			if((tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][0].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][1].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][2].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.idTabeCdTabela'].value == "")){
				alert("<bean:message key="prompt.Por_favor_selecione_qual_o_tipo_de_resposta_do_primeiro_campo"/>.");
				return false;
			}
		}
		if (tab.document.administracaoCsCdtbDetperfilDtpeForm.dtpeNrRespostas[1].checked == true){
			//////////////////// PRIMEIRO CAMPO /////////////////////////		
			if (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreDsTitulo'].value == ""){
				alert("<bean:message key="prompt.Por_favor_digite_o_titulo_para_o_campo_da_primeira_resposta"/>.");
				tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreDsTitulo'].focus();
				return false;
			}
			if ((tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].value == "") && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.idTabeCdTabela'].value == "")){
				alert("<bean:message key="prompt.Por_favor_digite_a_quantidade_maxima_de_carateres_para_o_campo_da_primeira_resposta"/>.");
				tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].focus();
				return false;
			}
			if (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].value.search(/[^\d]/) != -1) {
				alert ("<bean:message key="prompt.Digite_apenas_numeros_neste_campo"/>.");
				tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].focus();
				return false;
			}
			if((tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInObrigatorio'][0].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInObrigatorio'][1].checked == false)){
				alert("<bean:message key="prompt.Por_favor_selecione_se_a_primeira_resposta_sera_obrigatoria"/>.");
				return false;
			}
			if((tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][0].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][1].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreInTpresposta'][2].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.idTabeCdTabela'].value == "")){
				alert("<bean:message key="prompt.Por_favor_selecione_qual_o_tipo_de_resposta_do_primeiro_campo"/>.");
				return false;
			}		
			//////////////////// SEGUNDO CAMPO /////////////////////////		
			if (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreDsTitulo'].value == ""){
				alert("<bean:message key="prompt.Por_favor_digite_o_titulo_para_o_campo_da_segunda_resposta"/>.");
				tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreDsTitulo'].focus();
				return false;
			}
			if (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].value == ""){
				alert("<bean:message key="prompt.Por_favor_digite_a_quantidade_maxima_de_carateres_para_o_campo_da_segunda_resposta"/>.");
				tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].focus();
				return false;
			}
			if (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].value.search(/[^\d]/) != -1) {
				alert ("<bean:message key="prompt.Digite_apenas_numeros_neste_campo"/>.");
				tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].focus();
				return false;
			}
			if((tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInObrigatorio'][0].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInObrigatorio'][1].checked == false)){
				alert("<bean:message key="prompt.Por_favor_selecione_se_a_segunda_resposta_sera_obrigatoria"/>.");
				return false;
			}
			if((tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInTpresposta'][0].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInTpresposta'][1].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreInTpresposta'][2].checked == false) && (tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.idTabeCdTabela'].value == "")){
				alert("<bean:message key="prompt.Por_favor_selecione_qual_o_tipo_de_resposta_do_segundo_campo"/>.");
				return false;
			}
		}
		
		//habilita os campos para conseguir gravar
		tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbPrimeiraRespTpreVo.tpreNrTamanho'].disabled=false;
		tab.document.administracaoCsCdtbDetperfilDtpeForm['csCdtbSegundaRespTpreVo.tpreNrTamanho'].disabled=false;
		
		tab.document.administracaoCsCdtbDetperfilDtpeForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_DETPERFIL_DTPE%>';
		tab.document.administracaoCsCdtbDetperfilDtpeForm.target = admIframe.name;
		disableEnable(tab.document.administracaoCsCdtbDetperfilDtpeForm.idDtpeCdDetperfil, false);
		tab.document.administracaoCsCdtbDetperfilDtpeForm.submit();
		disableEnable(tab.document.administracaoCsCdtbDetperfilDtpeForm.idDtpeCdDetperfil, true);
		cancel();

	}
}

function submeteExcluir(codigo) {
	
	tab.document.administracaoCsCdtbDetperfilDtpeForm.idDtpeCdDetperfil.value = codigo;
	tab.document.administracaoCsCdtbDetperfilDtpeForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_DETPERFIL_DTPE%>';
	tab.document.administracaoCsCdtbDetperfilDtpeForm.target = editIframe.name;
	tab.document.administracaoCsCdtbDetperfilDtpeForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbDetperfilDtpeForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbDetperfilDtpeForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_DETPERFIL_DTPE%>';
		editIframe.document.administracaoCsCdtbDetperfilDtpeForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbDetperfilDtpeForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		disableEnable(editIframe.document.administracaoCsCdtbDetperfilDtpeForm.idDtpeCdDetperfil, false);
		editIframe.document.administracaoCsCdtbDetperfilDtpeForm.submit();
		disableEnable(editIframe.document.administracaoCsCdtbDetperfilDtpeForm.idDtpeCdDetperfil, true);
		AtivarPasta(admIframe);
		MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		editIframe.location = 'AdministracaoCsCdtbDetperfilDtpe.do?tela=editCsCdtbDetperfilDtpe&acao=incluir';
	}else{
		cancel();	
	}
}

function cancel(){

	editIframe.location = 'AdministracaoCsCdtbDetperfilDtpe.do?tela=editCsCdtbDetperfilDtpe&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsCdtbDetperfilDtpeForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbDetperfilDtpeForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_DETPERFIL_DTPE%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbDetperfilDtpeForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_DETPERFIL_DTPE%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			setaIdiomaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbDetperfilDtpeForm"	action="/AdministracaoCsCdtbDetperfilDtpe.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="principalPstQuadroLinkVazio">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadroLinkSelecionado"
										id="tdDestinatario" name="tdDestinatario"
										onClick="AtivarPasta(admIframe);">
									<bean:message key="prompt.procurar"/><!-- ## --></td>
									<td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao"
										name="tdManifestacao"
										onClick="AtivarPasta(editIframe);">
									<bean:message key="prompt.detalhePerfil"/><!-- ## --></td>

								</tr>
							</table>
							</td>
							<td width="4"><img
								src="webFiles/images/separadores/pxTranp.gif" width="1"
								height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="position: absolute; width: 97%; height: 225px; z-index: 6; visibility: hidden"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsCdtbDetperfilDtpe.do?tela=editCsCdtbDetperfilDtpe&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="position: absolute; width: 97%; height: 199px; z-index: 2; visibility: visible"> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLabel" width="7%" colspan="2"><bean:message key="prompt.descricaoDetalhePerfil"/></td>
                      </tr>
                      <tr> 
                        <td width="65%"> 
                          <table border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="25%"> <html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();"/> 
                              </td>
                              <td width="05%"> &nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()"> 
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" width="7%">&nbsp;</td>
                        <td class="principalLabel" colspan="2">&nbsp;</td>
                      </tr>
                   </table>
                   <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLstCab" width="18%">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalLstCab" width="25%">
										&nbsp;
									</td>
									<td class="principalLstCab" width="75%">
										&nbsp;&nbsp;<bean:message key="prompt.codigo"/>
									</td>
								</tr>
							</table>
						</td>
                        <td class="principalLstCab" width="38%"><bean:message key="prompt.descricao"/></td>
                        <td class="principalLstCab" width="34%"><bean:message key="prompt.perfil"/></td>
                        <td class="principalLstCab" align="left" width="19%"><bean:message key="prompt.inativo"/> 
                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="4" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsCdtbDetperfilDtpe.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
                      </tr>
                    </table>
							</div>

							
                </td>
							<td width="4" height="400"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir"	width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="540px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="540px"></td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_DETALHEDOPERFIL_INCLUSAO_CHAVE%>', document.administracaoCsCdtbDetperfilDtpeForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_DETALHEDOPERFIL_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_PERFIL_DETALHEDOPERFIL_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbDetperfilDtpeForm.imgGravar.disabled=true;
			document.administracaoCsCdtbDetperfilDtpeForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbDetperfilDtpeForm.imgGravar.title='';
	}
	
	desabilitaListaEmpresas();
	
	setaArquivoXml("CS_ASTB_IDIOMADETPERF_IDDP.xml");
	habilitaTelaIdioma();
	
</script>


</body>
</html>
