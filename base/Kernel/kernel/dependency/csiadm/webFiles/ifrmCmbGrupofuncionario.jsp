<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
function atualizaTreeView() {
	parent.parent.parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.administracaoPermissionamentoForm.acao.value = '<%= Constantes.ACAO_CONSULTAR %>'
	document.administracaoPermissionamentoForm.tela.value = '<%= MAConstantes.TELA_IFRM_TREE_VIEW %>';
	document.administracaoPermissionamentoForm.target = parent.ifrmTreeView.name;
	document.administracaoPermissionamentoForm.submit();
	limpaComboArea();
}	

function limpaComboArea() {
	document.administracaoPermissionamentoForm.acao.value = '<%= Constantes.ACAO_VISUALIZAR %>'
	document.administracaoPermissionamentoForm.tela.value = '<%= MAConstantes.TELA_IFRM_CMB_PERMAREA %>';
	document.administracaoPermissionamentoForm.target = parent.ifrmCmbPermArea.name;
	document.administracaoPermissionamentoForm.submit();
	limpaComboFuncionario();
}

function limpaComboFuncionario() {
	document.administracaoPermissionamentoForm.acao.value = '<%= Constantes.ACAO_VISUALIZAR %>'
	document.administracaoPermissionamentoForm.tela.value = '<%= MAConstantes.TELA_IFRM_CMB_PERMFUNC %>';
	document.administracaoPermissionamentoForm.target = parent.ifrmCmbPermFunc.name;
	document.administracaoPermissionamentoForm.submit();
}

function inicio(){
	if(!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_EMPRESA_PERMISSIONAMENTO_GRUPO_CHAVE%>')){
		document.administracaoPermissionamentoForm["csAstbPermissionamentoPetoVo.csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].disabled = true;
	}else{
		document.administracaoPermissionamentoForm["csAstbPermissionamentoPetoVo.csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm"].disabled = false;
	}
}
</script>

</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form action="/AdministracaoPermissionamento.do" styleId="administracaoPermissionamentoForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
		
	<html:select property="csAstbPermissionamentoPetoVo.csCdtbGrupofuncpermGfpeVo.idGfpeCdGrupofuncoerm" styleClass="principalObjForm" onchange="atualizaTreeView();">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		<html:options collection="grupoFuncVector" property="idGfpeCdGrupofuncoerm" labelProperty="gfpeDsGrupofuncperm"/>
	</html:select>
</html:form>
</body>
</html>
