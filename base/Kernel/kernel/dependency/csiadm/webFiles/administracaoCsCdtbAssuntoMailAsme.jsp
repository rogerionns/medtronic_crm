<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">
</script>
</head>
<!--Chamado: 98441 - 02/01/2015 - Leonardo M. Facchini-->
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');resizeContentDiv();" onresize="resizeContentDiv();">
<html:form styleId="editCsCdtbAssuntoMailAsmeForm" action="/AdministracaoCsCdtbAssuntoMailAsme.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="erro"/>
	<html:hidden property="topicoId"/>
	<html:hidden property="idAsmeCdAssuntoMail"/>
	
<script>var possuiRegistros=false;</script>
<div style="width: 100%;overflow: auto;height: 100%;">
<div id="contentdiv" style="float: left;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tbody>
  <logic:iterate id="ccttrtVector" name="CsCdtbAssuntoMailAsmeVector">
  <script>possuiRegistros=true;</script>
  <tr> 
    <td width="3%" class="principalLstPar">
      <img src="webFiles/images/botoes/lixeira18x18.gif" name="lixeira" width="18"	height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="parent.clearError();parent.submeteExcluir('<bean:write name="ccttrtVector" property="idAsmeCdAssuntoMail" />')">
    </td>
    <td width="3%" class="principalLstParMao"> 
    	&nbsp;<img class="geralCursoHand" src="webFiles/images/botoes/GloboAuOn.gif" title="<bean:message key="prompt.idiomas"/>" width="18" height="18" onClick="abrirModalIdioma('CS_ASTB_IDIOMAASSEMAIL_IDAE.xml','<bean:write name="ccttrtVector" property="idAsmeCdAssuntoMail" />')">
    </td>
    <td class="principalLstParMao" width="10%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idAsmeCdAssuntoMail" />')">
      <bean:write name="ccttrtVector" property="idAsmeCdAssuntoMail" />&nbsp;
    </td>
    <td class="principalLstParMao" align="left" width="64%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idAsmeCdAssuntoMail" />')">
      <bean:write name="ccttrtVector" property="asmeDsAssuntoMail" />&nbsp;
	</td>
	<td class="principalLstParMao" align="left" width="20%" onclick="parent.clearError();parent.submeteFormEdit('<bean:write name="ccttrtVector" property="idAsmeCdAssuntoMail" />')">
      <bean:write name="ccttrtVector" property="asmeDhInativo" />&nbsp;
	</td>
  </tr>
  </logic:iterate>
  <tr id="nenhumRegistro" style="display:none"><td height="260" width="800" align="center" class="principalLstPar" colspan="5"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
  </tbody>
</table>
</div>
</div>
<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

<script>
//Chamado: 98441 - 02/01/2015 - Leonardo M. Facchini
function resizeContentDiv(){
	var parentNodeOffsetWidth = document.getElementById('contentdiv').parentNode.offsetWidth;
	document.getElementById('contentdiv').style.width = parentNodeOffsetWidth - (parentNodeOffsetWidth * 0.02);
	document.getElementById('contentdiv').parentNode.style.height = document.getElementById('contentdiv').parentNode.parentNode.parentNode.offsetHeight;
}
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_GERENCIADORDEEMAIL_ASSUNTOEMAIL_EXCLUSAO_CHAVE%>', editCsCdtbAssuntoMailAsmeForm.lixeira);	
		
	if(divErro.innerHTML == null ){
		
	}else{
		parent.printError(divErro.innerHTML);
	}
</script>
</html:form>
</body>
</html>