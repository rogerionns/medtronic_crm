<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script>

function MontaLista(){
	lstArqCarga.location.href= "AdministracaoCsCdtbClassificacaoClas.do?tela=administracaoLstCsCdtbClassificacaoClas&acao=<%=Constantes.ACAO_VISUALIZAR%>&idTpclCdTpClassificacao=" + document.administracaoCsCdtbClassificacaoClasForm.idTpclCdTpClassificacao.value;
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(administracaoCsCdtbClassificacaoClasForm.idClasCdClassificacao.value);
}
	
</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista();inicio();">

<html:form styleId="administracaoCsCdtbClassificacaoClasForm" action="/AdministracaoCsCdtbClassificacaoClas.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<br>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.codigo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="10%"> <html:text property="idClasCdClassificacao" styleClass="text" readonly="true" /> 
    <script>document.all('idClasCdClassificacao').value = (document.all('idClasCdClassificacao').value=='0'?'':document.all('idClasCdClassificacao').value);</script>
    </td>
    <td width="48%">&nbsp;</td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.tipoClassificacao" />
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idTpclCdTpClassificacao" onchange="MontaLista()" styleClass="principalObjForm" > 
      <html:option value=""><bean:message key="prompt.selecione_uma_opcao" /></html:option> <html:options collection="csCdtbTpClassificacaoTpclVector" property="idTpclCdTpClassificacao" labelProperty="tpclDsTpClassificacao"/> 
      </html:select> </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.descricao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="clasDsClassificacao" styleClass="text" maxlength="60" /> 
    </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><div id="divInPess" style="poisition:absolute;visibility:hidden"><html:checkbox property="clasInPessoa" /> <bean:message key="prompt.pessoa" /></div></td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td class="principalLabel" width="48%"> 
      <table width="113%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="83%"> <html:checkbox value="true" property="clasDhInativo"/> 
          </td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo" /> </td>
        </tr>
      </table>
    </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="220">
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="3%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="10%">&nbsp;<bean:message key="prompt.codigo" /></td>
          <td class="principalLstCab" width="70%">&nbsp;<bean:message key="prompt.classificacao" /></td>
          <td class="principalLstCab" width="17%">&nbsp;<bean:message key="prompt.inativo" /></td>
        </tr>
        <tr valign="top"> 
          <td colspan="4" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			/*
			document.administracaoCsCdtbClassificacaoClasForm.idTpclCdTpClassificacao.disabled= true;	
			document.administracaoCsCdtbClassificacaoClasForm.clasDsClassificacao.disabled= true;	
			document.administracaoCsCdtbClassificacaoClasForm.clasDhInativo.disabled= true;
			*/
			document.administracaoCsCdtbClassificacaoClasForm.idTpclCdTpClassificacao.disabled= true;
			document.administracaoCsCdtbClassificacaoClasForm.clasDsClassificacao.disabled= true;	
			document.administracaoCsCdtbClassificacaoClasForm.clasInPessoa.disabled= true;
			document.administracaoCsCdtbClassificacaoClasForm.clasDhInativo.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>/*
			document.administracaoCsCdtbClassificacaoClasForm.idTpclCdTpClassificacao.disabled= false;	
			document.administracaoCsCdtbClassificacaoClasForm.idClasCdClassificacao.disabled= false;
			document.administracaoCsCdtbClassificacaoClasForm.idClasCdClassificacao.value= '';
			document.administracaoCsCdtbClassificacaoClasForm.idClasCdClassificacao.disabled= true;
			*/
		</script>
</logic:equal>
</html>