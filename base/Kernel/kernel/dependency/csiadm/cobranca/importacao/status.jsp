<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html:html>
<head>
	
	<title>CRM Plusoft - Status Importa��o</title>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">

	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	
</head>
<body class="ficha noscroll" scroll="no">

	<div class="frame shadow filtroparcelas">
		<div class="title">Importa��o</div>
		<div class="content" style="min-height: 200px; margin: 10px;">
			<div id="impcarregando">
				<img src="/plusoft-resources/images/lite/icones/ajax-loader.gif" align="absmiddle" /> &nbsp;&nbsp; Aguarde, carregando ...
			</div>
		
			<table width="500" style="margin-top: 20px;">
				<tr>
					<td width="150px" align="right">Processando o arquivo</td>
					<td class="seta"></td>
					<td><b><span id="arquivo">&nbsp;</span></b></td>
				</tr>
				<tr>
					<td width="200px" align="right">Registros processados</td>
					<td class="seta"></td>
					<td><b><span id="processados">&nbsp;</span></b></td>
				</tr>
				<tr>
					<td width="200px" align="right">Registros com erro</td>
					<td class="seta"></td>
					<td><b><span id="erros">&nbsp;</span></b></td>
				</tr>
				<tr>
					<td width="200px" align="right">In�cio da execu��o</td>
					<td class="seta"></td>
					<td><b><span id="inicio">&nbsp;</span></b></td>
				</tr>
				<tr>
					<td width="200px" align="right">Fim execu��o</td>
					<td class="seta"></td>
					<td><b><span id="fim">&nbsp;</span></b></td>
				</tr>

				<tr>
					<td colspan="3"><span id="data">&nbsp;</span></td>
				</tr>

			
			</table>
		</div>
	</div>
	
	<img src="/plusoft-resources/images/botoes/out.gif" title="<bean:message key="prompt.sair"/>" onClick="window.close();" style="position: absolute; bottom: 10px; right: 10px; cursor: pointer;" /> 
	
	
	<html:form styleId="statusImportacaoForm">
		<html:hidden property="idImcoCdImportacaocobr" />
	</html:form>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript">


	var refresh = function() {
		$.post("/csiadm/cobranca/importacao/status/refresh.do", { idImcoCdImportacaocobr : document.statusImportacaoForm.idImcoCdImportacaocobr.value }, function(ret) {
			if(ret.msgerro!=undefined) {
				alert(ret.msgerro);
				return;
			}

			$("#arquivo").text(ret.himc_nm_arquivo);
			$("#processados").text(ret.himc_nr_totallinhasprocessada);
			$("#erros").text(ret.himc_nr_totallinhaserro);
			$("#inicio").text(ret.himc_dh_processamento);
			$("#fim").text(ret.himc_dh_finalprocessamento);

			
			if(ret.himc_dh_finalprocessamento!="") {
				$("#impcarregando").remove();
				alert("A importa��o foi finalizada.");
				return;
			}


			setTimeout("refresh();", 1000);
		}); 
	};
	
	$(document).ready(function() {

		setTimeout("refresh();", 5000);

	});
	
	</script>

	
</body>
</html:html>
