<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.enderecotrocatitulo" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

var bEnvia = true;

function submeteForm() {

	alert("<bean:message key='prompt.osDadosNaoConfirmadosSeraoPerdidos'/>");
	
	if (!bEnvia) {
		return false;
	}
	
	// seta os valores na tela de tras para validacao, cham 66976
	//alteracao do try catch enviada pelo varandas
	try{
		var wi = window.dialogArguments;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep"].value = document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep"].value;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro"].value = document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro"].value;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero"].value = document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero"].value;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio"].value	= document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio"].value;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf"].value = document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf"].value;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro"].value = document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro"].value;
	}catch(e){}
		
	bEnvia = false;
	document.reclamacaoManiForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	document.reclamacaoManiForm.tela.value = '<%=MCConstantes.TELA_END_TROCA%>';
	document.reclamacaoManiForm.target = this.name = 'endTroca';
	document.reclamacaoManiForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';
}

function submeteReset() {
	document.reclamacaoManiForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	document.reclamacaoManiForm.tela.value = '<%=MCConstantes.TELA_END_TROCA%>';
	document.reclamacaoManiForm.target = this.name = 'endTroca';
	document.reclamacaoManiForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';
}

function preencheCamposPlusCep(logradouro, bairro, municipio, uf, cep, ddd){
	document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro').value = logradouro;
	document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro').value = bairro;
	document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio').value = municipio;
	document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf').value = uf;
	document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep').value = cep;
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="principalBgrPageIFRM" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';">

<html:form action="/ReclamacaoMani.do" styleId="reclamacaoManiForm">

<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />

<input type="hidden" name="logradouro" value="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro">
<input type="hidden" name="bairro" value="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro">
<input type="hidden" name="municipio" value="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio">
<input type="hidden" name="estado" value="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf">
<input type="hidden" name="cep" value="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep">

<table width="99%" align="center" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.enderecotroca" /> </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="99%" align="center" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center" valign="top"> <br>
		      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		        <tr> 
		          <td class="principalLabel" width="33%"><bean:message key="prompt.endereco" /></td>
		          <td class="principalLabel" width="28%">&nbsp;</td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" colspan="2">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			          	<tr>
			          		<td width="70%">
			          			<iframe id=ifrmEndPess name="ifrmEndPess" src="ReclamacaoMani.do?tela=<%=MCConstantes.TELA_CMB_END_PESSOA%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>" width="100%" height="20" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			          		</td>
			          		<td>&nbsp;</td>
			          	</tr>
			          </table>	
		          </td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="61%" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="10"></td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="33%"><bean:message key="prompt.endereco" /></td>
		          <td class="principalLabel" width="28%"><bean:message key="prompt.numerocomplemento" /></td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="33%"> 
		            <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro" styleClass="principalObjForm" maxlength="100"  />
		          </td>
		          <td class="principalLabel" width="28%"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="21%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero" styleClass="principalObjForm" maxlength="10"  />
		                </td>
		                <td width="79%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsComplemento" styleClass="principalObjForm" maxlength="50"  />
		                </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="33%"><bean:message key="prompt.bairro" /></td>
		          <td class="principalLabel" width="28%"><bean:message key="prompt.cidadeuf" /></td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="33%"> 
		            <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro" styleClass="principalObjForm" maxlength="60"  />
		          </td>
		          <td class="principalLabel" width="28%"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="86%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio" styleClass="principalObjForm" maxlength="80"  />
		                </td>
		                <td width="14%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf" styleClass="principalObjForm" maxlength="5"  />
		                </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="20%"><bean:message key="prompt.cep" /></td>
		          <td class="principalLabel" width="60%"><bean:message key="prompt.referencia" /></td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="33%"> 
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              <tr> 
		                <td width="91%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep" styleClass="principalObjForm" maxlength="8"  />
		                </td>
		                <td width="9%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" title="<bean:message key='prompt.buscaCep'/>" onClick="showModalDialog('PlusCep.do',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')" border="0"></td>
		              </tr>
		            </table>
		          </td>
		          <td class="principalLabel" width="28%" align="center"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="80%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsReferencia" styleClass="principalObjForm" maxlength="50"  />
		                </td>
			            <td class="principalLabel" width="20%" align="center"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.remaEnFoneTroca" styleClass="principalObjForm" maxlength="20" />
			            </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		      </table>
	        <table border="0" cellspacing="0" cellpadding="4" align="right">
	          <tr> 
	            <td> 
	              <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.gravar'/>" class="geralCursoHand" onclick="submeteForm()"></div>
	            </td>
	            <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.cancelar'/>" class="geralCursoHand" onclick="submeteReset()"></td>
	          </tr>
	        </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.cancelar'/>" onClick="javascript:window.close()" class="geralCursoHand">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
<div id="aguarde" style="position:absolute; left:250px; top:50px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>

<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>