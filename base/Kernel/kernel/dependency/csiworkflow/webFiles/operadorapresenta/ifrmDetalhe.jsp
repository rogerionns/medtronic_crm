<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<html>
<head>
<title>..: <bean:message key="prompt.detalhes" /> :.. </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <div name="textfield" style="position: relative; height: 440px;overflow: auto" id="textfield" class="principalObjForm">
      </div>
    </td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> <img src="../images/botoes/out.gif" width="25" height="25" border="0" title='<bean:message key="prompt.sair" />' onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<script>
	textfield.innerHTML = window.dialogArguments.value;</script>
</body>
</html>