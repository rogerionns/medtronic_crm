<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script>

function carregaTextoLaudo(){

	var idLaudo = investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.idLapaCdLaudopadrao'].value;
	var tipoLaudo = investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.lapaInTipolaudo'].value;	
	var txtLaudo = ""; 
	
	if (idLaudo.length > 0){
		txtLaudo = eval("investigacaoForm.txtLaudo" + idLaudo + ".value");
	}
	
	if (tipoLaudo == "T"){
		parent.investigacaoForm.relaTxLabLaudo.value = txtLaudo;
	}else if (tipoLaudo == "C"){
		parent.investigacaoForm.relaTxConsLaudo.value = txtLaudo;
	}
	
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/Investigacao.do" styleId="investigacaoForm">
	<html:hidden property="acao" value="showAll" />
	<html:hidden property="tela" value="cmbLaudoPadrao" />
	<html:hidden property="csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.lapaInTipolaudo" />
	
    <html:select property="csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.idLapaCdLaudopadrao" onchange="carregaTextoLaudo()" styleClass="principalObjForm">
	  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	  <logic:present name="csCdtbLaudopadraoLapaVector">
	    <html:options collection="csCdtbLaudopadraoLapaVector" property="idLapaCdLaudopadrao" labelProperty="lapaDsLaudopadrao"/>
	  </logic:present>
    </html:select>
    
    <logic:present name="csCdtbLaudopadraoLapaVector">
    	<logic:iterate id="laudoVector" name="csCdtbLaudopadraoLapaVector">
    		<input type="hidden" name='txtLaudo<bean:write name="laudoVector" property="idLapaCdLaudopadrao"/>' value='<bean:write name="laudoVector" property="lapaTxLaudopadrao"/>'>
    	</logic:iterate>
    </logic:present>
    
</html:form>
</body>
</html>