<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
function submeteForm() {
	//Chamado: 83639 - 07/08/2012 - Carlos Nunes
	 var src  = "ManifestacaoFollowup.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>";
	     src += "&tela=<%=MCConstantes.TELA_CMB_FUNC_FOLLOWUP_MANIF%>";
	     src += "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario=" + manifestacaoFollowupForm['csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario'].value ;
	     src += "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea=" + manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value;
	
	 parent.cmbResponsavel.location.href = src;
}

function iniciaTela(){
	submeteForm();

	verificaSeCmbEventoJaCarregou();	
	if (Number('<%=request.getAttribute("nrFollowup")%>')>0){
		manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = true;
	}
}

var count = 0;
function verificaSeCmbEventoJaCarregou() {
	try{
		if(count < 15){ 
			if(parent.cmbEvento && (parent.cmbEvento.location=='about:blank' ||  (parent.cmbEvento.document.reayState && parent.cmbEvento.document.reayState == 'complete'))) {
				if(parent.cmbEvento.document.forms[0]["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].disabled == true){
					manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = true;
				}
			} else {
				setTimeout('verificaSeCmbEventoJaCarregou()', 200);
				count++
			}
		}
	}catch(e){
		setTimeout('verificaSeCmbEventoJaCarregou()', 200);
		count++
	}
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();" style="overflow: hidden;">
<html:form action="/ManifestacaoFollowup.do" styleId="manifestacaoFollowupForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario" />
  <input type="hidden" name="idEmprCdEmpresa"></input>
  
  <html:select property="csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea" styleClass="principalObjForm" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbAreaAreaVector">
	  <html:options collection="csCdtbAreaAreaVector" property="idAreaCdArea" labelProperty="areaDsArea"/>
	</logic:present>
  </html:select>
</html:form>
</body>
</html>