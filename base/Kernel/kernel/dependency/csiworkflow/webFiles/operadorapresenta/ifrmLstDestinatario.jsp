<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
	String codFuncionario = "";
	String nomeFuncionario = "";
	String areaFuncionario = "";
	String inluirLinhaDest = "false";
	
	if(request.getSession() != null && request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null){
		codFuncionario = String.valueOf(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario());
		nomeFuncionario = (String.valueOf(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario()));
		areaFuncionario = (String.valueOf(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbAreaAreaVo().getAreaDsArea()));
		inluirLinhaDest = (String)request.getSession().getAttribute("adicionarLinhaDest");
	}
	
%>

<html>
<head>
<title>ifrmManifestacaoDestinatario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
nLinha = new Number(0);
nLinhaRespAntigo=new Number(0);

var contadorDestinatario = 0;
var destinatarioAtual = 0;

var dhPrevisaoDestinatario = "";
var dhPrevisaoIgualDhMani = false;

function addFunc(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, sucessoIntegracao, cFuncionarioAreaResp, modulo) {
	var idOportunidade = parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idOporCdOportunidade"].value;
	nLinha = nLinha + 1;

	//Armagena para depois repor o valor do respons�vel caso apague todos.
		obj=document.getElementsByName('madsInParaCc');
		obj1=document.getElementsByName('idFuncCdFuncionario');
	
		if(obj!=null && obj[0]!=undefined){
			for(i=0;i<obj.length;i++){
				if(obj[i].checked){
					nLinhaRespAntigo=i;
				}
			}
		}
	//

	if (resposta == ""){
		resposta = "0";
	}
	
	//Nao marca email se for marcar o destinatario como responsavel
	if(this.idFuncCdFuncionario==undefined && cDestinatario==0)
		mail = false;
	
	var val="";
	//for (var i = 0; i < document.all('resposta')[resposta].value.length; i++){
		//var val1 = document.all('resposta')[resposta].value.substr(i,1);
		/*if(val1.indexOf("\n")>-1){
			val =val+ val1.replace('\n', 'QBRLNH');
		}else if(val1.indexOf("\r")>-1){
			//N�o atribui esse caracter.
		}else if(val1.indexOf('"')>-1){
			val = val+val1.replace('"', '&quot;');
		}else if(val1.indexOf("'")>-1){
			val = val+val1.replace("'", "\\\'");
		}else if(val1.indexOf("\\")>-1){
			val = val+val1.replace("\\", "\\\\");
		}else{
			val=val+val1;
		}
	}*/
	
	if( document.getElementsByName('resposta').length == undefined ) {
		val =  document.all('resposta').value;
	} else {
		val =  document.getElementsByName('resposta')[resposta].value;
	}
	
	strTxt = "";
	if (tpManif) {
		strTxt += "	<table id=\"0\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr> ";
		strTxt += "     <td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 title='<bean:message key="prompt.excluir"/>' class=geralCursoHand onclick=removeFunc(\"0\")></td> ";
	} else {
		strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr> ";
		if (cDestinatario == "0")
			strTxt += "     <td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 title='<bean:message key="prompt.excluir"/>' class=geralCursoHand onclick=removeFunc(\"" + nLinha + "\")></td> ";
		else
			strTxt += "     <td class=principalLstPar width=2%>&nbsp;</td> ";
		
	}

	strTxt += "     <td class=principalLstPar width=33%> ";
	strTxt += acronymLst(nArea + " / " + nFuncionario, 40);
	strTxt += "       <input type=\"hidden\" name=\"idFuncCdFuncionario\" value=\"" + cFuncionario + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idFuncCdFuncAreaResponsavel\" value=\"" + cFuncionarioAreaResp + "\" > ";

	//Valdeci - quando o responsavel for uma area resolvedora, uma nova linha � inserida e o valor NOVO � passado para que a linha
	//seja incluida desabilitada. Deve zerar o valor para criar um registro novo na base
	
	if(cDestinatario == "NOVO"){
		strTxt += "       <input type=\"hidden\" name=\"idMadsNrSequencial\" value=\"" + 0 + "\" > ";
	}else{
		strTxt += "       <input type=\"hidden\" name=\"idMadsNrSequencial\" value=\"" + cDestinatario + "\" > ";
	}
	
	strTxt += "       <input type=\"hidden\" name=\"madsTxResposta\" value=\"" + val + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"madsDhResposta\" value=\"" + dResposta + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"madsDhEnvio\" value=\"" + dEnvio + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"madsDhPrevisao\" value=\"" + dPrevisao + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"madsDhPrevisaoOriginal\" value=\"" + dPrevisao + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idFuncCdAlteraPrevisao\" value=\"" + <%=codFuncionario%> + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"nLinha\" value=\"" + nLinha + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idRepaCdRespostaPadrao\" value=\"" + idResposta + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idEtprCdEtapaProcesso\" value=\"" + idEtpr + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"madsIdEtprCdEtapaProcesso\" value=\"\" > ";
	strTxt += "       <input type=\"hidden\" name=\"madsInModulo\" value=\"" + modulo + "\"> ";
	
	strTxt += "     </td> ";
	strTxt += "     <td class=principalLstPar width=6%> ";
	strTxt += "       <input type=\"radio\" name=\"madsInParaCc\" " + (para?"checked":((this.idFuncCdFuncionario==undefined && cDestinatario==0)?"checked":"")) + " " + ('<%=request.getParameter("workflow")%>' != 'null'?(cDestinatario != "0"?"disabled=true":""):"") + "> ";
	strTxt += "     </td> ";
	strTxt += "     <td class=principalLstPar width=6%> ";
	<%//Chamado: 100339 KERNEL-1041 - 14/04/2015 - Marcos Donato // %>
	strTxt += "       <input type=\"checkbox\" name=\"madsInMail\" " + (mail == "S"?"checked":"") + " " + (mail == "T" ? "mailT=\"T\"" : "") + " " + ('<%=request.getParameter("workflow")%>' != 'null'?(cDestinatario != "0"?"disabled=true":""):"") + "> ";
	strTxt += "     </td> ";
	strTxt += "     <td class=principalLstPar width=6%> ";
	strTxt += "       <input type=\"checkbox\" name=\"madsInPapel\" " + (papel?"checked":"") + " " + ('<%=request.getParameter("workflow")%>' != 'null'?(cDestinatario != "0"?"disabled=true":""):"") + "> ";
	strTxt += "     </td> ";
	strTxt += "     <td class=principalLstPar width=15%> ";
	strTxt += "       &nbsp; " + dEnvio;
	strTxt += "     </td> ";
	strTxt += "     <td class=principalLstPar width=15%> ";
	strTxt += "       &nbsp; " + dPrevisao;
	strTxt += "     </td> ";
	strTxt += "     <td class=principalLstPar width=15%> ";
	strTxt += "       &nbsp; " + dResposta;
	strTxt += "     </td> ";
	strTxt += "     <td class='principalLstPar' width='2%' onclick=\"abrirTelaResposta('"+ cDestinatario +"', '"+ cFuncionario +"', '"+ dEnvio +"', '"+ dResposta +"', '"+ nFuncionario +"', '"+ nArea +"', '"+ dPrevisao +"', '"+ idEtpr +"', "+ resposta +", '"+ sucessoIntegracao +"', '"+ idOportunidade +"', '"+ modulo +"')\"> ";
	strTxt += "       <img src=\"webFiles/images/botoes/pasta.gif\" width=16 height=20 class=geralCursoHand  title=\"<bean:message key="prompt.respostaDestinatario" />\"> ";
	strTxt += "     </td> ";
	strTxt += "		</tr> ";
	strTxt += " </table> ";

	//document.getElementById("lstDestinatario").innerHTML += strTxt;
	document.getElementById("lstDestinatario").insertAdjacentHTML("BeforeEnd", strTxt);	
}

function abrirTelaResposta(cDestinatario, cFuncionario, dEnvio, dResposta, nFuncionario, nArea, dPrevisao, idEtapa, resposta, sucessoIntegracao, idOportunidade, modulo){

	document.getElementsByName('linhaRespostaClicada').value = resposta;
	
	showModalDialog("ManifestacaoDestinatario.do?tela=manifResposta"+
		"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value +
		"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+ parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value +
		"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2="+ parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value +
		"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia="+ parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value +
		"&csAstbManifestacaoDestMadsVo.idEtprCdEtapaProcesso="+ idEtapa +
		"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa="+ parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa"].value +
		"&csAstbManifestacaoDestMadsVo.idMadsNrSequencial="+ cDestinatario +
		"&csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario="+ cFuncionario +
		"&nIdFuncCdFuncionario=" + cFuncionario +
		"&csAstbManifestacaoDestMadsVo.madsDhEnvio=" + dEnvio +
		"&csAstbManifestacaoDestMadsVo.madsDhResposta=" + dResposta +
		"&areaDestinatario=" + nArea + " / " + nFuncionario +
		"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.idOporCdOportunidade="+ idOportunidade +
		"&csAstbManifestacaoDestMadsVo.madsDhPrevisao="+ dPrevisao +
		"&csAstbManifestacaoDestMadsVo.madsInModulo="+ modulo +
		"&csAstbManifestacaoDestMadsVo.madsInSucessointegracao="+ sucessoIntegracao, window, "help:no;scroll:no;Status:NO;dialogWidth:690px;dialogHeight:350px,dialogTop:0px,dialogLeft:200px"); //Chamado: 90705 - 18/10/2013 - Carlos Nunes
}

function inicio(){
	/*if('<%=inluirLinhaDest%>' == 'true'){
	
		obj=document.getElementsByName('madsInParaCc');
		if (obj != null){
			if(obj.length != null){
				for (nNode=0;nNode<obj.length;nNode++) {
			  		obj[nNode].checked = false;
				}
			}else{
				obj.checked = false;
			}
		}
	
		addFunc('<%=areaFuncionario%>','<%=nomeFuncionario%>','<%=codFuncionario%>','NOVO',true,'N',false,'','','','');
	}*/
	
	parent.verificarHabilitaRespostaFluxo();
}

function removeFunc(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.func" />';
	if (confirm(msg)) {
		obj=document.getElementsByName('madsInParaCc');
		obj1=document.getElementsByName('nLinha');

// Faz uma busca para poder encaixar o ultimo responsavel caso a pessoa exclua o responsavel que ela colocou
		if(obj!=null && obj[0]!=undefined){
			if(obj1!=null && obj1[0]!=undefined){
				for(i=0;i<obj1.length;i++){
					if(obj1[i].value==nTblExcluir){
						if(obj[i].checked){
							obj[nLinhaRespAntigo].checked=true;
						}
					}
				}
			}
		}

		objIdTbl = document.getElementById(nTblExcluir);
		lstDestinatario.removeChild(objIdTbl);
	}
}

function removeFuncBD(nTblExcluir, cFuncionario, cDestinatario) {
	msg = '<bean:message key="prompt.alert.remov.func" />';
	if (confirm(msg)) {
		objIdTbl = document.getElementById(nTblExcluir);
		lstDestinatario.removeChild(objIdTbl);
		destinatariosExcluidos.value += cDestinatario + ";" + cFuncionario + ";";
	}
}

function removeFuncTpManif() {
	objIdTbl = window.document.all.item("0");
	try {
		if (objIdTbl.length == undefined) {
			if (objIdTbl != null)
				lstDestinatario.removeChild(objIdTbl);
		} else {
			var i = 0;
			while (i < objIdTbl.length) {
				if (objIdTbl[0] != null)
					lstDestinatario.removeChild(objIdTbl[0]);
			}
		}
	} catch(e) {}
}

var i = 1;

</script>
</head>

<body class="principalBgrPageIFRM" bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<input type="hidden" name="destinatariosExcluidos">
<input type="hidden" name="resposta" value="">
<input type="hidden" name="linhaRespostaClicada" value="">

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td colspan="3" class="principalLabel"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
  </tr>
  <tr> 
    <td colspan="3" class="principalLabel"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="principalLstCab" width="2%">&nbsp;</td> 
          <td class="principalLstCab" id="cab01" width="33%" colspan="2">
            &#160;<bean:message key="prompt.destarea" />
          </td>
          <td class="principalLstCab" id="cab02" width="6%">
            <img src="webFiles/images/icones/responsavel.gif" width="30" height="30"  title="<bean:message key="prompt.responsavel"/>">
          </td>
          <td class="principalLstCab" id="cab03" width="6%">
            <img src="webFiles/images/icones/email.gif" width="37" height="25"  title="<bean:message key="prompt.email"/>">
          </td>
          <td class="principalLstCab" id="cab04" width="6%">
            <img src="webFiles/images/icones/impress.gif" width="30" height="28"  title="<bean:message key="prompt.Imprimir"/>">
          </td>
          <td class="principalLstCab" id="cab05" width="15%">
            <img src="webFiles/images/icones/carta.gif" width="35" height="20"  title="<bean:message key="prompt.dataEnvioDestinatario"/>">
          </td>
          <td class="principalLstCab" id="cab01" width="15%">
          	<img src="webFiles/images/icones/PrevisaoResolucao.gif" width="30" height="29" title="<bean:message key="prompt.dataprevisao"/>">
          </td>
          <td class="principalLstCab" id="cab06" width="15%">
            <img src="webFiles/images/icones/resposta.gif" width="30" height="29"  title="<bean:message key="prompt.dataRespostaDestinatario"/>">
          </td>
          <td class="principalLstCab" id="cab06" width="2%">&#160;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="3" class="principalLabel" height="100" valign="top"> 
      <!--Inicio DIV Lst Destinatarios -->
      <div id="lstDestinatario" style="width:100%; height:100px; overflow: auto; visibility: visible"> 
	  <script>
	  	var x = 1;	
	  </script>
	  <logic:present name="csAstbManifestacaoDestMadsVector">
        <logic:iterate id="camdmVector" name="csAstbManifestacaoDestMadsVector">
           
       		<!-- Jonathan Costa 07/04/2014 -
        	Identificamos que o hidden do campo maniTxResposta estava usando uma fun��o //acronym//
         	e com isso quando o campo estava vazio era colocado //&nbsp;// no valor do campo,
         	com isso sempre que era verificado se o campo estava vazio a resposta era false
            e assim era inserido a data de resposta no destinat�rio. -->
           
        	<div id="txtResp" style="position:absolute; width:0px; height:0px; visibility: hidden">
        		<textarea name="resposta" class="princpalObjForm" rows="0"><%=((br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta()%></textarea>
        	</div>
         		 <script language="JavaScript">
          			
					addFunc('<bean:write name="camdmVector" property="csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.areaDsArea" />', 
						'<bean:write name="camdmVector" property="csCdtbFuncionarioFuncVo.funcNmFuncionario" />', 
						'<bean:write name="camdmVector" property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />', 
						'<bean:write name="camdmVector" property="idMadsNrSequencial" />', 
						<bean:write name="camdmVector" property="madsInParaCc" />, 
						'<bean:write name="camdmVector" property="madsInMail" />', 
						<bean:write name="camdmVector" property="madsInPapel" />, 
						'<bean:write name="camdmVector" property="madsDhEnvio" />', 
						'<bean:write name="camdmVector" property="madsDhResposta" />', 
						x, 
						false, 
						'<bean:write name="camdmVector" property="idRepaCdRespostaPadrao" />',
						'<bean:write name="camdmVector" property="idEtprCdEtapaProcesso" />',
						'<bean:write name="camdmVector" property="madsDhPrevisao" />',
						'<bean:write name="camdmVector" property="madsInSucessointegracao" />',
						'<bean:write name="camdmVector" property="csCdtbFuncionarioAreaRespVo.idFuncCdFuncionario" />',
						'<bean:write name="camdmVector" property="madsInModulo" />');
					x++;
					
		  </script>
        </logic:iterate>
      </logic:present>
      </div>
      <!--Final DIV Lst Destinatarios -->
    </td>
  </tr>
</table>
</body>
</html>