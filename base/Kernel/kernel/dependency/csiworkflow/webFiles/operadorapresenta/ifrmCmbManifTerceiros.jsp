<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/EnvolvimentoTerceiros.do" styleId="envolvimentoTerceirosForm">
	<html:hidden property="acao" value="showAll" />
	<html:hidden property="tela" value="cmbTerceiros" />
	<input type="hidden" name="cmbIdTptrCdTpTercReclOld" value='<%=request.getParameter("cmbIdTptrCdTpTercRecl")%>'>

	<html:select property="cmbIdTptrCdTpTercRecl" styleClass="principalObjForm">
	  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	  <logic:present name="csCdtbTpTercReclTptrVector">
		<html:options collection="csCdtbTpTercReclTptrVector" property="idTptrCdTpTercRecl" labelProperty="tptrDsTpTercRecl"/>
	  </logic:present>
	</html:select>  
	<logic:present parameter="banco">
	<logic:equal parameter="banco" value="true">
		<script language="JavaScript">
			this.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.disabled = true;
		</script>
	</logic:equal>
	</logic:present>
</html:form>
</body>
</html>