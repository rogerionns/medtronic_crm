<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

function submeteForm() {
	manifestacaoDestinatarioForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	manifestacaoDestinatarioForm.tela.value = '<%=MCConstantes.TELA_CMB_DESTINATARIO_MANIF%>';
	manifestacaoDestinatarioForm.target = parent.cmbDestinatarioManif.name;
	manifestacaoDestinatarioForm.submit();
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
	<html:form action="/ManifestacaoDestinatario.do" styleId="manifestacaoDestinatarioForm">
		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<html:select property="csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea" styleClass="principalObjForm" onchange="submeteForm()">
			<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			<logic:present name="csCdtbAreaAreaVector">
				<html:options collection="csCdtbAreaAreaVector" property="idAreaCdArea" labelProperty="areaDsArea"/>
			</logic:present>
		</html:select>
	</html:form>
</body>
</html>