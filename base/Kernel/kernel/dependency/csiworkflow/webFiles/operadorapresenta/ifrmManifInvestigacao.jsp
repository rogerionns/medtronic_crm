<%@ page language="java" import="com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>

<plusoft:include  id="funcoesManif" href='<%=fileInclude%>'/>
<bean:write name="funcoesManif" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%><html>
<head>
<title>..: <bean:message key="prompt.investig" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">
var bEnvia = true;

function submeteForm() {
	if(!confirm('<bean:message key="prompt.temCertezaDesejaGravarDados"/>'))
	{
		return false;
	}

	//chamada de funcao espec
	var retorno = false;
	try{
		retorno = antesGravarInvestigacao();
		if(!retorno) return;
	}catch(e){}
	
	if (!bEnvia) {
		return false;
	}
	montaLstInvestigacao();
	bEnvia = false;
	investigacaoForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	investigacaoForm.tela.value = '<%=MCConstantes.TELA_INVESTIGACAO%>';
	investigacaoForm.target= this.name = "investigacao";
	investigacaoForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';
}


function submeteReset() {
	investigacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	investigacaoForm.tela.value = '<%=MCConstantes.TELA_INVESTIGACAO%>';
	investigacaoForm.target= this.name = "investigacao";
	investigacaoForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';
}

function adicionarInvestigacao() {
	if (validaCampos()) {

		//chamada de funcao espec, cham 66976
		var retorno = false;
		try{
			retorno = antesIncluirInvestigacao();
			if(!retorno) return;
		}catch(e){}
		
		if (investigacaoForm.altLinha.value == "0" || lstInvestigacao.lstInvestigacao.all.item("lstInvestigacao" + investigacaoForm.altLinha.value) == null) {
			lstInvestigacao.addInvestigacao(cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"].value,
											cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"].value==''?'':cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"][cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"].selectedIndex].text,
											investigacaoForm.relaDhEnvio.value,
											investigacaoForm.relaDhRetorno.value,
											cmbOrigemProblema.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema"].value,
											cmbOrigemProblema.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema"].value==''?'':cmbOrigemProblema.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema"][cmbOrigemProblema.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema"].selectedIndex].text,
											cmbJustificativa.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa"].value, 
											cmbJustificativa.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa"].value==''?'':cmbJustificativa.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa"][cmbJustificativa.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa"].selectedIndex].text, 
											cmbProcedente.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente'].value,
											cmbProcedente.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente'].value==''?'':cmbProcedente.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente'][cmbProcedente.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente'].selectedIndex].text,
											cmbResultadoAnalise.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbResultadoanalReanVo.idReanCdResultadoanal'].value,											
											trataQuebraLinha(investigacaoForm.relaTxLabLaudo.value).substring(0,4000), 
											trataQuebraLinha(investigacaoForm.relaTxPlanoAcao.value).substring(0,4000),
											trataQuebraLinha(investigacaoForm.relaTxConsLaudo.value).substring(0,4000),
											investigacaoForm.tipoLaudoLab.value,
											investigacaoForm.banco.value=="true"?true:false,
											codificaStringHtml(investigacaoForm["csNgtbReclamacaoLaudoRelaVo.relaTxObservacao"]));
		} else {
			lstInvestigacao.alterInvestigacao(cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"].value,
											cmbLote.document.investigacaoForm.reloNrSequenciaOld.value,
											cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"].value==''?'':cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"][cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"].selectedIndex].text,
											investigacaoForm.relaDhEnvio.value,
											investigacaoForm.relaDhRetorno.value,
											cmbOrigemProblema.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema"].value,
											cmbOrigemProblema.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema"].value==''?'':cmbOrigemProblema.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema"][cmbOrigemProblema.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema"].selectedIndex].text,
											cmbJustificativa.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa"].value, 
											cmbJustificativa.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa"].value==''?'':cmbJustificativa.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa"][cmbJustificativa.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa"].selectedIndex].text, 
											cmbProcedente.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente'].value,
											cmbProcedente.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente'].value==''?'':cmbProcedente.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente'][cmbProcedente.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente'].selectedIndex].text,
											cmbResultadoAnalise.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbResultadoanalReanVo.idReanCdResultadoanal'].value,											
											trataQuebraLinha(investigacaoForm.relaTxLabLaudo.value).substring(0,4000), 
											trataQuebraLinha(investigacaoForm.relaTxPlanoAcao.value).substring(0,4000), 
											trataQuebraLinha(investigacaoForm.relaTxConsLaudo.value).substring(0,4000),
											investigacaoForm.tipoLaudoLab.value,
											investigacaoForm.banco.value=="true"?true:false,
											investigacaoForm.altLinha.value,
											codificaStringHtml(investigacaoForm["csNgtbReclamacaoLaudoRelaVo.relaTxObservacao"]));
		}
		
		cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"].value = "";
		cmbLote.document.investigacaoForm.submit();
		investigacaoForm.relaDhEnvio.value = "";
		investigacaoForm.relaDhRetorno.value = "";
		cmbOrigemProblema.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema"].value = "";
		cmbOrigemProblema.document.investigacaoForm.submit();
		cmbJustificativa.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa"].value = "";
		cmbJustificativa.document.investigacaoForm.submit();
		cmbProcedente.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente'].value = "";
		cmbProcedente.document.investigacaoForm.submit();
		
		cmbResultadoAnalise.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbResultadoanalReanVo.idReanCdResultadoanal'].value = "";
		cmbResultadoAnalise.document.investigacaoForm.submit();
		
		investigacaoForm.relaTxLabLaudo.value = "";
		investigacaoForm.relaTxPlanoAcao.value = "";
		investigacaoForm.relaTxConsLaudo.value = "";
		investigacaoForm.tipoLaudoLab.value = "";
		
		cmbLaudoTecnico.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.idLapaCdLaudopadrao'].value = "";
		cmbLaudoConsumidor.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.idLapaCdLaudopadrao'].value = "";
		
		investigacaoForm.banco.value = "false";
		investigacaoForm.altLinha.value = "0";
		investigacaoForm["csNgtbReclamacaoLaudoRelaVo.relaTxObservacao"].value = "";

	}
}

function validaCampos() {
	if (cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"].value == "") {
		alert('<bean:message key="prompt.alert.combo.lote" />');
		cmbLote.document.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"].focus();
		return false;
	}
	if (investigacaoForm.relaDhEnvio.value != "")
		if (!verificaData(investigacaoForm.relaDhEnvio))
			return false;
	if (investigacaoForm.relaDhRetorno.value != "")
		if (!verificaData(investigacaoForm.relaDhRetorno))
			return false;
	
	if (investigacaoForm.optTipoLaudo[0].checked){
		investigacaoForm.tipoLaudoLab.value = investigacaoForm.optTipoLaudo[0].value;
	}else if (investigacaoForm.optTipoLaudo[1].checked){
		investigacaoForm.tipoLaudoLab.value = investigacaoForm.optTipoLaudo[1].value;
	}		
			
	return true;
	
	
}

function montaLstInvestigacao() {
	try {
		if (lstInvestigacao.document.all["codigo"].length != undefined) {
			for (var i = 0; i < lstInvestigacao.document.all["codigo"].length; i++) {
				investigacaoForm.relaDhEnvioReq.value += (lstInvestigacao.document.all["envio"][i].value==''?' ':lstInvestigacao.document.all["envio"][i].value) + "@#@";
				investigacaoForm.relaDhRetornoReq.value += (lstInvestigacao.document.all["retorno"][i].value==''?' ':lstInvestigacao.document.all["retorno"][i].value) + "@#@";
				investigacaoForm.idOripCdOrigemProblemaReq.value += (lstInvestigacao.document.all["origem"][i].value==''?'0':lstInvestigacao.document.all["origem"][i].value) + ";";
				investigacaoForm.idJustCdJustificativaReq.value += (lstInvestigacao.document.all["justificativa"][i].value==''?'0':lstInvestigacao.document.all["justificativa"][i].value) + ";";
				investigacaoForm.relaInConProcedenteReq.value += (lstInvestigacao.document.all["procedente"][i].value==''?' ':lstInvestigacao.document.all["procedente"][i].value) + "@#@";
				investigacaoForm.idReanCdResultadoanalReq.value += (lstInvestigacao.document.all["resultAnalise"][i].value==''?'0':lstInvestigacao.document.all["resultAnalise"][i].value) + ";";
				
				var val = descodificaStringHtml(lstInvestigacao.document.all["laudo"][i].value);
				if( val == "") val = " ";
				investigacaoForm.relaTxLabLaudoReq.value += val + "@#@";
				
				val = descodificaStringHtml(lstInvestigacao.document.all["planoAcao"][i].value);
				if( val == "") val = " ";
				investigacaoForm.relaTxPlanoAcaoReq.value += val + "@#@";
				
				val = descodificaStringHtml(lstInvestigacao.document.all["observacao"][i].value);
				if( val == "") val = " ";
				investigacaoForm.relaTxObservacaoReq.value += val + "@#@";
				
				val = descodificaStringHtml(lstInvestigacao.document.all["consLaudo"][i].value);
				if( val == "") val = " ";
				investigacaoForm.relaTxConsLaudoReq.value += val + "@#@";
				
				investigacaoForm.reloNrSequenciaReq.value += lstInvestigacao.document.all["codigo"][i].value + ";";
				investigacaoForm.relaInTipoLaudoReq.value += (lstInvestigacao.document.all["tipoLaudoLab"][i].value==''?' ':lstInvestigacao.document.all["tipoLaudoLab"][i].value) + "@#@";
			}
		} else {
			investigacaoForm.relaDhEnvioReq.value = (lstInvestigacao.document.all["envio"].value==''?' ':lstInvestigacao.document.all["envio"].value) + "@#@";
			investigacaoForm.relaDhRetornoReq.value = (lstInvestigacao.document.all["retorno"].value==''?' ':lstInvestigacao.document.all["retorno"].value) + "@#@";
			investigacaoForm.idOripCdOrigemProblemaReq.value = (lstInvestigacao.document.all["origem"].value==''?'0':lstInvestigacao.document.all["origem"].value) + ";";
			investigacaoForm.idJustCdJustificativaReq.value = (lstInvestigacao.document.all["justificativa"].value==''?'0':lstInvestigacao.document.all["justificativa"].value) + ";";
			investigacaoForm.relaInConProcedenteReq.value = (lstInvestigacao.document.all["procedente"].value==''?' ':lstInvestigacao.document.all["procedente"].value) + "@#@";
			investigacaoForm.idReanCdResultadoanalReq.value = (lstInvestigacao.document.all["resultAnalise"].value==''?'0':lstInvestigacao.document.all["resultAnalise"].value) + ";";
			
			var val = descodificaStringHtml(lstInvestigacao.document.all["laudo"].value);
			if( val == "") val = " ";
			investigacaoForm.relaTxLabLaudoReq.value = val + "@#@";
			
			val = descodificaStringHtml(lstInvestigacao.document.all["planoAcao"].value);
			if( val == "") val = " ";
			investigacaoForm.relaTxPlanoAcaoReq.value = val + "@#@";
			
			val = descodificaStringHtml(lstInvestigacao.document.all["observacao"].value);
			if( val == "") val = " ";
			investigacaoForm.relaTxObservacaoReq.value = val + "@#@";
			
			val = descodificaStringHtml(lstInvestigacao.document.all["consLaudo"].value);
			if( val == "") val = " ";
			investigacaoForm.relaTxConsLaudoReq.value = val + "@#@";
			
			investigacaoForm.reloNrSequenciaReq.value = lstInvestigacao.document.all["codigo"].value + ";";
			investigacaoForm.relaInTipoLaudoReq.value += (lstInvestigacao.document.all["tipoLaudoLab"].value==''?' ':lstInvestigacao.document.all["tipoLaudoLab"].value) + "@#@";
		}
	} catch (e) {
	}
	investigacaoForm.investExcluidas.value = lstInvestigacao.document.all["investExcluidas"].value;
}


function trataLaudoTecnico(){
	var url;
	if (investigacaoForm.optTipoLaudo[0].checked){
		if (investigacaoForm.relaTxLabLaudo.value.length > 0){
			if (!confirm('<bean:message key="prompt.O_texto_de_laudo_digitado_sera_perdido_Deseja_continuar_a_operacao"/>?')){
				investigacaoForm.optTipoLaudo[1].checked = true;
				return false;
			}
		}
	
		//carrega combo de laudo Tecnico
		url = "&acao=showAll";
		investigacaoForm.relaTxLabLaudo.disabled = true;
		
	}else if (investigacaoForm.optTipoLaudo[1].checked){
		//limpa combo de laudo Tecnico
		url = "";
		investigacaoForm.relaTxLabLaudo.disabled = false;
	}

	investigacaoForm.relaTxLabLaudo.value = "";
	cmbLaudoTecnico.document.location.href = "Investigacao.do?tela=cmbLaudoPadrao&csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.lapaInTipolaudo=T" + url;

}

function inicio(){

	showError('<%=request.getAttribute("msgerro")%>');
	document.all.item('aguarde').style.visibility = 'hidden';

	try{
		//cham 66976
		onloadEspecInvestigacao();
	}catch(e){}

}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="principalBgrPageIFRM" leftmargin="0" onload="inicio();">

<html:form action="/Investigacao.do" styleId="investigacaoForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />

<html:hidden property="csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />

<input type="hidden" name="banco" value="false">
<input type="hidden" name="altLinha" value="0">

<!-- Lst Investigacao -->
<html:hidden property="relaDhEnvioReq" />
<html:hidden property="relaDhRetornoReq" />
<html:hidden property="idOripCdOrigemProblemaReq" />
<html:hidden property="idJustCdJustificativaReq" />
<html:hidden property="relaInConProcedenteReq" />
<html:hidden property="relaTxLabLaudoReq" />
<html:hidden property="relaTxPlanoAcaoReq" />
<html:hidden property="relaTxObservacaoReq" />

<html:hidden property="idReanCdResultadoanalReq"/>
<html:hidden property="relaTxConsLaudoReq" />
<html:hidden property="relaInTipoLaudoReq" />

<html:hidden property="reloNrSequenciaReq" />
<html:hidden property="investExcluidas" />

<input type="hidden" name="tipoLaudoLab">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166">
            <plusoft:message key="prompt.investigacao" />
          </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="200" align="center"> 
            <table width="99%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel"><plusoft:message key="prompt.produto" /></td>
                <td class="principalLabel" colspan="4"><plusoft:message key="prompt.reclamacao" /></td>
              </tr>
              <tr> 
                <td width="50%"> 
                  <input type="text" name="prasDsProdutoAssunto" class="principalObjForm" readonly="true" value="<%=request.getParameter("prasDsProdutoAssunto")%>" >
                  <script>
                  		investigacaoForm.prasDsProdutoAssunto.value = window.dialogArguments.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto"].value;
                  </script>
                </td>
                <td width="50%" colspan="4"> 
                  <input type="text" name="tpmaDsTpManifestacao" class="principalObjForm" readonly="true" value="<%=request.getParameter("tpmaDsTpManifestacao")%>" >
                	<script>
						investigacaoForm.tpmaDsTpManifestacao.value = window.dialogArguments.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao"].value;
					</script>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel"><plusoft:message key="prompt.lote" /></td>
                <td class="principalLabel" colspan="2"><plusoft:message key="prompt.dataenvio" /></td>
                <td class="principalLabel" colspan="2"><plusoft:message key="prompt.dataretorno" /></td>
              </tr>
              <tr> 
                <td width="50%"> 
	              <iframe name="cmbLote" src="Investigacao.do?acao=showAll&tela=cmbLote&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                </td>
               <td width="23%"> 
                  <input type="text" name="relaDhEnvio" class="principalObjForm" onkeypress="validaDigito(this,event)" onblur="verificaData(this,event);" maxlength="10" />
                </td>
                <td width="2%">
                  <img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" width="16" height="15" class="geralCursoHand" border="0" onclick="javascript:show_calendar('investigacaoForm.relaDhEnvio');">
                </td>
                <td width="22%"> 
                  <input type="text" name="relaDhRetorno" class="principalObjForm" onkeypress="validaDigito(this,event)" onblur="verificaData(this,event);" maxlength="10" />
                </td>
                <td width="3%">
                  <img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" width="16" height="15" class="geralCursoHand" border="0" onclick="javascript:show_calendar('investigacaoForm.relaDhRetorno');">
                </td>
               </tr>
              <tr> 
                <td colspan="8"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td colspan="2"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalLabel" width="50%"><plusoft:message key="prompt.laudoinvestigacao" />
	                            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
                            	<input type="radio" name="optTipoLaudo" value="P" checked onclick="trataLaudoTecnico()"><plusoft:message key="prompt.padrao"/>
                            	<input type="radio" name="optTipoLaudo" value="E" onclick="trataLaudoTecnico()"><plusoft:message key="prompt.Editavel"/>
                            </td>
                          </tr>
                          <tr> 
                            <td width="50%" > 
								<iframe name="cmbLaudoTecnico" src="Investigacao.do?acao=showAll&tela=cmbLaudoPadrao&csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.lapaInTipolaudo=T" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                            </td>
                          </tr>
                          <tr> 
                            <td> 
								<textarea name="relaTxLabLaudo" class="principalObjForm" disabled rows="5" onkeyup="textCounter(this, 4000)" onblur="textCounter(this, 4000)"></textarea>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td width="50%" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td class="principalLabel"><plusoft:message key="prompt.detalhamentoLaudo" /><br/>
									<html:textarea property="csNgtbReclamacaoLaudoRelaVo.relaTxObservacao"  styleClass="principalObjForm" style="height: 93px;" />
								</td>
							</tr>
						</table>
					</td>
                   </tr>
                    <tr>
                    	<td colspan="4">
	                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="50%">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
					                    <tr> 
					                      <td class="principalLabel" width="50%"><plusoft:message key="prompt.origem" /></td>
					                    </tr>
					                    <tr> 
					                      <td width="50%" height="23">
								            <iframe name="cmbOrigemProblema" src="Investigacao.do?acao=showAll&tela=cmbOrigemProblema" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
					                      </td>
					                    </tr>
					                    <tr> 
					                      <td class="principalLabel" width="50%"><%= getMessage("prompt.justificativaLaudo", request)%></td>
					                    </tr>
					                    <tr>
						                  <td width="50%" height="23">
					                    	<iframe name="cmbJustificativa" src="Investigacao.do?acao=showAll&tela=cmbJustificativa" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
					                      </td>
					                    </tr>
					                    <tr> 
					                      <td class="principalLabel" width="50%"><%= getMessage("prompt.ResultadoAnalise", request)%></td>
					                    </tr>
					                    <tr> 
					                      <td width="50%" height="23">
								            <iframe name="cmbResultadoAnalise" src="Investigacao.do?acao=showAll&tela=cmbResultadoAnalise" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
					                      </td>
					                    </tr>
					                    <tr> 
					                      <td class="principalLabel" width="50%"><%= getMessage("prompt.procedente", request)%></td>
					                    </tr>
					                    <tr> 
					                      <td width="50%" height="23">
								            <iframe name="cmbProcedente" src="Investigacao.do?acao=showAll&tela=cmbProcedente" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
					                      </td>
					                    </tr>
									</table>
								</td>
								<td width="50%" valign="top" >
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="principalLabel" ><plusoft:message key="prompt.planoacao" /></td>
										</tr>
										<tr>
				                            <td> 
				                              <textarea name="relaTxPlanoAcao" class="principalObjForm" rows="9" onkeyup="textCounter(this, 2000)" onblur="textCounter(this, 2000)" ></textarea>
				                            </td>
										</tr>
									</table>
								</td>
							</tr>
	                      </table>
                    	</td>
                    </tr>
                    
                    <tr>
                    	<td colspan="4">
                    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    			<tr>
             						<td class="principalLabel" width="50%"><plusoft:message key="prompt.LaudoConsumidor"/></td>
                    				<td width="50%">&nbsp;</td>
                    			</tr>
                    			<tr>
             						<td width="50%"><iframe name="cmbLaudoConsumidor" src="Investigacao.do?acao=showAll&tela=cmbLaudoPadrao&csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.lapaInTipolaudo=C" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                    				<td width="50%">&nbsp;</td>
                    			</tr>
                    			<tr>
             						<td colspan="2"><textarea name="relaTxConsLaudo" disabled class="principalObjForm" rows="5" onkeyup="textCounter(this, 4000)" onblur="textCounter(this, 4000)" ></textarea></td>
                    			</tr>
                    		</table>
                    	</td> 
                    </tr>
                  </table>
			      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="125">
			        <tr> 
			          <td colspan="2" valign="top" class="principalLabel" height="2">&nbsp;</td>
			        </tr>
			        <tr> 
			          <td width="98%" valign="top" class="principalBordaQuadro">
			            <iframe id=lstInvestigacao name="lstInvestigacao" src="Investigacao.do?acao=showAll&tela=lstInvestigacao&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />" width="100%" height="100%" scrolling="Yes" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			          </td>
			          <td width="2%" valign="top">
			            <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" class="geralCursoHand" title="<bean:message key='prompt.confirmarDadosLaudo'/>" onclick="adicionarInvestigacao()"><br>
			          </td>
			        </tr>
			      </table>
                </td>
              </tr>
            </table>
	        <table border="0" cellspacing="0" cellpadding="4" align="right">
	          <tr> 
	            <td> 
	              <div align="right"><img src="webFiles/images/botoes/gravar.gif" title="<bean:message key="prompt.gravar" />" width="20" height="20" border="0" class="geralCursoHand" onclick="submeteForm()"></div>
	            </td>
	            <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar" />" class="geralCursoHand" onclick="submeteReset()"></td>
	          </tr>
	        </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
<div id="aguarde" style="position:absolute; left:300px; top:200px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>