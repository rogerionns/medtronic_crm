<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
	
	/**
	* Valdeci - 29-05-2006
	* Chamado: 19506
	* Ao editar o followup, posiciona o combo com o id de funcion�rio que est� no combo de Area, 
	* se n�o estiver editando verifica se o evento de followup tem um destinat�rio padr�o
	**/
	function verificarFuncPadrao(){
		try{
			if(parent.cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value != 0 && parent.cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value != "")
			{
				manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = parent.cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value;
			}
			else if(parent.cmbEvento.manifestacaoFollowupForm['csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup'].value != "" && parent.cmbEvento.idFuncCdFuncionario != 0 && parent.cmbEvento.idFuncCdFuncionario != undefined)
			{
				manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = parent.cmbEvento.idFuncCdFuncionario;

				if(manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].selectedIndex == -1)
				{
					manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].selectedIndex = 0;
				}
			}			
		
			parent.cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = "";
			
			//Chamado: 83639 - 07/08/2012 - Carlos Nunes
			setTimeout('verificarEventoDisabled()', 500);
			
		}catch(e){}
		
	}
	
	//Chamado: 83639 - 07/08/2012 - Carlos Nunes
	function verificarEventoDisabled()
	{
		if(parent.cmbEvento.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].disabled == true){
			manifestacaoFollowupForm['csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario'].disabled=true;
		}
		else
		{
			//Chamado 74775 - Vinicius - Quando vier apenas 1 registro j� ficar selecionado
			if(manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].length == 2){
				manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].selectedIndex = 1;
			}
		}
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');setTimeout('verificarFuncPadrao()',200);" style="overflow: hidden;">
<html:form action="/ManifestacaoFollowup.do" styleId="manifestacaoFollowupForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
  <html:select property="csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario" styleClass="principalObjForm">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbFuncionarioFuncVector">
	  <html:options collection="csCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>
	</logic:present>
  </html:select>
</html:form>
</body>
</html>