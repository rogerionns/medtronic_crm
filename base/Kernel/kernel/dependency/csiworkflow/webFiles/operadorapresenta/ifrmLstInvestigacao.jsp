<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
nLinha = new Number(0);
estilo = new Number(0);

function addInvestigacao(cLote, nLote, dEnvio, dRetorno, cOrigem, nOrigem, cJustificativa, nJustificativa, cProcedente, nProcedente, cResultAnalise, tLaudo, tPlanoAcao, tConsLaudo, tipoLaudoLab, banco, cObservacao) {
	
	if (comparaChave(cLote)) {
		return false;
	}
	
	nLinha = nLinha + 1;
	estilo++;
	
	altTxt = "onclick=\"editInvestigacao('" + cLote + "', '" + nLote + "', '" + dEnvio + "', '" + dRetorno + "', '" + cOrigem + "', '" + nOrigem + "', '" + cJustificativa + "', '" + nJustificativa + "', '" + cProcedente + "', '" + nProcedente + "', '" + cResultAnalise + "', '" + tLaudo + "', '" + tPlanoAcao + "', '" + tConsLaudo + "', '" + tipoLaudoLab + "', '" + banco + "', '" + nLinha + "', '"+ cObservacao +"')\"";

	strTxt = "";
    strTxt += "<div id=\"lstInvestigacao" + nLinha + "\" style='width:100%; height:0px; z-index:1; overflow: auto'>";
    strTxt += "<table id=\"" + nLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
    strTxt += "    <input type='hidden' name='envio' value='" + dEnvio + "'>";
    strTxt += "    <input type='hidden' name='retorno' value='" + dRetorno + "'>";
    strTxt += "    <input type='hidden' name='origem' value='" + cOrigem + "'>";
    strTxt += "    <input type='hidden' name='justificativa' value='" + cJustificativa + "'>";
    strTxt += "    <input type='hidden' name='procedente' value='" + cProcedente + "'>";
    strTxt += "    <input type='hidden' name='resultAnalise' value='" + cResultAnalise + "'>";
    strTxt += "    <input type='hidden' name='laudo' value='" + tLaudo + "'>";
    strTxt += "    <input type='hidden' name='planoAcao' value='" + tPlanoAcao + "'>";
    strTxt += "    <input type='hidden' name='consLaudo' value='" + tConsLaudo + "'>";
    strTxt += "    <input type='hidden' name='tipoLaudoLab' value='" + tipoLaudoLab + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cLote + "'>";
    strTxt += "    <input type='hidden' name='observacao' value='" + cObservacao + "'>";
    if (!banco)
	    strTxt += "    <td class='principalLstParMao' width='2%' align='center'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeInvestigacao('" + nLinha + "')\"></td>";
	else
	    strTxt += "    <td class='principalLstParMao' width='2%' align='center'><img id='lixeiraInvestigacao' name='liseixaInvestigacao' src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeInvestigacaoBD('" + nLinha + "', '" + cLote + "')\"></td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='20%'>" + acronymLst(nLote!=undefined?nLote:"", 15) + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='14%'>" + dEnvio + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='14%'>" + dRetorno + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='20%'>" + acronymLst(nOrigem!=undefined?nOrigem:"", 15) + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='20%'>" + acronymLst(nJustificativa!=undefined?nJustificativa:"", 15) + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='10%'>" + acronymLst(nProcedente!=undefined?nProcedente:"", 15) + "&nbsp;</td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    strTxt += "</div>";

	document.getElementById("lstInvestigacao").innerHTML += strTxt;
}

function alterInvestigacao(cLote, cLoteOld, nLote, dEnvio, dRetorno, cOrigem, nOrigem, cJustificativa, nJustificativa, cProcedente, nProcedente, cResultAnalise, tLaudo, tPlanoAcao, tConsLaudo, tipoLaudoLab, banco, altLinha, cObservacao) {
	
	if (cLote != cLoteOld) {
		if (comparaChave(cLote)) {
			return false;
		}
	}

	altTxt = "onclick=\"editInvestigacao('" + cLote + "', '" + nLote + "', '" + dEnvio + "', '" + dRetorno + "', '" + cOrigem + "', '" + nOrigem + "', '" + cJustificativa + "', '" + nJustificativa + "', '" + cProcedente + "', '" + nProcedente + "', '" + cResultAnalise + "',  '" + tLaudo + "', '" + tPlanoAcao + "', '" + tConsLaudo + "', '" + tipoLaudoLab + "', '" + banco + "', '" + altLinha + "', '" + cObservacao +"')\"";

	strTxt = "";
    strTxt += "<table id=\"" + altLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
    strTxt += "    <input type='hidden' name='envio' value='" + dEnvio + "'>";
    strTxt += "    <input type='hidden' name='retorno' value='" + dRetorno + "'>";
    strTxt += "    <input type='hidden' name='origem' value='" + cOrigem + "'>";
    strTxt += "    <input type='hidden' name='justificativa' value='" + cJustificativa + "'>";
    strTxt += "    <input type='hidden' name='procedente' value='" + cProcedente + "'>";
    strTxt += "    <input type='hidden' name='resultAnalise' value='" + cResultAnalise + "'>";    
    strTxt += "    <input type='hidden' name='laudo' value='" + tLaudo + "'>";
    strTxt += "    <input type='hidden' name='planoAcao' value='" + tPlanoAcao + "'>";
    strTxt += "    <input type='hidden' name='consLaudo' value='" + tConsLaudo + "'>";
    strTxt += "    <input type='hidden' name='tipoLaudoLab' value='" + tipoLaudoLab + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cLote + "'>";
    strTxt += "    <input type='hidden' name='observacao' value='" + cObservacao + "'>";
    if (!banco)
	    strTxt += "    <td class='principalLstParMao' width='2%' align='center'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeInvestigacao('" + altLinha + "')\"></td>";
	else
	    strTxt += "    <td class='principalLstParMao' width='2%' align='center'><img id='lixeiraInvestigacao' name='liseixaInvestigacao' src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeInvestigacaoBD('" + altLinha + "', '" + cLote + "')\"></td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='20%'>" + acronymLst(nLote, 15) + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='14%'>" + dEnvio + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='14%'>" + dRetorno + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='20%'>" + acronymLst(nOrigem, 15) + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='20%'>" + acronymLst(nJustificativa, 15) + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='10%'>" + acronymLst(nProcedente, 15) + "&nbsp;</td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    
	removeInvestigacaoAlt(altLinha);
	document.getElementsByName("lstInvestigacao" + altLinha).innerHTML += strTxt;
}

function removeInvestigacao(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.inve" />';
	if (confirm(msg)) {
		objIdTbl = lstInvestigacao.getElementById("lstInvestigacao" + nTblExcluir);
		lstInvestigacao.removeChild(objIdTbl);
		estilo--;
	}
}

function removeInvestigacaoAlt(nTblExcluir) {
	objIdTbl = eval("lstInvestigacao" + nTblExcluir).getElementById(nTblExcluir);
	eval("lstInvestigacao" + nTblExcluir).removeChild(objIdTbl);
}

function removeInvestigacaoBD(nTblExcluir, cLote) {
	msg = '<bean:message key="prompt.alert.remov.inve" />';
	if (confirm(msg)) {
		objIdTbl = lstInvestigacao.getElementById("lstInvestigacao" + nTblExcluir);
		lstInvestigacao.removeChild(objIdTbl);
		investExcluidas.value += cLote + ";";
		estilo--;
	}
}

function editInvestigacao(cLote, nLote, dEnvio, dRetorno, cOrigem, nOrigem, cJustificativa, nJustificativa, cProcedente, nProcedente, cResultAnalise, tLaudo, tPlanoAcao, tConsLaudo, tipoLaudoLab, banco, altLinha, cObservacao) {

	if(banco == "true"){
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_TELAINVESTIGACAO_INVESTIGACAO_ALTERACAO_CHAVE%>')){
			alert("<bean:message key='prompt.semPermissaoParaEditar'/>");
			return false;
		}
	}
		
	window.parent.cmbLote.location = "Investigacao.do?acao=showAll&tela=cmbLote&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia=" + cLote + "&banco=" + banco + "&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />";
	window.parent.investigacaoForm.relaDhEnvio.value = dEnvio;
	window.parent.investigacaoForm.relaDhRetorno.value = dRetorno;
	window.parent.cmbOrigemProblema.location = "Investigacao.do?acao=showAll&tela=cmbOrigemProblema&csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema=" + cOrigem;
	window.parent.cmbJustificativa.location = "Investigacao.do?acao=showAll&tela=cmbJustificativa&csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa=" + cJustificativa;
	window.parent.cmbProcedente.location = "Investigacao.do?acao=showAll&tela=cmbProcedente&csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente=" + cProcedente;
	window.parent.cmbResultadoAnalise.location = "Investigacao.do?acao=showAll&tela=cmbResultadoAnalise&csNgtbReclamacaoLaudoRelaVo.csCdtbResultadoanalReanVo.idReanCdResultadoanal=" + cResultAnalise;

	if (tipoLaudoLab == "E")
		window.parent.investigacaoForm.optTipoLaudo[1].checked = true;
	else
		window.parent.investigacaoForm.optTipoLaudo[0].checked = true;
	window.parent.trataLaudoTecnico();
	
	window.parent.investigacaoForm.relaTxLabLaudo.value = trataQuebraLinha2(tLaudo);
	window.parent.investigacaoForm.relaTxPlanoAcao.value = trataQuebraLinha2(tPlanoAcao);
	window.parent.investigacaoForm.relaTxConsLaudo.value = trataQuebraLinha2(tConsLaudo);
	
	window.parent.cmbLaudoTecnico.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.idLapaCdLaudopadrao'].value = "";
	window.parent.cmbLaudoConsumidor.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.idLapaCdLaudopadrao'].value = "";
	
	window.parent.investigacaoForm.banco.value = banco;
	window.parent.investigacaoForm.altLinha.value = altLinha;
	
	window.parent.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.relaTxObservacao"].value = descodificaStringHtml(cObservacao);
}

function comparaChave(cLote) {
	try {
		if (codigo.length != undefined) {
			for (var i = 0; i < codigo.length; i++) {
				if (codigo[i].value == cLote) {
					alert('<bean:message key="prompt.alert.lote.exist" />');
					return true;
				}
			}
		} else {
			if (codigo.value == cLote) {
				alert('<bean:message key="prompt.alert.lote.exist" />');
				return true;
			}
		}
	} catch (e){}
	return false;
}
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<input type="hidden" name="investExcluidas">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="principalLstCab" width="2%">&nbsp;</td>
    <td class="principalLstCab" width="20%"><plusoft:message key="prompt.lote" /></td>
    <td class="principalLstCab" width="14%"><plusoft:message key="prompt.dataenvio" /></td>
    <td class="principalLstCab" width="14%"><plusoft:message key="prompt.dataretorno" /></td>
    <td class="principalLstCab" width="20%"><plusoft:message key="prompt.origemproblema" /></td>
    <td class="principalLstCab" width="20%"><%= getMessage("prompt.justificativaLaudo", request)%></td>
    <td class="principalLstCab" width="10%"><%= getMessage("prompt.procedente", request)%></td>
  </tr>
  <tr valign="top"> 
    <td class="principalLstPar" colspan="10" height="100%">
      <div id="lstInvestigacao" style="position:absolute; width:100%; height:0px; z-index:1">
        <!--Inicio Lista Lote -->
		<logic:present name="csNgtbReclamacaoLaudoRelaVector">
          <logic:iterate id="cnrlrVector" name="csNgtbReclamacaoLaudoRelaVector">
            <script language="JavaScript">
			  addInvestigacao('<bean:write name="cnrlrVector" property="csNgtbReclamacaoLoteReloVo.reloNrSequencia" />',
			  			'<bean:write name="cnrlrVector" property="csNgtbReclamacaoLoteReloVo.reloDsLote" />',
			  			'<bean:write name="cnrlrVector" property="relaDhEnvio" />',
						'<bean:write name="cnrlrVector" property="relaDhRetorno" />',
						'<bean:write name="cnrlrVector" property="csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema" />', 
						'<bean:write name="cnrlrVector" property="csCdtbOrigemProblemaOripVo.oripDsOrigemProblema" />', 
						'<bean:write name="cnrlrVector" property="csCdtbJustiflaudoJulaVo.idJulaCdJustificativa" />', 
						'<bean:write name="cnrlrVector" property="csCdtbJustiflaudoJulaVo.julaDsJustificativa" />', 
						'<bean:write name="cnrlrVector" property="csCdtbProcedenteProcVo.idProcCdProcedente" />', 
						'<bean:write name="cnrlrVector" property="csCdtbProcedenteProcVo.procDsProcedente" />', 
						'<bean:write name="cnrlrVector" property="csCdtbResultadoanalReanVo.idReanCdResultadoanal" />',
						'<bean:write name="cnrlrVector" property="relaTxLabLaudo" />', 
						'<bean:write name="cnrlrVector" property="relaTxPlanoAcao" />', 
						'<bean:write name="cnrlrVector" property="relaTxConsLaudo" />',
						'<bean:write name="cnrlrVector" property="relaInTipoLaudo" />',
						true,
						'<bean:write name="cnrlrVector" property="relaTxObservacao" />');
	        </script>
          </logic:iterate>
        </logic:present>
        <!--Final Lista Lote -->
      </div>
    </td>
  </tr>
</table>
</body>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_TELAINVESTIGACAO_INVESTIGACAO_EXCLUSAO_CHAVE%>', window.document.all.item("lixeiraInvestigacao"));
</script>

</html>