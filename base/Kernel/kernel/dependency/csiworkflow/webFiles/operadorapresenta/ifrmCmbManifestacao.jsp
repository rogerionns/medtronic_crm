<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
function submeteForm() {
	manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_GRUPO_MANIFESTACAO%>';
	manifestacaoForm.target = parent.cmbGrpManifestacao.name;
	manifestacaoForm.submit();
}

function showCombo() {
	if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value != "" && manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value != "0") {
		submeteForm();
	}
}

function submeteInformacao() {
	manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_GRUPO_MANIFESTACAO%>';
	manifestacaoForm.target = parent.cmbGrpManifestacao.name;
	manifestacaoForm.submit();
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');showCombo();">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto"/> 
  
  <html:select property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" styleClass="principalObjForm" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbManifTipoMatpVector">
	  <html:options collection="csCdtbManifTipoMatpVector" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo"/>
	</logic:present>
    <html:option value="-1"><bean:message key="prompt.followupm" /></html:option>
  </html:select>  
</html:form>
</body>
</html>