<%@ page language="java" %>
<%@ page import = "br.com.plusoft.csi.crm.util.SystemDate" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	//SystemDate s = new SystemDate();
	//String dataAtual = s.getDay() +"/"+ s.getMonth() +"/"+ s.getYear();
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>

<!-- 88642 - 03/09/2013 - Jaider Alba -->
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
</head>

<script language="JavaScript">
	
	var dataAtual = "";
	<logic:present name="manifestacaoFollowupForm" property="dataAtual">
		dataAtual = "<bean:write name="manifestacaoFollowupForm" property="dataAtual"/>";
	</logic:present>
	
	function getDataBanco()
	{
		if(dataAtual != "")
		{
			return dataAtual.substring(0,10);
		}
	}
	
	function getHoraBanco()
	{
		if(dataAtual != "")
		{
			return dataAtual.substring(10,dataAtual.length);
		}
	}
	
	//Funcion�rio padr�o para o followup
	var idFuncCdFuncionario = 0;
	
	function mudarEvento(obj){
		
		// 88642 - 03/09/2013 - Jaider Alba 
		var idTxpm = obj.options[obj.selectedIndex].getAttribute("idTxpmCdTxpadraomanif");
		if(idTxpm != null && parseInt(idTxpm)){
			try{
				var dadospost = { idtx : idTxpm };
				$.post("/csicrm/CarregarTextoPadraoFollowup.do", dadospost, function(ret) {
					if(ret.msgerro!=undefined) {
						alert("<bean:message key='prompt.erro.carregar_texto_padrao'/>"+ret.msgerro);
						return false;
					} else if(ret.txpm!=null && ret.txpm!=undefined && trim(ret.txpm)!="") {
						parent.document.getElementById('textoHistorico').innerHTML = 
							parent.document.getElementById('textoHistorico').innerHTML + "\n" + ret.txpm;
					}
					
				}, "json");
			}
			catch(x){		
				alert("Erro em carregarTextoPadraoFollowup()"+ x.description);		
			}
		}
		
		//DECRETO
		if(obj.options[obj.selectedIndex].getAttribute("inTextoManif") == "S" && obj.value != "" && !confirm('<bean:message key="prompt.confirmaTextoDoFollowupNaManif" />')){
			obj.value = "";
			//return false; // 88642 - 03/09/2013 - Jaider Alba: Removido para que ao fim da funcao seja preenchido o campo "Encerrar Follow Up"
		}
		else{
			//Valdeci 29/05/2006 - Compara o id de funcion�rio do evento com o funcion�rio que est� selecionado
			if(obj.value != "" && obj.options[obj.selectedIndex].getAttribute('idArea') != "0" && obj.options[obj.selectedIndex].getAttribute('idFuncionario') != parent.cmbResponsavel.document.forms[0]["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value){
				//parent.cmbArea.manifestacaoFollowupForm.idEmprCdEmpresa.value = window.top.ifrmTelaFiltro.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				parent.cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value = obj.options[obj.selectedIndex].getAttribute('idArea');
				parent.cmbArea.submeteForm();
				idFuncCdFuncionario = obj.options[obj.selectedIndex].getAttribute('idFuncionario');
				}	
			else{
				idFuncCdFuncionario = 0;
			}
			if(obj.options[obj.selectedIndex].getAttribute('tempoResolucao') != "0"){
				calcularDias(getDataBanco(), obj.options[obj.selectedIndex].getAttribute('tempoResolucao'),obj.options[obj.selectedIndex].getAttribute('inTempoResolucao'));
				parent.document.getElementsByName("registro")[0].value = getDataBanco();
			}else{
				atualizarData();
				parent.manifestacaoFollowupForm.dataPrevista.value = "";
				parent.manifestacaoFollowupForm.horaPrevista.value = "";
			}
		}
		
		// 88642 - 03/09/2013 - Jaider Alba
		if(obj.options[obj.selectedIndex].getAttribute("evfuInConcluir") == "S"){
			
			parent.manifestacaoFollowupForm.chkEncerramento.checked = true;
			
			if(trim(parent.manifestacaoFollowupForm.dataPrevista.value) == "" 
					&& trim(parent.manifestacaoFollowupForm.horaPrevista.value) == ""){
				parent.encerrarFollowup();
			}
		}
		else
		{
			parent.manifestacaoFollowupForm.chkEncerramento.checked = false;
		}
	}
	
	//Chamado: 86828 - 13/02/2013 - Carlos Nunes
	function inicio(){
		var obj = manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"];
		
		if(parent.manifestacaoForm.idEvfuCdEventoFollowup.value != "" && parent.manifestacaoForm.idEvfuCdEventoFollowup.value != 0)
			obj.value = parent.manifestacaoForm.idEvfuCdEventoFollowup.value;
		
		if (Number('<%=request.getAttribute("idEvfu")%>')>0){
			if (Number('<%=request.getAttribute("nrFollowup")%>')>0){
				obj.value = Number('<%=request.getAttribute("idEvfu")%>') ;
				
				obj.disabled = true;
			}
		}
	}

	/**
	 Retorna a data passada somada ao numero de dias
	*/
	function atualizarData(){
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.acao.value = '';
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.submit();
	}
	
	/**
	 Retorna a data passada somada ao numero de dias
	*/
	function calcularDias(data, somar, inTipo){
		var novdata = new Date();
		var retorno;

		ifrmCalculaDhPrevista.manifestacaoFollowupForm.dataAtual.value = data;
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.dias.value = somar;
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.inTipo.value = inTipo;
		//ifrmCalculaDhPrevista.manifestacaoFollowupForm.idEmprCdEmpresa.value = window.top.ifrmTelaFiltro.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.acao.value = '<%=Constantes.ACAO_GRAVAR%>';
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.submit();
		
		/*		
		novdata.setDate(Number(data.substring(0, 2)));
		novdata.setMonth(Number(data.substring(3, 5)));
		novdata.setFullYear(Number(data.substring(6)));
		novdata.setDate(novdata.getDate() + Number(somar));
		retorno = (novdata.getDate() < 10 ? "0"+ novdata.getDate() : novdata.getDate()) +"/";
		retorno += (novdata.getMonth() < 10 ? "0"+ novdata.getMonth() : novdata.getMonth()) +"/";
		retorno += novdata.getFullYear();
		return retorno;
		*/
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" style="overflow: hidden;">
<html:form action="/ManifestacaoFollowup.do" styleId="manifestacaoFollowupForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
	<html:select property="csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup" onchange="mudarEvento(this);" styleClass="principalObjForm">
		<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>

	<logic:present name="csCdtbEventoFollowupEvfuVector">
	<logic:iterate name="csCdtbEventoFollowupEvfuVector" id="csCdtbEventoFollowupEvfuVector" indexId="numero">
		<option
			value="<bean:write name="csCdtbEventoFollowupEvfuVector" property="idEvfuCdEventoFollowup"/>"
			idArea="<bean:write name="csCdtbEventoFollowupEvfuVector" property="csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"/>"
			idFuncionario="<bean:write name="csCdtbEventoFollowupEvfuVector" property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario"/>"
			tempoResolucao="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuNrTemporesolucao"/>"
			inTextoManif="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuInTextoManif"/>"
			inTempoResolucao="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuInTemporesolucao"/>" <% //DECRETO %>
			evfuInConcluir="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuInConcluir"/>" <% // 88642 - 03/09/2013 - Jaider Alba %>
			idTxpmCdTxpadraomanif="<bean:write name="csCdtbEventoFollowupEvfuVector" property="idTxpmCdTxpadraomanif"/>">
			<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuDsEventoFollowup"/>
		</option>
	</logic:iterate>
	</logic:present>
	
  </html:select>
<iframe id=ifrmCalculaDhPrevista name="ifrmCalculaDhPrevista" src="ManifestacaoFollowup.do?tela=<%=MCConstantes.TELA_CALCULA_DH_PREVISTA_FOUP%>" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>   
</html:form>
</body>
</html>