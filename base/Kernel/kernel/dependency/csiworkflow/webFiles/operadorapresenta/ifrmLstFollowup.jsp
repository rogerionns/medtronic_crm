<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo,br.com.plusoft.csi.crm.helper.MCConstantes,
								br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = null;

if(request.getSession() != null && request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) != null) {
	empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
}

CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
long nivelSupervisor = (MAConstantes.ID_NIVE_CD_NIVELACESSO_SUPERVISOR);

Vector funcAreaVector = new Vector();
try {
	funcAreaVector = (Vector) request.getAttribute("csAstbFuncresolvedorFureVector");
} catch(Exception e) {}

boolean seguracaFollowup = Configuracoes.obterFeature(ConfiguracaoConst.CONF_APL_VALIDAR_REGRAS_SEGURANCA_FOLLOWUP,empresaVo.getIdEmprCdEmpresa());

//Chamado: 94022 - 10/04/2014 - Carlos Nunes
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";

String fileIncludeKernel = "/csicrm/webFiles/includes/funcoesManif.jsp";
%>

<%//Chamado: 93408 - Carlos Nunes - 18/02/2014 %>
<plusoft:include  id="funcoesManifKernel" href='<%=fileIncludeKernel%>'/>
<bean:write name="funcoesManifKernel" filter="html"/>


<plusoft:include  id="funcoesManif" href='<%=fileInclude%>'/>
<bean:write name="funcoesManif" filter="html"/>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsAstbFuncresolvedorFureVo"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

var idIdioma = '<%=funcVo.getIdIdioCdIdioma()%>';
var idEmpresa = '<%=empresaVo.getIdEmprCdEmpresa()%>';

nLinha = new Number(0);
estilo = new Number(0);
funcResolvedor = new Array(<%=funcAreaVector.size() %>);
<% 
StringBuffer output = new StringBuffer();
for(int i=0; i < funcAreaVector.size(); i++) { 
	CsAstbFuncresolvedorFureVo fureVo = (CsAstbFuncresolvedorFureVo) funcAreaVector.get(i);
	
	if(fureVo.getFureInResolver()){
		output.append("\nfuncResolvedor[" + i + "] = " + fureVo.getCsCdtbFuncAreaFuncVo().getIdFuncCdFuncionario());	
	}
}

%><%=output.toString() %>

nLinha = new Number(0);

//Chamado: 89940 - 01/08/2013 - Carlos Nunes
function addFollowup(cArea, cResponsavel, nResponsavel, cEvento, nEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, nGerador, cGerador, inEncerramento, inEnvio, dhEnvio, cFuncEncerramento, nIdMatmCdManiftemp) {

	nLinha = nLinha + 1;
    //Chamado: 89940 - 01/08/2013 - Carlos Nunes
	altTxt = "onclick=\"editFollowup_valida('" + cArea + "', '" + cResponsavel + "', '" + nResponsavel + "', '" + cEvento + "', '" + nEvento + "', '" + dRegistro + "', '" + dPrevisao + "', '" + dEfetivo + "', '" + tHistorico + "', '" + cFollowup + "', '" + nLinha + "', '" + nGerador + "', '" + cGerador + "', '" + inEncerramento + "', '" + inEnvio + "' , '" + dhEnvio + "' , '" + cFuncEncerramento + "', '" + nIdMatmCdManiftemp + "')\"";

	strTxt = "";
    strTxt += "<div id=\"lstFollowup" + nLinha + "\" style='width:100%; overflow: auto'>";
    strTxt += "<table id=\"" + nLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr id=\"tr_" + nLinha + "\">"; //Chamado: 94022 - 10/04/2014 - Carlos Nunes
    strTxt += "    <input type='hidden' name='area' value='" + cArea + "'>";
    strTxt += "    <input type='hidden' name='responsavel' value='" + cResponsavel + "'>";
    strTxt += "    <input type='hidden' name='evento' value='" + cEvento + "'>";
    strTxt += "    <input type='hidden' name='registro' value='" + dRegistro + "'>";
    strTxt += "    <input type='hidden' name='previsao' value='" + dPrevisao + "'>";
    strTxt += "    <input type='hidden' name='efetivo' value='" + dEfetivo + "'>";
    strTxt += "    <input type='hidden' name='historico' value='" + tHistorico + "'>";
    strTxt += "    <input type='hidden' name='gerador' value='" + cGerador + "'>";
    strTxt += "    <input type='hidden' name='encerramento' value='" + inEncerramento + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cFollowup + "'>";
    strTxt += "    <input type='hidden' name='dhEnvio' value='" + dhEnvio + "'>";
    //Chamado: 89940 - 01/08/2013 - Carlos Nunes
    strTxt += "    <input type='hidden' name='matm' value='" + nIdMatmCdManiftemp + "'>";
    strTxt += "    <input type='hidden' name='idFuncCdEncerramento' value='" + cFuncEncerramento + "'>";

    //Chamado: 94022 - 10/04/2014 - Carlos Nunes
    if (cFollowup == '0'){
	    strTxt += "    <td id=\"tdLixeira_" + nLinha + "\" class='principalLstPar' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' title='<bean:message key="prompt.excluir"/>' onclick=\"removeFollowup('" + nLinha + "')\"></td>";

	}else{
		if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_EXCLUSAO%>')){
			strTxt += "   <td id=\"tdLixeira_" + nLinha + "\" class='principalLstPar' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"removeFollowupBD('" + nLinha + "', '" + cFollowup + "', '" + cGerador + "')\"></td>";
		}else{
			strTxt += "    <td id=\"tdLixeira_" + nLinha + "\" class='principalLstPar' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' disabled=true class='geralImgDisable' onclick=\"removeFollowupBD('" + nLinha + "', '" + cFollowup + "', '" + cGerador + "')\"></td>";
		}
	}

    //Chamado: 89940 - 01/08/2013 - Carlos Nunes
    strTxt += "    <td id=\"tdResponsavel_" + nLinha + "\" class='principalLstParMao' width='17%' height='2' " + altTxt + ">" + acronymLst(nResponsavel, 14) + "&nbsp;</td>";
    strTxt += "    <td id=\"tdEvento_"      + nLinha + "\" class='principalLstParMao' width='20%' height='2' " + altTxt + ">" + acronymLst(nEvento, 19) + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtRegistro_"  + nLinha + "\" class='principalLstParMao' width='14%' height='2' " + altTxt + ">" + dRegistro + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtPrevisao_"  + nLinha + "\" class='principalLstParMao' width='14%' height='2' " + altTxt + ">" + dPrevisao + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtEfetivo_"   + nLinha + "\" class='principalLstParMao' width='14%' height='2' " + altTxt + ">" + dEfetivo + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtEnvio_"     + nLinha + "\" class='principalLstParMao' width='14%' height='2' " + altTxt + ">" + dhEnvio + "&nbsp;</td>";

    //Chamado: 89940 - 01/08/2013 - Carlos Nunes
	strTxt += "    <td id=\"tdImgEmail_"    + nLinha + "\" class='principalLstPar' width='3%' height='2'>";
    
    if(nIdMatmCdManiftemp > 0)
    {
    	strTxt += "<img class=\"geralCursoHand\" src=\"/plusoft-resources/images/email/msg-read.gif\" onclick=\"parent.visualizarFichaClassificador("+nIdMatmCdManiftemp+");\" />";
    }
    else
    {
    	strTxt += "&nbsp;";
    }

    strTxt += "</td>";
    
    if (cFollowup == '0'){
        //Chamado: 90705 - 18/10/2013 - Carlos Nunes
   		strTxt += "    <td id=\"tdDetalheFollowup_" + nLinha + "\" class='principalLstPar' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "&csNgtbFollowupFoupVo.foupDhEnvio=" + dhEnvio + "&csNgtbFollowupFoupVo.foupNrSequencia=" + cFollowup + "',window,'help:no;scroll:no;Status:NO;dialogWidth:825px;dialogHeight:375px,dialogTop:0px,dialogLeft:200px')\">";
    }else{
	    if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_CONSULTA%>')){
	        //Chamado: 90705 - 18/10/2013 - Carlos Nunes
	   		strTxt += "    <td id=\"tdDetalheFollowup_" + nLinha + "\" class='principalLstPar' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "&csNgtbFollowupFoupVo.foupDhEnvio=" + dhEnvio + "&csNgtbFollowupFoupVo.foupNrSequencia=" + cFollowup + "',window,'help:no;scroll:no;Status:NO;dialogWidth:825px;dialogHeight:375px,dialogTop:0px,dialogLeft:200px')\">";
	    }else{
	   		strTxt += "    <td id=\"tdDetalheFollowup_" + nLinha + "\" class='principalLstPar' width='3%' height='2'>";		    
	    }
    }
   
	strTxt += "      <img src='webFiles/images/botoes/pasta.gif' width='20' height='20' title='<bean:message key="prompt.hist�ricoFollowUp"/>' class='geralCursoHand'>";
    strTxt += "    </td>";
    strTxt += "	   <td id=\"tdFoupInEnvio_" + nLinha + "\" class='principalLstParMao' width='2%' height='2'> ";
    strTxt += "		 <input type='checkbox' name='foupInEnvio' "+ (inEnvio=="S"?"checked":"") +" value='S'>";
    strTxt += "    </td>";
    strTxt += "  </tr>";
    strTxt += "  <tr id=\"trEspaco_" + nLinha + "\">";
    strTxt += "    <td colspan='7' height='2'><img src='webFiles/images/separadores/pxTranp.gif' width='100%' height='1'></td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    strTxt += "</div>";


	//document.getElementById("lstFollowup").innerHTML += strTxt;
	document.getElementById("lstFollowup").insertAdjacentHTML("BeforeEnd", strTxt);	
	
	//Jonathan Costa - Corre��o google chrome - SerEducacional
	//Chamado: 89940 - 01/08/2013 - Carlos Nunes
	if (cFollowup == '<bean:write name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.foupNrSequencia" />' && '<bean:write name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.foupNrSequencia" />' != 0  ){
		editFollowup(cArea, cResponsavel, cEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, nLinha, nGerador, cGerador, inEncerramento, "", dhEnvio, nIdMatmCdManiftemp);
	}
	
	//Chamado: 94022 - 10/04/2014 - Carlos Nunes
	try{
		aposConfirmarFollowup(nLinha);
	}catch(e){}
}


function preencheHidden(text){
	document.getElementById('textFollowup').value=text;
}

//Chamado: 89940 - 01/08/2013 - Carlos Nunes
function alterFollowup(cArea, cResponsavel, nResponsavel, cEvento, nEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento, inEnvio, dhEnvio, cFuncEncerramento, nIdMatmCdManiftemp) {
	altTxt = "onclick=\"editFollowup_valida('" + cArea + "', '" + cResponsavel + "', '" + nResponsavel + "', '" + cEvento + "', '" + nEvento + "', '" + dRegistro + "', '" + dPrevisao + "', '" + dEfetivo + "', '" + tHistorico + "', '" + cFollowup + "', '" + altLinha + "', '" + nGerador + "', '" + cGerador + "', '" + inEncerramento + "', '" + inEnvio + "' , '" + dhEnvio + "' , '" + cFuncEncerramento + "', '"+ nIdMatmCdManiftemp + "')\"";
	
	strTxt = "";
    strTxt += "<table id=\"" + altLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr id=\"tr_" + altLinha + "\" >"; //Chamado: 94022 - 10/04/2014 - Carlos Nunes
    strTxt += "    <input type='hidden' name='area' value='" + cArea + "'>";
    strTxt += "    <input type='hidden' name='responsavel' value='" + cResponsavel + "'>";
    strTxt += "    <input type='hidden' name='evento' value='" + cEvento + "'>";
    strTxt += "    <input type='hidden' name='registro' value='" + dRegistro + "'>";
    strTxt += "    <input type='hidden' name='previsao' value='" + dPrevisao + "'>";
    strTxt += "    <input type='hidden' name='efetivo' value='" + dEfetivo + "'>";
    strTxt += "    <input type='hidden' name='historico' value='" + tHistorico + "'>";
    strTxt += "    <input type='hidden' name='gerador' value='" + cGerador + "'>";
    strTxt += "    <input type='hidden' name='encerramento' value='" + inEncerramento + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cFollowup + "'>";
    strTxt += "    <input type='hidden' name='dhEnvio' value='" + dhEnvio + "'>";
    //Chamado: 89940 - 01/08/2013 - Carlos Nunes
    strTxt += "    <input type='hidden' name='matm' value='" + nIdMatmCdManiftemp + "'>";
    strTxt += "    <input type='hidden' name='idFuncCdEncerramento' value='" + cFuncEncerramento + "'>";
    
    //Chamado: 94022 - 10/04/2014 - Carlos Nunes
    if (cFollowup == '0'){
	    strTxt += "    <td id=\"tdLixeira_" + altLinha + "\" class='principalLstPar' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeFollowup('" + altLinha + "')\"></td>";
	}else{
		if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_EXCLUSAO%>')){
	    	strTxt += "    <td id=\"tdLixeira_" + altLinha + "\" class='principalLstPar' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"removeFollowupBD('" + altLinha + "', '" + cFollowup + "', '" + cGerador + "')\"></td>";
		}else{
			strTxt += "    <td id=\"tdLixeira_" + altLinha + "\" class='principalLstPar' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' disabled=true class='geralImgDisable' onclick=\"removeFollowupBD('" + altLinha + "', '" + cFollowup + "', '" + cGerador + "')\"></td>";
		}
	}
		
	//Chamado: 89940 - 01/08/2013 - Carlos Nunes
    strTxt += "    <td id=\"tdResponsavel_" + altLinha + "\" class='principalLstParMao' width='17%' height='2' " + altTxt + ">" + acronymLst(nResponsavel, 14) + "&nbsp;</td>";
    strTxt += "    <td id=\"tdEvento_"      + altLinha + "\" class='principalLstParMao' width='20%' height='2' " + altTxt + ">" + acronymLst(nEvento, 19) + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtRegistro_"  + altLinha + "\" class='principalLstParMao' width='14%' height='2' " + altTxt + ">" + dRegistro + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtPrevisao_"  + altLinha + "\" class='principalLstParMao' width='14%' height='2' " + altTxt + ">" + dPrevisao + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtEfetivo_"   + altLinha + "\" class='principalLstParMao' width='14%' height='2' " + altTxt + ">" + dEfetivo + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtEnvio_"     + altLinha + "\" class='principalLstParMao' width='14%' height='2' " + altTxt + ">" + dhEnvio + "&nbsp;</td>";

    //Chamado: 89940 - 01/08/2013 - Carlos Nunes
    strTxt += "    <td id=\"tdImgEmail_"    + altLinha + "\" class='principalLstPar' width='3%' height='2'>";
    
    if(nIdMatmCdManiftemp > 0)
    {
    	strTxt += "<img class=\"geralCursoHand\" src=\"/plusoft-resources/images/email/msg-read.gif\" onclick=\"parent.visualizarFichaClassificador("+nIdMatmCdManiftemp+");\" />";
    }
    else
    {
    	strTxt += "&nbsp;";
    }

    strTxt += "</td>";
        
    if (cFollowup == '0'){
        //Chamado: 90705 - 18/10/2013 - Carlos Nunes
    	strTxt += "    <td id=\"tdDetalheFollowup_" + altLinha + "\" class='principalLstPar' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "&csNgtbFollowupFoupVo.foupDhEnvio=" + dhEnvio + "&csNgtbFollowupFoupVo.foupNrSequencia=" + cFollowup + "',window,'help:no;scroll:no;Status:NO;dialogWidth:825px;dialogHeight:375px,dialogTop:0px,dialogLeft:200px')\">";
    }else{
	    if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_CONSULTA%>')){
	        //Chamado: 90705 - 18/10/2013 - Carlos Nunes
	    	strTxt += "    <td id=\"tdDetalheFollowup_" + altLinha + "\" class='principalLstPar' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "&csNgtbFollowupFoupVo.foupDhEnvio=" + dhEnvio + "&csNgtbFollowupFoupVo.foupNrSequencia=" + cFollowup + "',window,'help:no;scroll:no;Status:NO;dialogWidth:825px;dialogHeight:375px,dialogTop:0px,dialogLeft:200px')\">";
	    }else{
	    	strTxt += "    <td id=\"tdDetalheFollowup_" + altLinha + "\" class='principalLstPar' width='3%' height='2'>";
	    }
    }
    
    strTxt += "      <img src='webFiles/images/botoes/pasta.gif' width='20' height='20' title='<bean:message key="prompt.hist�ricoFollowUp"/>' class='geralCursoHand'>";
    strTxt += "    </td>";
    strTxt += "	   <td id=\"tdFoupInEnvio_" + altLinha + "\" class='principalLstParMao' width='2%' height='2'> ";
    strTxt += "		 <input type='checkbox' name='foupInEnvio' "+ (inEnvio=="S"?"checked":"") +" value='S'>";
    strTxt += "    </td>";    
    strTxt += "  </tr>";
    strTxt += "  <tr id=\"trEspaco_" + altLinha + "\" >";
    strTxt += "    <td colspan='7' height='2'><img src='webFiles/images/separadores/pxTranp.gif' width='100%' height='1'></td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    strTxt += "</div>";

	removeFollowupAlt(altLinha);
	document.getElementById("lstFollowup" + altLinha).innerHTML += strTxt;
	//document.getElementById("lstFollowup" + altLinha).insertAdjacentHTML("BeforeEnd", strTxt);
	
	//Chamado: 94022 - 10/04/2014 - Carlos Nunes
	try{
		aposConfirmarFollowup(altLinha);
	}catch(e){}
}

function removeFollowup(nTblExcluir) {
	//Chamado: 94022 - 10/04/2014 - Carlos Nunes
	var retorno = false;
	try{
		retorno = antesRemoveFollowup(nTblExcluir);
		if(!retorno) return;
	}catch(e){}
	
	msg = '<bean:message key="prompt.alert.remov.foup" />';
	if (confirm(msg)) {
		objIdTbl = document.getElementById("lstFollowup" + nTblExcluir);
		lstFollowup.removeChild(objIdTbl);
		
		//Chamado: 94022 - 10/04/2014 - Carlos Nunes
		try{
			aposRemoveFollowup(nTblExcluir);
		}catch(e){}
	}
}

function removeFollowupAlt(nTblExcluir) {
	
	//Chamado: 94022 - 10/04/2014 - Carlos Nunes
	var retorno = false;
	try{
		retorno = antesRemoveFollowup(nTblExcluir);
		if(!retorno) return;
	}catch(e){}
	
	objIdTbl = document.getElementById(nTblExcluir);
	document.getElementById("lstFollowup" + nTblExcluir).removeChild(objIdTbl);
	
	//Chamado: 94022 - 10/04/2014 - Carlos Nunes
	try{
		aposRemoveFollowup(nTblExcluir);
	}catch(e){}
}

//Chamado: 89941 - 01/08/2013 - Carlos Nunes
function removeFollowupBD(nTblExcluir, cFollowup, cGerador) {
	//Chamado: 94022 - 10/04/2014 - Carlos Nunes
	var retorno = false;
	try{
		retorno = antesRemoveFollowup(nTblExcluir);
		if(!retorno) return;
	}catch(e){}
	
	if(validaRemoverFollowup(cGerador)){
		msg = '<bean:message key="prompt.alert.remov.foup" />';
		if (confirm(msg)) {
			objIdTbl = document.getElementById("lstFollowup" + nTblExcluir);
			lstFollowup.removeChild(objIdTbl);
			followupExcluidos.value += cFollowup + ";";
			estilo--;
			
			//Chamado: 94022 - 10/04/2014 - Carlos Nunes
			try{
				aposRemoveFollowup(nTblExcluir);
			}catch(e){}
		}
	}
}

//Chamado: 89941 - 01/08/2013 - Carlos Nunes
function validaRemoverFollowup(cGerador) {
	//Se for supervisor ou o Gerador do follow-up ele pode excluir.
	if(('<%=nivelSupervisor %>'=='<%=funcVo.getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() %>') || (cGerador=='<%=funcVo.getIdFuncCdFuncionario() %>')) {
		return true;
	} else {
		alert('<bean:message key="prompt.alert.remov.foupnaopermitido" />');
		return false;
	}
	
}

//Chamado: 89940 - 01/08/2013 - Carlos Nunes
function editFollowup(cArea, cResponsavel, cEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento, inEnvio, dhEnvio, nIdMatmCdManiftemp) {
	try{
		try {
			window.parent.atualizaCmbEvento(cEvento,cFollowup);
		} catch(e1){}

		parent.cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = false;
		parent.cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].disabled = false;
		parent.cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].disabled = false;
		parent.document.all["registro"].disabled = false;
		parent.document.all["dataPrevista"].disabled = false;
		parent.document.all["horaPrevista"].disabled = false;
		parent.document.all["efetivo"].disabled = false;
		parent.document.getElementsByName("textoHistorico")[0].readOnly = false;
		parent.document.all["encerramento"].disabled = false;
		parent.document.all["imgDataFoup"].disabled = false;

		var dataPrevisao = dPrevisao.substring(0, 10);
		var horaPrevisao = dPrevisao.substring(11, 16);
		parent.cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value = cArea;
		parent.cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = cResponsavel;
		parent.cmbArea.location.href="ManifestacaoFollowup.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CMB_AREA_FOLLOWUP_MANIF%>&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea=" + cArea + 
									 "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario=" + cResponsavel + 
									 "&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>" +
						 			 "&csNgtbFollowupFoupVo.foupNrSequencia=" + cFollowup;

		parent.document.all["inEnvioFollowup"].value = inEnvio;
		//parent.cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value = cEvento;
		parent.document.all["registro"].value = dRegistro;
		parent.document.all["dataPrevista"].value = dataPrevisao;
		parent.document.all["horaPrevista"].value = horaPrevisao;
		parent.document.all["efetivo"].value = dEfetivo;
		parent.document.getElementsByName("textoHistorico")[0].value = descodificaStringHtml(trataQuebraLinha2(tHistorico));
		parent.document.all["codigo"].value = cFollowup;
		parent.document.all["nomeGerador"].value = nGerador;
		parent.document.all["codigoGerador"].value = cGerador;
		parent.document.all["dhEnvio"].value = dhEnvio;

		if (inEncerramento == "true") {
		    parent.document.all["encerramento"].checked = true;
		} else {
			parent.document.all["encerramento"].checked = false;
		}
		parent.document.all["altLinha"].value = altLinha;

		if (inEncerramento == "true") {
			var dataEfetiva = dEfetivo.substring(0, 10);
			var horaEfetiva = dEfetivo.substring(11, 16);
			parent.document.all["txtDtEfetiva"].value = dataEfetiva;
			parent.document.all["txtHrEfetiva"].value = horaEfetiva;
		} else {
			parent.document.all["txtDtEfetiva"].value = "";
			parent.document.all["txtHrEfetiva"].value = "";
		}

        //Chamado: 89940 - 01/08/2013 - Carlos Nunes
		parent.document.all.item("matm").value = nIdMatmCdManiftemp;

		if (cFollowup != "" && cFollowup != "0") {
			parent.cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = true;
			desabilitaResponsavel();
			desabilitaArea();
			parent.cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].disabled = true;
			parent.document.all["registro"].disabled = true;
			parent.document.all["dataPrevista"].disabled = true;
			parent.document.all["imgDataFoup"].disabled = true;
			parent.document.all["horaPrevista"].disabled = true;
			parent.document.all["efetivo"].disabled = true;
			parent.document.getElementsByName("textoHistorico")[0].readOnly = true;
			parent.document.getElementsByName("textoComplemento")[0].readOnly = false;
			if (inEncerramento == "true") {
				parent.document.all["encerramento"].disabled = true;
				parent.document.getElementsByName("textoComplemento")[0].readOnly = true;
			}
		}
		
		parent.bNovoRegistro = false;
		
		//Chamado: 80505 - Carlos Nunes - 19/01/2012
		//se ja esta grava e editando, valida no permissionamento, se pode alterar a data de previsao
		if(cFollowup > 0){
			if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_ALTERACAO_DATAPREVISAO%>')){
				parent.document.all["dataPrevista"].disabled = false;
				parent.document.getElementById("imgDataFoup").disabled=false;
				parent.document.all["horaPrevista"].disabled = false;
			}
		}
	//Chamado: 89940 - 01/08/2013 - Carlos Nunes
	}catch(e){setTimeout("editFollowup('"+cArea+"', '"+cResponsavel+"', '"+cEvento+"', '"+dRegistro+"', '"+dPrevisao+"', '"+dEfetivo+"', '"+tHistorico+"', '"+cFollowup+"', '"+altLinha+"', '"+nGerador+"', '"+cGerador+"', '"+inEncerramento+"', '"+inEnvio + "','" + dhEnvio + "','" + nIdMatmCdManiftemp +"')",1000);}
	
}

function desabilitaResponsavel() {
	try {
		parent.cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].disabled = true;
	} catch(e) {
		setTimeout("desabilitaResponsavel()", 200);
	}
}

function desabilitaArea() {
	try {
		parent.cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = true;
	} catch(e) {
		setTimeout("desabilitaArea()", 200);
	}
}

function validaPermissaoEdicaoFoup(cArea, cResponsavel, nResponsavel, cEvento, nEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento){
	var idFunc = '<%=funcVo.getIdFuncCdFuncionario() %>';
	
	//valida somente se o followup ja estiver gravado.
	if(cFollowup > 0){
		//se nao tem permissao para editar followup
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_ALTERACAO%>')){
			return false;
		}
		
		//se nao tem permissao de alterar followup concluido, valida a data efetvo
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_ALTERACAO_CONCLUIDO%>')){
			// Se o follow-up j� tiver sido conclu�do n�o permite edi��o.
			if(dEfetivo != ''){
				return false;
			}
		}
	}
	
	
	//Chamado: 86688 - 11/03/2013 - Carlos Nunes
	// Se for supervisor permite edi��o
	<%if(seguracaFollowup){%>
		if('<%=nivelSupervisor %>'=='<%=funcVo.getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() %>') 
		{
			return true;
		}
		
		// Se for o Respons�vel...
		if(cResponsavel==idFunc)
		{ 
			return true;
		}
		
		// Se for o gerador...
		if(cGerador==idFunc)
		{
			return true;
		}
			
		// Se tiver permiss�o de Resolver na �rea Resolvedora que � respons�vel pelo Follow-Up, permite edi��o...
		for(i=0;i<funcResolvedor.length;i++){
			if(cResponsavel==funcResolvedor[i]){
				return true;
			}
		}
		
		// Se n�o for nada disso...
		return false;
	<%}else{%>
	    return true;
	<%}%>
}

//Chamado: 89940 - 01/08/2013 - Carlos Nunes
function editFollowup_valida(cArea, cResponsavel, nResponsavel, cEvento, nEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento, inEnvio, dhEnvio, cFuncEncerramento, nIdMatmCdManiftemp) {
	if(validaPermissaoEdicaoFoup(cArea, cResponsavel, nResponsavel, cEvento, nEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento, inEnvio, dhEnvio)) {
		editFollowup(cArea, cResponsavel, cEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento, inEnvio, dhEnvio, nIdMatmCdManiftemp);
	} else {
		if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_CONSULTA%>')){
			preencheHidden(tHistorico);
			//Chamado: 90705 - 18/10/2013 - Carlos Nunes
			showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.foupTxHistorico=' + tHistorico + '&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=' + nGerador + '&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=' + nEvento + '&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=' + nResponsavel + '&csNgtbFollowupFoupVo.foupDhRegistro=' + dRegistro + '&csNgtbFollowupFoupVo.foupDhPrevista=' + dPrevisao + '&csNgtbFollowupFoupVo.foupDhEfetiva=' + dEfetivo + '&csNgtbFollowupFoupVo.foupDhEnvio=' + dhEnvio + '&csNgtbFollowupFoupVo.foupNrSequencia=' + cFollowup ,window,'help:no;scroll:no;Status:NO;dialogWidth:825px;dialogHeight:375px,dialogTop:0px,dialogLeft:200px');	
		}
	}
}

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<input type="hidden" name="followupExcluidos">
<input type="hidden" id="textFollowup" name="textFollowup">
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="principalLstCab" width="18%">&nbsp;<bean:message key="prompt.responsavel" /></td>
    <td class="principalLstCab" width="19%"><bean:message key="prompt.evento" /></td>
    <td class="principalLstCab" width="13%"><bean:message key="prompt.registro" /></td>
    <td class="principalLstCab" width="14%"><bean:message key="prompt.previsao" /></td>
    <td class="principalLstCab" width="12%"><bean:message key="prompt.efetivo" /></td>
    <td class="principalLstCab" width="18%"><bean:message key="prompt.dataenvio" /></td>
    <td class="principalLstCab" width="3%"><img src="webFiles/images/icones/email.gif" width="25" title="<bean:message key="prompt.email"/>"></td>
    <td width="1%"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td height="48" colspan="7"> 
      <div id="lstFollowup" style="width:100%; height:110; overflow: auto"> 
        <!--Inicio Lista Historico -->
		<logic:present name="csNgtbFollowupFoupVector">
          <logic:iterate id="cnffVector" name="csNgtbFollowupFoupVector">
            <script language="JavaScript">
              var val = "<bean:write name="cnffVector" property="foupTxHistorico" />";
            
			  addFollowup('<bean:write name="cnffVector" property="csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea" />',
						'<bean:write name="cnffVector" property="csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario" />',
						'<bean:write name="cnffVector" property="csCdtbFuncResponsavelFuncVo.funcNmFuncionario" />', 
						'<bean:write name="cnffVector" property="csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup" />', 
						'<bean:write name="cnffVector" property="csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup" />', 
						'<bean:write name="cnffVector" property="foupDhRegistro" />', 
						'<bean:write name="cnffVector" property="foupDhPrevista" />', 
						'<bean:write name="cnffVector" property="foupDhEfetiva" />', 
						val, 
						'<bean:write name="cnffVector" property="foupNrSequencia" />',
						'<bean:write name="cnffVector" property="csCdtbFuncGeradorFuncVo.funcNmFuncionario" />',
						'<bean:write name="cnffVector" property="csCdtbFuncGeradorFuncVo.idFuncCdFuncionario" />',
						'<bean:write name="cnffVector" property="foupDhEfetiva" />'!=''?'true':'false',
						'<bean:write name="cnffVector" property="foupInEnvio" />',
						'<bean:write name="cnffVector" property="foupDhEnvio" />',
						'<bean:write name="cnffVector" property="csCdtbFuncEncerramentoFuncVo.idFuncCdFuncionario" />',
						'<bean:write name="cnffVector" property="foupCdManiftemp" />'); //Chamado: 89940 - 01/08/2013 - Carlos Nunes
	        </script>
          </logic:iterate>
        </logic:present>
        <!--Final Lista Historico -->
      </div>
    </td>
  </tr>
</table>
</body>
</html>