<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbEnvolvTercReclEntrVo" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmLstManifTerceiros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
nLinha = new Number(0);
estilo = new Number(0);

function addTerceiro(cTerceiro, nTerceiro, data, tDescricao, banco) {
	
	if (comparaChave(cTerceiro)) {
		return false;
	}
	
	nLinha = nLinha + 1;
	estilo++;
	
	altTxt = "onclick=\"editTerceiro('" + cTerceiro + "', '" + nTerceiro + "', '" + data + "', '" + tDescricao + "', '" + banco + "', '" + nLinha + "')\"";

	strTxt = "";
    strTxt += "<div id=\"lstTerceiro" + nLinha + "\" style='width:100%; height:0px; z-index:1; overflow: auto'>";
    strTxt += "<table id=\"" + nLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
    strTxt += "    <input type='hidden' name='terceiro' value='" + cTerceiro + "'>";
    strTxt += "    <input type='hidden' name='data' value='" + data + "'>";
    strTxt += "    <input type='hidden' name='descricao' value='" + tDescricao + "'>";
    if (banco == false)
	    strTxt += "    <td class='principalLstParMao' width='2%'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeTerceiro('" + nLinha + "')\"></td>";
	else
	    strTxt += "    <td class='principalLstParMao' width='2%'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeTerceiroBD('" + nLinha + "', '" + cTerceiro + "')\"></td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='78%'>" + acronymLst(nTerceiro, 45) + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='20%'>" + data + "&nbsp;</td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    strTxt += "</div>";

	document.getElementsByName("lstTerceiro").innerHTML += strTxt;
}

function alterTerceiro(cTerceiro, cTerceiroOld, nTerceiro, data, tDescricao, banco, altLinha) {

	if (cTerceiro != cTerceiroOld) {
		if (comparaChave(cTerceiro)) {
			return false;
		}
	}

	altTxt = "onclick=\"editTerceiro('" + cTerceiro + "', '" + nTerceiro + "', '" + data + "', '" + tDescricao + "', '" + banco + "', '" + altLinha + "')\"";

	strTxt = "";
    strTxt += "<table id=\"" + nLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
    strTxt += "    <input type='hidden' name='terceiro' value='" + cTerceiro + "'>";
    strTxt += "    <input type='hidden' name='data' value='" + data + "'>";
    strTxt += "    <input type='hidden' name='descricao' value='" + tDescricao + "'>";
    if (banco == false)
	    strTxt += "    <td class='principalLstParMao' width='2%'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeTerceiro('" + altLinha + "')\"></td>";
	else
	    strTxt += "    <td class='principalLstParMao' width='2%'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeTerceiroBD('" + altLinha + "', '" + cTerceiro + "')\"></td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='78%'>" + acronymLst(nTerceiro, 45) + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='20%'>" + data + "&nbsp;</td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    
	removeTerceiroAlt(altLinha);
	document.getElementsByName("lstTerceiro" + altLinha).innerHTML += strTxt;
}

function removeTerceiro(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.item" />';
	if (confirm(msg)) {
		objIdTbl = lstTerceiro.getElementById("lstTerceiro" + nTblExcluir);
		lstTerceiro.removeChild(objIdTbl);
		estilo--;
	}
}

function removeTerceiroAlt(nTblExcluir) {
	objIdTbl = eval("lstTerceiro" + nTblExcluir).getElementById(nTblExcluir);
	eval("lstTerceiro" + nTblExcluir).removeChild(objIdTbl);
}

function removeTerceiroBD(nTblExcluir, cTerceiro) {
	msg = '<bean:message key="prompt.alert.remov.item" />';
	if (confirm(msg)) {
		objIdTbl = lstTerceiro.getElementById("lstTerceiro" + nTblExcluir);
		lstTerceiro.removeChild(objIdTbl);
		tercExcluidos.value += cTerceiro + ";";
		estilo--;
	}
}

function editTerceiro(cTerceiro, nTerceiro, data, tDescricao, banco, altLinha) {
	window.parent.envolvimentoTerceirosForm.data.value = data;
	window.parent.envolvimentoTerceirosForm.descricao.value = trataQuebraLinha2(tDescricao);
	window.parent.cmbTerceiros.location = "EnvolvimentoTerceiros.do?acao=showAll&tela=cmbTerceiros&cmbIdTptrCdTpTercRecl=" + cTerceiro + "&banco=" + banco;
	window.parent.envolvimentoTerceirosForm.banco.value = banco;
	window.parent.envolvimentoTerceirosForm.altLinha.value = altLinha;
}

function comparaChave(cTerceiro) {
	try {
		if (terceiro.length != undefined) {
			for (var i = 0; i < terceiro.length; i++) {
				if (terceiro[i].value == cTerceiro) {
					alert('<bean:message key="prompt.alert.terc.exist" />');
					return true;
				}
			}
		} else {
			if (terceiro.value == cTerceiro) {
				alert('<bean:message key="prompt.alert.terc.exist" />');
				return true;
			}
		}
	} catch (e){}
	return false;
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<input type="hidden" name="tercExcluidos">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="2%" class="principalLstCab">&nbsp;</td>
    <td width="78%" class="principalLstCab"><bean:message key="prompt.terceiros" /></td>
    <td width="20%" class="principalLstCab"><bean:message key="prompt.data" /></td>
  </tr>
  <tr valign="top"> 
    <td class="principalLstPar" colspan="3" height="100%">
	  <div id="lstTerceiro" style="position:absolute; width:100%; height:0px; z-index:1">
		  <logic:present name="csNgtbEnvolvTercReclEntrVector">
			  <logic:iterate name="csNgtbEnvolvTercReclEntrVector" id="cnetreVector">
		        <script language="JavaScript">
				  addTerceiro('<bean:write name="cnetreVector" property="csCdtbTpTercReclTptrVo.idTptrCdTpTercRecl" />',
							'<bean:write name="cnetreVector" property="csCdtbTpTercReclTptrVo.tptrDsTpTercRecl" />',
							'<bean:write name="cnetreVector" property="entrDhInicio" />', 
							'<bean:write name="cnetreVector" property="entrDsHistorico" />', 
							true);
		        </script>
			  </logic:iterate>
		  </logic:present>
	  </div>
    </td>
  </tr>
</table>
</body>
</html>