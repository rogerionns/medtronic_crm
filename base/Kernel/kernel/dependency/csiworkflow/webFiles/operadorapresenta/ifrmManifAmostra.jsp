<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>

<plusoft:include  id="funcoesManif" href='<%=fileInclude%>'/>
<bean:write name="funcoesManif" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%><html>
<head>
<title>..: <bean:message key="prompt.amostratitulo" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

var bEnvia = true;

function submeteForm() {
	if (!bEnvia) {
		return false;
	}
	if (validaCampos()) {

		//chamada de funcao espec
		var retorno = false;
		try{
			retorno = antesGravarAmostra();
			if(!retorno) return;
		}catch(e){}
		
		bEnvia = false;
		document.reclamacaoManiForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
		document.reclamacaoManiForm.tela.value = '<%=MCConstantes.TELA_AMOSTRA%>';
		document.reclamacaoManiForm.target = this.name = 'amostra';
		document.reclamacaoManiForm.submit();
		document.all.item('aguarde').style.visibility = 'visible';
	}
}

function submeteReset() {
	document.reclamacaoManiForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	document.reclamacaoManiForm.tela.value = '<%=MCConstantes.TELA_AMOSTRA%>';
	document.reclamacaoManiForm.target = this.name = 'amostra';
	document.reclamacaoManiForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';
}

function validaCampos() {
	if (reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.remaDhSaidaAmostra"].value != "")
		if (!verificaData(reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.remaDhSaidaAmostra"]))
			return false;
	if (reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.remaDhRetiradaAmostra"].value != "")
		if (!verificaData(reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.remaDhRetiradaAmostra"]))
			return false;
	if (reclamacaoManiForm.remaHrRetiradaAmostra.value != "") {
		if (reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.remaDhRetiradaAmostra"].value == "") {
			alert('<bean:message key="prompt.alert.texto.reti" />');
			reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.remaDhRetiradaAmostra"].focus();
			return false;
		} else if (!verificaHora(reclamacaoManiForm.remaHrRetiradaAmostra))
			return false;
	}
	if (reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.remaDhRetornoAmostra"].value != "")
		if (!verificaData(reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.remaDhRetornoAmostra"]))
			return false;
	return true;
}

function abreEndTroca() {
	var chamado = reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
	var manifestacao = reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
	var asn1 = reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	var asn2 = reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	url = 'ReclamacaoMani.do?acao=showAll&tela=endTroca&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + asn1 + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + asn2;
	return url;
}

//alteracao enviada pelo varandas
function inicio(){

    showError('<%=request.getAttribute("msgerro")%>');
    document.all.item('aguarde').style.visibility = 'hidden'

    try{
          onloadEspecAmostra();
    }catch(e){}

}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="principalBgrPageIFRM" leftmargin="0" onload="inicio();">
<html:form action="/ReclamacaoMani.do" styleId="reclamacaoManiForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<html:hidden property="csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniTxManifestacao" />
<html:hidden property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />

<html:hidden property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero"/>
<html:hidden property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.amostra" /> </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center" valign="top"> <br>
            <table width="99%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" width="60%"><%=getMessage("prompt.assuntoNivel1", request)%></td>
                <td class="principalLabel" colspan="2" width="40%"><bean:message key="prompt.reclamacao" /></td>
              </tr>
              <tr> 
                <td width="60%"> 
                  <input type="text" name="prasDsProdutoAssunto" class="principalObjForm" value="<%=request.getParameter("prasDsProdutoAssunto")%>" readonly="readonly">
                 	<script>
	    				if(window.dialogArguments.top.name == 'Chamado'){
	                  		reclamacaoManiForm.prasDsProdutoAssunto.value = window.dialogArguments.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto"].value;
						}
					</script>
                </td>
                <td colspan="2" width="40%"> 
                  <input type="text" name="tpmaDsTpManifestacao" class="principalObjForm" value="<%=request.getParameter("tpmaDsTpManifestacao")%>" readonly="readonly">
                	<script>
	    				if(window.dialogArguments.top.name == 'Chamado'){
							reclamacaoManiForm.tpmaDsTpManifestacao.value = window.dialogArguments.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao"].value;
						}
					</script>
                </td>
              </tr>
              <tr> 
                <td colspan="3" class="principalLabel"><bean:message key="prompt.historicoreclamacao" /></td>
              </tr>
              <tr> 
                <td colspan="3"> 
                  <textarea name="maniTxManifestacao" class="principalObjForm" rows="4" readonly="readonly"></textarea>
					<script>
		  				if(window.dialogArguments.top.name == 'Chamado'){
	                  		reclamacaoManiForm.maniTxManifestacao.value = window.dialogArguments.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value;
						}
					</script>
                </td>
              </tr>
              <tr> 
                <td colspan="3"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" width="31%" height="13"><bean:message key="prompt.prestadorservico" /></td>
                      <td class="principalLabel" height="13" colspan="2"><bean:message key="prompt.datasaida" /></td>
                      <td class="principalLabel" height="13" colspan="2"><bean:message key="prompt.dataretirada" /></td>
                      <td class="principalLabel" height="13" colspan="2"><bean:message key="prompt.horaretirada" /></td>
                    </tr>
                    <tr> 
                      <td width="31%" height="23">
						<html:select property="csNgtbReclamacaoManiRemaVo.csCdtbPrestadorServicoPrseVo.idPrseCdPrestadorServico" styleClass="principalObjForm">
						  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						  <logic:present name="csCdtbPrestadorServicoPrseVector">
							<html:options collection="csCdtbPrestadorServicoPrseVector" property="idPrseCdPrestadorServico" labelProperty="prseDsPrestadorServico"/>
						  </logic:present>
						</html:select>  
					  </td>
                      <td width="11%"> 
                        <html:text property="csNgtbReclamacaoManiRemaVo.remaDhSaidaAmostra" styleClass="principalObjForm" onkeypress="validaDigito(this, event)" maxlength="10"  readonly="true" />
                      </td>
                      <td width="4%"><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('reclamacaoManiForm[\'csNgtbReclamacaoManiRemaVo.remaDhSaidaAmostra\']');"></td>
                      <td width="12%"> 
                        <html:text property="csNgtbReclamacaoManiRemaVo.remaDhRetiradaAmostra" styleClass="principalObjForm" onkeypress="validaDigito(this, event)" maxlength="10" readonly="true" />
                      </td>
                      <td width="4%"><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('reclamacaoManiForm[\'csNgtbReclamacaoManiRemaVo.remaDhRetiradaAmostra\']');"></td>
                      <td width="15%"> 
                        <html:text property="remaHrRetiradaAmostra" styleClass="principalObjForm" onkeypress="validaDigitoHora(this, event)" maxlength="5" />
                      </td>
                      <td width="4%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td colspan="5" class="principalLabel"><bean:message key="prompt.recebidopor" /></td>
                      <td class="principalLabel" colspan="2"><bean:message key="prompt.dataretornoamostra" /></td>
                    </tr>
                    <tr> 
                      <td colspan="5"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="94%"> 
                              <html:text property="csNgtbReclamacaoManiRemaVo.remaNmAtendidoAmostra" styleClass="principalObjForm" maxlength="60" />
                            </td>
                            <td width="5%">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                      <td width="15%"> 
                        <html:text property="csNgtbReclamacaoManiRemaVo.remaDhRetornoAmostra" styleClass="principalObjForm" onkeypress="validaDigito(this, event)" maxlength="10" />
                      </td>
                      <td width="4%"><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('reclamacaoManiForm[\'csNgtbReclamacaoManiRemaVo.remaDhRetornoAmostra\']');"></td>
                    </tr>
                    <tr> 
                      <td colspan="7"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalLabel" width="21%"><bean:message key="prompt.formaressarcimento" /></td>
                            <td class="principalLabel" width="25%"><bean:message key="prompt.formaenvioamostra" /></td>
                            <td class="principalLabel" colspan="2"><bean:message key="prompt.valorressarcimento" /></td>
                            <td width="20%">&nbsp;</td>
                          </tr>
                          <tr> 
                            <td width="21%" height="2">
							  <html:select property="csNgtbReclamacaoManiRemaVo.csCdtbTipoRessarciTpreVo.idTpreCdTiporessarci" styleClass="principalObjForm">
							    <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							    <logic:present name="csCdtbTipoRessarciTpreVector">
							      <html:options collection="csCdtbTipoRessarciTpreVector" property="idTpreCdTiporessarci" labelProperty="tpreDsTiporessarci"/>
								</logic:present>
							  </html:select>  
                            </td>
                            <td width="25%" height="2">
							  <html:select property="csNgtbReclamacaoManiRemaVo.csCdtbTpEnvioAmostraTpeaVo.idTpeaCdTpEnvioAmostra" styleClass="principalObjForm">
							    <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							    <logic:present name="csCdtbTpEnvioAmostraTpeaVector">
							      <html:options collection="csCdtbTpEnvioAmostraTpeaVector" property="idTpeaCdTpEnvioAmostra" labelProperty="tpeaDsTpEnvioAmostra"/>
								</logic:present>
							  </html:select>  
							</td>
                            <td width="25%" height="2"> 
                              <html:text property="csNgtbReclamacaoManiRemaVo.remaVlRessarcAmostra" styleClass="principalObjForm" maxlength="13" />
                            </td>
                            <td width="8%" height="2">&nbsp;</td>
                            <td width="20%" height="2" align="center"><img src="webFiles/images/botoes/bt_endTroca.gif" width="118" height="22" class="geralCursoHand" onclick="showModalDialog(abreEndTroca(),0,'help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:290px,dialogTop:0px,dialogLeft:200px')"></td>
                          </tr>
                          <tr> 
                            <td colspan="5" class="principalLabel">
                              Observação
                            </td>
                          </tr>
                          <tr> 
                            <td colspan="5">
                              <html:textarea property="csNgtbReclamacaoManiRemaVo.remaTxAmostra" styleClass="principalObjForm3D" rows="3" onkeyup="textCounter(this, 4000)" onblur="textCounter(this, 4000)" />
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
	        <table border="0" cellspacing="0" cellpadding="4" align="right">
	          <tr> 
	            <td> 
	              <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar" />" class="geralCursoHand" onclick="submeteForm()"></div>
	            </td>
	            <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar" />" class="geralCursoHand" onclick="submeteReset()"></td>
	          </tr>
	        </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
<div id="aguarde" style="position:absolute; left:250px; top:100px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>

<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>