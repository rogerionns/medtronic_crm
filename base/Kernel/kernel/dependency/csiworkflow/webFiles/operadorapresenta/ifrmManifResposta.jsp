<%@page import="br.com.plusoft.csi.crm.helper.MCConstantes"%>
<%@ page language="java" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);			
	long idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();


	String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>

<html>
<head>
<title><bean:message key="prompt.resposta" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<plusoft:include id="funcoesManif" href='<%=fileInclude%>'/>
<bean:write name="funcoesManif" filter="html"/>

</head>

<script language="JavaScript">
	var win = (window.dialogArguments)?window.dialogArguments:window.opener;
	
	function executarIntegracao(){
		var idChamCdChamado = manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value;
		var maniNrSequencia = manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value;
		var idAsn1CdAssuntoNivel1 = manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value;
		var idAsn2CdAssuntoNivel2 = manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
		var idEtprCdEtapaprocesso = manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.idEtprCdEtapaProcesso"].value;
		var idPessCdPessoa = manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa"].value;
		var idFuncCdFuncionario = manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value;
		var idMadsNrSequencial = manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.idMadsNrSequencial"].value;
		var idEmprCdEmpresa = <%=idEmprCdEmpresa%>;
		var idOporCdOportunidade = manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.idOporCdOportunidade"].value;
		
		showModalDialog("/csicrm/ChamaIntegracao.do"+
			"?idChamCdChamado="+ idChamCdChamado +
			"&maniNrSequencia="+ maniNrSequencia +
			"&idAsn1CdAssuntoNivel1="+ idAsn1CdAssuntoNivel1 +
			"&idAsn2CdAssuntoNivel2="+ idAsn2CdAssuntoNivel2 +
			"&idEtprCdEtapaprocesso="+ idEtprCdEtapaprocesso +
			"&idPessCdPessoa=" + idPessCdPessoa +
			"&idMadsNrSequencial=" + idMadsNrSequencial +
			"&idEmprCdEmpresa=" + idEmprCdEmpresa +
			"&idOporCdOportunidade="+ idOporCdOportunidade +
			"&idFuncCdFuncionario="+ idFuncCdFuncionario, window, "help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:150px,dialogTop:0px,dialogLeft:200px");
			
		document.getElementById("tdIntegracao1").style.display = "none";
		document.getElementById("tdIntegracao2").style.display = "none";
	}
	
	function inicio(){
		showError('<%=request.getAttribute("msgerro")%>');

		if(manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.madsInSucessointegracao"].value == "N"){
			document.getElementById("tdIntegracao1").style.display = "block";
			document.getElementById("tdIntegracao2").style.display = "block";
		}

		if(document.forms[0]['csAstbManifestacaoDestMadsVo.idEtprCdEtapaProcesso'].value > 0) {
			document.getElementById("trTituloEtapa").style.display = 'block';
		}

		//Para exibir o bot�o de checklist precisa:
	  	//1 - O tipo da manifesta��o estar associado a um desenho de processo
	  	//2 - Na etapa da �rea respons�vel, dever� ter um checklist associado
		if(win.parent.manifestacaoForm["csAstbManifestacaoDestMadsVo.idChliCdChecklist"].value > 0 && win.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso"].value > 0) {
			document.getElementById("divCheckList").style.display = "block";
		}
	}

	function abrirDetalhesCheckList(desabilitaCampos){
		document.forms[0].desabilitaCamposPopupChecklist.value = desabilitaCampos;

		showModalDialog("ManifestacaoDestinatario.do"+
						"?acao=showAll"+
						"&tela=popUpChecklist"+
						"&csAstbManifestacaoDestMadsVo.idChliCdChecklist="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.idChliCdChecklist"].value +
						"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value +
						"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value +
						"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value +
						"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value +
						"&csAstbManifestacaoDestMadsVo.idMadsNrSequencial="+  manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.idMadsNrSequencial"].value + 
						"&csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario=" + manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value, window, "help:no;scroll:no;Status:NO;dialogWidth:720px;dialogHeight:350px,dialogTop:0px,dialogLeft:200px");
	}

</script>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();">
<html:form action="/ManifestacaoDestinatario.do" styleId="manifestacaoDestinatarioForm">

	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.idEtprCdEtapaProcesso" />
	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa" />
	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.idMadsNrSequencial" />
	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />
	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInSucessointegracao" />
	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.idOporCdOportunidade" />
	<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.idChliCdChecklist" />

	<input type="hidden" name="desabilitaCamposPopupChecklist" />

	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
		<tr> 
			<td width="1007" colspan="2"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr> 
						<td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.resposta" /></td>
						<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
						<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr> 
			<td class="principalBgrQuadro" valign="top"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
					<tr> 
						<td valign="top" height="1"> 
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr> 
									<td> 
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr> 
												<td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr> 
									<td class="principalLabel" colspan="2" height="107">
										<html:textarea name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsTxResposta" styleClass="principalObjForm3D" readonly="true" rows="7" />
										<script>
											var indice = new Number(win.document.getElementsByName('linhaRespostaClicada').value);
											var texto = win.document.all('resposta')[indice].value;
											texto = descodificaStringHtml(texto);
											//while(texto.indexOf("QBRLNH")> -1){
											//	texto = texto.replace('QBRLNH','\n');
											//}
											//while(texto.indexOf("&quot")> -1){
											//	texto = texto.replace('&quot','\"');
											//}
											//Chamado: 91219 - 10/10/2013 - Carlos Nunes
											if(texto != 'null')
											{
												document.getElementsByName('csAstbManifestacaoDestMadsVo.madsTxResposta')[0].value = texto;
											}
										</script>
									</td>
								</tr>
								<tr> 
									<td class="principalLabel" colspan="2">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr> 
												<td width="15%" class="principalLabel" align="right"> 
													<bean:message key="prompt.destinatario" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
												</td>
												<td> 
													<html:text style="height:20px; width:546px;" name="manifestacaoDestinatarioForm" property="areaDestinatario" styleClass="principalObjForm" readonly="true" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr> 
									<td class="principalLabel" colspan="2">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr> 
												<td width="13%" class="principalLabel" align="right"> 
													<bean:message key="prompt.enviadoem" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
												</td>
												<td width="19%"> 
													<html:text name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsDhEnvio" styleClass="principalObjForm" readonly="true" /><!-- @@ -->
												</td>
												<td width="15%" class="principalLabel" align="right"> 
													<bean:message key="prompt.respondidoem" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">  
												</td>
												<td> 
													<html:text name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsDhResposta" readonly="true" styleClass="principalObjForm" />
												</td>
											</tr>
											<tr>
												<td width="13%" class="principalLabel" align="right"> 
													<bean:message key="prompt.dataprevisao" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">  
												</td>
												<td> 
													<html:text style="height:20px;" name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsDhPrevisao" styleClass="principalObjForm" readonly="true" />
												</td>
												
												<logic:notEmpty name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo">
							                        <td width="15%" class="principalLabel" align="right"> 
														<bean:message key="prompt.respondidoNo" />
														<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
							                        </td>
							                        <td id="tdRespondidoNo" class="principalLabel"><input type="text" id="madsInModulo" class="principalObjForm" readonly="true" /></td>
							                        <script>
							                        	if('<bean:write name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo"/>' == '<%=MCConstantes.RESPONDIDO_CHAMADO%>'){
							                        		document.getElementById("madsInModulo").value = '<bean:message key="prompt.respondidoChamado" />';
							                        	}else if('<bean:write name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo"/>' == '<%=MCConstantes.RESPONDIDO_WORKFLOW%>'){
							                        		document.getElementById("madsInModulo").value = '<bean:message key="prompt.respondidoWorkflow" />';
							                        	}else if('<bean:write name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo"/>' == '<%=MCConstantes.RESPONDIDO_EMAIL%>'){
							                        		document.getElementById("madsInModulo").value = '<bean:message key="prompt.respondidoEmail" />';
							                        	}else if('<bean:write name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo"/>' == '<%=MCConstantes.RESPONDIDO_MESA_CHAMADO%>'){
							                        		document.getElementById("madsInModulo").value = '<bean:message key="prompt.respondidoMesaChamado" />';
							                        	}else if('<bean:write name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo"/>' == '<%=MCConstantes.RESPONDIDO_MESA_WORKFLOW%>'){
							                        		document.getElementById("madsInModulo").value = '<bean:message key="prompt.respondidoMesaWorkflow" />';
							                        	}
							                        </script>
						                        </logic:notEmpty>
											</tr>
										</table>
									</td>
								</tr>
								<tr id="trTituloEtapa" style="display: none"> 
									<td class="principalLabel" colspan="2">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr> 
												<td width="13%" class="principalLabel" align="right"> 
													<bean:message key="prompt.TitulodaEtapa" />
													<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
												</td>
												<td width="67%"> 
													<html:text name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.etprDsTituloetapa" styleClass="principalObjForm" readonly="true" />
												</td>
												<td width="20%" align="right" class="principalLabel">
													<div id="divCheckList">
														<bean:message key="prompt.checkList"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
			                							<img src="webFiles/images/botoes/CheckList4.gif" align="middle" style="cursor: pointer;" onclick="abrirDetalhesCheckList(true);"/>
			                						</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr height="5px;"><td colspan="1">&nbsp;</td></tr><tr>
							</table>
						</td>
						<td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
					</tr>
					<tr height=1> 
						<td valign="bottom" width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
						<td valign="bottom" width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<!-- Chamado: 90705 - 18/10/2013 - Carlos Nunes -->
   <logic:present name="historicoCobrancaVector">
   		
   		<table width="100%" border="0" cellspacing="0" cellpadding="0">
   		    <tr>
         	 	<td class="pLC" width="20%">&nbsp;<bean:message key="prompt.dataescalonamento"/></td>
         	 	<td class="pLC" width="39%"><bean:message key="prompt.superiorresponsavel"/></td>
         	 	<td class="pLC" width="41%"><bean:message key="prompt.email"/></td>
         	 </tr>         	
         </table>
         
        <div id="lstCobranca" style="width:100%; height:50px; overflow: scroll; visibility: visible"> 
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         	   <logic:iterate id="cobrVector" name="historicoCobrancaVector" indexId="indice">
	         	 <tr class="intercalaLst<%=indice.intValue()%2%>">
	         	 	<td class="pLP" width="20%">&nbsp;<bean:write name="cobrVector" property="field(cope_dh_data)" format='dd/MM/yyyy HH:mm:ss' locale='org.apache.struts.action.LOCALE' filter='html'/></td>
	         	 	<td class="pLP" width="40%">&nbsp;<bean:write name="cobrVector" property="acronymHTML(func_nm_funcionario,35)" filter="html"/></td>
	         	 	<td class="pLP" width="40%">&nbsp;<bean:write name="cobrVector" property="acronymHTML(func_ds_mail,40)" filter="html" /></td>
	         	 </tr>
	           </logic:iterate>
	         </table>
		</div>
         
         
         <script> window.dialogHeight='350px';</script>
   </logic:present>
   
	<table border="0" cellspacing="0" cellpadding="4" align="right">
		<tr>
			<td id="tdIntegracao1" class="principalLabel" valign="middle" style="display: none;"><bean:message key="prompt.integracao"/></td>
			<td id="tdIntegracao2" class="principalLabel" valign="middle" width="100" style="display: none;">
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7" align="middle">
				<img src="webFiles/images/botoes/integracao.gif" width="25" height="25" border="0" title="<bean:message key="prompt.integracao"/>" onClick="executarIntegracao();" class="geralCursoHand"></td> 
			<td>
				<img id="btnOut" src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
		</tr>
	</table>
	
	

<div id="divFuncaoExtraDestinatarioManif" style="position: absolute; visibility: hidden; left: 20px; " class="principalLabel">
	<img src="/plusoft-resources/images/botoes/Alterar.gif" title="<bean:message key='prompt.historico'/>" style="vertical-align: middle; cursor: pointer;  " onclick="onClickHistoricoManifestacaoDestinatario()" />
</div>
<script type="text/javascript">
	try {
		document.getElementById("divFuncaoExtraDestinatarioManif").style.top = document.getElementById("btnOut").style.top;
		onLoadManifestacaoDestinatario();
	} catch(e) {} 
</script>
</html:form>

</body>
<script>
	// Adicionado controle para n�o distorcer a tela, ele deixa no maximo 20 caracteres
	//if(document.all["areaDestinatario"].value.length>20){
	//	document.all["areaDestinatario"].value = document.all["areaDestinatario"].value.substring(0,20)+"...";
	//}
</script>
	
</html>

<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>