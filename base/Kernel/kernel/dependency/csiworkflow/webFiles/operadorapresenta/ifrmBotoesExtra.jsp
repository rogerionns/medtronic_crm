<%@ page language="java" import="br.com.plusoft.csi.crm.form.*, 
								br.com.plusoft.csi.crm.vo.*, 
								br.com.plusoft.csi.crm.helper.MCConstantes, 
								com.iberia.helper.Constantes, 
								br.com.plusoft.fw.app.Application, 
								br.com.plusoft.csi.adm.helper.*" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}
%>

<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>	
	</head>
	
	<script language="JavaScript">
		var bMover = false;
		
		/**
		 * Move lista de botoes para a direita
		 */
		function moverDireita(setarBMover){
			if(setarBMover)
				bMover = true;
			
			if(bMover){
				divBotoes.scrollLeft += 2;
				setTimeout("moverDireita(false);", 10);
			}
		}
		
		/**
		 * Move lista de bot�es para a esquerda
		 */
		function moverEsquerda(setarBMover){
			if(setarBMover)
				bMover = true;

			if(bMover){
				divBotoes.scrollLeft -= 2;
				setTimeout("moverEsquerda(false);", 10);
			}
		}
		
		/**
		 * Monta os parametros para passar nos links dos bot�es
		 */
		//function getUrl(link) {
		function getUrl(link, parametrosArray, idBotao) {
			
			var chamado = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
			var manifestacao = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
			var asn1 = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
			var asn2 = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			var manifTp = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
			var tpManif = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
			var grpManif = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value;
			var dsProduto = '';
			var idPessoa = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa'].value;
			
			var idLinha = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;

			try {
			var pessDsCgc = window.top.ifrmTelaWorkflow.ifrmPESSOA.pessoaForm.pessDsCgc.value;
			var pessDsCpf = window.top.ifrmTelaWorkflow.ifrmPESSOA.pessoaForm.pessDsCpf.value;
			} catch(e) {}
			
			if(link == "ParecerTecnico.do?"){
				url = 'tela=ifrmParecerTecnico'+
					'&csNgtbParecertecnicoPateVo.idChamCdChamado=' + chamado +
					'&csNgtbParecertecnicoPateVo.maniNrSequencia=' + manifestacao +
					'&csNgtbParecertecnicoPateVo.idAsn1CdAssuntonivel1=' + asn1 +
					'&csNgtbParecertecnicoPateVo.idAsn2CdAssuntonivel2=' + asn2 +
					'&idPessCdPessoa='+ idPessoa;
			}
			else{
			/*
				url = '&csNgtbFarmacoFarmVo.idChamCdChamado=' + chamado +
					'&csNgtbFarmacoFarmVo.maniNrSequencia=' + manifestacao +
					'&csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1=' + asn1 +
					'&csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2=' + asn2 +
					'&csNgtbFarmacoFarmVo.idTpmaCdTpManifestacao=' + tpManif +
					'&csNgtbFarmacoFarmVo.idGrmaCdGrupoManifestacao=' + grpManif +
					'&csNgtbFarmacoFarmVo.prasDsProdutoAssunto=' + dsProduto +
					'&idPessCdPessoa=' + idPessoa + '&csNgtbFarmacoFarmVo.idMatpCdManifTipo=' + manifTp;
			*/
				url = '&idChamCdChamado=' + chamado +
					'&maniNrSequencia=' + manifestacao +
					'&idAsn1CdAssuntoNivel1=' + asn1 +
					'&idAsn2CdAssuntoNivel2=' + asn2 +
					'&idLinhCdLinha=' + idLinha +
					'&idTpmaCdTpManifestacao=' + tpManif +
					'&idGrmaCdGrupoManifestacao=' + grpManif +
					'&prasDsProdutoAssunto=' + dsProduto +
					'&idPessCdPessoa=' + idPessoa +
					'&idMatpCdManifTipo=' + manifTp +
					'&pessDsCgc=' + pessDsCgc + 
					'&idFuncCdFuncionario=' + window.top.getIdFuncLogado() + //Chamado: 90471 - 16/09/2013 - Carlos Nunes
					'&pessDsCpf=' + pessDsCpf;
			}
			
			/* if (link.indexOf('?') > 0){
				return link + url;
			}else{
				if(url.indexOf('&') == 0){
					return link + "?" + url.substring(1);
				}else{
					return link + "?" + url;
				}
			} */
			
			if (link.indexOf('?') > 0){
				return parent.obterLink(link + url, parametrosArray, idBotao);
			}else{
				if(url.indexOf('&') == 0){
					return parent.obterLink(link + "?" + url.substring(1), parametrosArray, idBotao);
				}else{
					return parent.obterLink(link + "?" + url, parametrosArray, idBotao);
				}
			}
		}
		
		/**
		 * Acao ao clicar nos bot�es
		 */
		function acaoBotao(link, dimensao){
			showModalDialog(getUrl(link), window.top.principal.manifestacao, dimensao);
		}
		
		/**
		 * Rotinas executadas ao ser carregada a pagina.
		 * Os botoes somente aparecem se a manifestacao tiver sido salva
		 */
		 var nteste = 0;
		 var links = new Array();
		function inicio(){
		
			if(Number(parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value) <= 0)
				divBotoes.style.visibility = "hidden";
			else{
				divBotoes.style.visibility = "visible";
				if(divBotoes.scrollWidth > 570){
					setaEsquerda.style.display = "block";
					setaDireita.style.display = "block";
				}
			}
			
			//Caso a tela com informações da manifestacao nao tenha sido carregada ainda, ele tenta novamente at� conseugir.
			try{
				parent.resetAbas();
				
				var parametros;
				var nParametros = 0;
				
				<logic:present name="botoesAbasVector">
				<logic:iterate name="botoesAbasVector" id="botoesAbasVector" indexId="numero">
				
					parametros = new Array();
					nParametros = 0;
				
					links[<%=numero%>] = new Array();
					links[<%=numero%>][0] = new Array();
	  				links[<%=numero%>][0][0] = "wor.fc.<bean:write name="botoesAbasVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />.executar";  			
					links[<%=numero%>][0][1] = "aba<bean:write name="botoesAbasVector" property="csCdtbBotaoBotaVo.botaDsBotao" />";
					
					<logic:iterate id="csDmtbParametrobotaoPaboVector" name="botoesAbasVector" property="csCdtbBotaoBotaVo.csDmtbParametrobotaoPaboVector" indexId="numeroParametro">
		  				parametros[nParametros] = new Array();
		  				
		  				parametros[nParametros][1] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrobotao" />';
		  				parametros[nParametros][2] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsNomeinterno" />';
		  				parametros[nParametros][3] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrointerno" filter="false"/>';
		  				parametros[nParametros][4] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboInObrigatorio" />';
		  		
		  				nParametros++;
					</logic:iterate>
				
					links[<%=numero%>][0][2] = parametros;
				
					parent.criarAba("<bean:write name="botoesAbasVector" property="csCdtbBotaoBotaVo.botaDsBotao" />", getUrl("<bean:write name="botoesAbasVector" property="csCdtbBotaoBotaVo.botaDsLink" />", links[<%=numero%>][0][2], "<bean:write name="botoesAbasVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />"));
				</logic:iterate>
				</logic:present>
				
			/*	if(nteste < 3){
					nteste++;
					setTimeout("inicio();", 500);
				}*/
			}
			catch(x){
			//	alert(x.description);
				setTimeout("inicio();", 500);
			}

			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VERIFICAR_PERMISSIONAMENTO_BOTOES_ABAS,request).equals("S")) {	%>
			
				//torna as linha inviseis
				var tamanho = parent.document.getElementById("tdBotoes").width;
				for(var i = 0; i < links.length; i++){
					var permissao = links[i][0][0];
					var divFunc= links[i][0][1];
					if (!getPermissao(permissao)){
						try{
							tamanho = tamanho -100;
							parent.document.getElementById(divFunc).style.display = "none";
						}catch(e){}
							
					}
				}
				
				try{
					parent.document.getElementById("tdBotoes").style.width = tamanho;
				}catch(e){}

			<%}%>
			
		}
	</script>
	
	<body class="principalBgrPageIFRM" topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 scroll="no" onload="inicio();">
		<table cellpadding=0 cellspacing=0 border=0 width="100%">
			<tr>
				<td id="setaEsquerda" style="cursor: pointer; display: none;" onmouseover="moverEsquerda(true);" onmouseout="bMover=false;">
					<img src="webFiles/images/botoes/setaLeft.gif">
				</td>
				<td>
					<div id="divBotoes" style="overflow: hidden; width: 570; visibility: hidden;">
						<table cellpadding=0 cellspacing=0 border=0>
							<tr>
							
							<logic:present name="botoesModalVector">
							<logic:iterate name="botoesModalVector" id="botoesModalVector" >
								
								<td style="cursor: pointer" onclick="acaoBotao('<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.botaDsLink" />', '<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.botaDsDimensao" />');" class="principalLstParLink">
									<table cellpadding=0 cellspacing=0 border=0>
										<tr>
											<td><img src="FuncoesExtra.do?tela=funcoesExtraImage&fileName=<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.botaDsIcone" />&idBotaCdBotao=<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />" width="22" height="22" border="0"></td>
											<td class="principalLabel" valign="middle">&nbsp;&nbsp;<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.botaDsBotao" />&nbsp;&nbsp;</td>
										</tr>
									</table>
								</td>
	    					</logic:iterate>
	    					</logic:present>

							</tr>
						</table>
					</div>
				</td>
				<td id="setaDireita" style="cursor: pointer; display: none;" onmouseover="moverDireita(true)" onmouseout="bMover=false;">
					<img src="webFiles/images/botoes/setaRight.gif">
				</td>
			</tr>
		</table>
	</body>
</html>