<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page language="java"  import="br.com.plusoft.csi.crm.helper.MCConstantes, 
								br.com.plusoft.csi.crm.form.ManifestacaoForm, 
								br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo,
								br.com.plusoft.csi.adm.util.Geral" %>

<%@ page import="br.com.plusoft.fw.app.Application"%>								
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">
	function urlProduto() {
		var chamado = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		var manifestacao = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		var asn1 = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		var asn2 = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		var idLinha = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;
		var idGrma = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value;
		var idTpma = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		
		url = "<%=Geral.getActionProperty("produtoLoteAction",empresaVo.getIdEmprCdEmpresa())%>"+
			"?tela=produto&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ chamado +
			"&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia="+ manifestacao +
			"&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+ asn1 +
			"&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2="+ asn2 +
			"&idFuncCdFuncionario=<%=funcVo.getIdFuncCdFuncionario()%>" +
			"&csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha="+ idLinha +
			"&csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao="+ idTpma +
			"&csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao="+ idGrma;
		return url;
	}

	function urlTerceiros() {
		var chamado = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		var manifestacao = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		var asn1 = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		var asn2 = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		url = 'EnvolvimentoTerceiros.do?tela=terceiros&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + asn1 + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + asn2;
		return url;
	}

	function urlAmostra() {
		var chamado = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		var manifestacao = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		var asn1 = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		var asn2 = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		var reclamacao = ""; // parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao"].value;
		//var produto = parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1"].value;
		var produto = ""; // parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto"].value;
		var texto = ""; //parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value;
		var idTpma = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		
		url = 'ReclamacaoMani.do?acao=showAll&tela=amostra&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + asn1 + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + asn2 + '&prasDsProdutoAssunto=' + produto + '&tpmaDsTpManifestacao=' + reclamacao + '&csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + idTpma + "&maniTxManifestacao=" + trataQuebraLinha(texto);
		return url;
	}

	function urlInvestigacao() {
		var chamado = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		var manifestacao = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		var asn1 = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		var asn2 = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		//var produto = parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1"].value;
		var produto = "" ; //parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto"].value;
		var reclamacao = ""; //parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao"].value;
		var idTpma = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		
		url = '<%=Geral.getActionProperty("investigacaoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=investigacao&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + asn1 + '&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + asn2 + '&prasDsProdutoAssunto=' + produto + '&tpmaDsTpManifestacao=' + reclamacao + '&idFuncCdFuncionario=' + '<%=funcVo.getIdFuncCdFuncionario()%>' + '&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + idTpma;
		return url;
	}
	
	function carregaProdutoLote(){
		showModalDialog(urlProduto(),window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:640px;dialogTop:80px;dialogLeft:80px');
//		window.open(urlProduto());
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td id="tdProduto" width="21%" class="principalLabel"><img src="webFiles/images/botoes/bt_produto.gif" width="21" height="22" border="0" onclick="carregaProdutoLote()" class="geralCursoHand"><%= getMessage("prompt.Produto", request)%></td>
    <td id="tdTerceiros" width="25%" class="principalLabel"><img src="webFiles/images/botoes/bt_terceiros.gif" width="21" height="21" onclick="showModalDialog(urlTerceiros(),0,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:390px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand"><%= getMessage("prompt.terceiros", request)%></td>
    <td id="tdAmostra" width="24%" class="principalLabel"><img src="webFiles/images/botoes/bt_amostra.gif" width="21" height="23" onclick="showModalDialog(urlAmostra(),window,'help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:420px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand"><%= getMessage("prompt.amostra", request)%></td>
    <td id="tdAnalise" width="30%" class="principalLabel"><img src="webFiles/images/botoes/bt_investigacao.gif" width="21" height="21" onclick="showModalDialog(urlInvestigacao(),window,'help:no;scroll:no;Status:NO;dialogWidth:780px;dialogHeight:790px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand"><%= getMessage("prompt.investigacao", request)%></td>
  </tr>
</table>
</body>
</html>

<script>
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VERIFICAR_PERMISSIONAMENTO_BOTOES_ABAS,request).equals("S")) {	%>

		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_BOTOES_RECLAMACAO_PRODUTO_VISUALIZACAO_CHAVE%>')){
			document.getElementById("tdProduto").style.display="none";
		}
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_BOTOES_RECLAMACAO_TERCEIROS_VISUALIZACAO_CHAVE%>')){
			document.getElementById("tdTerceiros").style.display="none";
		}
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_BOTOES_RECLAMACAO_AMOSTRA_VISUALIZACAO_CHAVE%>')){
			document.getElementById("tdAmostra").style.display="none";
		}
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_BOTOES_RECLAMACAO_ANALISE_VISUALIZACAO_CHAVE%>')){
			document.getElementById("tdAnalise").style.display="none";
		}	
	
	<%}%>
	
</script>