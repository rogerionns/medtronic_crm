<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
var tipoInfo="E"; //endere�o

function iniciarTela(){

	window.parent.document.all.item('lblComboInfo').innerText = '<bean:message key="prompt.endereco"/>';
	window.parent.document.all.item('btnNovoInfo').title = '<bean:message key="prompt.endereco"/>';


	if (parent.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value != ""){
		reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value = parent.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value;
	}
	
	if (parent.reembolsoProdutoForm.remaInReembolso.checked)
		reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].disabled = false;
	else
		reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].disabled = true;
	
		
	//Posicionando o combo de endere�o
	var infoEnd = reembolsoProdutoForm.txtInfoEndereco.value;
	
	var idTipoEnd = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	var tipoEnd = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	var logradouro = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	var numero = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	var complemento = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	var bairro = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	var municipio = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	var estado = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	var cep = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	var referencia = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	var rg = infoEnd.substring(0, infoEnd.indexOf("@"));
	infoEnd = infoEnd.substring(infoEnd.indexOf("@") + 1);
	
	if(reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].length == 2){
		reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].selectedIndex = 1;
	}
	else if(reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].length > 2 && reembolsoProdutoForm.txtInfoEndereco.value != ""){
		var textoCombo = "";
		for(i = 0; i < reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].length; i++){
			textoCombo = reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].options[i].text;
			if(textoCombo.indexOf(logradouro.toUpperCase()) > -1 && textoCombo.indexOf(cep) > -1){
				reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].selectedIndex = i;
				break;
			}
		}
	}
/*	else if(reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].length > 2 && reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].selectedIndex <= 0){
		reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].selectedIndex = 1;
	}*/
	
	//parent.habilitaCamposReembolso();
}

function insereDados(idTipoEnd,tipoEnd,logradouro,numero,complemento,bairro,municipio,estado,cep,referencia,rg){
	
	
	reembolsoProdutoForm.txtInfoEndereco.value = idTipoEnd + "@" + tipoEnd + "@" + logradouro + "@" + numero + "@" + complemento + "@" + bairro + "@" + municipio + "@" + estado + "@" + cep + "@" + referencia + "@" + rg;

	reembolsoProdutoForm.tela.value='<%=MCConstantes.TELA_CMB_INFOBANCO%>';
	reembolsoProdutoForm.acao.value='<%=MCConstantes.ACAO_SHOW_ALL%>';
	reembolsoProdutoForm.usarFormaRetornoTela.value = "S";
	reembolsoProdutoForm.submit();

}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
  <html:hidden property="csNgtbReembolsoReemVo.reemNrSequencia" />
  <html:hidden property="csNgtbReembolsoReemVo.idForeCdFormareemb"/>
  <html:hidden property="txtInfoEndereco"/>	
  <html:hidden property="txtInfoBanco"/>
  
  <input type="hidden" name="usarFormaRetornoTela" >
	
	<!--html:text property="csNgtbReembolsoReemVo.remaDsLogradouro" /-->
	
  <html:select property="csNgtbReembolsoReemVo.remaDsLogradouro" styleClass="principalObjForm">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csNgtbReembolsoReemVector">
      <html:options collection="csNgtbReembolsoReemVector" property="remaDsLogradouro" labelProperty="infoEndereco"/>
    </logic:present>
  </html:select>
  	
  <logic:present name="csNgtbReembolsoReemVector" >
	  <logic:iterate name="csNgtbReembolsoReemVector" id="csNgtbReembolsoReemVector" indexId="numero">
		  
		  <input type="hidden" name='txtidTipoEnd' value='<bean:write name="csNgtbReembolsoReemVector" property="csDmtbTpenderecoTpenVo.idTpenCdTpendereco"/>'>
		  <input type="hidden" name='txtDsTipoEnd' value='<bean:write name="csNgtbReembolsoReemVector" property="csDmtbTpenderecoTpenVo.tpenDsTpendereco"/>'>
		  <input type="hidden" name='txtDsLogradouro' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsLogradouro"/>'>
		  <input type="hidden" name='txtDsNumero' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsNumero"/>'>
          <input type="hidden" name='txtDsComplemento' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsComplemento"/>'>
		  <input type="hidden" name='txtDsBairro' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsBairro"/>'>
		  <input type="hidden" name='txtDsMunicipio' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsMunicipio"/>'>
		  <input type="hidden" name='txtDsEstado' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsUf"/>'>
		  <input type="hidden" name='txtDsCep' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsCep"/>'>
		  <input type="hidden" name='txtDsReferencia' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsReferencia"/>'>
		  <input type="hidden" name='txtDsRg' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsRg"/>'>
		  
	  </logic:iterate>	  
  </logic:present>
  
  
</html:form>
</body>
</html>
