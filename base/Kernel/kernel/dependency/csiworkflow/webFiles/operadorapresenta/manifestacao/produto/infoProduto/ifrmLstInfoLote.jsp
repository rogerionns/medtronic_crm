<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLoteReloVo, br.com.plusoft.fw.app.Application,br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long idPesqCdPesquisa =0;

if (request.getSession() != null && request.getSession().getAttribute("lotePesquisaVo") != null)
	idPesqCdPesquisa = ((CsNgtbReclamacaoLoteReloVo)request.getSession().getAttribute("lotePesquisaVo")).getIdPesqCdPesquisa();

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
nLinha = new Number(0);
estilo = new Number(0);

var possuiAcessorio = false;
var exitePesqPendentes = "";

function addLote(dFabricacao, dValidade, tLote, cFabrica, nFabrica, qComprada, qReclamada, qFechada, inRepor, inTrocar, qAberta, qTroca, cLaboratorio, inAnalise, cLote, idLinha, idPras, descPras, idGrupoManif, idTpManif, idCondUso, idSitProd, idMotLoteBranco, inRessarcir, idMotRessarcir, inAcessorio, idQuest, possuiLaudo, pesquisaRespondida, vlProduto, vlUnitProduto, idDestinoProduto, prodVencido, idAsn2) {
	nLinha = nLinha + 1;
	estilo++;
	
	altTxt = "onclick=\"editLote('" + dFabricacao + "', '" + dValidade + "', '" + tLote + "', '" + cFabrica + "', '" + nFabrica + "', '" + qComprada + "', '" + qReclamada + "', '" + qFechada + "', '" + inRepor + "', '" + inTrocar + "', '" + qAberta + "', '" + qTroca + "', '" + cLaboratorio + "', '" + inAnalise + "', '" + cLote + "', '" + nLinha + "','" + idLinha + "', '" + idPras + "', '" + descPras + "', '" + idGrupoManif + "', '" + idTpManif + "', '" + idCondUso + "', '" + idSitProd + "', '" + idMotLoteBranco + "', '" + inRessarcir + "', '" + idMotRessarcir + "', '" + inAcessorio + "', '" + idQuest + "', '" + vlProduto + "', '" + vlUnitProduto + "', '" + idDestinoProduto + "', '" + prodVencido + "', '" + idAsn2 + "')\"";

	strTxt = "";
    strTxt += "<div id=\"lstLote" + nLinha + "\" style='width:100%; height: 20; overflow: auto'>";
    strTxt += "<table id=\"" + nLinha + "\" width='100%' height='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
    strTxt += "    <input type='hidden' name='fabricacao' value='" + dFabricacao + "'>";
    strTxt += "    <input type='hidden' name='validade' value='" + dValidade + "'>";
    strTxt += "    <input type='hidden' name='lote' value='" + tLote + "'>";
    strTxt += "    <input type='hidden' name='fabrica' value='" + cFabrica + "'>";
    strTxt += "    <input type='hidden' name='comprada' value='" + qComprada + "'>";
    strTxt += "    <input type='hidden' name='reclamada' value='" + qReclamada + "'>";
    strTxt += "    <input type='hidden' name='fechada' value='" + qFechada + "'>";
    strTxt += "    <input type='hidden' name='repor' value='" + inRepor + "'>";
    strTxt += "    <input type='hidden' name='trocar' value='" + inTrocar + "'>";
    strTxt += "    <input type='hidden' name='aberta' value='" + qAberta + "'>";
    strTxt += "    <input type='hidden' name='troca' value='" + qTroca + "'>";
    strTxt += "    <input type='hidden' name='laboratorio' value='" + cLaboratorio + "'>";
    strTxt += "    <input type='hidden' name='analise' value='" + inAnalise + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cLote + "'>";

	strTxt += "    <input type='hidden' name='idLinha' value='" + idLinha + "'>";
	strTxt += "    <input type='hidden' name='idPras' value='" + idPras + "'>";
	strTxt += "    <input type='hidden' name='idAsn2' value='" + idAsn2 + "'>";	
	strTxt += "    <input type='hidden' name='descPras' value='" + descPras + "'>";
	strTxt += "    <input type='hidden' name='idGrupoManif' value='" + idGrupoManif + "'>";
	strTxt += "    <input type='hidden' name='idTpManif' value='" + idTpManif + "'>";
	strTxt += "    <input type='hidden' name='idCondUso' value='" + idCondUso + "'>";
	strTxt += "    <input type='hidden' name='idSitProd' value='" + idSitProd + "'>";
	strTxt += "    <input type='hidden' name='idMotLoteBranco' value='" + idMotLoteBranco + "'>";
	strTxt += "    <input type='hidden' name='inRessarcir' value='" + inRessarcir + "'>";
	strTxt += "    <input type='hidden' name='idMotRessarcir' value='" + idMotRessarcir + "'>";	
	strTxt += "    <input type='hidden' name='inAcessorio' value='" + inAcessorio + "'>";	
	strTxt += "    <input type='hidden' name='idQuest' value='" + idQuest + "'>";	
	strTxt += "    <input type='hidden' name='vlProduto' value='" + vlProduto + "'>";    
	strTxt += "    <input type='hidden' name='vlUnitProduto' value='" + vlUnitProduto + "'>";
	strTxt += "    <input type='hidden' name='idDestinoProduto' value='" + idDestinoProduto + "'>";
	strTxt += "    <input type='hidden' name='prodVencido' value='" + prodVencido + "'>";
	    
    if (possuiLaudo == "S")
	    strTxt += "    <td class='principalLstParMao' width='2%' align='center'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeLote('POSSUI_LAUDO','')\"></td>";
	else
	    strTxt += "    <td class='principalLstParMao' width='2%' align='center'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeLote('" + cLote + "','" + vlProduto + "')\"></td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='28%'>" + acronymLst(descPras, 25) + "&nbsp;</td>";
    if (idQuest == "0")
    	if (pesquisaRespondida == "S")//mostra icone de pesquisa respondida
	        strTxt += "    <td class='principalLstParMao' " + altTxt + " width='14%'><img src='webFiles/images/botoes/interrogacao_check.gif' width='12' height='14' title=\'<bean:message key="prompt.Pesquisa_Respondida"/>\'>&nbsp;</td>";
    	else{
		    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='14%'>&nbsp;</td>";
		}    
    else{
	    strTxt += "    <td class='principalLstParMao' width='14%'><img src='webFiles/images/icones/interrogacao.gif' width='10' height='14' class='geralCursoHand' title=\'<bean:message key="prompt.Responder_Pesquisa"/>\'></td>";        
	    exitePesqPendentes="S";
	}    
    strTxt += "    <td class='principalLstParMao' width='12%'><img src='webFiles/images/icones/resposta.gif' width='14' height='14' class='geralCursoHand' onclick=\"carregaLocalCompra('" + cLote + "')\"></td>";
    if (inRessarcir == "N")
	    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='11%'><img src='webFiles/images/botoes/check.gif' width='11' height='12' class='geralCursoHand'></td>";
	else
	    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='11%'>&nbsp;</td>";
    if (inAcessorio == "S"){
    	possuiAcessorio = true;
	    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='9%'><img src='webFiles/images/botoes/check.gif' width='11' height='12' class='geralCursoHand'></td>";
    }else{
	    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='9%'>&nbsp;</td>";    
	}    
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='12%'>" + tLote + "&nbsp;</td>";
    strTxt += "    <td class='principalLstParMao' " + altTxt + " width='10%'>" + dValidade + "&nbsp;</td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    strTxt += "</div>";
	
	document.getElementById("lstLote").innerHTML += strTxt;
	
	calculaTotais();
}

function removeLote(nTblExcluir,vlProduto) {

	if (window.parent.edicaoLote == "true"){
		alert ('<bean:message key="prompt.Este_lote_esta_em_modo_de_edicao"/>.');
		return false;
	}

	if (nTblExcluir == "POSSUI_LAUDO"){
		alert('<bean:message key="prompt.Este_lote_possui_laudo_e_nao_pode_ser_excluido"/>.');
		return false;
	}
	
	
	if (!validaSaldo(vlProduto)){
		//alert('Este Lote n�o pode ser exclu�do.\nO valor do produto � superior ao Saldo a reembolsar');
		alert('<bean:message key="prompt.Este_Lote_nao_pode_ser_excluido_O_valor_do_produto_e_superior_ao_Saldo_a_reembolsar"/>');
		return false;
	}

	msg = '<bean:message key="prompt.alert.remov.lote" />';
	if (confirm(msg)) {
		parent.parent.excluirLote(nTblExcluir);
	}
}

function validaSaldo(vlProduto){

	var vlProd = new Number(0);
	var vlSaldo = new Number(0);

	var vlDsSaldo = window.top.ifrmRessarcimento.ifrmVlRessarcimento.reembolsoProdutoForm.txtVlSaldo.value;
	
	if (vlProduto != ''){
		vlProd = new Number(vlProduto.replace(",","."));
	}
	
	if (vlDsSaldo != ''){
		vlSaldo = new Number(vlDsSaldo.replace(",","."));
	}	

	if (vlProd <= vlSaldo)
		return true;
	else
		return false;

}

function editLote(dFabricacao, dValidade, tLote, cFabrica, nFabrica, qComprada, qReclamada, qFechada, inRepor, inTrocar, qAberta, qTroca, cLaboratorio, inAnalise, cLote, altLinha, idLinha, idPras, descPras, idGrupoManif, idTpManif, idCondUso, idSitProd, idMotLoteBranco, inRessarcir, idMotRessarcir, inAcessorio, idQuest, vlProduto, vlUnitProduto, idDestinoProduto, prodVencido, idAsn2) {
	
	window.parent.edicaoLote = "true";
	
	window.parent.produtoLoteForm.reloDhDtFabricacao.value = dFabricacao;
	window.parent.produtoLoteForm.reloDhDtValidade.value = dValidade;
	window.parent.produtoLoteForm.reloDsLote.value = tLote;
	window.parent.cmbFabrica.location = "ProdutoLote.do?acao=showAll&tela=cmbFabrica&cmbIdFabrCdFabrica=" + cFabrica +"&idEmprCdEmpresa="+ parent.idEmprCdEmpresa;
	
	window.parent.produtoLoteForm.reloNrComprada.value = (qComprada!="0"?qComprada:"") ;
	window.parent.produtoLoteForm.reloNrReclamada.value = (qReclamada!="0"?qReclamada:"");
	window.parent.produtoLoteForm.reloNrDisponivel.value = (qFechada!="0"?qFechada:"");
	window.parent.produtoLoteForm.reloNrAberta.value = (qAberta!="0"?qAberta:"");
	window.parent.produtoLoteForm.reloNrTroca.value = (qTroca!="0"?qTroca:"");
	
	window.parent.cmbLaboratorio.location = "ProdutoLote.do?acao=showAll&tela=cmbLaboratorio&cmbIdFabrCdLaboratorio=" + cLaboratorio +"&idEmprCdEmpresa="+ parent.idEmprCdEmpresa;
	window.parent.produtoLoteForm.codigo.value = cLote;
	window.parent.produtoLoteForm.altLinha.value = altLinha;
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
		window.parent.produtoLoteForm.chkDecontinuado.checked = false;
	<%}%>
	
	window.parent.ifrmCmbLinha.location = "ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_LINHA_LOTE%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha=" + idLinha + "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + idPras+ "&csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao=" + idGrupoManif + "&csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=" + idTpManif + "&csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto=" + idDestinoProduto + "&csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + idAsn2 +"&idEmprCdEmpresa="+ parent.idEmprCdEmpresa;
	//window.parent.ifrmCmbProduto.location = "ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_PRODUTO_LOTE%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha=" + idLinha + "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + idPras + "&csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao=" + idGrupoManif + "&csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=" + idTpManif + "&csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto=" + idDestinoProduto;
	
	window.parent.ifrmCmbCondUso.location = "ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_CONDICAO_USO%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idClotCdCondicaolote=" + idCondUso +"&idEmprCdEmpresa="+ parent.idEmprCdEmpresa;
	window.parent.ifrmCmbSitProduto.location = "ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_SITUACAO_PROD%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idSiloCdSituacaolote=" + idSitProd +"&idEmprCdEmpresa="+ parent.idEmprCdEmpresa;

	window.parent.ifrmCmbMotivoLtBranco.location = "ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_MOTIVO_LT_BRANCO%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idMoloCdMotivolote=" + idMotLoteBranco +"&idEmprCdEmpresa="+ parent.idEmprCdEmpresa;

	window.parent.ifrmCmbMotivo.location = "ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_MOTIVO%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idMotrCdMotivotroca=" + idMotRessarcir +"&idEmprCdEmpresa="+ parent.idEmprCdEmpresa;	

	if (inAnalise == "S"){
		window.parent.produtoLoteForm.reloInAnalise.checked = true;
	}else{
		window.parent.produtoLoteForm.reloInAnalise.checked = false;
	}
	
	if (inRessarcir == "S"){
		window.parent.produtoLoteForm.reloInNaoRessarcir[1].checked = true; //n�o ressarcir
	}else{
		window.parent.produtoLoteForm.reloInNaoRessarcir[0].checked = true;	//ressarcir
	}
	if (inAcessorio == "S")
		window.parent.produtoLoteForm.reloInAcessorio.checked = true;
	else	
		window.parent.produtoLoteForm.reloInAcessorio.checked = false

	window.parent.produtoLoteForm.txtVlProduto.value = vlProduto;
	window.parent.produtoLoteForm.txtVlUnitProduto.value = vlUnitProduto;
	
	window.top.desabiliaCampos_TelaProduto();
	window.top.trataDataValidade(prodVencido);
	
}

function calculaTotais() {
	var comp = 0;
	var recl = 0;
	var fech = 0;
	var aber = 0;
	var troc = 0;

	try {
		if (document.getElementsByName("comprada").length != undefined) {
			for (var i = 0; i < document.getElementsByName("comprada").length; i++) {
				 comp += new Number(document.getElementsByName("comprada")[i].value);
				 recl += new Number(document.getElementsByName("reclamada")[i].value);
				 fech += new Number(document.getElementsByName("fechada")[i].value);
				 aber += new Number(document.getElementsByName("aberta")[i].value);
				 troc += new Number(document.getElementsByName("troca")[i].value);
			}
		} else {
			comp = document.getElementsByName("comprada").value;
			recl = document.getElementsByName("reclamada").value;
			fech = document.getElementsByName("fechada").value;
			aber = document.getElementsByName("aberta").value;
			troc = document.getElementsByName("troca").value;
		}
	} catch (e) {
		alert (e);
		comp = "";
		recl = "";
		fech = "";
		aber = "";
		troc = "";
	}
	
	parent.produtoLoteForm.txtComprada.value = comp;
	parent.produtoLoteForm.txtReclamada.value = recl;
	parent.produtoLoteForm.txtFechada.value = fech;
	parent.produtoLoteForm.txtAberta.value = aber;
	parent.produtoLoteForm.txtTroca.value = troc;
}


function montaLotesReincidentes() {
	var reinc = "";
	try{
		reinc = 'LOTE                ' + 'QTD. DE VEZES RECLAMADO';
		if (document.all["nomeLote"] != null) {
			if (document.all["nomeLote"].length == undefined) {
				if(Number(document.all["qtdLote"].value) > 1){
					reinc += '\n' + document.all["nomeLote"].value;
				    for (var i = 0; i < (20 - document.all["nomeLote"].value.length); i++) {
				    	reinc += ' ';
				    }
				    reinc += document.all["qtdLote"].value;
				}
			} else {
				for (var j = 0; j < document.all["nomeLote"].length; j++) {
					if(Number(document.all["qtdLote"][j].value) > 1){
						reinc += '\n' + document.all["nomeLote"][j].value;
					    for (var i = 0; i < (20 - document.all["nomeLote"][j].value.length); i++) {
					    	reinc += ' ';
					    }
					    reinc += document.all["qtdLote"][j].value;
					}
				}
			}
		} else {
			reinc = "";
		}
	}catch(e){}
		
	parent.produtoLoteForm.lotesReincidentes.value = reinc;
}


function carregaLocalCompra(reloNrSequencial){
	var url;
	url = "ProdutoLote.do?tela=<%=MCConstantes.TELA_LOCAL_COMPRA%>";
	url = url + "&acao=<%=MCConstantes.ACAO_SHOW_ALL%>";
	url = url + "&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + parent.parent.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
	url = url + "&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + parent.parent.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
	url = url + "&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + parent.parent.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	url = url + "&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + parent.parent.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	url = url + "&reloNrSequencia=" + reloNrSequencial;
	
	
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:780px;dialogHeight:300px,dialogTop:200px,dialogLeft:200px');

}

function habilitaPesquisaLote(idQuest,reloNrSequencia){
	parent.parent.habilitaPesquisaLote(idQuest,reloNrSequencia);
}

function preparaPesquisaLote(){
	parent.parent.preparaPesquisaLote();
}

function iniciaTela(){
	parent.parent.document.all.item('aguarde').style.visibility = 'hidden';
	parent.parent.bEnvia = true;
	montaLotesReincidentes();
	
	<%if (request.getSession().getAttribute("lotePesquisaVo") != null){%>
		parent.parent.preparaPesquisa('<%=idPesqCdPesquisa%>');
	<%}%>
	
	window.top.desabiliaCampos_TelaProduto();
	
	
	//Atualiza valor a ressarcir
	//Henrique - 12/04/06 alterado para nao dar erro de javascript
	//window.top.ifrmRessarcimento.carregaCamposValor();
	//window.top.ifrmRessarcimento.habilitaCampos();

	//Chama os metodos que estao dentro do frame de ressarcimento
	acionaRessarcimento();	
}

//Metodo utilizado para so chamar o metodo para carregar os campos valor quando o ifrme ja estiver carregado
var nAcionaRessarcimento = 0;
function acionaRessarcimento(){
	try {
		window.top.ifrmRessarcimento.carregouCamposValor = false;
		window.top.ifrmRessarcimento.carregaCamposValor();
		window.top.ifrmRessarcimento.habilitaCampos();
		nAcionaRessarcimento = 0;
	}
	catch(x){
		if(nAcionaRessarcimento > 30){
			alert("Erro em acionaRessarcimento(): \n\n"+ x);
		}
		else{
			nAcionaRessarcimento++;
			setTimeout('acionaRessarcimento()', 300);
		}
	}
}


</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">

<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
<!-- marca o envio atual -->
<html:hidden property="csNgtbReclamacaoManiRemaVo.csDmtbEnvioEnviVo.idEnviCdEnvio"/>
<html:hidden property="csNgtbReclamacaoManiRemaVo.csDmtbEnvioEnviVo.enviDsEnvio"/>
<!-- ******************* -->
</html:form>

<input type="hidden" name="lotesExcluidos">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top"> 
    <td class="principalLstPar" colspan="10" height="100%">
      <div id="lstLote" style="width:100%; height:100%;">
        <!--Inicio Lista Lote -->
		<logic:present name="csNgtbReclamacaoLoteReloVector">
          <logic:iterate id="cnrlrVector" name="csNgtbReclamacaoLoteReloVector">
            <script language="JavaScript">
			  addLote('<bean:write name="cnrlrVector" property="reloDhDtFabricacao" />',
						'<bean:write name="cnrlrVector" property="reloDhDtValidade" />',
						'<bean:write name="cnrlrVector" property="reloDsLote" />', 
						'<bean:write name="cnrlrVector" property="csCdtbFabricaFabrVo.idFabrCdFabrica" />', 
						'<bean:write name="cnrlrVector" property="csCdtbFabricaFabrVo.fabrDsFabrica" />', 
						'<bean:write name="cnrlrVector" property="reloNrComprada" />', 
						'<bean:write name="cnrlrVector" property="reloNrReclamada" />', 
						'<bean:write name="cnrlrVector" property="reloNrDisponivel" />', 
						'<bean:write name="cnrlrVector" property="reloInRepor" />', 
						'<bean:write name="cnrlrVector" property="reloInTrocar" />', 
						'<bean:write name="cnrlrVector" property="reloNrAberta" />', 
						'<bean:write name="cnrlrVector" property="reloNrTroca" />',
						'<bean:write name="cnrlrVector" property="csCdtbLaboratorioFabrVo.idFabrCdFabrica" />',
						'<bean:write name="cnrlrVector" property="reloInAnalise" />',
						'<bean:write name="cnrlrVector" property="reloNrSequencia" />',
						'<bean:write name="cnrlrVector" property="produtoReclamadoVo.csCdtbLinhaLinhVo.idLinhCdLinha" />',
						'<bean:write name="cnrlrVector" property="produtoReclamadoVo.idAsnCdAssuntoNivel" />',
						'<bean:write name="cnrlrVector" property="produtoReclamadoVo.prasDsProdutoAssunto" />',
						'<bean:write name="cnrlrVector" property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />',	
						'<bean:write name="cnrlrVector" property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />',
						'<bean:write name="cnrlrVector" property="csCdtbCondicaoloteClotVo.idClotCdCondicaolote" />',
						'<bean:write name="cnrlrVector" property="csCdtbSituacaoloteSiloVo.idSiloCdSituacaolote" />',
						'<bean:write name="cnrlrVector" property="csCdtbMotivoloteMoloVo.idMoloCdMotivolote" />',
						'<bean:write name="cnrlrVector" property="reloInNaoRessarcir" />',
						'<bean:write name="cnrlrVector" property="csCdtbMotivotrocaMotrVo.idMotrCdMotivotroca" />',
						'<bean:write name="cnrlrVector" property="reloInAcessorio" />',						
						'<bean:write name="cnrlrVector" property="idPesqCdPesquisa"/>',
						'<bean:write name="cnrlrVector" property="possuiLaudo"/>',
						'<bean:write name="cnrlrVector" property="pesquisaRespondida"/>',
						'<bean:write name="cnrlrVector" property="reloVlProduto"/>',
						'<bean:write name="cnrlrVector" property="reloVlUnitProduto"/>',
						'<bean:write name="cnrlrVector" property="csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto"/>',
						'<bean:write name="cnrlrVector" property="prodVencido"/>',
						'<bean:write name="cnrlrVector" property="produtoReclamadoVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>');
						
	        </script>
          </logic:iterate>
        </logic:present>
        <!--Final Lista Lote -->
      </div>
    </td>
  </tr>
</table>
<logic:present name="loteReencidenteVector">
  <logic:iterate name="loteReencidenteVector" id="loteReencidenteVector">
    <input type="hidden" name="nomeLote" value="<bean:write name="loteReencidenteVector" property="reloDsLote"/>">
    <input type="hidden" name="qtdLote" value="<bean:write name="loteReencidenteVector" property="reloNrSequencia"/>">
    <input type="hidden" name="nomeProd" value="<bean:write name="loteReencidenteVector" property="produtoReclamadoVo.prasDsProdutoAssunto"/>">
  </logic:iterate>
</logic:present>
</body>
</html>