<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, 
                                 com.iberia.helper.Constantes, 
                                 br.com.plusoft.fw.app.Application,
                                 br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

function submeteForm() {
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		reembolsoProdutoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		reembolsoProdutoForm.tela.value = '<%=MCConstantes.TELA_CMB_VARIEDADE_ACESSORIO%>';
		reembolsoProdutoForm.target = parent.cmbVariedadeAcessorio.name;
		reembolsoProdutoForm.submit();
		
		reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = "";
	<%}%>
}
	
function mostraCampoBuscaProd(){
	window.location.href = "ReembolsoProduto.do?tela=<%=MCConstantes.TELA_CMB_PRODUTO_ACESSORIO%>&acao=<%=Constantes.ACAO_CONSULTAR%>";	
}

function buscarProduto(){

	if (reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value.length < 3){
		alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
		return false;
	}

	reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_CMB_PRODUTO_ACESSORIO%>";
	reembolsoProdutoForm.acao.value = "<%=Constantes.ACAO_FITRAR%>";

	reembolsoProdutoForm.submit();
}

function carregaLinha(){
	var idAsn1;
	var vIdAsn1;
	var idLinha;
	
	if (reembolsoProdutoForm.acao.value == '<%=Constantes.ACAO_FITRAR%>'){
		idAsn1 = reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;		
		
		vIdAsn1 = idAsn1.split("@");
		idAsn1 = vIdAsn1[0];
		if (idAsn1.length > 0){
			idLinha  = (eval("reembolsoProdutoForm.txtLinha" + idAsn1 + ".value;"));
			parent.ifrmCmbLinhaAcessorio.document.getElementById("reembolsoProdutoForm")['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = idLinha;
		}	
	}
}

function pressEnter(event) {
    if (event.keyCode == 13) {
		buscarProduto();
    }
}

function iniciaTela(){
	carregaLinha();
	verificarCmbDisabled();
}

var nVerificarCmbDisabled = 0;
function verificarCmbDisabled(){
	try{	
		reembolsoProdutoForm["csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].disabled = parent.ifrmCmbLinhaAcessorio.document.getElementById("reembolsoProdutoForm")['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].disabled;
	}
	catch(x){
		nVerificarCmbDisabled++;
		if(nVerificarCmbDisabled > 10){
			alert("Erro ao verificar o disabled do combo em ifrmCmbProdutoAcessorio\n"+ x.description);
		}
		else{
			setTimeout("verificarCmbDisabled()", 300);
		}
	}
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();submeteForm();">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />

	<logic:notEqual name="reembolsoProdutoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
			  <html:select property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" styleClass="principalObjForm" onchange="carregaLinha();submeteForm();">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			    <logic:present name="csCdtbProdutoAssuntoPrasVector">
				  <html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/>
				</logic:present>
			  </html:select>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand"  title='<bean:message key="prompt.PesquisarProduto"/>' onclick="mostraCampoBuscaProd();"></div>
		  	</td>
	  	</tr>
	  </table>

	  <logic:present name="csCdtbProdutoAssuntoPrasVector">
		  <logic:iterate name="csCdtbProdutoAssuntoPrasVector" id="csCdtbProdutoAssuntoPrasVector">
			  <input type="hidden" name='txtLinha<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>' value='<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>'>
		  </logic:iterate>	  
	  </logic:present>
	  
   </logic:notEqual>
   
   <logic:equal name="reembolsoProdutoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
	  	<tr>
	  		<td width="95%">
	  			<html:text property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" styleClass="principalObjForm" onkeydown="pressEnter(event)" />
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand"  title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
		  	</td>
		</tr> 	
	  </table>
	  <script>
		reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].select();
	  </script>
   </logic:equal>

</html:form>
</body>
</html>
