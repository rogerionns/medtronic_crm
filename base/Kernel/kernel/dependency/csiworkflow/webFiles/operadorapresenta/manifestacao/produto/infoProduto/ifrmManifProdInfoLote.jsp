<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*,br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<html>
<head>
<title>..: <bean:message key="prompt.informacoeslote" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

var idEmprCdEmpresa = "<%= empresaVo.getIdEmprCdEmpresa()%>";
var edicaoLote="";
var posicionouLinha=false;
var posicionouAsn1=false;
var posicionouAsn2=false;
var posicionouGrupo=false;
var posicionouTipo=false;

function adicionarLote() {
	if (validaCampos()) {
		edicaoLote = "";
		window.parent.submeteForm();
		limpaCampos();
		wi = window.top.dialogArguments;
		//wi.parent.top.principal.manifestacao.manifestacaoForm.inPossuiProdutoReclamado.value="true";
	}
}

function limpaCampos(){
		produtoLoteForm.reloDhDtFabricacao.value = "";
		produtoLoteForm.reloDhDtValidade.value = "";
		produtoLoteForm.reloDhDtValidade.value = "";
		produtoLoteForm.reloDsLote.value = "";
		cmbFabrica.produtoLoteForm.cmbIdFabrCdFabrica.value = "";
		cmbFabrica.produtoLoteForm.submit();
		produtoLoteForm.reloNrComprada.value = "";
		produtoLoteForm.reloNrReclamada.value = "";
		produtoLoteForm.reloNrDisponivel.value = "";
		produtoLoteForm.reloNrAberta.value = "";
		produtoLoteForm.reloNrTroca.value = "";
		//cmbLaboratorio.produtoLoteForm.cmbIdFabrCdLaboratorio.value = "";
		//cmbLaboratorio.produtoLoteForm.submit();
		
		produtoLoteForm.reloInAnalise.checked = true;
		produtoLoteForm.codigo.value = "0";
		produtoLoteForm.altLinha.value = "0";
		
		ifrmCmbLinha.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = ""; 
		ifrmCmbProduto.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = "";
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			cmbVariedade.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = "";
		<%}%>
		ifrmCmbGrupoManif.produtoLoteForm['csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value = "";
		ifrmCmbReclamacao.produtoLoteForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = "";
		ifrmCmbCondUso.document.produtoLoteForm.idClotCdCondicaolote.value = "";
		ifrmCmbSitProduto.produtoLoteForm.idSiloCdSituacaolote.value = "";
		ifrmCmbMotivoLtBranco.document.produtoLoteForm.idMoloCdMotivolote.value = "";
		produtoLoteForm.reloInNaoRessarcir[1].checked = true;
		ifrmCmbMotivo.document.produtoLoteForm.idMotrCdMotivotroca.value = "";
		produtoLoteForm.reloInAcessorio.checked = false;
		
		ifrmCmbDestino.produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].value = "";
		ifrmCmbDestino.produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].disabled = true;
		
		cmbLaboratorio.produtoLoteForm.cmbIdFabrCdLaboratorio.value = "";
		cmbLaboratorio.produtoLoteForm.cmbIdFabrCdLaboratorio.disabled = true;
		
		habilitaMotLoteBranco();
		//window.top.habilitaLaboratorio();
		habilitaMotivoRessarci();

}

function validaCampos() {

	if (window.top.validaObrigatorio_incluirProduto() == false){
		return false;
	}

	if (ifrmCmbProduto.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value == ""){
		alert ('<%=getMessage("prompt.Selecione_o_produto_reclamado",request)%>.');
		return false;
	}
	
	if (ifrmCmbReclamacao.produtoLoteForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value == ""){
		alert ('<%=getMessage("prompt.Selecione_o_tipo_de_reclamacao",request)%>.');
		return false;
	}
	
	if(produtoLoteForm.reloNrReclamada.value == ""){
		alert("<bean:message key="prompt.alert.Qtd_reclamada_obrigatorio"/>");
		return false;
	}
	
	if(produtoLoteForm.reloInNaoRessarcir[0].checked && produtoLoteForm.reloNrTroca.value == ""){
		alert("<bean:message key="prompt.alert.Campo_Qtd_Coletar"/>");
		return false;
	}
	
	if (produtoLoteForm.reloDhDtFabricacao.value != "")
		if (!verificaData(produtoLoteForm.reloDhDtFabricacao))
			return false;
	if (produtoLoteForm.reloDhDtValidade.value != "")
		if (!verificaData(produtoLoteForm.reloDhDtValidade))
			return false;
			
	if (produtoLoteForm.reloDsLote.value == ""){
		if (ifrmCmbMotivoLtBranco.document.produtoLoteForm.idMoloCdMotivolote.value == ""){
			alert ('<bean:message key="prompt.Selecione_o_motivo_do_lote_em_branco"/>.');
			return false;			
		}
	}
	
	if (produtoLoteForm.reloInNaoRessarcir[1].checked){
		if (ifrmCmbMotivo.document.produtoLoteForm.idMotrCdMotivotroca.value == ""){
			alert ('<bean:message key="prompt.Selecione_o_motivo_para_o_nao_ressarcimento"/>.');
			return false;
		}
	}
	
	if(ifrmCmbSitProduto.produtoLoteForm.idSiloCdSituacaolote.value == ""){
		alert("<bean:message key="prompr.alert.Campo_Situacao_produto"/>");
		return false;
	}

	//compara campos para evitar repeti��o
	if (lstLote.document.getElementsByName("idTpManif").length > 0){
		var idTpManif="";
		var lote="";
		var idPras="";
		var codigo="";
		
		var idTpManifNovo = ifrmCmbReclamacao.produtoLoteForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value==''?'0':ifrmCmbReclamacao.produtoLoteForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		var loteNovo = produtoLoteForm.reloDsLote.value;
		var idPrasNovo = ifrmCmbProduto.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value==''?'0':ifrmCmbProduto.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		
		var num = lstLote.document.getElementsByName("idTpManif").length;
		for (var i=0;i<num;i++){
			idTpManif = lstLote.document.getElementsByName("idTpManif").item(i).value;
			lote = lstLote.document.getElementsByName("lote").item(i).value;
			idPras = lstLote.document.getElementsByName("idPras").item(i).value;
			codigo = lstLote.document.getElementsByName("codigo").item(i).value;
			if (produtoLoteForm.codigo.value != codigo){
				if ((idTpManifNovo == idTpManif) && (loteNovo == lote) && (idPrasNovo == idPras)){
					alert ('<%=getMessage("prompt.Ja_exite_um_registro_com_o_mesmo_produto_queixa_lote",request)%>');
					return false;
				}
			}
		}	
	}	
	
	
	if (produtoLoteForm.codigo.value != "0"){
	 	if (!validaSaldo()){
			//alert ("O campo Qtd.Troca/Repor n�o pode ser alterado.\nO valor do produto � menor que o Saldo a Reembolsar");
			alert ('<%=getMessage("prompt.O_campo_QtdTroca_Repor_nao_pode_ser_alterado_O_valor_do_produto_e_menor_que_o_Saldo_a_Reembolsar",request)%>');
			return false;
		}
 	}
 			
	return true;
}


function validaSaldo(){
	var qtdTroca = new Number(0);
	var vlUnitProd = new Number(0);
	var vlProduto = new Number(0);
	var vlAtualProduto = new Number(0);
	var vlDiferenca = new Number(0);
	var vlAux = "";
	
	vlAux = window.top.ifrmRessarcimento.ifrmVlRessarcimento.reembolsoProdutoForm.txtVlSaldo.value.replace(".","");
	var vlDsSaldo = new Number(vlAux.replace(",","."));
	
	if (produtoLoteForm.reloNrTroca.value != "")
		qtdTroca = new Number(produtoLoteForm.reloNrTroca.value);
	
	if (produtoLoteForm.txtVlUnitProduto.value != ""){
		vlAux = "";
		vlAux = produtoLoteForm.txtVlUnitProduto.value.replace(".","");
		vlUnitProd = new Number(vlAux.replace(",","."));
	}	

	if (produtoLoteForm.txtVlProduto.value != ""){
		vlAux = "";
		vlAux = produtoLoteForm.txtVlProduto.value.replace(".","");
		vlAtualProduto = new Number(vlAux.replace(",","."));
	}	

	vlProduto = vlUnitProd * qtdTroca;
	vlProduto = vlProduto.toFixed(2);

	vlDiferenca = vlProduto - vlAtualProduto
	
	if (vlDiferenca + vlDsSaldo >= 0 )
		return true;
	else	
		return false;
	
}

function validaLote(){
	window.parent.validaLote();
}

function carregaProdDescontinuado(){
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
		if (produtoLoteForm.chkDecontinuado.checked == true)
			ifrmCmbLinha.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S";
		else{
			ifrmCmbLinha.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N";
		}
		
		ifrmCmbLinha.produtoLoteForm.tela.value='<%=MCConstantes.TELA_CMB_LINHA_LOTE%>';
        ifrmCmbLinha.produtoLoteForm.acao.value='<%=MCConstantes.ACAO_SHOW_ALL%>';
        ifrmCmbLinha.produtoLoteForm.submit();
	<%}%>
	
	ifrmCmbLinha.submeteForm();
	
}

function pressEnter(evnt) {
    if (evnt.keyCode == 13) {
    	validaLote();
    }
}


function habilitaMotLoteBranco(){
	if (trim(produtoLoteForm.reloDsLote.value).length == 0){
		lblMotivoLoteBranco.disabled = false;
		ifrmCmbMotivoLtBranco.document.produtoLoteForm.idMoloCdMotivolote.disabled = false;
	}else{
		lblMotivoLoteBranco.disabled = true;
		ifrmCmbMotivoLtBranco.document.produtoLoteForm.idMoloCdMotivolote.value = "";
		ifrmCmbMotivoLtBranco.document.produtoLoteForm.idMoloCdMotivolote.disabled = true;
	}
}

function habilitaMotivoRessarci(){

	//verifica se esta editando um lote
	if (produtoLoteForm.codigo.value != "0"){
		if (produtoLoteForm.reloInNaoRessarcir[1].checked){
			var vlProduto = produtoLoteForm.txtVlProduto.value;
			if (!lstLote.validaSaldo(vlProduto)){
				alert('<%=getMessage("prompt.Opcao_invalida_O_valor_do_Produto_e_maior_que_o_Saldo_a_Reembolsar",request)%>');
				produtoLoteForm.reloInNaoRessarcir[0].checked = true;
			}
		}
	}

	if (produtoLoteForm.reloInNaoRessarcir[1].checked){
		lblMotivo.disabled = false;
		ifrmCmbMotivo.document.produtoLoteForm.idMotrCdMotivotroca.disabled = false;
		produtoLoteForm.reloNrTroca.value = "";
	}else{
		lblMotivo.disabled = true;
		ifrmCmbMotivo.document.produtoLoteForm.idMotrCdMotivotroca.value = "";
		ifrmCmbMotivo.document.produtoLoteForm.idMotrCdMotivotroca.disabled = true;
		
		if(ifrmCmbProduto.produtoLoteForm["csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value != "")
			ifrmCmbReclamacao.submeteForm();
	}
	
	window.top.onclick_reloInNaoRessarcir();
	
}

function validaFlagAcessorio(){
	if (produtoLoteForm.codigo.value != ""){
		if (produtoLoteForm.reloInAcessorio.checked == false){
			if (lstLote.possuiAcessorio == true){
				if (window.top.ifrmRessarcimento.ifrmLstReembolso.possuiAcessorio == true){
					//alert ("Op��o inv�lida.\nExclua o acess�rio antes de escolher esta op��o.");
					alert ('<bean:message key="prompt.Opcao_invalidaExclua_o_acessorio_antes_de_escolher_esta_opcao"/>.');
					produtoLoteForm.reloInAcessorio.checked = true;
				}	
			}
		}
	}	
}	

function acaoJanelaCalendario(){
	//A��o da tela de calend�rio da data de validade
	//vWinCal.document.body.onunload = function(){
	//	corData();
	//}
}

var visualizaLotesReincidentes;
function visualizarLotes(){
	var valor = document.getElementById('lotesReincidentes').value;
	valor = replaceAll(valor, "\n", "<br>");
	valor = replaceAll(valor, " ", "&nbsp;");
	
	visualizaLotesReincidentes = new Object();
	visualizaLotesReincidentes.value = valor;

	showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp', visualizaLotesReincidentes, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')
}

function corData(){
	//Colocando cor da fonte vermelha caso seja menor que hoje
	sData = new String(produtoLoteForm.reloDhDtValidade.value);
	dia = sData.substring(0, 2);
	mes = sData.substring(3, 5);
	ano = sData.substring(6, sData.length);
	data = new Date(ano, mes-1, dia);
	hoje = new Date();
	hoje.setHours(0, 0, 0, 0);
	
	if(data < hoje){
		produtoLoteForm.reloDhDtValidade.style.color = "#FF0000";
	}
	else{
		produtoLoteForm.reloDhDtValidade.style.color = "#000000";
	}
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="0" class="principalBgrPageIFRM" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
<input type="hidden" name="codigo" value="0">
<input type="hidden" name="altLinha" value="0">
<input type="hidden" name="txtVlProduto" value="">
<input type="hidden" name="txtVlUnitProduto" value="">

<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td rowspan=8>&nbsp;</td>
  </tr>
  <tr> 
    <td class="principalLabel" colspan="3"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLabel" width="23%"><%=getMessage("prompt.linha",request)%></td>
          <td class="principalLabel" colspan="3" height="23" width="*">
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          			<tr>
          				<td class="principalLabel" width="35%">
          					<table width="100%" border="0" cellspacing="0" cellpadding="0">
				              <tr>
				                <td class="principalLabel" width="40%" height="23"><%=getMessage("prompt.Produto",request)%></td>
				                <td class="principalLabel" width="60%" height="23">
				                	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
				                  		<input type="checkbox" name="chkDecontinuado" value="" onclick="carregaProdDescontinuado()"><bean:message key="prompt.descontinuado"/></td>
				                  	<%}%>
				              </tr>
				            </table>
          				</td>
          				<td class="principalLabel" width="32%">
			          		<%=getMessage("prompt.Variedade",request)%>
          				</td>         
				        <td class="principalLabel" width="*">
				        	<%=getMessage("prompt.GrupoReclamacao",request)%>
				        </td>
          			</tr>
          		</table>
          	<%}else{%>
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          			<tr>
          				<td class="principalLabel" width="45%">
          					<table width="100%" border="0" cellspacing="0" cellpadding="0">
				              <tr>
				                <td class="principalLabel" width="40%" height="23"><%=getMessage("prompt.Produto",request)%></td>
				                <td class="principalLabel" width="60%" height="23">
				                	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
				                  		<input type="checkbox" name="chkDecontinuado" value="" onclick="carregaProdDescontinuado()"><bean:message key="prompt.descontinuado"/></td>
				                  	<%}%>
				              </tr>
				            </table>
          				</td>          				
				        <td class="principalLabel" width="*" height="23">
				        	<%=getMessage("prompt.GrupoReclamacao",request)%>
				        </td>
          			</tr>
          		</table>          	
          	<%}%>
          </td>
        </tr>
        <tr> 
          <td class="principalLabel" width="23%" height="23">
          	<iframe id=ifrmCmbLinha name="ifrmCmbLinha" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_LINHA_LOTE%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          </td>
          <td class="principalLabel" colspan="3" height="23" width="*">
          	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          			<tr>
          				<td class="principalLabel" width="35%" height="23">
          					<iframe id=ifrmCmbProduto name="ifrmCmbProduto" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_PRODUTO_LOTE%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          				</td>
          				<td class="principalLabel" width="32%" height="23">
			          		<iframe id="cmbVariedade" name="cmbVariedade" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_VARIEDADE%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0"></iframe>
			          		<!-- ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_VARIEDADE%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%> -->
          				</td>         
				        <td class="principalLabel" width="*" height="23">
				        	<iframe id=ifrmCmbGrupoManif name="ifrmCmbGrupoManif" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_GRUPOMANIF_LOTE%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
				        </td>
          			</tr>
          		</table>
          	<%}else{%>
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          			<tr>
          				<td class="principalLabel" width="45%" height="23">
          					<iframe id=ifrmCmbProduto name="ifrmCmbProduto" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_PRODUTO_LOTE%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          				</td>          				
				        <td class="principalLabel" width="*" height="23">
				        	<iframe id=ifrmCmbGrupoManif name="ifrmCmbGrupoManif" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_GRUPOMANIF_LOTE%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
				        </td>
          			</tr>
          		</table>          	
          	<%}%>
          </td>
        </tr>
        <tr> 
          <td class="principalLabel" width="23%"><%=getMessage("prompt.reclamacao",request)%></td>
          <td class="principalLabel" colspan="2" id="lblCondicaoUso"><bean:message key="prompt.CondicaoUso"/></td>
          <td class="principalLabel" width="27%"><bean:message key="prompt.SituacaoProduto"/></td>
        </tr>
        <tr> 
          <td class="principalLabel" width="23%" height="23"><iframe id=ifrmCmbReclamacao name="ifrmCmbReclamacao" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_MANIFESTACAO_LOTE%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
          <td class="principalLabel" colspan="2" height="23"><iframe id=ifrmCmbCondUso name="ifrmCmbCondUso" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_CONDICAO_USO%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
          <td class="principalLabel" width="27%" height="23"><iframe id=ifrmCmbSitProduto name="ifrmCmbSitProduto" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_SITUACAO_PROD%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalLabel" width="15%" colspan="2" id="lblDataFabricacao"><bean:message key="prompt.datafabricacao" /></td>
    <td class="principalLabel" width="85%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr class="principalLabel"> 
          <td width="15%"><bean:message key="prompt.lote" /></td>
          <td width="16%"><bean:message key="prompt.datavalidade" /></td>
          <td width="22%" id="lblFabrica"><bean:message key="prompt.fabrica" /></td>
          <td width="28%" height="23" id="lblMotivoLoteBranco"><bean:message key="prompt.MotivoLoteBranco"/></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="20%"> 
      <input type="text" name="reloDhDtFabricacao" class="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event);" onblur="verificaData(this);">
    </td>
    <td width="4%"> <img id="calDtFabricacao" name="id="calDtFabricacao"" src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" 
      width="16" height="15" class="geralCursoHand" onclick="show_calendar('produtoLoteForm.reloDhDtFabricacao')"> 
    </td>
    <td width="20%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="13%"> 
            <input type="text" name="reloDsLote" class="principalObjForm" maxlength="20" onkeydown="pressEnter(event)" onkeydown="habilitaMotLoteBranco()" onkeyup="habilitaMotLoteBranco()">
          </td>
          <%--td width="2%"> <img src="webFiles/images/botoes/check.gif" width="11" height="12" title="<bean:message key="prompt.validarLote" />" 
            onclick="validaLote()" class="geralCursoHand" > </td --%>
          <td width="2%"> &nbsp;</td>
          <td width="13%"> 
            <input type="text" name="reloDhDtValidade" class="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event);" onblur="corData();verificaData(this);">
          </td>
          <td width="3%"> <img id="calendarioValidade" name="calendarioValidade" src="webFiles/images/botoes/calendar.gif" width="16" title="<bean:message key="prompt.calendario" />" 
            height="15" class="geralCursoHand" onclick="show_calendar('produtoLoteForm.reloDhDtValidade');acaoJanelaCalendario();"> 
          </td>
          <td width="20%" height="23"> <iframe name="cmbFabrica" src="ProdutoLote.do?acao=showAll&tela=cmbFabrica&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
          <td width="30%" height="23" align="right"> 
            <table width="96%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="23"><iframe id=ifrmCmbMotivoLtBranco name="ifrmCmbMotivoLtBranco" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_MOTIVO_LT_BRANCO%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
  </tr>
  <tr> 
    <td colspan="3"> 
      <hr>
    </td>
  </tr>
  <tr> 
    <td colspan="3"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="22%" class="principalLabel" id="lblQtdComprada"><bean:message key="prompt.qtdcomprada" /></td>
          <td width="23%" class="principalLabel"><bean:message key="prompt.qtdreclamada" /></td>
          <td class="principalLabel" id="lblQtdDisponivelFechada"><bean:message key="prompt.qtddisponivelfechada" /></td>
          <td class="principalLabelValorFixo"><bean:message key="prompt.Ressarcir"/></td>
          <td class="principalLabel" id="lblMotivo"><bean:message key="prompt.Motivo"/></td>
        </tr>
        <tr> 
          <td width="22%"> 
            <input type="text" name="reloNrComprada" class="principalObjForm" onkeydown="return isDigito(event)" maxlength="5">
          </td>
          <td width="23%"> 
            <input type="text" name="reloNrReclamada" class="principalObjForm" onkeydown="return isDigito(event)" maxlength="5">
          </td>
          <td width="18%"> 
            <input type="text" name="reloNrDisponivel" class="principalObjForm" onkeydown="return isDigito(event)" maxlength="5">
          </td>
          <td class="principalLabel" width="14%"> 
            <html:radio property="reloInNaoRessarcir" value="" onclick="habilitaMotivoRessarci()"/><bean:message key="prompt.sim"/> 
            <html:radio property="reloInNaoRessarcir" value="" onclick="habilitaMotivoRessarci()"/><bean:message key="prompt.nao"/>
          </td>
          <td class="principalLabel" width="23%" height="23"> 
            <iframe id=ifrmCmbMotivo name="ifrmCmbMotivo" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_MOTIVO%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="103%" height="20" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
        </tr>
        <tr> 
          <td width="22%" class="principalLabel" id="lblQtdDisponivelAberta"><bean:message key="prompt.qtddisponivelaberta" /></td>
          <td width="23%" class="principalLabel" id="lblQtdTrocaRepor"><bean:message key="prompt.qtdtrocarepor" /></td>
		  <td colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
	          <td class="principalLabel" width="30%"></td>
	          <td class="principalLabel" width="65%" id="lblDestinoProduto"><bean:message key="prompt.DestinoProduto"/></td>
	          <td class="principalLabel" width="5%">&nbsp;</td>
			</tr>
			</table> 
		  </td>	
          <td class="principalLabel" width="23%" id="lblLaboratorio"><bean:message key="prompt.Laboratorio"/></td>
        </tr>
        <tr> 
          <td width="22%"> 
            <input type="text" name="reloNrAberta" class="principalObjForm" onkeydown="return isDigito(event)" maxlength="5">
          </td>
          <td width="23%"> 
            <input type="text" name="reloNrTroca" class="principalObjForm" onkeydown="return isDigito(event)" maxlength="5">
          </td>
          <td colspan="2">
	          <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          	<tr>
		          <td height="23" width="30%" class="principalLabel" align="left" id="lblAcessorio">
		            <html:checkbox property="reloInAcessorio" value="" onclick="validaFlagAcessorio()"/><bean:message key="prompt.Acessorio"/>
		          </td>
                  <td width="65%" height="23" class="principalLabel" align="center"> 
					<iframe id=ifrmCmbDestino name="ifrmCmbDestino" src="<%=Geral.getActionProperty("produtoLoteAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_CMB_DESTINO%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="106%" height="20" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
				  </td>
		          <td height="23" width="5%" class="principalLabel">
					<!-- 
						Este checked deve ficar invisivel e n�o pode ser removido da p�gina
						Ele � utilizado para controle do combo de laborat�rio e para a tela de laudo
						O conteudo do checke � controlado pelo combo de Destino Produto 
					-->	          	 
		          	<div id="LayerInEnviarAnalise" style="z-index:1; visibility: hidden">
			            <input type="checkbox" name="reloInAnalise" value="S" disabled="true" onclick="habilitaLaboratorio()" >
				        <!--bean:message key="prompt.enviaranalise" /-->
			        </div>    
		            
		          </td>
	          </tr>
          </table>   
          
          <td class="principalLabel" width="23%" height="23"> <iframe name="cmbLaboratorio" src="ProdutoLote.do?acao=showAll&tela=cmbLaboratorio&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="99%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="3"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="125">
        <tr> 
          <td colspan="9" valign="top" class="principalLabel" height="2">&nbsp;</td>
        </tr>
        <tr height="2"> 
          <td class="principalLstCab" width="2%">&nbsp;</td>
          <td class="principalLstCab" width="26%"><%=getMessage("prompt.produto",request)%></td>
          <td class="principalLstCab" width="12%"><bean:message key="prompt.quest" /></td>
          <td class="principalLstCab" width="12%"><bean:message key="prompt.LocalCompra" /></td>
          <td class="principalLstCab" width="12%"><bean:message key="prompt.Ressarcir" /></td>
          <td class="principalLstCab" width="12%"><bean:message key="prompt.Acessorio" /></td>
          <td class="principalLstCab" width="12%"><bean:message key="prompt.lote" /></td>
          <td class="principalLstCab" width="12%"><bean:message key="prompt.datavalidade" /></td>
          <td width="2%">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="8" width="98%" valign="top" class="principalBordaQuadro" height="80"> 
            <iframe id=lstLote name="lstLote" src="ProdutoLote.do?acao=showAll&tela=lstInfoLote&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />" width="100%" height="100%" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          </td>
          <td width="2%" valign="top"> <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" class="geralCursoHand" onclick="adicionarLote()"><br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="3">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLabel" width="47%"><bean:message key="prompt.lotesreincidentes" /></td>
          <td class="principalLabel" width="2%">&nbsp;</td>
          <td class="principalLabel" width="5%">&nbsp;</td>
          <td class="principalLabel" width="9%"><bean:message key="prompt.comprada" /></td>
          <td class="principalLabel" width="9%"><bean:message key="prompt.reclamada" /></td>
          <td class="principalLabel" width="9%"><bean:message key="prompt.fechada" /></td>
          <td class="principalLabel" width="9%"><bean:message key="prompt.aberta" /></td>
          <td class="principalLabel" width="10%"><bean:message key="prompt.trocar" /></td>
        </tr>
        <tr> 
          <td width="48%"> 
            <textarea name="lotesReincidentes" id="lotesReincidentes" class="principalObjForm2" rows="5" readonly="readonly"></textarea>
          </td>
          <td valign="bottom" width="2%"> <img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="visualizarLotes();" title="<bean:message key="prompt.visualizar"/>"> 
          </td>
          <td class="principalLabel" align="right" width="5%" valign="top"><bean:message key="prompt.totais" /> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="9%" valign="top"> 
            <input type="text" name="txtComprada" class="principalObjForm" readonly="readonly">
          </td>
          <td width="9%" valign="top"> 
            <input type="text" name="txtReclamada" class="principalObjForm" readonly="readonly">
          </td>
          <td width="9%" valign="top"> 
            <input type="text" name="txtFechada" class="principalObjForm" readonly="readonly">
          </td>
          <td width="9%" valign="top"> 
            <input type="text" name="txtAberta" class="principalObjForm" readonly="readonly">
          </td>
          <td width="9%" valign="top"> 
            <input type="text" name="txtTroca" class="principalObjForm" readonly="readonly">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<input type="hidden" name="lotesReincidentesAux" id="lotesReincidentesAux" />

<%--iframe id=ifrmValidaLote name="ifrmValidaLote" src="ProdutoLote.do?tela=<%=MCConstantes.TELA_VALIDA_LOTE%>&acao=<%=MCConstantes.ACAO_SHOW_NONE%>" width="0" height="0" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe--%>
<div id="camposHidden"></div>
</html:form>
</body>
</html>