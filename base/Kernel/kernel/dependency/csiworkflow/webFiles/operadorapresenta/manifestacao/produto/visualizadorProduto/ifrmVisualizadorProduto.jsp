<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,
	com.iberia.helper.Constantes,
	br.com.plusoft.csi.adm.util.Geral,
	br.com.plusoft.fw.app.Application"%>
	
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>
	
<html>
<head>
<title>CSI - PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">	

function ChamaTree() {
	
	if (visualizadorProdutoForm.SelectTree[0].checked == true){
		visualizadorProdutoForm.tipoTreeView.value = "P";
	}else if (visualizadorProdutoForm.SelectTree[1].checked == true){
		visualizadorProdutoForm.tipoTreeView.value = "F";
	}

	carregaProduto();
		
}	


function carregaCaracteristica(){
	var url="";

	url = "VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_CMB_CARACT_VISUAL_PRAS%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>";
	url = url + "&idLinhCdLinha=" + visualizadorProdutoForm.idLinhCdLinha.value;
	
	cmbCaractIframe.location.href = url;
	
}

var nLinha=0;
function addFiltro(){

var strTxt="";

	
	
	var idLinh = visualizadorProdutoForm.idLinhCdLinha.value;
	var idCapr = cmbCaractIframe.document.visualizadorProdutoForm.idCaprCdCaracteristicaprod.value;
	var idRtcp = cmbRespTabIframe.document.visualizadorProdutoForm.idRtcpRespTabCaractProd.value;

	if (exitefiltro(idLinh,idCapr,idRtcp)){
		return false;
	}
	
	if (idRtcp == 0){
		alert ('<bean:message key="prompt.Os_campos_linhaCaracteristicasConteudo_sao_obrigatorios"/>');
		return false;
	}
	
	nLinha++;
	
	var dsLinh = visualizadorProdutoForm.idLinhCdLinha.options[visualizadorProdutoForm.idLinhCdLinha.selectedIndex].text;
	var dsCapra = cmbCaractIframe.document.visualizadorProdutoForm.idCaprCdCaracteristicaprod.options[cmbCaractIframe.document.visualizadorProdutoForm.idCaprCdCaracteristicaprod.selectedIndex].text;
	var dsRtcp = cmbRespTabIframe.document.visualizadorProdutoForm.idRtcpRespTabCaractProd.options[cmbRespTabIframe.document.visualizadorProdutoForm.idRtcpRespTabCaractProd.selectedIndex].text;

	strTxt = strTxt + "<div id=\"lstFiltro" + nLinha + "\" style='width:100%; overflow: auto'>";
    strTxt = strTxt + "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	strTxt = strTxt + " <tr> ";
	strTxt = strTxt + " <input type='hidden' name='idLinh' value='" + idLinh + "'>";
	strTxt = strTxt + " <input type='hidden' name='idCapr' value='" + idCapr + "'>";
	strTxt = strTxt + " <input type='hidden' name='idRtcp' value='" + idRtcp + "'>";
	strTxt = strTxt + "   <td class='principalLstPar' width='3%'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeFiltro('" + nLinha + "')\"></td>";
	strTxt = strTxt + "   <td class='principalLstPar' width='33%'>" + dsLinh + "</td>";
	strTxt = strTxt + "   <td class='principalLstPar' width='33%'>" + dsCapra + "</td>";
	strTxt = strTxt + "   <td class='principalLstPar' width='31%'>" + dsRtcp + "</td>";
	strTxt = strTxt + " </tr>";
	strTxt = strTxt + "</table>";
	strTxt = strTxt + "</div>";
	
	document.getElementById("layerFiltro").innerHTML += strTxt;
	
	visualizadorProdutoForm.idLinhCdLinha.disabled = true;
	limpaCampos();

}

function exitefiltro(idLinh,idCapr,idRtcp){
	
	if (nLinha > 1){
		for (i=0;i<visualizadorProdutoForm.idLinh.length;i++){
			if (idLinh == visualizadorProdutoForm.idLinh[i].value && 
				idCapr == visualizadorProdutoForm.idCapr[i].value && 
				idRtcp == visualizadorProdutoForm.idRtcp[i].value ){
					alert('<bean:message key="prompt.Este_filtro_ja_existe_na_lista"/>');
					return true;								
			}
		}
	}else if (nLinha == 1){
		if (idLinh == visualizadorProdutoForm.idLinh.value && 
			idCapr == visualizadorProdutoForm.idCapr.value && 
			idRtcp == visualizadorProdutoForm.idRtcp.value ){
				alert('<bean:message key="prompt.Este_filtro_ja_existe_na_lista"/>');
				return true;								
		}
	}
	
	return false;

}

function limpaCampos(){
	
	//visualizadorProdutoForm.idLinhCdLinha.value=0;
	cmbCaractIframe.document.visualizadorProdutoForm.idCaprCdCaracteristicaprod.value=0;
	cmbRespTabIframe.location.href = "VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_CMB_RESPTAB_VISUAL_PRAS%>"
	

}

function removeFiltro(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.item"/>';
	if (confirm(msg)) {
		objIdTbl = document.getElementById("lstFiltro" + nTblExcluir);
		layerFiltro.removeChild(objIdTbl);
		nLinha --;
		
		if (nLinha == 0){
			visualizadorProdutoForm.idLinhCdLinha.disabled = false;
			visualizadorProdutoForm.idLinhCdLinha.value=0;
		}
		
		limpaIframes();
		limpaTreeView();
		
		
	}
}

function limpaTreeView(){
	
	visualizadorProdutoForm.SelectTree[0].selected = true;
	ifrmTreeview.location.href = "VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_TREEVIEW%>&acao=<%=Constantes.ACAO_CONSULTAR%>"
}

function carregaProduto(){
	var filtroLinha = "";
	var filtroCar = "";
	var filtroRespTab = "";
	
	if (nLinha==0 && visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value==""){
		alert ('<bean:message key="prompt.Selecione_um_filtro_para_pesquisa"/>');
		return false;
	}	

	if (nLinha > 1){
		for(i=0;i<visualizadorProdutoForm.idCapr.length;i++){
			filtroLinha += visualizadorProdutoForm.idLinh[i].value;
			filtroCar += visualizadorProdutoForm.idCapr[i].value;
			filtroRespTab += visualizadorProdutoForm.idRtcp[i].value;
			if (i<(visualizadorProdutoForm.idCapr.length - 1)){
				filtroLinha += ",";
				filtroCar += ",";
				filtroRespTab += ",";
			}		
		}
	}else if (nLinha == 1){
		filtroLinha = visualizadorProdutoForm.idLinh.value;
		filtroCar = visualizadorProdutoForm.idCapr.value;
		filtroRespTab = visualizadorProdutoForm.idRtcp.value;
	}
	
	ifrmTreeview.document.visualizadorProdutoForm.filtroLinha.value = filtroLinha;
	ifrmTreeview.document.visualizadorProdutoForm.filtroCar.value = filtroCar;
	ifrmTreeview.document.visualizadorProdutoForm.filtroRespTab.value = filtroRespTab;
	ifrmTreeview.document.visualizadorProdutoForm.tipoTreeView.value = visualizadorProdutoForm.tipoTreeView.value;
	ifrmTreeview.document.visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value = visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value;
	ifrmTreeview.document.visualizadorProdutoForm.tela.value = '<%=MCConstantes.TELA_IFRM_TREEVIEW%>';
	ifrmTreeview.document.visualizadorProdutoForm.acao.value = '<%=Constantes.ACAO_CONSULTAR%>';
	
	ifrmTreeview.document.visualizadorProdutoForm.submit();
	
	limpaIframes();
	
}

function limpaIframes(){
	ifrmDadosProduto.location.href="VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_DADOS_PRODUTO%>";
	ifrmImagem.location.href="VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_DADOS_IMAGEM%>";
}

function carregaDadosProduto(filtroPras){
	var url="";
	
	url = "VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_DADOS_PRODUTO%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>";
	url = url + "&filtroPras=" + filtroPras;
	
	ifrmDadosProduto.location.href = url;
	
}

function carregaImagensProduto(filtroPras){
	var url="";
	
	url = "VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_DADOS_IMAGEM%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>";
	url = url + "&filtroPras=" + filtroPras;
	
	ifrmImagem.location.href = url;

}

function pressEnter(evnt) {
    if (evnt.keyCode == 13) {
	    evnt.keyCode = 0;
    	carregaProduto();
    	return false;
    }
}


function carregaInfAdicionaisPras(){
	showModalDialog('VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_INFADICIONAIS_PRAS%>',window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:390px,dialogTop:0px,dialogLeft:650px');
}


function iniciarTela(){
	
	//inicializa treeview com produto
	visualizadorProdutoForm.tipoTreeView.value="P"; 
	
	setStatusTela();
}

function carregaObservacao(){
	document.getElementById("txObservacao").innerHTML = ifrmDadosProduto.visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasTxObservacao'].value;
}

var statusTela="CONSULTA";

function setStatusTela(){

	var filtroPras = visualizadorProdutoForm['filtroPras'].value;

	if (filtroPras != ""){
		//tela no modo Visualizar
		statusTela = "VISUALIZAR";
		
		posicionaTela();
		carregaDadosProduto(filtroPras);
		carregaImagensProduto(filtroPras);
		desabilitaCampos(true);
		
	}

}

function posicionaTela(){
	window.document.getElementById("layerDadosProduto").style.top = 260;
	window.document.getElementById("layerDadosProduto").style.left = 15;

//	window.document.getElementById("layerDadosProduto").style.top = 212;
	window.document.getElementById("layerDadosProduto").style.height = 310;
	window.document.getElementById("tableDadosProd").height=295;
}

function desabilitaCampos(status){
	visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].disabled = status;
	visualizadorProdutoForm.idLinhCdLinha.disabled = status;
	cmbCaractIframe.document.visualizadorProdutoForm.idCaprCdCaracteristicaprod.disabled = status;
	cmbRespTabIframe.document.visualizadorProdutoForm.idRtcpRespTabCaractProd.disabled = status;	
	
	window.document.getElementById("imgSetaDown").disabled = status;
	window.document.getElementById("imgSetaDown").className = "geralImgDisabled";
	
	window.document.getElementById("imgLupa").disabled = status;
	window.document.getElementById("imgLupa").className = "geralImgDisabled";
	
	window.document.getElementById("imgConfirmaPras").disabled = status;
	window.document.getElementById("imgConfirmaPras").className = "geralImgDisabled";
}


function setValoresForm(){
	var wi = window.dialogArguments;
	var idLinh = ifrmDadosProduto.document.visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;
	var idAsn1 = ifrmDadosProduto.document.visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	var idAsn2 = ifrmDadosProduto.document.visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	var asn2 = ifrmDadosProduto.document.visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2'].value;
	
	if (idAsn1 != "0" && idAsn2 != "0"){
		wi.setValoresProduto(idLinh,idAsn1,idAsn2, asn2);
		window.close();
		
	}else{
		alert ("Nenhum produto selecionado.");
		return false;
	}	
}

</script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();">
<html:form styleId="visualizadorProdutoForm" action="/VisualizadorProduto.do">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="filtroPras"/>
<html:hidden property="tipoTreeView"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroGiant" height="17" width="166"> <bean:message key="prompt.visualiza_produto" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabelValorFixo" height="20"> 
                  <div id="texto" style="width:385px; height:12px; visibility: visible" class="principalLabelValorFixo">
                  	<label id="descProduto"></label>
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn" height="2">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabel" width="25%">Procurar por</td>
                <td class="principalLabel" width="25%">&nbsp;</td>
                <td class="principalLabel" width="25%">&nbsp;</td>
                <td class="principalLabel" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="25%"> 
                  <html:text property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" styleClass="principalObjForm" onkeydown="return pressEnter(event);"/>
                </td>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp; </td>
                <td width="25%">&nbsp; </td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabel" width="25%">Linha</td>
                <td class="principalLabel" width="25%">Caracter&iacute;stica</td>
                <td class="principalLabel" width="25%">Conte&uacute;do</td>
                <td class="principalLabel" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="25%">
				  <html:select property="idLinhCdLinha" styleClass="principalObjForm" onchange="carregaCaracteristica();">
					<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
				    <logic:present name="linhaVector">
					  <html:options collection="linhaVector" property="csCdtbLinhaLinhVo.idLinhCdLinha" labelProperty="csCdtbLinhaLinhVo.linhDsLinha" />
					</logic:present>
				  </html:select>
                </td>
                <td width="25%" valign="middle">
	                <iframe id=cmbCaractIframe name="cmbCaractIframe" src="VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_CMB_CARACT_VISUAL_PRAS%>" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                </td>
                <td width="25%"> 
					<iframe id=cmbRespTabIframe name="cmbRespTabIframe" src="VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_CMB_RESPTAB_VISUAL_PRAS%>" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>	
                </td>
                <td width="25%"> <img id="imgSetaDown" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title='<bean:message key="prompt.confirmar"/>' onclick="addFiltro();"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="96%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td class="principalLstCab" width="3%">&nbsp</td> 
                      <td class="principalLstCab" width="31%">Linha</td>
                      <td class="principalLstCab" width="33%">Caracter&iacute;sticas</td>
                      <td class="principalLstCab" width="32%">Conte&uacute;do</td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="60" class="principalBordaQuadro">
                    <tr> 
                      <td valign="top"> 
                        <div id="layerFiltro" style="width:100%; height:55px; overflow: auto; visibility: visible"> 
						
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="3%" align="center"><img id="imgLupa" src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="carregaProduto();"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td valign="top" align="center"> 
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
                    <tr> 
                      <td height="370" valign="top"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="50%" valign="top"> 
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                                <tr>
                                  <td>&nbsp;</td>
                                </tr>
                              </table>
                              <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr> 
                                  <td height="25" valign="top"> 
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr> 
                                        <td class="principalLabel" width="23%"> 
                                          <input type="radio" name="SelectTree" value="radiobutton" onclick="ChamaTree()" checked>
                                          Produto </td>
                                        <td class="principalLabel" width="77%"> 
                                          <input type="radio" name="SelectTree" value="radiobutton" onclick="ChamaTree()">
                                          Fornecedor </td>
                                      </tr>
                                    </table>  
                                   </td>                                  
                                </tr>
                                <tr> 
                                  <td height="130" valign="top"> 
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
                                      <tr> 
                                   		<td>
                                   			 <iframe id=ifrmTreeview name="ifrmTreeview" src="VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_TREEVIEW%>&acao=<%=Constantes.ACAO_CONSULTAR%>" width="100%" height="130" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                                   		</td>     
                                      </tr>
                                    </table>
                                   </td>
                                </tr>
                              </table>
                              <div id="layerDadosProduto" style="position:absolute; left:15px; top:425px; width:402px; height: 100px; z-index:1; ">
	                              <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	                                <tr> 
	                                  <td class="principalLstCab">&nbsp;Dados do Produto</td>
	                                </tr>
	                              </table>
	                              <table id="tableDadosProd" width="98%" border="0" cellspacing="0" cellpadding="0" height="120" class="principalBordaQuadro" align="center">
	                                <tr> 
	                                  <td valign="top" height="100%">
											<iframe id=ifrmDadosProduto name="ifrmDadosProduto" src="VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_DADOS_PRODUTO%>" width="100%" height="100%" frameborder="0" scrolling="Default" marginwidth="0" marginheight="0" ></iframe>
	                                  </td>
	                                </tr>
	                              </table>
                              </div>
                            </td>
                            <td width="50%" valign="top"> 
                              <table width="100%" border="0" height="117" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td class="espacoPqn" height="7">&nbsp;</td>
                                </tr>
                                <tr> 
                                  <td>
	                                  <iframe id=ifrmImagem name="ifrmImagem" src="VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_DADOS_IMAGEM%>" width="100%" height="107" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                                  </td>
                                </tr>
                              </table>
                              <div class="principalLstCab" style="margin-left:4px; width:394px;">&nbsp;Informa&ccedil;&otilde;es Adicionais</div>
                              <div id="txObservacao" style="border: 1px #7088c5 solid; margin-left:4px;width:394px;height:190px;overflow:auto">
                              
                              </div>
                             </td>
                          </tr>
                        </table>
                        <br/>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr align="right" >
								<td class="principalLabel" width="84%"><img src="webFiles/images/botoes/Msg_Envio.gif" width="21" height="20" class="geralCursoHand" onClick="carregaInfAdicionaisPras();"></td>
								<td class="principalLabel" width="15%"><span class="geralCursoHand" onClick="carregaInfAdicionaisPras();">Informa&ccedil;&otilde;es Adicionais  </span></td>
								<td class="principalLabel" width="1%">&nbsp;</td>
							</tr>
						</table>
                      </td>
                    </tr>
                  </table>
                  <table><tr><td height="10"></td></tr></table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr> 
    <td width="94%" align="right"> 
      <img src="webFiles/images/icones/agendamentos01.gif" id="imgConfirmaPras" width="22" height="23" border="0" title="<bean:message key='prompt.confirmaProduto'/>" onClick="setValoresForm();" class="geralCursoHand">
    </td>
    <td width="5%" align="right"> 
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.voltar'/>" onClick="javascript:window.close()" class="geralCursoHand">
    </td>
    <td width="1%">&nbsp;</td> 
  </tr>
</table>
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>