<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
var wndoptions = "width=50,height=50,top=5,left=5,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1";
var obj = new Object();

function iniciarTela(){
	//parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';	
	parent.ifrmManifArq.location.href = "ManifArquivo.do?tela=ifrmManifArq";

}


function excluirArquivoTemp(idMaarCdManifArquivo,martDsSessao){
	
	if(!confirm('<bean:message key="prompt.alert.remov.item"/>'))
		return false;
	
	//parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';	
	
	manifArquivoForm.tela.value = '<%=MCConstantes.TELA_IFRM_LST_MANIFARQUIVO%>';
	manifArquivoForm.acao.value = '<%=Constantes.ACAO_EXCLUIR%>';
	
	manifArquivoForm['csNgtmManifArqTempMartVo.idMartCdManifArqTemp'].value = idMaarCdManifArquivo;
	manifArquivoForm['csNgtmManifArqTempMartVo.martDsSessao'].value = martDsSessao;
	manifArquivoForm.submit();

}

function excluirArquivo(idMaarCdManifArquivo){

	if(!confirm('<bean:message key="prompt.alert.remov.item"/>'))
		return false;

	//parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';	
	
	manifArquivoForm.tela.value = '<%=MCConstantes.TELA_IFRM_LST_MANIFARQUIVO%>';
	manifArquivoForm.acao.value = '<%=Constantes.ACAO_EXCLUIR%>';
	manifArquivoForm['csAstbManifArquivoMaarVo.idMaarCdManifArquivo'].value = idMaarCdManifArquivo;
	manifArquivoForm.submit();		
}

function downLoadArquivoTemp(idMartCdManifArqTemp,martDsSessao){

	var url="";
	url = "ManifArquivo.do?tela=<%=MCConstantes.TELA_IFRM_DOWNLOAD_MANIFARQUIVO%>";
	url = url + "&csNgtmManifArqTempMartVo.idMartCdManifArqTemp=" + idMartCdManifArqTemp;
	url = url + "&csNgtmManifArqTempMartVo.martDsSessao=" + martDsSessao;
	url = url + "&csAstbManifArquivoMaarVo.idMaarCdManifArquivo=0";
	
	ifrmDownloadManifArquivo.location = url;
	
	obj = window.open(url, 'downloadManifArq', wndoptions, true);

	//setTimeout("fecharJanela();", 1000);
	
}

function downLoadArquivo(idMaarCdManifArquivo){
	
	var url="";
	url = "ManifArquivo.do?tela=<%=MCConstantes.TELA_IFRM_DOWNLOAD_MANIFARQUIVO%>";
	url = url + "&idChamCdChamado=" + manifArquivoForm.idChamCdChamado.value;
	url = url + "&maniNrSequencia=" + manifArquivoForm.maniNrSequencia.value;
	url = url + "&idAsn1CdAssuntoNivel1=" + manifArquivoForm.idAsn1CdAssuntoNivel1.value;
	url = url + "&idAsn2CdAssuntoNivel2=" + manifArquivoForm.idAsn2CdAssuntoNivel2.value;
	url = url + "&csAstbManifArquivoMaarVo.idMaarCdManifArquivo=" + idMaarCdManifArquivo;
	url = url + "&csNgtmManifArqTempMartVo.idMartCdManifArqTemp=0";
	
	//ifrmDownloadManifArquivo.location = url;
	
	obj = window.open(url, 'downloadManifArq', wndoptions, true);
	
	//setTimeout("fecharJanela();", 1000);
}
	
function fecharJanela(){
	if(!obj.closed){
		obj.close();
		setTimeout("fecharJanela();", 100);
	}
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();">
<html:form action="/ManifArquivo.do" styleId="manifArquivoForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>

<html:hidden property="idChamCdChamado"/>
<html:hidden property="maniNrSequencia"/>
<html:hidden property="idAsn1CdAssuntoNivel1"/>
<html:hidden property="idAsn2CdAssuntoNivel2"/>

<html:hidden property="csNgtmManifArqTempMartVo.idMartCdManifArqTemp"/>
<html:hidden property="csNgtmManifArqTempMartVo.martDsSessao"/>
<html:hidden property="csAstbManifArquivoMaarVo.idMaarCdManifArquivo"/>


<div id="Layer1" style="width:100%; height:126px; z-index:1; overflow: auto">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<!--temporários-->
		<logic:present name="manifArqTempVector">
		  <logic:iterate id="manifArqTempVector" name="manifArqTempVector">
		  <tr> 
		    <td class="principalLstPar" width="3%">&nbsp;<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" title='<bean:message key="prompt.excluir"/>' class="geralCursoHand" onclick="excluirArquivoTemp('<bean:write name="manifArqTempVector" property="idMartCdManifArqTemp"/>','<bean:write name="manifArqTempVector" property="martDsSessao"/>');"></td>
		    <td class="principalLstPar" width="32%"><span class="geralCursoHand" title="<bean:message key='prompt.baixarArquivo'/>" onclick="downLoadArquivoTemp('<bean:write name="manifArqTempVector" property="idMartCdManifArqTemp"/>','<bean:write name="manifArqTempVector" property="martDsSessao"/>');"><plusoft:acronym name="manifArqTempVector" property="martDsManifArqTemp" length="40"/></span>&nbsp;</td>
		    <td class="principalLstPar" width="60%">&nbsp;</td>
		    <td class="principalLstPar" width="5%">&nbsp;
		    	<logic:equal  name="manifArqTempVector" property="martInOrigem" value="C">
		    		<img src="/plusoft-resources/images/botoes/CartaAmarela.gif" width="14" height="14" title="<bean:message key="prompt.classificadorDeEmail"/>" />
		    	</logic:equal>
		    	<logic:notEqual  name="manifArqTempVector" property="martInOrigem" value="C">
		    		<img src="/plusoft-resources/images/botoes/Processo.gif" width="14" height="14" title="<bean:message key="prompt.manifestacao"/>" />
		    	</logic:notEqual>
		    </td>
		  </tr>
		  </logic:iterate>
		</logic:present>  

		<!--Gravados em banco-->
		<logic:present name="manifArqVector">
		  <logic:iterate id="manifArqVector" name="manifArqVector">
		  <tr> 
		    <td class="principalLstPar" width="3%">&nbsp;<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" title="<bean:message key="prompt.excluir"/>" class="geralCursoHand" onclick="excluirArquivo('<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>');"></td>
		    <td class="principalLstPar" width="60%"><span class="geralCursoHand" onclick="downLoadArquivo('<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>');"><plusoft:acronym name="manifArqVector" property="maarDsManifArquivo" length="40"/></span>&nbsp;</td>
		    <td class="principalLstPar" width="32%" style="text-align: center;">&nbsp;

			<logic:notEqual name="manifArqVector" property="idExpuCdExpurgo" value="0">
		    
			    <logic:greaterThan name="manifArqVector" property="idRearCdRespositorioArq" value="0">	
					<span title="<bean:write name="manifArqVector" property="rearDsPath"/>/manifestacao/">
			    		<img src="/plusoft-resources/images/icones/fs.png" style="vertical-align: middle;" /> <bean:write name="manifArqVector" property="expuDhExpurgo"/>
			    	</span>			    	    	
			    </logic:greaterThan>

				<logic:equal name="manifArqVector" property="idRearCdRespositorioArq" value="0">
					<span title="<bean:message key='prompt.registroExpurgado'/>">
		    			<img src="/plusoft-resources/images/icones/db16.gif" style="vertical-align: middle;" /> <bean:write name="manifArqVector" property="expuDhExpurgo"/>
		    		</span>		    	
				</logic:equal>

		    </logic:notEqual>

		    </td>
		    <td class="principalLstPar" width="5%">&nbsp;
		    	<logic:equal  name="manifArqVector" property="maarInOrigem" value="C">
		    		<img src="/plusoft-resources/images/botoes/CartaAmarela.gif" width="14" height="14" title="<bean:message key="prompt.classificadorDeEmail"/>" />
		    	</logic:equal>
		    	<logic:notEqual name="manifArqVector" property="maarInOrigem" value="C">
		    		<img src="/plusoft-resources/images/botoes/Processo.gif" width="14" height="14" title="<bean:message key="prompt.manifestacao"/>" />
		    	</logic:notEqual>
		    </td>
		  </tr>
		  </logic:iterate>
		</logic:present>  
		
	</table>
</div>
</html:form>

<iframe name="ifrmDownloadManifArquivo" src="" width="0" height="0" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>

</body>
</html>
