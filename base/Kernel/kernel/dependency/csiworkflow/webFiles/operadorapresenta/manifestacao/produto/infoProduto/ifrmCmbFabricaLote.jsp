<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
	function iniciaTela(){
		if(produtoLoteForm.cmbIdFabrCdFabrica.length == 2){
			produtoLoteForm.cmbIdFabrCdFabrica.selectedIndex = 1;
		}
		parent.document.getElementById("lblFabrica").disabled = false;
	}	
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
  <html:hidden property="acao" value="showAll" />
  <html:hidden property="tela" value="cmbFabrica" />
  
  <html:select property="cmbIdFabrCdFabrica" styleClass="principalObjForm">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbFabricaFabrVector">
	  <html:options collection="csCdtbFabricaFabrVector" property="idFabrCdFabrica" labelProperty="fabrDsFabrica"/>
	</logic:present>
  </html:select>
</html:form>
</body>
</html>