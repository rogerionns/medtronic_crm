<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
	function submeteForm() {
		reembolsoProdutoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		reembolsoProdutoForm.tela.value = '<%=MCConstantes.TELA_CMB_PRODUTO_ACESSORIO%>';
		reembolsoProdutoForm.target = parent.ifrmCmbProdutoAcessorio.name;
		reembolsoProdutoForm.submit();
		
		reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = "";
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />

  <html:select property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" styleClass="principalObjForm" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbLinhaLinhVector">
      <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
    </logic:present>
  </html:select>

</html:form>
</body>
</html>
