<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">


</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');">

<html:form styleId="visualizadorProdutoForm" action="/VisualizadorProduto.do">
<html:hidden property="acao" />
<html:hidden property="tela" />

  <div id="layerImg" style="position:absolute; width:100%; height:160px; margin-top:10px; z-index:2; visibility: visible; overflow: auto "> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <logic:equal name="visualizadorProdutoForm" property="acao" value="<%=Constantes.ACAO_VISUALIZAR%>">
	      <tr> 
	        <td align="center"><img src="VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_IMG_PRAS%>&acao=<%=Constantes.ACAO_CONSULTAR%>&csCdtbProdutoImagemPrimVo.idPrimCdProdutoImagem=<bean:write name='visualizadorProdutoForm' property='csCdtbProdutoImagemPrimVo.idPrimCdProdutoImagem'/>&csCdtbProdutoImagemPrimVo.idAsn1CdAssuntonivel1=<bean:write name='visualizadorProdutoForm' property='csCdtbProdutoImagemPrimVo.idAsn1CdAssuntonivel1'/>&csCdtbProdutoImagemPrimVo.idAsn2CdAssuntonivel2=<bean:write name='visualizadorProdutoForm' property='csCdtbProdutoImagemPrimVo.idAsn2CdAssuntonivel2'/>" width="210" height="160"></td>
	      </tr>
	  </logic:equal>    
	  <logic:notEqual name="visualizadorProdutoForm" property="acao" value="<%=Constantes.ACAO_VISUALIZAR%>">
	      <tr> 
	        <td align="center">&nbsp;</td>
	      </tr>
	  </logic:notEqual>    
    </table>
  </div>

</html:form>
</body>
</html>