<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: LOCAL COMPRA :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function  Reset(){
				document.formulario.reset();
				return false;
  }


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->


function limpaCampos(){

	produtoLoteForm['csCdtbExposicaoExpoVo.idExpoCdExposicao'].value = ""; 
	produtoLoteForm.reloEnCep.value=""; 
	produtoLoteForm.reloDsDataCompra.value="";
	produtoLoteForm.reloDsLocalCompra.value="";
	produtoLoteForm.reloEnLogradouroCompra.value="";
	produtoLoteForm.reloEnNumeroCompra.value="";           
	produtoLoteForm.reloEnComplementoCompra.value="";
	produtoLoteForm.reloEnBairroCompra.value="";
	produtoLoteForm.reloEnMunicipioCompra.value="";
	produtoLoteForm.reloEnEstadoCompra.value="";
	produtoLoteForm.reloEnReferenciaCompra.value="";       	
	
}

function submeteForm(){

	if (produtoLoteForm.reloDsDataCompra.value != "")
		if (!verificaData(produtoLoteForm.reloDsDataCompra))
			return false;

	produtoLoteForm.tela.value = "<%=MCConstantes.TELA_LOCAL_COMPRA%>";
	produtoLoteForm.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
	produtoLoteForm.target = this.name = 'localCompra'
	produtoLoteForm.submit();
}

function preencheCamposPlusCep(logradouro, bairro, municipio, uf, cep, ddd){
	produtoLoteForm.reloEnLogradouroCompra.value = logradouro;
	produtoLoteForm.reloEnBairroCompra.value = bairro;
	produtoLoteForm.reloEnMunicipioCompra.value = municipio;
	produtoLoteForm.reloEnEstadoCompra.value = uf;
	produtoLoteForm.reloEnCep.value = cep;
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="principalBgrPage" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
<input type="hidden" name="cep" value="reloEnCep">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<!-- chave do lote -->
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<html:hidden property="reloNrSequencia" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.LocalCompra" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td valign="top" align="center"> 
            <table width="99%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="20%" class="principalLabel">&nbsp;</td>
                <td width="4%" class="principalLabel">&nbsp;</td>
                <td width="76%" class="principalLabel">&nbsp;</td>
              </tr>
              <tr> 
                <td width="20%" class="principalLabel"><bean:message key="prompt.datacompra"/></td>
                <td width="4%" class="principalLabel">&nbsp;</td>
                <td width="76%" class="principalLabel"><bean:message key="prompt.local"/></td>
              </tr>
              <tr> 
                <td width="20%" class="principalLabel"> 
                  <html:text property="reloDsDataCompra" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event);"/>
                </td>
                <td width="4%" class="principalLabel"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title='<bean:message key="prompt.calendario"/>' class="geralCursoHand" onclick="show_calendar('produtoLoteForm.reloDsDataCompra')"></td>
                <td width="76%" class="principalLabel"> 
                  <html:text property="reloDsLocalCompra" styleClass="principalObjForm" maxlength="60"/>
                </td>
              </tr>
              <tr> 
                <td colspan="3" class="principalLabel"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="57%" class="principalLabel"><bean:message key="prompt.endereco"/></td>
                      <td colspan="2" class="principalLabel"><bean:message key="prompt.numero"/> / <bean:message key="prompt.complemento"/></td>
                    </tr>
                    <tr> 
                      <td width="57%" class="principalLabel"> 
                        <html:text property="reloEnLogradouroCompra" styleClass="principalObjForm" maxlength="70"/>
                      </td>
                      <td width="6%" class="principalLabel"> 
                        <html:text property="reloEnNumeroCompra" styleClass="principalObjForm" maxlength="7"/>
                      </td>
                      <td width="37%" class="principalLabel"> 
                        <html:text property="reloEnComplementoCompra" styleClass="principalObjForm" maxlength="45"/>
                      </td>
                    </tr>
                    <tr> 
                      <td width="57%" class="principalLabel"><bean:message key="prompt.bairro"/></td>
                      <td colspan="2" class="principalLabel"><bean:message key="prompt.cidadeuf"/></td>
                    </tr>
                    <tr> 
                      <td width="57%" class="principalLabel"> 
                        <html:text property="reloEnBairroCompra" styleClass="principalObjForm" maxlength="60"/>
                      </td>
                      <td colspan="2" class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="74%"> 
                              <html:text property="reloEnMunicipioCompra" styleClass="principalObjForm" maxlength="40"/>
                            </td>
                            <td width="26%"> 
                              <html:text property="reloEnEstadoCompra" styleClass="principalObjForm" maxlength="5"/>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel"><bean:message key="prompt.cep"/></td>
                <td class="principalLabel">&nbsp;</td>
                <td class="principalLabel"><bean:message key="prompt.referencia"/></td>
              </tr>
              <tr> 
                <td class="principalLabel"> 
                  <html:text property="reloEnCep" styleClass="principalObjForm" maxlength="8" onkeydown="return isDigito(event)"/>
                </td>
                <td class="principalLabel"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" title='<bean:message key="prompt.buscaCep"/>' class="geralCursoHand" onClick="showModalDialog('PlusCep.do?tipo=cep&ddd=true',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:340px,dialogTop:0px,dialogLeft:200px')"></td>
                <td class="principalLabel"> 
                  <html:text property="reloEnReferenciaCompra" styleClass="principalObjForm" maxlength="50"/>
                </td>
              </tr>
              <tr> 
                <td colspan="3" class="principalLabel"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="57%" class="principalLabel"><bean:message key="prompt.expoProduto"/></td>
                      <td width="43%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="57%"> 
						  <html:select property="csCdtbExposicaoExpoVo.idExpoCdExposicao" styleClass="principalObjForm" >
							<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							<logic:present name="csCdtbExposicaoExpoVector">
						      <html:options collection="csCdtbExposicaoExpoVector" property="idExpoCdExposicao" labelProperty="expoDsExposicao"/>
						    </logic:present>
						  </html:select>
                      </td>
                      <td width="43%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="57%" class="principalLabel">&nbsp;</td>
                      <td width="43%" class="principalLabel" align="right"> 
                        <table width="60%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="34%" align="right">&nbsp;</td>
                            <td width="34%" class="principalLabel">&nbsp;</td>
                            <td width="20%" align="center"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="submeteForm()" title='<bean:message key="prompt.gravar"/>'></td>
                            <td width="12%" align="center"><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="limpaCampos()" title='<bean:message key="prompt.LimparCampos"/>'></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="210"></td>
  </tr>
  <tr> 
    <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title='<bean:message key="prompt.cancelar"/>' onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
