
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

var possuiProduto = false;
var remaInTroca = "N";
var remaInRepor = "N";

function iniciarTela(){

	parent.habilitaCheck();

	if (remaInTroca == "S"){
		parent.reembolsoProdutoForm['remaInTroca'].checked = true;
		parent.reembolsoProdutoForm['remaInRepor'].checked = false;
	}else if (remaInRepor == "S"){
		parent.reembolsoProdutoForm['remaInRepor'].checked = true;	
		parent.reembolsoProdutoForm['remaInTroca'].checked = false;		
	}
	
	//habilitaCampos();
	parent.habilitaCampos();

	//atualiza campos de valor
	parent.carregaCamposValor();

}

function habilitaCampos(){
	var a;
	if (parent.document.readyState != 'complete') {
		a = setTimeout('habilitaCampos()', 100);
	} else {
		clearTimeout(a);
		parent.habilitaCampos();
	}
}

//Adiciona o produto listado em um array para controle
var produtosAdicionados = new Array();

function adicionarProduto(idAsn){
	produtosAdicionados[produtosAdicionados.length] = idAsn;
}

//Adiciona a variedade listada em um array para controle
var variedadesAdicionadas = new Array();

function adicionarVariedades(idAsn2){
	variedadesAdicionadas[variedadesAdicionadas.length] = idAsn2;
}


//Verifica se o produto j� est� na lista
function produtoJaAdicionado(idAsn){
	var retorno = false;
	for(i = 0; i < produtosAdicionados.length; i++){
		if(produtosAdicionados[i] == idAsn){
			retorno = true;
			break;
		}
	}
	
	return retorno;
}

//Verifica se o produto e a variedade j� est�o na lista
function produtoJaAdicionadoAsn2(idAsn,idAsn2){
	var retorno = false;
	for(i = 0; i < produtosAdicionados.length; i++){
		if( (produtosAdicionados[i] == idAsn) && (variedadesAdicionadas[i] == idAsn2)){
			retorno = true;
			break;
		}
	}
	
	return retorno;
}


</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm" >
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	    <td width="66" class="principalLstCab">&nbsp;</td>
	    <td class="principalLstCab" width="572"><%=getMessage("prompt.Produto",request)%></td>
	    <!--td class="principalLstCab" width="140" align="center"><bean:message key="prompt.Estoque"/></td-->
	    <td class="principalLstCab" width="173" align="center"><bean:message key="prompt.ValorUnitario"/></td>
	    <td class="principalLstCab" width="150" align="center"><bean:message key="prompt.Quantidade"/></td>
	    <td class="principalLstCab" width="219" align="center"><bean:message key="prompt.ValorTotal"/></td>
	  </tr>
	</table>
	
	<div id="Layer1" style="position:absolute; width:100%; height:60; z-index:1; overflow: auto"> 
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<logic:present name="csNgtbProdutotrocaPrtrVector">
			<logic:iterate id="csNgtbProdutotrocaPrtrVector" name="csNgtbProdutotrocaPrtrVector" indexId="numero">
				<script>
					possuiProduto=true;
					remaInTroca = '<bean:write name="csNgtbProdutotrocaPrtrVector" property="csNgtbReclamacaoManiRemaVo.remaInTroca"/>';
					remaInRepor = '<bean:write name="csNgtbProdutotrocaPrtrVector" property="csNgtbReclamacaoManiRemaVo.remaInRepor"/>'; 
					adicionarProduto('<bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>');
					adicionarVariedades('<bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>');
				</script> 
			    <tr class="intercalaLst<%=numero.intValue()%2%>"> 
			      <td class="principalLstPar" width="65"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" title='<bean:message key="prompt.excluir"/>' onclick="parent.removeProdutoTroca('<bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrNrSequencia"/>')"></td>
			      		
			      		<input type="hidden" id="prtrNrSequencia" name="prtrNrSequencia" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrNrSequencia"/>'>
			      		<input type="hidden" id="idAsnCdAssuntoNivel" name="idAsnCdAssuntoNivel" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>'>
			      		<input type="hidden" id="idAsn2CdAssuntoNivel2" name="idAsn2CdAssuntoNivel2" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>'>
			      		<input type="hidden" id="idLinhCdLinha" name="idLinhCdLinha" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"/>'>
			      		<input type="hidden" id="prtrVlUnitario" name="prtrVlUnitario" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrVlUnitario"/>'>
			      		<input type="hidden" id="prtrInEstoque" name="prtrInEstoque" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrInEstoque"/>'>
			      		<input type="hidden" id="prtrNrQuantidade" name="prtrNrQuantidade" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrNrQuantidade"/>'>
			      		<input type="hidden" id="prtrVlTotal" name="prtrVlTotal" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrVlTotal"/>'>
			      		
			      <td class="principalLstPar" width="554" onclick="parent.editaProduto(<%=numero.intValue()%>)"><div class="geralCursoHand"><bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto"/>&nbsp;</div></td>
			      <!--td class="principalLstPar" width="84" onclick="parent.editaProduto(<%=numero.intValue()%>)">
			      	<div class="geralCursoHand">
					<logic:equal name="csNgtbProdutotrocaPrtrVector" property="prtrInEstoque" value="S" >
			      		<img src="webFiles/images/icones/check.gif" width="11" height="12">
			      	</logic:equal>
					&nbsp;
					</div>			      		
			      </td-->
			      <td class="principalLstPar" width="97" align="right" onclick="parent.editaProduto(<%=numero.intValue()%>)"><div class="geralCursoHand"><bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrVlUnitario"/>&nbsp;</div></td>
			      <td class="principalLstPar" width="104" align="center" onclick="parent.editaProduto(<%=numero.intValue()%>)"><div class="geralCursoHand">&nbsp;</div></td>
			      <td class="principalLstPar" width="154" align="center" onclick="parent.editaProduto(<%=numero.intValue()%>)"><div class="geralCursoHand"><bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrNrQuantidade"/>&nbsp;</div></td>
			      <td class="principalLstPar" width="137" align="right" onclick="parent.editaProduto(<%=numero.intValue()%>)"><div class="geralCursoHand"><bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrVlTotal"/>&nbsp;</div></td>
			      <td class="principalLstPar" width="54" align="center" onclick="parent.editaProduto(<%=numero.intValue()%>)"><div class="geralCursoHand">&nbsp;</div></td>
			    </tr>
			</logic:iterate>
	  	</logic:present>   
	  </table>
	</div>
</html:form>	

<script language="JavaScript">
<logic:present name="alerta" scope="session">
	alert("<bean:write name='alerta' />");
	<%request.getSession().removeAttribute("alerta");%>
</logic:present>
</script>

</body>
</html>
