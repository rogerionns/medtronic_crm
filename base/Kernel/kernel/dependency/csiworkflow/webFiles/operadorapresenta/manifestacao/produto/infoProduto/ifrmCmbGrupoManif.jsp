<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
function submeteForm() {

	produtoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	produtoLoteForm.tela.value = '<%=MCConstantes.TELA_CMB_MANIFESTACAO_LOTE%>';
	produtoLoteForm.target = parent.ifrmCmbReclamacao.name;
	produtoLoteForm.submit();
}

function inicio(){
	if(produtoLoteForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].length == 2){
		produtoLoteForm["csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].selectedIndex = 1;
	}
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		if(parent.posicionouAsn2 == true && parent.posicionouGrupo == false && parent.parent.produtoLoteForm['csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value != ""){
	<%}else{%>
		if(parent.posicionouAsn1 == true && parent.posicionouGrupo == false && parent.parent.produtoLoteForm['csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value != ""){
	<%}%>
		produtoLoteForm['csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value = parent.parent.produtoLoteForm['csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value;
		produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = parent.parent.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = parent.parent.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		parent.posicionouGrupo = true;
	}
	
	submeteForm();
}

</script> 
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>  
  <html:hidden property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"/>
  <html:hidden property="csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto"/>
  
  <html:select property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" styleClass="principalObjForm" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbGrupoManifestacaoGrmaVector">
	  <html:options collection="csCdtbGrupoManifestacaoGrmaVector" property="idGrmaCdGrupoManifestacao" labelProperty="grmaDsGrupoManifestacao"/>
	</logic:present>
  </html:select>

</html:form>
</body>
</html>
