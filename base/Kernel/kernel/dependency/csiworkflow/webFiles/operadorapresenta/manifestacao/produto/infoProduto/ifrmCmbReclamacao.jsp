<%@ page language="java"  import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo,br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
function submeteForm(){

	produtoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	produtoLoteForm.tela.value = '<%=MCConstantes.TELA_CMB_DESTINO%>';
	produtoLoteForm.target = parent.ifrmCmbDestino.name;
	produtoLoteForm.action = '<%=Geral.getActionProperty("produtoLoteAction", empresaVo.getIdEmprCdEmpresa())%>';
	produtoLoteForm.submit();
	
	produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].value = "";
	
}

function iniciaTela(){
	if(produtoLoteForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].length == 2){
		produtoLoteForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].selectedIndex = 1;
	}
	submeteForm();
}

</script> 
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="iniciaTela();">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm" >
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />
  <html:hidden property="csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto" />
  
  <html:select property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" styleClass="principalObjForm" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbTpManifestacaoTpmaVector">
      <html:options collection="csCdtbTpManifestacaoTpmaVector" property="idTpmaCdTpManifestacao" labelProperty="tpmaDsTpManifestacao"/>
    </logic:present>
  </html:select> 
  
</html:form>
</body>
</html>
