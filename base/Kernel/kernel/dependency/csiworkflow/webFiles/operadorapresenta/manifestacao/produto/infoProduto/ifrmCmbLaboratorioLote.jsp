<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
function iniciaTela(){
	if(produtoLoteForm.cmbIdFabrCdLaboratorio.length == 2){
		produtoLoteForm.cmbIdFabrCdLaboratorio.selectedIndex = 1;
	}
	
	window.top.habilitaLaboratorio();
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
  <html:hidden property="acao" value="showAll" />
  <html:hidden property="tela" value="cmbLaboratorio" />
  
  <html:select property="cmbIdFabrCdLaboratorio" styleClass="principalObjForm" disabled="true">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbLaboratorioFabrVector">
	  <html:options collection="csCdtbLaboratorioFabrVector" property="idFabrCdFabrica" labelProperty="fabrDsFabrica"/>
	</logic:present>
  </html:select>
</html:form>
</body>
</html>