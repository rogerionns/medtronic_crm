<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Dados Banc&aacute;rios</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language='javascript' src='webFiles/funcoes/TratarDados.js'></script>
<script language="JavaScript">

window.dialogHeight = "230px";

function iniciarTela(){
	var wi = window.dialogArguments;
	posicionarComboBanco();
	aguarde.style.visibility = "hidden";
}


function submeteForm(){
	var wi = window.dialogArguments;
	
	var idBanco = reembolsoProdutoForm['csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco'][1].value;
	var dsAgencia = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsAgencia'].value;
	var dsContaCorrente = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].value;
	var nmTitular = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaNmTitular'].value;
	var dsRg = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value;
	var dsCpf = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsCpf'].value;
	
	if (idBanco.length == 0){
		alert ('<bean:message key="prompt.selecioneUmBanco"/>.');
		return false;
	}
	
	if (trim(dsAgencia).length == 0){
		alert ('<bean:message key="prompt.O_campo_Agencia_e_obrigatorio"/>.');
		return false;
	}

	if (trim(dsContaCorrente).length == 0){
		alert ('<bean:message key="prompt.O_campo_Conta_Corrente_e_obrigatorio"/>.');
		return false;		
	}
	
	if (trim(nmTitular).length == 0){
		alert ('<bean:message key="prompt.O_campo_Nome_e_obrigatorio"/>.');
		return false;		
	}

	if (trim(dsCpf).length == 0){
		alert ('<bean:message key="prompt.O_campo_CPF_e_obrigatorio"/>.');
		return false;		
	}
	
	wi.ifrmCmbInfBanco.insereDados(idBanco,dsAgencia,dsContaCorrente,nmTitular,dsRg,dsCpf);
	aguarde.style.visibility = "visible";
	
	reembolsoProdutoForm["csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"][0].value = idBanco.substring(0, idBanco.indexOf("@"));
	reembolsoProdutoForm["csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"][1].name = "";

	reembolsoProdutoForm.target = window.name = "JanelaDadosBancariosRessarcimento";
	reembolsoProdutoForm.txtInfoBanco.value = idBanco + "@" + dsAgencia + "@" + dsContaCorrente + "@" + nmTitular + "@" + dsRg + "@" + dsCpf;
	reembolsoProdutoForm.submit();
}

function limparDados(){
	reembolsoProdutoForm["csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"][1].selectedIndex = 0;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsAgencia"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaNmTitular"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsCpf"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsRg"].value = "";
}

function mudarValores(obj){
	reembolsoProdutoForm["csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"][0].value = obj.options[obj.selectedIndex].codBanco;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsAgencia"].value = obj.options[obj.selectedIndex].dsAgencia;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].value = obj.options[obj.selectedIndex].dsConta;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaNmTitular"].value = obj.options[obj.selectedIndex].dsTitular;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsCpf"].value = obj.options[obj.selectedIndex].dsCPF;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsRg"].value = obj.options[obj.selectedIndex].dsRG;
	
	posicionarComboBanco();
}

function posicionarComboBanco(){
	//Posicionando o combo de banco
	reembolsoProdutoForm["csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"][1].selectedIndex = 0;
	var valorBanco = "";
	for(i = 1; i < reembolsoProdutoForm["csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"][1].length; i++){
		valorBanco = reembolsoProdutoForm["csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"][1].options[i].value;
		valorBanco = valorBanco.substring(0, valorBanco.indexOf("@"));
		
		if(valorBanco == reembolsoProdutoForm["csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"][0].value){
			reembolsoProdutoForm["csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"][1].selectedIndex = i;
			break;
		}
	}
}


//## FUN��ES PARA VALIDAR O CPF: ##

//  Documento JavaScript                                  '
//Funcao para Calculo do Digito do CPF/CNPJ
function DigitoCPFCNPJ(numCIC) {
var numDois = numCIC.substring(numCIC.length-2, numCIC.length);
var novoCIC = numCIC.substring(0, numCIC.length-2);
switch (numCIC.length){
 case 11 :
  numLim = 11;
  break;
 case 14 :
  numLim = 9;
  break;
 default : return false;
}
var numSoma = 0;
var Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
var numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
   //-- Primeiro digito calculado.  Fara parte do novo c�lculo.
   
   var numDigito = String(numResto);
   novoCIC = novoCIC.concat(numResto);
   //--
numSoma = 0;
Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
numResto = numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
//-- Segundo d�gito calculado.
numDigito = numDigito.concat(numResto);
if (numDigito == numDois) {
 return true;
}
else {
 return false;
}
}
//--< Fim da Fun�ao >--

//-- Retorna uma string apenas com os numeros da string enviada
function ApenasNum(strParm) {
strParm = String(strParm);
var chrPrt = "0";
var strRet = "";
var j=0;
for (var i=0; i < strParm.length; i++) {
 chrPrt = strParm.substring(i, i+1);
 if ( chrPrt.match(/\d/) ) {
  if (j==0) {
   strRet = chrPrt;
   j=1;
  }
  else {
   strRet = strRet.concat(chrPrt);
  }
 }
}
return strRet;
}
//--< Fim da Funcao >--

//-- Somente aceita os caracteres v�lidos para CPF e CNPJ.
function PreencheCIC(objCIC) {
var chrP = objCIC.value.substring(objCIC.value.length-1, objCIC.value.length);

if ( !chrP.match(/[0-9]/) && !chrP.match(/[\/.-]/) ) {
 objCIC.value = objCIC.value.substring(0, objCIC.value.length-1);
 return false;
}
return true;
}
//--< Fim da Funcao >--

function FormataCIC (numCIC) {
numCIC = String(numCIC);
switch (numCIC.length){
case 11 :
 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
case 14 :
 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
default : 
 alert("<bean:message key="prompt.Tamanho_incorreto_do_CPF_ou_CNPJ"/>");
 return "";
}
}

//-- Remove os sinais, deixando apenas os numeros e reconstroi o CPF ou CNPJ, verificando a validade
//-- Recebe como parametros o numero do CPF ou CNPJ, com ou sem sinais e o atualiza com sinais e validado.
function ConfereCIC(objCIC) {
if (objCIC.value == '') {
 return false;
}
var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

numCPFCNPJ = ApenasNum(objCIC.value);

if (!DigitoCPFCNPJ(numCPFCNPJ)) {
 alert("<bean:message key="prompt.Atencao_o_Digito_verificador_do_CPF_ou_CNPJ_e_invalido"/>");
 objCIC.focus();
 return false;
}

objCIC.value = FormataCIC(numCPFCNPJ);

if (objCIC.value.match(strCNPJPat)) {
 return true;
}
else if (objCIC.value.match(strCPFPat)) {
 return true;
}
else {
 alert("<bean:message key="prompt.Digite_um_CPF_ou_CNPJ_valido"/>");
		AtivarPasta("DADOSCOMPLEMENTARES");
 objCIC.focus();
 return false;
}
}
//Fim da Funcao para Calculo do Digito do CPF/CNPJ


</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm">
  <html:hidden property="tela"/>
  <html:hidden property="acao"/>
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
  <html:hidden property="csNgtbReembolsoReemVo.reemNrSequencia" />
  <html:hidden property="csNgtbReembolsoReemVo.idForeCdFormareemb" />
  <html:hidden property="csNgtbReembolsoReemVo.reemVlReembolso" />
  <input type="hidden" name="usarFormaRetornoTela" value="S">
  <input type="hidden" name="txtInfoBanco">


  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.dadosBancarios"/></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                	<td class="principalLabel">
                		<bean:message key="prompt.Banco"/>
                	</td>
                </tr>
                <tr>
                	<td>
                	   <div style="width: 50%">
						  <select name="dadosBancarios" class="principalObjForm" onchange="mudarValores(this);">
							 <option codBanco="" dsAgencia="" dsConta="" dsTitular="" dsCPF="" dsRG=""><bean:message key="prompt.combo.sel.opcao" /></option>
							<logic:present name="csNgtbReembolsoReemVector">
							  <logic:iterate name="csNgtbReembolsoReemVector" id="csNgtbReembolsoReemVector">
							  	<option codBanco="<bean:write name="csNgtbReembolsoReemVector" property="csCdtbBancoBancVo.idBancCdBanco"/>"
							  		dsAgencia="<bean:write name="csNgtbReembolsoReemVector" property="remaDsAgencia"/>"
							  		dsConta="<bean:write name="csNgtbReembolsoReemVector" property="remaDsContacorrente"/>"
							  		dsTitular="<bean:write name="csNgtbReembolsoReemVector" property="remaNmTitular"/>"
							  		dsCPF="<bean:write name="csNgtbReembolsoReemVector" property="remaDsCpf"/>"
							  		dsRG="<bean:write name="csNgtbReembolsoReemVector" property="remaDsRg"/>">
							  			<bean:write name="csNgtbReembolsoReemVector" property="csCdtbBancoBancVo.idBancCdBanco"/> <bean:write name="csNgtbReembolsoReemVector" property="csCdtbBancoBancVo.bancDsBanco"/>
							  			AG. <bean:write name="csNgtbReembolsoReemVector" property="remaDsAgencia"/> CT. <bean:write name="csNgtbReembolsoReemVector" property="remaDsContacorrente"/>
							  		</option>
							  </logic:iterate>
							</logic:present>
						  </select>
					   </div><br>
                	</td>
                </tr>
                <tr> 
                  <td valign="top"> 
                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLabel" width="627" height="8"> <bean:message key="prompt.Banco"/>
                          <br>
                        </td>
                        <td class="principalLabel" width="264" height="8"><bean:message key="prompt.Agencia"/> 
                        </td>
                        <td class="principalLabel" height="8" width="209"><bean:message key="prompt.conta"/> </td>
                        <td class="principalLabel" width="34" height="8">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="627" height="6"> 
                          <html:hidden property="csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"/>
						  <html:select property="csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco" styleClass="principalObjForm" >
							<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							<logic:present name="bancoVector">
						      <html:options collection="bancoVector" property="idBancCdBanco" labelProperty="bancDsBanco"/>
						    </logic:present>
						  </html:select>
                        </td>
                        <td valign="top" height="6" width="264"> 
                          <html:text property="csNgtbReembolsoReemVo.remaDsAgencia" styleClass="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);" />
                        </td>
                        <td height="6" width="209"> 
                          <html:text property="csNgtbReembolsoReemVo.remaDsContacorrente" styleClass="principalObjForm" onkeydown="return ValidaTipo(this, 'N', event);" />
                        </td>
                        <td width="34" height="6">&nbsp;</td>
                        <td width="31" height="6">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="627" height="6" class="principalLabel"><bean:message key="prompt.nome"/></td>
                        <td valign="top" height="6" width="264" class="principalLabel"><bean:message key="prompt.cpf"/></td>
                        <td height="6" width="209" class="principalLabel"><bean:message key="prompt.rg"/></td>
                        <td width="34" height="6">&nbsp;</td>
                        <td width="31" height="6">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="627" height="6"> 
                          <html:text property="csNgtbReembolsoReemVo.remaNmTitular" styleClass="principalObjForm"/>
                        </td>
                        <td valign="top" height="6" width="264"> 
                          <html:text property="csNgtbReembolsoReemVo.remaDsCpf" styleClass="principalObjForm" onblur="ConfereCIC(this);" maxlength="15"/>
                        </td>
                        <td height="6" width="209"> 
                          <html:text property="csNgtbReembolsoReemVo.remaDsRg" styleClass="principalObjForm"/>
                        </td>
                        <td width="34" height="6"><img src="webFiles/images/botoes/gravar.gif" width="21" height="18" class="geralCursoHand" onclick="submeteForm()" title='<bean:message key="prompt.confirmar"/>'></td>
                        <td width="31" height="6"><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="limparDados();" title='<bean:message key="prompt.cancelar"/>'></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="Espaco">&nbsp;</td>
                      </tr>
                    </table>
                    
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title='<bean:message key="prompt.fechar"/>' onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</html:form>
<div id="aguarde" style="position:absolute; left:250px; top:50px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>