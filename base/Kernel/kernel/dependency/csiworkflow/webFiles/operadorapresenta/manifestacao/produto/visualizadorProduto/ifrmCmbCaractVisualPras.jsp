<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
function atualizaCmbRespTab(){
	var url = "";
	
	url = "VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_CMB_RESPTAB_VISUAL_PRAS%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>";
	url = url + "&idCaprCdCaracteristicaprod=" + visualizadorProdutoForm.idCaprCdCaracteristicaprod.value;
	url = url + "&idLinhCdLinha=" + parent.document.forms[0].idLinhCdLinha.value;
	
	parent.cmbRespTabIframe.location.href = url;
	
}
</script>

<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');atualizaCmbRespTab();">

<html:form styleId="visualizadorProdutoForm" action="/VisualizadorProduto.do">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
		
	<html:select property="idCaprCdCaracteristicaprod" styleClass="principalObjForm" onchange="atualizaCmbRespTab();">
		<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
		<logic:present name="caractVector">
			<html:options collection="caractVector" property="csCdtbCaracteristicaProdCaprVo.idCaprCdCaracteristicaprod" labelProperty="csCdtbCaracteristicaProdCaprVo.caprDsCaracteristicaprod"/>
		</logic:present> 
	</html:select>

</html:form>
</body>
</html>