<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);


function setValoresToForm(form){

	
	strTxt = "";
	strTxt += "       <input type=\"hidden\" name=\"csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbManifEspecMaesVo.possueArquivo\" value=\"" + "S" + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbManifEspecMaesVo.csNgtmManifArqTempMartVo.martDsSessao\" value=\"" + manifArquivoForm['csNgtmManifArqTempMartVo.martDsSessao'].value + "\" > ";

	form.getElementsByName("camposManifEspec").item(0).innerHTML += strTxt;

}


function iniciarTela(){
	manifArquivoForm.tela.value="<%=MCConstantes.TELA_IFRM_LST_MANIFARQUIVO%>";
	manifArquivoForm.acao.value="<%=Constantes.ACAO_CONSULTAR%>";
	manifArquivoForm.target="ifrmLstManifArquivo";
	manifArquivoForm.submit();
}

function anexaArquivo(){
	ifrmManifArq.anexaArquivo();
}
-->

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();">
<html:form action="/ManifArquivo.do" styleId="manifArquivoForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>

<html:hidden property="idChamCdChamado"/>
<html:hidden property="maniNrSequencia"/>
<html:hidden property="idAsn1CdAssuntoNivel1"/>
<html:hidden property="idAsn2CdAssuntoNivel2"/>

<html:hidden property="csAstbManifArquivoMaarVo.idChamCdChamado"/>
<html:hidden property="csAstbManifArquivoMaarVo.maniNrSequencia"/>
<html:hidden property="csAstbManifArquivoMaarVo.idAsn1CdAssuntonivel1"/>
<html:hidden property="csAstbManifArquivoMaarVo.idAsn2CdAssuntonivel2"/>
<html:hidden property="csNgtmManifArqTempMartVo.martDsSessao"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalLabel" width="6%"><bean:message key="prompt.arquivo"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td class="principalLabel" width="50%">
        <iframe name="ifrmManifArq" src="ManifArquivo.do?tela=ifrmManifArq" height="20" width="100%" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>
    </td>
    <td class="principalLabel" width="44%">&nbsp;<span class="geralCursoHand"><img src="webFiles/images/icones/arquivos.gif" width="25" height="24" onclick="anexaArquivo();"  title='<bean:message key="prompt.anexar_arquivo"/>'></span></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr><td>&nbsp;</td></tr>
  <tr> 
    <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td class="principalLstCab" width="3%">&nbsp;</td>
				<td class="principalLstCab" width="33%">Nome</td>
				<td class="principalLstCab" width="60%">&nbsp</td>
				<td class="principalLstCab" width="5%" align="center">Origem</td>
			</tr>		
		</table> 
    </td>
  </tr>
  <tr>
	<td>
		<iframe name="ifrmLstManifArquivo" src="ManifArquivo.do?tela=<%=MCConstantes.TELA_IFRM_LST_MANIFARQUIVO%>" width="100%" height="150" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>
	</td>
  </tr>
</table>
</html:form>
</body>
</html>
