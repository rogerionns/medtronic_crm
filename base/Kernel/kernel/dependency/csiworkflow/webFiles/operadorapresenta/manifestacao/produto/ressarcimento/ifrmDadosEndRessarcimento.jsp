<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.endereco" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

function iniciaTela(){
	preencheTela();
}

function preencheTela(){
	var wi = window.dialogArguments;
	var wi2 = wi.dialogArguments;
	
	index = wi.ifrmCmbInfBanco.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].selectedIndex;
	
	if (index > 0){
		index = index -1;
		if (wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtidTipoEnd.length == undefined){
		//	reembolsoProdutoForm['csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtidTipoEnd.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsLogradouro.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsNumero'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsNumero.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsComplemento'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsComplemento.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsBairro'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsBairro.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsMunicipio'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsMunicipio.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsUf'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsEstado.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsCep'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsCep.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsReferencia'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsReferencia.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsRg.value;
		}else{
		//	reembolsoProdutoForm['csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtidTipoEnd[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsLogradouro[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsNumero'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsNumero[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsComplemento'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsComplemento[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsBairro'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsBairro[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsMunicipio'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsMunicipio[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsUf'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsEstado[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsCep'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsCep[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsReferencia'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsReferencia[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value = wi.ifrmCmbInfBanco.reembolsoProdutoForm.txtDsRg[index].value;
		}
	}

	if (trim(reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value) == ""){
		reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value = wi2.top.principal.pessoa.dadosPessoa.pessoaForm.pessDsRg.value;
	}
	
}


function submeteForm(){
	var wi = window.dialogArguments;
	
	var idTipoEnd = reembolsoProdutoForm['csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco'].value;

/*	if(reembolsoProdutoForm['csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco'].selectedIndex <= 0){
		alert ('<bean:message key="prompt.Selecione_o_tipo_de_endereco"/>.');
		return false;
	}*/
	
	var tipoEnd = "";
	var logradouro = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value;
	var numero = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsNumero'].value;
	var complemento = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsComplemento'].value;
	var bairro = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsBairro'].value;
	var municipio = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsMunicipio'].value;
	var estado = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsUf'].value;
	var cep = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsCep'].value;
	var referencia = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsReferencia'].value;
	var rg = reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value;
	
	if (trim(idTipoEnd).length == 0){
		alert ('<bean:message key="prompt.Selecione_o_tipo_de_endereco"/>.');
		return false;
	}

	if (trim(logradouro).length == 0){
		alert ('<bean:message key="prompt.O_campo_logradouro_e_obrigatorio"/>.');
		return false;
	}
	
	if (trim(numero).length == 0){
		alert ('<bean:message key="prompt.O_campo_numero_e_obrigatorio"/>.');
		return false;
	}

	if (trim(bairro).length == 0){
		alert ('<bean:message key="prompt.O_campo_bairro_e_obrigatorio"/>.');
		return false;
	}
	
	if (trim(municipio).length == 0){
		alert ('<bean:message key="prompt.O_campo_cidade_e_obrigatorio"/>.');
		return false;
	}
	
	if (trim(estado).length == 0){
		alert ('<bean:message key="prompt.O_campo_UF_e_obrigatorio"/>.');
		return false;
	}

	if (trim(cep).length == 0){
		alert ('<bean:message key="prompt.O_campo_CEP_e_obrigatorio"/>.');
		return false;
	}

	if (trim(rg).length == 0){
		alert ('<bean:message key="prompt.O_campo_RG_e_obrigatorio"/>.');
		return false;
	}
	
	wi.ifrmCmbInfBanco.insereDados(idTipoEnd,tipoEnd,logradouro,numero,complemento,bairro,municipio,estado,cep,referencia,rg);
	aguarde.style.visibility = "visible";
	
	reembolsoProdutoForm.target = window.name = "JanelaEndeRessarcimento";
	reembolsoProdutoForm.txtInfoEndereco.value = idTipoEnd + "@" + tipoEnd + "@" + logradouro + "@" + numero + "@" + complemento + "@" + bairro + "@" + municipio + "@" + estado + "@" + cep + "@" + referencia + "@" + rg;
	reembolsoProdutoForm.submit();
}

function limparDados() {
//	reembolsoProdutoForm["csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco"].selectedIndex = 0;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsNumero"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsComplemento"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsBairro"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsCep"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsMunicipio"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsUf"].value = "";
//	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsRg"].value = "";
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsReferencia"].value = "";
}

function preencheCamposPlusCep(logradouro, bairro, municipio, uf, cep, ddd){
	document.all.item('csNgtbReembolsoReemVo.remaDsLogradouro').value = logradouro;
	document.all.item('csNgtbReembolsoReemVo.remaDsBairro').value = bairro;
	document.all.item('csNgtbReembolsoReemVo.remaDsMunicipio').value = municipio;
	document.all.item('csNgtbReembolsoReemVo.remaDsUf').value = uf;
	document.all.item('csNgtbReembolsoReemVo.remaDsCep').value = cep;
}

function mudarValores(obj){
/*	if(obj.options[obj.selectedIndex].idTpEnd == "")
		reembolsoProdutoForm["csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco"].selectedIndex = 0;
	else
		reembolsoProdutoForm["csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco"].value = obj.options[obj.selectedIndex].idTpEnd;
*/	
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsLogradouro"].value = obj.options[obj.selectedIndex].dsLogradouro;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsNumero"].value = obj.options[obj.selectedIndex].dsNumero;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsComplemento"].value = obj.options[obj.selectedIndex].dsComplemento;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsBairro"].value = obj.options[obj.selectedIndex].dsBairro;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsCep"].value = obj.options[obj.selectedIndex].dsCep;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsMunicipio"].value = obj.options[obj.selectedIndex].dsMunicipio;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsUf"].value = obj.options[obj.selectedIndex].dsUf;
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsReferencia"].value = obj.options[obj.selectedIndex].dsReferencia;
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="principalBgrPageIFRM" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';iniciaTela();">

<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm">

  <html:hidden property="acao" />
  <html:hidden property="tela" value="ifrmDadosFinBancarios" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
  <html:hidden property="csNgtbReembolsoReemVo.reemNrSequencia" />
  <html:hidden property="csNgtbReembolsoReemVo.idForeCdFormareemb"/>
  <html:hidden property="txtInfoEndereco"/>
  
<input type="hidden" name="usarFormaRetornoTela" value="S">
<input type="hidden" name="logradouro" value="csNgtbReembolsoReemVo.remaDsLogradouro">
<input type="hidden" name="bairro" value="csNgtbReembolsoReemVo.remaDsBairro">
<input type="hidden" name="municipio" value="csNgtbReembolsoReemVo.remaDsMunicipio">
<input type="hidden" name="estado" value="csNgtbReembolsoReemVo.remaDsUf">
<input type="hidden" name="cep" value="csNgtbReembolsoReemVo.remaDsCep">

<table width="98%" align="center" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.endereco" /> </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center" valign="top">
		      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="principalLabel" width="61%" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="10"></td>
				</tr>
				<tr>
					<td colspan=2 class="principalLabel"><bean:message key="prompt.endereco" /></td>
				</tr>
				<tr>
					<td colspan=2>
                	   <div style="width: 50%">
						  <select name="dadosEnderecos" class="principalObjForm" onchange="mudarValores(this);">
							 <option idTpEnd="" dsLogradouro="" dsNumero="" dsComplemento="" dsReferencia="" dsBairro="" dsCep="" dsMunicipio="" dsUf="">
							 	<bean:message key="prompt.combo.sel.opcao" /></option>
							<logic:present name="csNgtbReembolsoReemVector">
							  <logic:iterate name="csNgtbReembolsoReemVector" id="csNgtbReembolsoReemVector">
							  	<option idTpEnd="<bean:write name="csNgtbReembolsoReemVector" property="csDmtbTpenderecoTpenVo.idTpenCdTpendereco"/>"
							  		dsLogradouro="<bean:write name="csNgtbReembolsoReemVector" property="remaDsLogradouro"/>"
							  		dsNumero="<bean:write name="csNgtbReembolsoReemVector" property="remaDsNumero"/>"
							  		dsComplemento="<bean:write name="csNgtbReembolsoReemVector" property="remaDsComplemento"/>"
							  		dsReferencia="<bean:write name="csNgtbReembolsoReemVector" property="remaDsReferencia"/>"
							  		dsBairro="<bean:write name="csNgtbReembolsoReemVector" property="remaDsBairro"/>"
							  		dsCep="<bean:write name="csNgtbReembolsoReemVector" property="remaDsCep"/>"
							  		dsMunicipio="<bean:write name="csNgtbReembolsoReemVector" property="remaDsMunicipio"/>"
							  		dsUf="<bean:write name="csNgtbReembolsoReemVector" property="remaDsUf"/>">
							  			<bean:write name="csNgtbReembolsoReemVector" property="csDmtbTpenderecoTpenVo.tpenDsTpendereco"/> - <bean:write name="csNgtbReembolsoReemVector" property="remaDsLogradouro"/>
							  			<bean:write name="csNgtbReembolsoReemVector" property="remaDsCep"/>
							  		</option>
							  </logic:iterate>
							</logic:present>
						  </select>
					   </div>
					</td>
				</tr>
		        <tr> 
		          <td class="principalLabel" width="33%">
					  <html:hidden property="csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco" value="0" />
		          </td>
		          <td class="principalLabel" width="28%">&nbsp;</td>
		        </tr>
				<tr>
					<td class="principalLabel" width="61%" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
				</tr>
		        <tr> 
		          <td class="principalLabel" width="33%"><bean:message key="prompt.endereco" /></td>
		          <td class="principalLabel" width="28%">
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="21%" class="principalLabel">
		                	<bean:message key="prompt.numero" />
		                </td>
		                <td width="79%" class="principalLabel">
		                	<bean:message key="prompt.complemento" />
		                </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="33%"> 
		            <html:text property="csNgtbReembolsoReemVo.remaDsLogradouro" styleClass="principalObjForm" maxlength="100" />
		          </td>
		          <td class="principalLabel" width="28%"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="21%"> 
		                  <html:text property="csNgtbReembolsoReemVo.remaDsNumero" styleClass="principalObjForm" maxlength="10" />
		                </td>
		                <td width="79%"> 
		                  <html:text property="csNgtbReembolsoReemVo.remaDsComplemento" styleClass="principalObjForm" maxlength="50" />
		                </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="33%"><bean:message key="prompt.bairro" /></td>
		          <td class="principalLabel" width="28%">
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="86%" class="principalLabel">
		                	<bean:message key="prompt.cidade" />
		                </td>
		                <td width="14%" class="principalLabel">
		                	<bean:message key="prompt.uf" />
		                </td>
		               </tr>
		             </table>
		          </td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="33%"> 
		            <html:text property="csNgtbReembolsoReemVo.remaDsBairro" styleClass="principalObjForm" maxlength="60" />
		          </td>
		          <td class="principalLabel" width="28%"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="86%"> 
		                  <html:text property="csNgtbReembolsoReemVo.remaDsMunicipio" styleClass="principalObjForm" maxlength="80" />
		                </td>
		                <td width="14%"> 
		                  <html:text property="csNgtbReembolsoReemVo.remaDsUf" styleClass="principalObjForm" maxlength="3" />
		                </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="20%"><bean:message key="prompt.cep" /></td>
		          <td class="principalLabel" width="60%"><bean:message key="prompt.referencia" /></td>
		        </tr>
		        <tr> 
		          <td class="principalLabel" width="33%"> 
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              <tr> 
		                <td width="91%"> 
		                  <html:text property="csNgtbReembolsoReemVo.remaDsCep" styleClass="principalObjForm" maxlength="8" />
		                </td>
		                <td width="9%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" title="<bean:message key='prompt.buscaCep'/>" onClick="showModalDialog('PlusCep.do',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')" border="0"></td>
		              </tr>
		            </table>
		          </td>
		          <td class="principalLabel" width="28%" align="center"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="100%"> 
		                  <html:text property="csNgtbReembolsoReemVo.remaDsReferencia" styleClass="principalObjForm" maxlength="50" />
		                </td>
			            <td class="principalLabel" width="0%" align="center"> 
		                  <!--html:text property="csNgtbReclamacaoManiRemaVo.remaDsReferencia" styleClass="principalObjForm" maxlength="20" /-->
			            </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr>
		        	<td class="principalLabel" width="33%"><bean:message key="prompt.rg" /></td>
		        	<td class="principalLabel" width="28%">&nbsp;</td>
		        </tr>
		        <tr>
		        	<td class="principalLabel" width="33%"><html:text property="csNgtbReembolsoReemVo.remaDsRg" styleClass="principalObjForm" maxlength="50" /></td>
		        	<td class="principalLabel" width="28%">&nbsp;</td>
		        </tr>
		      </table>
	        <table border="0" cellspacing="0" cellpadding="4" align="right">
	          <tr> 
	            <td> 
	              <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.gravar'/>" class="geralCursoHand" onclick="submeteForm()"></div>
	            </td>
	            <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.cancelar'/>" class="geralCursoHand" onclick="limparDados()"></td>
	          </tr>
	        </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.cancelar'/>" onClick="javascript:window.close()" class="geralCursoHand">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
<div id="aguarde" style="position:absolute; left:250px; top:50px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>