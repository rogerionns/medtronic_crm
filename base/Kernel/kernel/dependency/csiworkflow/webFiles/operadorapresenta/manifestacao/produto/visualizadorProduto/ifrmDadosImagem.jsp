<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,com.iberia.helper.Constantes"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
    
    
<html>
<head>
<title>-- CRM -- PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
<!--

function mostrarImagem(idAsn1,idAsn2,idPrim){
	var url="";
	
	url = "VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_IMG_PRAS%>";
	url = url + "&acao=<%=Constantes.ACAO_VISUALIZAR%>";
	url = url + "&csCdtbProdutoImagemPrimVo.idAsn1CdAssuntonivel1=" + idAsn1;
	url = url + "&csCdtbProdutoImagemPrimVo.idAsn2CdAssuntonivel2=" + idAsn2;
	url = url + "&csCdtbProdutoImagemPrimVo.idPrimCdProdutoImagem=" + idPrim;
		
	//ifrmImgProd.location.href = url;
	
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:230px;dialogHeight:210px;dialogTop:280px;dialogLeft:350px');
	
}


function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
//-->
</script>

</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0">
<html:form styleId="visualizadorProdutoForm" action="/VisualizadorProduto.do">

<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalPstQuadroLinkSelecionado">Imagens</td>
    <td class="principalLabel">&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
  <tr> 
    <td valign="top" height="80" >
		<div id="Layer1" style="position:absolute; width:100%; height:79px; z-index:1; overflow: auto">    	
	    <table width="98%" border="0" cellspacing="0" cellpadding="0" valign="top" align="center">
		  	<logic:present name="imagemProdVector">
				<logic:iterate id="imagemProdVector" name="imagemProdVector" indexId="numero">	
				  <tr> 
				    <td class="principalLstPar" onclick="mostrarImagem(<bean:write name="imagemProdVector" property="idAsn1CdAssuntonivel1"/>,<bean:write name="imagemProdVector" property="idAsn2CdAssuntonivel2"/>,<bean:write name="imagemProdVector" property="idPrimCdProdutoImagem"/>);">
				    	<span class="geralCursoHand"><bean:write name="imagemProdVector" property="primNmFile"/></span>
				    </td>
				  </tr>
				</logic:iterate>  
			</logic:present>	
		</table>	
		</div>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td height="160" valign="top"> 
		<iframe id=ifrmImgProd name="ifrmImgProd" src="VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_IMG_PRAS%>" width="100%" height="160" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>		    
    </td>
  </tr>
</table>

</html:form>
</body>
</html>
