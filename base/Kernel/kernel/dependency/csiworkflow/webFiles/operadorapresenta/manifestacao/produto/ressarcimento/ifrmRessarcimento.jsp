<%@ page language="java" import="com.iberia.helper.Constantes, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral" %>
 
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title>..: RESSARCIMENTO :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language="JavaScript">

function calculaValorProduto(){
	var vlUnitario = reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrVlUnitario'].value;
	var qtd = reembolsoProdutoForm.txtQuantidadeProduto.value;
	var vlTotal = 0;
	var vlDsTotal = "";
	
	if (trim(vlUnitario).length == 0)
		vlUnitario = "0";
	
	if (trim(qtd).lenght == 0)
		qtd = "0";

	vlUnitario = vlUnitario.replace(",",".");
	
	nVl = new Number(vlUnitario);
	nQtd = new Number(qtd);
	
	vlTotal = nVl * nQtd;

	vlDsTotal = vlTotal.toFixed(2).toString();
	vlDsTotal = vlDsTotal.replace(".",",");
	
	reembolsoProdutoForm.txtVlTotal.value = vlDsTotal;
	
}

function iniciaTela(){
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		if(cmbVariedade.document != undefined)
			ifrmCmbVariedadeManif = cmbVariedade;
	<%}%>
	
	carregaListaRessarcimento();
	carregaListaAcessorios();
	carregaComboInfoBanco();
	
	//mostra o envio atual
	obtemConteudoEnvio();
	
	if (reembolsoProdutoForm.possuiReembolso.value == "S")
		reembolsoProdutoForm.remaInReembolso.checked = true;
	else
		reembolsoProdutoForm.remaInReembolso.checked = false;
	
	//habilitaCheck();	
	//habilitaCampos();
}

function verificarCheck()
{
	if (reembolsoProdutoForm['remaInRepor'].checked == true){
		reembolsoProdutoForm['remaInTroca'].checked = false;
	}
	else
	{
		reembolsoProdutoForm['remaInTroca'].disabled = false;
		reembolsoProdutoForm['remaInReembolso'].disabled = false;
	}
}

var nObtemConteudoEnvio = 0;
function obtemConteudoEnvio(idEnvio,dsEnvio){
	try{
		reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csDmtbEnvioEnviVo.idEnviCdEnvio'].value =	window.top.infoLote.lstLote.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csDmtbEnvioEnviVo.idEnviCdEnvio'].value;
		reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csDmtbEnvioEnviVo.enviDsEnvio'].value	= window.top.infoLote.lstLote.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csDmtbEnvioEnviVo.enviDsEnvio'].value;
		window.document.all.item('divEnvio').innerHTML = reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csDmtbEnvioEnviVo.enviDsEnvio'].value;		
		nObtemConteudoEnvio = 0;
	}catch(e){
		if(nObtemConteudoEnvio > 20){
			alert("Erro em obtemConteudoEnvio(): \n\n"+ e);
		}
		else{
			setTimeout("obtemConteudoEnvio('" + idEnvio + "','" + dsEnvio + "')", 500);
			nObtemConteudoEnvio++;
		}
	}
}

var carregouCamposValor = false;

function carregaCamposValor(){
	if(!carregouCamposValor){
		reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_VL_RESSARCIMENTO%>";
		reembolsoProdutoForm.acao.value = "<%=MCConstantes.ACAO_SHOW_ALL%>";
		reembolsoProdutoForm.target = 'ifrmVlRessarcimento';
		reembolsoProdutoForm.submit();
		//setTimeout("carregaCamposValor()", 2000);
	}
}

function carregaListaRessarcimento(){
	
	reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_LST_RESSARCIMENTO%>";
	reembolsoProdutoForm.acao.value = "<%=MCConstantes.ACAO_SHOW_ALL%>";
	
	var actionOriginal = reembolsoProdutoForm.action;
	reembolsoProdutoForm.action = '<%=Geral.getActionProperty("reembolsoProdutoAction", empresaVo.getIdEmprCdEmpresa())%>';
	reembolsoProdutoForm.target = 'ifrmlstressarcimento';
	reembolsoProdutoForm.submit();
	reembolsoProdutoForm.action = actionOriginal;

}

function carregaListaAcessorios(){

	reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_LST_REEMBOLSO%>";
	reembolsoProdutoForm.acao.value = "<%=MCConstantes.ACAO_SHOW_ALL%>";
	reembolsoProdutoForm.target = 'ifrmLstReembolso';
	reembolsoProdutoForm.submit();

}

function carregaComboInfoBanco(){
	var url="";
	
	url = "ReembolsoProduto.do?tela=ifrmCmbInfoBanco&acao=<%=MCConstantes.ACAO_SHOW_ALL%>";
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	url = url + "&csNgtbReembolsoReemVo.reemNrSequencia=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.reemNrSequencia'].value;
	url = url + "&idEmprCdEmpresa="+ reembolsoProdutoForm.idEmprCdEmpresa.value;

	ifrmCmbInfBanco.document.location.href = url;

}

function submeteProdutoTroca(){
	
	if(reembolsoProdutoForm['remaInRepor'].checked != true)
	{		
		desabilitarCmbProdutos(false);
		
		if (!validaProdutoTroca())
			return false;
	
		reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = ifrmCmbProduto.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrNrQuantidade'].value = reembolsoProdutoForm.txtQuantidadeProduto.value;
	
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = ifrmCmbVariedadeManif.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		<%}%>
			
		reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_LST_RESSARCIMENTO%>";
		reembolsoProdutoForm.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
	
		var actionOriginal = reembolsoProdutoForm.action
		reembolsoProdutoForm.action = '<%=Geral.getActionProperty("reembolsoProdutoAction",empresaVo.getIdEmprCdEmpresa())%>';
		reembolsoProdutoForm.target = 'ifrmlstressarcimento';
		
		reembolsoProdutoForm.remaInTroca.disabled = false;
		reembolsoProdutoForm.submit();
		reembolsoProdutoForm.action = actionOriginal;
		
		limpaCampos("PRODUTO");
	}
	else
	{
		parent.isEditandoProduto = false;
		alert('<bean:message key="prompt.paraAdicionarNovoProdutoOpcaoReporNaoPodeEstarMarcada"/>');
	}

}

function validaProdutoTroca(){
	
	if (ifrmCmbProduto.document.reembolsoProdutoForm.acao.value == '<%=Constantes.ACAO_CONSULTAR%>' || ifrmCmbProduto.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value == 0){
		alert ('<%=getMessage("prompt.alert.combo.prod",request)%>');
		return false;
	}
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		if (ifrmCmbVariedadeManif.document.reembolsoProdutoForm.acao.value == '<%=Constantes.ACAO_CONSULTAR%>' || ifrmCmbVariedadeManif.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value == 0){		
			alert ('<%=getMessage("prompt.alert.combo.variedade",request)%>.');
			return false;
		}
	<%}%>
	
	if (trim(reembolsoProdutoForm.txtQuantidadeProduto.value) == ""){
		alert ('<bean:message key="prompt.O_campo_Quantidade_e_obrigatorio"/>.');
		return false;
	}

	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
    	if(!parent.isEditandoProduto)
		{
	    	if(ifrmlstressarcimento.produtoJaAdicionadoAsn2(ifrmCmbProduto.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value, ifrmCmbVariedadeManif.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value)){
				alert ('<%=getMessage("prompt.Produto_ja_selecionado",request)%>.');
				return false;
			}
		}
		
    <%}else{%>
		if(!parent.isEditandoProduto)
		{
			if(ifrmlstressarcimento.produtoJaAdicionado(ifrmCmbProduto.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value)){
				alert ('<%=getMessage("prompt.Produto_ja_selecionado",request)%>.');
				return false;
			}
		}
	<%}%>
	
	return true;
}

function submeteProdutoAcessorio(){

	if(reembolsoProdutoForm['remaInRepor'].checked != true)
	{		
		desabilitarCmbAcessorios(false);
		
		if (!validaProdutoAcessorio())
			return false;
	
		reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = ifrmCmbProdutoAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrNrQuantidade'].value = reembolsoProdutoForm.txtQuantidadeAcessorio.value;
	
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = cmbVariedadeAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		<%}%>
		
		reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_LST_REEMBOLSO%>";
		reembolsoProdutoForm.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
		reembolsoProdutoForm.target = 'ifrmLstReembolso';
		
		reembolsoProdutoForm.remaInTroca.disabled = false;
		reembolsoProdutoForm.submit();
		
		limpaCampos("ACESSORIO");
	}
	else
	{
		parent.isEditandoAcessorio = false;
		alert('<bean:message key="prompt.paraAdicionarNovoAcessorioOpcaoReporNaoPodeEstarMarcada"/>');
	}
}

function validaProdutoAcessorio(){

	if (ifrmCmbProdutoAcessorio.reembolsoProdutoForm.acao.value == '<%=Constantes.ACAO_CONSULTAR%>' || ifrmCmbProdutoAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value == 0){
		alert ('<bean:message key="prompt.SelecioneAcessorio"/>.');
		return false;
	}
	
	if (cmbVariedadeAcessorio.reembolsoProdutoForm.acao.value == '<%=Constantes.ACAO_CONSULTAR%>' || cmbVariedadeAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value == 0){		
		alert ('<%=getMessage("prompt.alert.combo.variedade",request)%>.');
		return false;
	}
	
	if (trim(reembolsoProdutoForm.txtQuantidadeAcessorio.value) == ""){
		alert ('<bean:message key="prompt.O_campo_Quantidade_e_obrigatorio"/>.');
		return false;
	}
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
    	if(!parent.isEditandoAcessorio)
		{
	    	if(ifrmLstReembolso.acessorioJaAdicionadoAsn2(ifrmCmbProdutoAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value, cmbVariedadeAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value)){
				alert ('<bean:message key="prompt.acessorio_ja_selecionado"/>.');
				return false;
			}
		}
		
    <%}else{%>
		if(!parent.isEditandoAcessorio)
		{
			if(ifrmLstReembolso.acessorioJaAdicionado(ifrmCmbProdutoAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value)){
				alert ('<bean:message key="prompt.acessorio_ja_selecionado"/>.');
				return false;
			}
		}
	<%}%>
	
	return true;
	
}

function submeteReembolso(){

	var index=0;
	
	if (!validaReembolso())
		return false;
	
	if (ifrmCmbInfBanco.tipoInfo == "B"){
		limpaDadosEndereco();
		index = ifrmCmbInfBanco.document.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].selectedIndex;
		index = index -1;
		
		if (ifrmCmbInfBanco.document.reembolsoProdutoForm.txtIdBanco.length == undefined){
			if(ifrmCmbInfBanco.reembolsoProdutoForm.txtIdBanco.value == ""){
				alert ('<bean:message key="prompt.Os_dados_bancarios_sao_obrigatorios"/>.');
				return false;
			}
			reembolsoProdutoForm['csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtIdBanco.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsAgencia'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsAgencia.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsContacorrente.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaNmTitular'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtNmTitular.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsRg.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsCpf'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsCpf.value;
		}else{
			if(ifrmCmbInfBanco.reembolsoProdutoForm.txtIdBanco[index].value == ""){
				alert ('<bean:message key="prompt.Os_dados_bancarios_sao_obrigatorios"/>.');
				return false;
			}
			reembolsoProdutoForm['csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtIdBanco[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsAgencia'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsAgencia[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsContacorrente[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaNmTitular'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtNmTitular[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsRg[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsCpf'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsCpf[index].value;
		}
	}
	
	if (ifrmCmbInfBanco.tipoInfo == "E"){
	
		limpaDadosBancario();
		index = ifrmCmbInfBanco.document.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].selectedIndex;
		index = index -1;

		if (ifrmCmbInfBanco.document.reembolsoProdutoForm.txtidTipoEnd.length == undefined){
			reembolsoProdutoForm['csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtidTipoEnd.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsLogradouro.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsNumero'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsNumero.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsComplemento'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsComplemento.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsBairro'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsBairro.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsMunicipio'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsMunicipio.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsUf'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsEstado.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsCep'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsCep.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsReferencia'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsReferencia.value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsRg.value;
		}else{
			reembolsoProdutoForm['csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtidTipoEnd[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsLogradouro[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsNumero'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsNumero[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsComplemento'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsComplemento[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsBairro'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsBairro[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsMunicipio'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsMunicipio[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsUf'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsEstado[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsCep'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsCep[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsReferencia'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsReferencia[index].value;
			reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value = ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsRg[index].value;
		}
		
		if(reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value == ""){
			alert("<bean:message key="prompt.O_campo_RG_e_obrigatorio"/>");
			MostraTelaDados();
			return false;
		}
		
	}

	reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_CALCULO_REEMBOLSO_PROD%>";
	reembolsoProdutoForm.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
	reembolsoProdutoForm.target = 'ifrmCalculoReembolsoProduto';
	
	reembolsoProdutoForm.remaInReembolso.disabled = false;
	reembolsoProdutoForm.submit();
	
	limpaCamposReembolso();
}

function validaReembolso(){

	if (reembolsoProdutoForm['csNgtbReembolsoReemVo.idForeCdFormareemb'].value == "" ){
		alert ('<bean:message key="prompt.O_campo_Forma_de_Reembolso_e_obrigatorio"/>.');
		return false;
	}

	if (trim(reembolsoProdutoForm['csNgtbReembolsoReemVo.reemVlReembolso'].value) == "" ){
		alert ('<bean:message key="prompt.O_campo_Valor_e_obrigatorio"/>.');
		return false;
	}
	
	if (ifrmCmbInfBanco.tipoInfo == "B"){
		if (ifrmCmbInfBanco.document.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].selectedIndex == 0){
			alert ('<bean:message key="prompt.Os_dados_bancarios_sao_obrigatorios"/>.');
			return false;
		}
	}	

	if (ifrmCmbInfBanco.tipoInfo == "E"){
		if (ifrmCmbInfBanco.document.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].selectedIndex == 0){
			alert ('<bean:message key="prompt.Os_dados_endereco_sao_obrigatorios"/>.');
			return false;
		}
	}	
	
	return true;
}


function removeProdutoTroca(prtrNrSequencia){

	if (!confirm("<bean:message key='prompt.desejaExcluirEsteProduto'/>"))
		return false;

	reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrNrSequencia'].value = prtrNrSequencia;
	reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_LST_RESSARCIMENTO%>";
	reembolsoProdutoForm.acao.value = "<%=Constantes.ACAO_EXCLUIR%>";
	var actionOriginal = reembolsoProdutoForm.action;
	reembolsoProdutoForm.action = '<%=Geral.getActionProperty("reembolsoProdutoAction", empresaVo.getIdEmprCdEmpresa())%>';
	reembolsoProdutoForm.target = 'ifrmlstressarcimento';
	reembolsoProdutoForm.submit();
	reembolsoProdutoForm.action = actionOriginal;
	
	limpaCampos("PRODUTO");

}

function removeProdutoAcessorio(prtrNrSequencia){

	if (!confirm("<bean:message key='prompt.desejaExcluirEsteAcessorio'/>"))
		return false;

	reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrNrSequencia'].value = prtrNrSequencia;
	reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_LST_REEMBOLSO%>";
	reembolsoProdutoForm.acao.value = "<%=Constantes.ACAO_EXCLUIR%>";
	reembolsoProdutoForm.target = 'ifrmLstReembolso';
	reembolsoProdutoForm.submit();
	
	limpaCampos("ACESSORIO");

}

function removeReembolso(){
	
	if (reembolsoProdutoForm.possuiReembolso.value != "S")
		return false;

	if (!confirm("<bean:message key='prompt.desejaExcluirEsteReembolso'/>"))
		return false;

	reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_CALCULO_REEMBOLSO_PROD%>";
	reembolsoProdutoForm.acao.value = "<%=Constantes.ACAO_EXCLUIR%>";
	reembolsoProdutoForm.target = 'ifrmCalculoReembolsoProduto';
	reembolsoProdutoForm.submit();
	
	limpaCamposReembolso();

}

function desabilitarCmbProdutos(valor)
{
	ifrmCmbLinha.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].disabled = valor;
	ifrmCmbProduto.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].disabled = valor;
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		ifrmCmbVariedadeManif.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].disabled = valor;
	<%}%>
}

function desabilitarCmbAcessorios(valor)
{
	ifrmCmbLinhaAcessorio.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].disabled = valor;
	ifrmCmbProdutoAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].disabled = valor;
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		cmbVariedadeAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].disabled = valor;
	<%}%>
}

function editaProduto(nRow){

	if(reembolsoProdutoForm['remaInRepor'].checked != true)
	{		
		if (ifrmlstressarcimento.reembolsoProdutoForm.prtrNrSequencia.length == undefined){
			reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrNrSequencia'].value = ifrmlstressarcimento.reembolsoProdutoForm.prtrNrSequencia.value;
			ifrmCmbLinha.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = ifrmlstressarcimento.reembolsoProdutoForm.idLinhCdLinha.value;
			ifrmCmbLinha.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = ifrmlstressarcimento.reembolsoProdutoForm.idAsnCdAssuntoNivel.value;
			ifrmCmbLinha.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = ifrmlstressarcimento.reembolsoProdutoForm.idAsn2CdAssuntoNivel2.value;
			ifrmCmbLinha.submeteForm();
			
			reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrVlUnitario'].value = ifrmlstressarcimento.reembolsoProdutoForm.prtrVlUnitario.value;
			reembolsoProdutoForm.txtQuantidadeProduto.value = ifrmlstressarcimento.reembolsoProdutoForm.prtrNrQuantidade.value;
			reembolsoProdutoForm.txtVlTotal.value = ifrmlstressarcimento.reembolsoProdutoForm.prtrVlTotal.value;			
			
		}else{
			reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrNrSequencia'].value = ifrmlstressarcimento.reembolsoProdutoForm.prtrNrSequencia[nRow].value;
			ifrmCmbLinha.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = ifrmlstressarcimento.reembolsoProdutoForm.idLinhCdLinha[nRow].value;
			ifrmCmbLinha.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = ifrmlstressarcimento.reembolsoProdutoForm.idAsnCdAssuntoNivel[nRow].value;
			ifrmCmbLinha.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = ifrmlstressarcimento.reembolsoProdutoForm.idAsn2CdAssuntoNivel2[nRow].value;
			ifrmCmbLinha.submeteForm();
			
			reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrVlUnitario'].value = ifrmlstressarcimento.reembolsoProdutoForm.prtrVlUnitario[nRow].value;
			reembolsoProdutoForm.txtQuantidadeProduto.value = ifrmlstressarcimento.reembolsoProdutoForm.prtrNrQuantidade[nRow].value;
			reembolsoProdutoForm.txtVlTotal.value = ifrmlstressarcimento.reembolsoProdutoForm.prtrVlTotal[nRow].value;
		}			
		setTimeout('desabilitarCmbProdutos('+ true +')',500);

		parent.isEditandoProduto = true;

	}
	else
	{
		alert('<bean:message key="prompt.paraEditarProdutoOpcaoReporNaoPodeEstarMarcada"/>');
	}
}

function editaAcessorio(nRow){
  
	if(reembolsoProdutoForm['remaInRepor'].checked != true)
	{
		if (ifrmLstReembolso.reembolsoProdutoForm.prtrNrSequencia.length == undefined){
			reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrNrSequencia'].value = ifrmLstReembolso.reembolsoProdutoForm.prtrNrSequencia.value;
			ifrmCmbLinhaAcessorio.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = ifrmLstReembolso.reembolsoProdutoForm.idLinhCdLinha.value;
			ifrmCmbLinhaAcessorio.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = ifrmLstReembolso.reembolsoProdutoForm.idAsnCdAssuntoNivel.value;
			ifrmCmbLinhaAcessorio.submeteForm();
			
			reembolsoProdutoForm.txtQuantidadeAcessorio.value = ifrmLstReembolso.reembolsoProdutoForm.prtrNrQuantidade.value;
			
		}else{
			reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrNrSequencia'].value = ifrmLstReembolso.reembolsoProdutoForm.prtrNrSequencia[nRow].value;
			ifrmCmbLinhaAcessorio.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = ifrmLstReembolso.reembolsoProdutoForm.idLinhCdLinha[nRow].value;
			ifrmCmbLinhaAcessorio.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = ifrmLstReembolso.reembolsoProdutoForm.idAsnCdAssuntoNivel[nRow].value;
			ifrmCmbLinhaAcessorio.submeteForm();
			
			reembolsoProdutoForm.txtQuantidadeAcessorio.value = ifrmLstReembolso.reembolsoProdutoForm.prtrNrQuantidade[nRow].value;
		}
		setTimeout('desabilitarCmbAcessorios('+ true +')',500);

		parent.isEditandoAcessorio = true;	
	}
	else
	{
		alert('<bean:message key="prompt.paraEditarAcessorioOpcaoReporNaoPodeEstarMarcada"/>');
	}
}


function limpaCampos(tipo){

	if (tipo=="PRODUTO"){
		ifrmCmbLinha.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = "";
		ifrmCmbLinha.submeteForm();
		reembolsoProdutoForm.txtQuantidadeProduto.value = "";
		reembolsoProdutoForm.txtVlTotal.value = "";
	}
	
	if (tipo=="ACESSORIO"){
		ifrmCmbLinhaAcessorio.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = "";
		ifrmCmbLinhaAcessorio.submeteForm();
		reembolsoProdutoForm.txtQuantidadeAcessorio.value = "";
	}

	reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrNrSequencia'].value = "0";
	reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = "";
	reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrVlUnitario'].value = "";
	reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrNrQuantidade'].value = "";
	
}

function limpaCamposReembolso(){

	reembolsoProdutoForm['csNgtbReembolsoReemVo.reemNrSequencia'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.idForeCdFormareemb'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.reemVlReembolso'].value = "";

	reembolsoProdutoForm.possuiReembolso.value = "";
	reembolsoProdutoForm.remaInReembolso.checked = false;
	
	limpaDadosBancario();
	limpaDadosEndereco();
	
	
}

function limpaDadosBancario(){
	
	if (ifrmCmbInfBanco.tipoInfo == "B")
		ifrmCmbInfBanco.document.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].value = "";

	reembolsoProdutoForm['csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsAgencia'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaNmTitular'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsRg'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsCpf'].value = "";

}

function limpaDadosEndereco(){

	if (ifrmCmbInfBanco.tipoInfo == "E")
		ifrmCmbInfBanco.document.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value = "";
	
	reembolsoProdutoForm['csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsNumero'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsComplemento'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsBairro'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsMunicipio'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsUf'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsCep'].value = "";
	reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsReferencia'].value = "";

}

var nCountHabilitaCheck = 0;

function habilitaCheck(){

	try{
		//reembolsoProdutoForm.remaInRepor.disabled = false;
		//reembolsoProdutoForm.remaInTroca.disabled = false;
	
		if ((reembolsoProdutoForm.possuiReembolso.value == "S") || (ifrmlstressarcimento.possuiProduto == true) || (ifrmLstReembolso.possuiAcessorio==true)){ 
			//reembolsoProdutoForm['remaInReembolso'].disabled = true;
			//reembolsoProdutoForm['remaInTroca'].disabled = true;
		}else{
			//reembolsoProdutoForm['remaInReembolso'].disabled = false;	
			//reembolsoProdutoForm['remaInTroca'].disabled = false;		
		}	
			
		//if (ifrmVlRessarcimento.reembolsoProdutoForm.txtVlARessarcir.value == "0,00"){
		if (obtemConteudoVlAressarcir() == "0,00" || obtemConteudoVlAressarcir() == ""){
			//reembolsoProdutoForm.remaInRepor.disabled = true;
			//reembolsoProdutoForm.remaInTroca.disabled = true;
			//reembolsoProdutoForm.remaInReembolso.disabled = true;
		}
	}catch(e){
		if(nCountHabilitaCheck < 20){
			setTimeout('habilitaCheck()',500);
			nCountHabilitaCheck++;
		}
	}
	
}

var nObtemConteudoVlAressarcir = 0;
function obtemConteudoVlAressarcir(){
	var retorno;
	//try{
		retorno = ifrmVlRessarcimento.reembolsoProdutoForm.txtVlARessarcir.value;
		nObtemConteudoVlAressarcir = 0;
		return retorno;
	//}catch(e){
		//if(nObtemConteudoVlAressarcir > 10){
		//	alert("erro em obtemConteudoVlAressarcir(): \n\n"+ e);
		//}
		//else{
		//	nObtemConteudoVlAressarcir++;
		//	setTimeout("obtemConteudoVlAressarcir()",200);
		//}
	//}
}

function trataCheckReembolso()
{
	if (reembolsoProdutoForm['remaInRepor'].checked == true){
		reembolsoProdutoForm['remaInTroca'].checked = false;
	
		var url="";
		
		url = "ReembolsoProduto.do?tela=inRepor&acao=<%=Constantes.ACAO_GRAVAR%>";
		url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		url = url + "&remaInRepor=" + reembolsoProdutoForm['remaInRepor'].value;
		url = url + "&csNgtbReembolsoReemVo.idForeCdFormareemb=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.idForeCdFormareemb'].value;
		url = url + "&possuiReembolso=" + reembolsoProdutoForm.possuiReembolso.value;
		url = url + "&csNgtbReembolsoReemVo.reemNrSequencia=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.reemNrSequencia'].value;
		url = url + "&idEmprCdEmpresa="+ reembolsoProdutoForm.idEmprCdEmpresa.value;
		
		document.location.href = url;		
	}
	if(reembolsoProdutoForm['remaInTroca'].checked == false)
	{		
		if (ifrmLstReembolso.possuiAcessorio==true || ifrmlstressarcimento.possuiProduto==true){
			if(confirm("<bean:message key='prompt.desejaExcluirItensListasProdutosAcessorios'/>"))
			{
			  url = "ReembolsoProduto.do?tela=inRepor&acao=<%=Constantes.ACAO_EXCLUIR%>";
			  url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
			  url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
			  url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
			  url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			  url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
			  url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
			  url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
			  url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			  url = url + "&remaInRepor=" + reembolsoProdutoForm['remaInRepor'].value;
			  url = url + "&csNgtbReembolsoReemVo.idForeCdFormareemb=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.idForeCdFormareemb'].value;
			  url = url + "&possuiReembolso=" + reembolsoProdutoForm.possuiReembolso.value;
			  url = url + "&csNgtbReembolsoReemVo.reemNrSequencia=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.reemNrSequencia'].value;
			  url = url + "&idEmprCdEmpresa="+ reembolsoProdutoForm.idEmprCdEmpresa.value;
				
			  document.location.href = url;		
			}
		}
		verificarCheck();		
	}
}

function trataCheck(tipo){

	window.top.onclick_TipoRessarcimento(tipo);

	if (tipo == "R"){
		if (reembolsoProdutoForm['remaInRepor'].checked == true){
			reembolsoProdutoForm['remaInTroca'].checked = false;
		}
	}
	if (tipo == "T"){
		if (reembolsoProdutoForm['remaInTroca'].checked == true){
			reembolsoProdutoForm['remaInRepor'].checked = false;
		}
		else
		{
		  if (ifrmLstReembolso.possuiAcessorio==true || ifrmlstressarcimento.possuiProduto==true){
			if(confirm("<bean:message key='prompt.desejaExcluirItensListasProdutosAcessorios'/>"))
			{
			  url = "ReembolsoProduto.do?tela=inRepor&acao=<%=Constantes.ACAO_EXCLUIR%>";
			  url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
			  url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
			  url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
			  url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			  url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
			  url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
			  url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
			  url = url + "&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			  url = url + "&remaInRepor=" + reembolsoProdutoForm['remaInRepor'].value;
			  url = url + "&csNgtbReembolsoReemVo.idForeCdFormareemb=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.idForeCdFormareemb'].value;
			  url = url + "&possuiReembolso=" + reembolsoProdutoForm.possuiReembolso.value;
			  url = url + "&csNgtbReembolsoReemVo.reemNrSequencia=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.reemNrSequencia'].value;
			  url = url + "&idEmprCdEmpresa="+ reembolsoProdutoForm.idEmprCdEmpresa.value;
				
			  document.location.href = url;		
			}
		  }
		}
	}
	//Comentado por carlos
	/*if (tipo == "R" || tipo == "T"){  
		if (!reembolsoProdutoForm['remaInRepor'].checked && !reembolsoProdutoForm['remaInTroca'].checked){
			if (ifrmLstReembolso.possuiAcessorio==true || ifrmlstressarcimento.possuiProduto==true){
				//alert ("Op��o inv�lida.\nExclua os produtos e acess�rios antes de escolher est� op��o.");
				alert ('<bean:message key="prompt.Opcao_invalida_Exclua_os_produtos_e_acessorios_antes_de_escolher_esta_opcao"/>.');
				if (tipo == "R"){
					reembolsoProdutoForm['remaInRepor'].checked = true;
				}
				if (tipo == "T"){
					reembolsoProdutoForm['remaInTroca'].checked = true;
				}			
			}
		}
	}	Fim do comentario do carlos*/	 
	
	if (reembolsoProdutoForm.possuiReembolso.value != "S"){
		//Limpando os campos da tela
		reembolsoProdutoForm["csNgtbReembolsoReemVo.idForeCdFormareemb"].selectedIndex = 0;
		setarCampoValor();
		carregaCmbInfo();
	}
	
	ifrmCmbLinha.document.reembolsoProdutoForm["csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value = "";
	ifrmCmbProduto.document.reembolsoProdutoForm["csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = "";
	reembolsoProdutoForm.txtQuantidadeProduto.value = "";
	reembolsoProdutoForm.txtVlTotal.value = "";
	
	limpaCampos("ACESSORIO");
	habilitaCampos();
	
}

function habilitaCampos(){
	if ((reembolsoProdutoForm['remaInTroca'].checked == false) && (reembolsoProdutoForm['remaInRepor'].checked == false)){
		habilitaCamposProduto(false);
		habilitaCamposAcessorios(false);
	}else{
		habilitaCamposProduto(true);
		habilitaCamposAcessorios(true);
	}

	habilitaCamposReembolso();
}

var nHabilitaCamposProduto = 0;
function habilitaCamposProduto(status){
	try{
		ifrmCmbLinha.document.getElementById("reembolsoProdutoForm")['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].disabled = !status;
		ifrmCmbProduto.document.getElementById("reembolsoProdutoForm")['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].disabled = !status;
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			ifrmCmbVariedadeManif.document.getElementById("reembolsoProdutoForm")['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].disabled = !status;
		<%}%>
	
		if(status){
			window.document.getElementById('LayerTravaProduto').style.visibility = "hidden";
		}else{
			window.document.getElementById('LayerTravaProduto').style.visibility = "visible";
		}
		
	
		//gambes para evitar que os campos "Data de Fabrica��o" "Lote" "Data Validade" fiquem travados por causa do div "LayerTravaProduto"
		if (window.top.document.getElementById('InfoLotes').style.visibility == 'visible'){
			window.top.MM_showHideLayers('ressarcimento','','show');
			window.top.MM_showHideLayers('ressarcimento','','hide');
		}
		//*******************************************************
		
		nHabilitaCamposProduto = 0;
	}catch(e){
		if(nHabilitaCamposProduto > 20){
			alert("Erro em habilitaCamposProduto(): \n\n"+ e.message);
		}
		else{
			nHabilitaCamposProduto++;
			setTimeout("habilitaCamposProduto('" + status + "')", 500);
		}
	}	
		
}

var nHabilitaCamposAcessorios = 0;
function habilitaCamposAcessorios(status){
	try{
		if (!window.top.infoLote.lstLote.possuiAcessorio){
			status = false;
		}
		
		ifrmCmbLinhaAcessorio.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].disabled = !status;
		ifrmCmbProdutoAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].disabled = !status;

		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			cmbVariedadeAcessorio.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].disabled = !status;
		<%}%>
		
		if(status){
			window.document.all.item('LayerTravaAcessorio').style.visibility = "hidden";
		}else{
			window.document.all.item('LayerTravaAcessorio').style.visibility = "visible";
		}	
		
		nHabilitaCamposAcessorios = 0;
	}catch(e){
		if(nHabilitaCamposAcessorios > 20){
			alert("Erro em habilitaCamposAcessorios(): \n\n"+ e);
		}
		else{
			nHabilitaCamposAcessorios++;
			setTimeout("habilitaCamposAcessorios('" + status + "')", 500);
		}
	}	
}

var nHabilitaCamposReembolso = 0;
function habilitaCamposReembolso(){
	var status = reembolsoProdutoForm['remaInReembolso'].checked;
	try{
		reembolsoProdutoForm['csNgtbReembolsoReemVo.idForeCdFormareemb'].disabled = !status;
		
		if (ifrmCmbInfBanco.tipoInfo == "B")
			ifrmCmbInfBanco.document.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].disabled = !status;
		else if (ifrmCmbInfBanco.tipoInfo == "E")	
			ifrmCmbInfBanco.document.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].disabled = !status;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		//	if(ifrmCmbInfBanco.document.readyState != 'complete')
		//	{
				setTimeout("habilitaCamposReembolso()",500);
		//	}
		<%}%>
		
		if(status){
			window.document.all.item('LayerTravaReembolso').style.visibility = "hidden";
		}else{
			window.document.all.item('LayerTravaReembolso').style.visibility = "visible";
		}
		nHabilitaCamposReembolso = 0;
	}catch(e){
		if(nHabilitaCamposReembolso > 20){
			alert("Erro em habilitaCamposReembolso(): \n\n"+ e);
		}
		else{
			setTimeout('habilitaCamposReembolso()', 500);
			nHabilitaCamposReembolso++;
		}
	}
}

function carregaTracking(){
	var url="";

	url = "Tracking.do?tela=<%=MCConstantes.TELA_TRACKING%>";
	url = url + "&acao=<%=MCConstantes.ACAO_SHOW_ALL%>"
	url = url + "&csNgtbReembolsoHistRehiVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
	url = url + "&csNgtbReembolsoHistRehiVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
	url = url + "&csNgtbReembolsoHistRehiVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	url = url + "&csNgtbReembolsoHistRehiVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	url = url + "&csNgtbReembolsoHistRehiVo.reemNrSequencia=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.reemNrSequencia'].value;
	
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:950px;dialogHeight:700px;dialogTop:20px;dialogLeft:35px')
}

function carregaCmbInfo(){

	var url="";
	
	url = "ReembolsoProduto.do?tela=ifrmCmbInfoBanco&acao=<%=MCConstantes.ACAO_SHOW_ALL%>";
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	url = url + "&csNgtbReembolsoReemVo.reemNrSequencia=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.reemNrSequencia'].value;
	url = url + "&csNgtbReembolsoReemVo.idForeCdFormareemb=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.idForeCdFormareemb'].value;
	url = url + "&usarFormaRetornoTela=S";
	url = url + "&idEmprCdEmpresa="+ reembolsoProdutoForm.idEmprCdEmpresa.value;
	
	ifrmCmbInfBanco.document.location.href = url;

}

var janela;
function MostraTelaDados(){
	var url="";
	var parametrosUrl = "";

	if (reembolsoProdutoForm['csNgtbReembolsoReemVo.idForeCdFormareemb'].value == ""){
		alert("<bean:message key='prompt.selecioneTipoRessarcimento'/>");
		return false;
	}	
	
	try{
		//Capturando os valores dos dados banc�rios para carregar na tela para edi��o de dados banc�rios
		var indice = ifrmCmbInfBanco.document.reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].selectedIndex - 1;
		if(indice >= 0){
			if(ifrmCmbInfBanco.document.reembolsoProdutoForm.txtIdBanco.length != undefined){
				parametrosUrl += "&csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtIdBanco[indice].value;
				parametrosUrl += "&csNgtbReembolsoReemVo.remaDsAgencia="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsAgencia[indice].value;
				parametrosUrl += "&csNgtbReembolsoReemVo.remaDsContacorrente="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsContacorrente[indice].value;
				parametrosUrl += "&csNgtbReembolsoReemVo.remaNmTitular="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtNmTitular[indice].value;
				parametrosUrl += "&csNgtbReembolsoReemVo.remaDsRg="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsRg[indice].value;
				parametrosUrl += "&csNgtbReembolsoReemVo.remaDsCpf="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsCpf[indice].value;
			}
			else{
				parametrosUrl += "&csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtIdBanco.value;
				parametrosUrl += "&csNgtbReembolsoReemVo.remaDsAgencia="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsAgencia.value;
				parametrosUrl += "&csNgtbReembolsoReemVo.remaDsContacorrente="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsContacorrente.value;
				parametrosUrl += "&csNgtbReembolsoReemVo.remaNmTitular="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtNmTitular.value;
				parametrosUrl += "&csNgtbReembolsoReemVo.remaDsRg="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsRg.value;
				parametrosUrl += "&csNgtbReembolsoReemVo.remaDsCpf="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtDsCpf.value;
			}
		}
	}catch(x){}
	
	url = 'ReembolsoProduto.do?tela=ifrmDadosFinBancarios&acao=<%=MCConstantes.ACAO_SHOW_ALL%>';
	url = url + "&csNgtbReembolsoReemVo.idForeCdFormareemb=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.idForeCdFormareemb'].value;
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	url = url + "&csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	url = url + "&csNgtbReembolsoReemVo.reemNrSequencia=" + reembolsoProdutoForm['csNgtbReembolsoReemVo.reemNrSequencia'].value;
	url = url + "&txtInfoBanco="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtInfoBanco.value;
	url = url + "&txtInfoEndereco="+ ifrmCmbInfBanco.document.reembolsoProdutoForm.txtInfoEndereco.value;
	url = url + "&usarFormaRetornoTela=S";
	url = url + "&idEmprCdEmpresa="+ reembolsoProdutoForm.idEmprCdEmpresa.value;

	url += parametrosUrl;
	
	janela = showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:360px;dialogTop:250px;dialogLeft:250px');
}

function carregaReenvio(){
	var idChamCdChamado = reembolsoProdutoForm["csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value;
	var maniNrSequencia = reembolsoProdutoForm["csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value;
	var idAsn1CdAssuntonivel1 = reembolsoProdutoForm["csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value;
	var idAsn2CdAssuntonivel2 = reembolsoProdutoForm["csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
	var reemNrSequencia = reembolsoProdutoForm["csNgtbReembolsoReemVo.reemNrSequencia"].value;
	var rmhiInTipo = "";
	if(reembolsoProdutoForm.remaInTroca.checked)
		rmhiInTipo = "T";
	else if(reembolsoProdutoForm.remaInReembolso.checked)
		rmhiInTipo = "C"
	
	var  url;
	url = "Reenvio.do?tela=<%=MCConstantes.TELA_NOVASOLICSERVICO%>"+
		"&reenvioVo.idChamCdChamado="+ idChamCdChamado +
		"&reenvioVo.maniNrSequencia="+ maniNrSequencia +
		"&reenvioVo.idAsn1CdAssuntonivel1="+ idAsn1CdAssuntonivel1 +
		"&reenvioVo.idAsn2CdAssuntonivel2="+ idAsn2CdAssuntonivel2 +
		"&reenvioVo.reemNrSequencia="+ reemNrSequencia +
		"&rmhiInTipo="+ rmhiInTipo;
		
		
	
	showModalDialog(url,this,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:385px;dialogTop:160px;dialogLeft:110px');
}

var nSetarCampoValor = 0;
function setarCampoValor(){
	//Colocando automaticamente o valor calculado no campo valor
	try{
		reembolsoProdutoForm["csNgtbReembolsoReemVo.reemVlReembolso"].value = ifrmVlRessarcimento.reembolsoProdutoForm.txtVlEmDinheiro.value;
		nSetarCampoValor = 0;
	}
	catch(x){
		if(nSetarCampoValor > 20){
			alert("Erro em setarCampoValor(): \n\n"+ x);
		}
		else{
			nSetarCampoValor++;
			setTimeout("setarCampoValor();", 500);
		}
	}
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();setTimeout('setarCampoValor()',5000);carregaCmbInfo();" scroll="no">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm" >
  <html:hidden property="tela"/>
  <html:hidden property="acao"/>
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.prtrNrSequencia" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
  <html:hidden property="csNgtbProdutotrocaPrtrVo.prtrNrQuantidade" />
  
  
  <!-- marca o envio atual -->
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csDmtbEnvioEnviVo.idEnviCdEnvio"/>
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csDmtbEnvioEnviVo.enviDsEnvio"/>
  <!-- ******************* -->

  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
  <html:hidden property="csNgtbReembolsoReemVo.reemNrSequencia" />
  <html:hidden property="csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco" />
  <html:hidden property="csNgtbReembolsoReemVo.remaDsAgencia" />
  <html:hidden property="csNgtbReembolsoReemVo.remaDsContacorrente" />
  <html:hidden property="csNgtbReembolsoReemVo.remaNmTitular" />
  <html:hidden property="csNgtbReembolsoReemVo.remaDsRg" />
  <html:hidden property="csNgtbReembolsoReemVo.remaDsCpf" />
  
  <html:hidden property="csNgtbReembolsoReemVo.csDmtbTpenderecoTpenVo.idTpenCdTpendereco"/>
  <html:hidden property="csNgtbReembolsoReemVo.remaDsLogradouro"/>
  <html:hidden property="csNgtbReembolsoReemVo.remaDsNumero"/>
  <html:hidden property="csNgtbReembolsoReemVo.remaDsComplemento"/>
  <html:hidden property="csNgtbReembolsoReemVo.remaDsReferencia"/>
  <html:hidden property="csNgtbReembolsoReemVo.remaDsBairro"/>
  <html:hidden property="csNgtbReembolsoReemVo.remaDsCep"/>
  <html:hidden property="csNgtbReembolsoReemVo.remaDsMunicipio"/>
  <html:hidden property="csNgtbReembolsoReemVo.remaDsUf"/>

  <html:hidden property="possuiReembolso"/>
  
  <input type="hidden" name="idEmprCdEmpresa" value="<%= empresaVo.getIdEmprCdEmpresa()%>"/>
  
  <div id="LayerTravaProduto" class="geralLayerDisable" style="position:absolute; left:6px; top:46px; width:773px; height:120px; z-index:35; background-color: #F3F3F3; background-color: #FFFFFF; border: 1px none #000000; visibility: hidden"></div>
  <div id="LayerTravaAcessorio" class="geralLayerDisable" style="position:absolute; left:6px; top:318px; width:773px; height:110px; z-index:31; background-color: #F3F3F3; background-color: #FFFFFF; border: 1px none #000000; visibility: hidden"></div>
  <div id="LayerTravaReembolso" class="geralLayerDisable" style="position:absolute; left:6px; top:190px; width:780px; height:45px; z-index:32; background-color: #F3F3F3; background-color: #FFFFFF; border: 1px none #000000; visibility: hidden"></div>        
  
  <div id="LayerEnvioAtual" style="position:absolute; left:300px; top:15px; width:200px; height:20px; z-index:33; visibility: visible">
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr><td class="principalLabel"><b><span id="divEnvio" name="divEnvio">&nbsp;</span></b></td></tr> 
  	</table>
  </div>        
  
  <table width="98%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="Espaco" align="right"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"> 
      </td>
      <td class="principalLabel" width="68%" align="right">
      	<div id="checkRepor" name="checkRepor"> 
        	<html:checkbox property="remaInRepor"  onclick="trataCheckReembolso();"/><bean:message key="prompt.Repor"/> 
        </div>
      </td>
      <td class="principalLabel" align="right">
        <html:checkbox property="remaInTroca" onclick="trataCheck('T');"/><bean:message key="prompt.troca"/> 
        <html:checkbox property="remaInReembolso" onclick="trataCheck('D');"/><bean:message key="prompt.Reembolso"/> &nbsp;&nbsp;&nbsp;
      </td>
    </tr>
    <tr> 
      <td class="principalPstQuadroLinkSelecionado"><%=getMessage("prompt.Produto",request)%></td>
    </tr>
  </table>
  <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
    <tr> 
      <td height="120" align="center" valign="top"> 
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td colspan="7"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
          </tr>
          <tr> 
            <td width="20%" class="principalLabel"><%=getMessage("prompt.linha",request)%></td>
            <td width="31%" class="principalLabel"><%=getMessage("prompt.Produto",request)%></td>
            <td width="20%" class="principalLabel">
            <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
            	<%=getMessage("prompt.Variedade",request)%>
            <%}%>
            </td> 
            <td width="9%" class="principalLabel"><bean:message key="prompt.ValorUnitario"/></td>
            <td width="8%" class="principalLabel"><bean:message key="prompt.Quantidade"/></td>
            <td width="9%" class="principalLabel"><bean:message key="prompt.ValorTotal"/></td>
            <td width="3%" class="principalLabel">&nbsp;</td>
          </tr>
          
          <tr>            
	        <td width="20%" height="23">
	          	<iframe id=ifrmCmbLinha name="ifrmCmbLinha" src="ReembolsoProduto.do?tela=ifrmCmbLinha&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	        </td>
	        <td width="31%" height="23">	        	
          		<iframe id=ifrmCmbProduto name="ifrmCmbProduto" src="ReembolsoProduto.do?tela=ifrmCmbProduto&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	        </td>
	        <td width="20%" class="principalLabel" height="23">
	        	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
					<iframe id="cmbVariedade" name="cmbVariedade" src="ReembolsoProduto.do?tela=<%=MCConstantes.TELA_CMB_VARIEDADE%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
				<%}%>
			</td>
            <td width="9%"> 
              <html:text property="csNgtbProdutotrocaPrtrVo.prtrVlUnitario" readonly="true" styleClass="principalObjForm" />
            </td>
            <td width="8%"> 
              <input type="text" name="txtQuantidadeProduto" class="principalObjForm" onchange="calculaValorProduto()" onkeypress="isDigito(this)"/>
            </td>
            <td width="9%"> 
              <input type="text" name="txtVlTotal" readonly class="principalObjForm">
            </td>
            <td width="3%" align="center"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" title='<bean:message key="prompt.Gravar_Produto_Troca"/>' class="geralCursoHand" onclick="submeteProdutoTroca()"> 
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td valign="top" height="70"><iframe id=ifrmlstressarcimento name="ifrmlstressarcimento" src="<%=Geral.getActionProperty("reembolsoProdutoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=ifrmLstRessarcimento" width="100%" height="100%" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="98%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
      <td align="right"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
    </tr>
    <tr> 
      <td class="principalPstQuadroLinkSelecionado"><bean:message key="prompt.Reembolso"/></td>
      <td class="principalLabel" align="right">&nbsp;</td>
    </tr>
  </table>
  <table width="98%" border="0" cellspacing="0" cellpadding="0" height="50" class="principalBordaQuadro">
    <tr> 
      <td align="center" valign="top"> 
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td colspan="4" class="Espaco">&nbsp;</td>
          </tr>
          <tr> 
            <td width="24%" class="principalLabel"><bean:message key="prompt.FormaReembolso"/></td>
            <td width="17%" class="principalLabel"><bean:message key="prompt.valor"/></td>
            <td class="principalLabel" width="53%"><span id="lblComboInfo" name="lblComboInfo">&nbsp;</td>
            <td width="6%" class="principalLabel">&nbsp;</td>
          </tr>
          <tr> 
            <td width="24%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
              	<tr>
              		<td width="7%">
			           <img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" title='<bean:message key="prompt.excluir"/>' onclick="removeReembolso()"> 
			        </td>
			        <td width="93%">      
					  <html:select property="csNgtbReembolsoReemVo.idForeCdFormareemb" styleClass="principalObjForm" onchange="carregaCmbInfo()" onclick="setarCampoValor();" >
						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						<logic:present name="CsDmtbFormareembForeVector">
					      <html:options collection="CsDmtbFormareembForeVector" property="idForeCdFormareemb" labelProperty="foreDsFormareeb"/>
					    </logic:present>
					  </html:select>
					</td>	  
				</tr>
			  </table>			  
            </td>
            <td width="17%" height="23"> 
              <html:text property="csNgtbReembolsoReemVo.reemVlReembolso" styleClass="principalObjForm" onkeydown="return isDigitoVirgula(event);" onblur="numberValidate(this, 2, '.', ',', event);" />
            </td>
            <td width="53%" height="23">
	            <iframe id=ifrmCmbInfBanco name="ifrmCmbInfBanco" src="ReembolsoProduto.do?tela=ifrmCmbInfoBanco" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
            </td>
            <td width="6%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td align="center" width="30%">
                  	<img id="btnNovoInfo" src="webFiles/images/botoes/new.gif" width="14" height="16" title='<bean:message key="prompt.Incluir_Dados_Bancarios"/>' onClick="MostraTelaDados()" class="geralCursoHand"> 
                  </td>
                  <td align="right" width="70%">
                  	<img src="webFiles/images/botoes/confirmaEdicao.gif" width="21" height="18" class="geralCursoHand" title='<bean:message key="prompt.GravarReembolso"/>' onclick="submeteReembolso()"> 
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="98%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="principalLabel" colspan="8" height="53">
      	<iframe id=ifrmVlRessarcimento name="ifrmVlRessarcimento" src="ReembolsoProduto.do?tela=ifrmVlRessarcimento" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPqn">
    <tr> 
      <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
    </tr>
  </table>
  <table width="98%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="principalPstQuadroLinkSelecionado"><bean:message key="prompt.Acessorio"/></td>
      <td class="principalLabel">&nbsp;</td>
    </tr>
  </table>
  <table width="98%" border="0" cellspacing="0" cellpadding="0" height="80" class="principalBordaQuadro">
    <tr> 
      <td align="center" valign="top"> 
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td colspan="5"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
          </tr>
          <tr> 
            <td width="23%" class="principalLabel"><%=getMessage("prompt.linha",request)%></td>
            <td width="40%" class="principalLabel"><bean:message key="prompt.Acessorio"/></td>
            <td width="25%" class="principalLabel">
            <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
            	<%=getMessage("prompt.Variedade",request)%>
            <%}%>
            </td>
            <td class="principalLabel" width="8%"><bean:message key="prompt.Quantidade"/></td>
            <td width="4%" class="principalLabel">&nbsp;</td>
          </tr>
          <tr> 
            <td width="23%" height="23"> 
				<iframe id=ifrmCmbLinhaAcessorio name="ifrmCmbLinhaAcessorio" src="ReembolsoProduto.do?tela=ifrmCmbLinhaAcessorio&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            </td>
            <td width="40%" height="23"> 
				<iframe id=ifrmCmbProdutoAcessorio name="ifrmCmbProdutoAcessorio" src="ReembolsoProduto.do?tela=ifrmCmbProdutoAcessorio&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            </td>
            <td width="25%" class="principalLabel" height="23">
            <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
            	<iframe id=cmbVariedadeAcessorio name="cmbVariedadeAcessorio" src="ReembolsoProduto.do?tela=<%=MCConstantes.TELA_CMB_VARIEDADE_ACESSORIO%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            <%}%>
            <td width="1%"> 
              <input type="text" name="txtQuantidadeAcessorio" class="principalObjForm" onkeypress="isDigito(this)" >
            </td>
            <td width="4%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title='<bean:message key="prompt.Gravar_Acessorio"/>' onclick="submeteProdutoAcessorio()"> 
            </td>
          </tr>
          <tr> 
            <td colspan="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
          </tr>
          <tr> 
            <td colspan="4" class="esquerdoLstPar"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="3%" class="principalLstCab">&nbsp;</td>
                  
                <td class="principalLstCab" width="29%"><%=getMessage("prompt.linha",request)%></td>
                  <td class="principalLstCab" width="44%"><bean:message key="prompt.Acessorio"/></td>
                  <td class="principalLstCab" width="24%"><bean:message key="prompt.Quantidade"/></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  
                <td valign="top" height="50"><iframe id=ifrmLstReembolso name="ifrmLstReembolso" src="ReembolsoProduto.do?tela=ifrmLstReembolso" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="21%"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
    <td width="21%"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
    <td colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
  </tr>
  <tr style="visibility: hidden"> 
    <td width="21%">&nbsp; </td>
    <td width="21%">&nbsp; </td>
    <td width="40%" class="principalLabel"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
        <tr> 
          <td class="principalLabel" width="30%">&nbsp; </td>
          <td width="25%" align="right"><img src="webFiles/images/botoes/bt_opcoesBackoffice.gif" width="22" height="23" onClick="" class="geralCursoHand">&nbsp;&nbsp;</td>
          <td width="45%" class="geralCursoHand" onClick="" ><bean:message key="prompt.novaSolicitacaoServ"/></td>
        </tr>
      </table>
    </td>
    <td width="18%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="21%" align="center"><img src="webFiles/images/botoes/bt_tracking.gif" width="20" height="20" class="geralCursoHand" onClick=""></td>
          <td width="52%" class="principalLabel"><span class="geralCursoHand" onClick="">Tracking</span></td>
          <td width="12%" align="center">&nbsp;</td>
          <td width="15%" align="center">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<iframe id=ifrmCalculoReembolsoProduto name="ifrmCalculoReembolsoProduto" src="ReembolsoProduto.do?tela=ifrmCalculoReembolsoProduto" width="0" height="0" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" >
</html:form>
</body>
</html>
