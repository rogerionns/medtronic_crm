<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

function iniciaTela(){
	
	if (reembolsoProdutoForm.acao.value == '<%=Constantes.ACAO_FITRAR%>'){
		parent.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrVlUnitario'].value = reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.prasVlUnitario'].value;
		parent.calculaValorProduto();
	}
	
	
	if (reembolsoProdutoForm.acao.value == '<%=Constantes.ACAO_EXCLUIR%>'){
		//parent.habilitaCheck();
		//parent.habilitaCampos();
		parent.carregaCamposValor(); 
	}
	
	if (reembolsoProdutoForm.acao.value == '<%=Constantes.ACAO_GRAVAR%>'){

		parent.reembolsoProdutoForm['csNgtbReembolsoReemVo.reemNrSequencia'].value = '<bean:write name="reembolsoProdutoForm" property="csNgtbReembolsoReemVo.reemNrSequencia"/>';
		parent.reembolsoProdutoForm['csNgtbReembolsoReemVo.idForeCdFormareemb'].value = '<bean:write name="reembolsoProdutoForm" property="csNgtbReembolsoReemVo.idForeCdFormareemb"/>';
		parent.reembolsoProdutoForm['csNgtbReembolsoReemVo.reemVlReembolso'].value = '<bean:write name="reembolsoProdutoForm" property="csNgtbReembolsoReemVo.reemVlReembolso"/>';
		
		if (parent.ifrmCmbInfBanco.tipoInfo == "B"){
			parent.ifrmCmbInfBanco.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].value = '<bean:write name="reembolsoProdutoForm" property="csNgtbReembolsoReemVo.remaDsContacorrente"/>';
			parent.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].value = '<bean:write name="reembolsoProdutoForm" property="csNgtbReembolsoReemVo.remaDsContacorrente"/>';
		}else{	
			parent.ifrmCmbInfBanco.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value = '<bean:write name="reembolsoProdutoForm" property="csNgtbReembolsoReemVo.remaDsLogradouro"/>';
			parent.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsLogradouro'].value = '<bean:write name="reembolsoProdutoForm" property="csNgtbReembolsoReemVo.remaDsLogradouro"/>';
		}
		parent.reembolsoProdutoForm.possuiReembolso.value = "S";
		parent.reembolsoProdutoForm.remaInReembolso.checked = true;

		//parent.habilitaCheck();
		//parent.habilitaCampos();
		parent.carregaCamposValor();
	}
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.prasVlUnitario" />
</html:form>

<script language="JavaScript">
<logic:present name="alerta" scope="session">
	alert("<bean:write name='alerta' />");
	<%request.getSession().removeAttribute("alerta");%>
</logic:present>
</script>

</body>
</html>