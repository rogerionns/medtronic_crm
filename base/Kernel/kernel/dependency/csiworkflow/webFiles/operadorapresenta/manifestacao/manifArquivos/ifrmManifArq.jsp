<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

function iniciaTela(){
	manifArquivoForm.idChamCdChamado.value = parent.manifArquivoForm.idChamCdChamado.value;
	manifArquivoForm.maniNrSequencia.value = parent.manifArquivoForm.maniNrSequencia.value;
	manifArquivoForm.idAsn1CdAssuntoNivel1.value = parent.manifArquivoForm.idAsn1CdAssuntoNivel1.value;
	manifArquivoForm.idAsn2CdAssuntoNivel2.value = parent.manifArquivoForm.idAsn2CdAssuntoNivel2.value;
	
	manifArquivoForm['csAstbManifArquivoMaarVo.idChamCdChamado'].value = parent.manifArquivoForm['csAstbManifArquivoMaarVo.idChamCdChamado'].value;
	manifArquivoForm['csAstbManifArquivoMaarVo.maniNrSequencia'].value = parent.manifArquivoForm['csAstbManifArquivoMaarVo.maniNrSequencia'].value;
	manifArquivoForm['csAstbManifArquivoMaarVo.idAsn1CdAssuntonivel1'].value = parent.manifArquivoForm['csAstbManifArquivoMaarVo.idAsn1CdAssuntonivel1'].value;
	manifArquivoForm['csAstbManifArquivoMaarVo.idAsn2CdAssuntonivel2'].value = parent.manifArquivoForm['csAstbManifArquivoMaarVo.idAsn2CdAssuntonivel2'].value;
	manifArquivoForm['csNgtmManifArqTempMartVo.martDsSessao'].value = parent.manifArquivoForm['csNgtmManifArqTempMartVo.martDsSessao'].value;
}

function anexaArquivo(){

	if (manifArquivoForm.pathArquivo.value == ""){
		alert ('<bean:message key="prompt.Por_favor_escolha_um_arquivo"/>');
		return false;
	}

	//parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
	
	manifArquivoForm.tela.value="<%=MCConstantes.TELA_IFRM_LST_MANIFARQUIVO%>";
	manifArquivoForm.acao.value="<%=Constantes.ACAO_INCLUIR%>";
	manifArquivoForm.target="ifrmLstManifArquivo";
	
	manifArquivoForm.submit();
	
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ManifArquivo.do" enctype="multipart/form-data" styleId="manifArquivoForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>

<html:hidden property="idChamCdChamado"/>
<html:hidden property="maniNrSequencia"/>
<html:hidden property="idAsn1CdAssuntoNivel1"/>
<html:hidden property="idAsn2CdAssuntoNivel2"/>

<html:hidden property="csAstbManifArquivoMaarVo.idChamCdChamado"/>
<html:hidden property="csAstbManifArquivoMaarVo.maniNrSequencia"/>
<html:hidden property="csAstbManifArquivoMaarVo.idAsn1CdAssuntonivel1"/>
<html:hidden property="csAstbManifArquivoMaarVo.idAsn2CdAssuntonivel2"/>
<html:hidden property="csNgtmManifArqTempMartVo.martDsSessao"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
	<tr>
		<td><html:file property="pathArquivo" styleClass="principalObjForm"/></td>
	</tr>
</html:form>
</body>
</html>
