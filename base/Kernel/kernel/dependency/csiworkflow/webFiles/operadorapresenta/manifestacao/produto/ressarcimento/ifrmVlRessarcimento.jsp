<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
var contIniciaTela = 0;
function iniciaTela(){
	try{
		parent.habilitaCheck();
	}
	catch(x){
		if(contIniciaTela < 20){
			contIniciaTela++;
			setTimeout("iniciaTela();", 1000);
		}
		else{
			alert("Erro ao tentar carregar tela VlRessarcimento:\n"+ x.description);
		}
	}
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="principalLabel" colspan="8" height="8">&nbsp; </td>
    </tr>
    <tr> 
      <td class="principalLabel" width="10%">&nbsp;</td>
      <td class="principalLabel" width="17%"><bean:message key="prompt.A_Ressarcir"/></td>
      <td class="principalLabel" width="5%">&nbsp;</td>
      <td class="principalLabel" width="20%"><bean:message key="prompt.Ressarcido_em_Produtos"/></td>
      <td class="principalLabel" width="5%">&nbsp;</td>
      <td class="principalLabel" width="19%"><bean:message key="prompt.Ressarcido_em_Dinheiro"/></td>
      <td class="principalLabel" width="5%">&nbsp;</td>
      <td class="principalLabel" width="19%"><bean:message key="prompt.Saldo_a_Reembolsar"/></td>
    </tr>
    <tr> 
      <td class="principalLabel" align="right" width="10%" valign="top"><bean:message key="prompt.totais"/> 
        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
      <td width="17%" valign="top"> 
        <html:text property="txtVlARessarcir" styleClass="principalObjForm" readonly="true"/>
      </td>
      <td width="5%" valign="top" align="center"> -</td>
      <td width="20%" valign="top"> 
        <html:text property="txtVlEmProduto" styleClass="principalObjForm" readonly="true"/>
      </td>
      <td width="5%" valign="top" align="center"> -</td>
      <td width="19%" valign="top"> 
        <html:text property="txtVlEmDinheiro" styleClass="principalObjForm" readonly="true"/>
      </td>
      <td width="5%" valign="top" align="center">=</td>
      <td width="19%" valign="top"> 
        <html:text property="txtVlSaldo" styleClass="principalObjForm" readonly="true"/>
      </td>
    </tr>
    <tr> 
      <td align="right" colspan="8" valign="bottom"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
    </tr>
  </table>
	<script language="JavaScript">
		//Se o saldo for negativo, o campo fica vermelho
		var vlSaldo = reembolsoProdutoForm.txtVlSaldo.value;
		if(vlSaldo.indexOf("-") == 0){
			reembolsoProdutoForm.txtVlSaldo.style.color = "#FF0000";
		}
	</script>
</html:form>
</body>
</html>
