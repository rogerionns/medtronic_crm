<%@page import="br.com.plusoft.saas.SaasHelper"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*, 
				br.com.plusoft.fw.app.Application"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	</head>
	
	<script language="JavaScript">
	    //Chamado: 95927 - 18/07/2014 - Carlos Nunes
		function atualizarCombosTela(isLoad)
		{
			$.post("getActionProperty.do", 
					{ 
				       id_empr_cd_empresa : document.empresaForm.csCdtbEmpresaEmpr.value,
				       propertie : "localizadorAtendimentoEspecAction"
				    }, 
				    function(ret) {
						if(ret.msgerro) {
							alert(ret.msgerro);
							return;
						}else{
							atualizarCombosTelaRetorno(isLoad, ret.propertie);
						}
				    }
			);
		}
		/***************************************************************
		 Atualiza os combos da tela de acordo com a empresa selecionada
		***************************************************************/
		function atualizarCombosTelaRetorno(isLoad, actionEspec){ //Chamado: 95927 - 18/07/2014 - Carlos Nunes
			parent.atualizarCmbManifTipo();
			parent.atualizarCmbLocalGrupoManif();
			parent.atualizarCmbLocalTipoManif();
			parent.atualizarCmbLocalLinha();
			parent.atualizarCmbLocalArea();
			parent.atualizarCmbLocalAtend();
			parent.atualizarCmbStatusManif();
			parent.atualizarCmbClassifManif();
			setTimeout('parent.atualizarCmbLocalEventoFollowup();',200);
			
			setTimeout("parent.parent.recarregarLogoTipo();",1000);

			if(isLoad==undefined || isLoad==false) {
				setTimeout("parent.carregaListaAtend();", 1000);
			}
			//Chamado: 90471 - 16/09/2013 - Carlos Nunes
			//------------------------------------------------------------------
			// Gargamel - 24-03-11
			// Fun��o para chamada espec quando atualizamos o combo de empresa
			try { parent.atualizarTela(); } catch(e) {}
			//------------------------------------------------------------------
			//Chamado: 95927 - 18/07/2014 - Carlos Nunes
		    try {
				parent.actionLocalizadorEspec = actionEspec;
				if( (actionEspec != "") && (actionEspec != "LocalizadorAtendimento.do") ){
					parent.localAtendEspec.location = actionEspec + '?tela=localizadorAtendimentoEspec';
					parent.document.getElementById("tdFiltroEspec").style.display = "block";
					parent.document.getElementById("divFiltroEspec").style.visibility = "visible";
				}
				else{
					parent.localAtendEspec.location = "about:blank";
					parent.document.getElementById("tdFiltroEspec").style.display = "none";
					parent.document.getElementById("divFiltroEspec").style.visibility = "hidden";
				}
				
				parent.AtivarPasta('FP');
			} catch(e) {} 

		}
		
		function empresaClick(){
			parent.atualizarCmbDesenhoprocesso();
			
			document.forms[0].acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
			document.forms[0].submit();
		}
	</script>
	
	<body leftmargin=0 rightmargin=0 bottommargin=0 topmargin=0 onload="atualizarCombosTela(true);" style="overflow: hidden;">
		
		<html:form action="MultiEmpresa.do" styleId="empresaForm">
			<html:hidden property="acao"/>
			<html:hidden property="tela"/>
			<html:select property="csCdtbEmpresaEmpr" styleClass="principalObjForm" onchange="empresaClick();">
				<html:options collection="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" labelProperty="emprDsEmpresa"/>
			</html:select>
			
			<%
      		String url = "";
			String urlCrm = "../csicrm/inicializarSessao.do";  
		try{
			CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
			url = Geral.getActionProperty("inicializaSessaoCRMCliente", empresaVo.getIdEmprCdEmpresa());
			CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			if(url != null && !url.equals("") && funcVo != null){
				url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();			
			}		
			
			urlCrm = urlCrm + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();
			
			long idFuncCdFuncionarioSaas = 0;
			long idEmprCdEmpresaSaas = 0;
			
			if(SaasHelper.aplicacaoSaas) {
				idFuncCdFuncionarioSaas = new SaasHelper().getSessionData().getIdFuncCdFuncionarioSaas();
			} 
			
			if(idFuncCdFuncionarioSaas > 0) {
				if(url != null && !url.equals("") && funcVo != null)
					url+= "&idFuncCdFuncionarioSaas="+idFuncCdFuncionarioSaas;
					
				urlCrm += "&idFuncCdFuncionarioSaas=" + funcVo.getIdFuncCdFuncionario();
			}
			
		}catch(Exception e){}
	  %>
          <iframe src="<%=url %>" id="ifrmSessaoEspec" name="ifrmSessaoEspec"  style="display:none"></iframe>  
		  <iframe src="<%=urlCrm %>" id="ifrmSessaoCrm" name="ifrmSessaoCrm"  style="display:none"></iframe>
		</html:form>
	</body>
</html>