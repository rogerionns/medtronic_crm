<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.concluirManifestacoesEmLote" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
	var nVar = 0;
	var tela = new Object();
	tela = window.dialogArguments;

	function selecionaTextoConclusao(){
		if( confirm('<bean:message key="prompt.confirmaConlusaoManifestacaoEmLote"/>') ){
			if(trim(document.localizadorAtendimentoForm.maniTxResposta.value) != ""){
				tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend").maniTxResposta.value = document.localizadorAtendimentoForm.maniTxResposta.value;
				//================
				//64319 - gargamel
				//================
				if ((!document.localizadorAtendimentoForm["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].disabled) && (document.localizadorAtendimentoForm["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].value!="0")) {
					tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend")["localizadorAtendimentoVo.idStmaCdStatusmanif"].value = document.localizadorAtendimentoForm["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].value;
				}
				//================

					tela.lstIndicacoes.alteraModalManifestacao();
				tela.transferenciaManifestacao= true;
				tela.verificarModalManifestacao();
				window.close();
			}else{
				alert('<bean:message key="prompt.preencherDescricaoConclusaoEmLote"/>');
			}
		}
	}

	//================
	//64319 - gargamel
	//================
	function mudaStatus(){
		if (!document.localizadorAtendimentoForm["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].disabled) {
			document.localizadorAtendimentoForm["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].value="0";
		} 
		document.localizadorAtendimentoForm["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].disabled=(!document.localizadorAtendimentoForm["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].disabled);
	}
	//================

</script>
</head>
	
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmDestinatario">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.conclusao" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="37" align="center"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalLabel">&nbsp;</td>
            <td class="principalLabel" width="6%">&nbsp;</td>
          </tr>
          <tr> 
            <td class="principalLabel"><bean:message key="prompt.conclusao" /></td>
            <td class="principalLabel" width="6%">&nbsp;</td>
          </tr>
          <tr> 
            <td class="principalLabel">
		    	<div id="Layer3" style="position:relative; width:575; z-index:1; overflow: auto; height:90"> 
			 		<html:textarea rows="5" styleClass="principalObjForm" property="maniTxResposta" onkeypress="textCounter(this, 1990)" onkeyup="textCounter(this, 1990)"/>
			 	</div>
            </td>            
            <td class="principalLabel" width="6%">&nbsp;</td>
          </tr>
          <tr>
            <td class="principalLabel" width="94%">
		        <table width="575" border="0" cellspacing="0" cellpadding="0">
		          <tr> 
					<td class="principalLabel" width="2%"><input type=checkbox name="chkStatus" onclick="mudaStatus();"></td>
					<td class="principalLabel" width="14%"><%= getMessage("prompt.alterar.status", request)%></td>
					<td class="principalLabel" width="84%">
						<html:select property="csCdtbStatusManifStmaVo.idStmaCdStatusmanif" styleClass="principalObjForm">
							<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
							<logic:present name="statusVector">
								<html:options collection="statusVector" property="idStmaCdStatusmanif" labelProperty="stmaDsStatusmanif" />  
							</logic:present>
						</html:select>
						<script>document.localizadorAtendimentoForm["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].disabled=true;</script>
					</td>
		          </tr>
				</table>
			</td>
            <td class="principalLabel" width="6%"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="selecionaTextoConclusao()" title="<bean:message key='prompt.concluir'/>"></td>
          </tr>
          <tr> 
            <td class="principalLabel">&nbsp;</td>
            <td class="principalLabel" width="6%">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" onClick="javascript:window.close()" class="geralCursoHand" title="<bean:message key="prompt.sair"/>"></td>
    </tr>
  </table>
</html:form>
</body>
</html>
