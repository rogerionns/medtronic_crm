<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

	var nCountCarregaVariedade =0;
	
	function carregaVariedade(){
		try{
			var url="";
			var nIdLinha;
			nIdLinha = parent.ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")['csCdtbLinhaLinhVo.idLinhCdLinha'].value;
						
			var id1AsnCdAssuntoNivel1;
			id1AsnCdAssuntoNivel1 = document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
			
			if (nIdLinha > 0 && Number(id1AsnCdAssuntoNivel1) > 0){
/*				window.parent.ifrmCmbLocalVariedade.location.href = "LocalizadorAtendimento.do?tela=cmbLocalVariedade&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbLinhaLinhVo.idLinhCdLinha=" + nIdLinha + "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + idAsn + "&csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + id1AsnCdAssuntoNivel1;
*/

				cTela = document.localizadorAtendimentoForm.tela.value;
				cAcao = document.localizadorAtendimentoForm.acao.value;
				document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = id1AsnCdAssuntoNivel1 + "@0";
				document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = 0;
				document.localizadorAtendimentoForm.acao.value = "<%= Constantes.ACAO_VISUALIZAR %>";
				document.localizadorAtendimentoForm.tela.value = "<%= MCConstantes.TELA_CMB_LOCAL_VARIEDADE %>";
				document.localizadorAtendimentoForm.target = parent.ifrmCmbLocalVariedade.name;
				document.localizadorAtendimentoForm.submit();
				document.localizadorAtendimentoForm.tela.value = cTela;
				document.localizadorAtendimentoForm.acao.value = cAcao;
			}
			else{
				window.parent.ifrmCmbLocalVariedade.location.href = "LocalizadorAtendimento.do?tela=cmbLocalVariedade";
			}
			
		}catch(e){
			if(nCountCarregaVariedade < 5){
				setTimeout('carregaVariedade()',500);
				nCountCarregaVariedade++;
			}
		}
	}	
	
	function getValorCombo(){
		if(document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value == '0' ||  document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value == ''){
			return document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		}else{
			return document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		}
	}
	
	function carregaManifTipo()
	{

		var nidAsn = document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO2") || Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO3") || Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO4")) {%>
		
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TIPOLINHA,request).equals("S")) {%>
				if (nidAsn != 0)
				{
					window.parent.ifrmCmbLocalManifTipo.location.href = "LocalizadorAtendimento.do?tela=cmbLocalManifTipo&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel="+ nidAsn +"&idEmprCdEmpresa="+ parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
				}else{
					//window.parent.ifrmCmbLocalManifTipo.location.href = "LocalizadorAtendimento.do?tela=cmbLocalManifTipo&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresa="+ parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
				}
			<%}%>
			
		<%}%>
	}
	
	function inicio(){
		//Posicionamento autom�tico do combo (Apenas quando s� existe um registro)
		if (localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].length == 2){
			localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'][1].selected = true;
		}
		
		if(localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value != '' && localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value > 0){
			parent.ifrmCmbLocalLinha.localizadorAtendimentoForm['csCdtbLinhaLinhVo.idLinhCdLinha'].value = localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;
			localizadorAtendimentoForm['csCdtbLinhaLinhVo.idLinhCdLinha'].value = localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;
		}
		
		if(localizadorAtendimentoForm['csCdtbLinhaLinhVo.idLinhCdLinha'].value != '' && localizadorAtendimentoForm['csCdtbLinhaLinhVo.idLinhCdLinha'].value > 0){
			parent.ifrmCmbLocalLinha.localizadorAtendimentoForm['csCdtbLinhaLinhVo.idLinhCdLinha'].value = localizadorAtendimentoForm['csCdtbLinhaLinhVo.idLinhCdLinha'].value;
			localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = localizadorAtendimentoForm['csCdtbLinhaLinhVo.idLinhCdLinha'].value;
		}
		
	}
	
	function carreganoOnLoad(){

		//CHAMADO 74100 - VINICIUS - VERIFICAR SE O COMBO TEM ALGUM ITEM SELECIONADO, CASO TENHA INSERE O VALOR NO HIDDEN
		if(ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value == 0 && ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value != ""){
			ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		}else if(ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value != 0 && ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value != ""){
			ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		}
		
		var nidAsn = document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		
				carregaVariedade();
				//Chamado 76577 - Vinicius - Comentado para que n�o seja mais vinculado a composi��o do produto com a de manifesta��o
				//setTimeout('window.parent.ifrmCmbLocalVariedade.carregaManifTipo()',1000);
	
		<%	}else{	%>	
			
			<%	if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO2") || Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO3")) {	%>
				//Chamado 76577 - Vinicius - Comentado para que n�o seja mais vinculado a composi��o do produto com a de manifesta��o
				//setTimeout("carregaManifTipo()",1000);	
			<% } %>
		
		<%	}	%>
	}

	//Chamado: 84937 - 26/10/2012 - Carlos Nunes
	function atualizarCombo()
	{
		document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		carreganoOnLoad();
	}
	
	function mostraCampoBuscaProd(){
		cTela = document.localizadorAtendimentoForm.tela.value;
		cAcao = document.localizadorAtendimentoForm.acao.value;
		document.localizadorAtendimentoForm.acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
		document.localizadorAtendimentoForm.tela.value = "<%= MCConstantes.TELA_CMB_LOCAL_PRODUTO %>";
		document.localizadorAtendimentoForm.target = this.name;
		document.localizadorAtendimentoForm.submit();
	}
	
	function buscarProduto(){
		
		if (document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}
		
		var idTpmaCdTpManifestacao = parent.ifrmCmbLocalTipoManif.localizadorAtendimentoForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		
		if (idTpmaCdTpManifestacao == "")
			idTpmaCdTpManifestacao = 0;
			 
		localizadorAtendimentoForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = idTpmaCdTpManifestacao;

		cTela = document.localizadorAtendimentoForm.tela.value;
		cAcao = document.localizadorAtendimentoForm.acao.value;
		document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = 0;
		document.localizadorAtendimentoForm.tela.value = "<%= MCConstantes.TELA_CMB_LOCAL_PRODUTO %>";
		document.localizadorAtendimentoForm.acao.value = "<%= Constantes.ACAO_FITRAR %>";
		document.localizadorAtendimentoForm.target = this.name;
		document.localizadorAtendimentoForm.submit();
	
	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="inicio();showError('<%=request.getAttribute("msgerro")%>');carreganoOnLoad();" style="overflow: hidden;">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmCmbLocalProduto">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="csCdtbLinhaLinhVo.idLinhCdLinha"/>
	<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"/>
	<html:hidden property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>
	<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
	<html:hidden property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />

	<logic:notEqual name="baseForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
		<table width="95%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="95%">
					<html:select property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" styleClass="principalObjForm" onchange="atualizarCombo();">
						<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
						<html:options collection="produtoVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" labelProperty="prasDsProdutoAssunto"/> 
 					</html:select>
			 	</td>
			 	<td width="5%" valign="middle">
			 		<div align="right" id="divBotao"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
			 	</td>
			</tr>
		</table>
	</logic:notEqual>
	  
	<logic:equal name="baseForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
		<table width="95%" border="0" cellspacing="0" cellpadding="0">	   	  
			<tr>
				<td width="95%">
					<html:text property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
				</td>
				<td width="5%" valign="middle">
					<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
				</td>
			</tr> 	
		</table>
		<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
	</logic:equal>
</html:form>
</body>
</html>
<logic:equal name="baseForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	<script>
		document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1'].focus();
	</script>
</logic:equal>
