<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,
                                 br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.LocalizadorAtendimentoVo"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

//pagina��o****************************************
long numRegTotal=0;

//Marco Costa - Chamado: 94790 - 13/05/2014
long numCount = 0;
long numManif = 0;
long numFollowup = 0;
long numEmAtraso = 0;

long numCoutConcluido = 0;
long numManifConcluido = 0;
long numFollowupConcluido = 0;
long numEmAtrasoConcluido = 0;

if (request.getAttribute("locAtendVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("locAtendVector"));
	if (v.size() > 0){
		numRegTotal = ((LocalizadorAtendimentoVo)v.get(0)).getNumRegTotal();
		
		//Marco Costa - Chamado: 94790 - 13/05/2014
		numCount = ((LocalizadorAtendimentoVo)v.get(0)).getNumCount();
		numManif = ((LocalizadorAtendimentoVo)v.get(0)).getNumManif();
		numFollowup = ((LocalizadorAtendimentoVo)v.get(0)).getNumFollowup();
		numEmAtraso = ((LocalizadorAtendimentoVo)v.get(0)).getNumEmAtraso();
		
		numCoutConcluido = ((LocalizadorAtendimentoVo)v.get(0)).getNumCoutConcluido();
		numManifConcluido = ((LocalizadorAtendimentoVo)v.get(0)).getNumManifConcluido();
		numFollowupConcluido = ((LocalizadorAtendimentoVo)v.get(0)).getNumFollowupConcluido();
		numEmAtrasoConcluido = ((LocalizadorAtendimentoVo)v.get(0)).getNumEmAtrasoConcluido();
		
	}
}

long regDe = 0;
long regAte = 0;

if (request.getParameter("regDe") != null && !((String)request.getParameter("regDe")).equals(""))
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null && !((String)request.getParameter("regAte")).equals(""))
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************

%>

<html>
<head>
<title>ifrmLstLocalizadorAtend</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
var bExisteReg;

bExisteReg = false;
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

var janelaManif;
var janelaPessoa;
var	chamado; 
var	manifestacao;
var	tpManifestacao;
var	assuntoNivel; 
var	assuntoNivel1; 
var	assuntoNivel2; 
var idPessoa;
var tppublico;
var dhEncerramento;
var empresa;

//Chamado: 80047
function verificaRegistro(idPessCdPessoa,idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, foupNrSequencia, idUtilCdFuncionario, idTppuCdTipopublico, idMatpCdManifTipo, idLinhCdLinha, idGrmaCdGrupoManifestacao, maniDhEncerramento,idEmpresa) {
	
	//Chamado: 84517 - 25/09/2012;
	//Chamado: 86878 - 04/04/2013 - Carlos Nunes
	//if(idEmpresa != '' && idEmpresa != '0' && parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value != idEmpresa)
	//{
		//parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value = idEmpresa;
		//parent.ifrmCmbEmpresa.empresaClick();
	//}
	
	cUrl = "<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=ifrmVerificaTrava"+
		"&localizadorAtendimentoVo.idPessCdPessoa=" + idPessCdPessoa +
		"&localizadorAtendimentoVo.idChamCdChamado=" + idChamCdChamado +
		"&idChamCdChamado=" + idChamCdChamado +
		"&localizadorAtendimentoVo.maniNrSequencia=" + maniNrSequencia +
		"&localizadorAtendimentoVo.idTpmaCdTpManifestacao=" + idTpmaCdTpManifestacao +
		"&localizadorAtendimentoVo.idAsnCdAssuntoNivel=" + idAsnCdAssuntoNivel +
		"&localizadorAtendimentoVo.foupNrSequencia=" + foupNrSequencia  +
		"&localizadorAtendimentoVo.idUtilCdFuncionario=" + idUtilCdFuncionario +
		"&localizadorAtendimentoVo.idTppuCdTipopublico=" + idTppuCdTipopublico +
		"&localizadorAtendimentoVo.idMatpCdManifTipo=" + idMatpCdManifTipo +
		"&localizadorAtendimentoVo.idLinhCdLinha=" + idLinhCdLinha +
		"&localizadorAtendimentoVo.idGrmaCdGrupoManifestacao=" + idGrmaCdGrupoManifestacao;

		if(idEmpresa != '' && idEmpresa != '0' && parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value != idEmpresa)
		{
			cUrl += "&idEmprCdEmpresa=" +  idEmpresa;
			parent.parent.empresaDoChamado = idEmpresa;
		}
		else
		{
			cUrl += "&idEmprCdEmpresa=" +  parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
			parent.parent.empresaDoChamado = parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
		}
		 

		cUrl +=  "&maniDhEncerramento=" + maniDhEncerramento;
		
	ifrmVerificacao.location.href = cUrl;	
}

//Chamado: 80047
function verificaRegistroFicha(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1,idAsn2CdAssuntoNivel2,idPessCdPessoa, idEmpresa) {
	
	//Chamado: 84517 - 25/09/2012;
	if(idEmpresa != '' && idEmpresa != '0' && parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value != idEmpresa)
	{
		//Chamado: 86878 - 04/04/2013 - Carlos Nunes
		//parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value = idEmpresa;
		//parent.ifrmCmbEmpresa.empresaClick();

		empresa = idEmpresa;
	}
	else
	{
		empresa = parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
	}
	
	idPessoa = idPessCdPessoa;
	chamado = idChamCdChamado;
	manifestacao = maniNrSequencia;
	tpManifestacao = idTpmaCdTpManifestacao;
	assuntoNivel = idAsnCdAssuntoNivel;
	assuntoNivel1 = idAsn1CdAssuntoNivel1;
	assuntoNivel2 = idAsn2CdAssuntoNivel2;
	//empresa = idEmpresa; //Chamado: 86878 - 04/04/2013 - Carlos Nunes
	
	ifrmVerificacao.location = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=ifrmVerificaTrava&acao=<%=Constantes.ACAO_VERIFICAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=' + empresa + '&origem=workflow';
	
}


function mudaManifestacao() {
	janelaManif = window.dialogArguments;
	if (window.dialogArguments.top.esquerdo.ifrm0.dataInicio.value == "")
		window.dialogArguments.top.esquerdo.ifrm0.dataInicio.value = "01/01/2000 00:00:00";
	window.dialogArguments.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value = idPessoa;
	janelaManif = janelaManif.top.principal.manifestacao;
	janelaPessoa = janelaManif.top.principal.pessoa;
	
	janelaManif.parent.parent.superior.AtivarPasta('MANIFESTACAO');
	preencheManifestacao();
}

function preencheManifestacao() {
	if (janelaManif.document.readyState != 'complete')
		a = setTimeout ("preencheManifestacao()",100);
	else {
		try {
			clearTimeout(a);
		} catch(e) {}

		if(janelaManif.submeteConsultar(chamado, manifestacao, tpManifestacao, assuntoNivel)){
			janelaPessoa.dadosPessoa.abrirCont(idPessoa, 'localAtend');
			window.close();
		}
	}
}


function boxSelecao(valor) {
	if(lstLocalizadorAtend['localizadorAtendimentoVo.destCheck'] != null) {
		for (i=0;i<lstLocalizadorAtend['localizadorAtendimentoVo.destCheck'].length;i++) {
				  lstLocalizadorAtend['localizadorAtendimentoVo.destCheck'][i].checked = valor;
		}  
	}
	else {
		alert("<bean:message key='prompt.A_lista_esta_vazia' />");
	}	
}

var nCout = 0;
var nManif = 0;
var nFollowup = 0;
var nEmAtraso = 0;

var nCoutConcluido = 0;
var nManifConcluido = 0;
var nFollowupConcluido = 0;
var nEmAtrasoConcluido = 0;

function alteraModal(){
	parent.bModal = true;
}

function alteraModalManifestacao(){
	parent.bModalManifestacao = true;
}


function consultaManifestacao() {
	<%if(CONF_FICHA_NOVA){%>
		var url = '/csicrm/FichaManifestacao.do?idChamCdChamado='+ chamado +
		'&maniNrSequencia='+ manifestacao +
		'&idTpmaCdTpManifestacao='+ tpManifestacao +
		'&idAsnCdAssuntoNivel='+ assuntoNivel1 + "@" + assuntoNivel2 +
		'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
		'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
		'&idPessCdPessoa='+ idPessoa +
		'&idEmprCdEmpresa='+ empresa +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csiworkflow';
				
		showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}else{%>
	
		var url = '<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado='+ chamado +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia='+ manifestacao +
			'&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao='+ tpManifestacao +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel='+ assuntoNivel +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
			'&idPessCdPessoa='+ idPessoa +
			'&modulo=workflow';
		showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
		
	<%}%>
}


function alteraDestinatario(){
	lstLocalizadorAtend.tela.value = '<%=MCConstantes.TELA_CMB_LOCAL_DESTINATARIO%>';
	lstLocalizadorAtend.acao.value = '<%=Constantes.ACAO_EDITAR%>';
	lstLocalizadorAtend.submit();
}

function alteraManifestacao(){
	lstLocalizadorAtend.tela.value = '<%=MCConstantes.TELA_MANIFESTACAO_CONCLUSAO%>';
	lstLocalizadorAtend.acao.value = '<%=Constantes.ACAO_EDITAR%>';
	lstLocalizadorAtend.submit();
}

function enviaResposta(){
	lstLocalizadorAtend.tela.value = '<%=MCConstantes.TELA_MANIFESTACAO_RESPOSTA%>';
	lstLocalizadorAtend.acao.value = '<%=Constantes.ACAO_EDITAR%>';
	lstLocalizadorAtend.submit();
}

function inicio(){
    //parent.sorttable.init();
    
	showError("<%=request.getAttribute("msgerro")%>");
	if(parent.transferenciaDestinatario){
		parent.transferenciaDestinatario = false;
		parent.carregaListaAtend();
		parent.parent.document.getElementById("aguarde").style.visibility = "visible";
	}
	else{
		parent.parent.document.getElementById("aguarde").style.visibility = "hidden";
	}
}

var nCountIniciaTela = 0;

function iniciaTela(){
	try{
		parent.setPaginacao(<%=regDe%>,<%=regAte%>);
		parent.atualizaPaginacao(<%=numRegTotal%>);		
	}catch(e){
		if(nCountIniciaTela < 5){
			setTimeout('iniciaTela()',300);
			nCountIniciaTela++;
		}
	}

	if(parent.statusLista == "min"){
		document.getElementById("lstRegistro").style.height = "405px";
	}else{
		document.getElementById("lstRegistro").style.height = "166px";
	}
	
	parent.document.getElementById("tituloLocalizadorAtend").innerHTML = parent.document.getElementById("tituloLocalizadorAtendEscondido").innerHTML;
	parent.sorttable.makeSortable(parent.document.getElementById("tabelaTitulo"));

	//jvarandas - Comentado pois n�o deve reordenar a lista no iniciar da tela. (Chamado 69824)
	//setTimeout('parent.document.getElementById("classificacao").click()',100);
	//setTimeout('parent.document.getElementById("classificacao").click()',200);

	//Chamado: 91288 - 09/10/2013 - Carlos Nunes
	parent.document.getElementById('btnFiltro').disabled=false;
	parent.document.getElementById('btnFiltro').className = 'geralCursoHand';
	
	parent.$('#btnExportar').attr('disabled',false);
 	parent.$('#spnExportar').attr('disabled',false);	
 	
 	//Marco Costa - Chamado: 94790 - 13/05/2014
 	parent.parent.document.all.item("lblTotal").innerHTML = "<bean:message key='prompt.pendentes'/>: <%=numCount%>";
	parent.parent.document.all.item("lblTotal").className = 'principalResultList';
	parent.parent.document.all.item("lblManif").innerHTML = "<bean:message key="prompt.manif" />: <%=numManif%>";
	parent.parent.document.all.item("lblManif").className = 'principalResultList';
	parent.parent.document.all.item("lblFollowup").innerHTML = "<bean:message key="prompt.followup" />: <%=numFollowup%>";
	parent.parent.document.all.item("lblFollowup").className = 'principalResultList';
	parent.parent.document.all.item("lblEmAtraso").innerHTML = "<bean:message key='prompt.EmAtraso'/>: <%=numEmAtraso%>";
	parent.parent.document.all.item("lblEmAtraso").className = 'principalResultList';

	//Marco Costa - Chamado: 94790 - 13/05/2014
	parent.parent.document.all.item("lblTotalConcluido").innerHTML = "<bean:message key='prompt.Concluido'/>: <%=numCoutConcluido%>";
	parent.parent.document.all.item("lblTotalConcluido").className = 'principalResultListAzul';
	parent.parent.document.all.item("lblManifConcluido").innerHTML = "<bean:message key="prompt.manif" />: <%=numManifConcluido%>" ;
	parent.parent.document.all.item("lblManifConcluido").className = 'principalResultListAzul';
	parent.parent.document.all.item("lblFollowupConcluido").innerHTML = "<bean:message key="prompt.followup" />: <%=numFollowupConcluido%>";
	parent.parent.document.all.item("lblFollowupConcluido").className = 'principalResultListAzul';
	parent.parent.document.all.item("lblEmAtrasoConcluido").innerHTML = "<bean:message key='prompt.EmAtraso'/>: <%=numEmAtrasoConcluido%>";
	parent.parent.document.all.item("lblEmAtrasoConcluido").className = 'principalResultListAzul';
 	
}

//-->
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="iniciaTela();inicio();" topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0>
<html:form action="/LocalizadorAtendimento.do" styleId="lstLocalizadorAtend" >

<input type="hidden" name="localizadorAtendimentoVo.regDe" />
<input type="hidden" name="localizadorAtendimentoVo.regAte" />

<input type="hidden" name="idEmprCdEmpresa" value="0" />
<input type="hidden" name="idEmprCdEmpresaMesa" value="0" />
	
<table id="tabelaTitulos" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr valign="top"> 
    <td height="100%" colspan="9">
    
    	<!--  
    	Valdeci - 26-05-2006
    	Chamado: 19506 , Item 4
    	Foi feito altera��o, pois ao mandar imprimir a lista de atendimentos n�o saia todo o conteudo devido
    	ao DIV. a tabela de titulos abaixo fica escondida por baixo do titulo da tela de cima
    	-->
    	
     <!-- Danilo Prevides - 23/09/2009 - 66422 - INI 
     	Quando o bot�o de impress�o � clicado, o titulo(header) do ifrmLocalizadorAtend 
     	fica invisivel, e esse div � ativado.
     	Esse div deve ser identico ao ifrmLocalizadorAtendimento.tituloLocalizadorAtend
     -->
     <!-- Chamado: 91506 - 24/10/2013 - Carlos Nunes -->
	 <div id="divTitulo" style='<bean:write name="configuracoesGerais" property="field(div_titulo_style)" />'> 
   	 <table width='<bean:write name="configuracoesGerais" property="field(table_titulo_width)" />' border="0" cellspacing="0" cellpadding="0" class="sortable" style="cursor: pointer;">
		<tr>
			<td class="principalLstCab" width="65px"><img src="webFiles/images/icones/bt_selecionar_nao.gif" onclick="lstIndicacoes.boxSelecao(false);" title="<bean:message key='prompt.desmarcar'/>" class="geralCursoHand"><img src="webFiles/images/icones/bt_selecionar_sim.gif" onclick="lstIndicacoes.boxSelecao(true);" title="<bean:message key='prompt.marcar'/>" class="geralCursoHand"></td>															
			
			<logic:present name="colunaList">
                <logic:iterate id="coluna" name="colunaList">
                   <td class='<bean:write name="coluna" property="field(css)" />' width='<bean:write name="coluna" property="field(width)" />'><bean:write name="coluna" property="field(label)" /></td>
                </logic:iterate>
             </logic:present>
             
			<td id="classificacao" class="principalLstCab" width="0px">&nbsp;</td>
		</tr>
	</table> 
	</div>
     <!-- Danilo Prevides - 23/09/2009 - 66422 - FIM -->
	
	<!-- Danilo Prevides - 23/09/2009 - 66422 - INI  -->
	<!-- <div id="lstRegistro" style="overflow: auto; height: 165px; width: 970px; position: absolute" onScroll="parent.tituloLocalizadorAtend.scrollLeft=this.scrollLeft;"> -->
	<!-- Chamado: 91506 - 24/10/2013 - Carlos Nunes -->
	<div id="lstRegistro" style='<bean:write name="configuracoesGerais" property="field(lst_registro_style)" />' onScroll="parent.document.getElementById('tituloLocalizadorAtend').scrollLeft=this.scrollLeft;">
	<!-- Danilo Prevides - 23/09/2009 - 66422 - FIM  -->
 	<table width='<bean:write name="configuracoesGerais" property="field(table_lst_registro_width)" />' border="0" cellspacing="0" cellpadding="0" class="sortable">       
	<!--  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="sortable">  -->
		  
		  <logic:present name="valoresList">
	   		<logic:iterate id="valoresPrincipal" name="valoresList">
			  <tr>
			  	<% // Chamado 96034 - 15/08/2014 - Daniel Gon�alves  %>
			  	<!-- Atendimento em atraso -->
			    <logic:equal name="valoresPrincipal" property="field(in_em_atraso)" value="true">
					
				
					<script>
			    		if ('<bean:write name="valoresPrincipal" property="field(mani_dh_encerramento)"/>' == ""){
			    			nCout++;
			    			nEmAtraso++;
	
				    		if (trim('<bean:write name="valoresPrincipal" property="field(matp_ds_maniftipo)"/>') == 'FOLLOW - UP'){
							    nFollowup++;		
				    		}else{
								nManif++;	    		
				    		}
			    		}else{
			    			nCoutConcluido++;
			    			//nEmAtrasoConcluido++;
	
				    		if (trim('<bean:write name="valoresPrincipal" property="field(matp_ds_maniftipo)"/>') == 'FOLLOW - UP'){
							    nFollowupConcluido++;
				    		}else{
								nManifConcluido++;
				    		}
			    		}
	
					</script>
				</logic:equal>
				<logic:notEqual name="valoresPrincipal" property="field(in_em_atraso)" value="true">
		        	<script>
			    		if ('<bean:write name="valoresPrincipal" property="field(mani_dh_encerramento)"/>' == ""){
			    			nCout++;
			    			
				    		if (trim('<bean:write name="valoresPrincipal" property="field(matp_ds_maniftipo)"/>') == 'FOLLOW - UP'){
							    nFollowup++;		
				    		}else{
								nManif++;	    		
				    		}
			    		}else{
			    			nCoutConcluido++;

				    		if (trim('<bean:write name="valoresPrincipal" property="field(matp_ds_maniftipo)"/>') == 'FOLLOW - UP'){
							    nFollowupConcluido++;
				    		}else{
								nManifConcluido++;
				    		}
			    		}
	
					</script>
		        </logic:notEqual>
				
				
				<td style="color: <bean:write name='valoresPrincipal' property='field(cor)'/>;" class="<bean:write name='valoresPrincipal' property='field(css)'/>" width="68px">
					<input type="checkbox" name="localizadorAtendimentoVo.destCheck" idEtprCdEtapaprocesso="<bean:write name='valoresPrincipal' property='field(id_etpr_cd_etapaprocesso)'/>" value="<bean:write name='valoresPrincipal' property='field(id_cham_cd_chamado)'/>@<bean:write name='valoresPrincipal' property='field(mani_nr_sequencia)'/>@<bean:write name="valoresPrincipal" property="field(id_asn_cd_assuntonivel)"/>@<bean:write name="valoresPrincipal" property="field(foup_nr_sequencia)"/>@<bean:write name="valoresPrincipal" property="field(mani_dh_encerramento)"/>">
					<img id="lupa" src="webFiles/images/botoes/lupa.gif" width="15" height="15" title="<bean:message key='prompt.consultaManifestacao'/>" onClick="verificaRegistroFicha(<bean:write name='valoresPrincipal' property='field(id_cham_cd_chamado)'/>, <bean:write name='valoresPrincipal' property='field(mani_nr_sequencia)'/>, <bean:write name='valoresPrincipal' property='field(id_tpma_cd_tpmanifestacao)'/>, '<bean:write name="valoresPrincipal" property="field(id_asn_cd_assuntonivel)"/>','<bean:write name="valoresPrincipal" property="field(id_asn1_cd_assuntonivel1)"/>','<bean:write name="valoresPrincipal" property="field(id_asn2_cd_assuntonivel2)"/>','<bean:write name='valoresPrincipal' property='field(id_pess_cd_pessoa)'/>',<bean:write name='valoresPrincipal' property='field(id_empr_cd_empresa)'/>)" class="geralCursoHand">
					<script>bExisteReg = true;</script>
					
					<logic:equal name="valoresPrincipal" property="field(in_em_atraso)" value="true">
						<logic:equal name="valoresPrincipal" property="field(matp_ds_maniftipo)" value="FOLLOW - UP">
							<img src="webFiles/images/icones/historico.gif" width="20" title="<bean:message key='prompt.followUpEmAtraso'/>" onclick="verificaRegistro(<bean:write name='valoresPrincipal' property='field(id_pess_cd_pessoa)'/>,<bean:write name='valoresPrincipal' property='field(id_cham_cd_chamado)'/>,<bean:write name='valoresPrincipal' property='field(mani_nr_sequencia)'/>,<bean:write name='valoresPrincipal' property='field(id_tpma_cd_tpmanifestacao)'/>,'<bean:write name="valoresPrincipal" property="field(id_asn_cd_assuntonivel)"/>', '<bean:write name="valoresPrincipal" property="field(foup_nr_sequencia)"/>', '<bean:write name="valoresPrincipal" property="field(id_util_cd_funcionario)"/>', '<bean:write name="valoresPrincipal" property="field(id_tppu_cd_tipopublico)"/>', '<bean:write name="valoresPrincipal" property="field(id_matp_cd_maniftipo)"/>', '<bean:write name="valoresPrincipal" property="field(id_linh_cd_linha)"/>', '<bean:write name="valoresPrincipal" property="field(id_grma_cd_grupomanifestacao)"/>', '<bean:write name="valoresPrincipal" property="field(mani_dh_encerramento)"/>',<bean:write name='valoresPrincipal' property='field(id_empr_cd_empresa)'/>)" class="geralCursoHand">
						</logic:equal>
						<logic:notEqual name="valoresPrincipal" property="field(matp_ds_maniftipo)" value="FOLLOW - UP">
							<img src="webFiles/images/icones/historico.gif" width="20" title="<bean:message key='prompt.manifestacaoEmAtraso'/>" onclick="verificaRegistro(<bean:write name='valoresPrincipal' property='field(id_pess_cd_pessoa)'/>,<bean:write name='valoresPrincipal' property='field(id_cham_cd_chamado)'/>,<bean:write name='valoresPrincipal' property='field(mani_nr_sequencia)'/>,<bean:write name='valoresPrincipal' property='field(id_tpma_cd_tpmanifestacao)'/>,'<bean:write name="valoresPrincipal" property="field(id_asn_cd_assuntonivel)"/>', '<bean:write name="valoresPrincipal" property="field(foup_nr_sequencia)"/>', '<bean:write name="valoresPrincipal" property="field(id_util_cd_funcionario)"/>', '<bean:write name="valoresPrincipal" property="field(id_tppu_cd_tipopublico)"/>', '<bean:write name="valoresPrincipal" property="field(id_matp_cd_maniftipo)"/>', '<bean:write name="valoresPrincipal" property="field(id_linh_cd_linha)"/>', '<bean:write name="valoresPrincipal" property="field(id_grma_cd_grupomanifestacao)"/>', '<bean:write name="valoresPrincipal" property="field(mani_dh_encerramento)"/>',<bean:write name='valoresPrincipal' property='field(id_empr_cd_empresa)'/>)" class="geralCursoHand">
						</logic:notEqual>
					</logic:equal>
				</td>
			
			    <bean:define id="listaValores" name="valoresPrincipal" property="field(valoreslistaux)" />
			    <logic:iterate id="valores" name="listaValores">
			      <td <bean:write name="valores" property="field(customkey)" /> class='<bean:write name="valores" property="field(css)" />' style="<bean:write name='valores' property='field(style)'/>;" align="<bean:write name='valores' property='field(align)'/>" 
			          width='<bean:write name="valores" property="field(width)" />' onclick="verificaRegistro(<bean:write name='valoresPrincipal' property='field(id_pess_cd_pessoa)'/>,
			                                                                                                  <bean:write name='valoresPrincipal' property='field(id_cham_cd_chamado)'/>,
			                                                                                                  <bean:write name='valoresPrincipal' property='field(mani_nr_sequencia)'/>,
			                                                                                                  <bean:write name='valoresPrincipal' property='field(id_tpma_cd_tpmanifestacao)'/>,
			                                                                                                  '<bean:write name="valoresPrincipal" property="field(id_asn_cd_assuntonivel)"/>', 
			                                                                                                  '<bean:write name="valoresPrincipal" property="field(foup_nr_sequencia)"/>', 
			                                                                                                  '<bean:write name="valoresPrincipal" property="field(id_util_cd_funcionario)"/>', 
			                                                                                                  '<bean:write name="valoresPrincipal" property="field(id_tppu_cd_tipopublico)"/>', 
			                                                                                                  '<bean:write name="valoresPrincipal" property="field(id_matp_cd_maniftipo)"/>', 
			                                                                                                  '<bean:write name="valoresPrincipal" property="field(id_linh_cd_linha)"/>', 
			                                                                                                  '<bean:write name="valoresPrincipal" property="field(id_grma_cd_grupomanifestacao)"/>', 
			                                                                                                  '<bean:write name="valoresPrincipal" property="field(mani_dh_encerramento)"/>',
			                                                                                                  <bean:write name='valoresPrincipal' property='field(id_empr_cd_empresa)'/>)">
			         <span class="geralCursoHand"><bean:write name="valores" property="field(value)" filter="html" /></span>
			      </td>
			    </logic:iterate>
		        
		        
				<td style="visibility:hidden; color: <bean:write name='valoresPrincipal' property='field(cor)'/>;" class='<bean:write name="valores" property="field(css)" />' width="0px" ><span class="geralCursoHand"><bean:write name="valoresPrincipal" property="field(clma_nr_sequencia)"/>&nbsp;</span></td>
			</logic:iterate>
			</tr>
		</logic:present>
		
		<script>
		  if (bExisteReg == false && parent.document.getElementById("localizadorAtendimento").existeRegistroLista.value == 'false'){
		  	document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="200" ><b>Nenhum registro encontrado.</b></td></tr>');
		  }
		</script>	
			
			<!-- Atendimento no prazo  -->
		  
        </table>
       </div>	
    </td>
  </tr>
</table>
<html:hidden property="localizadorAtendimentoVo.idMatpCdManifTipo"/>
<html:hidden property="localizadorAtendimentoVo.idGrmaCdGrupoManifestacao"/>
<html:hidden property="localizadorAtendimentoVo.idTpmaCdTpManifestacao"/>
<html:hidden property="localizadorAtendimentoVo.idLinhCdLinha"/>
<html:hidden property="localizadorAtendimentoVo.idAsn1CdAssuntoNivel1"/>
<html:hidden property="localizadorAtendimentoVo.idAsn2CdAssuntoNivel2"/>
<html:hidden property="localizadorAtendimentoVo.chamDhInicial"/>
<html:hidden property="localizadorAtendimentoVo.chamDhFinal"/>
<html:hidden property="localizadorAtendimentoVo.idAreaCdDestinatario"/>
<html:hidden property="localizadorAtendimentoVo.idFuncCdDestinatario"/>
<html:hidden property="localizadorAtendimentoVo.idFuncCdFuncionario"/>
<html:hidden property="localizadorAtendimentoVo.pessNmPessoa"/>
<html:hidden property="localizadorAtendimentoVo.idChamCdChamado"/>
<html:hidden property="localizadorAtendimentoVo.maniInGrave"/>
<html:hidden property="localizadorAtendimentoVo.idStmaCdStatusmanif"/>
<html:hidden property="localizadorAtendimentoVo.idClmaCdClassifmanif"/>
<html:hidden property="localizadorAtendimentoVo.idEvfuCdEventoFollowUp"/>
<html:hidden property="localizadorAtendimentoVo.idPendencia"/>
<!--html:hidden property="localizadorAtendimentoVo.csNgtbManiftiaedaMateVo.idPediCdPedido"/-->
<!--html:hidden property="localizadorAtendimentoVo.csNgtbManiftiaedaMateVo.tbOrStoStoreVo.stoKey"/-->
<html:hidden property="csCdtbAreaAreaIIVo.idAreaCdArea"/>
<html:hidden property="csCdtbFuncionarioFuncIIVo.idFuncCdFuncionario"/>
<html:hidden property="localizadorAtendimentoVo.idSugrCdSupergrupo"/>
<html:hidden property="maniTxResposta"/>
<html:hidden property="corrTxCorrespondencia"/>
<html:hidden property="csCdtbDocumentoDocuVo.idDocuCdDocumento" />
<html:hidden property="inConcluir"/>
<html:hidden property="corrDsTitulo"/>
<html:hidden property="corrDsEmailDe"/>
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="localizadorAtendimentoVo.porcentagemCheckListDe"/>
<html:hidden property="localizadorAtendimentoVo.porcentagemCheckListAte"/>
<html:hidden property="localizadorAtendimentoVo.periodo"/>
<html:hidden property="localizadorAtendimentoVo.codStatusPendencia"/>
<html:hidden property="localizadorAtendimentoVo.idDeprCdDesenhoprocesso"/>
<html:hidden property="localizadorAtendimentoVo.idEtprCdEtapaprocesso"/>

<iframe name="ifrmVerificacao" id="ifrmVerificacao" src="" width="0" height="0" frameborder=0></iframe>

<!-- Esse div nao pode ser removido pois o mesmo se refere ao campos que ser�o inclu�dos de acordo com 
	o projeto especifico
-->
<div id="camposEspec" name="camposEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden"></div>




</html:form>
</body>
</html>