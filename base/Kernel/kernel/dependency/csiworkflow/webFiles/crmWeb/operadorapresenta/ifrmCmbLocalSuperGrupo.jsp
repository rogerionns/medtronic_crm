<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
	function carregaGrupoManif()
	{
		try{
			var nidSuperGrupo;
			var nidManifTipo;
	
			nidSuperGrupo = ifrmCmbLocalSuperGrupo['csCdtbSupergrupoSugrVo.idSugrCdSupergrupo'].value;
			nidManifTipo = parent.ifrmCmbLocalManifTipo.document.getElementById("ifrmCmbLocalManifTipo")['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
	
	/*		if (nidSuperGrupo != 0)
			{*/
				parent.ifrmCmbLocalGrupoManif.location.href = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbSupergrupoSugrVo.idSugrCdSupergrupo=" + nidSuperGrupo +"&csCdtbManifTipoMatpVo.idMatpCdManifTipo="+ nidManifTipo +"&idEmprCdEmpresa="+ parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;;
	/*		}else{
				window.parent.ifrmCmbLocalGrupoManif.location.href = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif&idEmprCdEmpresa="+ parent.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
			}*/
	
	//		window.parent.ifrmCmbLocalTipoManif.location.href = "LocalizadorAtendimento.do?tela=cmbLocalTipoManif";		
		}catch(e){}
		
	}
	
	function getValorCombo(){
		return ifrmCmbLocalSuperGrupo['csCdtbSupergrupoSugrVo.idSugrCdSupergrupo'].value;
	}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaGrupoManif();" style="overflow: hidden;">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmCmbLocalSuperGrupo">
  <html:select property="csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" onchange="carregaGrupoManif()" styleClass="principalObjForm">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
  	<logic:present name="superGrupoVector">
  		<html:options collection="superGrupoVector" property="idSugrCdSupergrupo" labelProperty="sugrDsSupergrupo" />  
  	</logic:present>
  </html:select>

</html:form>
</body>
</html>