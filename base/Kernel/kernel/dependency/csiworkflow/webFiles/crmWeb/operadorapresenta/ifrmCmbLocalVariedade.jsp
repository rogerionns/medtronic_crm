<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.crm.form.LocalizadorAtendimentoForm, br.com.plusoft.csi.crm.vo.LocalizadorAtendimentoVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

if (session != null && session.getAttribute("locaAtendimentoVo") != null) {
	LocalizadorAtendimentoVo newVo = LocalizadorAtendimentoVo.getInstance(empresaVo.getIdEmprCdEmpresa());
	newVo = ((LocalizadorAtendimentoVo)session.getAttribute("locaAtendimentoVo"));
	
	((LocalizadorAtendimentoForm)request.getAttribute("localizadorAtendimentoForm")).getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().setIdAsn2CdAssuntoNivel2(((LocalizadorAtendimentoForm)request.getAttribute("localizadorAtendimentoForm")).getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2());
	
	
	newVo = null;
}

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script>
	function getValorCombo(){
		return document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	}
	
	function concatena() {
		var idAsn1CdAssuntoNivel1;
		var idAsn2CdAssuntoNivel2;
		
		idAsn1CdAssuntoNivel1 = parent.ifrmCmbLocalProduto.document.getElementById("ifrmCmbLocalProduto")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		idAsn2CdAssuntoNivel2 = document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		
		var index = idAsn1CdAssuntoNivel1.indexOf("@");
	
		var concatena = idAsn1CdAssuntoNivel1.substring(0,index) + "@" + idAsn2CdAssuntoNivel2;
		
		return concatena;
	}
	
	var nCountCarregaManifTipo = 0;
	
	function carregaManifTipo()
	{
		try{
			
		//var nidAsn = window.parent.ifrmCmbLocalProduto.ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		var nidAsn = verificaSeComboLocalProdutoJaCarregou();
		var nidAsn2 = window.document.ifrmCmbLocalVariedade['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO2") || Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO3") || Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO4")) {%>
		
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TIPOLINHA,request).equals("S")) {%>
				var cLinkMATP = '';
				if (window.parent.ifrmCmbLocalManifTipo.ifrmCmbLocalManifTipo!=undefined) { 
					if (window.parent.ifrmCmbLocalManifTipo.ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo']!=undefined) { 
						if ((window.parent.ifrmCmbLocalManifTipo.ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value!=0) &&
							(window.parent.ifrmCmbLocalManifTipo.ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value!='')) {
							cLinkMATP = '&csCdtbManifTipoMatpVo.idMatpCdManifTipo=' + window.parent.ifrmCmbLocalManifTipo.ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value; 
						}
					}
				}
				if (nidAsn2 != 0)
				{
					window.parent.ifrmCmbLocalManifTipo.location.href = "LocalizadorAtendimento.do?tela=cmbLocalManifTipo&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel="+ nidAsn + "&csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + nidAsn2 +"&idEmprCdEmpresa="+ parent.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value + cLinkMATP;
				}else{
					window.parent.ifrmCmbLocalManifTipo.location.href = "LocalizadorAtendimento.do?tela=cmbLocalManifTipo&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresa="+ parent.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value + cLinkMATP;
				}
			<%}%>
			
		<%}%>
		
		}catch(e){
			
			if(nCountCarregaManifTipo < 10){
				nCountCarregaManifTipo++;
				//Chamado 76577 - Vinicius - Comentado para que n�o seja mais vinculado a composi��o do produto com a de manifesta��o
				//setTimeout('carregaManifTipo()',1000);
			}
		
		}
	}
	
	function verificaSeComboLocalProdutoJaCarregou() {
		if(window.parent.ifrmCmbLocalProduto.document.readyState == 'complete') {
			return window.parent.ifrmCmbLocalProduto.document.forms[0]['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		} else {
			setTimeout('verificaSeComboLocalProdutoJaCarregou()', 200);
		}
	}
	
	function carreganoOnLoad(){
			
		//if(parent.ifrmCmbLocalLinha.visualizador == true){
			if ( document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].length == 2 ){
				document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].options[1].selected = true;	
			}
		//	parent.ifrmCmbLocalLinha.visualizador = false;
		//}
				
		<%	if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO2") || Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO3") || Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO4")) {	%>
			//Chamado 76577 - Vinicius - Comentado para que n�o seja mais vinculado a composi��o do produto com a de manifesta��o
			//carregaManifTipo();
		<% } %>
	}
	
	function mostraCampoBuscaProd(){
		cTela = document.localizadorAtendimentoForm.tela.value;
		cAcao = document.localizadorAtendimentoForm.acao.value;
		document.localizadorAtendimentoForm.acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
		document.localizadorAtendimentoForm.tela.value = "<%= MCConstantes.TELA_CMB_LOCAL_VARIEDADE %>";
		document.localizadorAtendimentoForm.target = this.name;
		document.localizadorAtendimentoForm.submit();
	}
	
	function buscarProduto(){

		if (document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}

		cTela = document.localizadorAtendimentoForm.tela.value;
		cAcao = document.localizadorAtendimentoForm.acao.value;
		document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = 0;
		document.localizadorAtendimentoForm.tela.value = "<%= MCConstantes.TELA_CMB_LOCAL_VARIEDADE %>";
		document.localizadorAtendimentoForm.acao.value = "<%= Constantes.ACAO_VISUALIZAR %>";
		document.localizadorAtendimentoForm.target = this.name;
		document.localizadorAtendimentoForm.submit();

	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="carreganoOnLoad()" leftmargin=0 topmargin=0 rightmargin=0 bottommargin=0 style="overflow: hidden;">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmCmbLocalVariedade">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="csCdtbLinhaLinhVo.idLinhCdLinha"/>
	<html:hidden property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>
	<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>

	<logic:notEqual name="baseForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
		<table width="95%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="95%">
					<html:select property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" styleClass="principalObjForm" onchange="carreganoOnLoad();">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
  		<html:options collection="variedadeVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2" />  
  </html:select>  
			 	</td>
			 	<td width="5%" valign="middle">
			 		<div align="right" id="divBotao"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
			 	</td>
			</tr>
		</table>
	</logic:notEqual>
	  
	<logic:equal name="baseForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
		<table width="95%" border="0" cellspacing="0" cellpadding="0">	   	  
			<tr>
				<td width="95%">
					<html:text property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2" styleClass="principalObjForm" onkeydown="pressEnter(event)" /><br>
				</td>
				<td width="5%" valign="middle">
					<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
				</td>
			</tr> 	
		</table>
		<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
	</logic:equal>
</html:form>
</body>
</html>
<logic:equal name="baseForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	<script>
		document.localizadorAtendimentoForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2'].focus();
	</script>
</logic:equal>
