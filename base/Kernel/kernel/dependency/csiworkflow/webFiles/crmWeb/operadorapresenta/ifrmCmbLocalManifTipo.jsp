<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

	var nCountCarregaGrupoManif = 0;

	function carregaGrupoManif()
	{
		try{
			if(ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value == -1){
//Chamado 100157 KERNEL-1098 QA - 20/04/2015 - Marcos Donato //
//				parent.document.getElementById("divEventoFollowup").style.visibility="visible";
				parent.document.getElementById("labelManifestacao").style.display = 'none';
				parent.document.getElementById("ifrmCmbLocalGrupoManif").style.display = 'none';
				parent.document.getElementById("labelEvento").style.display = 'inline';
				parent.document.getElementById("ifrmCmbLocalEventoFollowup").style.display = 'inline';
			}else{
//Chamado 100157 KERNEL-1098 QA - 20/04/2015 - Marcos Donato //
//				parent.document.getElementById("divEventoFollowup").style.visibility="hidden";
				parent.document.getElementById("labelManifestacao").style.display = 'inline';
				parent.document.getElementById("ifrmCmbLocalGrupoManif").style.display = 'inline';
				parent.document.getElementById("labelEvento").style.display = 'none';
				parent.document.getElementById("ifrmCmbLocalEventoFollowup").style.display = 'none';
			}
	
			var nidManifTipo;
			nidManifTipo = ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;

			var nidAsn = 0;
			var nidAsn2 = 0;
			
			<%if (!Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO1")) {	%>
			//Chamado 76577 - Vinicius - Comentado para que n�o seja mais vinculado a composi��o do produto com a de manifesta��o	
			//nidAsn = window.parent.ifrmCmbLocalProduto.ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
					//Chamado 76577 - Vinicius - Comentado para que n�o seja mais vinculado a composi��o do produto com a de manifesta��o
					//nidAsn2 = window.parent.ifrmCmbLocalVariedade.ifrmCmbLocalVariedade['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
				<%}%>
			<%}%>
		
			if (nidManifTipo != 0)
			{
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
					var url = "LocalizadorAtendimento.do?tela=cmbSuperGrupo&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbManifTipoMatpVo.idMatpCdManifTipo=" + nidManifTipo + "&csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + nidAsn2 + "&idEmprCdEmpresa="+ parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
					if(nidAsn != '0'){
						url +=  "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + nidAsn;
					}
					window.parent.ifrmCmbLocalSuperGrupo.location.href = url;
				<%}else{%>
					var cLinkGRMA = '';
					if ((window.parent.ifrmCmbLocalGrupoManif.ifrmCmbLocalGrupoManif['csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value!=0) &&
						(window.parent.ifrmCmbLocalGrupoManif.ifrmCmbLocalGrupoManif['csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value!='')) {
						cLinkGRMA = '&csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao=' + window.parent.ifrmCmbLocalGrupoManif.ifrmCmbLocalGrupoManif['csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value; 
					}

					var url = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbManifTipoMatpVo.idMatpCdManifTipo=" + nidManifTipo +  "&csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + nidAsn2 + "&idEmprCdEmpresa="+ parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value+cLinkGRMA;
					if(nidAsn != '0'){
						url +=  "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + nidAsn;
					}
					window.parent.ifrmCmbLocalGrupoManif.location.href = url;
				<%}%>
			}else{
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
					var url = "LocalizadorAtendimento.do?tela=cmbSuperGrupo&idEmprCdEmpresa="+ parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
					if(nidAsn != '0'){
						url +=  "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + nidAsn;
					}
					window.parent.ifrmCmbLocalSuperGrupo.location.href = url;
				<%}else{%>
					var url = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif&idEmprCdEmpresa="+ parent.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
					if(nidAsn != '0'){
						url +=  "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + nidAsn;
					}
					window.parent.ifrmCmbLocalGrupoManif.location.href = url;
				<%}%>
			}

		}catch(e){
			if(nCountCarregaGrupoManif < 5){
				nCountCarregaGrupoManif++;
				setTimeout("carregaGrupoManif()",1000);
			}
		}

	}
	
	function getValorCombo(){
		return ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
	}
	
	function carreganoOnLoad(){
		//if(window.document.ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value > 0){
			carregaGrupoManif();
		//}
	}
	
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="carreganoOnLoad();showError('<%=request.getAttribute("msgerro")%>')" style="overflow: hidden;">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmCmbLocalManifTipo">
   <html:select property="csCdtbManifTipoMatpVo.idMatpCdManifTipo" onchange="parent.carregaCamposComboPeriodo();carregaGrupoManif();" styleClass="principalObjForm">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
  	<logic:present name="manifTipoVector">
  		<html:options collection="manifTipoVector" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo" />  
  	</logic:present>
	<html:option value="-1"><bean:message key="prompt.followupUP" /></html:option>  	
  </html:select>
</html:form>
</body>
</html>