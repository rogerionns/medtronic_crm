<%@ page language="java" import="br.com.plusoft.csi.adm.util.*, br.com.plusoft.csi.crm.helper.*, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<title>ifrmDadosPessoa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

function submeteForm(tela) { //v3.0
  //Desmarca todos os options que estao dentro do iframe especifico
  //Danilo Prevides - 14/12/2009 - 68033 - INI
	try {
	 		ifrmHistoricoEspec.desmarcaTodos();
	}catch (e){};  
  //Danilo Prevides - 14/12/2009 - 68033 - FIM
  //eval("lstHistorico.location='Historico.do?acao=consultar&tela=" + tela + "&idPessCdPessoa=" + historicoForm.idPessCdPessoa.value + "&consDsCodigoMedico=" + historicoForm.consDsCodigoMedico.value + "'");

  if(tela != 'chat'){
	  	eval("lstHistorico.location='Historico.do?acao=consultar&tela=" + tela + "&idPessCdPessoa=" + historicoForm.idPessCdPessoa.value + "&consDsCodigoMedico=" + historicoForm.consDsCodigoMedico.value + "'");
	}else{
		eval("lstHistorico.location='../ChatWEB/AbrirListaHistoricoChat.do?idPessCdPessoa=" + historicoForm.idPessCdPessoa.value + "&idIdioCdIdioma=" + <%=funcVo.getIdIdioCdIdioma()%> + "&idEmprCdEmpresa=" + <%=empresaVo.getIdEmprCdEmpresa()%> + "&strModulo=workflow&idFuncCdFuncionario=" + <%=funcVo.getIdFuncCdFuncionario()%> + "'");
	}
	
}

function desmarcaTodos(){
	for (x = 0;  x < historicoForm.elements.length;  x++) {
		Campo = historicoForm.elements[x];
		if  (Campo.type == "radio"){
			Campo.checked = false;
		}
	}
}

parent.divAguarde.style.visibility = "hidden";

</script>
</head>

<html:form action="Historico.do" styleId="historicoForm">
	<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
	  <input type="hidden" name="idPessCdPessoa" value='<bean:write name="historicoForm" property="idPessCdPessoa" />' >
	  <input type="hidden" name="consDsCodigoMedico" value='<bean:write name="historicoForm" property="consDsCodigoMedico" />' >  	
	  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" >
		<tr align="left"> 
		  <td class="principalLabelValorFixo" align="left">
		  
			  <table cellpadding=0 cellspacing=0 border=0>
			  	<tr>
					  <td class="principalLabel" align="left" width="100%"> 
			  			<%=getMessage("prompt.filtros",request) %>
					  	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
			  		  </td>
			  	<tr>
			  	<tr>
			  		<td>
			  			&nbsp;
			  		</td>
			  	</tr>
			  </table>
		  </td>
		  <td>
			  <table cellpadding=0 cellspacing=0 border=0 style="width:700px">
				  <tr>
				  <td class="principalLabel" align="left" width="100%"> 
					<input type="radio" name="optTpHistorico" value="maniAnteriores" onClick="submeteForm('manifestacaoAnterior')">
					<%=getMessage("prompt.manifanteriores",request) %>
					<input type="radio" name="optTpHistorico" value="maniPendentes" onClick="submeteForm('manifestacaoPendente')" checked>
					<%=getMessage("prompt.manifpendentes",request) %>
					<input type="radio" name="optTpHistorico" value="informacao" onClick="submeteForm('informacao')">
					<%=getMessage("prompt.informacao",request) %>
					<input type="radio" name="optTpHistorico" value="script" onClick="submeteForm('pesquisa')">
					<%=getMessage("prompt.pesquisa",request) %>
					<input type="radio" name="optTpHistorico" value="carta" onClick="submeteForm('carta')">
					<%=getMessage("prompt.carta",request) %>
					<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {%>
					<input type="radio" name="optTpHistorico" value="marketingRelacionamento" onClick="submeteForm('marketingRelacionamento')">
					<%=getMessage("prompt.mr",request) %>
					<% 		  } %>
					<input type="radio" name="optTpHistorico" value="chat" onClick="submeteForm('chat')">
					<%=getMessage("prompt.historicoChat",request) %>
					</td>
					
				</tr>
				
				<tr>
				
					<td class="principalLabel" align="left"> 
						<iframe name="ifrmHistoricoEspec" 
								src="<%= Geral.getActionProperty("historicoAction",empresaVo.getIdEmprCdEmpresa())%>?acao=<%= Constantes.ACAO_VISUALIZAR%>&tela=INICIAL&modulo=workflow" 
								width="100%" 
								height="20" 
								scrolling="No" 
								frameborder="0" 
								marginwidth="0" 
								marginheight="0" >
						</iframe>
					</td>
					
				</tr>
				
			  </table>
			 </td>
		</tr>
	  </table>
	  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<!-- Inicio do Header Historico -->
		<!-- Final do Header Historico -->
		<tr valign="top"> 
		  <td height="565"colspan="7"> 
			<!--Inicio Iframe Lista Historico -->
			<iframe name="lstHistorico" src="Historico.do?acao=consultar&tela=manifestacaoPendente&idPessCdPessoa=<bean:write name='historicoForm' property='idPessCdPessoa' />" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			<!--Final Iframe Lista Historico -->
		  </td>
		</tr>
	  </table>
	</body>
</html:form>