<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,
                                 br.com.plusoft.csi.adm.util.Geral" %>
                                 
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = ((HistoricoListVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************

%>


<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.crm.vo.HistoricoListVo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">
var chamado = '0';
var manifestacao = '0';
var tpManifestacao = '0';
var assuntoNivel = '0';
var assuntoNivel1 = '0';
var assuntoNivel2 = '0';
var empresa = '0';
var idPessoa = "0";

var wnd = window.top;
if(parent.window.dialogArguments != undefined) {
	wnd = parent.window.dialogArguments.top;
}

function verificaRegistro(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel) {

		chamado = idChamCdChamado;
		manifestacao = maniNrSequencia;
		tpManifestacao = idTpmaCdTpManifestacao;
		assuntoNivel = idAsnCdAssuntoNivel;

		//ifrmRegistro.location = 'LocalizadorAtendimento.do?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_CONSULTAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel


}

//Chamado: 80047
function verificaRegistroFicha(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idEmprCdEmpresa, idPessCdPessoa) {
	chamado = idChamCdChamado;
	manifestacao = maniNrSequencia;
	tpManifestacao = idTpmaCdTpManifestacao;
	assuntoNivel = idAsnCdAssuntoNivel;
	assuntoNivel1 = idAsn1CdAssuntoNivel1;
	assuntoNivel2 = idAsn2CdAssuntoNivel2;
	empresa = idEmprCdEmpresa;
	idPessoa = idPessCdPessoa; 
		
	ifrmRegistro.location = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=ifrmVerificaTrava&acao=<%=Constantes.ACAO_VERIFICAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>';
	
}

function mudaManifestacao(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel) {
	parent.parent.parent.superior.AtivarPasta('MANIFESTACAO');
	preencheManifestacao();
}

function preencheManifestacao() {
	if (parent.parent.parent.principal.manifestacao.document.readyState != 'complete')
		a = setTimeout ("preencheManifestacao()",100);
	else {
		try {
			clearTimeout(a);
		} catch(e) {}
		parent.parent.parent.principal.manifestacao.submeteConsultar(chamado, manifestacao, tpManifestacao, assuntoNivel);
	}
}

function consultaManifestacao() {
	<%if(CONF_FICHA_NOVA){%>
		var url = '/csicrm/FichaManifestacao.do?idChamCdChamado='+ chamado +
		'&maniNrSequencia='+ manifestacao +
		'&idTpmaCdTpManifestacao='+ tpManifestacao +
		'&idAsnCdAssuntoNivel='+ assuntoNivel1 + "@" + assuntoNivel2 +
		'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
		'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
		'&idPessCdPessoa='+ idPessoa +
		'&idEmprCdEmpresa=' + empresa +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csiworkflow';
		
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}else{%>
		showModalDialog('<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + tpManifestacao + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=' + assuntoNivel+ '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + assuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + assuntoNivel2+ '&idPessCdPessoa=' + historicoForm.idPessCdPessoa.value + '&modulo=workflow',window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}%>
}

function consultaManifestacao_OLD(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2) {
	chamado = idChamCdChamado;
	manifestacao = maniNrSequencia;
	tpManifestacao = idTpmaCdTpManifestacao;
	assuntoNivel = idAsnCdAssuntoNivel;
	showModalDialog('<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + tpManifestacao + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=' + assuntoNivel+ '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2+ '&idPessCdPessoa=' + historicoForm.idPessCdPessoa.value,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
}

function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "Historico.do?";
	
	if(parent.historicoForm.optTpHistorico[0].checked){
		url = url + "tela=manifestacaoAnterior";
	}else if(parent.historicoForm.optTpHistorico[1].checked){
		url = url + "tela=manifestacaoPendente";
	}
	
	url = url + "&acao=consultar" ;
	url = url + "&idPessCdPessoa=" + historicoForm.idPessCdPessoa.value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;
	
	window.document.location.href = url;
	
}

function inicio(){

	setPaginacao(<%=regDe%>,<%=regAte%>);
	atualizaPaginacao(<%=numRegTotal%>);
	
}

</script>
</head>

<html:form action="Historico.do" styleId="historicoForm">
<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:hidden property="idPessCdPessoa" />
<table height="100%" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  <tr height="20"> 
    <td class="principalLstCab" width="10%">&nbsp;<%=getMessage("prompt.numatend",request) %></td>
    <td class="principalLstCab" width="11%">&nbsp;<%=getMessage("prompt.dtatend",request) %></td>
    <td class="principalLstCab" width="17%">&nbsp;<%= getMessage("prompt.manifestacao", request)%></td>
    <td class="principalLstCab" width="17%">&nbsp;<%=getMessage("prompt.prodassunto",request) %></td>
    <td class="principalLstCab" width="17%">&nbsp;<%=getMessage("prompt.contato",request) %></td>
    <td class="principalLstCab" width="11%">&nbsp;<%=getMessage("prompt.conclusao",request) %></td>
    <td id="tdAtendente" class="principalLstCab" width="15%">&nbsp;<%=getMessage("prompt.atendente",request) %></td>
    <td class="principalLstCab" width="2%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td colspan="8"> 
      <div id="lstHistorico" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <!--Inicio Lista Historico -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="principalLstParMao" width="10%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;<bean:write name="historicoVector" property="idChamCdChamado" />
            </td>
            <td class="principalLstParMao" width="11%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;<script>acronym('<bean:write name="historicoVector" property="chamDhInicial" />',13);</script>
            </td>
            <td class="principalLstParMao" width="17%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getMatpDsManifTipo(), 15)%>
            </td>
            <td class="principalLstParMao" width="17%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getPrasDsProdutoAssunto(), 15)%>
            </td>
            <td class="principalLstParMao" width="17%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getPessNmPessoa(), 15)%>
            </td>
            <td class="principalLstParMao" width="11%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getManiDhEncerramento(), 15)%>
            </td>
            <td class="principalLstParMao" width="15%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFuncNmFuncionario(), 15)%>
            </td>
            <td width="2%">&nbsp;</td>
          </tr>
          <tr> 
            <td width="10%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="11%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="17%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="17%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="17%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="11%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="15%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
          </tr>
          </logic:iterate>
          </logic:present>
        </table>
		<iframe name="ifrmRegistro" id="ifrmRegistro" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>        
        <!--Final Lista Historico -->
      </div>
    </td>
  </tr>
  
  <tr> 
    <td class="principalLabel" style="height:30px;" colspan="8">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="principalLabel" width="20%">
			    	<%@ include file = "../../../webFiles/includes/funcoesPaginacaoHistorico.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="principalLabel">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>
    </td>
  </tr>
  
</table>
</body>
</html:form>
</html>