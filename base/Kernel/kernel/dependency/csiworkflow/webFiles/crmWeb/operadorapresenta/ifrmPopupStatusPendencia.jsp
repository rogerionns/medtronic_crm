<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script>
	function inicio() {
		var msg = '<%=request.getAttribute("msgerro")%>';
		if(msg != 'Altera��o realizada com sucesso!') {
			showError(msg);
		} else {
			alert(msg);
			window.close();
		}
	}

	function gravar() {
		if(document.historicoForm.idStpdCdStatuspendencia.value == '') {
			alert('<bean:message key="prompt.OCampoStatusPendenciaEObrigatorio" />');
		} else {
			document.historicoForm.acao.value = 'editar';
			document.historicoForm.tela.value = 'ifrmPopupStatusPendencia';
			document.historicoForm.target = this.name = 'popupStatusPendencia';
			document.historicoForm.submit();
		}
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="inicio()" style="overflow: hidden;">
<html:form styleClass="historicoForm" action="/Historico.do">
<html:hidden property="acao"/>
<html:hidden property="tela"/>
<html:hidden property="csNgtbChamadoChamVo.idChamCdChamado"/>
<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"/>
<html:hidden property="csNgtbChamadoChamVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="10">
	<tr>
		<td class="espacoPqn" align="right">&nbsp;</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="100%" class="principalLabel">&nbsp;&nbsp;<bean:message key="prompt.StatusPendencia" /></td>
	</tr>
	<tr>
		<td width="100%">
			<html:select property="idStpdCdStatuspendencia" styleClass="principalObjForm">
       			<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
       			<logic:present name="statusPendenciaVector">
       				<html:options collection="statusPendenciaVector" property="field(id_stpd_cd_statuspendencia)" labelProperty="field(stpd_ds_statuspendencia)"/>
       			</logic:present>
       		</html:select>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="10">
	<tr>
		<td class="espacoPqn" align="right">&nbsp;</td>
	</tr>
</table>	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="100%" align="right">
			<img id="btnOut" src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.gravar'/>" onClick="gravar()" class="geralCursoHand">
			<img id="btnOut" src="webFiles/images/botoes/out.gif" width="20" height="20" border="0" title="<bean:message key='prompt.sair'/>" onClick="javascript:window.close()" class="geralCursoHand">
		</td>
	</tr>
</table>
</html:form>
</body>
</html>