<%@ page import="java.util.ArrayList"%>
<%@ page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@ page import="br.com.plusoft.csi.adm.helper.AdministracaoCsAstbPermissionamentoPetoHelper"%>
<%@ page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@ page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*" %>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo" %>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

long idEmpresa = empresaVo.getIdEmprCdEmpresa();

CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

long idFuncionario = funcVo.getIdFuncCdFuncionario();

String url = Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa());
if(url.indexOf("/") == -1)
	url = "../csicrm/" + url;

boolean podeVisualizar = true;
ArrayList perm = AdministracaoCsAstbPermissionamentoPetoHelper.findPermissao(funcVo.getIdFuncCdFuncionario(), 0L, PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CARTA_VISUALIZA, idEmpresa);
if(perm == null || perm.size() == 0){
	podeVisualizar = false;
}


//VERIFICA A FEATURE PARA HABILITAR O ENVIO DE FAX NO M�DULO DE CORRESPON�NCIA 
String featureHabilitaFax;
String featureRetornoCorresp;
try{
	featureHabilitaFax = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CRM_CORRESPONDENCIA_HABILITA_ENVIO_FAX, request);
}catch(Exception e){ featureHabilitaFax = "N"; }

try{
	featureRetornoCorresp = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_RETORNOCORRESP,request);
}catch(Exception e){ featureRetornoCorresp = "N";}

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";

%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function consultaCarta(idCorrCdCorrespondenci , idChamCdChamado, idPessCdPessoa, corrInSms) {
	if(corrInSms != 'S'){
		window.open('<%=url%>?tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + idPessCdPessoa + '&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>','Documento','width=950,height=600,top=150,left=85');
	}else{
		window.open('/csicrm/sms/consultar.do?tela=compose&acao=editar&idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&idChamCdChamado=' + idChamCdChamado + '&idPessCdPessoa=' + idPessCdPessoa + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,'Documento','width=455,height=350,top=150,left=85')
	}
}

function reencaminharCarta(idCorrCdCorrespondenci , idChamCdChamado, idPessCdPessoa, dtInicial) {
	window.open('<%=url%>?tela=compose&origem=workflow&origem=workflow&acao=reencaminhar&acaoReencaminhar=reencaminhar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbCorrespondenciCorrVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + idPessCdPessoa + '&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>&idFuncCdFuncionario=<%=idFuncionario%>'  + '&chamDhInicial=' + dtInicial,'Documento','width=950,height=600,top=100,left=50');
}

//Chamado: 91854 - 27/11/2013 - Carlos Nunes
function consultaRetorno(idCorrCdCorrespondenci,idPessCdPessoa,idRecoCdRetornocorresp){
	showModalDialog('../csicrm/RetornoCorresp.do?tela=ifrmPopupRetornoCorresp&acao=consultar&csNgtbRetornocorrespRecoVo.idPessCdPessoa=' + idPessCdPessoa + '&csNgtbRetornocorrespRecoVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp=' + idRecoCdRetornocorresp,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:275px,dialogTop:0px,dialogLeft:200px');
}

//-->
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">

<html:form action="Historico.do" styleId="historicoForm">

<html:hidden property="acao"/>
<html:hidden property="tela"/>
<html:hidden property="csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci"/>
<html:hidden property="csNgtbCorrespondenciCorrVo.csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp"/>
<html:hidden property="idPessCdPessoa"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="principalLstCab" width="1%" height="20px">&nbsp;</td>
    <td class="principalLstCab" width="7%">&nbsp;<%=getMessage("prompt.numatend",request) %></td>
    <td class="principalLstCab" width="14%">&nbsp;<%=getMessage("prompt.dtatend",request) %></td>
	<td class="principalLstCab" width="19%">&nbsp;<%=getMessage("prompt.tipo",request) %></td>
    <td class="principalLstCab" width="19%">
    	<!-- VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX -->
		<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
			&nbsp;<%=getMessage("prompt.titulo",request) %>&nbsp; / &nbsp;<%=getMessage("prompt.NumFax",request) %>
		<% } else { %>
			&nbsp;<%=getMessage("prompt.titulo",request) %>
		<% } %>   
    </td>
    <td class="principalLstCab" width="15%">&nbsp;&nbsp;<%=getMessage("prompt.envio",request) %></td>
    <td id="tdAtendente" class="principalLstCab">&nbsp;&nbsp;<%=getMessage("prompt.atendente",request) %></td>
  </tr>
  <!-- Final do Header Historico -->
  
  <td colspan="7" valign="top" height="550px;"> 
      <div id="lstHistorico" style="width:100%; height: 550px; overflow: auto"> 
        <!--Inicio Lista Historico -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<logic:present name="historicoVector">
	        <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          <tr height="20" class="intercalaLst<%=numero.intValue()%2%>"> 
          
            <td class="principalLstPar" width="1%" valign="top">
      			&nbsp;
    		</td>
            <td class="principalLstPar" width="7%">&nbsp;<bean:write name="historicoVector" property="idChamCdChamado" /></td>
            <td class="principalLstPar" width="15%">&nbsp;<bean:write name="historicoVector" property="chamDhInicial" /></td>
            <td class="principalLstPar" width="19%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getDocuDsDocumento(), 15)%>
            </td>
            <td class="principalLstPar" width="20%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrDsTitulo(), 20)%>
            </td>
            <td class="principalLstPar" width="15%">&nbsp;
            	<% 
            	String corrInEnviaEmail = ((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrInEnviaEmail();
            	String dtEmissao = ((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrDtEmissao();
            	if(corrInEnviaEmail.equalsIgnoreCase("B") ){
					dtEmissao = "<acronym style=\"color: #ff0000\" title=\"N�o Contactar\">N�o Contactar</acronym>";
            	} else if(corrInEnviaEmail.equalsIgnoreCase("F") ){
            		dtEmissao = "<acronym style=\"color: #ff0000\" title=\"Falha no Envio\">Falha</acronym>";
            	} else if(corrInEnviaEmail.equalsIgnoreCase("S") ){
            		dtEmissao = "<acronym style=\"color: #c0c0c0\" title=\"Pendente\">Pendente</acronym>";
            	}
            	out.println(dtEmissao);
            	%>
            </td>
            <td class="principalLstPar" width="17%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFuncNmFuncionario(), 15)%>
            </td>
            <td class="principalLstPar" width="5%">&nbsp;
           		<% if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrInEnviaEmail().equalsIgnoreCase("T") && podeVisualizar){%>
           			<img src="webFiles/images/icones/envioEmail2.gif" width="19" align="absmiddle" border="0" class="geralCursoHand" title="<bean:message key="prompt.reencaminhar" />" onClick="reencaminharCarta('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoVector" property="idChamCdChamado" />','<bean:write name="historicoForm" property="idPessCdPessoa" />')"> 	      
           		<% } %>
			</td>
            <td class="principalLstPar" width="3%">  <!-- Chamado: 91854 - 27/11/2013 - Carlos Nunes -->
            	<%if (featureRetornoCorresp.equals("S")) {%>
					<%if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo().equals("D") && podeVisualizar){%>
							<img src="webFiles/images/botoes/RetornoCorrespAnimated.gif" border="0" class="geralCursoHand" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">
					<%}else if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo().equals("R") && podeVisualizar){%>
							<img src="webFiles/images/botoes/encaminhar.gif" border="0" class="geralCursoHand" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">	            		
					<%}
	              }%>
            </td>
            <!-- Chamado: 91854 - 27/11/2013 - Carlos Nunes -->
            <logic:equal name="historicoVector" property="idCorrCdCorrespondenci" value="0">
              <td class="principalLstPar" width="3%">&nbsp;</td>
            </logic:equal>
            <logic:notEqual name="historicoVector" property="idCorrCdCorrespondenci" value="0">
            	<td class="principalLstPar" width="3%"><img src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.visualizar" />" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.ConsultarCorrespondencia" />" onClick="consultaCarta('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoVector" property="idChamCdChamado" />','<bean:write name="historicoForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="corrInSms" />')"></td>
            </logic:notEqual>
          </tr>
        </logic:iterate>
        </logic:present>
        </table>
        <!--Final Lista Historico -->
      </div>
       </td>
  </tr>
  
</table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>