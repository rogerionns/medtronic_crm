<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

	function verificacao(){
		var editar = false;

		if(lstLocalizadorAtend.acao.value == "<%=Constantes.ACAO_VERIFICAR%>"){

			if (lstLocalizadorAtend.permissaoManifestacao.value == "N"){
				alert ('<bean:message key="prompt.alert.usuario_sem_permissao_de_acesso_a_este_tipo_de_manifestacao"/>.');
				return false;
			}else{
				parent.consultaManifestacao();
				return false;
			}
		}
		
		if (lstLocalizadorAtend.permissaoManifestacao.value == "N"){
			alert ('<bean:message key="prompt.alert.usuario_sem_permissao_de_acesso_a_este_tipo_de_manifestacao"/>.');
			return false;
		}

		if(lstLocalizadorAtend.acao.value == "verificado"){
			
			var mensagem = '';
			<%
			//Caso o usuario que esta logado seja igual ao usuario que travou deve apresentar a mensagem independente da feature.
			boolean mesmoUsuario = false;
			
			String permiteEditar = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PERMITE_EDITAR_CHAMADO_USO,request);
			CsCdtbFuncionarioFuncVo csCdtbFuncionarioFuncTravadoVo = (CsCdtbFuncionarioFuncVo)request.getAttribute("csCdtbFuncionarioFuncTravadoVo");
			CsCdtbFuncionarioFuncVo csCdtbFuncionarioFuncSessaoVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			if (csCdtbFuncionarioFuncTravadoVo != null && csCdtbFuncionarioFuncTravadoVo.getIdFuncCdFuncionario() == csCdtbFuncionarioFuncSessaoVo.getIdFuncCdFuncionario()){
				mesmoUsuario = true;
			}
			
			if(csCdtbFuncionarioFuncTravadoVo != null){
				if (permiteEditar!= null && permiteEditar.equals("N") && !mesmoUsuario){				
				%>
					mensagem = "<bean:message key="prompt.alert.registro_sendo_utilizado_nao_permitir"/>";
					mensagem = mensagem.replace("##", "<bean:write name='csCdtbFuncionarioFuncTravadoVo' property='funcNmFuncionario'/>");
					alert(mensagem);
				<%
				}else{
				%>
					mensagem = "<bean:message key="prompt.alert.registro_sendo_utilizado"/>";
					mensagem = mensagem.replace("##", "<bean:write name='csCdtbFuncionarioFuncTravadoVo' property='funcNmFuncionario'/>");
					if (confirm(mensagem)){
						editar = true;
					}
				<%
				}
			}
			%>
		}else{
			editar = true;
		}
		
		
		if(editar)
			parent.parent.parent.carregarWkf(
					lstLocalizadorAtend['localizadorAtendimentoVo.idPessCdPessoa'].value,
					lstLocalizadorAtend['localizadorAtendimentoVo.idChamCdChamado'].value, 
					lstLocalizadorAtend['localizadorAtendimentoVo.maniNrSequencia'].value, 
					lstLocalizadorAtend['localizadorAtendimentoVo.idTpmaCdTpManifestacao'].value, 
					lstLocalizadorAtend['localizadorAtendimentoVo.idAsnCdAssuntoNivel'].value, 
					lstLocalizadorAtend['localizadorAtendimentoVo.foupNrSequencia'].value,
					lstLocalizadorAtend['localizadorAtendimentoVo.idUtilCdFuncionario'].value,
					lstLocalizadorAtend['localizadorAtendimentoVo.idTppuCdTipopublico'].value,
					lstLocalizadorAtend['localizadorAtendimentoVo.idMatpCdManifTipo'].value,
					lstLocalizadorAtend['localizadorAtendimentoVo.idLinhCdLinha'].value,
					lstLocalizadorAtend['localizadorAtendimentoVo.idGrmaCdGrupoManifestacao'].value);
		
		
	}
	
</script>
<body onload="verificacao()">
<html:form action="/LocalizadorAtendimento.do" styleId="lstLocalizadorAtend">

<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="localizadorAtendimentoVo.idPessCdPessoa"/>
<html:hidden property="localizadorAtendimentoVo.idChamCdChamado"/>
<html:hidden property="localizadorAtendimentoVo.maniNrSequencia"/>
<html:hidden property="localizadorAtendimentoVo.idTpmaCdTpManifestacao"/>
<html:hidden property="localizadorAtendimentoVo.idAsnCdAssuntoNivel"/>
<html:hidden property="localizadorAtendimentoVo.foupNrSequencia"/>
<html:hidden property="localizadorAtendimentoVo.idUtilCdFuncionario"/>
<html:hidden property="localizadorAtendimentoVo.idTppuCdTipopublico"/>

<html:hidden property="localizadorAtendimentoVo.idMatpCdManifTipo"/>
<html:hidden property="localizadorAtendimentoVo.idLinhCdLinha"/>
<html:hidden property="localizadorAtendimentoVo.idGrmaCdGrupoManifestacao"/>

<html:hidden property="permissaoManifestacao" />

</html:form>
</body>
</html>
