<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="br.com.plusoft.fw.util.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

boolean isW3c = br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request);

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
    //Chamado: 95927 - 18/07/2014 - Carlos Nunes
	String actionLocalizadorEspecAux = new String("");
	actionLocalizadorEspecAux = Geral.getActionProperty("localizadorAtendimentoEspecAction" , empresaVo.getIdEmprCdEmpresa());

	//Chamado: 84585 - 08/10/2012 - Carlos Nunes
	String actionLocalizadorKernel = new String("");
	actionLocalizadorKernel = Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa());
%>

<%
//Gargamel Solicita��o do Henrique - Carlos/Alana fizeram para a Gol no espec
//Valida��o de limita��o de datas na mesa
int nMaxDias = 0; //(DEFAULT os campos podem ficar em branco)

try{
	nMaxDias = Integer.parseInt((String) AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_APL_MESA_INTERVALO_DATA_DIAS, 1));
}catch(Exception e){}

%>

<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>


	<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
	<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
	<script type="text/javascript" src="webFiles/funcoes/variaveis.js"></script>
	<script type="text/javascript" src="webFiles/funcoes/sorttable.js"></script>


	<script type="text/javascript">
	    //Chamado: 95927 - 18/07/2014 - Carlos Nunes
	    var actionLocalizadorEspec = "<%=actionLocalizadorEspecAux%>";
	    var modulo = "workflow";
		/**
		  * Fun��es Resposta em Lote
		  * jvarandas - 03/09/2010
		  * 
		  * As fun��es e altera��es nesse trecho devem ser replicadas no CRM e no Workflow
		  */
	
		/**
		  * Fun��o que valida se possuem pend�ncias selecionadas e se a sele��o � v�lida
		  *
		  * -> N�o deve permitir que um usu�rio selecione pend�ncias de v�rias etapas para responder em lote, 
		  * se for um processo de workflow, ele s� pode responder em lote pend�ncias que estiverem na mesma etapa.
		  */
		var nEtapaProcesso = -1;
		function validaPendenciaSelecionada() {
			var objs = lstIndicacoes.document.getElementsByName("localizadorAtendimentoVo.destCheck");
	
			var bPendSelecionada = false;
			var sPendSelecionadas = "";
			nEtapaProcesso = -1;
	
			for(var i = 0; i < objs.length; i++) {
				var destCheck = objs[i];
	
				if(destCheck.checked) {
					bPendSelecionada = true;
	
					if(nEtapaProcesso == -1) {
						nEtapaProcesso = destCheck.getAttribute("idEtprCdEtapaprocesso");
					} else if(nEtapaProcesso != destCheck.getAttribute("idEtprCdEtapaprocesso")) {
						alert("<bean:message key='prompt.asPendenciasSelecionadasEstaoEtapasDiferentesFluxoWorkflow'/>");
						return false;
					}
				}
			}
	
			if(objs.length==0 || bPendSelecionada==false) {
				alert("<bean:message key="prompt.alert.selecione_uma_pendencia"/>");
				return false;
			}
	
			return true;
		}
	
	
		/**
		  * Fun��o utilizada para carregar a 
		  */
		function carregaTelaResposta(){
			if(validaPendenciaSelecionada() == false)
				return false;

			var url = "../csicrm/AbrirTelaRespostaLote.do?idEmprCdEmpresa="+ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value+"&idEtprCdEtapaprocesso="+nEtapaProcesso;
			url = parent.ifrmTelaWorkflow.obterLinkSaas(url);
			
			showModalDialog(url, window, "help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:400px,dialogTop:200px,dialogLeft:200px");
		}
	</script>

	<script type="text/javascript">
		parent.document.getElementById('aguarde').style.visibility = 'visible';


		//Danilo Prevides - 17/11/2009 - 67645 - INI
		function carregaProdDescontinuado(){
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
			    var cmbLinha =  ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha");
				if (localizadorAtendimentoForm.chkDecontinuado.checked == true){
					cmbLinha['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S";
				}else{
					cmbLinha['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N";
				}	
			<%}%>

			<% // Chamado: 95626 - 31/07/2014 - Daniel Gon�alves %>
			if(ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbLinhaLinhVo.idLinhCdLinha"].value != 0){
				cmbLinha.submit();
			}else {
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").target     = "ifrmCmbLocalLinha";
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").tela.value = "<%= MCConstantes.TELA_CMB_LOCAL_LINHA%>";
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbLinhaLinhVo.idLinhCdLinha"].value = 0;
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").idEmprCdEmpresa.value = parent.idEmprCdEmpresa;
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").submit();
			}
			
		}
		//Danilo Prevides - 17/11/2009 - 67645 - FIM
		
		//Danilo Prevides - 15/10/2009 - 66813 - INI 
		
		function carregaTodosCamposComboPeriodo(){
			limpaCamposComboPeriodo();
			adicionaCamposComFollowUp();			
		}
		
		function adicionaCamposPadrao(){

			var optionSelOpcao = new Option();		
			optionSelOpcao.text = '<bean:message key="prompt.combo.sel.opcao"/>';
			optionSelOpcao.value = 0;

			//Chamado 78938 - Renomeado para Data Abertura da Manifesta��o
			var optionAberturaManif = new Option();
			optionAberturaManif.text = '<bean:message key="prompt.DataAbertutaManif"/>'; 
			optionAberturaManif.value = 1;
			
			//Chamado 78938 - Incluido busca pela data do chamado, pois pela mani_dh_abertura j� existia
			var optionDataChamado = new Option();
			optionDataChamado.text = '<bean:message key="prompt.DataDoChamado"/>'; 
			optionDataChamado.value = 10;
			
			var optionDataPrevisaoManif = new Option();
			optionDataPrevisaoManif.text = '<bean:message key="prompt.DataDePrevisaoDaManifestacao"/>'; 
			optionDataPrevisaoManif.value = 2;

			var optionDataConclusaoManif = new Option();
			optionDataConclusaoManif.text = '<bean:message key="prompt.DataDeConclusaoDaManifestacao"/>'; 
			optionDataConclusaoManif.value = 3;
			
			var optionDataConclusaoPendencia = new Option();
			optionDataConclusaoPendencia.text = '<bean:message key="prompt.DataDeConclusaoDaPendencia"/>'; 
			optionDataConclusaoPendencia.value = 4;

			var optionDataCriacaoPendencia = new Option();
			optionDataCriacaoPendencia.text = '<bean:message key="prompt.DataDeCriacaoDaPendencia"/>'; 
			optionDataCriacaoPendencia.value = 5;

			var optionDataRespostaPendencia = new Option();
			optionDataRespostaPendencia.text = '<bean:message key="prompt.DataDeRespostaDaPendencia"/>'; 
			optionDataRespostaPendencia.value = 6;

			var selectPeriodo = document.getElementsByName("localizadorAtendimentoVo.periodo")[0];

			try {//IE
				selectPeriodo.add(optionSelOpcao);
				selectPeriodo.add(optionAberturaManif);
				selectPeriodo.add(optionDataChamado);
				selectPeriodo.add(optionDataPrevisaoManif);
				selectPeriodo.add(optionDataConclusaoManif);
				selectPeriodo.add(optionDataConclusaoPendencia);
				selectPeriodo.add(optionDataCriacaoPendencia);
				selectPeriodo.add(optionDataRespostaPendencia);
			} catch(e) { //Firefox, Safari
				selectPeriodo.add(optionSelOpcao, null);
				selectPeriodo.add(optionAberturaManif, null);
				selectPeriodo.add(optionDataChamado, null);
				selectPeriodo.add(optionDataPrevisaoManif, null);
				selectPeriodo.add(optionDataConclusaoManif, null);
				selectPeriodo.add(optionDataConclusaoPendencia, null);
				selectPeriodo.add(optionDataCriacaoPendencia, null);
				selectPeriodo.add(optionDataRespostaPendencia, null);
			}
		}
		
		function adicionaCamposSemFollowUp(){
			adicionaCamposPadrao();	
		}

		function adicionaCamposComFollowUp(){

			var selectPeriodo = document.getElementsByName("localizadorAtendimentoVo.periodo")[0];
			
			adicionaCamposPadrao();
			
			//Chamado 103826 - 22/09/2015 Victor Godinho
			return;
			
			var optionDataPrevisaoFollowUp = new Option();		
			optionDataPrevisaoFollowUp.text = '<bean:message key="prompt.DataDePrevisaoDoFollowup"/>';
			optionDataPrevisaoFollowUp.value = 7;

			var optionDataCriacaoFollowUp = new Option();		
			optionDataCriacaoFollowUp.text = '<bean:message key="prompt.DataDeCriacaoDoFollowup"/>';
			optionDataCriacaoFollowUp.value = 8;
			
			var optionDataConclusaoFollowUp = new Option();		
			optionDataConclusaoFollowUp.text = '<bean:message key="prompt.DataDeConclusaoDoFollowup"/>';
			optionDataConclusaoFollowUp.value = 9;

			try {//IE
				selectPeriodo.add(optionDataPrevisaoFollowUp);
				selectPeriodo.add(optionDataCriacaoFollowUp);
				selectPeriodo.add(optionDataConclusaoFollowUp);
			} catch(e) { //Firefox, Safari
				selectPeriodo.add(optionDataPrevisaoFollowUp, null);
				selectPeriodo.add(optionDataCriacaoFollowUp, null);
				selectPeriodo.add(optionDataConclusaoFollowUp, null);
			}			
		}
		
		function limpaCamposComboPeriodo(){
			var selectPeriodo = document.getElementsByName("localizadorAtendimentoVo.periodo")[0];
			try {
				for (i = selectPeriodo.length - 1; i>=0; i--) {
					//var campo = selectPeriodo.options[i];
					selectPeriodo.remove(i);
				}
			} catch (e){}
			//Caso o combo esteja nulo, acontecera o erro
			

		}
		
		function carregaCamposComboPeriodo(){
			//Limpando todos os campos do combo periodo antes de adicionar novos campos
			limpaCamposComboPeriodo();

			//Caso o campo selecionado for deiferente de followUp, adicionar os campos indiferente do followUp
			if (ifrmCmbLocalManifTipo.getValorCombo() == 0){
				adicionaCamposComFollowUp();
			}else if (ifrmCmbLocalManifTipo.getValorCombo() != -1){
				adicionaCamposSemFollowUp();
			}else {
				adicionaCamposComFollowUp();
			}

		}
		//Danilo Prevides - 15/10/2009 - 66813 - FIM

		
		function carregaListaAtend(exportXls){ // Chamado 81376 - 22/11/2013 - Jaider Alba (flag exportXls)
			var bFiltro;
			var nIdManifTipo;
			var nIdGrupoManif;
			var nIdTipoManif;
			var cDhChamInicio;
			var cDhChamFim;
			var nIdAreaDest;
			var nIdFuncDest;
			var nIdFuncAtend;
			var cNomePess;
			var nIdCham;
			var inGrave;
			var nIdLinha;
			var nIdASN;
			var nIdASNVector;
			var nIdStatus;
			var nIdClassificacao;
			var nIdLoja;
			var nPedido;
			var nIdPendencia;
			var nIdSuperGrupo;
			var nPorcentagemDe;
			var nPorcentagemAte;
			var nPeriodo;
			var nCodStatusPendencia;
			var nIdDesenho;
			var nIdEtapa;
			
			// Chamado 81376 - 22/11/2013 - Jaider Alba
			if(exportXls == undefined || exportXls == null || (exportXls != 'true' &&  exportXls != true)){
				exportXls = false;
			}
			
			bFiltro = false;
			
		/*	if (lstIndicacoes.document.readyState != 'complete'){
				return false;
			}*/
			
			if(localizadorAtendimento.txtDhInicio.value != "" || localizadorAtendimento.txtDhFim.value != "") {
				if(localizadorAtendimento['localizadorAtendimentoVo.periodo'].value == 0) {
					alert("<bean:message key="prompt.InformeUmPeriodo"/>");
					return false;
				}
				
				if((localizadorAtendimento['localizadorAtendimentoVo.periodo'].value == 3 || localizadorAtendimento['localizadorAtendimentoVo.periodo'].value == 9) && localizadorAtendimento['localizadorAtendimentoVo.idPendencia'].value < 3) {
					alert("<bean:message key="prompt.ParaEsteTipoBusca"/>");
					return false;
				}
			}
			
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				localizadorAtendimento['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = ifrmCmbLocalVariedade.concatena();
			<%}%>
			
			nIdManifTipo = ifrmCmbLocalManifTipo.getValorCombo();
			nIdGrupoManif = ifrmCmbLocalGrupoManif.getValorCombo();
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
					nIdSuperGrupo = ifrmCmbLocalSuperGrupo.getValorCombo();
			<%}%>
			nIdTipoManif = ifrmCmbLocalTipoManif.getValorCombo();
			cDhChamInicio = localizadorAtendimento.txtDhInicio.value;
			cDhChamFim = localizadorAtendimento.txtDhFim.value;
			nIdAreaDest = ifrmCmbLocalArea.getValorCombo();
			nIdFuncDest = ifrmCmbLocalDest.getValorCombo();
			nIdFuncAtend = IfrmCmbLocalAtend.getValorCombo();
			
			nIdEventoFollowup = "0";
			try{
			nIdEventoFollowup = ifrmCmbLocalEventoFollowup.getValorCombo();
			}catch(e){}
			
			cNomePess = localizadorAtendimento.txtNome.value;
			//while(cNomePess.indexOf("'") >= 0){
				//cNomePess = cNomePess.replace("'","");
			//}
			
			<% if(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_ATENDENTE){ %>
				if(nIdFuncDest == "0" && nIdFuncAtend == "0"){
					alert("<bean:message key="prompt.Selecione_uma_opcao_combo_destinatario_ou_atendente"/>");
					return false;
				}
			<%}	%>
			
			nIdCham = localizadorAtendimento.txtIdCham.value; 
			nPorcentagemDe = localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListDe'].value;
			nPorcentagemAte = localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListAte'].value;
			nPeriodo = localizadorAtendimento['localizadorAtendimentoVo.periodo'].value;
			nCodStatusPendencia = localizadorAtendimento['localizadorAtendimentoVo.codStatusPendencia'].value;
						
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			nIdLinha = ifrmCmbLocalLinha.getValorCombo();
			<%}%>
			
			nIdASN = ifrmCmbLocalProduto.getValorCombo();

			
			nRegDe = localizadorAtendimento['localizadorAtendimentoVo.regDe'].value;
			nRegAte = localizadorAtendimento['localizadorAtendimentoVo.regAte'].value;
			
			nIdStatus = cmbStatusManif.document.getElementById("cmbStatusManif")['csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value;
			nIdClassificacao = cmbClassifManif.document.getElementById("cmbClassifManif")['csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
			nIdPendencia = localizadorAtendimento['localizadorAtendimentoVo.idPendencia'].value;
			nIdDesenho = localizadorAtendimento['localizadorAtendimentoVo.idDeprCdDesenhoprocesso'].value;
			nIdEtapa = cmbEtapa.document.forms[0]['localizadorAtendimentoVo.idEtprCdEtapaprocesso'].value;
			
			if (nIdCham.length > 0 ){
				//=================================================
				// Gargamel - 23/09/2009 - Acertos na busca da mesa
				// Chamado: 66115
				//-------------------------------------------------
				// Confirmar se o valor � num�rico e 
				// devolver para o campo em tela. Tirar 
				// os espa�os e zerar quando n�o for n�mero
				//-------------------------------------------------
				if (Number(trim(nIdCham)).toString()=='NaN') {
					nIdCham = '0';
				} else {
					nIdCham = Number(trim(nIdCham)).toString();
				}
				localizadorAtendimento.txtIdCham.value = nIdCham;
				//=================================================
			}

			if ((nIdCham==null) || (nIdCham=="") || (nIdCham=="0")) {
				if (!validaFiltroData()) {
					return false;
				}
			}

	/*		inGrave = ""
			if (localizadorAtendimento.chkInGrave.checked)
				inGrave = "S"
	*/		
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idMatpCdManifTipo'].value = nIdManifTipo;
			if (nIdManifTipo != "0"){
				bFiltro = true;
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idGrmaCdGrupoManifestacao'].value = nIdGrupoManif;			
			if (nIdGrupoManif != "0"){
				bFiltro = true;
			}
	
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idSugrCdSupergrupo'].value = nIdSuperGrupo;			
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idTpmaCdTpManifestacao'].value = nIdTipoManif;					
			if (nIdTipoManif != "0"){
				bFiltro = true;			
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.chamDhInicial'].value = cDhChamInicio;		
			if (cDhChamInicio.length > 0 ){
				//Chamado 78120 - Validar se a data inicial � maior que a final!
				if (cDhChamFim.length > 0 ){
					var dataInicio = cDhChamInicio.substring(3,5)+"/"+cDhChamInicio.substring(0,2)+"/"+cDhChamInicio.substring(7,11);
					var dataFim = cDhChamFim.substring(3,5)+"/"+cDhChamFim.substring(0,2)+"/"+cDhChamFim.substring(7,11);
					if(new Date(dataInicio)>new Date(dataFim)){
						alert('<bean:message key="prompt.dataInicial.maior"/>!');
						return false;
					}					
				}
				bFiltro = true;			
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.chamDhFinal'].value = cDhChamFim;
			if (cDhChamFim.length > 0 ){
				bFiltro = true;			
			}
	
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAreaCdDestinatario'].value = nIdAreaDest;
			if (nIdAreaDest != "0"){
				bFiltro = true;
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idFuncCdDestinatario'].value = nIdFuncDest;
			if (nIdFuncDest != "0"){
				bFiltro = true;			
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idFuncCdFuncionario'].value = nIdFuncAtend;
			if (nIdFuncAtend != "0"){
				bFiltro = true;			
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idEvfuCdEventoFollowUp'].value = "";
			if (nIdEventoFollowup != "0"){
				bFiltro = true;
				lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idEvfuCdEventoFollowUp'].value = nIdEventoFollowup;
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.pessNmPessoa'].value = cNomePess.toUpperCase();
			if (cNomePess.length > 0 ){
				if (cNomePess.length >= 3){
					bFiltro = true;
				}
				else{
					alert("<bean:message key="prompt.Digite_no_minimo_tres_caracteres_para_pesquisa"/>")
					return false;
				}
			}
					
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idChamCdChamado'].value = "";
			if (nIdCham.length > 0 ){
				bFiltro = true;
				lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idChamCdChamado'].value = nIdCham;
			}

			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.porcentagemCheckListDe'].value = nPorcentagemDe;
			if (nPorcentagemDe.length > 0 ){
				bFiltro = true;
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.porcentagemCheckListAte'].value = nPorcentagemAte;
			if (nPorcentagemAte.length > 0 ){
				bFiltro = true;
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.periodo'].value = "";
			if (nPeriodo.length > 0 ){
				bFiltro = true;
				lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.periodo'].value = nPeriodo;
			}	
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.codStatusPendencia'].value = "";
			if (nCodStatusPendencia.length > 0 ){
				bFiltro = true;
				lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.codStatusPendencia'].value = nCodStatusPendencia;
			}	
									
	/*		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.maniInGrave'].value = "";
			if (inGrave == "S" ){
				bFiltro = true;
				lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.maniInGrave'].value = inGrave;
			}
	*/		
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idLinhCdLinha'].value = nIdLinha;
			if (nIdLinha != "0"){
				bFiltro = true;
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAsn1CdAssuntoNivel1'].value = "";
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAsn2CdAssuntoNivel2'].value = "";
			
			if (nIdASN != "0"){
				bFiltro = true;
				nIdASNVector = nIdASN.split("@");
				lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAsn1CdAssuntoNivel1'].value = nIdASNVector[0];
				lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAsn2CdAssuntoNivel2'].value = nIdASNVector[1];	
			}
			
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAsn2CdAssuntoNivel2'].value = ifrmCmbLocalVariedade.getValorCombo();
			<%}%>
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idStmaCdStatusmanif'].value = nIdStatus;
			if (nIdStatus != "0"){
				bFiltro = true;			
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idClmaCdClassifmanif'].value = nIdClassificacao;
			if (nIdClassificacao != "0"){
				bFiltro = true;			
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idPendencia'].value = nIdPendencia;
			if (nIdPendencia != "0"){
				bFiltro = true;			
			}

			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idDeprCdDesenhoprocesso'].value = nIdDesenho;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idEtprCdEtapaprocesso'].value = nIdEtapa;	
			
			//Caso nao tenha nenhum filtro informado a aplicacao verifica o form especifico
			if 	(bFiltro == false){
				bFiltro = camposEspec.camposInformados();
			}
			
			if 	(bFiltro == false){
				alert("<bean:message key="prompt.Selecione_um_filtro_para_pesquisa"/>");
				return false;
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.regDe'].value = nRegDe;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.regAte'].value = nRegAte;
			//Chamado: 95927 - 18/07/2014 - Carlos Nunes
			if(actionLocalizadorEspec != 'LocalizadorAtendimento.do'){
				try{
					if(localAtendEspec.validaFiltrosEspec() == false){
						 return false;
					}
				}catch(e){}
			}
			
			//obtem valores do iframe especificio e coloca como hidden na lista para executar o submit
			camposEspec.setValoresToForm(lstIndicacoes);
			lstIndicacoes.document.getElementById("lstLocalizadorAtend").idEmprCdEmpresa.value = ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend").idEmprCdEmpresaMesa.value = ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
			//Chamado: 95927 - 18/07/2014 - Carlos Nunes
			if(actionLocalizadorEspec != 'LocalizadorAtendimento.do'){
				if(localAtendEspec != undefined && localAtendEspec.document.forms[0] != undefined){
					localAtendEspec.setValoresToForm(lstIndicacoes);					
				}
			}
			
			if(exportXls){ // Chamado 81376 - 22/11/2013 - Jaider Alba
				lstIndicacoes.document.getElementById("lstLocalizadorAtend").tela.value = '<%=MCConstantes.TELA_EXPORTA_XLS%>';
				lstIndicacoes.document.getElementById("lstLocalizadorAtend").acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
				lstIndicacoes.document.getElementById("lstLocalizadorAtend").target = '<%=MCConstantes.IFRM_EXPORTA_XLS%>';

				alert('<%= getMessage("prompt.mensagem_exportar_xls", request)%>');
			}
			else{
				localizadorAtendimento.existeRegistroLista.value = false;				
				parent.document.getElementById('aguarde').style.visibility = 'visible';
								
				lstIndicacoes.document.getElementById("lstLocalizadorAtend").tela.value = '<%=MCConstantes.TELA_LST_LOCALIZADORATEND%>';
				lstIndicacoes.document.getElementById("lstLocalizadorAtend").acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
				lstIndicacoes.document.getElementById("lstLocalizadorAtend").target = 'lstIndicacoes';
				
				//Chamado: 83390 - 25/07/2012 - Carlos Nunes
				document.getElementById('tituloLocalizadorAtend').scrollLeft=0;
				document.getElementById('tituloLocalizadorAtendEscondido').scrollLeft=0;
				lstIndicacoes.document.getElementById("lstRegistro").scrollLeft=0;
				lstIndicacoes.document.getElementById("divTitulo").scrollLeft=0;
	
				//Chamado: 91288 - 09/10/2013 - Carlos Nunes
				document.getElementById('btnFiltro').disabled=true;
				document.getElementById('btnFiltro').className = 'geralImgDisable';
				
				$('#btnExportar').attr('disabled','disabled');
				$('#spnExportar').attr('disabled','disabled');
			}
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend").submit();
		}

		function validaFiltroData(){
			var dataInicial = localizadorAtendimento['txtDhInicio'].value;
			var dataFinal = localizadorAtendimento['txtDhFim'].value;
			var nDiasMax = Number(<%=nMaxDias%>);
			var mensAlert = "";

			if (nDiasMax == 0) {	return true; } 

			if ((dataInicial == null) || (dataInicial == "")) {
				mensAlert = '<%= getMessage("prompt.alert.Data_inicial_chamado_mesa_obrigatorio", request)%>';
			} else {
				if ((dataFinal == null) || (dataFinal == "")) {
					mensAlert = '<%= getMessage("prompt.alert.Data_final_chamado_mesa_obrigatorio", request)%>';
				} else {
					dataInicial = dataInicial;
					dataFinal = dataFinal;
		            if(dateDiff(dataInicial, dataFinal) > (nDiasMax-1)) {
		        		mensAlert = '<%= getMessage("prompt.alert.Intervalo_permitido_entre_datas", request)%>';
		        		mensAlert = mensAlert.replace("XXX", nDiasMax);
		    		}
				}
			}
			if (mensAlert!="") {
				alert(mensAlert);
			}
			return (mensAlert=="");
		}           

		function pressEnter(evnt) {
			if (evnt.keyCode == 13) {
				
				window.document.getElementById('localizadorAtendimento')['localizadorAtendimentoVo.regDe'].value='0';
				window.document.getElementById('localizadorAtendimento')['localizadorAtendimentoVo.regDe'].value='0';
				initPaginacao();
								
				dtIni = localizadorAtendimento.txtDhInicio.value;
				dtFim = localizadorAtendimento.txtDhFim.value;
				
				if(dtIni.length>0){
					localizadorAtendimento.txtDhInicio.onblur="";
					if(!verificaData(localizadorAtendimento.txtDhInicio)){
						localizadorAtendimento.txtDhInicio.onblur=function anonymous(){ verificaData(this) };
						return false;
					}else{
						carregaListaAtend();
						return true;
					}
				}

				if(dtFim.length>0){
					localizadorAtendimento.txtDhFim.onblur="";
					if(!verificaData(localizadorAtendimento.txtDhFim)){
						localizadorAtendimento.txtDhFim.onblur=function anonymous(){ verificaData(this) };
						return false;
					}else{
						carregaListaAtend();
						return true;
					}
				}

				carregaListaAtend();
			}
		}

		function printPage() { print(); }
		
		function printIframe(id)
		{
		    var iframe = document.frames ? document.frames[id] : document.getElementById(id);
		    var ifWin = iframe.contentWindow || iframe;
		    ifWin.printPage();
		    return false;
		}

		
		function imprimir() {
			lstIndicacoes.document.getElementById("divTitulo").style.display = "block";
			document.getElementById('tituloLocalizadorAtend').style.display = "none";
			//Danilo Prevides - 23/09/2009 - 66422 - INI
			lstIndicacoes.document.getElementById("lstRegistro").style.overflow = "";
			//Danilo Prevides - 23/09/2009 - 66422 - FIM
			
			if (localizadorAtendimento.existeRegistroLista.value == 'false') {
				lstIndicacoes.focus();
				lstIndicacoes.print();
			} else {
				alert("<bean:message key="prompt.A_lista_esta_vazia" />");
			}
			//Danilo Prevides - 23/09/2009 - 66422 - INI
			setTimeout("lstIndicacoes.document.getElementById('divTitulo').style.display = 'none';document.getElementById('tituloLocalizadorAtend').style.display = 'block';lstIndicacoes.document.getElementById('lstRegistro').style.overflow='auto'", 2000);
			//Danilo Prevides - 23/09/2009 - 66422 - FIM
		}
	
		function limparFiltro(){
			document.location = "LocalizadorAtendimento.do?tela=localizadorAtendimento";
		}
	    //Chamado: 95927 - 18/07/2014 - Carlos Nunes
		function AtivarPasta(pasta) {
			switch (pasta) {
				case 'FP':
		
					if(actionLocalizadorEspec != "LocalizadorAtendimento.do"){
					<%//if(!actionLocalizadorEspec.equals("LocalizadorAtendimento.do")){ %>
						MM_showHideLayers('divFiltroPadrao','','show','divFiltroEspec','','hide');
					<%//}%>
							
					<%//if(!actionLocalizadorEspec.equals("LocalizadorAtendimento.do")){ %>
						SetClassFolder('tdFiltroEspec','principalPstQuadroLinkNormal');
					<%//}%>
			        }
						
					SetClassFolder('tdFiltroPadrao','principalPstQuadroLinkSelecionado');
					break;
		
				case 'FE':
					if(actionLocalizadorEspec != "LocalizadorAtendimento.do"){
					<%//if(!actionLocalizadorEspec.equals("LocalizadorAtendimento.do")){ %>
						MM_showHideLayers('divFiltroPadrao','','hide','divFiltroEspec','','show');
					<%//}%>
							
					<%//if(!actionLocalizadorEspec.equals("LocalizadorAtendimento.do")){ %>
						SetClassFolder('tdFiltroEspec','principalPstQuadroLinkSelecionado');
					<%//}%>
					}
						
					SetClassFolder('tdFiltroPadrao','principalPstQuadroLinkNormal');
					break;
			}
		}
		
		function MM_showHideLayers() { //v3.0
		  var i,p,v,obj,args=MM_showHideLayers.arguments;
		  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
		    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
		    obj.visibility=v; }
		}
		
		function MM_findObj(n, d) { //v4.0
		  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		  if(!x && document.getElementById) x=document.getElementById(n); return x;
		}
		
		function SetClassFolder(pasta, estilo) {
			stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
			eval(stracao);
		} 
	
		
		function verificarModalManifestacao(){
			if(bModalManifestacao){
				lstIndicacoes.alteraManifestacao();
			}
		}		
	
		function verificarModalResposta(){
			if(bModalManifestacao){
				lstIndicacoes.document.getElementById("lstLocalizadorAtend").idEmprCdEmpresaMesa.value = ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
				
				lstIndicacoes.enviaResposta();
			}
		}		

		function inicio(){
			showError("<%=request.getAttribute("msgerro")%>");
			//Chamado: 95927 - 18/07/2014 - Carlos Nunes
			if(actionLocalizadorEspec != 'LocalizadorAtendimento.do'){
				localAtendEspec.location = actionLocalizadorEspec + '?tela=localizadorAtendimentoEspec';
			}
			
			
			//if(localizadorAtendimento.carregaLista.value != "") {
			//	setPaginacao(window.document.localizadorAtendimento['localizadorAtendimentoVo.regDe'].value,window.document.localizadorAtendimento['localizadorAtendimentoVo.regAte'].value);
			//}else{
				initPaginacao();
			//}
			
			atualizarCmbManifTipo();
			atualizarCmbLocalGrupoManif();
			atualizarCmbLocalTipoManif();
			atualizarCmbLocalLinha();
			atualizarCmbLocalArea();
			atualizarCmbLocalAtend();
			atualizarCmbStatusManif();
			atualizarCmbClassifManif();
			atualizarCmbEtapa();
			
			limpaCampos();

			try{
				var varDivDesc = document.getElementById("divDescontinuado").style.top;
				//68040, se nao der pau � porque o checkbox esta visivel na tela.
				document.getElementById("bt1").style.top = 250; 
				document.getElementById("bt2").style.top = 250; 
				document.getElementById("bt3").style.top = 245; 
			}catch(e){}
			//Chamado: 95927 - 18/07/2014 - Carlos Nunes
			if(actionLocalizadorEspec != 'LocalizadorAtendimento.do'){
				document.getElementById("tdFiltroEspec").style.display = "block";
			}
		}
		
		function carregaProdAssunto(){
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
				// 91085 - 01/10/2013 - Jaider Alba
				if (localizadorAtendimento.chkAssunto!=undefined && localizadorAtendimento.chkAssunto.checked == true){
					ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "N";
				}
				else{
					ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "S";
				}	
		<%	}	%>
			//cmbLinha.submeteForm();
			
			ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").target     = "ifrmCmbLocalLinha";
			ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").tela.value = "<%= MCConstantes.TELA_CMB_LOCAL_LINHA%>";
			ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbLinhaLinhVo.idLinhCdLinha"].value = 0;
			ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = ifrmCmbLocalTipoManif.document.getElementById("ifrmCmbTipoManif")["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
			ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").idEmprCdEmpresa.value = ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
			
			ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").submit();
		}
		
		function FechaJanela(){
			window.top.fechaSistema();
		}

		function submitPaginacao(regDe,regAte){
			
			localizadorAtendimento['localizadorAtendimentoVo.regDe'].value=regDe;
			localizadorAtendimento['localizadorAtendimentoVo.regAte'].value=regAte;
			carregaListaAtend();
			
		}

		function verificaConclusaoEmLote(){
			if(localizadorAtendimento['localizadorAtendimentoVo.idPendencia'].value==3){
				document.all.item("bntResposta").disabled = true;
				document.all.item("bntResposta").className = 'geralImgDisable';
			}else{
				document.all.item("bntResposta").disabled = false;
				document.all.item("bntResposta").className = 'geralCursoHand';
			}
		}
		
		function visualizaProduto(){
			var url = "";
			var asn=""
			var asnArray;
			
			var asn1="";
			var asn2="";
			var idLinh="";
			var filtroPras = "";
			
			idLinh = ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbLinhaLinhVo.idLinhCdLinha"].value;
			asn = ifrmCmbLocalProduto.document.getElementById("ifrmCmbLocalProduto")["csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;
		
			if (asn!="" && idLinh != "" && asn!="0" && idLinh != "0"){
				asnArray = asn.split("@");
				asn1 = asnArray[0];
				asn2 = asnArray[1];
				
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
					asn2 = ifrmCmbLocalVariedade.document.getElementById("ifrmCmbLocalVariedade")["csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
				<%}%>
				
				filtroPras = idLinh + "_" + asn1 + "_" + asn2;
				
			}
			
			url = "VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_VISUALIZADOR_PRODUTO%>";
			url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>";
			url = url + "&filtroPras=" + filtroPras;
			
			//window.open(url,'window','width=850,height=610,top=80,left=85');
			showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:720px;dialogTop:80px;dialogLeft:85px');
		}
		
        //Chamado: 90471 - 16/09/2013 - Carlos Nunes
		//------------------------------------------------------------------
		//Gargamel - 24-03-11
		//Fun��o para chamada espec quando atualizamos o combo de empresa
		function atualizarTela() {//Chamado: 95927 - 18/07/2014 - Carlos Nunes
			if(actionLocalizadorEspec != 'LocalizadorAtendimento.do'){
				try{
					localAtendEspec.atualizarEmpresaCombo();
				}catch(e){
					if(nCountAtualizarTela<5){
						setTimeout('atualizarTela()',300);
						nCountAtualizarTela++;
					}
				}
			}
		}
		//------------------------------------------------------------------

	
		function setValoresProduto(idLinh,idAsn1,idAsn2){
			ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbLinhaLinhVo.idLinhCdLinha"].value = idLinh;
			ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = idAsn1 + "@"+ idAsn2;
			ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = idAsn1;
			ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = idAsn2;
			ifrmCmbLocalLinha.carregaProduto();
			//ifrmCmbLocalLinha.visualizador = true;
		}
		
		/** Variaveis para controle do coarregamento dos objetos na tela **/
		var nCountAtualizarCmbLocalAtend = 0;
		var nCountAtualizarCmbStatusManif = 0;
		var nCountAtualizarCmbClassifManif = 0;
		var nCountAtualizarCmbLocalArea = 0;
		var nCountAtualizarCmbEtapa = 0;
		
		
		/**** FUNCOES PARA ATUALIZAR OS COMBOS DA TELA ****/
		function atualizarCmbManifTipo(){
			document.getElementById("ifrmCmbLocalManifTipo").src = "LocalizadorAtendimento.do?tela=cmbLocalManifTipo&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresa="+ ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
		}
		
		function atualizarCmbLocalGrupoManif(){
			document.getElementById("ifrmCmbLocalGrupoManif").src = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif&idEmprCdEmpresa="+ ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
		}

		function atualizarCmbLocalTipoManif(){
			document.getElementById("ifrmCmbLocalTipoManif").src = "LocalizadorAtendimento.do?tela=cmbLocalTipoManif&idEmprCdEmpresa="+ ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
		}
			
		function atualizarCmbLocalLinha(){
			document.getElementById("ifrmCmbLocalLinha").src = "LocalizadorAtendimento.do?tela=cmbLocalLinha&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresa="+ ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
		}
		
		function atualizarCmbLocalEventoFollowup(){
			document.getElementById("ifrmCmbLocalEventoFollowup").src = "LocalizadorAtendimento.do?tela=cmbLocalEventoFollowup&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresa="+ ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
		}
		
		function atualizarCmbLocalArea(){
			try{
				document.getElementById("ifrmCmbLocalArea").src = "LocalizadorAtendimento.do?tela=cmbLocalArea&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresa="+ ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
			}catch(e){
				if(nCountAtualizarCmbLocalArea<5){
					setTimeout('atualizarCmbLocalArea()',300);
					nCountAtualizarCmbLocalArea++;
				}
			}
		}
			
		function atualizarCmbLocalAtend(){
			try{
				//Chamado: 84585 - 08/10/2012 - Carlos Nunes
				document.getElementById("IfrmCmbLocalAtend").src = "<%=actionLocalizadorKernel%>?tela=cmbLocalAtend&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresa="+ ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
			}catch(e){
				if(nCountAtualizarCmbLocalAtend<5){
					setTimeout('atualizarCmbLocalAtend()',300);
					nCountAtualizarCmbLocalAtend++;
				}
			}
		}
			
		function atualizarCmbStatusManif(){
			try{
				document.getElementById("cmbStatusManif").src = "LocalizadorAtendimento.do?tela=cmbStatusManif&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresa="+ ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
			}catch(e){
				if(nCountAtualizarCmbStatusManif<5){
					setTimeout('atualizarCmbStatusManif()',300);
					nCountAtualizarCmbStatusManif++;
				}
			}
		}
			
		function atualizarCmbClassifManif(){
			try{
				document.getElementById("cmbClassifManif").src = "LocalizadorAtendimento.do?tela=cmbClassifManif&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresa="+ ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
			}catch(e){
				if(nCountAtualizarCmbClassifManif<5){
					setTimeout('atualizarCmbClassifManif()',300);
					nCountAtualizarCmbClassifManif++;
				}
			}
		}

		function atualizarCmbEtapa(){
			try{
				document.getElementById("cmbEtapa").src = "LocalizadorAtendimento.do?tela=cmbEtapa&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresa="+ ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value + '&localizadorAtendimentoVo.idDeprCdDesenhoprocesso=' + document.forms[0]['localizadorAtendimentoVo.idDeprCdDesenhoprocesso'].value;
			}catch(e){
				if(nCountAtualizarCmbEtapa<5){
					setTimeout('atualizarCmbEtapa()',300);
					nCountAtualizarCmbEtapa++;
				}
			}
		}
			
		/**** FIM DAS FUNCOES PARA ATUALIZAR OS COMBOS DA TELA ****/
		
		function limpaCampos() {
			if(localizadorAtendimento.txtIdCham.value == '0')
				localizadorAtendimento.txtIdCham.value = '';
			if(localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListDe'].value == '0')
				localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListDe'].value = '';
			if(localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListAte'].value == '0')
				localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListAte'].value = '';
		}

		var statusLista = "max";


//Chamado: 99344 - 11/02/2014 - Marcos Donato
		function min(){
//			document.getElementById("divFiltroEspec").style.height = "0px";
//			document.getElementById("tableFiltroEspec").style.display = "none";
//			document.getElementById("tableFiltroPadrao").style.display = "none";
//			document.getElementById("divFiltroPadrao").style.height = "0px";
			document.getElementById("divFiltroEspec").style.display = "none";
			document.getElementById("divFiltroPadrao").style.display = "none";

			document.getElementById("tdListaResultado").style.height = "455px";
			lstIndicacoes.document.getElementById("lstRegistro").style.height = "410px";
//			document.getElementById("bt1").style.top = "10px";
//			document.getElementById("bt2").style.top = "19px";
//			document.getElementById("bt3").style.top = "14px";
//			document.getElementById("bt1").style.left = "845px";
//			document.getElementById("bt2").style.left = "913px";
//			document.getElementById("bt3").style.left = "815px";
			document.getElementById("bt1").style.display = "none";
			document.getElementById("bt2").style.display = "none";
			document.getElementById("bt3").style.display = "none";
			document.getElementById("imgMaisMenos").src= "webFiles/images/menuVert/menos.gif";
			document.getElementById("imgMaisMenos").title = '<bean:message key="prompt.expandirFiltros"/>';
			statusLista = "min";
		}

//Chamado: 99344 - 11/02/2014 - Marcos Donato
		function max(){
//			document.getElementById("divFiltroEspec").style.height = "278px";
//			document.getElementById("tableFiltroEspec").style.display = "block";
//			document.getElementById("tableFiltroPadrao").style.display = "block";
//			document.getElementById("divFiltroPadrao").style.height = "230px";
			document.getElementById("divFiltroEspec").style.display = "";
			document.getElementById("divFiltroPadrao").style.display = "";
			
			// Chamado: 85553 - 31/12/2012 - Carlos Nunes
			document.getElementById("tdListaResultado").style.height = "166px";
			lstIndicacoes.document.getElementById("lstRegistro").style.height = "166px";
//			document.getElementById("bt1").style.top = "240px";
//			document.getElementById("bt2").style.top = "240px";
//			document.getElementById("bt3").style.top = "235px";
//			document.getElementById("bt1").style.left = "875px";
//			document.getElementById("bt2").style.left = "943px";
//			document.getElementById("bt3").style.left = "845px";
			document.getElementById("bt1").style.display = "";
			document.getElementById("bt2").style.display = "";
			document.getElementById("bt3").style.display = "";
			document.getElementById("imgMaisMenos").src= "webFiles/images/menuVert/mais01.gif";
			document.getElementById("imgMaisMenos").title = '<bean:message key="prompt.contrairFiltros"/>';
			statusLista = "max";
		}
		
		function maisMenos(){
			if(statusLista == "max"){
				try{
					min();
				}catch(e){
					max();
				}
			}else{
				try{
					max();
				}catch(e){
					min();
				}
			}
		}

		function atualizarCmbDesenhoprocesso()
	    {
	    	$("#cmbDesenhoprocesso").jsonoptions("LocalizadorAtendimento.do?idEmprCdEmpresa=" + ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value + "&acao=visualizar&tela=cmbDesenhoProcesso", "id_depr_cd_desenhoprocesso", "depr_ds_desenhoprocesso", null,function(ret) {});
	    	$('#cmbDesenhoprocesso').css('width', '305px');
	    }
	</script>

	<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" rightmasrgin="0" bottommargin="0" onload="inicio();">
		<html:form action="/LocalizadorAtendimento.do" styleId="localizadorAtendimento">

		<html:hidden property="localizadorAtendimentoVo.regDe" />
		<html:hidden property="localizadorAtendimentoVo.regAte" />

		<input type="hidden" name="madsInModulo" value="K"/>

			<input type="hidden" name="existeRegistroLista" value="true"><br>

		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			<html:hidden property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>
		<%}%>
			
			<div style="position: absolute; left: 0; top: 0; width: 100%">
				<table width="100%" cellpadding=0 cellspacing=0 border=0>
					<tr>
						<td width="20%" class="principalLabel" align="right">
							<bean:message key="prompt.empresa"/>:&nbsp;
						</td>
						<td width="30%">
							<iframe name="ifrmCmbEmpresa" id="ifrmCmbEmpresa" src="MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CMB_EMPRESA%>&csCdtbEmpresaEmpr=<%=empresaVo.getIdEmprCdEmpresa() %>" frameborder=0 height="20px"></iframe>
						</td>
						<td width="50%">&nbsp;</td>
					</tr>
				</table>
			</div>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
				<tr> 
					<td valign="top"> 
						<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
							<tr> 
								<td valign="top" height="1"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0" width="975">
										<tr> 
											<td class="principalPstQuadroLinkVazio2"> 
												<table border="0" cellspacing="0" cellpadding="0" width="975">
													<tr> 
														<td class="principalPstQuadroLinkSelecionado" id="tdFiltroPadrao" name="tdFiltroPadrao" onClick="AtivarPasta('FP')"> 
															<bean:message key="prompt.filtrosPadrao"/>
														</td>
													    <!-- Chamado: 95927 - 18/07/2014 - Carlos Nunes -->
														<td class="principalPstQuadroLinkNormal" id="tdFiltroEspec" name="tdFiltroEspec" onClick="AtivarPasta('FE')" style="display: none"> 
															<%= getMessage("prompt.filtrosEspec", request)%>
														</td>
													
													<td style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; text-decoration: none; height: 18px; " align="right"> 
														<img id="imgMaisMenos" name="imgMaisMenos" src="webFiles/images/menuVert/mais01.gif" title='<bean:message key="prompt.contrairFiltros" />' onClick="maisMenos()" class="geralCursoHand" width="15" height="15">
													</td>
													  </tr>
												</table>
											</td> 
										 </tr>
									</table>
									
									<div id="divFiltroPadrao" style="visibility: visible;">
										<table id="tableFiltroPadrao" width="100%" align="center" valign="top" border="0" cellspacing="1" cellpadding="1" class="principalBordaQuadro">
											<tr> 
												<td width="32%" class="principalLabel">
													<table cellpadding=0 cellspacing=0 border=0 width="100%">
														<tr height="5">
															<td class="principalLabel">
																<%= getMessage("prompt.manifestacao", request)%>
															</td>
			
													<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
															<td width="50%" class="principalLabel">Segmento</td>
													<%	}	%>
													
														</tr>
													</table>
												</td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td width="30%" class="principalLabel">
													<%//Chamado 100157 KERNEL-1098 QA - 20/04/2015 - Marcos Donato //%>
													<span id="labelManifestacao"><%= getMessage("prompt.grupomanif", request)%></span>
													<span id="labelEvento" style="display:none;"><%= getMessage("prompt.evento", request)%></span>
												</td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td class="principalLabel"><%= getMessage("prompt.tipomanif", request)%></td>
												<td width="3%" class="principalLabel">&nbsp;</td>
											</tr>
											<tr> 
												<td width="32%" height="23">
													<table cellpadding=0 cellspacing=0 border=0 width="100%">
														<tr><!--Jonathan | Adequa��o para o IE 10-->
															<td height="20px">
																<iframe id=ifrmCmbLocalManifTipo name="ifrmCmbLocalManifTipo" src="" width="100%" height="20px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
													
													<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
															</td>
															<td width="50%" height="20px"><!--Jonathan | Adequa��o para o IE 10-->
																<iframe name="ifrmCmbLocalSuperGrupo" id=ifrmCmbLocalSuperGrupo src="LocalizadorAtendimento.do?tela=cmbSuperGrupo&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="20px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
															</td>
													<%	}
														else{	%>
																<iframe name="ifrmCmbLocalSuperGrupo" id=ifrmCmbLocalSuperGrupo src="" width="0" height="0" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
															</td>
													<%	}	%>
													
														</tr>
													</table>
												</td>
												<td width="2%" class="principalLabel" height="23">&nbsp;</td>
												<td width="30%" height="23px">
													<%//Chamado 100157 KERNEL-1098 QA - 20/04/2015 - Marcos Donato //%>
								                	<iframe id="ifrmCmbLocalEventoFollowup" name="ifrmCmbLocalEventoFollowup" src="" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" style="display:none;" ></iframe>
													<iframe id=ifrmCmbLocalGrupoManif name="ifrmCmbLocalGrupoManif" src="" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
												</td>
												<td width="2%" class="principalLabel" height="23px">&nbsp;</td>
												<td height="23px"><!--Jonathan | Adequa��o para o IE 10-->
													<iframe id=ifrmCmbLocalTipoManif id="ifrmCmbLocalTipoManif" name="ifrmCmbLocalTipoManif" src="" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
												</td>
												<td width="3%" class="principalLabel" height="23px">&nbsp;</td>
											</tr>
											<tr>
											
									<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("N")) {%>
									<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
												
												<td width="32%">
													<table cellpading=0 cellspacing=0 border=0 width="100%">
														<tr>
															<td width="50%" class="principalLabel" style="vertical-align:bottom;">
																<%= getMessage("prompt.linha", request)%>
															</td>
															<td width="50%" align="right" class="principalLabel">
															
														<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
																<input type=checkbox name="chkAssunto" onclick="carregaProdAssunto();">
																<bean:message key="prompt.produto.assunto" />
														<%	}	%>
																	
															</td>
														</tr>
													</table>
												</td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td width="30%" class="principalLabel">
													<table cellpading=0 cellspacing=0 border=0 width="100%">
														<tr>
															<td class="principalLabel" width="95%" style="vertical-align:bottom;"><%=getMessage("prompt.prodserv", request)%></td>
															<td class="principalLabel" width="5%" >
																<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO4")) {	%>
																	<img id="imgCaractPras" src="webFiles/images/botoes/Visao16.gif" width="17" height="16" class="geralCursoHand" title='<bean:message key="prompt.visualiza_produto" />' onclick="visualizaProduto();">
																<% } %>
															</td>							                    																
														</tr>
												   </table>
												</td>										

									<%		} else {%>

												<td width="32%">
													<table cellpading=0 cellspacing=0 border=0 width="100%">
														<tr>
															<td width="50%" class="principalLabel" style="vertical-align:bottom;">
																<div style="heigth:23; width:80; visibility: hidden" >
																	<%= getMessage("prompt.linha", request)%>
																</div>
															</td>
															<td width="50%" align="right" class="principalLabel">
																<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
																	<input type=checkbox name="chkAssunto" onclick="carregaProdAssunto();">
																	<bean:message key="prompt.produto.assunto" />
																<%}%>
															</td>
														</tr>
													</table>
												</td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td width="30%" class="principalLabel">
													<table cellpading=0 cellspacing=0 border=0 width="100%">
														<tr>
															<td class="principalLabel" width="95%" style="vertical-align:bottom;"><%=getMessage("prompt.prodserv", request)%></td>
															<td class="principalLabel" width="5%" >
																<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO4")) {	%>
																	<img id="imgCaractPras" src="webFiles/images/botoes/Visao16.gif" width="17" height="16" class="geralCursoHand" title='<bean:message key="prompt.visualiza_produto" />' onclick="visualizaProduto();">
																<% } %>
															</td>							                    																
														</tr>
												   </table>		
												</td>
									<%		}	%>
									<%	}else{	%>
									<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
													
												<td colspan="3" width="64%" height="23">
													<table cellpading=0 cellspacing=0 border=0 width="100%">
														<tr height="23">
															<td width="17%" class="principalLabel" style="vertical-align:bottom;">
																<%= getMessage("prompt.linha", request)%>
															</td>
															<td width="16%" align="left" class="principalLabel" >
																<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
																	<input type=checkbox name="chkAssunto" onclick="carregaProdAssunto();">
																	<bean:message key="prompt.produto.assunto" />

																	<div id="divDescontinuado" class="principalLabel">
																		<input type=checkbox name="chkDecontinuado" onclick="carregaProdDescontinuado();">
																		<%= getMessage("prompt.descontinuado", request)%>
																	</div>								
																<%}%>
																
															</td>															
															
															<td width="28%" class="principalLabel" style="vertical-align:bottom;">
																&nbsp;<%= getMessage("prompt.assuntoNivel1", request)%>
															</td>
															<td class="principalLabel" width="5%" style="vertical-align:bottom;">
																<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO4")) {	%>
																	<img id="imgCaractPras" src="webFiles/images/botoes/Visao16.gif" width="17" height="16" class="geralCursoHand" title='<bean:message key="prompt.visualiza_produto" />' onclick="visualizaProduto();">
																<% } %>
															</td>							                    	
															<td width="33%" class="principalLabel" style="vertical-align:bottom;">
																<%= getMessage("prompt.assuntoNivel2", request)%>
															</td>
														</tr>
													</table>
												</td>		
																								
									<%		} else {	%>

												<td colspan="3" width="64%" height="23">
													<table cellpading=0 cellspacing=0 border=0 width="100%">
														<tr height="23">
															<td width="26%" class="principalLabel" style="vertical-align:bottom;">
																<%= getMessage("prompt.assuntoNivel1", request)%>
															</td>
															<td width="19%" align="left" class="principalLabel">
															
															<!--  valdeci -->																
																<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
																	<div id="divDescontinuado" class="principalLabel">
																		<input type=checkbox name="chkDecontinuado" onclick="carregaProdDescontinuado();">
																		<%= getMessage("prompt.descontinuado", request)%>
																	</div>								
																<%}%>
																
														<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
																<input type=checkbox name="chkAssunto" onclick="carregaProdAssunto();">
																<bean:message key="prompt.produto.assunto" />
														<%	}	%>

															</td>
															<td class="principalLabel" width="8%" style="vertical-align:bottom;">
																<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO4")) {	%>
																	<img id="imgCaractPras" src="webFiles/images/botoes/Visao16.gif" width="17" height="16" class="geralCursoHand" title='<bean:message key="prompt.visualiza_produto" />' onclick="visualizaProduto();">
																<% } %>
															</td>							                    	
															<td width="47%" class="principalLabel" style="vertical-align:bottom;">
																<%= getMessage("prompt.assuntoNivel2", request)%>
															</td>
														</tr>
													</table>
												</td>													
									<%		}	%>
									<%	}		%>

												<td width="2%" class="principalLabel">&nbsp;</td>
												<td class="principalLabel" style="vertical-align:bottom;"><bean:message key="prompt.periodo"/></td>
												<td width="3%" class="principalLabel">&nbsp;</td>
											</tr>
											<tr> 
											
									<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("N")) {%>
									<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
												
												<td width="32%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
													<iframe id=ifrmCmbLocalLinha name="ifrmCmbLocalLinha" src="LocalizadorAtendimento.do?tela=cmbLocalLinha&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
													<!-- input type="text" name="txtNome2" class="principalObjForm" onkeydown="pressEnter(event)"-->
												</td>
												<td width="2%" height="23px">&nbsp;</td>
												
												<td width="30%" height="23px">
													<iframe id=ifrmCmbLocalProduto name="ifrmCmbLocalProduto" src="LocalizadorAtendimento.do?tela=cmbLocalProduto" width="100%" height="23px%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
												</td>
													
									<%		} else {	%>
												
												<td width="32%" height="23">
													<div style="heigth:23; width:80; visibility: hidden" >
														<iframe id=ifrmCmbLocalLinha name="ifrmCmbLocalLinha" src="LocalizadorAtendimento.do?tela=cmbLocalLinha&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="0%" height="0%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
														<!-- input type="text" name="txtNome2" class="principalObjForm" onkeydown="pressEnter(event)"-->
													</div>
												</td>
												<td width="2%" height="23px">&nbsp;</td>
												
												<td width="30%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
													<iframe id=ifrmCmbLocalProduto name="ifrmCmbLocalProduto" src="LocalizadorAtendimento.do?tela=cmbLocalProduto" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
												</td>

									<%		}	%>		
									<%	}else{	%>
									<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
												
												<td colspan="3" width="64%" height="23">
													<table cellpading=0 cellspacing=0 border=0 width="100%">
														<tr height="23px"> 
															<td width="33%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
																<iframe id=ifrmCmbLocalLinha name="ifrmCmbLocalLinha" src="LocalizadorAtendimento.do?tela=cmbLocalLinha&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
																<!-- input type="text" name="txtNome2" class="principalObjForm" onkeydown="pressEnter(event)"-->
															</td>	
															<td width="33%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
																<iframe id=ifrmCmbLocalProduto name="ifrmCmbLocalProduto" src="LocalizadorAtendimento.do?tela=cmbLocalProduto" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
															</td>	
															<td width="33%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
																<iframe id=ifrmCmbLocalVariedade name="ifrmCmbLocalVariedade" src="LocalizadorAtendimento.do?tela=cmbLocalVariedade" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
															</td>
														</tr>
													</table>
												</td>												
												
									<%		} else {	%>
												
												<td colspan="3" width="64%" height="23px">
													<table cellpading=0 cellspacing=0 border=0 width="100%">
														<tr> 
															<td width="49%" height="23px">
																<div style="heigth:23px; width:80; display: none;" > <!--Jonathan | Adequa��o para o IE 10-->
																	<iframe id=ifrmCmbLocalLinha name="ifrmCmbLocalLinha" src="LocalizadorAtendimento.do?tela=cmbLocalLinha&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
																	<!-- input type="text" name="txtNome2" class="principalObjForm" onkeydown="pressEnter(event)"-->
																</div><!--Jonathan | Adequa��o para o IE 10-->
																<iframe id=ifrmCmbLocalProduto name="ifrmCmbLocalProduto" src="LocalizadorAtendimento.do?tela=cmbLocalProduto" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
															</td>
															<td width="4%" height="23px"></td>	
															<td width="47%" height="23px">
																<iframe id=ifrmCmbLocalVariedade name="ifrmCmbLocalVariedade" src="LocalizadorAtendimento.do?tela=cmbLocalVariedade" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
															</td>
														</tr>
													</table>
												</td>												

									<%		}	%>											
									<%	}		%>
												<td width="2%" height="23">&nbsp;</td>
												<td height="23">
												<!-- Danilo Prevides - 06/10/2009 - 66813 - INI  -->
													<html:select property="localizadorAtendimentoVo.periodo" styleClass="principalObjForm" onkeydown="pressEnter(event)">
									               		<!--<html:option value="0"><bean:message key="prompt.combo.sel.opcao"/></html:option>
									               		<html:option value="1"><bean:message key="prompt.DataDoChamado"/></html:option>
														<html:option value="2"><bean:message key="prompt.DataDePrevisaoDaManifestacao"/></html:option>
														<html:option value="3"><bean:message key="prompt.DataDeConclusaoDaManifestacao"/></html:option>
														<html:option value="4"><bean:message key="prompt.DataDeConclusaoDaPendencia"/></html:option>
														<html:option value="5"><bean:message key="prompt.DataDeCriacaoDaPendencia"/></html:option>
														<html:option value="6"><bean:message key="prompt.DataDeRespostaDaPendencia"/></html:option>
														<html:option value="7"><bean:message key="prompt.DataDePrevisaoDoFollowup"/></html:option>
														<html:option value="8"><bean:message key="prompt.DataDeCriacaoDoFollowup"/></html:option>
														<html:option value="9"><bean:message key="prompt.DataDeConclusaoDoFollowup"/></html:option>-->
									               	</html:select> 
										        <script type="text/javascript">
										        	carregaTodosCamposComboPeriodo();
										        </script>									               	
									               	
									            <!-- Danilo Prevides - 06/10/2009 - 66813 - FIM  -->
												</td>
											</tr>
											<tr> 
												<td width="32%" class="principalLabel"><bean:message key="prompt.area"/></td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td width="30%" class="principalLabel"><bean:message key="prompt.destinatario"/></td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td class="principalLabel">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
															<td width="41%" class="principalLabel"><bean:message key="prompt.datainicial"/></td>
															<td width="9%" class="principalLabel">&nbsp;</td>
															<td width="50%" class="principalLabel"><bean:message key="prompt.datafinal"/></td>
														</tr>
													</table>
												</td>
												<td width="3%" class="principalLabel">&nbsp;</td>
											</tr>
											<tr> 
												<td width="32%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
													<iframe id=ifrmCmbLocalArea name="ifrmCmbLocalArea" src="" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
												</td>
												<td width="2%">&nbsp;</td>
												<td width="30%" height="23px">
													<iframe id=ifrmCmbLocalDest name="ifrmCmbLocalDest" src="LocalizadorAtendimento.do?tela=cmbLocalDest&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
												</td>
												<td width="2%">&nbsp;</td>
												<td height="23px">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
															<td width="41%"> 
																<input type="text" name="txtDhInicio" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" maxlength="10" class="principalObjForm">
															</td>
															<td width="8%">
																&nbsp;<img src="webFiles/images/botoes/calendar.gif" width="16" onclick="show_calendar('getElementById(\'localizadorAtendimento\').txtDhInicio')" class="geralCursoHand" height="15" title="<bean:message key='prompt.calendario' />">
															</td>
															<td width="41%"> 
																<input type="text" name="txtDhFim" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" maxlength="10" class="principalObjForm">
															</td>
															<td width="8%">
																&nbsp;<img src="webFiles/images/botoes/calendar.gif" onclick="show_calendar('getElementById(\'localizadorAtendimento\').txtDhFim')" class="geralCursoHand" width="16" height="15" title="<bean:message key='prompt.calendario' />">
															</td>
														</tr>
													</table>
												</td>
												<td width="3%">&nbsp;</td>
											</tr>
											<tr> 
												<td width="32%" class="principalLabel"><bean:message key="prompt.manifstatus"/></td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td width="30%" class="principalLabel"><bean:message key="prompt.manifsituacao"/></td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td class="principalLabel" height="20px">
													<bean:message key="prompt.atendente"/><!--Jonathan | Adequa��o para o IE 10-->
													<div id="divLabelLocalEspec" name="divLabelLocalEspec" style="position:absolute; width:0px; height:0px; z-index:110; visibility: hidden">
														<iframe id="camposEspecLabel"
																name="camposEspecLabel" 
																src="<%= Geral.getActionProperty("labelLocalEspec", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_CMB_LOCAL_SPEC_LABEL%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" 
																width="100%" 
																height="20px" 
																scrolling="Default" 
																frameborder="0" 
																marginwidth="0" 
																marginheight="0" >
														</iframe>
													</div>
												</td>
												<td width="3%" class="principalLabel">&nbsp;</td>
											</tr>
											<tr> 
												<td width="32%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
													<iframe id=cmbStatusManif name="cmbStatusManif" src="" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
												</td>
												<td width="2%">&nbsp;</td>
												<td width="30%" height="23px">
													<iframe id=cmbClassifManif name="cmbClassifManif" src="" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
												</td>
												<td width="2%">&nbsp;</td>
												<td height="23px"><!--Jonathan | Adequa��o para o IE 10-->
													<iframe id=IfrmCmbLocalAtend name="IfrmCmbLocalAtend" src="" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
													<div id="divCamposLocalEspec" name="divCamposLocalEspec" style="position:absolute; width:0px; height:0px; z-index:112; visibility: hidden">
														<iframe id="camposEspec"
																name="camposEspec" 
																src="<%= Geral.getActionProperty("camposLocalEspec", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_CMB_LOCAL_SPEC%>&acao=<%=Constantes.ACAO_VISUALIZAR%>"
																width="100%" 
																height="23px" 
																scrolling="Default" 
																frameborder="0" 
																marginwidth="0" 
																marginheight="0" >
														</iframe>
													</div>
												</td>
												<td width="3%">&nbsp;</td>
											</tr>
											<tr> 
												<td width="32%">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
									                    <tr> 
									                      <td width="48%" class="principalLabel"><bean:message key="prompt.filtroMesa"/></td>
									                      <td width="4%" class="principalLabel">&nbsp;</td>
									                      <td width="48%" class="principalLabel"><bean:message key="prompt.StatusPendencia"/></td>
									                    </tr>
								                    </table>
												</td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td width="30%" class="principalLabel"><bean:message key="prompt.nome"/></td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td class="principalLabel" colspan="2">
								                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
									                    <tr> 
									                      <td width="15%" class="principalLabel"><%= getMessage("prompt.chamado", request)%></td>
									                      <td width="5%" class="principalLabel">&nbsp;</td>
									                      <td width="25%" class="principalLabel">&nbsp;&nbsp;&nbsp;<bean:message key="prompt.PorcentagemChecklist"/></td>
									                      <td width="45%" class="principalLabel">&nbsp;</td>
									                    </tr>
								                    </table>
								                </td>
											</tr>
											<tr> 
	                							<td width="32%" height="23"> 
	                								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									                    <tr> 
									                      <td width="48%" class="principalLabel">
									                      	<%
									                      	//CHAMADO 74100 - VINICIUS - O COMBO FOI TRANSFORMADO EM UM OBJETO (JAVA) PARA QUE SEJA SETADO O VALOR PELO FORM 
									                      	%>
									                      	<html:select property="localizadorAtendimentoVo.idPendencia" styleClass="principalObjForm">
																<html:option value="2"><bean:message key="prompt.pendenteResposta" /></html:option>
																<html:option value="1"><bean:message key="prompt.pendenteConclusao" /></html:option>
																<html:option value="3"><bean:message key="prompt.Concluido" /></html:option>
																<html:option value="4"><bean:message key="prompt.todos" /></html:option>
															</html:select>
														  </td>
									                      <td width="4%" class="principalLabel">&nbsp;</td>
									                      <td width="48%" class="principalLabel">
								                      		<html:select property="localizadorAtendimentoVo.codStatusPendencia" styleClass="principalObjForm">
								                      			<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
								                      			<logic:present name="statusPendenciaVector">
								                      				<html:options collection="statusPendenciaVector" property="field(id_stpd_cd_statuspendencia)" labelProperty="field(stpd_ds_statuspendencia)"/>
								                      			</logic:present>
								                      		</html:select>
									                      </td>
									                    </tr>
								                    </table>
												</td>
												<td width="2%">&nbsp;</td>
												<td width="30%" height="23">
													<input type="text" name="txtNome" class="principalObjForm" maxlength="80" onkeydown="pressEnter(event)">
												</td>
												<td width="2%">&nbsp; </td>
												<td colspan="2"> 
								                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
								                    <tr> 
								                      <td width="15%"> 
								                      	<input type="text" name="txtIdCham" onkeydown="pressEnter(event)" onkeypress="isDigito(this)" maxlength="15" class="principalObjForm">
								                      </td>
								                      <td width="5%">&nbsp;</td>
								                      <td width="10%">
								                      	<html:text property="localizadorAtendimentoVo.porcentagemCheckListDe" styleClass="principalObjForm" maxlength="3" onkeydown="pressEnter(event)" onkeypress="isDigito(this)" />
													  </td>  
													  <td width="5%">&nbsp;</td>
													  <td width="10%">
								                      	<html:text property="localizadorAtendimentoVo.porcentagemCheckListAte" styleClass="principalObjForm" maxlength="3" onkeydown="pressEnter(event)" onkeypress="isDigito(this)" />
													  </td>     
													  <td width="45%" class="principalLabel">&nbsp;</td>               
								                    </tr>
								                  </table>
								                </td>
											</tr>
																						
											<tr> 
												<td width="32%" class="principalLabel"><bean:message key="prompt.Desenho"/></td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td width="30%" class="principalLabel"><bean:message key="prompt.Etapa"/></td>
												<td width="2%" class="principalLabel">&nbsp;</td>
												<td class="principalLabel" height="20">
													&nbsp;
												</td>
												<td width="3%" class="principalLabel">&nbsp;</td>
											</tr>
											<tr> 
												<td width="32%" height="23">
													<html:select styleId="cmbDesenhoprocesso" property="localizadorAtendimentoVo.idDeprCdDesenhoprocesso" styleClass="principalObjForm" onchange="atualizarCmbEtapa()">
						                      			<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						                      			<logic:present name="csCdtbDesenhoprocessoDeprVector">
						                      				<html:options collection="csCdtbDesenhoprocessoDeprVector" property="field(id_depr_cd_desenhoprocesso)" labelProperty="field(depr_ds_desenhoprocesso)"/>
						                      			</logic:present>
						                      		</html:select>
												</td>
												<td width="2%">&nbsp;</td>
												<td width="30%" height="23px">
													<iframe id=cmbEtapa name="cmbEtapa" src="" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
												</td>
												<td width="2%">&nbsp;</td>
												<td>
												</td>
												<td width="3%">&nbsp;</td>
											</tr>											
											
											<tr height="5"> 
												<td class="principalLabel"></td>
											</tr>
											<tr height="5"> 
												<td class="principalLabel"></td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr> 
												<td class="principalLabel">&nbsp; </td>
											</tr>
										</table>
									</div>

									<div id="bt3" style="position:absolute; left:845px; top:235px; width:25px; height:25px; z-index:100; visibility: visible">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr> 
												<td width="15%" align="right">
													<img id="bntResposta" name="bntResposta" src="webFiles/images/botoes/RetornoCorresp.gif" onclick="carregaTelaResposta()" class="geralCursoHand" title="<%= getMessage("prompt.resposta.lote", request)%>" width="25">&nbsp;
													<script>
														if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_RESPOSTA_PERMISSAO_CHAVE%>')){
															document.all.item("bntResposta").disabled = true;
						 									document.all.item("bntResposta").className = 'geralImgDisable';
														}
													</script>
												</td>
											</tr>
										</table>
									</div>
									
									<div id="bt1" style="position:absolute; left:880px; top:240px; width:50px; height:35px; z-index:100; visibility: visible">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr> 
												<td width="100%" align="center" valign="bottom"><img src="webFiles/images/icones/acao.gif" onclick="limparFiltro()" class="geralCursoHand" title="<bean:message key="prompt.limparFiltro"/>"></td>
											</tr>
										</table>
									</div>
                                    <!-- Chamado: 91288  - 09/10/2013 - Carlos Nunes -->
									<div id="bt2" style="position:absolute; left:943px; top:240px; width:25px; height:25px; z-index:101; visibility: visible">
										<img id="btnFiltro" src="webFiles/images/botoes/setaDown.gif" onclick="window.document.getElementById('localizadorAtendimento')['localizadorAtendimentoVo.regDe'].value='0';window.document.getElementById('localizadorAtendimento')['localizadorAtendimentoVo.regDe'].value='0';initPaginacao();carregaListaAtend()" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.buscar"/>">
									</div>
										
									<div id="divFiltroEspec" style="position:absolute; left:5px; top:38px; width:974px; height:278px; z-index:1; visibility: hidden">
	            						<table id="tableFiltroEspec" width="100%" height="100%" class="principalBordaQuadro">
											<tr>
												<td>
													 <iframe name="localAtendEspec" src="" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>	
												</td>												
											</tr>
										</table>
									</div>

									<!-- 
									Valdeci 26-05-2006 
									Chamado: 19506 , Item 4
									Pagina alterada para deixar imprimir todo o conteudo da lista de pendencias	
									-->
									<!-- <div id="tituloLocalizadorAtend" style="margin-left:5px; scroll: no; overflow: hidden;; height: 20; width: 940"> -->
									 <!-- Chamado: 91506 - 24/10/2013 - Carlos Nunes -->
									 <div id="tituloLocalizadorAtend" style='<bean:write name="configuracoesGerais" property="field(titulo_localizador_atend_style)" />'>
								    	 <table id="tabelaTitulo" width='<bean:write name="configuracoesGerais" property="field(tabela_titulo_width)" />' border="0" cellspacing="0" cellpadding="0" class="sortable" style="cursor: pointer;">
											<tr>
												<td class="sorttable_nosort principalLstCab" width="65px"><img src="webFiles/images/icones/bt_selecionar_nao.gif" onclick="lstIndicacoes.boxSelecao(false);" title="<bean:message key='prompt.desmarcar'/>" class="geralCursoHand"><img src="webFiles/images/icones/bt_selecionar_sim.gif" onclick="lstIndicacoes.boxSelecao(true);" title="<bean:message key='prompt.marcar'/>" class="geralCursoHand"></td>															
												
												<logic:present name="colunaList">
								                   <logic:iterate id="coluna" name="colunaList">
								                      <td class='<bean:write name="coluna" property="field(css)" />' width='<bean:write name="coluna" property="field(width)" />'><bean:write name="coluna" property="field(label)" /></td>
								                   </logic:iterate>
								                </logic:present>
				
												<td id="classificacao" class="principalLstCab" width="0px">&nbsp;</td>
											</tr>
										</table> 
									</div>
                                    <!-- Chamado: 91506 - 24/10/2013 - Carlos Nunes -->
									<div id="tituloLocalizadorAtendEscondido" style='<bean:write name="configuracoesGerais" property="field(titulo_localizador_atend_escondido_style)" />'>
								    	 <table id="tabelaTitulo" width='<bean:write name="configuracoesGerais" property="field(tabela_titulo_width)" />' border="0" cellspacing="0" cellpadding="0" class="sortable" style="cursor: pointer;">
											<tr>
												<td class="sorttable_nosort principalLstCab" width="65px"><img src="webFiles/images/icones/bt_selecionar_nao.gif" onclick="lstIndicacoes.boxSelecao(false);" title="<bean:message key='prompt.desmarcar'/>" class="geralCursoHand"><img src="webFiles/images/icones/bt_selecionar_sim.gif" onclick="lstIndicacoes.boxSelecao(true);" title="<bean:message key='prompt.marcar'/>" class="geralCursoHand"></td>															
												
												<logic:present name="colunaList">
								                   <logic:iterate id="coluna" name="colunaList">
								                      <td class='<bean:write name="coluna" property="field(css)" />' width='<bean:write name="coluna" property="field(width)" />'><bean:write name="coluna" property="field(label)" /></td>
								                   </logic:iterate>
								                </logic:present>
								                
												<td id="classificacao" class="principalLstCab" width="0px">&nbsp;</td>
											</tr>
										</table> 
									</div>
										
									<div id="listaLocalizadorAtend" style="margin-left:0px; overflow: no; width: 970;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
											<tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
												<td id="tdListaResultado" height="166px" valign="top">
													<!--Inifio Iframe Lst Indica��es -->
													<iframe name="lstIndicacoes" src="LocalizadorAtendimento.do?tela=lstLocalizadorAtend" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
													<!--Final Iframe Lst Indica��es -->
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" width="100%">
				<tr> 
					<td class="esquerdoBgrPageIFRM" style="padding:0px;">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td width="40%" class="principalLabel"> </td>
								<td class="principalLabel" width="20%" colspan="3">
						    		<%@ include file = "/webFiles/includes/funcoesPaginacaoMesa.jsp" %>	    		
				    			</td>
								<td width="40%" class="principalLabel" align="right">									
                 		 			<div>
    									<% // Chamado 81376 - 22/11/2013 - Jaider Alba %>										 
										<span style="margin:9px 0px 0px 180px; float:left;" id="bntExportar" name="bntExportar" class="xls-jpg<%=(isW3c)?"64":""%>" title="<bean:message key='prompt.exportar_xls' />" onclick="carregaListaAtend(true)"></span>
										<label class="geralCursoHand" onClick='carregaListaAtend(true)' title="<bean:message key='prompt.exportar_xls' />" >
	                 		 				<font face="Arial, Helvetica, sans-serif" size="1">
		                 		 				<bean:message key='prompt.exportar' />
		                 		 			</font>
		                 		 		</label>										 
										<iframe name="<%=MCConstantes.IFRM_EXPORTA_XLS%>" style="display: none;"></iframe>             		 			
	                 		 			
	                 		 			<img style="margin-left:20px;" src="webFiles/images/icones/impressora.gif" width="22" height="22" class="geralCursoHand" title="<bean:message key="prompt.imprimir" />" onclick="imprimir()">
	                 		 			<label class="geralCursoHand" onClick='imprimir()' title="<bean:message key='prompt.imprimir' />" >
	                 		 				<font face="Arial, Helvetica, sans-serif" size="1">
		                 		 				<bean:message key="prompt.imprimir" />
		                 		 			</font>
		                 		 		</label>
									                 		 		
	                 		 			<img style="margin-left:20px;" src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" class="geralCursoHand" title="<bean:message key='prompt.sairSistema'/>" onclick="FechaJanela()">
	                 		 			<label id="lblSair" class="geralCursoHand" onClick='FechaJanela()' title="<bean:message key='prompt.sair' />" >
	                 		 				<font face="Arial, Helvetica, sans-serif" size="1">
	                 		 					<bean:message key="prompt.sair"/>
	               		 					</font>
	             		 				</label>
             		 				</div>
                 		 		</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			
		<logic:present name="voPeriodo">
	  		<script>localizadorAtendimento['localizadorAtendimentoVo.periodo'].value = <bean:write name="voPeriodo" property="field(periodo)"/></script>
	    </logic:present>
  
		</html:form>
	</body>
</html>

<script type="text/javascript" src="webFiles/js/funcoesMozilla.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>