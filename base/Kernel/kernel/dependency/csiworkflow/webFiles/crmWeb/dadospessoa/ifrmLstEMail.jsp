<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmLstEMail</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="listForm">

</head>
<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalLstCab" id="cab10" name="cab10" width="2%">&nbsp;</td>
    <td class="principalLstCab" id="cab11" name="cab11" width="83%"><bean:message key="prompt.email" /></td>
    <td class="principalLstCab" id="cab12" name="cab12" width="15%"><bean:message key="prompt.principal" /></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td valign="top"> 
      <div id="lstEmail" style="width:100%; height:100%; overflow-y: auto; overflow-x: no"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <script>try {window.top.superiorBarra.barraEmail.mailPessoa.innerText = '';} catch(e) {}</script>
        <logic:iterate name="listVector" id="Email" indexId="numero" >	
		<table name="<bean:write name="numero"/>" id="<bean:write name="numero"/>" width=100% border=0 cellspacing=0 cellpadding=0>
			<tr class="intercalaLst<%=numero.intValue()%2%>">
				<td class="principalLstPar" width="83%">
				  &nbsp;<script>acronym('<bean:write name="Email" property="pcomDsComplemento"/>', 35);</script>
				  <input type="hidden" name="txtEmail" value="<bean:write name="Email" property="pcomDsComunicacao"/>">
				</td>
				<td class="principalLstPar" width="15%">
				  &nbsp;
				  <input type="hidden" name="cPrincipal<bean:write name="numero"/>" value="<bean:write name="Email" property="pcomInPrincipal"/>">
				  <input type="hidden" name="cEmail<bean:write name="numero"/>" value="<bean:write name="Email" property="pcomDsComplemento"/>">
				  <logic:equal property="pcomInPrincipal" name="Email" value="true">
					<img src="webFiles/images/icones/check.gif" width=11 height=12>
                    <script>try {window.top.superiorBarra.barraEmail.mailPessoa.innerHTML = acronymLst('<bean:write name="Email" property="pcomDsComplemento" />', 25);} catch(e) {}</script>
				  </logic:equal>
				</td> 
			</tr> 
		</table> 
        </logic:iterate>
        </table>
      </div>
    </td>
  </tr>
</table>
</body>
</html:form>
</html>