<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">

	function cmbTipoPublico_onChange(){
	
		//Exibindo as funções extras associadas ao tipo de público selecionado 
		if(pessoaForm.idTpPublico.value > 0){
			parent.ifrmMultiEmpresa.document.location = "MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico="+ pessoaForm.idTpPublico.value +"&idEmprCdEmpresa="+ parent.parent.parent.ifrmTelaFiltro.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
			//parent.ifrmMultiEmpresa.document.location = 'MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico=' + pessoaForm.publico.value;
		}
	
	}

	function inicio(){
		cmbTipoPublico_onChange();
	}

	function retornaValor(){

		if(pessoaForm.idTpPublico.value == '-1')
			return '';
		else
			return pessoaForm.idTpPublico.value;
	}

</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="pessoaForm">
	<html:select property="idTpPublico" styleId="idTpPublico" styleClass="principalObjForm" disabled="true">
		<html:option value='-1'>&nbsp;</html:option>
		<html:options collection="comboVector" property="idTppuCdTipoPublico" labelProperty="tppuDsTipoPublico"/>
	</html:select>
	
<script>
function disab() {
	for (x = 0;  x < pessoaForm.elements.length;  x++) {
		Campo = pessoaForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}
}

document.pessoaForm.idTpPublico.value = parent.idtppublicoAux;
cmbTipoPublico_onChange();

</script>
</html:form>
</body>
</html>
