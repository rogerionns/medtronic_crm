<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
	<title>..: <bean:message key="prompt.perfilup" /> :..</title>
	<frameset rows="*">
		<frame name="mainFrame" id="mainFrame" src="Perfil.do?idPessCdPessoa=<bean:write name="baseForm" property="idPessCdPessoa"/>&tela=<%= MCConstantes.TELA_PERFIL %>" scrolling="auto" />
	</frameset>
</head>
</html>