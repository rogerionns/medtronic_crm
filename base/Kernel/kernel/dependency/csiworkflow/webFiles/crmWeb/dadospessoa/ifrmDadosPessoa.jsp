<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.form.PessoaForm,com.iberia.helper.Constantes, br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesPessoa.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>' />
<bean:write name="funcoesPessoa" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<!-- script language='javascript' src='webFiles/javascripts/TratarDados.js'></script-->
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>	
<script language="JavaScript">

//Guarda o número de abas dinâmicas
var numAbasDinamicas = new Number(0);

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// 
parent.parent.document.getElementById("aguarde").style.visibility = "visible";
var bEnvia = true;
bSemPermissao=false;
var countPublico=0;

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function Link(click1,click2){
	window.parent.document.all.item("dadoPessoa").src = click1
	window.parent.all.item("complemento").src = click2

}


function MM_showHideLayers() { //v3.0
	var i, p, v, obj, args = MM_showHideLayers.arguments;
	for (i = 0; i < (args.length - 1); i += 2)
		document.getElementById(args[i]).style.display = args[i + 1];
}

function  Reset(){
	document.formulario.reset();
	return false;
}



function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

/*
Este metodo tem como objetivo receber todos os campos mais os parametros para as bustituicoes
*/

function obterLink(link, parametros, idBotao){
//parametros[0...][1] - paboDsParametrobotao
//parametros[0...][2] - paboDsNomeinterno
//parametros[0...][3] - paboDsParametrointerno
//parametros[0...][4] - paboInObrigatorio

	var valor = "";
	var newLink = link;
	var encontrouParametro = false;
	
	for (var i = 1; i < parametros.length; i++){
		if (newLink.indexOf('?') == -1){
			newLink = newLink + "?";
		}
		
		var ultimoCaracter = newLink.substring(newLink.length - 1);
		if (ultimoCaracter != "?" && ultimoCaracter != "&"){
			newLink = newLink + "&";
		}
		
		try{			
			//onde obter a informacao
			valor = "";
			if(parametros[i][2] == "perm"){
				valor = findPermissoesByFuncionalidade("adm.fc." + idBotao + ".");
			}else{
				var obj = parametros[i][2];
				var campo = parametros[i][3];
				
				//Verificando onde obter a informacao
				if( obj == 'idPessCdPessoa' ) {
					campo = "window.document.pessoaForm.idPessCdPessoa.value";
				}
				if( obj == 'pessCdCorporativo' ) {
					campo = "window.document.pessoaForm.pessCdCorporativo.value";
				}
				if( obj == 'idTppuCdTppublico' ) {
					<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
						campo = "parent.idTppuCdTipopublico";
					<%}else{%>
						campo = "document.pessoaForm.lstTpPublico[1].value";
					<%}%>
				}

				//Danilo Prevides - 19/11/2009 - 67546 - INI
				if (obj == 'pessDsCpf'){
					campo = "window.document.pessoaForm['pessDsCpf'].value";
				}	
				//Danilo Prevides - 19/11/2009 - 67546 - FIM
				
				// UNIMED *****************
				if( obj == 'tpPesquisa' ) {
					campo = "window.document.pessoaForm['csCdtbPessoaespecPeesVo.campoAux1'].value";
				}
				//*************************
				
				valor = eval(campo); 
				encontrouParametro = true;
			}
		}catch(e){
			encontrouParametro = false;
		}
		
		
		//Se o parametro e obrigatorio
		if (parametros[i][4] == 'S'){

			if (parametros[i][2] == ""){
				alert("<bean:message key='prompt.naoFoiPossivelObterNomeInternoParametroObrigatorio'/>"+ " " + parametros[i][1]);
				return "";
			}


			if (valor == "" || valor == "0"){
				alert("<bean:message key="prompt.alert.parametroObrigatorio"/>");
				return "";
			}

		}
		

		if (valor != null && encontrouParametro){
			newLink = newLink + parametros[i][2] + "=" + valor;
		}

	}
	
	return newLink; 
}


function AtivarPasta(pasta) {
	
	//Se a aba que estiver tentando visualizar for de fun��o extra, exibe o div de func. extra
	if(pasta.substring(0, 3) == "aba"){
		document.getElementById("iframes").style.display = "block";
	}
	else{
		document.getElementById("iframes").style.display = "none";
	}

	switch (pasta) {
		case 'ENDERECO':
			MM_showHideLayers('endereco','block','Complemento','none','Fisica','none','Juridica','none','Banco','none','divTpPublico','none');
			SetClassFolder('tdendereco','principalPstQuadroLinkSelecionado');
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');
			break;
		case 'DADOSCOMPLEMENTARES':
		
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
				MM_showHideLayers('Complemento','block','endereco','none','Banco','block','divTpPublico','block');
			<%}else{%>
				MM_showHideLayers('Complemento','block','endereco','none','Banco','block','divTpPublico','none');
			<%}%>
			
			verificaFisicaJuridica();

			SetClassFolder('tdendereco','principalPstQuadroLinkNormal');
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkSelecionadoMAIOR');	
			break;
		
		default : 
				MM_showHideLayers('endereco','none','Complemento','none','Fisica','none','Juridica','none','Banco','none','divTpPublico','none');
				SetClassFolder('tdendereco','principalPstQuadroLinkNormal');	
				SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');	
				SetClassFolder(pasta, 'principalPstQuadroLinkSelecionadoMAIOR');
	}
	ativarAbasDinamicas(pasta);
}

function ativarAbasDinamicas(pasta) {
	var numAba = pasta.substring(3);	
	try {
		for (i = 0; i < numAbasDinamicas; i++) {
			if (i == eval(numAba)) {
				objIfrm = document.getElementById("ifrm" + i);
				
				link = objIfrm.src;

				var pos = link.indexOf('idPessCdPessoa=');
				if (pos >= 0) {
					link = link.replace("idPessCdPessoa=", "idPessCdPessoa=" + pessoaForm.idPessCdPessoa.value);
				}
				
				var pos2 = link.indexOf('pessCdCorporativo=');
				if (pos2 >= 0) {
					link = link.replace("pessCdCorporativo=", "pessCdCorporativo=" + pessoaForm.pessCdCorporativo.value);
				}
				objIfrm.location = link;
				
				MM_showHideLayers('div' + i,'block'); 
			}
			else {
				MM_showHideLayers('div' + i,'none');
				SetClassFolder('aba' + i , 'principalPstQuadroLinkNormalMAIOR');
			}
		}
	} catch(e) {
		for (i = 0; i < numAbasDinamicas; i++) {
			MM_showHideLayers('div' + i,'none');
			SetClassFolder('aba' + i , 'principalPstQuadroLinkNormalMAIOR');
		}
	}
	
	try{
		var iframeEspec = eval("ifrm" + numAba);
		iframeEspec.funcaoAbaEspec();
	}catch(e){}
	
}

function getEstadoCivil() { 
	pessoaForm.idEsciCdEstadocil.value = ifrmCmbEstadoCivil.document.pessoaForm.idEsciCdEstadocil.value;
}

function getTipoPublico(){
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("N")) {	%>
		var str= "<input type=\"hidden\" name=\"lstTpPublico\" value=\"" + ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value + "\" >";
		document.getElementById("divLstTpPublico").innerHTML = str;	
		//pessoaForm.idTpPublico.value = ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value;
	<%}%>
}

function getForma(){
	pessoaForm.idTratCdTipotratamento.value = CmbFrmTratamento.document.pessoaForm.idTratCdTipotratamento.value;
}

function getGenisys(){
	pessoaForm.idCoreCdConsRegional.value = ifrmDadosAdicionais.cmbDadosConsReg.cmbDadosConsReg.idCoreCdConsRegional.value;
	pessoaForm.consDsConsRegional.value = ifrmDadosAdicionais.dadosAdicionais.consDsConsRegional.value;
	pessoaForm.consDsUfConsRegional.value = ifrmDadosAdicionais.dadosAdicionais.consDsUfConsRegional.value;
	pessoaForm.pessCdInternetId.value = ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.senhaDadosAdicionais.ctpeNrNumeroPedido.value;
	pessoaForm.pessCdInternetPwd.value = ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.senhaDadosAdicionais.consCdInternetPwd.value;
}

function getDadosAdicionais() {

	if (pessoaForm.pessCdBanco.value.lenght == 0){
		pessoaForm.pessCdBanco.value = cmbBanco.dadosAdicionaisForm.pessCdBanco.value;
		pessoaForm.pessDsBanco.value = cmbBanco.dadosAdicionaisForm.pessDsBanco.value;
	}
	
	if (pessoaForm.pessCdAgencia.value.length == 0){
		pessoaForm.pessCdAgencia.value = cmbAgencia.document.dadosAdicionaisPessForm.pessCdAgencia.value;
		pessoaForm.pessDsAgencia.value = cmbAgencia.document.dadosAdicionaisPessForm.pessDsAgencia.value;
	}	
	
	pessoaForm.pessDsCodigoEPharma.value = ifrmDadosAdicionais.dadosAdicionais.pessDsCodigoEPharma.value;
	pessoaForm.pessDsCartaoEPharma.value = ifrmDadosAdicionais.dadosAdicionais.pessDsCartaoEPharma.value;
	pessoaForm.pessCdInternetAlt.value = ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.senhaDadosAdicionais.pessCdInternetAlt.value;
	pessoaForm.pessInColecionador.value = ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.senhaDadosAdicionais.pessInColecionador.checked;
	pessoaForm.consDsCodigoMedico.value = ifrmDadosAdicionais.dadosAdicionais.consDsCodigoMedico.value;

	especialidadeHidden.innerHTML = '';
	if (ifrmDadosAdicionais.dadosAdicionais.idEspeCdEspecialidadeArray != null) {
		if (ifrmDadosAdicionais.dadosAdicionais.idEspeCdEspecialidadeArray.length == undefined) {
			especialidadeHidden.innerHTML = '<input type="hidden" name="idEspeCdEspecialidade" value="' + ifrmDadosAdicionais.dadosAdicionais.idEspeCdEspecialidadeArray.value + '">';
		} else {
			for (var i = 0; i < ifrmDadosAdicionais.dadosAdicionais.idEspeCdEspecialidadeArray.length; i++) {
				especialidadeHidden.innerHTML += '<input type="hidden" name="idEspeCdEspecialidade" value="' + ifrmDadosAdicionais.dadosAdicionais.idEspeCdEspecialidadeArray[i].value + '"> ';
			}
		}
	}
}

function getCamposEspecificos(){
	for (var i = 0; i < numAbasDinamicas; i++){
		var iframeEspec = eval("ifrm" + i);	
		var isFormEspec = false;
		try{
			isFormEspec = iframeEspec.isFormEspec();			
		}catch(e){}
		
		if (isFormEspec){
			var validaCamposEspec = false;			
			validaCamposEspec = iframeEspec.validaCamposEspec();
			if (!validaCamposEspec){
				break;
			}else{
				iframeEspec.setValoresToForm(document);
			}
		}			
	}
}

function Save(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados" />")) {
	if (!bEnvia) {
		return false;
	}
	bEnvia = false;

	if(validate(true)){
		getCamposEspecificos();
			
		getTipoPublico();
		getForma();
		getGenisys();
		getDadosAdicionais();
		getEstadoCivil();
		truncaCampos();
		parent.parent.document.getElementById("aguarde").style.visibility = "visible";
		pessoaForm.submit();
	} else {
		bEnvia = true;
	}
  }
}

function truncaCampos() {
  textCounter(pessoaForm.consDsConsRegional, 10);
  textCounter(pessoaForm.consDsUfConsRegional, 2);
  textCounter(pessoaForm.pessCdInternetId, 10);
  textCounter(pessoaForm.pessCdInternetPwd, 10);
  textCounter(pessoaForm.pessDsBanco, 50);
  textCounter(pessoaForm.pessCdBanco, 10);
  textCounter(pessoaForm.pessCdAgencia, 10);
  textCounter(pessoaForm.pessDsAgencia, 50);
  textCounter(pessoaForm.pessDsCodigoEPharma, 30);
  textCounter(pessoaForm.pessDsCartaoEPharma, 40);
  textCounter(pessoaForm.pessCdInternetAlt, 40);
  textCounter(pessoaForm.pessInColecionador, 1);
  textCounter(pessoaForm.consDsCodigoMedico, 15);
  textCounter(pessoaForm.pessNmPessoa, 80);
  textCounter(pessoaForm.pessNmApelido, 60);
}

function Fechar(){
	novo();
}

/**
 * Funcaoo executada no inicio da pagina.
 * e realizado um controle para caso algum objeto da tela nao tenha sido carregado.
 *### ATENCAO: se for alterar esta funcao, comente o try catch para visualizar os possiveis erros!!!
 */
var controleCarregaLoad = 0;
function carregaLoad() {

	showError('<%=request.getAttribute("msgerro")%>');
	parent.parent.document.getElementById("aguarde").style.visibility = "hidden";

	try{

		if(pessoaForm.idTpdoCdTipodocumento.value > 0){
			pessoaForm.cmbTipoDocumentoPF.value = pessoaForm.idTpdoCdTipodocumento.value;
		}
		pessoaForm.txtDocumentoPF.value = pessoaForm.pessDsDocumento.value;
		pessoaForm.txtDataEmissaoPF.value =pessoaForm.pessDhEmissaodocumento.value;
		if(pessoaForm.idTpdoCdTipodocumento.value > 0){
			pessoaForm.cmbTipoDocumentoPJ.value = pessoaForm.idTpdoCdTipodocumento.value;
		}

		pessoaForm.txtDocumentoPJ.value = pessoaForm.pessDsDocumento.value;
		pessoaForm.txtDataEmissaoPJ.value = pessoaForm.pessDhEmissaodocumento.value;

		if(document.pessoaForm.idTpPublico.value > 0){
			ifrmCmbTipoPub.pessoaForm.idTpPublico.value = document.pessoaForm.idTpPublico.value;
			ifrmCmbTipoPub.cmbTipoPublico_onChange();
			objPublico = document.pessoaForm.idLstTpPublico;
			if (objPublico != null){
				if(objPublico.length != null){
					for (nNode=0;nNode<objPublico.length;nNode++) {
						if (objPublico[nNode].value == document.pessoaForm.idTpPublico.value) {
							objPublico[nNode].checked=true;
						}else{
							objPublico[nNode].checked=false;
						}
					}
				}else{
					if (objPublico.value == document.pessoaForm.idTpPublico.value) {
						objPublico.checked=true;
					}else{
						objPublico.checked=false;
					}
				}
			}
		}
		
		if(countPublico == 1 && ifrmCmbTipoPub.pessoaForm.idTpPublico.value < 0 ){
			document.pessoaForm.idLstTpPublico.checked=true;
			ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value = document.pessoaForm.lstTpPublico[1].value;
			ifrmCmbTipoPub.cmbTipoPublico_onChange();
		}
		
		onLoadEspec();

		//Danilo Prevides - 04/12/2009 - 67546 - INI
		if (pessoaForm.pessInPfj[0].checked == true){
			window.top.ifrmTelaWorkflow.pessDsCpf = document.pessoaForm.pessDsCpf.value ;
		} else {
			window.top.ifrmTelaWorkflow.pessDsCpf = document.pessoaForm.pessDsCgc.value;
		}
		//Danilo Prevides - 04/12/2009 - 67546 - FIM

	}
	catch(e){
		if(controleCarregaLoad < 5){
			controleCarregaLoad++;
			setTimeout("carregaLoad();", 300);
		}
		//else{
			//alert(e.message);
		//}
	}
}

function disabledCorporativo(){
	bSemPermissao=true;
	ifrmCmbTipoPub.document.pessoaForm.idTpPublico.disabled=true;
	pessoaForm.optPessoaFisica.disabled=true;
	pessoaForm.optPessoaJuridica.disabled=true;
	pessoaForm.pessNmPessoa.disabled=true;
	pessoaForm.pessNmApelido.disabled=true;
	pessoaForm.Sexo[0].disabled=true;
	pessoaForm.Sexo[1].disabled=true;
	pessoaForm.pessDsCpf.disabled=true;
	pessoaForm.pessDsRg.disabled=true;
	pessoaForm.pessDsOrgemissrg.disabled=true;
	pessoaForm.pessDhNascimento.disabled=true;
	pessoaForm.txtIdade.disabled=true;
	pessoaForm.pessDsCgc.disabled = true;
	pessoaForm.pessDsIe.disabled = true;
	ifrmCmbEstadoCivil.document.pessoaForm.idEsciCdEstadocil.disabled=true;

	cmbBanco.dadosAdicionaisForm.pessCdBanco.disabled = true;
	cmbAgencia.document.dadosAdicionaisPessForm.pessCdAgencia.disabled = true;
	pessoaForm.pessCdBanco.disabled = true;
	pessoaForm.pessCdAgencia.disabled = true;
	pessoaForm.pessDsConta.disabled = true;
	pessoaForm.pessDsTitularidade.disabled = true;
	pessoaForm.pessDsCpfTitular.disabled = true;
	pessoaForm.pessDsRgTitular.disabled = true;
}

function novo(nomePessoa){
	parent.parent.document.getElementById("aguarde").style.visibility = "visible";
	pessoaForm.acao.value = "<%= Constantes.ACAO_EDITAR %>";
	pessoaForm.pessNmPessoa.disabled = false;
	pessoaForm.pessNmPessoa.value = nomePessoa;
	pessoaForm.submit();
}

function cancelar(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_cancelar" />")) {
	parent.parent.document.getElementById("aguarde").style.visibility = "visible";
	pessoaForm.acao.value = "";
	pessoaForm.submit();
  }
}

function abrirContato(){
	url = 'DadosContato.do?idPessCdPessoaPrinc=' + pessoaForm.idPessCdPessoa.value + '&idEmprCdEmpresa=' + parent.parent.ifrmTelaFiltro.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
	showModalDialog(url, this, 'help:no;scroll:no;Status:NO;dialogWidth:860px;dialogHeight:670px,dialogTop:0px,dialogLeft:10px');
}

function validate(par){
	if (ifrmEndereco.ifrmEndereco.endForm.peenDsLogradouro.disabled == false) {
		alert("<bean:message key="prompt.alert.endereco.desab" />");
		bEnvia = true;
		return false;
	}

	try {
		//Chama a funcao do include do cliente para saber quais sao as regras
		return validateEspec(par);
	}
	catch(e){ 
		return true;		
	}
}

function disab(){
	for (x = 0;  x < pessoaForm.elements.length;  x++)
	{
		Campo = pessoaForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}	 
}

function verificaFisicaJuridica(){

	if (Complemento.style.display == 'block') {
		//Pessoa Fisica ?
		if (pessoaForm.pessInPfj[0].checked == true){
			Fisica.style.display = 'block';
			Juridica.style.display = 'none';
		}else if (pessoaForm.pessInPfj[1].checked == true){
			Fisica.style.display = 'none';
			Juridica.style.display = 'block';
		}
	}
	
}

function preencheSexo(){
	if (pessoaForm.pessInSexo.value == "true"){
		pessoaForm.Sexo[0].checked = true;
	}else{
		pessoaForm.Sexo[1].checked = true;	
	}
	if (pessoaForm.pessInSexo.value == ""){
		pessoaForm.Sexo[0].checked = false;	
		pessoaForm.Sexo[1].checked = false;	
	}
}

function preencheHiddenSexo(){
	if (pessoaForm.Sexo[0].checked == true){
		pessoaForm.pessInSexo.value = true;
		return true;
	}
	if (pessoaForm.Sexo[1].checked == true){
		pessoaForm.pessInSexo.value = false;
		return true;
	}
	pessoaForm.pessInSexo.value = "";
	return false;
}

function calcage(data){
	if (data==""){
		return false;
	}
	 dd = data.substring(0, 2);
	 mm = data.substring(3, 5);
	 yy = data.substring(6, 10);

	thedate = new Date() 
	mm2 = thedate.getMonth() + 1 
	dd2 = thedate.getDate() 
	yy2 = thedate.getYear() 
	
	if (yy2 < 1000) { 
		yy2 = yy2 + 1900 
	} 
	
	yourage = yy2 - yy;
	if (mm2 < mm) { 
		yourage = yourage - 1; 
	} 

	if (mm2 == mm) { 
		if (dd2 < dd) { 
			yourage = yourage - 1; 
		} 
	} 
	
	agestring = yourage;
	if (agestring >0 && agestring <120){
		pessoaForm.txtIdade.value = agestring;
	}else{
		pessoaForm.txtIdade.value = "";	
	}
}

function identificaPessoa() {
	
	if (parent.parent.parent.esquerdo.comandos.document.all["dataInicio"].value == "") {
    	alert('<bean:message key="prompt.alert.iniciar.atend.pessoa" />');
    }else{
		showModalDialog('<%= Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>?pessoa=nome',window, '<%= Geral.getConfigProperty("app.crm.identificao.dimensao", empresaVo.getIdEmprCdEmpresa())%>');
	}
}

function pressEnter(event) {
    if (event.keyCode == 13) {
    	identificaPessoa();
    }
}
</script>





<Script language="javascript">
//  Documento JavaScript                                  '
//Funcao para Calculo do Digito do CPF/CNPJ
function DigitoCPFCNPJ(numCIC) {
var numDois = numCIC.substring(numCIC.length-2, numCIC.length);
var novoCIC = numCIC.substring(0, numCIC.length-2);
switch (numCIC.length){
 case 11 :
  numLim = 11;
  break;
 case 14 :
  numLim = 9;
  break;
 default : return false;
}
var numSoma = 0;
var Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
var numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
   //-- Primeiro digito calculado.  Fara parte do novo calculo.
   
   var numDigito = String(numResto);
   novoCIC = novoCIC.concat(numResto);
   //--
numSoma = 0;
Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
numResto = numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
//-- Segundo digito calculado.
numDigito = numDigito.concat(numResto);
if (numDigito == numDois) {
 return true;
}
else {
 return false;
}
}
//--< Fim da Funcao >--

//-- Retorna uma string apenas com os numeros da string enviada
function ApenasNum(strParm) {
strParm = String(strParm);
var chrPrt = "0";
var strRet = "";
var j=0;
for (var i=0; i < strParm.length; i++) {
 chrPrt = strParm.substring(i, i+1);
 if ( chrPrt.match(/\d/) ) {
  if (j==0) {
   strRet = chrPrt;
   j=1;
  }
  else {
   strRet = strRet.concat(chrPrt);
  }
 }
}
return strRet;
}
//--< Fim da Funcao >--

//-- Somente aceita os caracteres validos para CPF e CNPJ.
function PreencheCIC(objCIC) {
var chrP = objCIC.value.substring(objCIC.value.length-1, objCIC.value.length);

if ( !chrP.match(/[0-9]/) && !chrP.match(/[\/.-]/) ) {
 objCIC.value = objCIC.value.substring(0, objCIC.value.length-1);
 return false;
}
return true;
}
//--< Fim da Funcao >--

function FormataCIC (numCIC) {
numCIC = String(numCIC);
switch (numCIC.length){
case 11 :
 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
case 14 :
 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
default : 
 alert("<bean:message key="prompt.Tamanho_incorreto_do_CPF_ou_CNPJ"/>");
 return "";
}
}

//-- Remove os sinais, deixando apenas os numeros e reconstroi o CPF ou CNPJ, verificando a validade
//-- Recebe como parametros o numero do CPF ou CNPJ, com ou sem sinais e o atualiza com sinais e validado.
function ConfereCIC(objCIC) {
if (objCIC.value == '') {
 return false;
}
var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

numCPFCNPJ = ApenasNum(objCIC.value);

if (!DigitoCPFCNPJ(numCPFCNPJ)) {
 alert("<bean:message key="prompt.Atencao_o_Digito_verificador_do_CPF_ou_CNPJ_e_invalido"/>");
 AtivarPasta("DADOSCOMPLEMENTARES");
 try{
 	objCIC.focus();
 }catch(e){}
 return false;
}

objCIC.value = FormataCIC(numCPFCNPJ);

if (objCIC.value.match(strCNPJPat)) {
 return true;
}
else if (objCIC.value.match(strCPFPat)) {
 return true;
}
else {
 alert("<bean:message key="prompt.Digite_um_CPF_ou_CNPJ_valido"/>");
		AtivarPasta("DADOSCOMPLEMENTARES");
 objCIC.focus();
 return false;
}
}
//Fim da Funcao para Calculo do Digito do CPF/CNPJ


function posicionaRegistro(idTppu){
	ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value = idTppu;
	ifrmCmbTipoPub.cmbTipoPublico_onChange();
}

function adicionarTpPublico(){
	var idTppublico;
	
	idTppublico = ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value;
	
	if (ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value == "" || ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value == "-1"){
		alert('<bean:message key="prompt.selecione_tipo_publico"/>');
		return false;
	}
	addTpPublico(idTppublico,ifrmCmbTipoPub.document.pessoaForm.idTpPublico.options[ifrmCmbTipoPub.document.pessoaForm.idTpPublico.selectedIndex].text,false); 
}

nLinhaC = new Number(100);
estiloC = new Number(100);

var idtppublicoAux = 0;
function addTpPublico(idtppublico,desc,desabilita, tppenInPrincipal) {
	
	nLinhaC = Number(nLinhaC) + 1;
	estiloC++;
		
	objPublico = document.pessoaForm.lstTpPublico;
	if (objPublico != null){
		for (nNode=0;nNode<objPublico.length;nNode++) {
		  if (objPublico[nNode].value == idtppublico) {
			  idtppublico="";
			 }
		}
	}

	if(document.pessoaForm.idTpPublico.value == 0 && tppenInPrincipal == "S"){
		idtppublicoAux = idtppublico;
	}
	
	if (idtppublico != ""){
		strTxt = "";
		strTxt += "	<table id=\"tb" + nLinhaC + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr class='intercalaLst" + (estiloC-1)%2 + "'>";
		strTxt += "	        <td class=principalLstPar width=1%></td>";
		strTxt += "     	<td class=principalLstPar width=1%></td>";
		strTxt += "	        <td class=principalLstPar width=1%>&nbsp;</td>";
		if(desabilita==true){
			strTxt += "	        <td class=principalLstPar width=1%><input type=\"radio\" onclick=\"posicionaRegistro(\'" + idtppublico + "'\)\"" + " name=\"idLstTpPublico\" value=\"" + idtppublico + "\" disabled></td>";
		}else{
			strTxt += "	        <td class=principalLstPar width=1%><input type=\"radio\" onclick=\"posicionaRegistro(\'" + idtppublico + "'\)\"" + " name=\"idLstTpPublico\" value=\"" + idtppublico + "\" ></td>";
		}
		strTxt += "	        <td class=principalLstPar width=1%>&nbsp;</td>";
		strTxt += "     	<td class=principalLstPar width=38%> " + desc.toUpperCase() + "<input type=\"hidden\" name=\"lstTpPublico\" value=\"" + idtppublico + "\" ></td>";

		if(tppenInPrincipal == "S"){
			strTxt += "     	<td id=tdTpPublicoPrincipal" + idtppublico + " name=tdTpPublicoPrincipal" +  idtppublico + " class=principalLstPar width=3% ><img name=imgTpPublicoPrincipal id=imgTpPublicoPrincipal src=webFiles/images/botoes/check.gif width=12 height=12 class=geralCursoHand title=\"<bean:message key="prompt.principal" />\"><input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"S\" ></td>";
		}else{
			strTxt += "     	<td id=tdTpPublicoPrincipal" + idtppublico + " name=tdTpPublicoPrincipal" +  idtppublico + " class=principalLstPar width=3% >&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" ></td>";
		}
		
		strTxt += "		</tr>";
		strTxt += " </table>";
		
		document.getElementById("divLstTpPublico").innerHTML += strTxt;
	}else{
		alert('<bean:message key="prompt.alert.registroRepetido"/>');
	}
}
	
function removeTpPublico(nTblExcluir) {
	if (confirm('<bean:message key="prompt.confirm.Remover_este_registro"/>')) {
		objIdTbl = window.document.all.item("tb"+ nTblExcluir);
		document.getElementById("divLstTpPublico").removeChild(objIdTbl);
		estiloC--;
	}
}

//Esta variável guarda descrição e link de todas as abas dinamicas
var abasDinamicas = new Array();

/*****************************************************************************************************
 Adiciona parametros no Array abasDinamicas para ser exibida na tela ao chamar o método mostrarAbas()
 Recebe descrição da aba e caminho para carregar o iframe (com os parametros já resolvidos)
******************************************************************************************************/
function criarAbaDinamica(dsAba, linkAba, HTMLAba){
	abasDinamicas[numAbasDinamicas] = new Array();
	abasDinamicas[numAbasDinamicas][0] = dsAba;
	abasDinamicas[numAbasDinamicas][1] = linkAba;
	abasDinamicas[numAbasDinamicas][2] = HTMLAba;
	numAbasDinamicas++;
}

/************************************************************************************************************
 Mostra abas dinâmicas na tela - Cada vez que for chamado, criará na tela as abas e os iframes. Se já houver 
 alguma aba criada e este método for chamado, o que estiver no iframe será perdido se não tiver sido salvo.
************************************************************************************************************/
function mostrarAbas(links){
	var larguraAbas = 0;
	var HTMLAbaDinamica = "";
	var HTMLIframesDinamicos = "";

	for(i = 0; i < numAbasDinamicas; i++){
		var nomeAba = abasDinamicas[i][0];
		try {
			var nomeAba = getAcronym(abasDinamicas[i][0], 18);
		} catch(e) {}
		
		if (!getPermissao(links[i][0])){
			HTMLAbaDinamica += "<td valign=\"top\" class=\"principalPstQuadroLinkNormalMAIOR\" id=\"aba"+ i + "\" name=\"aba"+ i +"\" style=\"display:none\"" + " onclick=\"AtivarPasta('aba"+ i +"')\">"+ nomeAba +"</td>";
		}else{
			larguraAbas += 150;
			HTMLAbaDinamica += "<td valign=\"top\" class=\"principalPstQuadroLinkNormalMAIOR\" id=\"aba"+ i + "\" name=\"aba"+ i +"\" " + " onclick=\"AtivarPasta('aba"+ i +"')\">"+ nomeAba +"</td>";
		}
		
		HTMLIframesDinamicos += "<div id='div"+ i +"' style='width:99%; height: 100%; display: none'>"+
								"<iframe name='ifrm"+ i +"'"+
								"	id='ifrm"+ i +"'"+
                    			"	src='"+ abasDinamicas[i][1] +"'"+
                      			"	width='100%' height='100%' scrolling='Yes' frameborder='0' marginwidth='0' marginheight='0' >"+
								"</iframe></div>";
		
	}
	
	HTMLAbaDinamica = "<table cellpadding=0 cellspacing=0 border=0 width='"+ larguraAbas +"'><tr>" + HTMLAbaDinamica +"</tr></table>";
	
	if(larguraAbas == 0) larguraAbas = 1;
	
	document.getElementById("tdAbasDinamicas").width = larguraAbas;
	document.getElementById("tdAbasDinamicas").innerHTML = HTMLAbaDinamica;
	document.getElementById("iframes").innerHTML = HTMLIframesDinamicos;
	
	for(i = 0; i < numAbasDinamicas; i++){
		if(abasDinamicas[i][2].indexOf("<") > -1){
			eval("document.ifrm"+ i +".document.location = 'about:blank'");
			eval("document.ifrm"+ i +".document.write(abasDinamicas[i][2])");
		}
	}
	
}

/*****************************************
 Remove TODAS as abas e iframes dinâmicos
*****************************************/

var nCountAbas = 0;

function removerAbas(){
	try{
		document.getElementById("tdAbasDinamicas").innerHTML = "";
		document.getElementById("iframes").innerHTML = "";
		abasDinamicas = new Array();
		numAbasDinamicas = 0;
	}
	catch(e){
		if(nCountAbas<5){
			setTimeout('removerAbas()',200);
			nCountAbas++;
		}
	}
}

//Atualiza o combo de linha passando o id da empresa
var nAtualizaCmbTipoPub = 0;
function atualizaCmbTipoPub(){
	try{
		document.getElementById("ifrmCmbTipoPub").src = "ShowPessCombo.do?tela=<%=MCConstantes.TELA_CMB_TP_PUBLICO%>&idEmprCdEmpresa="+ parent.parent.ifrmTelaFiltro.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
	}
	catch(x){
		if(nAtualizaCmbTipoPub < 30){
			nAtualizaCmbTipoPub++;
			setTimeout("atualizaCmbTipoPub();", 500);
		}
		else{
			alert("Erro em atualizaCmbTipoPub(): "+ x.description);
		}
	}
}


function scrollAbasMais(){
	document.getElementById("abas").scrollLeft += 100;
}

function scrollAbasMenos(){
	document.getElementById("abas").scrollLeft -= 100;
}



</script>

</head>
<body class="principalBgrPageIFRM" text="#000000" onload="carregaLoad();">
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="pessoaForm" >
  <!-- Campos Auxiliares para PessoaEspec -->
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux1"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux2"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux3"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux4"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux5"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux6"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux7"/>

  <html:hidden property="idTpPublico"/>
  <html:hidden property="idTratCdTipotratamento"/>
  <html:hidden property="acao"/>
  <html:hidden property="tela"/>
  <html:hidden property="idPessCdPessoa"/>
  <html:hidden property="pessInSexo"/>
  <input type="hidden" name="continuacao" value="">
  <html:hidden property="idEsciCdEstadocil" />

  <html:hidden property="idCoreCdConsRegional"/>
  <html:hidden property="consDsConsRegional"/>
  <html:hidden property="consDsUfConsRegional"/>
  <html:hidden property="pessCdInternetId"/>
  <html:hidden property="pessCdInternetPwd"/>

  <html:hidden property="pessDsBanco" />
  <!--html:hidden property="pessCdBanco" /-->
  <!--html:hidden property="pessCdAgencia" /-->
  <html:hidden property="pessDsAgencia" />
  <html:hidden property="pessDsCodigoEPharma" />
  <html:hidden property="pessDsCartaoEPharma" />
  <html:hidden property="pessCdInternetAlt" />
  <html:hidden property="pessInColecionador" />
  <html:hidden property="consDsCodigoMedico" />

  <html:hidden property="pessEmail" />
  <html:hidden property="detPessoa" />
  <html:hidden property="pessDsObservacao" />    

  <html:hidden property="idTpdoCdTipodocumento" />
  <html:hidden property="pessDsDocumento" />
  <html:hidden property="pessDhEmissaodocumento" />	

  <table width="99%" border="0" cellspacing="1" cellpadding="0" align="center">
    <tr> 
      <td class="principalLabel"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
    </tr>
    <tr> 
      <td class="principalLabel" height="13">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalLabel"><bean:message key="prompt.tipopublico" /></td>
                  <td class="principalLabel">&nbsp;</td>
                  <td class="principalLabel">&nbsp; </td>
                  <td class="principalLabel">&nbsp;</td>
                </tr>
                <tr> 
                  <td class="principalLabel" height="23">
                  	<table height="23">
                  		<tr>
                  			<td height="23px"><!--Jonathan | Adequa��o para o IE 10-->
                  				<iframe id="ifrmCmbTipoPub" name="ifrmCmbTipoPub" src="" width="100%" height="23px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  				<script>atualizaCmbTipoPub();</script>
                  			</td>
                  			<td>
                  				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
                  					<!-- img class="geralCursoHand" src="webFiles/images/botoes/check.gif" title="" onclick="adicionarTpPublico()" width="12" height="12"--> 
                  				<%}%>
                  			</td>
                  		</tr>
                  	</table>
     		      </td>
                  <td class="principalLabel" width="258" height="23">
                    <span class="principalLabelValorFixo">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.pessoa" /></span>
                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                    <html:radio property="pessInPfj" styleId="optPessoaFisica" value="F" onclick="verificaFisicaJuridica();" disabled="true"/>
                    <bean:message key="prompt.fisica" />
                    <html:radio property="pessInPfj" styleId="optPessoaJuridica" value="J" onclick="verificaFisicaJuridica();" disabled="true"/>
                    <bean:message key="prompt.juridica" />
                  </td>
                  <td class="principalLabel" width="10px">&nbsp;</td>
                  <td class="principalLabel" width="415px">
                    <span class="principalLabelValorFixo">&nbsp;&nbsp;&nbsp;<bean:message key="prompt.naocontactar" /></span>
                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                    <html:checkbox property="pessInTelefone" disabled="true"/>
                    <bean:message key="prompt.telefone" />
                    <html:checkbox property="pessInEmail" disabled="true"/>
                    <bean:message key="prompt.email" />
                    <html:checkbox property="pessInCarta" disabled="true"/>
                    <bean:message key="prompt.carta" />
                    <html:checkbox property="pessInSms" disabled="true"/>
                    <bean:message key="prompt.sms" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td width="551" valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td colspan="5"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td  class="principalLabel" width="35%"><bean:message key="prompt.formatrat" /></td>
                        <td class="principalLabel" width="60%"><%= getMessage("prompt.nome", request)%> <font color="red">*</font></td>
                        <td class="principalLabel" align="left" width="6%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" width="35%">
             				<iframe name="CmbFrmTratamento" src="ShowPessCombo.do?tela=<%=MCConstantes.TELA_CMB_TRATAMENTO%>" width="100%" height="20px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" >
				             </iframe>
				        </td>
                        <td width="60%"> 
                            <html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" style="width:360;" onkeydown="pressEnter(event)" disabled="true"/>
                            <script>
                            	try {
	                            	if ('<bean:write name="baseForm" property="idPessCdPessoa" />' != '0') {
		                            	window.top.superiorBarra.barraNome.document.getElementById("nomePessoa").innerHTML = '<%=acronymChar(((br.com.plusoft.csi.crm.form.PessoaForm)request.getAttribute("baseForm")).getPessNmPessoa(), 25)%>';
				                    //	window.top.superiorBarra.barraCamp.document.getElementById("codigoPessoa").innerHTML = "<bean:write name="baseForm" property="idPessCdPessoa" />";
				                    	window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = "<bean:message key="prompt.novo" />";
				                    	window.top.superior.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.disabled = true;
				                    } else {
		                            	window.top.superiorBarra.barraNome.document.getElementById("nomePessoa").innerHTML = '';
				                    //	window.top.superiorBarra.barraCamp.document.getElementById("codigoPessoa").innerHTML = '';
				                    	window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = '';
				                    //	window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = false;
				                    }
			                	 } catch(e) {}
		                    </script>
                        </td>
                        <td class="principalLabel" align="left" width="6%">
                          <!-- img id="btlupa" src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.identificarPessoa"/>" align="left" width="15" height="15" onClick="identificaPessoa()" class="geralCursoHand" border="0"-->
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="5"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalLabel" width="10%"><%= getMessage("prompt.cognome", request)%></td>
                        <td class="principalLabel" width="15%"><bean:message key="prompt.codigoCorporativo" /></td>                        
                        <td class="principalLabel" width="15%"><bean:message key="prompt.codigo" /></td>
                        <td width="60%"></td>
                      </tr>
                      <tr> 
                        <td class="principalLabel">
                          <html:text property="pessNmApelido" styleClass="principalObjForm" maxlength="60" style="width:268;" disabled="true"/>
                        </td>
                        <td class="principalLabel">
                          <html:text property="pessCdCorporativo" styleClass="principalObjForm" maxlength="10" readonly="true" style="width:70" />
                        </td>
                        <td class="principalLabel">
	                       <html:text property="idPessCdPessoaLabel" styleClass="principalObjForm" readonly="true"/>
	                       <script>pessoaForm.idPessCdPessoaLabel.value == 0?pessoaForm.idPessCdPessoaLabel.value = '':'';</script>
                        </td>
                        <td class="principalLabel" align="center" nowrap="nowrap">
                          <INPUT type="radio" name="Sexo" disabled>
                          <bean:message key="prompt.masc" />
                          <INPUT type="radio" name="Sexo" disabled>
                          <bean:message key="prompt.fem" />&nbsp;
                         </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="10" valign="top">&nbsp;</td>
            <td width="550" valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr valign="top"> 
                  <td class="principalLabel" height="90px"><!--Jonathan | Adequa��o para o IE 10-->
                    <iframe name="Email" id="Email" src='MailPess.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>' width="100%" height="90px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td valign="top" height="189"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td valign="top" height="254"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td valign="top" class="principalPstQuadroLinkVazio"> 
                  	<div id=abas name="abas" class="principalPstQuadroLinkVazio" style="float: left; overflow: hidden; width: 745px; margin: 0;">
                    <table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top">
	             				<table border="0" cellspacing="0" cellpadding="0" width="250">
	               					<tr>
										<td valign="top" class="principalPstQuadroLinkSelecionado" id="tdendereco" name="tdendereco" onclick="AtivarPasta('ENDERECO')"><bean:message key="prompt.endereco" /></td>
										<td valign="top" class="principalPstQuadroLinkNormalMAIOR" id="tddadoscomplementares" name="tddadoscomplementares" onclick="AtivarPasta('DADOSCOMPLEMENTARES')"><bean:message key="prompt.dadoscompl" /></td>
									</tr>
								</table>
							</td>
							
							<!-- ABAS DINAMICAS -->
							<td id="tdAbasDinamicas" valign="top">
							</td>
							<!-- FIM DAS ABAS DINAMICAS -->
						</tr>
					</table>
                    </div>
                    <div class="principalPstQuadroLinkVazio" style="float: left; margin: 0;"><img src="webFiles/images/botoes/esq.gif" style="cursor: pointer;" onclick="scrollAbasMenos();"><img src="webFiles/images/botoes/dir.gif" style="cursor: pointer;" onclick="scrollAbasMais();"></div>
                  <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td valign="top" class="principalBgrQuadro" align="center">
                    <div id="endereco" style="width:99%; height: 270px; display: block"><!--Jonathan | Adequa��o para o IE 10-->
                      <iframe name="ifrmEndereco" id="ifrmEndereco" src="webFiles/crmWeb/dadospessoa/ifrmEnderecoTelefone.jsp?name_input=<%=request.getAttribute("name_input").toString()%>" width="99%" height="270px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
                    <div id="dadosadicionais" style="width:99%; height: 270px; display: none" align="center"><!--Jonathan | Adequa��o para o IE 10-->
                       <iframe name="ifrmDadosAdicionais" id="ifrmDadosAdicionais" src="" width="99%" height="270px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
                    
					<div id="Complemento" style="width:99%; height: 270px; display: none"><br>
					<table WIDTH="100%" height="100%" cellpadding=0 cellspacing=0 border=0>
						<tr>
							<td width="50%" valign="top">
								<div id="Fisica" name="Fisica" style="width:100%; height: 200; display: none">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
										<tr> 
											<td class="principalLstCab" colspan="2"> 
												<div align="center"><bean:message key="prompt.pessoafisica" /></div>
											</td>
										</tr>
										<tr> 
											<td class="principalLabel" colspan="2"><bean:message key="prompt.cpf" /></td>
											<td height="1">&nbsp;</td>
										</tr>
										<tr> 
											<td class="principalLabel" colspan="2">
												<html:text property="pessDsCpf" styleClass="principalObjForm" maxlength="15" onblur="ConfereCIC(this);" disabled="true"/>
											</td>
											<td height="1">&nbsp;</td>
										</tr>
										<tr> 
											<td class="principalLabel"><bean:message key="prompt.rg" /></td>
											<td class="principalLabel"><bean:message key="prompt.orgaoemissor" /></td>
										</tr>
										<tr> 
											<td class="principalLabel"> 
												<html:text property="pessDsRg" styleClass="principalObjForm" onfocus="SetarEvento(this,'N')" maxlength="20" disabled="true"/>
											</td>
											<td class="principalLabel"> 
												<html:text property="pessDsOrgemissrg" styleClass="principalObjForm" maxlength="30" disabled="true"/>
											</td>
										</tr>
										
										<tr> 
											<td width="50%" class="principalLabel"><bean:message key="prompt.tipoDocumento" /></td>
											<td width="50%" class="principalLabel">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="principalLabel"><bean:message key="prompt.documento" /></td>
														<td width="50%" class="principalLabel"><bean:message key="prompt.dataEmissao" /></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td width="50%" class="principalLabel"> 
												  <select name="cmbTipoDocumentoPF" class="principalObjForm" onchange="" disabled="true">
							                      <option tpResultado="" value=""><bean:message key="prompt.combo.sel.opcao" /></option>
													<logic:present name="csCdtbTipodocumentoTpdoVector">
							                              <logic:iterate name="csCdtbTipodocumentoTpdoVector" id="tpdoPF">
							                              		<option value="<bean:write name="tpdoPF" property="field(id_tpdo_cd_tipodocumento)"/>">
							                               			<bean:write name="tpdoPF" property="field(tpdo_ds_tipodocumento)"/>
							                             		</option>
							                        	</logic:iterate>
							                        </logic:present>
					                    		</select>
											</td>
											<td width="50%" class="principalLabel"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%"><input type="text" style="width:94px;" maxlength="20" name="txtDocumentoPF" class="principalObjForm" readonly></td>
														<td width="50%"><input type="text" name="txtDataEmissaoPF" maxlength="10" class="principalObjForm" readonly></td>
													</tr>
												</table>
											</td>
										</tr>
										
										<tr> 
											<td class="principalLabel"><bean:message key="prompt.datanascimento" /></td>
											<td class="principalLabel"><bean:message key="prompt.idade" /></td>
										</tr>
										<tr> 
											<td class="principalLabel" height="1" valign="top"> 
												<html:text property="pessDhNascimento" styleClass="principalObjForm" onfocus="SetarEvento(this,'D')" maxlength="10" onblur="calcage(this.value);" disabled="true"/>
											</td>
											<td class="principalLabel" height="1" valign="top"> 
												<input type="text" name="txtIdade" class="principalObjForm" readonly>
											</td>
										</tr>
										<tr>
											<td class="principalLabel" colspan="2"><bean:message key="prompt.estadoCivil" /></td>
										</tr>
										<tr>
											<td class="principalLabel" colspan="2">
												<iframe id=ifrmCmbEstadoCivil name="ifrmCmbEstadoCivil" src="ShowPessCombo.do?tela=<%=MCConstantes.TELA_CMB_ESTADOCIVIL%>&idEsciCdEstadocil=<bean:write name='pessoaForm' property='idEsciCdEstadocil'/>" width="100%" height="20px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
											</td>
										</tr>
										<tr id="trPromptPessNmFiliacao" style="display:block">
											<td class="principalLabel" colspan="2"><plusoft:message key="prompt.nomeMae" /></td>
										</tr>
										<tr id="trCampoPessNmFiliacao" style="display:block">
											<td class="principalLabel" colspan="2">
												<html:text property="pessNmFiliacao" styleClass="principalObjForm" maxlength="80" disabled="true" style="width: 250" />
											</td>
										</tr>
									</table>
								</div>
								<div id="Juridica" name="Juridica" style="width:100%; height: 200; display: none">
									<table width="99%" border="0" cellspacing="0" cellpadding="0" align="left">
										<tr> 
											<td class="principalLstCab" colspan="2"> 
												<div align="center"><bean:message key="prompt.pessoajuridica" /></div>
											</td>
										</tr>
										<tr> 
											<td class="principalLabel"><bean:message key="prompt.cnpj" /></td>
										</tr>
										<tr> 
											<td class="principalLabel"> 
												<html:text property="pessDsCgc" styleClass="principalObjForm" onfocus="SetarEvento(this,'N')" maxlength="15" onblur="ConfereCIC(this);" disabled="true"/>
											</td>
										</tr>
										<tr> 
											<td class="principalLabel"><bean:message key="prompt.inscrestadual" /></td>
										</tr>
										<tr> 
											<td class="principalLabel"> 
												<html:text property="pessDsIe" styleClass="principalObjForm" maxlength="20" disabled="true"/>
											</td>
										</tr>
										
										<tr> 
											<td class="principalLabel">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="principalLabel"><bean:message key="prompt.tipoDocumento" /></td>
														<td width="25%" class="principalLabel"><bean:message key="prompt.documento" /></td>
														<td width="25%" class="principalLabel"><bean:message key="prompt.dataEmissao" /></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td class="principalLabel"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="principalLabel"> 
															  <select name="cmbTipoDocumentoPJ" class="principalObjForm" onchange="" disabled="true">
										                      <option tpResultado="" value=""><bean:message key="prompt.combo.sel.opcao" /></option>
																<logic:present name="csCdtbTipodocumentoTpdoVector">
										                              <logic:iterate name="csCdtbTipodocumentoTpdoVector" id="tpdoPJ">
										                              		<option value="<bean:write name="tpdoPJ" property="field(id_tpdo_cd_tipodocumento)"/>">
										                               			<bean:write name="tpdoPJ" property="field(tpdo_ds_tipodocumento)"/>
										                             		</option>
										                        	</logic:iterate>
										                        </logic:present>
								                    		</select>
														</td>
														<td width="25%"><input type="text" style="width:94px;" maxlength="20" name="txtDocumentoPJ" class="principalObjForm" readonly></td>
														<td width="25%"><input type="text" name="txtDataEmissaoPJ" maxlength="10" class="principalObjForm" readonly></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</div>
							</td>
							<td width="50%" valign="top">
								<div id="Banco" name="Banco" style="width:100%; height: 120; display: none">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
											<td class="principalLstCab" colspan="3"> 
												<div align="center"><bean:message key="prompt.dadosBancarios"/></div>
											</td>
										</tr>
										<tr height="11"> 
											<td class="principalLabel" width="25%">
												<bean:message key="prompt.Banco" />
											</td>
											<td id="labelAgencia" class="principalLabel" width="25%"> 
												<bean:message key="prompt.agencia" />
											</td>
											<td class="principalLabel" width="25%"> 
												&nbsp;
											</td>
										</tr>
										<tr> 
											<td height="18px"><!--Jonathan | Adequa��o para o IE 10-->
												<iframe name="cmbBanco" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_CMB_BANCO%>&pessCdBanco=<bean:write name="baseForm" property="pessCdBanco"/>&pessDsBanco=<bean:write name="baseForm" property="pessDsBanco"/>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsAgencia=<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="18px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
											</td>
											<td height="18px"> <!--Jonathan | Adequa��o para o IE 10-->
												<iframe id="cmbAgencia" name="cmbAgencia" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_CMB_AGENCIA%>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsAgencia=<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="18px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
											</td>
											<td> &nbsp;</td>
										</tr>
										<tr height="11"> 
											<td class="principalLabel" width="25%">
												<bean:message key="prompt.CodBanco" />
											</td>
											<td class="principalLabel">
												<bean:message key="prompt.CodAgencia" />
											</td>
											<td class="principalLabel">
												<bean:message key="prompt.conta" />
											</td>
										</tr>
										<tr height="11"> 
											<td>
												<html:text property="pessCdBanco" styleClass="principalObjForm" readonly="true" disabled="true"/>
											</td>
											<td> 
												<html:text property="pessCdAgencia" styleClass="principalObjForm" maxlength="10" disabled="true"/>
											</td>
											<td> 
												<html:text property="pessDsConta" styleClass="principalObjForm" maxlength="20" disabled="true"/>
											</td>
										</tr>
										<tr height="11"> 
											<td class="principalLabel" width="25%">
												<bean:message key="prompt.titularidade"/>
											</td>
											<td class="principalLabel">
												<bean:message key="prompt.cpftitular"/>
											</td>
											<td class="principalLabel">
												<bean:message key="prompt.rgtitular"/>
											</td>
										</tr>
										<tr height="11"> 
											<td>
												<html:text property="pessDsTitularidade" styleClass="principalObjForm" maxlength="50" style="width: 98px;" disabled="true"/>
											</td>
											<td> 
												<html:text property="pessDsCpfTitular" styleClass="principalObjForm" maxlength="15" onblur="ConfereCIC(this);" disabled="true"/>
											</td>
											<td> 
												<html:text property="pessDsRgTitular" styleClass="principalObjForm" maxlength="20" disabled="true"/>
											</td>
										</tr>
									</table>
								</div>
								<div id="divTpPublico" name="divTpPublico" style="width:100%; height: 200; display: none ">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr height="10">
											<td class="principalLstCab" colspan="3" height="10"> 
												<div align="center"><bean:message key="prompt.tipopublico" /></div>
											</td>
										</tr>
										<tr>
											<td height="70">
												<div name="lstTpPublico" id="divLstTpPublico" style="width:100%; height:98%; overflow-y:auto;">
													<input type="hidden" name="lstTpPublico" value="" >
													<!--Inicio Lista de Tipo de publico -->
													<logic:present name="lstPublicoVector">
														<logic:iterate id="cdppVector" name="lstPublicoVector">
															<script language="JavaScript">
																countPublico++;
																addTpPublico('<bean:write name="cdppVector" property="idTppuCdTipopublico" />','<bean:write name="cdppVector" property="tppuDsTipopublico" />',true, '<bean:write name="cdppVector" property="tppeInPrincipal" />');
															</script>
														</logic:iterate>
													</logic:present>
												</div>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
					</div>
	                    
						  <!--DIV DESTINADO PARA A INCLUSAO DOS CAMPOS ESPECIFICOS -->
					  <div name="camposDetalhePessoaEspec" id="camposDetalhePessoaEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden">
			
					  </div>
                    
                    <!-- DIVS DINAMICOS -->
                    <div id="iframes" name="iframes" style="width:99%; height: 230; display: none;"></div>
                    <!-- FIM DOS DIVS DINAMICOS -->
                    
                  </td>
                  <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="270"></td>
                </tr>
                <tr> 
                  <td width="1003" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                  <td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>
		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="right">
	        <tr>
				<logic:notEqual name="baseForm" property="idPessCdPessoa" value="0" >
					<td class="principalLabelValorFixo" width="22">
						<!-- img src="webFiles/images/botoes/novoContato.gif" title="<bean:message key="prompt.novoContato"/>" width="22" height="26" onClick="abrirContato()" class="geralCursoHand"-->
					</td>
					<td id="abrirContato" name="abrirContato" class="principalLabelValorFixo" width="90" onClick="//abrirContato()">
						<!-- span class="geralCursoHand">
							<bean:message key="prompt.contatos" />
						</span-->
					</td>
						<td class="principalLabelValorFixo" width="22">
							<!-- img src="webFiles/images/icones/perfil01.gif" width="21" height="25" onClick="showModalDialog('Perfil.do?idPessCdPessoa=' + pessoaForm.idPessCdPessoa.value, window, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand"-->
						</td>
						<td id="abrePerfil" name="abrePerfil" class="principalLabelValorFixo" width="31" onClick="//showModalDialog('Perfil.do?idPessCdPessoa=' + pessoaForm.idPessCdPessoa.value, window, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px')">
							  <!-- span class="geralCursoHand">
								  <bean:message key="prompt.perfil" />
							  </span-->
						</td>
				</logic:notEqual>
            	<td width="940"> 
              		<div align="right">
              			
	              			<!-- img id="btsalvar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onClick="javascript:Save();"--> 
	              		
                		<!-- img id="btcancelar" src="webFiles/images/botoes/cancelar.gif" width="20" height="20" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand" onclick="javascript:cancelar();"--> 
             		</div>
	            </td>
         	</tr>
        </table>
      </td>
    </tr>
  </table>

<iframe id="ifrmMultiEmpresa" name="ifrmMultiEmpresa" src="" frameborder=0 width=0 height=0></iframe>

<script language="JavaScript">
	//ifrmMultiEmpresa.document.location = "MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico=<%=request.getParameter("idTpPublico")%>&idEmprCdEmpresa="+ parent.parent.ifrmTelaFiltro.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
</script>
	
<script>

if (pessoaForm.acao.value == "<%= Constantes.ACAO_GRAVAR %>" || pessoaForm.acao.value == "<%= Constantes.ACAO_INCLUIR %>"){
	preencheSexo();
	calcage(pessoaForm.pessDhNascimento.value);
}

</script>
<div id="especialidadeHidden"></div>
</html:form>
</body>
</html>