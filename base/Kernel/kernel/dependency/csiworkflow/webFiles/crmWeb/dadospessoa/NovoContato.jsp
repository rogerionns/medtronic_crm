<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.novocontato" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}

MM_reloadPage(true);
// -->

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function chamaTela() {
		showModalDialog('IdentificaTiaeda.do',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px')
}

function AtivarPasta(pasta) {
	switch (pasta) {
		case 'ENDERECO':
			MM_showHideLayers('Endereco','','show','','','','Complemento','','hide','Fisica','','hide','Juridica','','hide','Banco','','hide')
			SetClassFolder('tdendereco','principalPstQuadroLinkSelecionado');	
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');	
			break;
		case 'DADOSCOMPLEMENTARES':
			MM_showHideLayers('Complemento','','show','','','','Endereco','','hide','Banco','','show')
			verificaFisicaJuridica();
			
			SetClassFolder('tdendereco','principalPstQuadroLinkNormal');
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkSelecionadoMAIOR');	
			break;
	}
}


function getEstadoCivil() { 
	contatoForm.idEsciCdEstadocil.value = ifrmCmbEstadoCivil.pessoaForm.idEsciCdEstadocil.value;
}

function getTipoPublico(){
	contatoForm.idTpPublico.value = ifrmCmbTipoPub.pessoaForm.idTpPublico.value;
}

function getForma(){
	contatoForm.idTratCdTipotratamento.value = CmbFrmTratamento.pessoaForm.idTratCdTipotratamento.value;
}

function getPrincipal(){
	contatoForm.idPessCdPessoaPrinc.value = window.dialogArguments.pessoaForm.idPessCdPessoa.value;
}

function getRelacao(){
	contatoForm.idTpreCdTiporelacao.value = CmbClassificacao.relacaoForm.idTpreCdTiporelacao.value;
}

function getDadosAdicionais() {

	/*
	contatoForm.pessCdBanco.value = cmbBanco.dadosAdicionaisForm.pessCdBanco.value;
	contatoForm.pessDsBanco.value = cmbBanco.dadosAdicionaisForm.pessDsBanco.value;
	contatoForm.pessCdAgencia.value = cmbAgencia.dadosAdicionaisForm.pessCdAgencia.value;
	contatoForm.pessDsAgencia.value = cmbAgencia.dadosAdicionaisForm.pessDsAgencia.value;
	*/
	
	if (contatoForm.pessCdBanco.value.lenght == 0){
		contatoForm.pessCdBanco.value = cmbBanco.dadosAdicionaisForm.pessCdBanco.value;
		contatoForm.pessDsBanco.value = cmbBanco.dadosAdicionaisForm.pessDsBanco.value;
	}
	
	if (contatoForm.pessCdAgencia.value.length == 0){
		contatoForm.pessCdAgencia.value = cmbAgencia.dadosAdicionaisForm.pessCdAgencia.value;
		contatoForm.pessDsAgencia.value = cmbAgencia.dadosAdicionaisForm.pessDsAgencia.value;
	}	
	
	especialidadeHidden.innerHTML = '';
}

var bEnvia = true;

function Save(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados" />")) {
	if (!bEnvia) {
		return false;
	}
	if(validate(true)){
		getTipoPublico();
		getForma();
		getPrincipal();
		getRelacao();
		getDadosAdicionais();
		getEstadoCivil();
		truncaCampos();
		if (document.forms[0].idTpreCdTiporelacao.value == -1 || document.forms[0].idTpreCdTiporelacao.value == ""){
			alert('<bean:message key="prompt.alert.combo.tpre" />');
		}else{
			bEnvia = false;
			contatoForm.target = this.name = "contato";
			contatoForm.submit();
		}
	}
  }
}

function truncaCampos() {
  textCounter(contatoForm.consDsConsRegional, 10);
  textCounter(contatoForm.consDsUfConsRegional, 2);
  textCounter(contatoForm.pessCdInternetId, 10);
  textCounter(contatoForm.pessCdInternetPwd, 10);
  textCounter(contatoForm.pessDsBanco, 50);
  textCounter(contatoForm.pessCdBanco, 10);
  textCounter(contatoForm.pessCdAgencia, 10);
  textCounter(contatoForm.pessDsAgencia, 50);
  textCounter(contatoForm.pessDsCodigoEPharma, 30);
  textCounter(contatoForm.pessDsCartaoEPharma, 40);
  textCounter(contatoForm.pessCdInternetAlt, 40);
  textCounter(contatoForm.pessInColecionador, 1);
  textCounter(contatoForm.consDsCodigoMedico, 15);
  textCounter(contatoForm.pessNmPessoa, 80);
  textCounter(contatoForm.pessNmApelido, 60);
}

function Fechar(){
	novo();
}
function abrir(id, contato){
	contatoForm.idPessCdPessoa.value = id;
	contatoForm.acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
	contatoForm.idTpreCdTiporelacao.value = contato;
	contatoForm.target = this.name = "contato";
	contatoForm.submit();
}

function abrirCorp(consDsCodigoMedico,idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,idPess,nmPess){
	contatoForm.consDsCodigoMedico.value = consDsCodigoMedico;
	contatoForm.idCoreCdConsRegional.value = idCoreCdConsRegional;
	contatoForm.consDsConsRegional.value = consDsConsRegional;
	contatoForm.consDsUfConsRegional.value = consDsUfConsRegional;
	contatoForm.idPessCdPessoa.value = idPess;
	contatoForm.target = this.name = "contato";
	contatoForm.acao.value = "<%=MCConstantes.ACAO_CONSULTAR_CORP %>";
	contatoForm.submit();
}

function abrirNI(id){
	contatoForm.action = contatoForm.action;
	abrir(id);
}

function abrirUltimo(){
	contatoForm.acao.value = "<%= MCConstantes.ACAO_CONSULTAR_ULTIMO %>";
	contatoForm.target = this.name = "contato";
	contatoForm.submit();
}

function novo(nomePessoa){
	contatoForm.acao.value = "<%= Constantes.ACAO_EDITAR %>";
	contatoForm.target = this.name = "contato";
	contatoForm.pessNmPessoa.disabled = false;
	contatoForm.pessNmPessoa.value = nomePessoa;
	contatoForm.submit();
}

function cancelar(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_cancelar" />")) {
	contatoForm.acao.value = "";
	contatoForm.target = this.name = "contato";
	contatoForm.submit();
  }
}

function validate(par){
	if (endereco.ifrmEndereco.endForm.peenDsLogradouro.disabled == false) {
		alert("<bean:message key="prompt.alert.endereco.desab" />");
		bEnvia = true;
		return false;
	}
	if (par==true && contatoForm.pessNmPessoa.value == "") {
		alert("<bean:message key="prompt.alert.texto.nome" />");
		contatoForm.pessNmPessoa.focus();
		bEnvia = true;
		return false;
	}
	if (par==true &&!preencheHiddenSexo()){
		if (!confirm('<bean:message key="prompt.alert.sexo" />')){
			bEnvia = true;
			return false;
		}
	}
	if (Critica_Formulario(contatoForm)){
//		MM_showHideLayers('Complemento','','hide');
		return true;
	}
	bEnvia = true;
	return false;
}

function verificaFisicaJuridica(){
	if (Complemento.style.visibility == 'visible') {
	//Pessoa Fisica ?
	if (contatoForm.pessInPfj[0].checked == true){
		Fisica.style.visibility = 'visible';
		Juridica.style.visibility = 'hidden';
		contatoForm.pessDsCpf.disabled = false;
		contatoForm.pessDsRg.disabled = false;
		contatoForm.pessDsOrgemissrg.disabled = false;
		contatoForm.pessDhNascimento.disabled = false;
		contatoForm.txtIdade.disabled = false;
		
		contatoForm.pessDsCgc.disabled = true;
		contatoForm.pessDsIe.disabled = true;
	}else if (contatoForm.pessInPfj[1].checked == true){
		Fisica.style.visibility = 'hidden';
		Juridica.style.visibility = 'visible';
		contatoForm.pessDsCgc.disabled = false;
		contatoForm.pessDsIe.disabled = false;
		
		contatoForm.pessDsCpf.disabled = true;
		contatoForm.pessDsRg.disabled = true;
		contatoForm.pessDsOrgemissrg.disabled = true;
		contatoForm.pessDhNascimento.disabled = true;
		contatoForm.txtIdade.disabled = true;
	}
	}
/*	//Pessoa Fisica ?
	if (contatoForm.pessInPfj[0].checked == true){
		contatoForm.pessDsCpf.disabled = false;
		contatoForm.pessDsRg.disabled = false;
		contatoForm.pessDsOrgemissrg.disabled = false;
		contatoForm.pessDhNascimento.disabled = false;
		contatoForm.txtIdade.disabled = false;
		
		contatoForm.pessDsCgc.disabled = true;
		contatoForm.pessDsIe.disabled = true;
	}else{
		contatoForm.pessDsCgc.disabled = false;
		contatoForm.pessDsIe.disabled = false;
		
		contatoForm.pessDsCpf.disabled = true;
		contatoForm.pessDsRg.disabled = true;
		contatoForm.pessDsOrgemissrg.disabled = true;
		contatoForm.pessDhNascimento.disabled = true;
		contatoForm.txtIdade.disabled = true;
	}*/
}

function desabilita_campos(){
		for (x = 0;  x < contatoForm.elements.length;  x++)
		{
			Campo = contatoForm.elements[x];
			if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
				Campo.disabled = true;
			}
		}
}

function preencheSexo(){
	if (contatoForm.pessInSexo.value == "true"){
		contatoForm.Sexo[0].checked = true;
	}else{
		contatoForm.Sexo[1].checked = true;	
	}
	if (contatoForm.pessInSexo.value == ""){
		contatoForm.Sexo[0].checked = false;	
		contatoForm.Sexo[1].checked = false;	
	}
}

function preencheHiddenSexo(){
	if (contatoForm.Sexo[0].checked == true){
		contatoForm.pessInSexo.value = true;
		return true;
	}
	if (contatoForm.Sexo[1].checked == true){
		contatoForm.pessInSexo.value = false;
		return true;
	}
	contatoForm.pessInSexo.value = "";
	return false;
}
function calcage(data){
 dd = data.substring(0, 2);
 mm = data.substring(3, 5);
 yy = data.substring(6, 10);

	thedate = new Date() 
	mm2 = thedate.getMonth() + 1 
	dd2 = thedate.getDate() 
	yy2 = thedate.getYear() 
	if (yy2 < 100) { 
	yy2 = yy2 + 1900 } 
	yourage = yy2 - yy 
	if (mm2 < mm) { 
	yourage = yourage - 1; } 
	if (mm2 == mm) { 
	if (dd2 < dd) { 
	yourage = yourage - 1; } 
	} 
	agestring = yourage;
	if (agestring >0 && agestring <120){
		contatoForm.txtIdade.value = agestring;
	}else{
		contatoForm.txtIdade.value = "";	
	}
}

</script>

<Script language="javascript">
//  Documento JavaScript                                  '
//Fun��o para C�lculo do Digito do CPF/CNPJ
function DigitoCPFCNPJ(numCIC) {
var numDois = numCIC.substring(numCIC.length-2, numCIC.length);
var novoCIC = numCIC.substring(0, numCIC.length-2);
switch (numCIC.length){
 case 11 :
  numLim = 11;
  break;
 case 14 :
  numLim = 9;
  break;
 default : return false;
}
var numSoma = 0;
var Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
var numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
   //-- Primeiro d�gito calculado.  Far� parte do novo c�lculo.
   
   var numDigito = String(numResto);
   novoCIC = novoCIC.concat(numResto);
   //--
numSoma = 0;
Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
numResto = numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
//-- Segundo d�gito calculado.
numDigito = numDigito.concat(numResto);
if (numDigito == numDois) {
 return true;
}
else {
 return false;
}
}
//--< Fim da Fun��o >--

//-- Retorna uma string apenas com os n�meros da string enviada
function ApenasNum(strParm) {
strParm = String(strParm);
var chrPrt = "0";
var strRet = "";
var j=0;
for (var i=0; i < strParm.length; i++) {
 chrPrt = strParm.substring(i, i+1);
 if ( chrPrt.match(/\d/) ) {
  if (j==0) {
   strRet = chrPrt;
   j=1;
  }
  else {
   strRet = strRet.concat(chrPrt);
  }
 }
}
return strRet;
}
//--< Fim da Fun��o >--

//-- Somente aceita os caracteres v�lidos para CPF e CNPJ.
function PreencheCIC(objCIC) {
var chrP = objCIC.value.substring(objCIC.value.length-1, objCIC.value.length);

if ( !chrP.match(/[0-9]/) && !chrP.match(/[\/.-]/) ) {
 objCIC.value = objCIC.value.substring(0, objCIC.value.length-1);
 return false;
}
return true;
}
//--< Fim da Fun��o >--

function FormataCIC (numCIC) {
numCIC = String(numCIC);
switch (numCIC.length){
case 11 :
 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
case 14 :
 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
default : 
 alert("<bean:message key="prompt.Tamanho_incorreto_do_CPF_ou_CNPJ"/>");
 return "";
}
}

//-- Remove os sinais, deixando apenas os n�meros e reconstroi o CPF ou CNPJ, verificando a validade
//-- Recebe como par�metros o n�mero do CPF ou CNPJ, com ou sem sinais e o atualiza com sinais � validado.
function ConfereCIC(objCIC) {
if (objCIC.value == '') {
 return false;
}
var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

numCPFCNPJ = ApenasNum(objCIC.value);

if (!DigitoCPFCNPJ(numCPFCNPJ)) {
 alert("<bean:message key="prompt.Atencao_o_Digito_verificador_do_CPF_ou_CNPJ_e_invalido"/>");
 objCIC.focus();
 return false;
}

objCIC.value = FormataCIC(numCPFCNPJ);

if (objCIC.value.match(strCNPJPat)) {
 return true;
}
else if (objCIC.value.match(strCPFPat)) {
 return true;
}
else {
 alert("<bean:message key="prompt.Digite_um_CPF_ou_CNPJ_valido"/>");
 objCIC.focus();
 return false;
}
}
//Fim da Fun��o para C�lculo do Digito do CPF/CNPJ

	function inicio(){
		showError("<%=request.getAttribute("msgerro")%>");
	}

</script>


</head>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="contatoForm" >
  <html:hidden property="idTpPublico"/>
  <html:hidden property="idTratCdTipotratamento"/>
  <html:hidden property="acao"/>
  <html:hidden property="idPessCdPessoa"/>
  <html:hidden property="idTpreCdTiporelacao"/>
  <html:hidden property="idPessCdPessoaPrinc"/>
  <html:hidden property="pessInSexo"/>
  <html:hidden property="idEsciCdEstadocil" />

  <html:hidden property="idCoreCdConsRegional"/>
  <html:hidden property="consDsConsRegional"/>
  <html:hidden property="consDsUfConsRegional"/>
  <html:hidden property="pessCdInternetId"/>
  <html:hidden property="pessCdInternetPwd"/>

  <!--html:hidden property="pessCdBanco" /-->
  <html:hidden property="pessDsBanco" />
  <!--html:hidden property="pessCdAgencia" /-->
  <html:hidden property="pessDsAgencia" />
  <html:hidden property="pessDsCodigoEPharma" />
  <html:hidden property="pessDsCartaoEPharma" />
  <html:hidden property="pessCdInternetAlt" />
  <html:hidden property="pessInColecionador" />
  <html:hidden property="consDsCodigoMedico" />
	
	<%/**
	   * Chamado 75799 - Vinicius
	   */%>
	<html:hidden property="erro"/>
	
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.tiporel" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top">
      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td valign="top"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
        </tr>
        <tr> 
          <td valign="middle" height="30"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="50%">
                <iframe id=CmbClassificacao name="CmbClassificacao" src="ShowContatoCombo.do?tela=<%=MCConstantes.TELA_CMB_TRELACAO%>" width="100%"  height="20px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                </td>
                <td width="50%">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.dadospessoa" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="99%" border="0" cellspacing="0" cellpadding="0" height="100%" align="center">
        <tr> 
          <td valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td colspan="3"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalLabel"><bean:message key="prompt.tipopublico" /></td>
                        <td class="principalLabel">&nbsp;</td>
                        <td class="principalLabel">&nbsp;</td>
                        <td class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" width="290" height="23px">
                        <iframe id=ifrmCmbTipoPub name="ifrmCmbTipoPub" src="ShowContatoCombo.do?tela=<%=MCConstantes.TELA_CMB_TP_PUBLICO%>" width="100%" height="23px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
	                  	<td class="principalLabel" width="258" height="23"><span class="principalLabelValorFixo">&nbsp;&nbsp; 
	                    	&nbsp; &nbsp;&nbsp;<bean:message key="prompt.pessoa" /></span> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
		                    <html:radio property="pessInPfj" value="F" onclick="verificaFisicaJuridica()"></html:radio>
		                    <bean:message key="prompt.fisica" />
		                    <html:radio property="pessInPfj" value="J" onclick="verificaFisicaJuridica()"></html:radio>
		                    <bean:message key="prompt.juridica" />&nbsp;</td>
	                  <td class="principalLabel" width="10">&nbsp;</td>
	                  <td class="principalLabel" width="415"><span class="principalLabelValorFixo"> 
		                    &nbsp; &nbsp; &nbsp;<bean:message key="prompt.naocontactar" /></span> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
		                    <html:checkbox property="pessInTelefone" ></html:checkbox>
		                    <bean:message key="prompt.telefone" /> 
		                    <html:checkbox property="pessInEmail"></html:checkbox>                    
		                    <bean:message key="prompt.email" /> 
		                    <html:checkbox property="pessInCarta"> </html:checkbox>
		                    <bean:message key="prompt.carta" />
		                    <html:checkbox property="pessInSms" />
	                    	<bean:message key="prompt.sms" />
                      </td>
	                 </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="551" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td colspan="5"> 
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td  class="principalLabel" width="100"><bean:message key="prompt.formatrat" /></td>
                        <td class="principalLabel" width="355"><bean:message key="prompt.nome" /> <font color="red">*</font></td>
                        <td class="principalLabel" align="left" width="10">&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="principalLabel" width="100">
             				<iframe name="CmbFrmTratamento" src="ShowContatoCombo.do?tela=<%=MCConstantes.TELA_CMB_TRATAMENTO%>" width="100%" height="20px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" >
				             </iframe>
				        </td>
                        <td class="principalLabel" width="355"> 
                            <html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" style="width:350;"/>
                        </td>
                        <td class="principalLabel" align="left" width="10">
                        	<img src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.identificarPessoa"/>" align="left" width="15" height="15" onClick="chamaTela()" class="geralCursoHand" border="0"> 
                        </td>
                      </tr>
                    </table>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="5"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLabel" width="51%"><bean:message key="prompt.cognome" /></td>
                              <td class="principalLabel" colspan="3"><bean:message key="prompt.codigo" /></td>
                            </tr>
                            <tr> 
                              <td class="principalLabel" width="51%"> 
                               <html:text property="pessNmApelido" styleClass="principalObjForm" maxlength="60" style="width:280;"/>
                              </td>
                              <td class="principalLabel" width="15%"> 
                                <html:text property="idPessCdPessoaLabel" styleClass="principalObjForm" style="width:80" readonly="true"/>
                                <script>contatoForm.idPessCdPessoaLabel.value == 0?contatoForm.idPessCdPessoaLabel.value = '':'';</script>
                              </td>
                        <td class="principalLabel" width="25%" align="center">
                          <INPUT type="radio" name="Sexo">
                          <bean:message key="prompt.masc" />
                          <INPUT type="radio" name="Sexo">
                          <bean:message key="prompt.fem" />&nbsp;
                         </td>
                              <td class="principalLabel" width="6%" align="center">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="10" valign="top">&nbsp;</td>
                  <td width="700" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="top"> 
                        <td class="principalLabel" height="90px"><!--Jonathan | Adequa��o para o IE 10-->
                          <iframe name="Email" src="MailContato.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>" width="100%" height="90px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
					    </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td valign="top" height="189"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td height="254"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalPstQuadroLinkVazio"> 
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalPstQuadroLinkSelecionado" id="tdendereco" name="tdendereco"	onclick="AtivarPasta('ENDERECO')"><bean:message key="prompt.endereco" /></td>
                        <td class="principalPstQuadroLinkNormalMAIOR" id="tddadoscomplementares" name="tddadoscomplementares" onclick="AtivarPasta('DADOSCOMPLEMENTARES')"><bean:message key="prompt.dadoscompl" /></td>
                      </tr>
                    </table>
                  </td>
                  <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td valign="top" class="principalBgrQuadro"> &nbsp; 
                    <div id="Endereco" style="position:absolute; width:99%; z-index:2; height: 224px; visibility: visible">
                      <iframe name="endereco" id="endereco" src="webFiles/dadospessoa/ifrmEnderecoTelefoneContato.jsp?name_input=<%=request.getAttribute("name_input").toString()%>" width="100%" height="224px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
                    <div id="Complemento" style="position:absolute; width:99%; z-index:4; height: 224px; visibility: hidden">
                     <br>
                     <div id="Fisica" style="position:absolute; width:40%; height: 224px; z-index:5; visibility: hidden">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
			            <tr> 
			              <td class="principalLstCab" colspan="2"> 
			                <div align="center"><bean:message key="prompt.pessoafisica" /></div>
			              </td>
			            </tr>
			            <tr> 
			              <td class="principalLabel" colspan="2"><bean:message key="prompt.cpf" /></td>
			              <td height="1">&nbsp;</td>
			            </tr>
			            <tr> 
			              <td class="principalLabel" colspan="2">               
			                <html:text property="pessDsCpf" styleClass="principalObjForm" maxlength="15" onblur="ConfereCIC(this);" />
			              </td>
			              <td height="1">&nbsp;</td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"><bean:message key="prompt.rg" /></td>
			              <td class="principalLabel"><bean:message key="prompt.orgaoemissor" /></td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"> 
			                <html:text property="pessDsRg" styleClass="principalObjForm" maxlength="20"  />
			              </td>
			              <td class="principalLabel"> 
			                <html:text property="pessDsOrgemissrg" styleClass="principalObjForm" maxlength="4"  />
			              </td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"><bean:message key="prompt.datanascimento" /></td>
			              <td class="principalLabel"><bean:message key="prompt.idade" /></td>
			            </tr>
			            <tr> 
			              <td class="principalLabel" height="1" valign="top"> 
			                <html:text property="pessDhNascimento" styleClass="principalObjForm" onfocus="SetarEvento(this,'D')" maxlength="10" onblur="calcage(this.value);"/>
			              </td>
			              <td class="principalLabel" height="1" valign="top"> 
			                <input type="text" name="txtIdade" class="principalObjForm" readonly="true">
			              </td>
			            </tr>
			            <tr>
				            <td class="principalLabel" colspan="2"><bean:message key="prompt.estadoCivil" /></td>
				        </tr>
				        <tr>
				        	<td class="principalLabel" colspan="2">
					        	<iframe id=ifrmCmbEstadoCivil name="ifrmCmbEstadocivil" src="ShowPessCombo.do?tela=<%=MCConstantes.TELA_CMB_ESTADOCIVIL%>&idEsciCdEstadocil=<bean:write name='contatoForm' property='idEsciCdEstadocil'/>" width="100%" height="20%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>	
							</td>
						</tr>
			           </table>
			         </div>
                     <div id="Juridica" style="position:absolute; width:40%; height: 224px; z-index:5; visibility: hidden">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
			            <tr> 
			              <td class="principalLstCab" colspan="2"> 
			                <div align="center"><bean:message key="prompt.pessoajuridica" /></div>
			              </td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"><bean:message key="prompt.cnpj" /></td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"> 
			                <html:text property="pessDsCgc" styleClass="principalObjForm" maxlength="15" onblur="ConfereCIC(this);" />
			              </td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"><bean:message key="prompt.inscrestadual" /></td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"> 
			              	<html:text property="pessDsIe" styleClass="principalObjForm" maxlength="20"  />
			              </td>
			            </tr>
			          </table>
                     </div>
                     <div id="Banco" style="position:absolute; width:55%; height: 224px; left: 350; z-index:5; visibility: hidden">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			            <tr> 
			              <td class="principalLstCab" colspan="3"> 
			                <div align="center">Dados Banc�rios</div>
			              </td>
			            </tr>
						<tr height="11"> 
						  <td class="principalLabel" width="25%">
						    <bean:message key="prompt.banco" />
						  </td>
						  <td class="principalLabel" width="25%"> 
						    <bean:message key="prompt.agencia" />
						  </td>
						  <td class="principalLabel" width="25%"> 
						     &nbsp;
						  </td>
						</tr>
						<tr height="11px"> <!--Jonathan | Adequa��o para o IE 10-->
						  <td>
						    <iframe name="cmbBanco" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_CMB_BANCO%>&pessCdBanco=<bean:write name="baseForm" property="pessCdBanco"/>&pessDsBanco=<bean:write name="baseForm" property="pessDsBanco"/>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsAgencia=<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="11px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
						  </td>
						  <td> 
						    <iframe name="cmbAgencia" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_CMB_AGENCIA%>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsAgencia=<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="11px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
						  </td>
						  <td> 
						    &nbsp;
						  </td>
						</tr>
						<tr height="11px"> <!--Jonathan | Adequa��o para o IE 10-->
						  <td class="principalLabel" width="25%">
						    <bean:message key="prompt.CodBanco" />
						  </td>
						  <td class="principalLabel">
						    <bean:message key="prompt.CodAgencia" />
						  </td>
						  <td class="principalLabel">
						    <bean:message key="prompt.conta" />
						  </td>
						</tr>
						<tr height="11"> 
						  <td>
						    <html:text property="pessCdBanco" styleClass="principalObjForm" readonly="true" />
						  </td>
						  <td> 
						    <html:text property="pessCdAgencia" styleClass="principalObjForm" maxlength="10" />
						  </td>
						  <td> 
						    <html:text property="pessDsConta" styleClass="principalObjForm" maxlength="20" />
						  </td>
						</tr>
						<tr height="11"> 
						  <td class="principalLabel" width="25%">
						    <bean:message key="prompt.titularidade" />
						  </td>
						  <td class="principalLabel">
						    <bean:message key="prompt.cpftitular" />
						  </td>
						  <td class="principalLabel">
						    <bean:message key="prompt.rgtitular" />
						  </td>
						</tr>
						<tr height="11"> 
						  <td>
						    <html:text property="pessDsTitularidade" styleClass="principalObjForm"  maxlength="50" />
						  </td>
						  <td> 
						    <html:text property="pessDsCpfTitular" styleClass="principalObjForm" maxlength="20" onblur="ConfereCIC(this);" />
						  </td>
						  <td> 
						    <html:text property="pessDsRgTitular" styleClass="principalObjForm" maxlength="20" />
						  </td>
						</tr>
					  </table>
                     </div>
                    </div>
                  </td>
                  <td width="4" height="230"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
                <tr> 
                  <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                  <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="right"><img src="webFiles/images/icones/perfil01.gif" title="<bean:message key="prompt.perfil" />" width="21" height="25" onClick="showModalDialog('Perfil.do?idPessCdPessoa=' + contatoForm.idPessCdPessoa.value, 0, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand"></td>
                  <td class="principalLabelValorFixo" width="5%" onClick="showModalDialog('Perfil.do?idPessCdPessoa=' + contatoForm.idPessCdPessoa.value, 0, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px')"><span class="geralCursoHand">&nbsp;<bean:message key="prompt.perfil" /></span></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalPstQuadro" width="166"><bean:message key="prompt.contatosreg" /></td>
                  <td class="principalQuadroPstVazia"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
		          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="2" valign="top"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
                </tr>
                <tr> 
                  <td valign="top" class="principalBgrQuadro" height="49px"><!--Jonathan | Adequa��o para o IE 10-->
                    <iframe name="contatos" src="ShowContatoList.do?tela=<%= MCConstantes.TELA_LST_CONTATOS %>" width="100%" height="49px" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  </td>
                  <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
                <tr> 
				  <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				  <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
			    <tr> 
			      <td colspan="2">
					<table width="99%" border="0" cellspacing="0" cellpadding="5" align="right">
				      <tr>
				       <td>
				        <div align="right"> 
				          
					        <img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onclick="Save()">
					     
						  <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="cancelar()" class="geralCursoHand"></td>
					    </div>
					   </td>
					  </tr>
					</table>
			      </td>
			    </tr>
              </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
	  <div align="right"><img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></div>
    </td>
  </tr>
</table>

<script>
verificaFisicaJuridica();
	
if (contatoForm.acao.value == "<%= Constantes.ACAO_GRAVAR %>"){
	preencheSexo();
	calcage(contatoForm.pessDhNascimento.value);
}
</script>
<div id="especialidadeHidden"></div>
</html:form>
</body>
</html>