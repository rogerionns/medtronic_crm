<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="telForm">
	<html:hidden property="acao" />
	<html:hidden property="idEnderecoVector" />
	<html:hidden property="idTelefoneVector"/>	
	<html:hidden property="idTelefonePessoaVector"/>	
<head>
<title>ifrmFormaContato</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
</head>
<script language="JavaScript">
	function inicio(){
		showError("<%=request.getAttribute("msgerro")%>");
	}

</script>
<body class="principalBgrPageIfrm" text="#000000" onload="inicio();">
<table border="0" cellspacing="0" cellpadding="0" align="right" width="97%" height="150">
  <tr> 
    <td class="principalLabelOptChk" colspan="7" height="110" valign="top">
		<iframe name="LstFormaContato" src="<%= request.getAttribute("name_input").toString() %>?tela=<%=MCConstantes.TELA_LST_TELEFONE%>" width="100%" height="100%" scrolling="default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
  </tr>
</table>
</body>
<script>
function disable_en(vr){
	for (x = 0;  x < telForm.elements.length;  x++)
	{
		Campo = telForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = vr;
		}
	}
}

disable_en(parent.disable_tel);

</script>
</html:form>