<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//Chamado: 80921 - Carlos Nunes - 27/03/2012
final String TIPO_LOGRADOURO_COMBO = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TIPO_LOGRADOURO_COMBO,request);

%>

<html>
<head>
<title>ifrmDadosPessoa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src='webFiles/funcoes/TratarDados.js'></script>
<script>

//Chamado: 80921 - Carlos Nunes - 27/03/2012
var sBrowser = navigator.userAgent.toLowerCase() ;

function isFirefox(){
	return (sBrowser.indexOf("mozilla") > -1);
}

	var novo="new";
	var editar="edit";
	var cancel_confirm = "c_Confirm";
	var remove_edit = "removeOrEdit";
	var padrao = "padrao";
	
    function Acao(act){ 
                try{ 
                        window.top.superior.MM_showHideLayers('Pessoa','','hide','Manifestacao','','show','Informacao','','hide','Pesquisa','','hide');

                        window.top.superior.MM_showHideLayers('Pessoa','','show','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide');

                }catch(x){} 
                endForm.acao.value = act; 
                endForm.submit(); 
        } 
        
    function Edit(){ 
            Enable(); 
            endForm.acao.value = "<%= Constantes.ACAO_GRAVAR %>"; 
            acoes_nav(edit); 
            cmbTpEndereco.cmbForm.idTpenCdTpendereco.disabled=false; 
            
            try{ 
                    window.top.superior.MM_showHideLayers('Pessoa','','hide','Manifestacao','','show','Informacao','','hide','Pesquisa','','hide');

                    window.top.superior.MM_showHideLayers('Pessoa','','show','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide');

            }catch(x){} 
            
            disable_tel = false; 
//              ifrmFormaContato.disable_en(disable_tel); 
    } 
 
	function Submit(){

		//Chamado 74837 - Vinicius - Torna o tipo de endere�o obrigat�rio
		if(cmbTpEndereco.cmbForm.idTpenCdTpendereco.value == 0 || cmbTpEndereco.cmbForm.idTpenCdTpendereco.value == ""){
			alert('<%= getMessage("prompt.Selecione_o_tipo_de_endereco", request)%>');
			return false;
		}
		
		var campoPreenchido = false;
		for (var i = 0;  i < endForm.elements.length;  i++)	{
			Campo = endForm.elements[i];
			if  (Campo.type == "text") {
				if (Campo.value != "")
					campoPreenchido = true;
			}
		}

		if (campoPreenchido) {
			endForm.idTpenCdTpendereco.value = cmbTpEndereco.cmbForm.idTpenCdTpendereco.value;
			endForm.submit();
		} else {
			alert("<bean:message key="prompt.alert.campo.obrigatorio" />")
			endForm.peenDsLogradouro.focus();
		}
	}
	function Remove(){
		msg = '<bean:message key="prompt.alert.remov.endereco" />';
		if (endForm.idEnderecoVector.value != -1){
			if (confirm(msg)) {
				endForm.acao.value = "<%= Constantes.ACAO_EXCLUIR %>";
				endForm.submit();
			}
		}
	}
	function Enable(){
		disable_end(false);
	}
	
	function disable_end(en){
		for (x = 0;  x < endForm.elements.length;  x++)
		{
			Campo = endForm.elements[x];
			if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
				Campo.disabled = en;
			}
		}
/*			document.all.item('lupaCep').disabled = en;
			if (en)
				document.all.item('lupaCep').className = 'desabilitado';
			else
				document.all.item('lupaCep').className = 'geralCursoHand';
			document.all.item('lupaBairro').disabled = en;
			if (en)
				document.all.item('lupaBairro').className = 'desabilitado';
			else
				document.all.item('lupaBairro').className = 'geralCursoHand';
			document.all.item('lupaEndereco').disabled = en;
			if (en)
				document.all.item('lupaEndereco').className = 'desabilitado';
			else
				document.all.item('lupaEndereco').className = 'geralCursoHand';*/
			disable_tel = en;
	}
	
	var disable_tel = true;
	
	//Chamado: 80921 - Carlos Nunes - 27/03/2012
	function telefone() {
		<%if(String.valueOf(request.getSession().getAttribute("modulo")).equalsIgnoreCase("workFlow")){%>
			window.parent.ifrmFormaContato.location.href = "../../../csiworkflow/<%= request.getAttribute("name_input").toString() %>";
		<%}else{%>
			window.parent.ifrmFormaContato.location.href = "../../csiworkflow/<%= request.getAttribute("name_input").toString() %>";
		<%}%>
	}
	
	function inicio(){
		showError("<%=request.getAttribute("msgerro")%>");
		telefone();
		
		//Chamado: 80921 - Carlos Nunes - 27/03/2012
		var tipo = "";
	      
        if(isFirefox())
        {
        	tipo = endForm.idTplgCdTplogradouro[endForm.idTplgCdTplogradouro.selectedIndex].text;
        }
        else
        {
        	tipo = endForm.idTplgCdTplogradouro[endForm.idTplgCdTplogradouro.selectedIndex].innerText;
        }
        
		var rua = endForm.peenDsLogradouro.value;
		
		<%	if (TIPO_LOGRADOURO_COMBO.equals("S")) {	%>
			document.getElementById("divTpLogradouro").style.display = "block";
			endForm.peenDsLogradouro.style.width = "215px";
			document.all.item('peenDsLogradouro').value = rua.substring(tipo.length, rua.length);
		<%	}else{ %>
				document.getElementById("divTpLogradouro").style.display = "none";
				endForm.peenDsLogradouro.style.width = "305px";
				document.all.item('peenDsLogradouro').value = rua;
		<%	} %>
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="inicio();">
<html:form action='<%= request.getAttribute(\"name_action\").toString() %>' styleId="endForm">
	<html:hidden property="idEnderecoVector" />
	<html:hidden property="idTpenCdTpendereco" />
	<html:hidden property="idPeenCdEndereco"/>
	<html:hidden property="acao" />

	<input type="hidden" name="logradouro" value="peenDsLogradouro">
	<input type="hidden" name="bairro" value="peenDsBairro">
	<input type="hidden" name="municipio" value="peenDsMunicipio">
	<input type="hidden" name="estado" value="peenDsUf">
	<input type="hidden" name="cep" value="peenDsCep">
	<input type="hidden" name="ddd" value="">
	
  <table border="0" cellspacing="1" cellpadding="0" align="center">
    <tr> 
      <td class="principalLabel" colspan="6"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="10"></td>
      <td valign="top" class="principalLabel" width="390">&nbsp; </td>
    </tr>
    <tr> 
      <td class="principalLabel" width="223"><bean:message key="prompt.tipoendereco" /></td>
      <td class="principalLabel" colspan="6"></td>
    </tr>
    <tr> 
      <td class="principalLabel" width="223"> 
 		<iframe name="cmbTpEndereco" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
 		<script> 
 		    //Chamado: 80921 - Carlos Nunes - 27/03/2012
 			setTimeout("cmbTpEndereco.location.href='ShowPessCombo.do?tela=<%=MCConstantes.TELA_CMB_TP_ENDERECO%>&idTpenCdTpendereco=" + endForm.idTpenCdTpendereco.value + "';" ,100);
 		</script>
 	</td>
      <td class="principalLabel" colspan="5">
     	<html:checkbox property="peenInPrincipal" disabled="true" value="true"> </html:checkbox> 
        <bean:message key="prompt.principal" /></td>
    </tr>
    <tr> 
      <td  class="principalLabel" colspan="5">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalLabel" width="310">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="principalLabel"><bean:message key="prompt.endereco" /></td>
							</tr>
						</table>
					</td>
					<td class="principalLabel">
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td width="40%" class="principalLabel"><bean:message key="prompt.numero" /></td>
								<td class="principalLabel"><bean:message key="prompt.complemento" /></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
    </tr>
    <tr>
    	<td colspan="5">
    		 <table width="100%" border="0" cellspacing="0" cellpadding="0">
    		 	<tr> 
			      <td class="principalLabel" width="223">
			        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			          <tr> 
			          	<td>
						   <div id="divTpLogradouro" style="display: none;">
						     <html:select property="idTplgCdTplogradouro" styleId="idTplgCdTplogradouro" disabled="true" styleClass="principalObjForm" style="width:90px;">
							    <html:option value="0">&nbsp;</html:option>
								<logic:present name="tpLogradouroVector">
									<html:options collection="tpLogradouroVector" property="field(id_tplg_cd_tplogradouro)" labelProperty="field(tplg_ds_tplogradouro)"/>
								</logic:present>
							</html:select>
						   </div>				
						</td>
			            <td>
			            <%//Chamado 72952 - Vinicius - O Campo logradouro teve o tamanho aumentado de 100 para 255 caracteres%>
			            <html:text property="peenDsLogradouro" styleClass="principalObjForm" disabled="true" maxlength="255" style="width:215;"/>
			            </td>
			            <td>
			              &nbsp;<!--img id="lupaEndereco" disabled="true" src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="desabilitado" onClick="showModalDialog('PlusCep.do?tipo=endereco&ddd=true',endForm,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px');parent.ifrmFormaContato.document.all('telDDD').value = endForm.ddd.value;" border="0"-->
			            </td>
			          </tr>
			        </table>
			      </td>
			      <td class="principalLabel" width="65"> 
			     	<html:text property="peenDsNumero" styleClass="principalObjForm" disabled="true" maxlength="10" style="width:75;" />
			      </td>
			      <td class="principalLabel" colspan="4"> 
			        <html:text property="peenDsComplemento" styleClass="principalObjForm" disabled="true" maxlength="50" style="width:95;"/>
			      </td>
			      
			    </tr>
    		 </table>
    	</td>
    </tr>
    
    <tr> 
      <td class="principalLabel" width="223"><bean:message key="prompt.bairro" /></td>
      <td class="principalLabel" width="60"><bean:message key="prompt.cidade" /></td>
      <td class="principalLabel" width="70">&nbsp;</td>
      <td class="principalLabel" width="85">&nbsp;</td>
      <td class="principalLabel" colspan="2"><bean:message key="prompt.uf" /></td>
    </tr>
    <tr> 
      <td class="principalLabel" width="223">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="91%">
            	<html:text property="peenDsBairro" styleClass="principalObjForm" disabled="true" maxlength="60" style="width:215;"/>
            </td>
            <td width="9%">
              &nbsp;<!--img id="lupaBairro" disabled="true" src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="desabilitado" title="<bean:message key="prompt.buscaCep"/>" onClick="showModalDialog('PlusCep.do?tipo=bairro&ddd=true',endForm,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px');parent.ifrmFormaContato.document.all('telDDD').value = endForm.ddd.value;" border="0"-->
            </td>
          </tr>
        </table>
      </td>
      <td class="principalLabel" colspan="3"> 
         <html:text property="peenDsMunicipio" styleClass="principalObjForm" disabled="true" maxlength="80" style="width:230;"/>
      </td>
      <td class="principalLabel" width="45"> 
        <html:text property="peenDsUf" styleClass="principalObjForm" disabled="true" maxlength="3" />
      </td>
      <td class="principalLabel" width="22" align="center">&nbsp;</td>
    </tr>
    <tr> 
      <td class="principalLabel" width="223"><bean:message key="prompt.cep" /></td>
      <td class="principalLabel" width="65"><bean:message key="prompt.pais" /></td>
      <td class="principalLabel" width="70">&nbsp;</td>
      <td class="principalLabel" colspan="3"><bean:message key="prompt.referencia" /></td>
    </tr>
    <tr> 
      <td class="principalLabel" width="223"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td > 
	            <html:text property="peenDsCep" styleClass="principalObjForm" disabled="true" onfocus="SetarEvento(this,'N')" maxlength="8" style="width:215;"/>
            </td>
            <td >
              &nbsp;<!--img id="lupaCep" disabled="true" src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="desabilitado" onClick="showModalDialog('PlusCep.do?tipo=cep&ddd=true',endForm,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px');parent.ifrmFormaContato.document.all('telDDD').value = endForm.ddd.value;" border="0"-->
            </td>
          </tr>
        </table>
      </td>
      <td class="principalLabel" align="center" colspan="2"> 
        <div align="left"> 
          <html:text property="peenDsPais" styleClass="principalObjForm" disabled="true" maxlength="60" style="width:140;" />
          <script>
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SOMENTEBRASIL,request).equals("S")) {%>
					endForm.peenDsPais.value = 'BRASIL';
			<%}%>
		  </script>
        </div>
      </td>
      <td class="principalLabel" colspan="2"> 
        <html:text property="peenDsReferencia" styleClass="principalObjForm" disabled="true" maxlength="255" style="width:136;"/>
      </td>
      <td class="principalLabel" width="22">&nbsp;</td>
    </tr>
    
    
    
    <tr> 
      <td class="principalLabel" width="223"><bean:message key="prompt.caixaPostal" /></td>
      <td class="principalLabel" width="65">&nbsp;</td>
      <td class="principalLabel" width="70">&nbsp;</td>
      <td class="principalLabel" colspan="3">&nbsp;</td>
    </tr>
    
    <tr> 
      <td class="principalLabel" width="223"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td > 
	            <html:text property="peenDsCaixaPostal" styleClass="principalObjForm" disabled="true" maxlength="80" style="width:215;"/>
            </td>
            <td >
              &nbsp;<!--img id="lupaCep" disabled="true" src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="desabilitado" onClick="showModalDialog('PlusCep.do?tipo=cep&ddd=true',endForm,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px');parent.ifrmFormaContato.document.all('telDDD').value = endForm.ddd.value;" border="0"-->
            </td>
          </tr>
        </table>
      </td>
      <td class="principalLabel" align="center" colspan="2"> 
        &nbsp;
      </td>
      <td class="principalLabel" colspan="2"> 
        &nbsp;
      </td>
      <td class="principalLabel" width="22">&nbsp;</td>
    </tr>
    
    
    
    <tr> 
      <td class="principalLabel" width="223">&nbsp; </td>
      <td class="principalLabel" valign="bottom" align="right" colspan="5"> 
        <table border="0" cellspacing="0" cellpadding="5" width="70%">
          <tr> 
            <td>
            	<div id="nova">
	           		<!--img src="webFiles/images/botoes/new.gif" width="14" height="16" class="geralCursoHand" title="<bean:message key="prompt.novoCadastro"/>" onClick="javascript:Acao('<%=Constantes.ACAO_INCLUIR%>');"-->
	           	</div>
            </td>
            <td>
            	<div id="edit">
            		<!--img src="webFiles/images/botoes/editar.gif" width="16" height="16" class="geralCursoHand" title="<bean:message key="prompt.alterarCadastro"/>" onClick="javascript:Edit();"-->
            	</div>
            </td>

            <td>
            	<div id="left">
		            <img src="webFiles/images/botoes/setaLeft.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.itemAnterior"/>" onClick="javascript:Acao('<%= MCConstantes.ACAO_LAST %>');">
		    	</div>
		    </td>
            <td>
            	<div id="right">
		            <img src="webFiles/images/botoes/setaRight.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.proximoItem"/>" border="0" onClick="javascript:Acao('<%= MCConstantes.ACAO_NEXT %>');">
		        </div>
		    </td>
            <td>
            	<div id="remove">
			        <!--img src="webFiles/images/botoes/lixeira18x18.gif" width="18" height="18" class="geralCursoHand" title="<bean:message key="prompt.excluirItem"/>" onClick="javascript:Remove();" -->
			    </div>
		    </td>
            <td>
            	<div id="confirma">
	            	<!--img src="webFiles/images/botoes/confirmaEdicao.gif" width="21" height="18" class="geralCursoHand" title="Confirmar Altera&ccedil;&atilde;o" onClick="javascript:Submit();"-->
            	</div>
            </td>
            <td>
            	<div id="cancel">
		            <!--img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="Cancelar Altera&ccedil;&atilde;o" onClick="javascript:Acao('<%= Constantes.ACAO_CANCELAR %>');" -->
		    	</div>
		    </td>
          </tr>
        </table>
        <div id="cont" style="position: absolute; left: 370px; top: 180px;">
          <table>
           <tr>
            <td class="principalLabel">
	          <bean:write name="baseForm" property="posicao" /> / <bean:write name="baseForm" property="tamanho" />
            </td>
           </tr>
          </table>
      </td>
    </tr>
  </table>
</div>
  <script>
    function preencheDDD(ddd) {

    }
    
	function acoes_nav(tipo){
		if (tipo==edit){
			nova.style.visibility ='hidden';
	        edit.style.visibility='hidden';
			confirma.style.visibility='visible';
			cancel.style.visibility='visible';
			left.style.visibility='hidden';
			cont.style.visibility='hidden';
			right.style.visibility='hidden';
			remove.style.visibility='hidden';
		}
		if (tipo==novo){
			nova.style.visibility='hidden';
	        edit.style.visibility='hidden';
			confirma.style.visibility='visible';
			cancel.style.visibility='visible';
			left.style.visibility='hidden';
			cont.style.visibility='hidden';
			right.style.visibility='hidden';
			remove.style.visibility='hidden';
		}
		if(tipo==cancel_confirm){
			nova.style.visibility="visible";
			
	        edit.style.visibility="hidden";
	        
			confirma.style.visibility="hidden";
			cancel.style.visibility="hidden";
			left.style.visibility="vidible";
			cont.style.visibility="vidible";
			right.style.visibility="vidible";
			
			remove.style.visibility="hidden";
		}
		if(tipo==remove_edit){
			nova.style.visibility="visible";
	        edit.style.visibility="visible";
			confirma.style.visibility="hidden";
			cancel.style.visibility="hidden";
			left.style.visibility="visible";
			cont.style.visibility="visible";
			right.style.visibility="visible";
			remove.style.visibility="visible";
		}
		if(tipo==padrao){
			nova.style.visibility="visible";
			
	        edit.style.visibility="hidden";
	        
			confirma.style.visibility="hidden";
			cancel.style.visibility="hidden";
			left.style.visibility="hidden";
			cont.style.visibility="hidden";
			right.style.visibility="hidden";
			
			remove.style.visibility="hidden";
		}
	}
  	// A tela ja esta em novo
  	function verif(){
		if (endForm.idEnderecoVector.value != -1){
			acoes_nav(remove_edit);
		}else{
			acoes_nav(padrao);
		}
	  	if (endForm.acao.value=="<%= Constantes.ACAO_EDITAR %>"){
		  	Enable();
		  	endForm.acao.value="<%= Constantes.ACAO_GRAVAR %>";
		  	acoes_nav(novo);
		}

	}
	verif();

	</script>
 </html:form>
</body>
</html>