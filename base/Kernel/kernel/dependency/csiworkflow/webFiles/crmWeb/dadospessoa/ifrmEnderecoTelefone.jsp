<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmDadosPessoa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/global.css" type="text/css">
<script language="JavaScript" src="../../<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="../../js/funcoesMozilla.js"></script>
<script language='javascript' src='../../funcoes/TratarDados.js'></script>
</head>

<script language="JavaScript">
	function inicio(){
		showError("<%=request.getAttribute("msgerro")%>");
		endLoad();
	}

	function endLoad() {
		<%if(String.valueOf(request.getSession().getAttribute("modulo")).equalsIgnoreCase("workFlow")){%>
			ifrmEndereco.location="../../../EnderecoPess.do"
		<%}else{%>
			ifrmEndereco.location="../../EnderecoPess.do"
		<%}%>
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="inicio();">
  <table border="0" cellspacing="1" cellpadding="0" align="center" height="100%">
    <tr height="100%"> 
      <td width="65%"> 
        <iframe name="ifrmEndereco" id="ifrmEndereco" 
        	src=""
        width="99%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </td>
      <td width="35%"> 
        <iframe name="ifrmFormaContato"
        <%if(String.valueOf(request.getSession().getAttribute("modulo")).equalsIgnoreCase("workFlow")){%>
        	src="../../../TelefonePess.do"
        <%}else{%>
        	src="../../TelefonePess.do"
        <%}%>
        width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </td>
    </tr>
  </table>
</body>
</html>