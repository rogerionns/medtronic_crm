<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language='javascript' src='<bean:message key="prompt.funcoes"/>/util.js'></script>
<script language="JavaScript" src='<html:rewrite page="/webFiles/includes/validator.jsp"/>'></script> 
<script language="JavaScript">

var nLinhaAltEspecMedica = new Number(0);

function validaCamposEspec(){

	 return true;
	
}


function isFormEspec(){

      return true;
}

function setValoresToForm(form){	

	var idPessCdPessoa = 0;
	if(parent.pessoaForm == undefined){
		idPessCdPessoa = parent.contatoForm.idPessCdPessoa.value;
	}else{
		idPessCdPessoa = parent.pessoaForm.idPessCdPessoa.value;
	}

	strTxt = "";

	strTxt += " <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.0.CS_CDTB_PESSOAFARMACO_PEFA.pefa_ds_conselhoprofissional\" value=\"" + pessoaFarmacoForm.pefaDsConselhoprofissional.value + "\" > ";
	strTxt += " <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.0.CS_CDTB_PESSOAFARMACO_PEFA.id_pess_cd_pessoa\" value=\"" + idPessCdPessoa + "\" > ";
	strTxt += " <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.0.CS_CDTB_PESSOAFARMACO_PEFA.id_prof_cd_profissao\" value=\"" + pessoaFarmacoForm.idProfCdProfissao.value + "\" > ";
	strTxt += " <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.0.CS_CDTB_PESSOAFARMACO_PEFA.pefa_ds_empresatrabalha\" value=\"" + pessoaFarmacoForm.pefaDsEmpresatrabalha.value + "\" > ";

	strTxt += " <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.0.CS_CDTB_PESSOAFARMACO_PEFA.entityName\" value=\"br/com/plusoft/csi/crm/dao/xml/CS_CDTB_PESSOAFARMACO_PEFA.xml\" > ";
	
	/*
	if (pessoaFarmacoForm.idEspeCdEspecialidadeArray != null) {
		if (pessoaFarmacoForm.idEspeCdEspecialidadeArray.length == undefined) {
			parent.document.getElementById("especialidadeHidden").innerHTML = '<input type="hidden" name="idEspeCdEspecialidade" value="' + pessoaFarmacoForm.idEspeCdEspecialidadeArray.value + '">';
		} else {
			for (var i = 0; i < pessoaFarmacoForm.idEspeCdEspecialidadeArray.length; i++) {
				parent.document.getElementById("especialidadeHidden").innerHTML += '<input type="hidden" name="idEspeCdEspecialidade" value="' + pessoaFarmacoForm.idEspeCdEspecialidadeArray[i].value + '"> ';
			}
		}
	}
	*/
	parent.document.getElementById("camposDetalhePessoaEspec").innerHTML += document.getElementById("divEspecmedica").innerHTML;			
	parent.document.getElementById("camposDetalhePessoaEspec").innerHTML += strTxt;

}

function adicionarItem(){

	var idEspecMedica = document.getElementById("pessoaFarmacoForm").idEspeCdEspecialidade.value;

	if(idEspecMedica > 0){
           if (document.all['csCdtbPessoaespecPeesVo.1.CS_ASTB_PESSESPECIALIDADE_PEES.id_espe_cd_especialidade'] != undefined){
                      for (i=0;i<=pessoaFarmacoForm.hdnTotalLinhasEspecMedica.value;i++){
                            if (document.all['csCdtbPessoaespecPeesVo.' + [i] + '.CS_ASTB_PESSESPECIALIDADE_PEES.id_espe_cd_especialidade'] != undefined){
                                  if(document.all['csCdtbPessoaespecPeesVo.' + [i] + '.CS_ASTB_PESSESPECIALIDADE_PEES.id_espe_cd_especialidade'].value==idEspecMedica){
                                        alert("<bean:message key='prompt.aListaJaContemTipoEspecialidadeMedicaSelecionada'/>");
                                        pessoaFarmacoForm.idEspeCdEspecialidade.value="";
                                        return false;
                                  }
                            }
                      }
            }
   		addItem(document.getElementById("pessoaFarmacoForm").idEspeCdEspecialidade.value,document.getElementById("pessoaFarmacoForm").idEspeCdEspecialidade[document.getElementById("pessoaFarmacoForm").idEspeCdEspecialidade.selectedIndex].text);       
	}else{
		alert("<bean:message key='prompt.eNecessarioSelecionarEspecialidadeMedica'/>");
	}
	
}

function comparaChave(cFunc) {
}

nLinha = new Number(0);

function addItem(idEspecmedica, dsEspecmedica) {

	var idPessCdPessoa = 0;
	if(parent.pessoaForm == undefined){
		idPessCdPessoa = parent.contatoForm.idPessCdPessoa.value;
	}else{
		idPessCdPessoa = parent.pessoaForm.idPessCdPessoa.value;
	}

	var nLinha = new Number(pessoaFarmacoForm.hdnTotalLinhasEspecMedica.value);
	nLinha = nLinha + 1;
	
	strTxt = "";
	strTxt += "<table id=\"especMedica" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
    strTxt += "  <tr>";
    
	strTxt += "		 <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo." + nLinha + ".CS_ASTB_PESSESPECIALIDADE_PEES.id_pess_cd_pessoa\" value=\"" + idPessCdPessoa + "\" > ";
	strTxt += "		 <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo." + nLinha + ".CS_ASTB_PESSESPECIALIDADE_PEES.id_espe_cd_especialidade\" value=\"" + idEspecmedica + "\" > ";
	strTxt += "		 <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo." + nLinha + ".CS_ASTB_PESSESPECIALIDADE_PEES.entityName\" value=\"br/com/plusoft/csi/crm/dao/xml/CS_ASTB_PESSESPECIALIDADE_PEES.xml\" > ";

	strTxt += " 	<td class=\"principalLstPar\" width=\"3%\"><img src=\"webFiles/images/botoes/lixeira.gif\" width=\"14\" height=\"14\" class=\"geralCursoHand\" onclick=\"removeItem('" + nLinha + "')\" ></td> ";
	strTxt += "     <td class=\"principalLstPar\" width=\"97%\">" + dsEspecmedica + "</td> ";

	strTxt += "	 </tr> ";
	strTxt += " </table> ";
	
	
	document.getElementsByName("divEspecmedica").innerHTML += strTxt;

	pessoaFarmacoForm.hdnTotalLinhasEspecMedica.value = nLinha;      
    pessoaFarmacoForm.hdnTotalEspecMedica.value = pessoaFarmacoForm.hdnTotalEspecMedica.value + 1;

	pessoaFarmacoForm.idEspeCdEspecialidade.value = "";
}


function removeItem(nTblExcluir) {
  if (confirm("<bean:message key='prompt.desejaExcluirEstaEspecialidadeMedica'/>")) {
		objIdTbl = window.document.getElementById("especMedica" + nTblExcluir);
		divEspecmedica.removeChild(objIdTbl);
		
		pessoaFarmacoForm.hdnTotalEspecMedica.value = pessoaFarmacoForm.hdnTotalEspecMedica.value - 1;
	}
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000">
<html:form action="/PessoaFarmaco" styleId="pessoaFarmacoForm">
<input type="hidden" name="hdnTotalEspecMedica" />
<input type="hidden" name="hdnTotalLinhasEspecMedica" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="principalLabel">&nbsp;</td>
  </tr>
</table>
<table width="60%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="30%" align="right" class="principalLabel" height="25"><bean:message key="prompt.conselhoProf" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td width="70%" class="principalLabel"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="36%"> 
            <html:text property="pefaDsConselhoprofissional" styleClass="principalObjForm" maxlength="20" style="width:120px"/>
          </td>
          <td width="64%">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="30%" align="right" class="principalLabel" height="25"><bean:message key="prompt.profissao" /> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
    <td width="70%" class="principalLabel"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="65%"> 
            <html:select property="idProfCdProfissao" styleClass="principalObjForm" style="width:215px">
				<html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="vetorProfissao">
					<html:options collection="vetorProfissao" property="idProfCdProfissao" labelProperty="profDsProfissao"/>
				</logic:present>
			</html:select> 
          </td>
          <td width="35%">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="30%" align="right" class="principalLabel" height="25"><bean:message key="prompt.empresaTrabalha" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td width="70%" class="principalLabel"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="65%"> 
            <html:text property="pefaDsEmpresatrabalha" styleClass="principalObjForm" maxlength="60" style="width:215px"/>
          </td>
          <td width="35%">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="30%" align="right" class="principalLabel" height="25"><bean:message key="prompt.especialidadeMedica" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td width="70%" class="principalLabel"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="65%"> 
             <html:select property="idEspeCdEspecialidade" styleClass="principalObjForm" style="width:215px">
				<html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="vetorEspecialidade">
					<html:options collection="vetorEspecialidade" property="idEspeCdEspecialidade" labelProperty="espeDsEspecialidade"/>
				</logic:present>
			</html:select> 
          </td>
          <td width="35%"><img src="webFiles/images/botoes/setaDown.gif" title="<bean:message key="prompt.confirmar" />" width="21" height="18" class="geralCursoHand" onclick="adicionarItem()"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="60%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
  <tr> 
    <td width="30%">&nbsp;</td>
  </tr>
</table>
<table width="60%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="30%">&nbsp;</td>
    <td width="70%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="65%" height="80" valign="top" class="principalBordaQuadro"> 
            <div id="divEspecmedica" style="position:absolute; width:100%; height:80px; z-index:1; overflow: auto">
                <logic:present name="vetorListaEspecialidade">
                  <logic:iterate name="vetorListaEspecialidade" id="vetorListaEspecialidade">
                    <script>
                    addItem('<bean:write name="vetorListaEspecialidade" property="idEspeCdEspecialidade" />', '<bean:write name="vetorListaEspecialidade" property="espeDsEspecialidade" />');
                    </script>
                  </logic:iterate>
                </logic:present>
            </div>
          </td>
          <td width="35%">&nbsp;</td>
        </tr>
      </table>
      
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
