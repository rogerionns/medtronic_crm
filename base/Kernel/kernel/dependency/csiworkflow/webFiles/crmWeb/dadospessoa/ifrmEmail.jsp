<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="mailForm">
	<html:hidden property="idEmailVector"/>
	<html:hidden property="acao"/>

<head>
<title>ifrmEmail</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
</head>
<script language="JavaScript">
	function AdicinarEmail(cEmail,cPrincipal,NrLinha){
		if (cEmail == ""){
			alert('<bean:message key="prompt.alert.email" />');
			document.all('pessEmail').focus();
			return false;
		}else{
			if (cEmail.search(/\S/) != -1) {
				//regExp = /[A-Za-z0-9_]+@[A-Za-z0-9_]{2,}\.[A-Za-z]{2,}/
				//Danilo Prevides - 04/09/2009 - 66241 - 29/10/2009 - 67200 -  INI 
				regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{1,}\.[A-Za-z0-9]{2,}/
				//Danilo Prevides - 04/09/2009 - 66241 - 29/10/2009 - 67200 - FIM				
				if (cEmail.length < 7 || cEmail.search(regExp) == -1){
					alert ('<bean:message key="prompt.alert.email.correto" />');
				    document.all('pessEmail').focus();
				    return false;
				}						
			}
			num1 = cEmail.indexOf("@");
			num2 = cEmail.lastIndexOf("@");
			if (num1 != num2){
			    alert ('<bean:message key="prompt.alert.email.correto" />');
			    document.all('pessEmail').focus();
				return false;
			}
		}
		if (mailForm.acao.value != "<%=Constantes.ACAO_GRAVAR%>"){
			mailForm.acao.value = "<%= Constantes.ACAO_INCLUIR %>";
		}
		mailForm.submit();
	}
	
	function excluir(num){
		mailForm.acao.value = "<%= Constantes.ACAO_EXCLUIR%>";
		mailForm.idEmailVector.value = num;
		mailForm.submit();
	}
	
	function inicio(){
		showError("<%=request.getAttribute("msgerro")%>");
	}
</script>
<body class="principalBgrPageIFRM" text="#000000" onload="inicio();">
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td height="50" valign="top"><iframe name="LstEMail" src="<%= request.getAttribute("name_input").toString() %>?tela=<%=MCConstantes.TELA_LST_MAIL%>" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
  </tr>
</table>
</body>

<script>
function disab() {
	for (x = 0;  x < mailForm.elements.length;  x++) {
		Campo = mailForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}	 
}

</script>
</html:form>