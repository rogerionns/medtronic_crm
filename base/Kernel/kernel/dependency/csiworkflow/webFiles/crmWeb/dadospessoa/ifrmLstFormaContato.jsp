<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="listForm">
<script language="JavaScript">
<!--
nLinha = new Number(0);

function RemoveTel(nTblExcluir,telEnd){
	msg = '<bean:message key="prompt.alert.remov.telefone" />';
	if (confirm(msg)) {
		parent.excluir(nTblExcluir,telEnd);
	}
}
function EditContato (nLinha){
	cDDI = eval("listForm.EcDDI" + nLinha + ".value");
	cDDD = eval("listForm.EcDDD" + nLinha + ".value");
	cNumero = eval("listForm.EcNumero" + nLinha + ".value");
	cRamal = eval("listForm.EcRamal" + nLinha + ".value");
	cTipo = eval("listForm.EcTipo" + nLinha + ".value");
	cPrincipal = eval("listForm.EcPrincipal" + nLinha + ".value");

	window.parent.telForm.telTipo.value = cTipo;
	window.parent.telForm.telDDI.value = cDDI;
	window.parent.telForm.telDDD.value = cDDD;
	window.parent.telForm.telTelefone.value = cNumero;
	window.parent.telForm.telRamal.value = cRamal;
	if (cPrincipal == "true"){
		window.parent.telForm.telInPrincipal.checked = true;
	}else{
		window.parent.telForm.telInPrincipal.checked = false;
	}
	window.parent.telForm.telefoneEndereco.checked = true;
	window.parent.telForm.NrLinha.value = nLinha;
	parent.telForm.idTelefoneVector.value = nLinha;
	parent.telForm.idTelefonePessoaVector.value = "-1";
	parent.telForm.acao.value = "<%= Constantes.ACAO_GRAVAR %>";
}

function EditContatoPess (nLinha){
	cDDI = eval("listForm.cDDI" + nLinha + ".value");
	cDDD = eval("listForm.cDDD" + nLinha + ".value");
	cNumero = eval("listForm.cNumero" + nLinha + ".value");
	cRamal = eval("listForm.cRamal" + nLinha + ".value");
	cTipo = eval("listForm.cTipo" + nLinha + ".value");
	cPrincipal = eval("listForm.cPrincipal" + nLinha + ".value");

	window.parent.telForm.telTipo.value = cTipo;
	window.parent.telForm.telDDI.value = cDDI;
	window.parent.telForm.telDDD.value = cDDD;
	window.parent.telForm.telTelefone.value = cNumero;
	window.parent.telForm.telRamal.value = cRamal;
	if (cPrincipal == "true"){
		window.parent.telForm.telInPrincipal.checked = true;
	}else{
		window.parent.telForm.telInPrincipal.checked = false;
	}
	window.parent.telForm.telefoneEndereco.checked = false;
	window.parent.telForm.NrLinha.value = nLinha;
	parent.telForm.idTelefoneVector.value = "-1";
	parent.telForm.idTelefonePessoaVector.value = nLinha;
	parent.telForm.acao.value = "<%= Constantes.ACAO_GRAVAR %>";
}
//-->
</script>
</head>

<body class="esquerdoBgrPageIFRM" leftmargin=0 rightmargin=0 bottommargin=0 topmargin=0 text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="principalLstCab" id="cabSeta" name="cabSeta" width="5%">&nbsp;</td>
    <td class="principalLstCab" id="cabExcluir" name="cabExcluir" width="5%">&nbsp;</td>
    <td class="principalLstCab" id="cabTipo" name="cabTipo" width="15%"><bean:message key="prompt.tipo" /></td>
    <td class="principalLstCab" id="cabDdi" name="cabDdi" width="12%"><bean:message key="prompt.ddi" /></td>
    <td class="principalLstCab" id="cabDdd" name="cabDdd" width="17%"><bean:message key="prompt.ddd" /></td>
    <td class="principalLstCab" id="cabNumero" name="cabNumero" width="25%"><bean:message key="prompt.numero" /></td>
    <td class="principalLstCab" id="cabRamal" name="cabRamal" width="18%"><bean:message key="prompt.ramal" /></td>
    <td class="principalLstCab" id="cabPrincipal" name="cabPrincipal" width="15%" align="right"><bean:message key="prompt.princ" /></td>
  </tr>
</table>

      <script>try {window.top.superiorBarra.barraFone.fonePessoa.innerText = '';} catch(e) {}</script>
<logic:iterate name="listVector" id="Telefones" indexId="numero" >	
	<table name="<bean:write name="numero"/>" id="<bean:write name="numero"/>" width=100% border=0 cellspacing=0 cellpadding=0>
		<tr class="intercalaLst<%=numero.intValue()%2%>">
			<td class=principalLstPar width="5%" align=center>
			  &nbsp;<img src="webFiles/images/separadores/pxTranp.gif" width=11 height=10>
			</td>
			<td class=principalLstPar width="2%" align=center> &nbsp;
			
			<!--img src="webFiles/images/botoes/horaContato.gif" width="15" height="16" class="geralCursoHand" onClick="showModalDialog('CsCdtbHorariocomunicHoco.do?linhaEdicao=<bean:write name="numero"/>&telefoneEnd=0',0,'help:no;scroll:no;Status:NO;dialogWidth:820px;dialogHeight:435px;dialogTop:200px;dialogLeft:150px')"--> 
            </td>
	        <td class=principalLstPar width="3%" align=center>
		  		&nbsp;<!--img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand title=Excluir Telefone onClick=RemoveTel("<bean:write name="numero"/>",false)--> 
			</td>
			<td class=principalLstPar width="15%">&nbsp;<script>acronym('<bean:write name="Telefones" property="csDmtbTpComunicacaoTpcoVo.tpcoDsTpcomunicacao"/>', 3)</script></td>
			<td class=principalLstPar width="10%" align="left">&nbsp;<script>acronym('<bean:write name="Telefones" property="pcomDsDdi"/>', 3)</script><input type="hidden" name="cDDI<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsDdi"/>"></td>
			<td class=principalLstPar width="13%" align="left">&nbsp;<script>acronym('<bean:write name="Telefones" property="pcomDsDdd"/>', 3)</script><input type="hidden" name="cDDD<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsDdd"/>"></td>
			<td class=principalLstPar width="28%">&nbsp;<script>acronym('<bean:write name="Telefones" property="pcomDsComunicacao"/>', 9)</script><input type="hidden" name="cNumero<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsComunicacao"/>"></td> 
			<td class=principalLstPar width="15%">&nbsp;<script>acronym('<bean:write name="Telefones" property="pcomDsComplemento"/>', 5)</script><input type="hidden" name="cRamal<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsComplemento"/>">
			  <input type="hidden" name="cTipo<bean:write name="numero"/>" value="<bean:write name="Telefones" property="idTpcoCdTpcomunicacao"/>">
			  <input type="hidden" name="cPrincipal<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomInPrincipal"/>">
			  <input type="hidden" name="cEndereco<bean:write name="numero"/>" value="<bean:write name="Telefones" property="telefoneEndereco"/>">
            </td>
			<td class=principalLstPar width="15%" align="center">&nbsp;
				<logic:equal property="pcomInPrincipal" name="Telefones" value="true">
					<!--img src=webFiles/images/icones/check.gif width=11 height=12 onclick="javascript:EditContatoPess(<bean:write name="numero"/>)"-->
					<img src=webFiles/images/icones/check.gif width=11 height=12>
                    <script>try {window.top.superiorBarra.barraFone.fonePessoa.innerText = '(<bean:write name="Telefones" property="pcomDsDdd" />) <bean:write name="Telefones" property="pcomDsComunicacao" />';} catch(e) {}</script>
				</logic:equal>
				<logic:notEqual property="pcomInPrincipal" name="Telefones" value="true">
					<!--img src=webFiles/images/separadores/pxTranp.gif width=11 height=12-->
				</logic:notEqual>
			</td>
		</tr>
	</table>
</logic:iterate>
<logic:iterate name="listVector2" id="Telefones" indexId="numero" >	
	<table name="E<bean:write name="numero"/>" id="E<bean:write name="numero"/>" width=100% border=0 cellspacing=0 cellpadding=0>
		<tr class="intercalaLst<%=numero.intValue()%2%>">
			<td class=principalLstPar width="5%" align=center>
			  &nbsp;<img src="webFiles/images/icones/setaAzulIndicador.gif" width=11 height=10>
			</td>
			<td class=principalLstPar width="2%" align=center>&nbsp;
			
			<!--img src="webFiles/images/botoes/horaContato.gif" width="15" height="16" class="geralCursoHand" onClick="showModalDialog('CsCdtbHorariocomunicHoco.do?linhaEdicao=<bean:write name="numero"/>&telefoneEnd=1',0,'help:no;scroll:no;Status:NO;dialogWidth:820px;dialogHeight:435px;dialogTop:200px;dialogLeft:150px')"--> 
            </td>
	        <td class=principalLstPar width="3%" align=center>
		  		&nbsp;<!--img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand title=Excluir Telefone onClick=RemoveTel("<bean:write name="numero"/>",true)--> 
			</td>			<td class=principalLstPar width="15%">&nbsp;<script>acronym('<bean:write name="Telefones" property="csDmtbTpComunicacaoTpcoVo.tpcoDsTpcomunicacao"/>', 3)</script></td>
			<td class=principalLstPar width="10%" align="left">&nbsp;<script>acronym('<bean:write name="Telefones" property="pcomDsDdi"/>', 3)</script><input type="hidden" name="EcDDI<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsDdi"/>"></td>
			<td class=principalLstPar width="13%" align="left">&nbsp;<script>acronym('<bean:write name="Telefones" property="pcomDsDdd"/>', 3)</script><input type="hidden" name="EcDDD<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsDdd"/>"></td>
			<td class=principalLstPar width="28%">&nbsp;<script>acronym('<bean:write name="Telefones" property="pcomDsComunicacao"/>', 9)</script><input type="hidden" name="EcNumero<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsComunicacao"/>"></td> 
			<td class=principalLstPar width="15%">&nbsp;<script>acronym('<bean:write name="Telefones" property="pcomDsComplemento"/>', 5)</script><input type="hidden" name="EcRamal<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsComplemento"/>">
			  <input type="hidden" name="EcTipo<bean:write name="numero"/>" value="<bean:write name="Telefones" property="idTpcoCdTpcomunicacao"/>">
			  <input type="hidden" name="EcPrincipal<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomInPrincipal"/>">
			  <input type="hidden" name="EcEndereco<bean:write name="numero"/>" value="<bean:write name="Telefones" property="telefoneEndereco"/>">
            </td>
			<td class=principalLstPar width="15%" align="center">&nbsp;
				<logic:equal property="pcomInPrincipal" name="Telefones" value="true">
					<!--img src=webFiles/images/icones/check.gif width=11 height=12 onclick="javascript:EditContato(<bean:write name="numero"/>)"-->
					<img src=webFiles/images/icones/check.gif width=11 height=12>
                    <script>try {window.top.superiorBarra.barraFone.fonePessoa.innerText = '<bean:write name="Telefones" property="pcomDsComunicacao" />';} catch(e) {}</script>
				</logic:equal>
				<logic:notEqual property="pcomInPrincipal" name="Telefones" value="true">
					<img src=webFiles/images/separadores/pxTranp.gif width=11 height=12>
				</logic:notEqual>
			</td>
		</tr>
	</table>
</logic:iterate>

</body>

</html:form>
</html>