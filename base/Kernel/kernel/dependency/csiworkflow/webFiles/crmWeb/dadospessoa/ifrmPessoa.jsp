<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

function SetClassFolder(pasta, estilo) {
	stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
	eval(stracao);
} 


function AtivarPasta(pasta){
	switch (pasta){
		case 'Atendimento':
			SetClassFolder('tdContatos','principalPstQuadroLinkNormalMENOR');
			SetClassFolder('tdHistorico','principalPstQuadroLinkNormalMENOR');
			SetClassFolder('tdAtendimento','principalPstQuadroLinkSelecionado');
			stracao = "document.all.complemento.src = 'ifrmAtendimentoPessoa.htm'";				
			break;
		
		case 'CONTATOS':
			SetClassFolder('tdContatos','principalPstQuadroLinkSelecionadoMENOR');
			SetClassFolder('tdHistorico','principalPstQuadroLinkNormalMENOR');
			SetClassFolder('tdAtendimento','principalPstQuadroLinkNormal');
			stracao = "document.all.complemento.src = 'ifrmContatos.htm'";
			break;
		
		case 'HISTORICO':
			SetClassFolder('tdContatos','principalPstQuadroLinkNormalMENOR');
			SetClassFolder('tdHistorico','principalPstQuadroLinkSelecionadoMENOR');
			SetClassFolder('tdAtendimento','principalPstQuadroLinkNormal');
			stracao = "document.all.complemento.src = 'ifrmHistorico.htm'";	
			break;
		
	}
	eval(stracao);
}
	
	function inicio(){
		showError("<%=request.getAttribute("msgerro")%>");
	}
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="inicio();" leftmargi=0 topmargin=0 scroll="no">

<div id="aniversario" style="position:absolute; left:300px; top:2px; width:300px; height:27; z-index:5; visibility: hidden;">
<table>
<tr align="center" valign="center">
<td><img src="webFiles/images/icones/aniversario.gif" width="24" height="26"></td>
<td><span class="principalLabelValorFixo"><bean:message key="prompt.felizAniversario"/></span></td>
</tr>
</table>

</div>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.dadospessoa" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp;</td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td valign="top" height="445">
            	<iframe name="dadosPessoa" scrolling="no" src="DadosPess.do?tela=<%= br.com.plusoft.csi.crm.helper.MCConstantes.TELA_DADOS_PESSOA %>" width="100%" marginwidth="0" frameborder="0" height="100%" marginheight="0" ></iframe>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</body>
</html>