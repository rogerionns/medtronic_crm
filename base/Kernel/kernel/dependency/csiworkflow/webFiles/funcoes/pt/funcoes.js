function showError(msgErro) {
	//showModalDialog('webFiles/erro.jsp?msgerro=' + msgErro,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:300px,dialogTop:0px,dialogLeft:200px');

	if (msgErro != 'null')
		showModalDialog('webFiles/erro.jsp?msgerro=' + msgErro,0,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:250px,dialogTop:0px,dialogLeft:200px');
}

function preencheData(campoCheck, campoTexto) {

	if (campoCheck.checked) {
		Hr = new Date(); 

		// converte retornto para string
		dd = "" + Hr.getDate(); 
		mm = "" + (Hr.getMonth() + 1); 
		aa = "" + Hr.getYear(); 

		if(aa < 1000)
			aa = Number(aa) + 1900;
		
		if (dd.length == 1) {dd = '0' + dd;}
		if (mm.length == 1) {mm = '0' + mm;}
		dAtual = dd + '/' + mm + '/'+ aa;

			campoTexto.value = dAtual;
	} else {
		campoTexto.value = "";
	}
}

function replaceAll (s, fromStr, toStr) 
{ 
   var new_s = s; 
   for (i = 0; new_s.indexOf (fromStr) != -1; i++) 
   { 
      new_s = new_s.replace (fromStr, toStr); 
   } 
   return new_s; 
}

function limpaTexto(texto) {
	if (texto.length > 0) {
		texto=replaceAll(texto.toString(),"\"","\\\"");
		document.write("<ACRONYM title=\"" + texto + "\" style=\"border: 0\">");
		document.write("</ACRONYM>");
	} else {
		document.write(texto);
	}
}

function preencheDataHora(campoCheck, campoTextoData, campoTextoHora) {
	
	if (campoCheck.checked) {
		Hr = new Date(); 
		
		// converte retornto para string
		dd = "" + Hr.getDate(); 
		mm = "" + (Hr.getMonth() + 1); 
		aa = "" + Hr.getYear(); 
		
		if(aa < 1000)
			aa = Number(aa) + 1900;
		
		hh = "" + Hr.getHours();
		mi = "" + Hr.getMinutes();
		
		if (dd.length == 1) {dd = '0' + dd;}
		if (mm.length == 1) {mm = '0' + mm;}
		dAtual = dd + '/' + mm + '/'+ aa;
		
		if (hh.length == 1) {hh = '0' + hh;}
		if (mi.length == 1) {mi = '0' + mi;}
		
		campoTextoData.value = dAtual;
		campoTextoHora.value = hh + ":" + mi;
	} else {
		campoTextoData.value = "";
		campoTextoHora.value = "";
	}
}

function preencheDataCompleta() {

	Hr = new Date(); 
	
	// converte retornto para string
	dd = "" + Hr.getDate(); 
	mm = "" + (Hr.getMonth() + 1); 
	aa = "" + Hr.getFullYear(); 
	
	hh = "" + Hr.getHours();
	mi = "" + Hr.getMinutes();
	ss = "" + Hr.getSeconds();
		
	if (dd.length == 1) dd = '0' + dd;
	if (mm.length == 1) mm = '0' + mm;

	if (hh.length == 1) hh = '0' + hh;
	if (mi.length == 1) mi = '0' + mi;
	if (ss.length == 1) ss = '0' + ss;
	
	dAtual = dd + "/" + mm + "/" + aa + " " + hh + ":" + mi + ":" + ss;
	
	return dAtual;
}

function textCounter(field, maxlimit) {
	if (field.value.length > maxlimit) 
		field.value = field.value.substring(0, maxlimit);
}

function acronym(texto, nr) {
	if (texto.length > nr) {
		document.write("<ACRONYM title=\"" + texto + "\" style=\"border: 0\">");
		document.write(texto.substring(0, nr) + "...");
		document.write("</ACRONYM>");
	} else {
		document.write(texto);
	}
}

function acronymLst(texto, nr) {
	acro = "";
	if (texto.length > nr) {
		acro += "<ACRONYM title=\"" + texto + "\" style=\"border: 0\">";
		acro += texto.substring(0, nr) + "...";
		acro += "</ACRONYM>";
	} else {
		acro = texto;
	}
	return acro;
}

function acronymLeftLst(texto, nr) {
	acro = "";
	if (texto.length > nr) {
		acro += "<ACRONYM title=\"" + texto + "\" style=\"border: 0\">";
		acro += "..."+ texto.substring(texto.length - nr, texto.length);
		acro += "</ACRONYM>";
	} else {
		acro = texto;
	}
	return acro;
}

function isDigito(evnt) {
	var tk;
	// Recebe a tela pressionada
	tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : evnt.which;
		
	if(tk == 8 || tk == 0){
		return true;
	}

	if((!((tk >= 96 && tk <= 105) || (tk >= 48 && tk <= 57))) || tk == 13){
		(navigator.appName == "Microsoft Internet Explorer") ? event.returnValue = null : evnt.returnValue = null;
		return false;
	}
}

function isDigitoPonto(obj) {
    if (((event.keyCode < 48 && event.keyCode != 46) ||(event.keyCode > 57)) && event.keyCode != 8)
        event.returnValue = false;
}

function trataQuebraLinha(valor) {
	var val = valor;
	for (var i = 0; i < valor.length; i++)
		val = val.replace('\r\n', 'QBRLNH');
	return val;
}

function trataQuebraLinha2(valor) {
	var val = valor;
	for (var i = 0; i < valor.length; i++)
  	val = val.replace('QBRLNH', '\n');
	return val;
}

function trataQuebraLinha3(valor) {
	var val = valor;
	for (var i = 0; i < valor.length; i++)
  	val = val.replace('QBRLNH', '<BR>');
	return val;
}

function getData() {
		Hr = new Date(); 
		
		// converte retornto para string
		dd = "" + Hr.getDate(); 
		mm = "" + (Hr.getMonth() + 1); 
		aa = "" + Hr.getYear(); 
		
		if (dd.length == 1) {dd = '0' + dd;}
		if (mm.length == 1) {mm = '0' + mm;}
		dAtual = dd + '/' + mm + '/'+ aa;
		
		return dAtual;
}

function getHora() {
		Hr = new Date(); 
		
		hh = "" + Hr.getHours();
		mi = "" + Hr.getMinutes();
		
		if (hh.length == 1) {hh = '0' + hh;}
		if (mi.length == 1) {mi = '0' + mi;}
		
		return (hh + ":" + mi);
}

function trim(cStr){
		if (typeof(cStr) != "undefined"){
			var re = /^\s+/
			cStr = cStr.replace (re, "")
			re = /\s+$/
			cStr = cStr.replace (re, "")
			return cStr
		}
		else
			return ""
}

//Substitui os caracteres especias de strings.
function codificaStringHtml(objetoStr){
	
	//Chamado: 100198 KERNEL-1084 - 13/04/2015 - Marcos Donato //
	var retorno = objetoStr.value
					.replace(/\r\n/gi,'QBRLNH')
					.replace(/\n/gi,'QBRLNH')
					.replace(/\"/gi,'&quot;')
					.replace(/\'/gi,'ASPASIMPLES')
					.replace(/\\/gi,'\\\\');

	/*
	for (var i = 0; i < objetoStr.value.length; i++){
	
		var val1 = objetoStr.value.substr(i,1);
		
		if(val1.indexOf("\n")>-1){
			retorno += val1.replace('\n', 'QBRLNH');
			
		}else if(val1.indexOf("\r")>-1){
			//N?o atribui esse caracter.
			
		}else if(val1.indexOf('"')>-1){
			retorno += val1.replace('"', '&quot;');
			
		}else if(val1.indexOf("'")>-1){
			retorno += val1.replace("'", "ASPASIMPLES");
			
		}else if(val1.indexOf("\\")>-1){
			retorno += val1.replace("\\", "\\\\");
			
		}else{
			retorno=retorno+val1;
		}
		
	}
	*/
	
	return retorno;
	
}


//Substitui os caracteres especias de strings.
function descodificaStringHtml(objetoStr){
	
	var retorno = objetoStr;
	
	while(retorno.indexOf("QBRLNH")>-1){
		retorno = retorno.replace('QBRLNH', '\r\n');
	}

	while(retorno.indexOf('&quot;')>-1){
		retorno = retorno.replace('&quot;', '"');
	}
	
	while(retorno.indexOf('&amp;quot;')>-1){
		retorno = retorno.replace('&amp;quot;', '"');
	}
	
	while(retorno.indexOf("ASPASIMPLES")>-1){
		retorno = retorno.replace('ASPASIMPLES', "'");
	}			
	
	while(retorno.indexOf("\\\\")>-1){
		retorno = retorno.replace('\\\\', "\\");
	}	
	
	return retorno;
	
}	