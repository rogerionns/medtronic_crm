<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes,
                                 br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<script language="JavaScript">

function buscaManifestacao(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, foupNrSequencia, matpDsManifTipo) {
	parent.document.all('Layer1').style.visibility='';
	window.parent.manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	window.parent.manifestacaoForm.tela.value = '<%=MCConstantes.TELA_MANIFESTACAO%>';
	window.parent.manifestacaoForm.target = window.parent.name = "pendentes";
	
	window.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value = idChamCdChamado;
	window.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value = maniNrSequencia;
	window.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = idTpmaCdTpManifestacao;
	window.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = idAsn1CdAssuntoNivel1;
	window.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = idAsn2CdAssuntoNivel2;
	if (matpDsManifTipo == "FOLLOW-UP")
		window.parent.manifestacaoForm.foupNrSequencia.value = foupNrSequencia;
	else
		window.parent.manifestacaoForm.foupNrSequencia.value = "0";
	window.parent.manifestacaoForm.submit();
}

function consultaManifestacao(chamado, manifestacao, tpManifestacao, assuntoNivel) {
	showModalDialog('<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&workflow=followup&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + tpManifestacao + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=' + assuntoNivel,0,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	//window.open('Historico.do?acao=consultar&tela=manifestacaoConsulta&workflow=followup&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + tpManifestacao + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=' + assuntoNivel);
}


function inicio(){
	showError("<%=request.getAttribute("msgerro")%>");
	parent.document.all("Layer1").style.visibility = "hidden";
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="inicio();">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <logic:present name="pendenciasVector">
 <logic:iterate name="pendenciasVector" id="pendenciasVector">
  <script>parent.manifestacaoForm.existeRegistro.value = "true";</script>
  <tr class="geralCursoHand"> 
	<logic:notEqual name="pendenciasVector" property="emAtraso" value="true">
    <td width="6%" class="principalLstPar" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')">
      &nbsp;<bean:write name="pendenciasVector" property="idChamCdChamado" />
    </td>
    <td width="5%" class="principalLstPar" align="center" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')">
    	&nbsp;
		<logic:equal name="pendenciasVector" property="maniInGrave" value="true">
			<img src="webFiles/images/icones/exclamacao.gif" width="10" height="14" title="<bean:message key="prompt.manifestaçãoUrgente"/>">
		</logic:equal>
    </td>
    <td width="15%" class="principalLstPar" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')">
      &nbsp;<bean:write name="pendenciasVector" property="chamDhInicial" />
    </td>
    <td width="15%" class="principalLstPar" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')"> 
      &nbsp;<bean:write name="pendenciasVector" property="maniDhPrevisao" />
    </td>
    <td width="19%" class="principalLstPar" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')"> 
      &nbsp;<script>acronym('<bean:write name="pendenciasVector" property="pessNmPessoa" />', 25);</script>
    </td>
    <td width="19%" class="principalLstPar" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')"> 
      &nbsp;<script>acronym('<bean:write name="pendenciasVector" property="matpDsManifTipo" />', 20);</script>
    </td>
    <td width="19%" class="principalLstPar" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')">
      &nbsp;<script>acronym('<bean:write name="pendenciasVector" property="tpmaDsTpManifestacao" /> ', 25);</script>
    </td>
    <td width="2%" class="principalLstPar">
      <img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" onClick="consultaManifestacao('<bean:write name='pendenciasVector' property='idChamCdChamado' />', '<bean:write name='pendenciasVector' property='maniNrSequencia' />', '<bean:write name='pendenciasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='pendenciasVector' property='idAsn1CdAssuntoNivel1' />@<bean:write name='pendenciasVector' property='idAsn2CdAssuntoNivel2' />')">
    </td>
    </logic:notEqual>
	<logic:equal name="pendenciasVector" property="emAtraso" value="true">
    <td width="6%" class="principalLstVermelho" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')">
      &nbsp;<bean:write name="pendenciasVector" property="idChamCdChamado" />
    </td>
    <td width="5%" class="principalLstVermelho" align="center" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')">
    	&nbsp;
		<logic:equal name="pendenciasVector" property="maniInGrave" value="true">
			<img src="webFiles/images/icones/exclamacao.gif" width="10" height="14" title="<bean:message key='prompt.manifestacaoGrave'/>">
		</logic:equal>
    </td>
    <td width="15%" class="principalLstVermelho" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')">
      &nbsp;<bean:write name="pendenciasVector" property="chamDhInicial" />
    </td>
    <td width="15%" class="principalLstVermelho" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')"> 
      &nbsp;<bean:write name="pendenciasVector" property="maniDhPrevisao" />
    </td>
    <td width="19%" class="principalLstVermelho" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')"> 
      &nbsp;<script>acronym('<bean:write name="pendenciasVector" property="pessNmPessoa" />', 25);</script>
    </td>
    <td width="19%" class="principalLstVermelho" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')"> 
      &nbsp;<script>acronym('<bean:write name="pendenciasVector" property="matpDsManifTipo" />', 20);</script>
    </td>
    <td width="19%" class="principalLstVermelho" onclick="buscaManifestacao('<bean:write name="pendenciasVector" property="idChamCdChamado" />', '<bean:write name="pendenciasVector" property="maniNrSequencia" />', '<bean:write name="pendenciasVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="pendenciasVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="pendenciasVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="pendenciasVector" property="idMadsNrSequencial" />', '<bean:write name="pendenciasVector" property="matpDsManifTipo" />')">
      &nbsp;<script>acronym('<bean:write name="pendenciasVector" property="tpmaDsTpManifestacao" /> ', 25);</script>
    </td>
    <td width="2%" class="principalLstVermelho">
      <img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" onClick="consultaManifestacao('<bean:write name='pendenciasVector' property='idChamCdChamado' />', '<bean:write name='pendenciasVector' property='maniNrSequencia' />', '<bean:write name='pendenciasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='pendenciasVector' property='idAsn1CdAssuntoNivel1' />@<bean:write name='pendenciasVector' property='idAsn2CdAssuntoNivel2' />')">
    </td>
    </logic:equal>
  </tr>
 </logic:iterate>
 </logic:present>
 <script>
   if (parent.manifestacaoForm.existeRegistro.value == "false")
     document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="200" ><b>Nenhum registro encontrado.</b></td></tr>');
 </script>
</table>
</body>
</html>