<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html:html>
<head>
	<title><bean:message key="prompt.title.login"/></title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<link rel="shortcut icon" href="/plusoft-resources/images/favicon.ico" />
	
	<meta name="application-name" content="Plusoft CRM"/>
	<meta name="msapplication-tooltip" content="Plusoft CRM"/>
	<meta name="msapplication-starturl" content="http://localhost:8080/csicrm/Login.do?tela=login&loginIni=true"/>
	<meta name="msapplication-window" content="width=800;height=600"/>
	<meta name="msapplication-task" content="name=Chamado;action-uri=http://localhost:8080/csicrm/Login.do?tela=login&loginIni=true;icon-uri=http://localhost:8080/plusoft-resources/images/favicon.ico" />
	<meta name="msapplication-task" content="name=Cadastro;action-uri=http://localhost:8080/csiadm/Login.do?tela=login&loginIni=true;icon-uri=http://localhost:8080/plusoft-resources/images/favicon.ico" />
	<meta name="msapplication-task" content="name=Workflow;action-uri=http://localhost:8080/csiworkflow/Login.do?tela=login&loginIni=true;icon-uri=http://localhost:8080/plusoft-resources/images/favicon.ico" />
	<meta name="msapplication-task" content="name=Gerente;action-uri=http://localhost:8080/csigerente/Login.do?tela=login&loginIni=true;icon-uri=http://localhost:8080/plusoft-resources/images/favicon.ico" />
	<meta name="msapplication-task" content="name=SFA;action-uri=http://localhost:8080/csisfa/Login.do?tela=login&loginIni=true;icon-uri=http://localhost:8080/plusoft-resources/images/favicon.ico" />
	
	<link rel="stylesheet" href="/plusoft-resources/css/login.css" type="text/css">
	<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
	<% } %>
</head>

<body class="nomargin">
<html:form action="/Login.do">
	<input type="hidden" name="tela" value="modulo" />
	<input type="hidden" name="modulo" value="<bean:message key="aplicacao.modulo" />" />
	<input type="hidden" name="acao" value="" />
	
	<html:hidden property="erro" />
	<html:hidden property="ssoUser" />
	<html:hidden property="sobrescreverLogin" value="" />

	<div id="aguarde" style="display: none;">
		<div class="block"></div>
		<div class="aguarde"></div>
	</div>

	<div class="modulologin">
		<bean:message key="prompt.workflow" />
	</div>
		
	<div class="telalogin">
		<div>
			<table>
				<tr>
					<td width="85px" align="right"><bean:message key="prompt.login.login" /></td>
					<td width="10px"><span class="setaazul"></span></td>
					<td width="240px">
						<html:text property="funcDsLoginname" styleId="funcDsLoginname" styleClass="text"  maxlength="100" />
					</td>
				</tr>
				<tr>
					<td align="right"><bean:message key="prompt.login.senha" /></td>
					<td><span class="setaazul"></span></td>
					<td>
						<html:password property="funcDsPassword" styleClass="text" maxlength="20" />
					</td>
				</tr>
				<tr>
					<td align="right"><bean:message key="prompt.login.informacoes" /></td>
					<td><span class="setaazul"></span></td>
					<td>
						<html:textarea property="xmlInformacao" styleClass="text" readonly="true" rows="2" />
					</td>
				</tr>
				<!-- Chamado: 82685 -->
		        <logic:present name="diasParaExpirar">
		        	<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td width="240px" class="errors"><bean:write name="diasParaExpirar"/></td>
					</tr>
			    </logic:present>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td width="240px" class="errors" id="errors"><html:errors header="erro.login.header" footer="erro.login.footer" /></td> <!-- Chamado: 88445 - 17/05/2013 - Marco Costa -->
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="center">
					   <table>
					      <tr>
					        <td id="btOk" width="80px">
								<input type="submit" onclick="return login.validaCampos();" value="<bean:message key="prompt.login.Ok" />" class="button login" />
							</td>
							<td width="80px">
								<input type="button" onclick="window.close();" value="<bean:message key="prompt.login.Sair" />" class="button login" />
							</td>
							<td width="80px" id="btSenha">
								<input type="submit" onclick="return login.trocaSenha();" value="<bean:message key="prompt.login.TrocaSenha" />" class="button login-intermediario" />
							</td>
						   </tr>
						   <tr>
						     <td id="btSSO" colspan="3" align="center">
								<logic:equal name="loginForm" property="usaLoginIntegrado" value="true">
									<a href="/sso/<bean:message key="aplicacao.context" />" onclick="login.aguarde();" class="button login-extended"><bean:message key="prompt.login.LoginIntegrado" /></a>
								</logic:equal>
							  </td>
							</tr>
							<tr>
								<td align="center" colspan="3"><br><span class="link" onclick="login.esqueciMinhaSenha();"><bean:message key="prompt.login.esqueci.senha" /></span></td>
							</tr>
					   </table>
					</td>
				</tr>
			</table>
		
		</div>
	</div>
</html:form>

<!-- Chamado: 82565 - 26/07/2012 - Carlos Nunes -->

<script type="text/javascript" src="/plusoft-resources/javascripts/plusoft-login.js"></script>

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>

<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">

<div id="dialog-message" title="Aviso" style="overflow: hidden;">
	<p>
		<div id="messagemSessaoDuplicada" style="width: 250px; height: 100px;overflow: hidden;"></div>
	</p>
</div>

<script type="text/javascript">
	/** mensagens multi-idioma **/
	login.Por_favor_digite_seu_login="<bean:message key="prompt.Por_favor_digite_seu_login" />";
	login.Por_favor_digite_sua_senha="<bean:message key="prompt.Por_favor_digite_sua_senha"/>";
	login.Voce_realmente_deseja_resetar_sua_senha="<bean:message key="prompt.Voce_realmente_deseja_resetar_sua_senha"/>";
	
	<logic:present name="abrirCadastroLicenca">
		var wndlicenca = window.open("/csiadm/CadastroLicenca.do", "licenca", "width=820px,height=570px,top=100px,left=100px,status=no,location=no,menubar=no,resizable=no,scrollbars=no,toolbar=no,channelmode=no,directories=no");
	</logic:present>
	
	<logic:notPresent name="abrirCadastroLicenca">
	    //Chamado: 82565 - 26/07/2012 - Carlos Nunes
		setTimeout("try{document.forms[0].funcDsLoginname.focus();}catch(e){}", 100);		
	</logic:notPresent>

	<logic:present name="usuarioJaLogado">
	if(confirm('<bean:message key="prompt.loginUtilizadoJaEstaEmUso"/>')){
		document.loginForm.sobrescreverLogin.value = true;
		document.loginForm.submit();
	}
	</logic:present>

	<logic:present name="msgerro">
	showModalDialog('/plusoft-resources/erro.jsp?msgerro=<bean:write name="msgerro" />', window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:250px,dialogTop:0px,dialogLeft:200px');
	</logic:present>

	 <logic:present name="esqueciSenha">
	 	document.getElementById("errors").innerHTML = "<bean:message key="prompt.senha_enviada_para_seu_email" />";
	 </logic:present>
	 
	<logic:notPresent name="abrirCadastroLicenca">
		<logic:notPresent name="sessaoexpirada">
		    //Chamado: 82565 - 26/07/2012 - Carlos Nunes
			$(document).ready(function(){
		
				   $.post('/csicrm/ValidarDuplicidadeDeSessaoParaOFuncionarioCorrente.do', function(ret){
					  
					   document.getElementById("btOk").style.display = "none";
					   document.getElementById("btSenha").style.display = "none";
					   document.getElementById("btSSO").style.display = "none";
					   
					   if(ret.retorno == "S") {
						   
						    document.forms[0].funcDsLoginname.disabled = true;
						    document.forms[0].funcDsPassword.disabled = true;
						   
						    $( "#dialog:ui-dialog" ).dialog( "destroy" );
						  
                          document.getElementById("messagemSessaoDuplicada").innerText = ret.mensagem;

						  //Chamado: 93507 - 04/03/2014 - Carlos Nunes
						  if( navigator.userAgent.toLowerCase().indexOf("firefox") > -1 )
						  {
						    document.getElementById("messagemSessaoDuplicada").textContent = ret.mensagem;
						  }
						     
						    $( "#dialog-message" ).dialog({
								modal: true,
								buttons: {
									Sair: function() {
										$( this ).dialog( "close" );
									}
								}
							});
						    
							return;
					   }
					   else
					   {
						   document.getElementById("btOk").style.display = "block";
						   document.getElementById("btSenha").style.display = "block";
						   document.getElementById("btSSO").style.display = "block";
					   }
				   });
				
		 	});
		</logic:notPresent>
	</logic:notPresent>
</script>

</body>
</html:html>
