<%@page import="br.com.plusoft.saas.SaasHelper"%>
<%@page import="br.com.plusoft.licenca.helper.ModuloHelper"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,
							br.com.plusoft.fw.app.Application,
							br.com.plusoft.csi.adm.helper.*,
							com.iberia.helper.Constantes,
							br.com.plusoft.csi.adm.util.Geral " %>
							
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

	long idFuncCdFuncionario = 0;
	CsCdtbFuncionarioFuncVo funcionarioVo = null;

	String caminhoLogoTipo = "";
	long idEmprCdEmpresa = 0;
	String funcDsLoginname = "";
	String idPsf2CdPlusoft3 = "";
	
	try{
		idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
		caminhoLogoTipo = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CAMINHO_LOGOTIPO_CLIENTE,idEmprCdEmpresa);
	}catch(Exception e){}
		
	if(caminhoLogoTipo == null || caminhoLogoTipo.equals("")){
		caminhoLogoTipo = "";
	}
	
	if(request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) != null) {
		empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	
		idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
	}
	
	if(request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null) {
		funcionarioVo = (CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
		idFuncCdFuncionario = funcionarioVo.getIdFuncCdFuncionario();
		funcDsLoginname = funcionarioVo.getFuncDsLoginname();
		if(funcionarioVo.getIdPsf2CdPlusoft3()!=null) idPsf2CdPlusoft3 = funcionarioVo.getIdPsf2CdPlusoft3();
	}
	
	request.getSession(true).setAttribute("idModuCdModulo", String.valueOf(ModuloHelper.CODIGO_MODULO_WORKFLOW));
	//Chamado: 92729 - 10/01/2014 - Carlos Nunes
	long idFuncCdFuncionarioSaas = 0;
	
	if(SaasHelper.aplicacaoSaas) {
		idFuncCdFuncionarioSaas = new SaasHelper().getSessionData().getIdFuncCdFuncionarioSaas();
	}
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<html>
	<head>
		<title><bean:message key="prompt.csiworkflow" /></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
	<script type="text/javascript" src="webFiles/funcoes/variaveis.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
	<script type="text/javascript">
        var empresaDoChamado = 0; //Chamado: 94870 - 03/06/2014 - Carlos Nunes
		var modulo = "workflow";
		var idModuCdModulo = '<%=ModuloHelper.CODIGO_MODULO_WORKFLOW %>';
		
		function carregarWkf(idPessCdPessoa, idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, foupNrSequencia, idUtilCdFuncionario, idTppuCdTipopublico, idMatpCdManifTipo, idLinhCdLinha, idGrmaCdGrupoManifestacao){
		//	aguarde.style.visibility = "visible";
			telaFiltro.style.display = "none";
			telaWorkflow.style.display = "block";
			
			//Capturando os ids de assuntonivel1 e 2
			var idAsn1 = idAsnCdAssuntoNivel.substring(0, idAsnCdAssuntoNivel.indexOf("@"));
			var idAsn2 = idAsnCdAssuntoNivel.substring(idAsnCdAssuntoNivel.indexOf("@") +1);

/*			if (matpDsManifTipo != "FOLLOW-UP")
				foupNrSequencia.value = 0; */
			
			ifrmTelaWorkflow.abrirItem(idChamCdChamado, maniNrSequencia, idAsn1, idAsn2, idPessCdPessoa, foupNrSequencia, idTpmaCdTpManifestacao, idTppuCdTipopublico, <%=idEmprCdEmpresa%>, idMatpCdManifTipo, idLinhCdLinha, idGrmaCdGrupoManifestacao);
		}

		//Chama um action que executa rotinas para fechar a manifesta��o
		function voltar(){
			ifrmTelaWorkflow.ifrmMenu.location = "WorkFlow.do?tela=<%=MCConstantes.TELA_FECHAR_MANIFESTACAO%>"+
				"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ ifrmTelaWorkflow.idChamCdChamado;
		}
		

		function fechaSistema() {
			if (confirm("<bean:message key='prompt.confirm.Tem_Certeza_que_deseja_sair_do_sistema'/>")) {
				podeFecharSistema = true;
				window.top.close();
			}
		}			


		var podeFecharSistema = false;
		
		function encerrarSistema(){
			if(!podeFecharSistema && '<%=funcDsLoginname%>'!='') {
				var dt = new Date();
				var dd = ("0"+dt.getDate());
				if(dd.length>2) 
					dd=dd.substring(1, 3);
				
				var mm = ("0"+(new Number(dt.getMonth())+1));
				if(mm.length>2) 
					mm=mm.substring(1, 3);
				
				var yy = dt.getFullYear();
				var hh = ("0"+dt.getHours());
				if(hh.length>2) 
					hh=hh.substring(1, 3);
				var mi = ("0"+dt.getMinutes());
				if(mi.length>2) 
					mi=mi.substring(1, 3);
				var ss = ("0"+dt.getSeconds());
				if(ss.length>2) 
					ss=ss.substring(1, 3);
				
				//return false;
				var avs = "<plusoft:message key="prompt.confirm.encerraSistema" />";
					avs+= "[<%=funcDsLoginname%>] - ";
					avs+= dd +"/"+ mm +"/"+ yy +" "+ hh +":"+ mi +":"+ ss;
				return avs;
			}
		}
		
		//Chamado: 79676 - Carlos Nunes - 20/03/2012
		/* window.onbeforeunload = function (e) {
			  e = e || window.event;

			 // For IE and Firefox prior to version 4
			  if (e) {
				 e.returnValue = encerrarSistema();
			  }

			  unloadSistema();
			  
			  // For Safari
			  return  encerrarSistema();
			}; */
			
		var unloading = false;
		window.onbeforeunload = function (evt) {
			unloading = true;
			if (typeof evt == 'undefined') {
				evt = window.event;
			}

			if (evt) {
				evt.returnValue = encerrarSistema();
			}

			return encerrarSistema();
		};
		
		setInterval(function(){
		    if(unloading){
		        unloading = false;
		        setTimeout(function(){}, 1000);
		    }
		}, 400);
		
		window.onunload = function(){
			unloadSistema();
		};
			
		function unloadSistema(){
			var wnd = window.open("", "logoutwkf", "top=190,left=250,status=no,width=300,height=50,center=yes");

			wnd.document.open();
			wnd.document.write("<html><head><title><plusoft:message key="prompt.csiworkflow"/></title></head><body>");
			wnd.document.write("<table width=\"100%\"><tr><td width=\"10%\"><img src=\"/plusoft-resources/images/plusoft-logo-128.png\" style=\"width: 40px; \" /></td><td style=\"font-family: Arial,sans-serif; font-size: 11px;\"><plusoft:message key="prompt.aguarde.finaliza" /></td></tr></table>");
			wnd.document.write("<form action=\"/csiworkflow/Logout.do\" name=\"logoutForm\" method=\"POST\">");
			wnd.document.write("<input type=\"hidden\" name=\"l\" value=\"<%=idPsf2CdPlusoft3 %>\" />");
			wnd.document.write("</form>");
			wnd.document.write("</body></html>");
			wnd.document.close();

			wnd.document.forms[0].submit();
			wnd.setTimeout("window.close()", 3000);

			podeFecharSistema = true;
		}
		
		var nCountInicio = 0;
		
		function inicio(){
			try{
				<%  //Chamado: 92729 - 10/01/2014 - Carlos Nunes
					String urlCrm =  "/csicrm/inicializarSessao.do?idFuncCdFuncionario=" + idFuncCdFuncionario + "&idEmpresa=" + idEmprCdEmpresa;
						
					if(idFuncCdFuncionarioSaas > 0) {
						if(urlCrm != null && !urlCrm.equals("") && funcionarioVo != null)
							urlCrm+= "&idFuncCdFuncionarioSaas="+idFuncCdFuncionarioSaas;
					}
				%>
				
				chamarURL("", "GET", "<%=urlCrm%>", "");
				
				if("<%=caminhoLogoTipo%>" != ""){
					document.getElementById("divLogo").innerHTML = "<img id='imgLogo' src='" + "<%=caminhoLogoTipo%>" +  "'></img>";
				}
			}catch(e){
				if(nCountInicio <5){
					setTimeout('inicio()',300);
					nCountInicio++;
				}
			}
		}
				
		function carregarLogo(caminho){
			if(caminho != ""){
				document.getElementById("divLogo").innerHTML = "<img id='imgLogo' src='" + caminho +  "'></img>";
			}
		}

		function recarregarLogoTipo(){
			ifrmRecarregaLogotipo.location = "webFiles/ifrmRecarregarLogotipo.jsp"
		}
		//Chamado: 90471 - 16/09/2013 - Carlos Nunes
		function getIdFuncLogado(){
			return '<%=idFuncCdFuncionario%>';
		}

		function carregaWorkflow() {
			ifrmTelaFiltro.location = "LocalizadorAtendimento.do?tela=localizadorAtendimento";
			ifrmTelaWorkflow.location = "WorkFlow.do?tela=workflow";
		}
		
	</script>
	
	<%//Chamado: 101201 KERNEL-1216 - 18/05/2015 - Marcos Donato // distancia das margens colocadas no style devido a incompatibilidade entre os IEs%>
	<body class="esquerdoBgrPageIFRM" text="#000000" onload="inicio();" style="overflow: hidden; margin: 5px 5px 5px 5px;">
		<div id="aguarde" style="position:absolute; left:380px; top:230px; width:199px; height:148px; z-index:1; visibility: hidden"><!--Jonathan | Adequa��o para o IE 10-->
			<div align="center"><iframe src="webFiles/workflow/aguarde.jsp" width="100%" height="148px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
		</div>

	  	<div id="divSpin" name="divSpin" style="position:absolute; visibility:hidden">
	  		<img src="../../webFiles/images/botoes/spinner.gif" width="20" height="20">
	  	</div>
		
		<%//Chamado: 101201 KERNEL-1216 - 18/05/2015 - Marcos Donato // modificado o TOP de 20 para 10px%>
		<div id="divLogo" style="text-align:right; position:absolute; left:815px; top:10px; width:105px; height:30px; z-index:1; visibility:visible"></div>
		<!--T�tulo CSI-->
		<table width="101%" cellpadding=0 cellspacing=0 border=0>
			<tr>
				<td class="PrincipalQuadroPstVazia">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td class="PrincipalBgrQuadro" width="20">
								<img src="webFiles/images/logo/banner_new_work.gif">
							</td>
							<td background="webFiles/images/logo/bgr_superior.jpg" align="right">
								<img src="/plusoft-resources/images/plusoft-logo-128.png" style="width: 40px; ">&nbsp;&nbsp;
							</td>
						</tr>
					</table>
				</td>
				<td><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>
	
			<div id="telaFiltro" style="display: block;">
				<!--Tela com filtro-->
				<table width="100%" height="70%" border="0" cellspacing="0" cellpadding="0">
					<tr height="10">
						<td colspan=2>
		   					<table width="100%" border="0" cellspacing="0" cellpadding="0">
			   					<tr> 
									<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.workflow" /></td>
									<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
									<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			   					</tr>
		   					</table>
						</td>
					</tr>
					<tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
						<td height="570px" class="principalBgrQuadro" valign="top" align="center"><!--Jonathan | Adequa��o para o IE 10-->
							<iframe name="ifrmTelaFiltro" id="ifrmTelaFiltro" src="" width="99%" height="570px" frameborder=0 scrolling="no"></iframe>
						</td>
						<td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
					</tr>
					<tr><!-- Chamado: 90471 - 16/09/2013 - Carlos Nunes -->
						<!-- ---------------------- -->
						<!-- Gargamel 28/03         -->
						<!-- Alterada a estrutura   -->
						<!-- para incluir td espec  -->
						<!-- ---------------------- -->
						<td class="principalBgrQuadro">
							<table width="100%" cellpadding=0 cellspacing=0 border=0>
								<tr>
									<td >&nbsp;&nbsp;
										<font id="lblTotal" class=""></font>&nbsp;&nbsp;&nbsp;&nbsp;
										<font id="lblManif" class=""></font>&nbsp;&nbsp;&nbsp;
										<font id="lblFollowup" class=""></font>&nbsp;&nbsp;&nbsp;
										<font id="lblEmAtraso" class=""></font>
									</td>
									<td width="60%" rowspan="2" id="tdResumo" name="tdResumo" >&nbsp;&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;&nbsp;
										<font id="lblTotalConcluido" class=""></font>&nbsp;&nbsp;&nbsp;&nbsp;
										<font id="lblManifConcluido" class=""></font>&nbsp;&nbsp;&nbsp;
										<font id="lblFollowupConcluido" class=""></font>&nbsp;&nbsp;&nbsp;
										<font id="lblEmAtrasoConcluido" class=""></font>
									</td>
								</tr>
		   					</table>
						</td>
						<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
					</tr>
					<tr> 
						<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
						<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
					</tr>
				</table>
			</div>
			<div id="telaWorkflow" style="display: none;">
				<!--Tela do workflow, pendencia selecionada-->
				<iframe name="ifrmTelaWorkflow" id="ifrmTelaWorkflow" src="" width="100%" height="100%" frameborder=0></iframe>
			</div>
		
			<!--Permissao -->
			<div id="permissaoDiv" style="position:absolute; width:0px; z-index:3; height: 0px; visibility: hidden">
				<iframe id="ifrmPermissao" 
						name="ifrmPermissao" 
						src="AdministracaoPermissionamento.do?tela=<%= Geral.getActionProperty("permissaoFrame",empresaVo.getIdEmprCdEmpresa())%>&acao=<%= Constantes.ACAO_CONSULTAR%>&idModuCdModulo=<%= PermissaoConst.MODULO_CODIGO_WORKFLOW%>" 
						width="10%" 
						height="0" 
						scrolling="no" 
						marginwidth="0" 
						marginheight="0" 
						frameborder="0">
				</iframe>
			</div>

			<!--Permissao -->
			
			<!-- IFRM responsavel pelo reload de verifica��o de licen�as. -->
			<!--  iframe name="ifrmLicenca" id="ifrmLicenca" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" -->
			<!-- -->
			
			<iframe id="ifrmRecarregaLogotipo" name="ifrmRecarregaLogotipo" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	</body>
	
	<%
      String url = "../csicrm/inicializarSessao.do";      
		try{ //Chamado: 92729 - 10/01/2014 - Carlos Nunes			
			CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			if(url != null && !url.equals("") && funcVo != null){
				url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();	
				
				if(idFuncCdFuncionarioSaas > 0) {
					if(url != null && !url.equals("") && funcionarioVo != null)
						url+= "&idFuncCdFuncionarioSaas="+idFuncCdFuncionarioSaas;
				}
			}			
		}catch(Exception e){}
	  %>
      <iframe src="<%=url %>" id="ifrmSessaoCrm" name="ifrmSessaoCrm"  style="display:none"></iframe>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>