<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title>..: <bean:message key="prompt.detalhes" /> :.. </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
</head>

<script>
    //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
    var wi = (window.dialogArguments)?window.dialogArguments:window.opener;

	function inicio() {
		document.getElementsByName("txtDescricao")[0].value = wi.objBinoculo.value;	
		document.getElementsByName("txtDescricao")[0].readOnly = (wi.objBinoculo.readOnly || wi.objBinoculo.disabled);
	}

	function carregaTexto(){		
		if(document.getElementsByName("txtDescricao")[0].readOnly == false){
			wi.objBinoculo.value = document.getElementsByName("txtDescricao")[0].value;			
		}
		window.close();
	}

	<%
	/**
	 * Chamado 68525 - Vinicius - Assinatura quando complementar a resposta da conclus�o
	 */
	%>
	var assinatura = wi.assinatura;
	var tamTextoAntigo = 0;

	function alteracao(sair){

		<%if("ifrmManifestacaoDetalhe".equals(request.getParameter("tela"))){%>
			if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_DESCRICAOMANIFESTACAO_LIVREALTERACAO%>')){
				return false;
			}
		<%}else if("ifrmManifestacaoConclusao".equals(request.getParameter("tela"))){%>
			if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_TEXTOCONCLUSAO_LIVREALTERACAO%>')){
				return false;
			}
		<%}%>

		if(txtDescricaoAntiga.value == ''){
			return false;
		}
		
		if(!document.all["txtDescricao"].readOnly){
			if(!(event.keyCode >= 37 && event.keyCode <= 40)){
				var conteudoAntigo = txtDescricaoAntiga.value;
				var conteudoNovo = txtDescricao.value;	
						
				if(conteudoAntigo.substring(tamTextoAntigo).indexOf(assinatura) < 0 && !sair){
					txtDescricaoAntiga.value += "\n"+ assinatura +"\n";
				}
				
				conteudoNovo = conteudoAntigo + conteudoNovo.substring(conteudoAntigo.length);
		
				if(txtDescricao.value != conteudoNovo)
					txtDescricao.value = conteudoNovo;
			}
				manif.value = txtDescricao.value;
			}
	}	
</script>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="inicio()" >
<input type="hidden" name="txtDescricaoAntiga" value=""/>
<input type="hidden" name="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao" id="manif" value=""/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <textarea name="txtDescricao" styleClass="principalObjForm3D" onfocus="alteracao(false)" rows="25" cols="100" readonly="readonly"></textarea>
    </td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> <img src="../images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:carregaTexto()" class="geralCursoHand"></td>          
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>