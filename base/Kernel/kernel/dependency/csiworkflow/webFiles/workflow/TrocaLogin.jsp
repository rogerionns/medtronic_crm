<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*,br.com.plusoft.fw.app.Application"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");

	int nSenha = 6;
	try{
		nSenha = Integer.parseInt( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CADASTRO_FUNCIONARIO_SENHA, request));
	}catch(Exception e){ }

%>
<html:html>
<head>
<title><bean:message key="prompt.title.login"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script type="text/javascript">

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function ValidaCampos(){
	
	if(document.loginForm.funcDsPassword.value == ""){
		alert("<bean:message key="prompt.Por_favor_digite_sua_senha"/>");
		document.loginForm.funcDsPassword.focus();
		return false;
	}
	if(document.loginForm.funcDsNovoPassword.value == ""){
		alert("<bean:message key="prompt.Por_favor_digite_sua_nova_senha"/>");
		document.loginForm.funcDsNovoPassword.focus();
		return false;
	}
	if(document.loginForm.funcDsConfirmaNovoPassword.value == ""){
		alert("<bean:message key="prompt.Por_favor_digite_sua_confirma_nova_senha"/>");
		document.loginForm.funcDsConfirmaNovoPassword.focus();
		return false;
	}
	if(document.loginForm.funcDsNovoPassword.value != document.loginForm.funcDsConfirmaNovoPassword.value) {
		alert("<bean:message key="prompt.senhas_nao_conferem"/>");
		document.loginForm.funcDsNovoPassword.focus();
		return false;
	}
	if(document.loginForm.funcDsNovoPassword.value.length < new Number("<%=nSenha%>")) {
		alert("<bean:message key="prompt.A_senha_digitada_nao_contem_o_minimo_de_caracteres_exigido"/>");
		document.loginForm.funcDsNovoPassword.focus();
		return false;
	}

	document.loginForm.acao.value="loginAlteracaoSenha";
	return true;
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onload="MM_preloadImages('webFiles/images/login/bt_ok02.gif','webFiles/images/login/bt_Sair02.gif');">
<html:form styleId="loginForm" action="/Login.do" focus="funcDsPassword">
	<html:hidden property="tela" value="trocaLogin" />
	<html:hidden property="acao" value="" />

	<table width="547" border="0" cellspacing="0" cellpadding="0" height="358" background="<bean:message key="prompt.image.telaLogin" />">
		<tr>
			<td align="center">
			<table width="80%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalLabel" width="32%" align="right">
						<bean:message key="prompt.login" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
					<td width="52%">
						<html:text property="funcDsLoginname" styleClass="principalObjForm" maxlength="100" />
					</td>
					<td width="16%">&nbsp;</td>
				</tr>
				<tr>
					<td class="principalLabel" width="32%" align="right">
						Senha<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
					<td width="52%">
						<html:password property="funcDsPassword" styleClass="principalObjForm" maxlength="20" />
					</td>
					<td width="16%">&nbsp;</td>
				</tr>
				<tr>
					<td class="principalLabel" width="32%" align="right">
						Nova Senha <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
					<td width="52%">
						<html:password property="funcDsNovoPassword" styleClass="principalObjForm" maxlength="20" />
					</td>
					<td width="16%">&nbsp;</td>
				</tr>
				<tr>
					<td width="32%" class="principalLabel" align="right">
						Confirma Nova Senha <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
					<td width="52%" class="principalLabel">
						<html:password property="funcDsConfirmaNovoPassword" styleClass="principalObjForm" maxlength="20" />
					</td>
					<td width="16%">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3" class="principalLabel"><font color="red"><html:errors /></font></td>
				</tr>
				<tr>
					<td width="32%">&nbsp;</td>
					<td width="52%">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr align="center">
							<td width="50%">
								<input type="image" src="webFiles/images/login/bt_ok01.gif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ImageOK','','webFiles/images/login/bt_ok02.gif',1)" name="ImageOK" border="0" width="72" height="29" onclick="return ValidaCampos()">
							</td>
							<td width="50%">
								<a href="javascript:window.close()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image4','','webFiles/images/login/bt_Sair02.gif',1)">
									<img name="Image4" border="0" src="webFiles/images/login/bt_Sair01.gif" width="71" height="29">
								</a>
							</td>
						</tr>
					</table>
					</td>
					<td width="16%">&nbsp;</td>
				</tr>
				<tr>
					<td width="32%">&nbsp;</td>
					<td width="52%">&nbsp;</td>
					<td width="16%">&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html:html>
