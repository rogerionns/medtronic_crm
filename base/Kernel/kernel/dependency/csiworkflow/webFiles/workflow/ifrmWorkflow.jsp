
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<script>

if(typeof HTMLElement!="undefined" && !
HTMLElement.prototype.insertAdjacentElement){
	HTMLElement.prototype.insertAdjacentElement = function
(where,parsedNode)
	{
		switch (where){
		case 'beforeBegin':
			this.parentNode.insertBefore(parsedNode,this)
			break;
		case 'afterBegin':
			this.insertBefore(parsedNode,this.firstChild);
			break;
		case 'beforeEnd':
			this.appendChild(parsedNode);
			break;
		case 'afterEnd':
			if (this.nextSibling) 
this.parentNode.insertBefore(parsedNode,this.nextSibling);
			else this.parentNode.appendChild(parsedNode);
			break;
		}
	}

	HTMLElement.prototype.insertAdjacentHTML = function
(where,htmlStr)
	{
		var r = this.ownerDocument.createRange();
		r.setStartBefore(this);
		var parsedHTML = r.createContextualFragment(htmlStr);
		this.insertAdjacentElement(where,parsedHTML)
	}


	HTMLElement.prototype.insertAdjacentText = function
(where,txtStr)
	{
		var parsedText = document.createTextNode(txtStr)
		this.insertAdjacentElement(where,parsedText)
	}
}

</script>

<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,
								com.iberia.helper.Constantes, br.com.plusoft.csi.adm.vo.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@page import="br.com.plusoft.fw.app.Application"%>
<%@page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

long idFuncGerador=0;
CsCdtbFuncionarioFuncVo funcionarioVo = null;
if (session != null && session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
	idFuncGerador = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();

	funcionarioVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
}

String fileIncludeBotoes = "/csiworkflow/webFiles/includes/funcoesBotoesExtra.jsp";

//Chamado: 109643 - 23/06/2016 - Victor Godinho
String fileIncludeWorkflow = "/csiworkflow/webFiles/includes/funcoesWorkflow.jsp";

%>

<%//Chamado: 109643 - 23/06/2016 - Victor Godinho %>
<plusoft:include  id="fileIncludeWorkflow" href='<%=fileIncludeWorkflow%>'/>
<bean:write name="fileIncludeWorkflow" filter="html"/>

<plusoft:include  id="funcoesBotoesExtra" href='<%=fileIncludeBotoes%>'/>
<bean:write name="funcoesBotoesExtra" filter="html"/>

<%@page import="br.com.plusoft.saas.SaasHelper"%><html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>
	
	<script language="JavaScript">
		/**********************************************************************
		 Vari�veis com as chaves da maniesta��o e valores utilizados nas telas
		**********************************************************************/
		var idChamCdChamado = 0;
		var maniNrSequencia = 0;
		var idAsn1CdAssuntonivel1 = 0;
		var idAsn2CdAssuntonivel2 = 0;
		var idPessCdPessoa = 0;
		var foupNrSequencia = 0;
		var idTpmaCdTpManifestacao = 0;
		var idTppuCdTipopublico = 0;
		var idEmprCdEmpresa = 0;
		var idMatpCdManiftipo = 0;
		var idLinhCdLinha = 0;
		var idGrmaCdGrupoManifestacao = 0;
		//Danilo Prevides - 04/12/2009 - 67546 - INI
		var pessDsCpf = 0;
		var prasDsProdutoAssunto = '';
		maniTxManifestacao = '';
		//Danilo Prevides - 04/12/2009 - 67546 - FIM
		
		/**************************************************************************
		 Arrays com refer�ncia para todos os controles (iframes e tabelas) da tela
		 para facilitar a manipula��o dos mesmos
		**************************************************************************/
		var iframesTela = new Array();
		var tabelasTela = new Array();
		
		//Auxiliar para esconder ou mostrar as tabelas
		bTbVisivel = new Array();
		bTbVisivel[false] = "none";
		bTbVisivel[true] = "block";


		var idFuncCdFuncionarioSaas = 0;
		var idEmprCdEmpresaSaas = 0;
		var idFuncCdFuncionario = <%=funcionarioVo.getIdFuncCdFuncionario() %>;

		<% if(SaasHelper.aplicacaoSaas) { %>
			idFuncCdFuncionarioSaas = new Number("<%=new SaasHelper().getSessionData().getIdFuncCdFuncionarioSaas() %>");
			idEmprCdEmpresaSaas = new Number("<%=new SaasHelper().getSessionData().getIdEmprCdEmpresaSaas() %>");
		<% } %>

		
		/************************************************************************
		 Fun��o chamada ao clicar em uma pendencia da mesa
		 Zera os locations dos iframes da tela para a nova pendencia selecionada
		************************************************************************/
		function abrirItem(idCham, maniSeq, idAsn1, idAsn2, idPess, foupSeq, idTpma, idTppu, idEmpr, idMatp, idLinh, idGrma){
			idChamCdChamado = idCham;
			maniNrSequencia = maniSeq;
			idAsn1CdAssuntonivel1 = idAsn1;
			idAsn2CdAssuntonivel2 = idAsn2;
			idPessCdPessoa = idPess;
			foupNrSequencia = foupSeq;
			idTpmaCdTpManifestacao = idTpma;
			idTppuCdTipopublico = idTppu;
			idMatpCdManiftipo = idMatp;
			idLinhCdLinha = idLinh;
			idGrmaCdGrupoManifestacao = idGrma;
			
			for(i = 0; i < iframesTela.length; i++){
				document.getElementById(iframesTela[i]).src = "about:blank";
				document.getElementById(iframesTela[i]).carregou = "false";
			}

			//Chamado: 86793 - 08/02/2013 - Carlos Nunes
			var src = 	"../csiworkflow/Menu.do?acao=consultar&resource=WorkFlowWEBmenu.xml"+
						"&idChamCdChamado="+idChamCdChamado+"&maniNrSequencia="+maniNrSequencia+
						"&idAsn1CdAssuntoNivel1="+idAsn1CdAssuntonivel1+"&idAsn2CdAssuntoNivel2="+idAsn2CdAssuntonivel2+
						"&idLinhCdLinha="+idLinhCdLinha+"&idGrmaCdGrupoManifestacao="+idGrmaCdGrupoManifestacao+
						"&idTpmaCdTpManifestacao="+idTpmaCdTpManifestacao+
						"&idMatpCdManifTipo="+idMatp+
						"&idPessCdPessoa="+idPess+
						"&idEmprCdEmpresa=" + parent.empresaDoChamado; //parent.ifrmTelaFiltro.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;

			ifrmMenu.location = src;
		}
				
		/**************************************************************************************
		 A��o do menu clicado.
		 Se o iframe do conteudo correspondente ao menu clicado nao existir, ele � criado.
		 Se o iframe j� existir, � chamado o link para abrir seu conte�do.
		 Se o conte�do j� tiver sido carregado anteriormente, somente mostra o iframe vis�vel.
		**************************************************************************************/
		function acaoMenu(nome, action, parametros, idBotao){

			 if(document.getElementById("ifrm"+ nome) == null){
				iframesTela[iframesTela.length] = "ifrm"+ nome;
				tabelasTela[tabelasTela.length] = "tb"+ nome;
				var novoIframe = "<div id=\""+ tabelasTela[tabelasTela.length - 1] +"\""+
					"width=100% height=100% style=\"display: none\"><iframe id=\""+ iframesTela[iframesTela.length - 1] +"\""+
					"src=\""+ montarLink(action, parametros, idBotao) +"\" width=820 height=640 frameborder=0></iframe></div>";		
				tdConteudo.insertAdjacentHTML('beforeEnd',novoIframe);
				divAguarde.style.visibility = "visible";

			//Se o iframe ainda n�o foi carregado, ele � carregado aqui
			} else if(document.getElementById("ifrm"+ nome).carregou == "false"){
				document.getElementById("ifrm"+ nome).src = montarLink(action, parametros, idBotao);
				document.getElementById("ifrm"+ nome).carregou = "true";
				divAguarde.style.visibility = "visible";
			}
			
			for(i = 0; i < tabelasTela.length; i++) {
				if((tabelasTela[i] == "tb"+ nome)) {
					document.getElementById(tabelasTela[i]).style.display = "";
				} else {
					document.getElementById(tabelasTela[i]).style.display = "none";
				}
			}
			divAguarde.style.visibility = "hidden";
			
		}
		
		/*******************************************
		 Monta o link de acordo com o action passado
		********************************************/
		function montarLink(action, parametros, idBotao){
			var retorno = action;
			if(action == "DadosPess.do"){
				retorno += "?idPessCdPessoa="+ idPessCdPessoa +
					"&acao=<%= Constantes.ACAO_CONSULTAR %>"+
					"&idTpPublico="+ idTppuCdTipopublico +
					"&idChamCdChamado="+ idChamCdChamado + //Chamado: 90471 - 16/09/2013 - Carlos Nunes
					"&idFuncCdFuncionario=<%=idFuncGerador%>";
			}
			else if(action == "WorkFlow.do"){
				retorno += "?acao=<%= MCConstantes.ACAO_SHOW_ALL %>"+
					"&tela=manifestacao"+
					"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ idChamCdChamado +
					"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia="+ maniNrSequencia +
					"&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao="+ idTpmaCdTpManifestacao +
					"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+ idAsn1CdAssuntonivel1 +
					"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2="+ idAsn2CdAssuntonivel2 +
					"&foupNrSequencia="+ foupNrSequencia +
					"&idEmprCdEmpresa=" + parent.empresaDoChamado + //parent.ifrmTelaFiltro.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value+
					"&idFuncCdFuncionario=<%=idFuncGerador%>"; //Chamado: 90471 - 16/09/2013 - Carlos Nunes
			}
			else if(action == "Historico.do"){
				retorno += "?acao=<%= MCConstantes.ACAO_SHOW_ALL %>"+
					"&tela=historico"+
					"&idPessCdPessoa="+ idPessCdPessoa +
					"&consDsCodigoMedico=0" +
					"&idEmprCdEmpresa=" + parent.empresaDoChamado + //parent.ifrmTelaFiltro.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value+
					"&idFuncCdFuncionario=<%=idFuncGerador%>";
			}
			else if(action.indexOf("Farmaco.do?") >= 0){
				retorno += "tela=questionario"+
					"&idFuncCdFuncionario=<%=idFuncGerador%>"+
					"&csNgtbFarmacoFarmVo.idChamCdChamado="+ idChamCdChamado +
					"&csNgtbFarmacoFarmVo.maniNrSequencia="+ maniNrSequencia +
					"&csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1="+ idAsn1CdAssuntonivel1 +
					"&csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2="+ idAsn2CdAssuntonivel2 +
					"&csNgtbFarmacoFarmVo.idTpmaCdTpManifestacao="+ idTpmaCdTpManifestacao +

					//Danilo Prevides - 10/12/2009 - 67945 - INI
					"&csNgtbFarmacoFarmVo.idMatpCdManifTipo=" + idMatpCdManiftipo +					
					"&csNgtbFarmacoFarmVo.idGrmaCdGrupoManifestacao="+ idGrmaCdGrupoManifestacao +
					"&csNgtbFarmacoFarmVo.prasDsProdutoAssunto="+prasDsProdutoAssunto +
					//Danilo Prevides - 10/12/2009 - 67945 - DIM
					
					//"&csNgtbFarmacoFarmVo.prasDsProdutoAssunto="+ dsProduto +
					"&idPessCdPessoa="+ idPessCdPessoa +
					"&idEmprCdEmpresa="+parent.empresaDoChamado; // parent.ifrmTelaFiltro.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
			}
			else{
				if (retorno.indexOf('?') <= 0){
					retorno = retorno + "?";
					retorno += "idFuncCdFuncionario=<%=idFuncGerador%>";
				}else{
					retorno += "&idFuncCdFuncionario=<%=idFuncGerador%>";
				}
				
				retorno += "&idChamCdChamado="+ idChamCdChamado +
					"&maniNrSequencia="+ maniNrSequencia +
					"&idAsn1CdAssuntoNivel1="+ idAsn1CdAssuntonivel1 +
					"&idAsn2CdAssuntoNivel2="+ idAsn2CdAssuntonivel2 +
					"&idPessCdPessoa="+ idPessCdPessoa +
					"&idTpmaCdTpManifestacao=" + idTpmaCdTpManifestacao +
					
					//Chamado: 101679 - SOUZACRUZ - 04.44.02.04 / 04.44.03 - 09/06/2015 - Marco Costa
					"&idMatpCdManifTipo=" + idMatpCdManiftipo +
					"&idLinhCdLinha=" + idLinhCdLinha +
					"&prasDsProdutoAssunto=" +// + prasDsProdutoAssunto +
					"&idGrmaCdGrupoManifestacao=" + idGrmaCdGrupoManifestacao +
					
					"&idEmprCdEmpresa=" + parent.empresaDoChamado; //parent.ifrmTelaFiltro.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;


				//especifico para a Wurth
				try{
					retorno += "&pessCdCorporativo="+ ifrmPESSOA.document.forms[0].pessCdCorporativo.value;
				}catch(x){}
				
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
					retorno += "&idTppuCdTppublico="+ idTppuCdTipopublico;
				<%}else{%>
					retorno += "&idTppuCdTppublico="+ ifrmPESSOA.document.ifrmCmbTipoPub.retornaValor();
				<%}%>
				
				retorno = obterLink(retorno,parametros,idBotao);
				
			}
            //Chamado: 93411 - 06/03/2014 - Carlos Nunes
			retorno = tratarParametros(retorno,parametros);
			
			return retorno;
		}

		//Chamado: 93411 - 06/03/2014 - Carlos Nunes
		function tratarParametros(retorno, parametros) {
			
			//Chamado 109857 - 23/06/2016 Victor Godinho
			if (isTratarParametrosEspec()) {
				return tratarParametrosEspec(retorno, parametros);
			}
			
			if(parametros == undefined) return retorno;
			
			for (var i = 0; i < parametros.length; i++){
			
				if(parametros[i][1] == "pessCdCorporativo" && retorno.indexOf("pessCdCorporativo") < 0){
					
					try{
						if (parametros[i][3] == 'S' && ifrmPESSOA.document.forms[0].pessCdCorporativo.value == ""){
							alert("<bean:message key="prompt.alert.parametroObrigatorioCompl"/>");
							return "";
						}
						
						retorno += "&pessCdCorporativo="+ ifrmPESSOA.document.forms[0].pessCdCorporativo.value;
					}catch(x){}
				}
				else if(parametros[i][1] == "pessDsCpf" && retorno.indexOf("pessDsCpf") < 0){
					
					try{
						
						if (parametros[i][3] == 'S' && ifrmPESSOA.document.forms[0].pessDsCpf.value == ""){
							alert("<bean:message key="prompt.alert.parametroObrigatorioCompl"/>");
							return "";
						}
						
						retorno += "&pessDsCpf="+ ifrmPESSOA.document.forms[0].pessDsCpf.value;
					}catch(x){}
				}
				else{
					
					try{
						if (parametros[i][3] == 'S'&& eval(parametros[i][2]) != undefined && eval(parametros[i][2]) == ""){
							alert("<bean:message key="prompt.alert.parametroObrigatorioCompl"/>");
							return "";
						}
						else{
							//Chamado: 102108 - 07/07/2015 - Carlos Nunes
							if(retorno.indexOf(parametros[i][1]) < 0){
							
								if(eval(parametros[i][2]) != undefined){
									retorno += "&"+parametros[i][1]+"="+ eval(parametros[i][2]);
								}
								else{
									retorno += "&"+parametros[i][1]+"=";
								}
							}
						}
					}catch(x){
						retorno += "&"+parametros[i][1]+"=";
					}
				}

			}
			
			return retorno;
		}

		/*
		Este metodo tem como objetivo receber todos os campos mais os parametros para as bustituicoes
		
		
		FUN��O ABAIXO TRASFERIDA PARA O INCLUDE DE funcoesBotoesExtra.jsp		
		*/
		<%-- function obterLink(link, parametros, idBotao){
		//parametros[0...][1] - paboDsParametrobotao
		//parametros[0...][2] - paboDsNomeinterno
		//parametros[0...][3] - paboDsParametrointerno
		//parametros[0...][4] - paboInObrigatorio

			var valor = "";
			var newLink = link;
			var bExecutavel = (link.toUpperCase().indexOf("EXE") > 0);
			var encontrouParametro = false;
			
			if(parametros == undefined) return;
			
			if(bExecutavel && parametros.length > 0){
				if(newLink.indexOf(" ") > 0)
					newLink = "'"+ newLink.substring(0, newLink.indexOf(" ")) +"' '"+ newLink.substring(newLink.indexOf(" "));
				else
					newLink = "'"+ newLink +"' '";
			}
			else if(bExecutavel){ 
				newLink = "'"+ newLink;
			}
			
			for (var i = 0; i < parametros.length; i++){
				valor = "";
				
				try{			
					//onde obter a informacao
					if(parametros[i][2] == "perm"){
						//valor = findPermissoesByFuncionalidade("adm.fc." + idBotao + ".");
					}else{
						//if(parametros[i][3] == "c" && !isNaN(window.top.superiorBarra.barraCamp.chamado.innerText))
						//	valor = Number(window.top.superiorBarra.barraCamp.chamado.innerText);
						//else
							valor = eval(parametros[i][3]);					
					}
					encontrouParametro = true;
				}catch(e){
					encontrouParametro = false;
				}
				
				//Se o parametro e obrigatorio
				if (parametros[i][4] == 'S'){

					if(newLink.indexOf(parametros[i][2]) == -1){
					
						if(parametros[i][2] == "idChamCdChamado" && (valor == "" || valor == "0" || valor == "Novo")){
							alert("<bean:message key="prompt.alert.parametroObrigatorio"/>");
							return "";
						}
	
						if (parametros[i][2] == ""){
							alert("<bean:message key='prompt.naoFoiPossivelObterNomeInternoParametroObrigatorio'/>" + " " + parametros[i][1]);
							return "";
						}
	
	
						if (valor == "" || valor == "0"){
							alert("<bean:message key="prompt.alert.parametroObrigatorio"/>");
							return "";
						}
					}
				}
				
				if (valor != null && encontrouParametro){

					if(newLink.indexOf(parametros[i][2]) == -1){
						
						if (newLink.indexOf('?') == -1 && !bExecutavel){
							newLink = newLink + "?";
						}
						
						var ultimoCaracter = newLink.substring(newLink.length - 1);
						if (ultimoCaracter != "?" && ultimoCaracter != "&"){
							newLink = newLink + "&";
						}

						newLink = newLink + parametros[i][2] + "=" + valor;
					}
					
				}
			}
			
			if(bExecutavel)
				newLink += "'";
			
			if (!bExecutavel && newLink.indexOf('http') == -1){
				if (newLink.indexOf('?') == -1){
					newLink = newLink + "?";
				}
				
				var ultimoCaracter = newLink.substring(newLink.length - 1);
				if (ultimoCaracter != "?" && ultimoCaracter != "&"){
					newLink = newLink + "&";
				}
				newLink+= "idBotaCdBotao="+idBotao;
				<% if(SaasHelper.aplicacaoSaas) { %>
				newLink = obterLinkSaas(newLink);
				<% } %>
						
			}
			
			return newLink; 
		} --%>


		function obterLinkSaas(newLink) {
			if(idFuncCdFuncionarioSaas > 0) {
				newLink+= "&idFuncCdFuncionarioSaas="+idFuncCdFuncionarioSaas;

				// Se o link come�a com ../ significa que � outro contexto, ent�o passa o id do funcion�rio logado e o id de empresa da sess�o
				if(newLink.indexOf("../") == 0) {
					if(newLink.indexOf("idFuncCdFuncionario=") == -1) {
						newLink+= "&idFuncCdFuncionario="+idFuncCdFuncionario;
					}
					if(newLink.indexOf("idEmprCdEmpresa=") == -1) {
						newLink+= "&idEmprCdEmpresa="+window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
					}
				}
				
			}

			return newLink;
		}
		
	</script>
	
	<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" scroll="no">
		<div id="divAguarde" style="position:absolute; left:450px; top:200px; width:199px; height:148px; z-index:1000; visibility: visible"> 
  			<iframe src="webFiles/workflow/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
		</div>
		<table width="100%" height="90%" cellpadding=0 cellspacing=0 border=0>
			<tr>
				<td width="17%" height="90%" valign="top">
					<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
					<iframe name="ifrmMenu" id="ifrmMenu" src="" width="100%" height="95%" frameborder=0 scrolling="no"></iframe>
				</td>
				<td id="tdConteudo" valign="top" width="83%" height="90%"></td>
			</tr>
		</table>
	</body>
	<!-- Chamado: 85859 - 25/01/2013 - Carlos Nunes -->
	<iframe name="ifrmPooling" id="ifrmPooling" src="" width="0" height="0" frameborder=0 scrolling="no"></iframe>
</html>

<script>

// Chamado: 85859 - 25/01/2013 - Carlos Nunes
function executarPooling()
{
	ifrmPooling.location="../../csicrm/webFiles/pooling.jsp";

	setTimeout("executarPooling()", 60000);
}

executarPooling();

</script>