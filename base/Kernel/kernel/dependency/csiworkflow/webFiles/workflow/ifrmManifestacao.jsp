<%@ page language="java" import="br.com.plusoft.csi.adm.helper.*,
								br.com.plusoft.csi.adm.util.Geral, 
								br.com.plusoft.csi.crm.form.ManifestacaoForm, 
								br.com.plusoft.csi.crm.helper.MCConstantes, 
								com.iberia.helper.Constantes, 
								br.com.plusoft.csi.adm.vo.*" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

//Chamado: 101865 KERNEL-1288 - 04.40.21.04 - 04.40.22 - 04.44.03 - 12/06/2015 - Marcos Donato //
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

String locale = "";
if(request.getSession().getAttribute("org.apache.struts.action.LOCALE") != null){
	locale = ((Locale)request.getSession().getAttribute("org.apache.struts.action.LOCALE")).getLanguage();
}

long idFuncGerador=0;
long idIdioma=0;
String nomeGerador="";
if (session != null && session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
	idFuncGerador = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
	nomeGerador = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario();
	idIdioma = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();
}

%>

<%
long idNivelAcesso=0;
if (request.getSession() != null && request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null)
	idNivelAcesso = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso();
%>

<%
String fileIncludeBotoes = "/csiworkflow/webFiles/includes/funcoesBotoesExtra.jsp";

String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>
<plusoft:include  id="funcoesManif" href='<%=fileInclude%>'/>
<bean:write name="funcoesManif" filter="html"/>

<plusoft:include  id="funcoesBotoesExtra" href='<%=fileIncludeBotoes%>'/>
<bean:write name="funcoesBotoesExtra" filter="html"/>


<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="br.com.plusoft.fw.log.Log"%>

<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%>
<%@page import="br.com.plusoft.fw.webapp.RequestHeaderHelper"%><html>
<head>
<title><bean:message key="prompt.csiworkflow" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script type="text/javascript" src="webFiles/js/funcoesMozilla.js"></script>
<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script type="text/javascript" src='webFiles/funcoes/TratarDados.js'></script>
<script type="text/javascript" src="webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>

<script type="text/javascript">
//Action Center - Objeto criado, porque no Firefox e Chrome n�o funciona passar o objeto como par�metro da fun��o showModal
var objBinoculo = new Object();

function setarObjBinoculo(obj)
{
	objBinoculo = obj;
}


var bUltimaEtapa = false;

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
} 


<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->

var dataAtual = "";
<logic:present name="manifestacaoForm" property="dataAssinatura">
	dataAtual = "<bean:write name="manifestacaoForm" property="dataAssinatura"/>";
</logic:present>

var assinatura = "(<%=String.valueOf(((CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario())%> " + dataAtual + ")";


var pendenteConclusao = false;

function getDataBanco()
{
	if(dataAtual != "")
	{
		return dataAtual.substring(0,10);
	}
}

function getHoraBanco()
{
	if(dataAtual != "")
	{
		return dataAtual.substring(11,dataAtual.length);
	}
}

function consultaManifestacao() {
	var chamado = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value;
	var manifestacao = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value;
	var tpManifestacao = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
	var assuntoNivel = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value + "@" + manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
	var assuntoNivel1 = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value;
	var assuntoNivel2 = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
	var idPessoa = <%=((ManifestacaoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()%>;

	<%if(CONF_FICHA_NOVA){ //Chamado: 101865 KERNEL-1288 - 04.40.21.04 - 04.40.22 - 04.44.03 - 12/06/2015 - Marcos Donato // %>
		var url = '/csicrm/FichaManifestacao.do?idChamCdChamado='+ chamado +
		'&maniNrSequencia='+ manifestacao +
		'&idTpmaCdTpManifestacao='+ tpManifestacao +
		'&idAsnCdAssuntoNivel='+ assuntoNivel1 + "@" + assuntoNivel2 +
		'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
		'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
		'&idPessCdPessoa='+ idPessoa +
		'&idEmprCdEmpresa='+ <%=empresaVo.getIdEmprCdEmpresa()%> +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csiworkflow';
				
		showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}else{%>
		showModalDialog('<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&workflow=followup&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + tpManifestacao + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=' + assuntoNivel + "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + assuntoNivel2,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}%>
}

function voltar() {
	if(window.dialogArguments == undefined){
	window.location = 'WorkFlow.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_PENDENTES%>';
	}else{
		window.close();
}
}

function addFollowup() {
	var regData;
	var regHora;

	if (document.all.item("altLinha").value == "0" || lstFollowup.document.getElementById("lstFollowup" + document.all.item("altLinha").value) == null) {
		
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_INCLUSAO%>')){
			alert('<plusoft:message key="prompt.semPermissaoParaIncluirNovoFollowUp"/>');
			return false;
		}
	}
	
	regData = document.manifestacaoFollowupForm.registro.value.substr(0,10);
	regHora = document.manifestacaoFollowupForm.registro.value.substr(11,8);

	if (regData=='') regData = getDataBanco();
	if (regHora=='') regHora = getHoraBanco();
	
	if (document.manifestacaoFollowupForm.dataPrevista.value == "" && document.manifestacaoFollowupForm.horaPrevista.value == "" && document.all.item("chkDataConclusao").checked) {
		document.manifestacaoFollowupForm.dataPrevista.value = regData;
		document.manifestacaoFollowupForm.horaPrevista.value = regHora;
	}

	if (cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value == "") {
		alert('<plusoft:message key="prompt.alert.combo.resp" />');
		cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].focus();
	} else if (cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value == "") {
		alert('<plusoft:message key="prompt.alert.combo.even" />');
		cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].focus();
	} else if (document.getElementsByName("textoHistorico")[0].value == "") {
		alert('<plusoft:message key="prompt.alert.texto.follow" />');
		document.getElementsByName("textoHistorico")[0].focus();
	} else if (document.all.item("dataPrevista").value == "") {
		alert('<plusoft:message key="prompt.alert.texto.data" />');
		document.all.item("dataPrevista").focus();
	} else if (document.all.item("horaPrevista").value == "") {
		alert('<plusoft:message key="prompt.alert.texto.hora" />');
		document.all.item("horaPrevista").focus();
	} else if (!(verificaDataHora(document.all.item("dataPrevista")) && verificaHora(document.all.item("horaPrevista")))) {
		return false;
	} else if (document.all.item("codigo").value == "0" && !validaPeriodoHora(regData, document.all.item("dataPrevista").value, regHora, document.all.item("horaPrevista").value)) {
		alert('<plusoft:message key="prompt.alert.data.hora.menor" />');
		document.all.item("dataPrevista").focus();
	} else {
	
	
		var val="";
		val = codificaStringHtml(document.getElementsByName("textoHistorico")[0]);
		
		if (document.getElementsByName("textoComplemento")[0].value != "") {
			val += "QBRLNH-----------------------------------------------------------QBRLNH";
			val += "<plusoft:message key="prompt.incluidopor" /> <%=((br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo) session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario()%> <plusoft:message key="prompt.em" /> " + getDataBanco() + " " + getHoraBanco() + "QBRLNH";
			
			var valCompl="";
			valCompl = codificaStringHtml(document.getElementsByName("textoComplemento")[0]);
			
			val += valCompl;
			
		}

		//chamada de funcao espec, cham 66976
		var retorno = false;
		try{
			retorno = antesConfirmarFollowup();
			if(!retorno) return;
		}catch(e){}
		
		var nIdFuncEncerramento = 0;

		if (document.all.item("encerramento").checked) {
			if (document.all.item("idFuncCdEncerramento").value!=null && document.all.item("idFuncCdEncerramento").value!='' && document.all.item("idFuncCdEncerramento").value!='0') { 
				nIdFuncEncerramento =  document.all.item("idFuncCdEncerramento").value;
			} else {
				nIdFuncEncerramento =  <%=idFuncGerador%>;
			} 
		}

		if (document.all.item("altLinha").value == "0" || lstFollowup.document.getElementById("lstFollowup" + document.all.item("altLinha").value) == null) {
			lstFollowup.addFollowup(cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value,
									cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value,
									cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].options[cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].selectedIndex].text,
									cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value,
									cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].options[cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].selectedIndex].text,
									//document.all.item("registro").value, // 91033 - 18/09/2013 - Jaider Alba
									document.manifestacaoFollowupForm.registro.value, 
									document.all.item("dataPrevista").value + " " + document.all("horaPrevista").value,
									document.all.item("txtDtEfetiva").value + " " + document.all("txtHrEfetiva").value,
									val,
									document.all.item("codigo").value,
									document.all.item("nomeGerador").value,
									document.all.item("codigoGerador").value,
									document.all.item("encerramento").checked?"true":"false",
									"N",
									document.all.item("dhEnvio").value,
									nIdFuncEncerramento, 0); //Chamado: 89940 - 01/08/2013 - Carlos Nunes
		} else {
			lstFollowup.alterFollowup(cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value,
									cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value,
									cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].options[cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].selectedIndex].text,
									cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value,
									cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].options[cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].selectedIndex].text,
									//document.all.item("registro").value, // 91033 - 18/09/2013 - Jaider Alba
									document.manifestacaoFollowupForm.registro.value,
									document.all.item("dataPrevista").value + " " + document.all("horaPrevista").value,
									document.all.item("txtDtEfetiva").value + " " + document.all("txtHrEfetiva").value,
									val,
									document.all.item("codigo").value, 
									document.all.item("altLinha").value,
									document.all.item("nomeGerador").value,
									document.all.item("codigoGerador").value,
									document.all.item("encerramento").checked?"true":"false",
									document.all.item("inEnvioFollowup").value,
									document.all.item("dhEnvio").value,
									nIdFuncEncerramento,
									document.all.item("matm").value); //Chamado: 89940 - 01/08/2013 - Carlos Nunes
		}
		
		//DECRETO
		if(cmbEvento.document.manifestacaoFollowupForm['csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup'].options[cmbEvento.document.manifestacaoFollowupForm['csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup'].selectedIndex].getAttribute("inTextoManif") == "S" && cmbEvento.document.manifestacaoFollowupForm['csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup'].value != ""){
			if(document.getElementsByName("textoHistorico")[0].readOnly == true){
				document.all.item("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao").value += "\n" + assinatura + "\n" + document.getElementsByName("textoComplemento")[0].value;
			}else{
				document.all.item("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao").value += "\n" + assinatura + "\n" + document.getElementsByName("textoHistorico")[0].value;		
			}
		}
		
		document.all.item("dataPrevista").value = "";
		document.all.item("horaPrevista").value = "";
		document.all.item("txtDtEfetiva").value = "";
		document.all.item("txtHrEfetiva").value = "";
		document.getElementsByName("textoHistorico")[0].value = "";
		document.getElementsByName("textoComplemento")[0].value = "";
		document.all.item("registro").value = "";
		document.all.item("efetivo").value = "";
		document.all.item("codigo").value = "0";
		document.all.item("nomeGerador").value = '<%=nomeGerador%>';
		document.all.item("codigoGerador").value = '<%=idFuncGerador%>';
		document.all.item("encerramento").checked = false;
		document.all.item("altLinha").value = "0";
		document.all.item("dhEnvio").value = "";
		document.all.item("idFuncCdEncerramento").value = "";

        //Chamado: 89940 - 01/08/2013 - Carlos Nunes
		document.all.item("matm").value = "0";

		cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = false;
		cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].disabled = false;
		atualizaCmbEvento(0);
		//cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].disabled = false;
		document.all.item("registro").disabled = false;
		document.all.item("dataPrevista").disabled = false;
		document.all.item("horaPrevista").disabled = false;
		document.all.item("imgDataFoup").disabled = false;
		document.getElementsByName("textoHistorico")[0].readOnly = false;
		document.getElementsByName("textoHistorico")[0].disabled = false;
		document.all.item("encerramento").disabled = false;
		document.getElementsByName("textoComplemento")[0].readOnly = true;

		cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value = "";
		//cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value = "";
		//cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = "";
		//cmbArea.submeteForm();
		//cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = "";

		setTimeout("clearArea();",500);
	}
}

var nClearArea = 0;
function clearArea(){

	try{
		cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value = "";
		cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = "";
		cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = "";
		cmbArea.submeteForm();
		
	}catch(e){

		if(nClearArea < 5){
			setTimeout("clearArea();", 500);
			nClearArea++;
		}
	}	
}

function adicionarFunc(){
		if (cmbDestinatarioManif.document.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value == ""){
			alert("<plusoft:message key="prompt.alert.combo.func" />");
			cmbDestinatarioManif.document.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].focus();
			return false;
		}
		lstDestinatario.addFunc(cmbAreaDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].options[cmbAreaDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].selectedIndex].text, 
						cmbDestinatarioManif.document.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].options[cmbDestinatarioManif.document.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].selectedIndex].text,
						cmbDestinatarioManif.document.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value,
						0, false, "S", false, "", "", "", false,0, 0, "", "", 
						cmbDestinatarioManif.document.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value, ""); 
}

var bEnvia = true;

function validaAbasEspec(){
	var especOk = true;
	document.getElementById("camposManifEspec").innerHTML="";
	for(i = 0; i <= abasTela.length; i++){
		if(document.getElementById("ifrmEspec"+ i) != undefined){
		try{
			especOk = eval("ifrmEspec"+ i +".validaEspec();");
		}catch(e){}
	}

	if(!especOk) 
		return false;
	}
	
	return true;
} 

function submeteSalvar() {
	if(!validaAbasEspec())
		return; 

	gravarAbasEspec();

	if (!bEnvia) {
		return false;
	}
	bEnvia = false;
	
	manifestacaoForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value = trim(manifestacaoForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value);
	
	//Chamado: 95208 - 16/07/2014 - Carlos Nunes
    //Campo acrescentado para fazer o controle da resposta do destinat�io
    //caso a manifesta��o esteja encerrada o operador n�o pode dar seguimento no fluxo.
    //e a resposta deve ter a assinatura do operador
	if(manifestacaoForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value != ""){
		if(document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value != ""){
			
			manifestacaoForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value += "\n" + assinatura;
		}
	}
	
	if(manifestacaoForm.chkFluxoPadrao[0].checked == true){
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.seguirFluxo"].value = "S";
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.sucessoEtapa"].value = "S";
   	}else if(manifestacaoForm.chkFluxoPadrao[1].checked == true){
   		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.seguirFluxo"].value = "S";
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.sucessoEtapa"].value = "N";
  	}else if(manifestacaoForm.chkFluxoPadrao[2].checked == true){
  		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.seguirFluxo"].value = "N";
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.sucessoEtapa"].value = "N";
	}

	if(manifestacaoForm.chkFluxoPadrao[0].checked || manifestacaoForm.chkFluxoPadrao[1].checked || manifestacaoForm.chkFluxoPadrao[2].checked ){
		if(manifestacaoForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value == ""){
			alert("<plusoft:message key='prompt.favorPreencherCampoResposta'/>");
			limpaCampos();
			bEnvia = true;
			return;
		}
	 //Chamado: 95208 - 16/07/2014 - Carlos Nunes
    //Campo acrescentado para fazer o controle da resposta do destinat�io
    //caso a manifesta��o esteja encerrada o operador n�o pode dar seguimento no fluxo.
    //e a resposta deve ter a assinatura do operador
	} else if(document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value == ""){
		//Chamado: 86354 - 18/01/2013 - Carlos Nunes
		if(!bUltimaEtapa && getIdEtapaProcesso() > 0 && manifestacaoForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value != ""){
			alert("<plusoft:message key='prompt.eNecessarioResponderPerguntaFluxoSimNaoSelecionarOpcaoEncaminharManual'/>");
			limpaCampos();
			bEnvia = true;
			return false;
		}
	}

	manifestacaoForm.inCarregouPendenteConclusao.value = pendenteConclusao;
	
	if (validaCampos()) {
		if(funcionalidadeAdicionalWkf()){	

			validarSubmitManifestacao(); //cham 66976
		} else {
			limpaCampos();
			bEnvia = true;
		}
	} else {
		limpaCampos();
		bEnvia = true;
	}
}

function validarSubmitManifestacao(){

	var ajax = new ConsultaBanco("", "/csiworkflow/ValidaManifestacao.do");

	ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value);
	ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value);
	ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value);
	ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value);
	ajax.addField("csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value);

	//Chamado: 97610 - 15/12/2014 - Carlos Nunes
	var contDestResp = new Number(0);
	
	 //Obtem o funcionario responsavel da manifestacao atual
	for (var i = 0; i < lstDestinatario.document.getElementsByName("idFuncCdFuncionario").length; i++) {
		if (lstDestinatario.document.getElementsByName("madsInParaCc")[i].checked) {
			ajax.addField("csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario", lstDestinatario.document.getElementsByName("idFuncCdFuncionario")[i].value);
			//break;
			contDestResp++;
		}
	}
	//Chamado: 97610 - 15/12/2014 - Carlos Nunes
	if(contDestResp > 1){
		alert("<plusoft:message key='prompt.aviso.dois.destinatarios'/>");
    	limpaCampos();
    	habilitaBtGravar();
    	return;
    }
	
	ajax.addField("modulo", "workflow");

	 
	ajax.executarConsulta(callbackValidarSubmitManifestacao, false, true);
	
}

function callbackValidarSubmitManifestacao(ajax) {
	if(ajax.getMessage() != ''){
		showError(ajax.getMessage());
		return false; 
	}
	
	rs = ajax.getRecordset();
	if(rs.next()) {
		var sucesso = rs.get("sucesso");
		var msg = rs.get("mensagem");

		if(msg != "") {
			alert(msg);
		} 

		continuaSubmitManifestacao(sucesso);
	} else {
		alert("<plusoft:message key='prompt.naoFoiPossivelValidarGravacaoManifestacao'/>");
	}

}

function continuaSubmitManifestacao(retorno){

	if(retorno == "S"){

		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value = cmbStatusManif.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value;
		//parent.divAguarde.style.visibility = "visible";
		manifestacaoForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao'].disabled= false;
		manifestacaoForm.target = this.name = "manifestacao"
		manifestacaoForm.submit();
		
	}else{
		limpaCampos();
		bEnvia = true;
	}	
}

function gravarAbasEspec(){
	try{
		document.getElementById("camposManifEspec").innerHTML="";
		for(i = 0; i <= abasTela.length; i++)
			if(document.getElementById("ifrmEspec"+ i) != undefined){
				eval("ifrmEspec"+ i +".setValoresToForm(document);");
			}
	}catch(x){}	
}

function Salva_CamposManifEspec() {
	ManifestacaoEspec.setValoresToForm(document);
}

function setValoresToForm(form){
}
	
function validaCampos() {
  limpaCampos();
  return (validaCamposDestinatario() && validaCamposConclusao(validaCamposFollowup()) && validaFollowupAberto());
}

function validaFollowupAberto() {

	try{
		if(cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value != "" ||
				cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value != "" ||
				cmbEvento.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value != ""
		){
			AtivarPasta('FOLLOWUP');
			alert('<plusoft:message key="alert.manifestacao.followup.aberto" />');
			return false;
		}else{
			return true;
		}
	}catch(e){
		return false;
	}
	
}

function validaCamposDestinatario() {
	if (lstDestinatario.document.all["idFuncCdFuncionario"] != null) {
	  var inPara = false;
		if (lstDestinatario.document.all["idFuncCdFuncionario"].length == undefined) {
			manifestacaoForm.idFuncCdFuncionario.value = lstDestinatario.document.all["idFuncCdFuncionario"].value + ";";
			manifestacaoForm.idFuncCdFuncAreaResponsavel.value = lstDestinatario.document.all["idFuncCdFuncAreaResponsavel"].value + ";";
			manifestacaoForm.idMadsNrSequencial.value = lstDestinatario.document.all["idMadsNrSequencial"].value + ";";
		  	manifestacaoForm.madsInParaCc.value = (lstDestinatario.document.all["madsInParaCc"].checked?"true":"false") + ";";

			<%//Chamado: 100339 KERNEL-1041 - 14/04/2015 - Marcos Donato // %>
		  	if( lstDestinatario.document.all["madsInMail"].getAttribute('mailT') != null &&
		  			lstDestinatario.document.all["madsInMail"].getAttribute('mailT') != '' ){
		  		manifestacaoForm.madsInMail.value = "T;";
		  	} else {
			  	manifestacaoForm.madsInMail.value = (lstDestinatario.document.all["madsInMail"].checked?"true":"false") + ";";
		  	}

		  	manifestacaoForm.madsInPapel.value = (lstDestinatario.document.all["madsInPapel"].checked?"true":"false") + ";";
		  	manifestacaoForm.madsDhResposta.value = (lstDestinatario.document.all["madsDhResposta"].value!=''?lstDestinatario.document.all["madsDhResposta"].value:' ') + "|";
		  	manifestacaoForm.madsDhPrevisao.value = (lstDestinatario.document.all["madsDhPrevisao"].value!=''?lstDestinatario.document.all["madsDhPrevisao"].value:' ') + "|";
		  	manifestacaoForm.madsDhPrevisaoOriginal.value = (lstDestinatario.document.all["madsDhPrevisaoOriginal"].value!=''?lstDestinatario.document.all["madsDhPrevisaoOriginal"].value:' ') + "|";
		  	if (lstDestinatario.document.all["madsInParaCc"].checked) {
		  		inPara = true;
		  		
		  		if (lstDestinatario.document.all["madsDhEnvio"].value == '')
		  		  manifestacaoForm.madsDhEnvio.value = 'true';
		  		else
		  		  manifestacaoForm.madsDhEnvio.value = (lstDestinatario.document.all["madsDhEnvio"].value!=''?lstDestinatario.document.all["madsDhEnvio"].value:' ') + "|";

		  		manifestacaoForm.madsTxResposta.value = (document.getElementsByName("csAstbManifestacaoDestMadsVo.madsTxResposta")[0].value!=''?document.getElementsByName("csAstbManifestacaoDestMadsVo.madsTxResposta")[0].value:' ') + "|";
		  		
		  		if(manifestacaoForm.madsTxResposta.value != "" && manifestacaoForm["csAstbManifestacaoDestMadsVo.madsTxResposta"].readOnly != true)
		  			manifestacaoForm.madsInModulo.value = 'W' + '|';
		  		else
					manifestacaoForm.madsInModulo.value = lstDestinatario.document.all["madsInModulo"].value + " |";
		  		
		  	} else {
		  	  manifestacaoForm.madsDhEnvio.value = (lstDestinatario.document.all["madsDhEnvio"].value!=''?lstDestinatario.document.all["madsDhEnvio"].value:' ') + "|";
		  	  manifestacaoForm.madsTxResposta.value = (lstDestinatario.document.all["madsTxResposta"].value!=''?lstDestinatario.document.all["madsTxResposta"].value:' ') + "|";
		  	  manifestacaoForm.madsInModulo.value = lstDestinatario.document.all["madsInModulo"].value + " |";
		  	}
		  	
		  	manifestacaoForm.madsIdRepaCdRespostaPadrao.value = lstDestinatario.document.all["idRepaCdRespostaPadrao"].value + ";";
		  	manifestacaoForm.madsIdEtprCdEtapaProcesso.value = lstDestinatario.document.all["idEtprCdEtapaProcesso"].value + ";";
		  	manifestacaoForm.madsIdFuncCdAlteraPrevisao.value = lstDestinatario.document.all["idFuncCdAlteraPrevisao"].value + ";";
		} else {
		  for (var i = 0; i < lstDestinatario.document.all["idFuncCdFuncionario"].length; i++) {
		  	manifestacaoForm.idFuncCdFuncionario.value += lstDestinatario.document.all["idFuncCdFuncionario"][i].value + ";";
		  	manifestacaoForm.idFuncCdFuncAreaResponsavel.value += lstDestinatario.document.all["idFuncCdFuncAreaResponsavel"][i].value + ";";
			manifestacaoForm.idMadsNrSequencial.value += lstDestinatario.document.all["idMadsNrSequencial"][i].value + ";";
		  	manifestacaoForm.madsInParaCc.value += (lstDestinatario.document.all["madsInParaCc"][i].checked?"true":"false") + ";";

			<%//Chamado: 100339 KERNEL-1041 - 14/04/2015 - Marcos Donato // %>
		  	if( lstDestinatario.document.all["madsInMail"][i].getAttribute('mailT') != null &&
		  			lstDestinatario.document.all["madsInMail"][i].getAttribute('mailT') != '' ){
		  		manifestacaoForm.madsInMail.value += "T;";
		  	} else {
				manifestacaoForm.madsInMail.value += (lstDestinatario.document.all["madsInMail"][i].checked?"true":"false") + ";";
		  	}
		  	
		  	manifestacaoForm.madsInPapel.value += (lstDestinatario.document.all["madsInPapel"][i].checked?"true":"false") + ";";
		  	manifestacaoForm.madsDhResposta.value += (lstDestinatario.document.all["madsDhResposta"][i].value!=''?lstDestinatario.document.all["madsDhResposta"][i].value:' ') + "|";
		  	manifestacaoForm.madsDhPrevisao.value += (lstDestinatario.document.all["madsDhPrevisao"][i].value!=''?lstDestinatario.document.all["madsDhPrevisao"][i].value:' ') + "|";
		  	manifestacaoForm.madsDhPrevisaoOriginal.value += (lstDestinatario.document.all["madsDhPrevisaoOriginal"][i].value!=''?lstDestinatario.document.all["madsDhPrevisaoOriginal"][i].value:' ') + "|";
		  	if (lstDestinatario.document.all["madsInParaCc"][i].checked) {
		  		inPara = true;
		  		
		  		//manifestacaoForm.madsInModulo.value += 'W' + '|';
		  		
		  		if (lstDestinatario.document.all["madsDhEnvio"][i].value == '')
		  			manifestacaoForm.madsDhEnvio.value += 'true' + '|';
		  		else
		  			manifestacaoForm.madsDhEnvio.value += (lstDestinatario.document.all["madsDhEnvio"][i].value!=''?lstDestinatario.document.all["madsDhEnvio"][i].value:' ') + "|";
		  		
				manifestacaoForm.madsTxResposta.value += (document.getElementsByName("csAstbManifestacaoDestMadsVo.madsTxResposta")[0].value!=''?document.getElementsByName("csAstbManifestacaoDestMadsVo.madsTxResposta")[0].value:' ') + "|";
				
				if(manifestacaoForm.madsTxResposta.value != "" && manifestacaoForm["csAstbManifestacaoDestMadsVo.madsTxResposta"].readOnly != true)
		  			manifestacaoForm.madsInModulo.value += 'W' + '|';
				else
					manifestacaoForm.madsInModulo.value += lstDestinatario.document.all["madsInModulo"][i].value + " |";
				
		  	} else {
		  	    manifestacaoForm.madsDhEnvio.value += (lstDestinatario.document.all["madsDhEnvio"][i].value!=''?lstDestinatario.document.all["madsDhEnvio"][i].value:' ') + "|";
			  	manifestacaoForm.madsTxResposta.value += (lstDestinatario.document.all["madsTxResposta"][i].value!=''?lstDestinatario.document.all["madsTxResposta"][i].value:' ') + "|";
			  	manifestacaoForm.madsInModulo.value += lstDestinatario.document.all["madsInModulo"][i].value + " |";
		  	}
		  	manifestacaoForm.madsIdRepaCdRespostaPadrao.value += lstDestinatario.document.all["idRepaCdRespostaPadrao"][i].value + ";";
		  	manifestacaoForm.madsIdEtprCdEtapaProcesso.value += lstDestinatario.document.all["idEtprCdEtapaProcesso"][i].value + ";";
		  	manifestacaoForm.madsIdFuncCdAlteraPrevisao.value += lstDestinatario.document.all["idFuncCdAlteraPrevisao"][i].value + ";";
		  }
		}
		
		manifestacaoForm.madsTxResposta.value = descodificaStringHtml(manifestacaoForm.madsTxResposta.value);
		
	  if (!inPara) {
			alert('<plusoft:message key="prompt.alert.radio.resp.dest" />');
			return false;
	  }
	} /*else {
		alert('<plusoft:message key="prompt.alert.inclu.dest" />');
		return false;
	}*/
	return true;
}

function validaCamposFollowup() {
  var encerrado = true;
  if (lstFollowup.document.all["codigo"] != null) {
		if (lstFollowup.document.all["codigo"].length == undefined) {
			manifestacaoForm.foupNrSequencia.value = lstFollowup.document.all["codigo"].value + ";";
			manifestacaoForm.idFuncCdFuncResponsavel.value = lstFollowup.document.all["responsavel"].value + ";";
			manifestacaoForm.idEvfuCdEventoFollowup.value = lstFollowup.document.all["evento"].value + ";";
			if (lstFollowup.document.all["registro"].value != "")
				manifestacaoForm.foupDhRegistro.value = lstFollowup.document.all["registro"].value + "|";
			else
				manifestacaoForm.foupDhRegistro.value = " " + "|";
			manifestacaoForm.foupDhPrevista.value = lstFollowup.document.all["previsao"].value + "|";
			if (lstFollowup.document.all["efetivo"].value != "")
				manifestacaoForm.foupDhEfetiva.value = lstFollowup.document.all["efetivo"].value + "|";
			else
				manifestacaoForm.foupDhEfetiva.value = " " + "|";
			
			var val = descodificaStringHtml(lstFollowup.document.all["historico"].value);
			//Gargamel (Chamado: 64669)
			//manifestacaoForm.foupTxHistorico.value = val + "|";
			manifestacaoForm.foupTxHistorico.value = val.replace(/\|/gim,"&#124;") + "|";
			
			manifestacaoForm.idFuncCdFuncGerador.value = lstFollowup.document.all["gerador"].value + ";";
			manifestacaoForm.inEncerramento.value = lstFollowup.document.all["encerramento"].value + ";";
			if (lstFollowup.document.all["encerramento"].value == "false")
				encerrado = false;
				
			manifestacaoForm.foupInEnvio.value = lstFollowup.document.all["foupInEnvio"].checked?"S|":"N|";
			manifestacaoForm.idFuncCdEncerramento.value = lstFollowup.document.all["idFuncCdEncerramento"].value + ";";
		} else {
		  for (var i = 0; i < lstFollowup.document.all["codigo"].length; i++) {
			manifestacaoForm.foupNrSequencia.value += lstFollowup.document.all["codigo"][i].value + ";";
			manifestacaoForm.idFuncCdFuncResponsavel.value += lstFollowup.document.all["responsavel"][i].value + ";";
			manifestacaoForm.idEvfuCdEventoFollowup.value += lstFollowup.document.all["evento"][i].value + ";";
			if (lstFollowup.document.all["registro"][i].value != "")
				manifestacaoForm.foupDhRegistro.value += lstFollowup.document.all["registro"][i].value + "|";
			else
				manifestacaoForm.foupDhRegistro.value += " " + "|";
			manifestacaoForm.foupDhPrevista.value += lstFollowup.document.all["previsao"][i].value + "|";
			if (lstFollowup.document.all["efetivo"][i].value != "")
				manifestacaoForm.foupDhEfetiva.value += lstFollowup.document.all["efetivo"][i].value + "|";
			else
				manifestacaoForm.foupDhEfetiva.value += " " + "|";
			
			var val = descodificaStringHtml(lstFollowup.document.all["historico"][i].value);
			//Gargamel (Chamado: 64669)
			//manifestacaoForm.foupTxHistorico.value += val + "|";
			manifestacaoForm.foupTxHistorico.value += val.replace(/\|/gim,"&#124;") + "|";
			
			manifestacaoForm.idFuncCdFuncGerador.value += lstFollowup.document.all["gerador"][i].value + ";";
			manifestacaoForm.inEncerramento.value += lstFollowup.document.all["encerramento"][i].value + ";";
			if (lstFollowup.document.all["encerramento"][i].value == "false")
				encerrado = false;
				
			manifestacaoForm.foupInEnvio.value += lstFollowup.document.all["foupInEnvio"][i].checked?"S|":"N|";
			manifestacaoForm.idFuncCdEncerramento.value += lstFollowup.document.all["idFuncCdEncerramento"][i].value + ";";
		  }
		}
	}
    
    //Chamado: 89941 - 01/08/2013 - Carlos Nunes
    manifestacaoForm.followupExcluidos.value = lstFollowup.document.all["followupExcluidos"].value;
  
	return encerrado;
}

function validaCamposConclusao(encerramentoFollowup) {
	
	
	//Chamado: 97587 - 3M - 04.40.17 e 04.43.03 - Marco Costa
	//N�o permite concluir com espa�os em branco ou nulo.
	
	var maniTxResposta = trim(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value);
	if(maniTxResposta.length>0){ //O campo de conclus�o tem texto.
		//Aplica o trim e verifica novamente se o campo conclus�o tem texto, se n�o tiver eram espa�os em branco, alerta o usu�rio.
		maniTxResposta = trim(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value);
		if(maniTxResposta.length<=0){
			//Apos o trim o campo ficou nulo, o conteudo eram espa�os em branco.
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value = '';
			alert('<plusoft:message key="prompt.alert.texto.conc.branco" />');
			return false;
		}
	}
	
	if (trim(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value) != "") {

		if (!encerramentoFollowup) {
			alert('<plusoft:message key="prompt.alert.concl.foup" />');
			return false;
		}
		
		//Chamado: 97587 - 3M - 04.40.17 e 04.43.03 - Marco Costa
		if (trim(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value) == "") {
			alert('<plusoft:message key="prompt.alert.texto.conc" />');
			return false;
		}
		<% /****************************
		valdeci, quando a pessoa preenchia o texto de conlusao sem preencher a data, a funcao retornava false, e nao tinha mensagem
	    dai parecia que o botao nao estava funcionando, foi colocado essa validaca para validar a data apenas se ela estiver preenchida.
	    ****************************/ %>
		 //Chamado: 95208 - 16/07/2014 - Carlos Nunes
		/* if(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value != ""){
			if (!verificaDataHora(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"])) {
				return false;
			}
		} */
	}
	return true;
}

//Chamado: 97587 - 3M - 04.40.17 e 04.43.03 - Marco Costa
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function limpaCampos() {
  manifestacaoForm.idFuncCdFuncionario.value = "";
  manifestacaoForm.idFuncCdFuncAreaResponsavel.value = "";
  manifestacaoForm.idMadsNrSequencial.value = "";
  manifestacaoForm.madsInParaCc.value = "";
  manifestacaoForm.madsInMail.value = "";
  manifestacaoForm.madsInPapel.value = "";
  manifestacaoForm.madsTxResposta.value = "";
  manifestacaoForm.madsDhResposta.value = "";
  manifestacaoForm.madsDhPrevisao.value = "";
  manifestacaoForm.madsDhPrevisaoOriginal.value = "";
  manifestacaoForm.madsIdRepaCdRespostaPadrao.value = "";
  manifestacaoForm.madsIdEtprCdEtapaProcesso.value = "";
  manifestacaoForm.madsIdFuncCdAlteraPrevisao.value = "";

  manifestacaoForm.foupNrSequencia.value = "";
  manifestacaoForm.idFuncCdFuncResponsavel.value = "";
  manifestacaoForm.idEvfuCdEventoFollowup.value = "";
  manifestacaoForm.foupDhRegistro.value = "";
  manifestacaoForm.foupDhPrevista.value = "";
  manifestacaoForm.foupDhEfetiva.value = "";
  manifestacaoForm.foupTxHistorico.value = "";
  manifestacaoForm.idFuncCdFuncGerador.value = "";
  manifestacaoForm.inEncerramento.value = "";
  manifestacaoForm.idFuncCdEncerramento.value = "";
}

function encerrarFollowup(){
	if(document.getElementById("chkEncerramento").checked && document.all.item("dataPrevista").value == ""){

		//Carlos Nunes (15/12/2006 - Altera��o para obter a data do banco ao inv�s da data javascript)
		//preencheDataHora(document.getElementById("chkEncerramento"), document.all.item("txtDtEfetiva"), document.all.item("txtHrEfetiva"));
		if (document.getElementById("chkEncerramento").checked) {
			document.all.item("txtDtEfetiva").value = cmbEvento.ifrmCalculaDhPrevista.getDataBanco();
			document.all.item("txtHrEfetiva").value = cmbEvento.ifrmCalculaDhPrevista.getHoraBanco();
		}else{
			document.all.item("txtDtEfetiva").value = "";
			document.all.item("txtHrEfetiva").value = "";
		}
		//preencheDataAtualDoBanco(document.getElementById("chkEncerramento"), document.all.item("txtDtEfetiva"), document.all.item("txtHrEfetiva"));
		
		//Carlos Nunes (15/12/2006 - Altera��o para obter a data do banco ao inv�s da data javascript)
		document.all.item("dataPrevista").value = document.all.item("txtDtEfetiva").value;
		//document.all.item("dataPrevista").value = getDataBanco();
		
		//Carlos Nunes (15/12/2006 - Altera��o para obter a hora do banco ao inv�s da data javascript)
		//document.all.item("horaPrevista").value = document.all.item("txtHrEfetiva").value;
		document.all.item("horaPrevista").value = cmbEvento.ifrmCalculaDhPrevista.getHoraBanco();
		
		document.all.item("dataPrevista").disabled = true;
		document.all.item("horaPrevista").disabled = true;
		document.all.item("imgDataFoup").disabled = true;
	}
	else if((!document.getElementById("chkEncerramento").checked) && document.all.item("dataPrevista").value == document.all.item("txtDtEfetiva").value){
	
		//Chamado: 80505 - Carlos Nunes - 19/01/2012
		if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_ALTERACAO_DATAPREVISAO%>')){
			document.all.item("dataPrevista").value = "";
			document.all.item("horaPrevista").value = "";
			document.all.item("dataPrevista").disabled = false;
			document.all.item("horaPrevista").disabled = false;
			document.all.item("imgDataFoup").disabled = false;
		}
	
		//Carlos Nunes (15/12/2006 - Altera��o para obter a data do banco ao inv�s da data javascript)
		//preencheDataHora(document.getElementById("chkEncerramento"), document.all.item("txtDtEfetiva"), document.all.item("txtHrEfetiva"));
		if (document.getElementById("chkEncerramento").checked) {
			document.all.item("txtDtEfetiva").value = cmbEvento.ifrmCalculaDhPrevista.getDataBanco();
			document.all.item("txtHrEfetiva").value = cmbEvento.ifrmCalculaDhPrevista.getHoraBanco();
		}else{
			document.all.item("txtDtEfetiva").value = "";
			document.all.item("txtHrEfetiva").value = "";
	    }
		//preencheDataAtualDoBanco(document.getElementById("chkEncerramento"), document.all.item("txtDtEfetiva"), document.all.item("txtHrEfetiva"));
	}
	else{
		//Carlos Nunes (15/12/2006 - Altera��o para obter a data do banco ao inv�s da data javascript)
		//preencheDataHora(document.getElementById("chkEncerramento"), document.all.item("txtDtEfetiva"), document.all.item("txtHrEfetiva"));
		if (document.getElementById("chkEncerramento").checked) {
			document.all.item("txtDtEfetiva").value = cmbEvento.ifrmCalculaDhPrevista.getDataBanco();
			document.all.item("txtHrEfetiva").value = cmbEvento.ifrmCalculaDhPrevista.getHoraBanco();
		}else{
			document.all.item("txtDtEfetiva").value = "";
			document.all.item("txtHrEfetiva").value = "";
	    }
		//preencheDataAtualDoBanco(document.getElementById("chkEncerramento"), document.all.item("txtDtEfetiva"), document.all.item("txtHrEfetiva"));
}
}

function setaDataResposta(){
	if(manifestacaoForm["csAstbManifestacaoDestMadsVo.madsTxResposta"].value != ""){
		/*var data = new Date();
		var dia = data.getDate();
		var mes = data.getMonth() + 1;
		var ano = data.getFullYear();
		
		if(dia < 10)
			dia = "0"+ dia;
		
		if(mes < 10)
			mes = "0"+ mes;
	
		manifestacaoForm["csAstbManifestacaoDestMadsVo.madsDhResposta"].value = dia +"/"+ mes +"/"+ ano;
		*/
		manifestacaoForm["csAstbManifestacaoDestMadsVo.madsDhResposta"].value = dataAtual;
	}
	else{
		manifestacaoForm["csAstbManifestacaoDestMadsVo.madsDhResposta"].value = "";
	}
}

function inicio(){
	
	showError("<%=request.getAttribute("msgerro")%>");
	
	var totalFormManif = 0;
	for (var i = 0; i < document.forms.length; i++){
		if (document.forms[i].name == 'manifestacaoForm'){
			totalFormManif = totalFormManif + 1;
		}
	}
	
	if (totalFormManif > 1){
	
		//Nao remover essa mensagem pois existe um problema nao identificado no struts
		//que quando tem uma tag form dentro de outra tag form o struts mantem o nome do primeiro form para os outros
		//criando dessa forma um array de forms.
		alert("<plusoft:message key='prompt.contateSuporteErroQuantidadeForms'/>");
	}
	
	
	if(manifestacaoForm.acao.value == ""){
		parent.parent.voltar();
	}
	else{
		//parent.divAguarde.style.visibility = "hidden";
	}
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
		if(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value == 3){
			
			if(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto"].value != "N"){
				//ifrmManifBotoesReclama.location = "Manifestacao.do?tela=manifBotoesReclama";
				// divBotoesReclama.style.visibility = "visible";
			}
		}
	<%}%>
	
	//valdeci, cham 67673
	if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value != "null"  && manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value != ""){
		document.all.item("chkDataConclusao").checked=true;
		//reabrir
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACA_CONCLUSAO_LIBERADO%>')){
			document.all.item("chkDataConclusao").disabled = true;
		}
	}else{
		//concluir
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_CONCLUSAO_PERMISSAO_CHAVE%>')){
			manifestacaoForm.chkDataConclusao.disabled=true;
			//Chamado: 100178 - 3M - 04.40.21 / 04.44.03 - 20/05/2015 - Marco Costa
			//manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].disabled=true;
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].readOnly=true;
		}
	}
	
  	//Para exibir o bot�o de checklist precisa:
  	//1 - O tipo da manifesta��o estar associado a um desenho de processo
  	//2 - Na etapa da �rea respons�vel, dever� ter um checklist associado
  	if(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso"].value > 0 && manifestacaoForm["csAstbManifestacaoDestMadsVo.idChliCdChecklist"].value > 0){
  		document.getElementById("divCheckList").style.display = "block";	
  	}
  	
  	setaParametros();
  	
  	try{
		onLoadManifWorkflowEspec();
	}catch(e){}


	/**
	  * Se a manifesta��o for relacionada a uma mensagem de Classificador, permite
	  * a exibi��o da Ficha
	  */
	//Chamado: 89940 - 01/08/2013 - Carlos Nunes
	if(new Number(document.manifestacaoForm["matmNrCount"].value) >= 1) {
		document.getElementById("mensagensrel").style.display = "";		
	}
}

function getIdEtapaProcesso() {
	var idEtapaProcesso = 0;
	//Verificando se a manifesta��o est� dentro de um fluxo
  	if(lstDestinatario.document.all["idFuncCdFuncionario"].length == undefined) {
		if(lstDestinatario.document.all["madsInParaCc"].checked){
			if(Number(lstDestinatario.document.all["idEtprCdEtapaProcesso"].value) > 0 && Number(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idFuncCdSaiuFluxo"].value) == 0){
				idEtapaProcesso = lstDestinatario.document.all["idEtprCdEtapaProcesso"].value;
			}
		}
  	} else{
		for (var i = 0; i < lstDestinatario.document.all["idFuncCdFuncionario"].length; i++) {
			if(lstDestinatario.document.all["madsInParaCc"][i].checked){
				if(Number(lstDestinatario.document.all["idEtprCdEtapaProcesso"][i].value) > 0 && Number(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idFuncCdSaiuFluxo"].value) == 0){
					idEtapaProcesso = lstDestinatario.document.all["idEtprCdEtapaProcesso"][i].value;
				}
			}
		}
  	}

  	return new Number(idEtapaProcesso);
}

var nCountVerificarHabilitaRespostaFluxo = 0;

function verificarHabilitaRespostaFluxo(){
	var idEtapaProcesso = 0;
	
	try{
		idEtapaProcesso = getIdEtapaProcesso(); 	

		if(idEtapaProcesso > 0){
			desabilitarDest();
		}
		// Jonathan Costa - Acerto para o Google Chrome
	  	//if(!bUltimaEtapa && idEtapaProcesso > 0 && document.all.item("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta").readOnly != true){
	  	if(!bUltimaEtapa && idEtapaProcesso > 0 && document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].readOnly != true){	  	
	  		document.getElementById("divFluxoWkf").style.display = "block";
	  		document.getElementById("divPerguntaFluxoWkf").style.display = "block";
	  		
			//Permissionamento para sair do fluxo
			if(getPermissao("<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_ENCAMINHARMANUAL_ACESSO%>")){
				document.getElementById("divSairFluxo").style.display = "block";
			}
			
			//Chamado: 95208 - 16/07/2014 - Carlos Nunes
            //Campo acrescentado para fazer o controle da resposta do destinat�io
            //caso a manifesta��o esteja encerrada o operador n�o pode dar seguimento no fluxo.
            //e a resposta deve ter a assinatura do operador
			if(document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value != ""){
				
				manifestacaoForm.chkFluxoPadrao[0].disabled = true;
				manifestacaoForm.chkFluxoPadrao[1].disabled = true;
				manifestacaoForm.chkFluxoPadrao[2].disabled = true;
			}
	  	}
	  	
		//CHAMADO: 97375 - QUALICORP - 04.30.30 - 28/01/2015 - Marco Costa
		//O desenho de processo est� sendo bloqueado pelo sistema estar apresentando a op��o de proceguir a uma etapa que n�o existe.
		var sEtprSN = '<%=request.getAttribute("etprSN")%>';
		if(sEtprSN.indexOf('S')==-1){
			manifestacaoForm.chkFluxoPadrao[0].disabled = true;
			manifestacaoForm.chkFluxoPadrao[0].title = '<bean:message key="prompt.estaetapanaopossuiestaopcao" />';
		}
		if(sEtprSN.indexOf('N')==-1){
			manifestacaoForm.chkFluxoPadrao[1].disabled = true;
			manifestacaoForm.chkFluxoPadrao[1].title = '<bean:message key="prompt.estaetapanaopossuiestaopcao" />';
		}
		//------------------------------------------------------------------------------------------------------------------
		
	  	
	  }catch(e){
	  
	  	if(nCountVerificarHabilitaRespostaFluxo< 5){
	  	
	  		setTimeout('verificarHabilitaRespostaFluxo()',300);
	  		nCountVerificarHabilitaRespostaFluxo++;
	  		
	  	}
	  
	  }
}

var nCountDesabilitarDest = 0;

function desabilitarDest(){
	try{
		cmbAreaDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = true;
		cmbDestinatarioManif.document.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].disabled = true;
		document.all.item("setaDest").disabled = true;
	}catch(e){
		if(nCountDesabilitarDest < 5){
			setTimeout('desabilitarDest()',300);
			nCountDesabilitarDest++;
		}
	}
}

function habilitarDest(){
	
	cmbAreaDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = false;
	cmbDestinatarioManif.document.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].disabled = false;
	document.all.item("setaDest").disabled = false;
	
}

/**
 * Reseta as abas removendo as abas espec�ficas que podem ter sido adicionadas
 */
function resetAbas(){
	AtivarPasta("MANIFESTACAO");
	abasTela = new Array();
	abasTela[0] = "MANIFESTACAO";
	abasTela[1] = "DESTINATARIO";
	abasTela[2] = "FOLLOWUP";
	conteudoAbas = "";
	larguraAbas = 0;
	tdBotoes.innerHTML = "";
	try{tdBotoes.width = "1";}catch(x){}
}

/**
 * Cria aba de acordo com os bot?es n?o modal cadastrados vinculados a tipo de manifesta??o
 * e o iframe para carregar o seu conte?do (link)
 */
var conteudoAbas = "";
var larguraAbas = 0;

function criarAba(descricao, link){
	abasTela[abasTela.length] = descricao;
		
	//Chamado: 83872 - 17/08/2012 - Carlos Nunes
	conteudoAbas += "<td nowrap=\"nowrap\" style=\"width:100px\" class=\"principalPstQuadroLinkNormal\" id=\"aba"+ descricao +"\" onClick=\"try{ifrmEspec"+ abasTela.length + ".funcaoAbaEspec()}catch(e){};AtivarPasta('"+ descricao +"')\">"+ acronymLst(descricao,12) +"</td>";
	larguraAbas += 100;
		
	tdBotoes.innerHTML = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+ conteudoAbas +"</table>";
	
	//Os iframes nunca s�o apagados para nao perder o conteudo preenchido
	if(document.getElementById("div"+ descricao) == null){
		divBotoes.innerHTML += "<div id=\"div"+ descricao +"\" style=\"width:99%; height:393px; display: none\">" + 
			   "<iframe name=\"ifrmEspec"+ abasTela.length +"\" id=\"ifrmEspec"+ abasTela.length +"\" src=\""+ link +"\" width=100% height=392px scrolling=\"no\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" ></iframe></div>";
			   
	}
	
	divBotoes.style.display = "block";
	tdBotoes.width = larguraAbas;
}

	/**
	 * Array que conter? todas as abas da tela para facilitar para exibir ou esconder as abas
	 */
	var abasTela = new Array();

	/**
	 * Ativa a aba clicada e exibe o seu conte?do
	 */
	function AtivarPasta(pasta) {
		for(i = 0; i < abasTela.length; i++){
			if(abasTela[i] == pasta){
				document.getElementById("div"+ abasTela[i]).style.display = "block";
				document.getElementById("aba"+ abasTela[i]).className = "principalPstQuadroLinkSelecionado";
			}
			else{
				document.getElementById("div"+ abasTela[i]).style.display = "none";
				document.getElementById("aba"+ abasTela[i]).className = "principalPstQuadroLinkNormal";
			}
		}
	}
	

function definirSexo()
{
	<%try{%>
		var sexo = '<%=((ManifestacaoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo()%>';
	<%}catch(Exception x){Log.log(this.getClass(), Log.ERROR, x.getMessage(), x);%>
		var sexo = '';
	<%}%>

	if(sexo == 'true')
	{
      document.write("MASCULINO");
    }
    else if(sexo == 'false')
	{
      document.write("FEMININO");
    }
    
    document.write("");
}
function verificaDataHora(sDate){

    cDate = sDate.value;

    newDate = new Object();

    if(cDate.length > 10){

        cDate = cDate.substring(0,10);

        newDate.value = cDate;

        return verificaData(newDate);

    }else{

        return verificaData(sDate);

    }
}

function scrollAbasMais(){
	document.getElementById("dvAbasManif").scrollLeft += 100;
}

function scrollAbasMenos(){
	document.getElementById("dvAbasManif").scrollLeft -= 100;
}

function abrePopupRespostaPadrao(){
	showModalDialog('Manifestacao.do?acao=consultar&tela=ifrmPopupRespostaPadrao&idRepaCdRespostaPadrao=' + manifestacaoForm.idRepaCdRespostaPadrao.value,window,'help:no;scroll:no;Status:NO;dialogWidth:720px;dialogHeight:530px,dialogTop:0px,dialogLeft:200px');
	//window.open('Manifestacao.do?acao=consultar&tela=ifrmPopupRespostaPadrao&idRepaCdRespostaPadrao=' + manifestacaoForm.idRepaCdRespostaPadrao.value,'xpto','help:no;scroll:auto;Status:NO;dialogWidth:570px;dialogHeight:430px,dialogTop:0px,dialogLeft:200px');
}

function adicionarRespostaPadrao(){
	if(manifestacaoForm.idRepaCdRespostaPadrao.value > 0){
		if(confirm('<plusoft:message key="prompt.confirmaInserirRespostaPadrao" />')){
			ifrmRespPadrao.location = 'Manifestacao.do?isIframe=true&acao=consultar&tela=ifrmPopupRespostaPadrao&idRepaCdRespostaPadrao=' + manifestacaoForm.idRepaCdRespostaPadrao.value;
			manifestacaoForm.respostaPadrao.value = manifestacaoForm.idRepaCdRespostaPadrao.value;
		}
	}
}

function abrirDetalhesCheckList(desabilitaCampos){
	document.forms[0].desabilitaCamposPopupChecklist.value = desabilitaCampos;

	var idMadsNrSequencial = 0;
	var idFuncCdFuncionario = 0;

	if(lstDestinatario.document.all["idFuncCdFuncionario"].length != undefined){
		for (var i = 0; i < lstDestinatario.document.all["idFuncCdFuncionario"].length; i++) {
			if(lstDestinatario.document.all["madsInParaCc"][i].checked){
				
				idFuncCdFuncionario = lstDestinatario.document.all["idFuncCdFuncionario"][i].value;
				idMadsNrSequencial = lstDestinatario.document.all["idMadsNrSequencial"][i].value;
			}
		}
	}else{
		if(lstDestinatario.document.all["madsInParaCc"].checked){
			idFuncCdFuncionario = lstDestinatario.document.all["idFuncCdFuncionario"].value;
			idMadsNrSequencial = lstDestinatario.document.all["idMadsNrSequencial"].value;
		}
	}

	showModalDialog("ManifestacaoDestinatario.do"+
					"?acao=showAll"+
					"&tela=popUpChecklist"+
					"&csAstbManifestacaoDestMadsVo.idChliCdChecklist="+ document.manifestacaoForm["csAstbManifestacaoDestMadsVo.idChliCdChecklist"].value +
					"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value +
					"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia="+ document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value +
					"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+ document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value +
					"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2="+ document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value +
					"&csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario="+  idFuncCdFuncionario + "&csAstbManifestacaoDestMadsVo.idMadsNrSequencial="+  idMadsNrSequencial, window, "help:no;scroll:no;Status:NO;dialogWidth:720px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px");
}

var ultimaSelecionada = "";

function encaminharManual(){
	
	if(!document.all["chkFluxoPadrao"][0].disabled && !document.all["chkFluxoPadrao"][1].disabled ){
	
		if(confirm('<plusoft:message key="prompt.desejaRealmenteSairFluxoManifestacaoEncaminharManual"/>')){

			ultimaSelecionada = "M";
			
			habilitarDest();
			document.all["chkFluxoPadrao"][0].disabled = true;
			document.all["chkFluxoPadrao"][1].disabled = true;
			AtivarPasta('DESTINATARIO');
		}else{

			if(ultimaSelecionada == "S"){
				document.all["chkFluxoPadrao"][0].checked = true;
				ultimaSelecionada = "S";
			}else if(ultimaSelecionada == "N"){
				document.all["chkFluxoPadrao"][1].checked = true;
				ultimaSelecionada = "N";
			}else if(ultimaSelecionada == ""){
				document.all["chkFluxoPadrao"][2].checked = false;
			}

		}
	}

}

function chamaCarta() {
	
	//Chamado: 97921 - CCEE - v.30/.40/.43 - 26/11/2014 - Marco Costa
	//Se for uma manifesta��o com origem do classificador de emails
	if(new Number(document.manifestacaoForm["matmNrCount"].value) >= 1) {
		chamaCartaClassificadorEmail();
		return;
	}
	
	if(document.getElementsByName("pomiCdCorporativo").length > 0) {
		scrm.responderPost(document.getElementsByName("pomiCdCorporativo")[0].value);
		return;
	}
	
	if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value > 0){
		var idDocumento = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbDocumentoDocuVo.idDocuCdDocumento'].value;
		var url = "";
		var url1 = "";
		var url2 = "";
		
		url += "&csNgtbCorrespondenciCorrVo.idPessCdPessoa=" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa'].value;
		url += "&csNgtbCorrespondenciCorrVo.idChamCdChamado=" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		url += "&csNgtbCorrespondenciCorrVo.maniNrSequencia=" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		url += "&csNgtbCorrespondenciCorrVo.idAsn1CdAssuntonivel1=" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		url += "&csNgtbCorrespondenciCorrVo.idAsn2CdAssuntonivel2=" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		url += "&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>";
		url += "&idFuncCdFuncionario=<%=idFuncGerador%>";

		if(idDocumento > 0){
			url1 = "../csicrm/Correspondencia.do?fcksource=true&acao=showAll&csNgtbCorrespondenciCorrVo.corrInEnviaEmail=S&tela=compose";
			
			url = url1 + url + "&csNgtbCorrespondenciCorrVo.csCdtbDocumentoDocuVo.idDocuCdDocumento=" + idDocumento;
		}else{
			url2 = "../csicrm/Correspondencia.do?fcksource=true&tela=compose&csNgtbCorrespondenciCorrVo.corrInEnviaEmail=S";
						
			url = url2 + url + "&campo=lstManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxOrientacao']";
		}
		
		//alert(url);
		window.open(url + '&origem=workflow', 'Documento','width=950,height=600,top=150,left=85');
	}
}

//Chamado: 97921 - CCEE - v.30/.40/.43 - 26/11/2014 - Marco Costa
function chamaCartaClassificadorEmail(){
	
	if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value > 0){

		if(manifestacaoForm['idMatmCdManifTemp'].value != "" && manifestacaoForm['idMatmCdManifTemp'].value != "0"){
			
			correspondenciaForm['csNgtbCorrespondenciCorrVo.idChamCdChamado'].value = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value;
			correspondenciaForm['csNgtbCorrespondenciCorrVo.maniNrSequencia'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
			correspondenciaForm['csNgtbCorrespondenciCorrVo.idAsn1CdAssuntonivel1'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
			correspondenciaForm['csNgtbCorrespondenciCorrVo.idAsn2CdAssuntonivel2'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			
			correspondenciaForm['csNgtbCorrespondenciCorrVo.idPessCdPessoa'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa'].value;
			correspondenciaForm['idEmprCdEmpresa'].value = <%=empresaVo.getIdEmprCdEmpresa()%>;
			correspondenciaForm['idMatmCdManiftemp'].value = manifestacaoForm['idMatmCdManifTemp'].value
			correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailDe'].value = '';
			correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailPara'].value = manifestacaoForm['matmDsEmail'].value;
			correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailCC'].value = manifestacaoForm['matmDsCc'].value;
			correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value = manifestacaoForm['matmDsSubject'].value;
			correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value = ConvJS2HTML(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao'].value);

			window.open('', 'Documento', 'width=950,height=600,top=50,left=50');
			correspondenciaForm.target = 'Documento';
			correspondenciaForm.submit();
			
		}

	}
	
}

function ConvJS2HTML(cSource) {
	var cRet = '';
	cRet = cSource;
	cRet = cRet.replace(/\n/gim,'<br />');
	cRet = cRet.replace(/\b/gim,'');
	cRet = cRet.replace(/\f/gim,'');
	cRet = cRet.replace(/\r/gim,'');
	cRet = cRet.replace(/\t/gim,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	return cRet;
}

//Atualiza o combo de evento passando o id da empresa
var nAtualizaCmbEvento = 0;
function atualizaCmbEvento(idEvfu,cFollowup){
	try{
		var idEmpresa = '<%=empresaVo.getIdEmprCdEmpresa()%>';
		if(parent.parent.ifrmTelaFiltro != undefined){
			idEmpresa = parent.parent.ifrmTelaFiltro.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.value;
		}
		
		manifestacaoForm.idEvfuCdEventoFollowup.value = idEvfu;
		
		document.getElementById("cmbEvento").src = "ManifestacaoFollowup.do?acao=showAll"+
			"&tela=cmbEventoFollowupManif&idEmprCdEmpresa="+ idEmpresa + 
			"&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=" + manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value +
			"&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup="+idEvfu +
			"&csNgtbFollowupFoupVo.foupNrSequencia="+cFollowup;
	}
	catch(x){
		if(nAtualizaCmbEvento < 30){
			nAtualizaCmbEvento++;
			setTimeout("atualizaCmbEvento("+idEvfu+","+cFollowup+");", 500);
		}
		else{
			alert("Erro em atualizaCmbEvento()"+ x.description);
		}
	}
}

function marcarFluxoSim(){
	ultimaSelecionada = "S";
}

function marcarFluxoNao(){
	ultimaSelecionada = "N";
}

//valdeci, estava printado o valor do campo com beanwrite mas dava erro quando tinha
//quebra de linha. alterado para pegar dos hidden e colocado nesta funcao chamado do inicio.
function setaParametros(){
	//Danilo Prevides - 10/12/2009 - 67945 - INI
	try {
		window.top.ifrmTelaWorkflow.prasDsProdutoAssunto = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1'].value;
		window.top.ifrmTelaWorkflow.maniTxManifestacao = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao'].value;
	}catch (e){}
}
//Danilo Prevides - 10/12/2009 - 67945 - FIM



function validaConclusao(sair){

	if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_TEXTOCONCLUSAO_LIVREALTERACAO%>')){
		return false;
	}

	if(document.all.item("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta").readOnly){
		return false;
	}

	if(document.all.item("txtDescricaoAntiga").value == ''){
		return false;
	}

	if(!(event.keyCode >= 37 && event.keyCode <= 40)){
		var conteudoAntigo = document.all.item("txtDescricaoAntiga").value;
		var conteudoNovo = document.all.item("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta").value;

		if(conteudoAntigo.substring(tamTextoAntigo).indexOf(assinatura) < 0 && !sair){
			document.all.item("txtDescricaoAntiga").value += "\n"+ assinatura +"\n";
			}
	
		//alert("#"+ conteudoAntigo +"#\n\n\n#"+ conteudoNovo +"#");
		conteudoNovo = conteudoAntigo + conteudoNovo.substring(conteudoAntigo.length);
	
		if(conteudoNovo.length < conteudoAntigo.length && sair)
			conteudoNovo = conteudoAntigo.substring(0, conteudoAntigo.length - assinatura.length - 4);
	
		if(document.all.item("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta").value != conteudoNovo)
			document.all.item("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta").value = conteudoNovo;
	}
}


/**
 * Exibe a Ficha do Classificador
 * Se possuir mais de 1 mensagem relacionada, exibe a lista de mensagens
 */
//Chamado: 89940 - 01/08/2013 - Carlos Nunes
function visualizarFichaClassificador(idMatm) {
	
	if(idMatm == undefined && document.manifestacaoForm["idMatmCdManifTemp"].value > 0){
		matm = document.manifestacaoForm["idMatmCdManifTemp"].value;
	}
	else
	{
		matm = idMatm
	}

	/**
	  * Se for a visualiza��o da Ficha de um Foup espec�fico j� carrega a ficha ou se s� tiver 1 mensagem relacionada
	  */ 
	if(new Number(document.manifestacaoForm["matmNrCount"].value) == 1) {
		var url = "../csicrm/ConsultarMensagemClassificador.do?matm="+matm;

		var ficha = window.open(url, "fichamatm", "width=860,height=650,left=60,top=40,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1", true);
		ficha.focus();
		return;
	} else {
		/**
		  * Se n�o definiu e tem mais que um, exibe a lista.
		  */
		var cham = document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value;
		var mani = document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value;
		var asn1 = document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value;
		var asn2 = document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
			  
		var url = "../csicrm/ListarMensagensRelacionadas.do?tela=tela&cham="+cham+"&mani="+mani+"&asn1="+asn1+"&asn2="+asn2;
		
		window.open(url, "mensRelClassif", "width=600,height=250,left=200,top=110,help=0,location=0,menubar=0,resizable=0,scrollbars=0,status=0", true);
		return;
	}
}

 var urlICostumer = '<%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_ICUSTOMER_URL,request)%>';
 
var scrm = (function() {
	return {
		responderPost : function(postid) {
			//var postwnd = window.open("http://icustomer.plusoft.com.br/socialmonitor/respostas/post/"+postid+"/single/?username=<%=SessionHelper.getUsuarioLogado(request).getFuncDsLoginname() %>&callbackurl=<%=RequestHeaderHelper.getRequestServer(request) %>/plusoft-resources/pages/scrm/responderpost.jsp", "postscrm", "width=950,height=600,top=150,left=85");
			
			var postwnd = window.open(urlICostumer+"/crm/post/"+postid+"/get-reply-content/?access_token=<%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_ICUSTOMER_TOKEN,request) %>&icusername=<%=SessionHelper.getUsuarioLogado(request).getFuncDsLoginname() %>&callbackurl=<%=RequestHeaderHelper.getRequestServer(request) %>/plusoft-resources/pages/scrm/responderpost.jsp", "postscrm", "width=950,height=600,top=150,left=85");
		}
	};
}());

//Chamado: 95425 - NESTLE 
function preencheCampoDataEncerramento(){
	if(document.getElementById('chkDataConclusao').checked){
		document.getElementById('maniDhEncerramento').value=dataAtual;
	}else{
		document.getElementById('maniDhEncerramento').value='';
	}
}

</script>

</head>

<body text="#000000" class="esquerdoBgrPageIFRM" onload="inicio();" topmargin=0 leftmargin=5 rightmargin=0 bottommargin=0 scroll="no">
<html:form action="/WorkFlow.do" styleId="manifestacaoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
   <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" />  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto" />  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif" />  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif" /> 
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.tipPublicoVo.idTppuCdTipopublico" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso" />

	<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" />
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.seguirFluxo" />
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.sucessoEtapa" />
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idFuncCdSaiuFluxo" />
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idOporCdOportunidade" />
	

  <!-- ifrm Destinatario -->
  <html:hidden property="idFuncCdFuncionario" />
  <html:hidden property="idFuncCdFuncAreaResponsavel" />
  <html:hidden property="idMadsNrSequencial" />
  <html:hidden property="madsInParaCc" />
  <html:hidden property="madsInMail" />
  <html:hidden property="madsInPapel" />
  <html:hidden property="madsTxResposta" />
  <html:hidden property="madsDhResposta" />
  <html:hidden property="madsDhPrevisao" />
  <html:hidden property="madsDhPrevisaoOriginal" />
  <html:hidden property="madsDhEnvio" />
  <html:hidden property="madsIdRepaCdRespostaPadrao" />
  <html:hidden property="madsIdEtprCdEtapaProcesso" />
  <html:hidden property="madsIdFuncCdAlteraPrevisao" />
  <!-- ifrm Followup -->
  <html:hidden property="foupNrSequencia" />
  <html:hidden property="idFuncCdFuncResponsavel" />
  <html:hidden property="idEvfuCdEventoFollowup" />
  <html:hidden property="foupDhRegistro" />
  <html:hidden property="foupDhPrevista" />
  <html:hidden property="foupDhEfetiva" />
  <html:hidden property="foupTxHistorico" />
  <html:hidden property="idFuncCdFuncGerador" />
  <html:hidden property="inEncerramento" />
  <html:hidden property="idFuncCdConclusao" />
  <html:hidden property="inCarregouConcluida" />
  <html:hidden property="inCarregouPendenteConclusao" />
  <html:hidden property="foupInEnvio" />
  <html:hidden property="idFuncCdEncerramento" />
  
  <html:hidden property="madsInModulo"/>
  
  <logic:present name="manifestacaoForm" property="csNgtbPostmidiaPomiVo">
  	<input type="hidden" name="pomiCdCorporativo" 
  	       value="<bean:write name="manifestacaoForm" property="csNgtbPostmidiaPomiVo.field(pomi_cd_corporativo)"/>" />
  </logic:present>

  <input type="hidden" name="respostaPadrao" />
  <input type="hidden" name="inEnvioFollowup" />
  <input type="hidden" name="dhEnvio" value="">
  <input type="hidden" name="desabilitaCamposPopupChecklist" />
   
  <input type="hidden" name="txtDescricaoAntiga" id="txtDescricaoAntiga" value="<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"/>"/>
   
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa" />
  <html:hidden property="csAstbManifestacaoDestMadsVo.idChliCdChecklist" />

  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbTipoPublicoTppuVo.idTppuCdTipoPublico" />
  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbDocumentoDocuVo.idDocuCdDocumento"/>

  <!-- Chamado: 89940 - 01/08/2013 - Carlos Nunes -->
  <html:hidden property="idMatmCdManifTemp"/>
  <html:hidden property="matmNrCount"/>
  <html:hidden property="matmDsEmail" />
  <html:hidden property="matmDsSubject" />
  <html:hidden property="matmDsCc" />
  
  <input type="hidden" name="matm" value="0">
  
  <html:hidden property="followupExcluidos" />
  
   <html:hidden property="csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"/>
  
  		<!--DIV DESTINADO PARA A INCLUSAO DOS CAMPOS ESPECIFICOS -->
  		<div id="camposDetalheManifEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden">

  		</div>
  
	<!--DIV DESTINADO PARA A INCLUSAO DE ABAS -->
	<div id="camposManifEspec" name="camposManifEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden">
	</div>  		

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.workflow" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="585" align="center"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <!--<tr align="right"> 
                  <td class="principalLabel">&nbsp; </td>
                </tr>-->
                <tr> 
                  <td height="0" class="principalLabel">
                  <!-- Chamado 106250 - 14/01/2016 Victor Godinho -->
                  <div id="cabecalho" style="position:relative; width:790px; height:110px; z-index:0; overflow: no;visibility: hidden;display: none;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="15%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.cliente" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="20%" class="principalLabelValorVariavel" height="23">
						  &nbsp;<plusoft:acronym name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmPessoa" length="20" />
						</td>
                        <td width="16%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.numeroatendimento" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="16%" class="principalLabelValorVariavel">
						  &nbsp;<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
                        </td>
                        <td width="16%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.tipopublico" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="17%" class="principalLabelValorVariavel">
                          &nbsp;<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.tipPublicoVo.tppuDsTipopublico" />
                        </td>
                      </tr>
                      <tr> 
                        <td width="15%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.cidade" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="20%" class="principalLabelValorVariavel" height="23">
						  &nbsp;<plusoft:acronym name="manifestacaoForm" property="csCdtbPessoaendPeenVo.peenDsMunicipio" length="20" />
                        </td>
                        <td width="16%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.email" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="16%" class="principalLabelValorVariavel">
						  &nbsp;<span id="spanEmail"><plusoft:acronym name="manifestacaoForm" property="csCdtbPessoacomunicEmailVo.pcomDsComplemento" length="20" /></span>
                        </td>
                        <td width="16%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.fone" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                        </td>
                        <td width="17%" class="principalLabelValorVariavel">
						  &nbsp;(<bean:write name="manifestacaoForm" property="csCdtbPessoacomunicPcomVo.pcomDsDdi" />)&nbsp;<bean:write name="manifestacaoForm" property="csCdtbPessoacomunicPcomVo.pcomDsDdd" />&nbsp;<plusoft:acronym name="manifestacaoForm" property="csCdtbPessoacomunicPcomVo.pcomDsComunicacao" length="9" />
                        </td>
                      </tr>
                      <tr> 
                        <td width="15%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.tipopessoa" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="20%" class="principalLabelValorVariavel" height="23">
						  &nbsp;<script>
						  			if( '<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessInPfj" />' != '' ) {
						  				document.write('<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessInPfj" />' == 'F'?"F�SICA":"JUR�DICA");
						  			}					  			
						  	   </script>
                        </td>
                        <td width="16%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.sexo" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="16%" class="principalLabelValorVariavel">
                          &nbsp;<script>definirSexo();</script>
                        </td>
                        <td width="16%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.dtnascimento" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                        </td>
                        <td width="17%" class="principalLabelValorVariavel">
						  &nbsp;<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.dataNascimento" />
                        </td>
                      </tr>
                      <tr> 
                        <td width="15%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.estadoanimo" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="20%" class="principalLabelValorVariavel" height="23">
						  &nbsp;<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanVo.esanDsEstadoAnimo" />
                        </td>
                        <td width="16%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.formaretorno" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="16%" class="principalLabelValorVariavel">
						  &nbsp;<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.tpreDsTipoRetorno" />
                        </td>
                        <td width="16%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.hrretorno" />
                           <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                        </td>
                        <td width="17%" class="principalLabelValorVariavel">
						  &nbsp;<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.chamDsHoraPrefRetorno" />
                        </td>
                      </tr>
                      
                      <tr> 
                        <td width="15%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.contato" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="20%" class="principalLabelValorVariavel" height="23">
						  &nbsp;<plusoft:acronym name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.pessNmPessoa" length="20" />
                        </td>
                        <td width="16%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.atendente" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="16%" class="principalLabelValorVariavel">
						  &nbsp;<plusoft:acronym name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbFuncionarioFuncVo.funcNmFuncionario" length="15"/>
                        </td>
                        <td width="16%" class="principalLabelValorFixo" align="right"><plusoft:message key="prompt.empresa" />
                            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="17%" class="principalLabelValorVariavel">
						  &nbsp;<plusoft:acronym name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.emprDsEmpresa" length="17" />
                        </td>
                      </tr>
                    </table>
                    
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td style="height:10px"> 
                    &nbsp;
                  </td>
                </tr>
                <tr> 
                  <td> 
					
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr> 
							<td>
								<div class="principalPstQuadroLinkVazio" id="dvAbasManif" style="float: left; overflow: hidden; width: 760px; margin: 0;"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr> 
	                  						<td width="300">
	                    						<table border="0" cellspacing="0" cellpadding="0">
	                      							<tr> 
	                        							<td nowrap="nowrap" style="width: 100px" class="principalPstQuadroLinkSelecionado" id="abaMANIFESTACAO" onClick="AtivarPasta('MANIFESTACAO')">
	                          								<bean:message key="prompt.manifestacaofixo" />
	                        							</td>
	                        							<td nowrap="nowrap" style="width: 100px" class="principalPstQuadroLinkNormal" id="abaDESTINATARIO" onClick="AtivarPasta('DESTINATARIO')"> 
	                          								<bean:message key="prompt.destinatario" />
	                        							</td>
														<td nowrap="nowrap" style="width: 100px" class="principalPstQuadroLinkNormal" id="abaFOLLOWUP" onClick="AtivarPasta('FOLLOWUP')"> 
	                          								<bean:message key="prompt.followup" />
	                        							</td>                        							
	                        						</tr>
	                        					</table>
	                        				</td>
	                        				
	                        				<!--Neste TD ser�o carregadas as abas de acordo com os bot�es cadastrados relacionados com o tipo de manifesta��o-->
	                        				<td id="tdBotoes" align="left" width="0" style="width: 100%;"></td>	                        				
	                        				
	                  						<!-- Danilo Prevides - 24/11/2009 - AC:11891 - Tamanho do campo ampliado para 400-->
	                  						<td width="400"></td>
										</tr>
									</table>
								</div>
								<div class="principalPstQuadroLinkVazio" style="float: left; margin: 0;"><img src="webFiles/images/botoes/esq.gif" style="cursor: pointer;" onclick="scrollAbasMenos();"><img src="webFiles/images/botoes/dir.gif" style="cursor: pointer;" onclick="scrollAbasMais();"></div>
							</td>
						</tr>
						<tr>
							<td valign="top" class="principalBgrQuadro">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">

                            <tr> 
                              <td valign="top" class="principalBgrQuadro" height="410"> 
                                &nbsp; 
                                <div id="divMANIFESTACAO" style="width:99%; height:393px; display: block">
									<table width="100%" border=0 cellpadding=0 cellspacing=0>
										<tr>
											<td width="10"></td>
											<td>
											
				                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                                    
				                                    <tr> 
				                                      	<td width="36%" class="principalLabel"><%= getMessage("prompt.manifestacao", request)%></td>
				                                      	<td width="36%" class="principalLabel"><%= getMessage("prompt.grupomanif", request)%></td>
				                                      	<td width="36%" class="principalLabel"><%= getMessage("prompt.tipomanif", request)%></td>
				                                    </tr>
				                                    <tr> 
				                                      	<td width="36%"> 
				                                        	<html:text style="width:250px;" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.matpDsManifTipo" styleClass="principalObjForm" disabled="true" />
				                                      	</td>
				                                      	<td width="36%"> 
				                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				                                          		<tr> 
				                                            		<td width="90%"> 
				                                              			<html:text property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.grmaDsGrupoManifestacao" styleClass="principalObjForm" style="width: 235px;" disabled="true" />
				                                            		</td>
				                                            		<td width="10%">&nbsp;</td>
				                                          		</tr>
				                                        	</table>
				                                      	</td>
				                                      	<td width="36%"> 
				                                        	<html:text property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao" styleClass="principalObjForm" style="width: 235px;" disabled="true" />
				                                      	</td>
				                                    </tr>
				                                    
				                                    <tr> 
				                                      	<td width="36%" class="principalLabel"><plusoft:message key="prompt.numerochamado" /></td>
				                                      	<td width="36%" class="principalLabel"><plusoft:message key="prompt.prazoconclusao" /></td>
				                                      	<td width="36%" class="principalLabel"><plusoft:message key="prompt.codigoProduto" /></td>
				                                    </tr>
				                                    <tr> 
				                                      	<td width="36%"> 
				                                        	<input style="width:250px;" type="text" name="chamado" value="<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />" style="width: 247px;" class="principalObjForm" disabled="true">
				                                      	</td>
				                                      	<td width="36%"> 
				                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				                                          		<tr> 
				                                            		<td width="90%"> 
				                                              			<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao" styleClass="principalObjForm" style="width: 235px;" disabled="true" />
				                                            		</td>
				                                            		<td width="10%">&nbsp;</td>
			                                          			</tr>
			                                        		</table>
			                                      		</td>
			                                      		<td width="36%"> 
					                                       <html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd" styleClass="principalObjForm" style="width: 235px;" disabled="true" />
			                                      		</td>
			                                    	</tr>

				                                    <tr> 
				                                      	<td width="36%" class="principalLabel"><plusoft:message key="prompt.linha" /></td>
				                                      	<td width="36%" class="principalLabel"><plusoft:message key="prompt.assuntoNivel1" /></td>
				                                      	<td width="36%" class="principalLabel"><plusoft:message key="prompt.assuntoNivel2" /></td>
				                                    </tr>
				                                    <tr> 
				                                      	<td width="36%"> 
				                                        	<input style="width:250px;" type="text" name="chamado" value="<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.linhDsLinha' />" class="principalObjForm" style="width: 247px;" disabled="true">
				                                      	</td>
				                                      	<td width="36%"> 
				                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				                                          		<tr> 
				                                            		<td width="90%"> 
				                                              			<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1" styleClass="principalObjForm" style="width: 235px;" disabled="true" />
				                                            		</td>
				                                            		<td width="10%">&nbsp;</td>
			                                          			</tr>
			                                        		</table>
			                                      		</td>
			                                      		<td width="36%"> 
					                                       <html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2" styleClass="principalObjForm" style="width: 235px;" disabled="true" />
			                                      		</td>
			                                    	</tr>
			                                    	
			                                    	
			                                    <tr> 
			                                      <td colspan="3" class="principalEspacoPqn">&nbsp;</td>
			                                    </tr>
			                                    <tr id="tr_descricao_label">
												  <td valign="top"><div id="div_descricao_mani" style="position:absolute; width:767; height:80px; z-index:1; background-color: #F4F4F4; layer-background-color: #F4F4F4; visibility: hidden;" >&nbsp;</div></td>			                                    
			                                    </tr>
			                                    <tr id="tr_descricao_campo"> 
			                                      <td colspan="2" class="principalLabel"><plusoft:message key="prompt.descricaomanifestacao" /></td>
			                                      <td align="right" class="principalLabel">
							                		<div id="divCheckList" style="float:right; display: none; margin-right: 30px;">
								                		<bean:message key="prompt.checkList"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
								                		<img src="webFiles/images/botoes/CheckList4.gif" align="middle" style="cursor: pointer;" onclick="abrirDetalhesCheckList(false);"/>
								                	</div>
			                                      </td>
			                                    </tr>
			                                    <tr> 
			                                      <td colspan="3"> 
												    <table width="100%" cellspacing="0" cellpadding="0">
			                                          <tr>
			                                            <td width="98%">
										                  <html:textarea property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao" styleClass="principalObjForm" style="width: 730px;" rows="4" readonly="true" />
										                </td>
										                <td width="2%" valign="middle"> <!-- Chamado: 89940 - 01/08/2013 - Carlos Nunes -->
										                  <div id="mensagensrel" style="margin-top; top: 3px; left: 350px; cursor: pointer; display: none; " onclick="visualizarFichaClassificador(); ">
															<img src="/plusoft-resources/images/email/mail-message.gif" style="vertical-align: middle;" title="<bean:message key='prompt.mensagemrelacionada'/>"/>
														</div>
										                  <img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="setarObjBinoculo(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao']);showModalDialog('webFiles/workflow/ifrmTxManifFoup.jsp',window,'help:no;scroll:no;status:no;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')" title="<bean:message key="prompt.visualizar"/>"/>
										                </td>
										              </tr>
			                                        </table>
			                                      </td>
			                                    </tr>
			                                    <tr>
				                                    <td colspan="3">
					                                    <table width="100%" cellspacing="0" cellpadding="0" >
				                                          <tr>
				                                          	<td width="14%" class="principalLabel"><bean:message key="prompt.dataresposta" /></td>
															<td width="46%">
											                  &nbsp;
											                </td>
				                                            <td width="30%"  class="principalLabel">
				                                            <% 
				                                            	if(request.getAttribute("etapaAtualVo") != null){
				                                            %>
											                  <div class="principalLabel" id="divRespPadrao" style="position: absolute;">
											                     <table width="100%" cellspacing="0" cellpadding="0" >
											                     	<tr>
											                     		<td colspan="3" class="principalLabel">Resposta Padr�o</td>
											                     	</tr>
											                     	<tr>
											                     		<td class="principalLabel">
											                     			<html:select property="idRepaCdRespostaPadrao" styleClass="principalObjForm" >
																			<html:option value=''><bean:message key="prompt.combo.sel.opcao"/></html:option>
																			<logic:present name="csCdtbRespostaPadraoRepaVector">
																				<html:options collection="csCdtbRespostaPadraoRepaVector" property="field(id_repa_cd_respostapadrao)" labelProperty="field(repa_ds_respostapadrao)"/>
																			</logic:present>
																	 	 </html:select>    
																		</td>
																		<td width="3%"><img name="imgLupaRepa" id="imgLupaRepa" src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" onclick="abrePopupRespostaPadrao()" title="<bean:message key='prompt.visualizar'/>"></td>
																		<td width="3%"><img name="imgSetaRepa" id="imgSetaRepa" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarRespostaPadrao()" title="<bean:message key="prompt.inserir" />"></td>
											                     	</tr>
											                     </table>
											                 <%
											                 	}
				                                             %>
											                </td>
											                <td width="10%">
											                  &nbsp;
											                </td>
											              </tr>
				                                        </table>
				                                    </td>
				                                </tr>
			                                    <tr> 
			                                      <td colspan="3">
			                                      	<div id="div_resposta_padrao" name="div_resposta_padrao" style="visibility:visible"> 
			                                      	<table width="100%" cellspacing="0" cellpadding="0">
			                                          <tr>
			                                          	<td width="14%"> 
						                                      <html:text property="csAstbManifestacaoDestMadsVo.madsDhResposta" styleClass="principalObjForm" disabled="true" style="height: 18; width:105" />
						                                </td>
														<td width="46%">&nbsp;</td>
			                                            <td width="30%">&nbsp;</td>
										                <td width="3%">&nbsp;</td>
										                <td width="3%">&nbsp;</td>
										                <td width="4%">
										                  <iframe id="ifrmRespPadrao" name="ifrmRespPadrao" src="" width="0" height="0" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
										                </td>
										              </tr>
			                                        </table>
			                                        </div>
			                                      </td>
			                                    </tr>
			                                    <tr>
												  <td valign="top"><div id="div_resposta_mani" style="position:absolute; width:767; height:100px; z-index:1; background-color: #F4F4F4; layer-background-color: #F4F4F4; visibility: hidden;" >&nbsp;</div></td>			                                    
			                                    </tr>	
			                                    <tr id="tr_resposta_label"> 
			                                      <td colspan="3" class="principalLabel"><plusoft:message key="prompt.resposta" /></td>
			                                    </tr>
			                                    <tr id="tr_resposta_campo"> 
			                                      <td colspan="3"> 
												    <html:textarea property="csAstbManifestacaoDestMadsVo.madsTxResposta" styleClass="principalObjForm" rows="4" style="width: 730px;" onkeyup="textCounter(this, 4000);setaDataResposta();" onblur="textCounter(this, 4000)" />
												    <script>
			                                          if( trim(manifestacaoForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value) != ""){
															var texto = manifestacaoForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value;
															while(texto.indexOf("QBRLNH")> -1){
																texto = texto.replace('QBRLNH','\n');
															}
															while(texto.indexOf("&quot")> -1){
																texto = texto.replace('&quot','\"');
															}
															manifestacaoForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value = texto;
			                                          		document.getElementsByName("csAstbManifestacaoDestMadsVo.madsTxResposta")[0].readOnly = true;
			                                          		pendenteConclusao = true;
			                                          }
			                                        </script>
			                                      </td>
			                                    </tr>
			                                    
			                                    <tr>
			                                    	<td colspan="3">
			                                    		<table width="100%" cellspacing="0" cellpadding="0">
			                                    			<tr>
			                                      				<td width="35%" class="principalLabel"><plusoft:message key="prompt.manifstatus" /></td>
			                                      				<td width="65%" class="principalLabel">
			                                      					<div id="divPerguntaFluxoWkf" style="float:left; display: none;">
													                  	&nbsp;&nbsp;
													                  	<%
											                				if(request.getAttribute("etapaAtualVo") != null){
											                					if(!"S".equals(((Vo)request.getAttribute("etapaAtualVo")).getFieldAsString("etpr_in_ultimaetapa"))){
											                						out.print(acronymChar(((Vo)request.getAttribute("etapaAtualVo")).getFieldAsString("etpr_ds_perguntafluxo"), 65));
											                					}
											                					else{ %>
										                							<script>bUltimaEtapa = true;</script>
										                        			<%	}
											                				}
											                			%>
														      			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
														      		</div>
																</td>
			                                      			</tr>
			                                      		</table>
			                                      	</td>
			                                    </tr>
			                                    <tr>
			                                    	<td colspan="3">
			                                    		<table width="100%" cellspacing="0" cellpadding="0">
			                                    			<tr>			                              
						                                      	<td align="center" width="35%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
						                                      		<iframe id="cmbStatusManif" name="cmbStatusManif" src="" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
						                                      		<script>cmbStatusManif.location = "Manifestacao.do?tela=cmbStatusManif&acao=<%=MCConstantes.ACAO_SHOW_ALL%>";</script>
						                                      	</td>
						                                      	<td align="center" width="1%" height="23"></td>
						                                      	<td width="64%">
											                		<div class="principalLabel" id="divFluxoWkf" style="display: none; float: left;">
												                		<input type="radio" onclick="marcarFluxoSim()" name="chkFluxoPadrao"/><bean:message key="prompt.sim"/>
												                		&nbsp;&nbsp;<input type="radio" onclick="marcarFluxoNao()" name="chkFluxoPadrao"/><bean:message key="prompt.nao"/>
												                	</div>
												                	<div class="principalLabel" id="divSairFluxo" style="display: none; float: left;">
												                		&nbsp;&nbsp;<input type="radio" name="chkFluxoPadrao" onclick="encaminharManual()"/><bean:message key="prompt.encaminharManual"/>
												                	</div>
						                                      	</td>
			                                    			</tr>
			                                    		</table>
			                                    	</td>
			                                    </tr>
			                                    <tr> 
			                                      <td width="36%">&nbsp;</td>
			                                      <td width="25%">&nbsp;</td>
			                                      <td width="7%" align="center">&nbsp;</td>
			                                    </tr>
			                                  </table>
										</td></tr>
										</table>
	                                </div>
	
	                           <div id="divDESTINATARIO" style="width:99%; height:393px; display: none"> 
	                           		<table width="100%" border=0 cellpadding=0 cellspacing=0>
										<tr>
											<td width="10"></td>
											<td>
	                           
				                                 <html:form action="/ManifestacaoDestinatario.do" styleId="manifestacaoDestinatarioForm">
				                                 
				                                 	<html:hidden property="csAstbManifestacaoDestMadsVo.idMadsNrSequencial" />
				                                 
				                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                                    <tr> 
				                                      <td width="35%" class="principalLabel"><bean:message key="prompt.areas" /></td>
				                                      <td width="48%" class="principalLabel"><bean:message key="prompt.responsavel" /> / <bean:message key="prompt.cargo" /></td>
				                                      <td width="17%" class="principalLabel">&nbsp;</td>
				                                    </tr>
				                                    <tr> 
				                                      <td width="35%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
													    <iframe name="cmbAreaDestinatarioManif" src="" width="100%" height="20px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
													    <script>cmbAreaDestinatarioManif.location = "ManifestacaoDestinatario.do?acao=showAll&tela=cmbAreaDestinatarioManif";</script>
				                                      </td>
				                                      <td width="48%">
												        <iframe name="cmbDestinatarioManif" src="" width="100%" height="20px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
												        <script>
												        	cmbDestinatarioManif.location = "ManifestacaoDestinatario.do?tela=cmbDestinatarioManif";</script>
				                                      </td>
				                                      <td width="17%">
				                                        <img name="setaDest" id="setaDest" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarFunc()" title="<bean:message key="prompt.inserir" />">
				                                      </td>
				                                    </tr>
				                                    <tr> 
				                                      <td colspan="3">
				                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                                          <!--tr> 
				                                            <td width="2%" class="principalLstCab" align="center"><img src="../images/botoes/lixeira.gif" width="14" height="14"></td>
				                                            <td width="46%" class="principalLstCab">Destinat&aacute;rios</td>
				                                            <td width="6%" class="principalLstCab"><img src="../images/icones/responsavel.gif" width="30" height="30" title="Respons�vel"></td>
				                                            <td width="6%" class="principalLstCab"><img src="../images/icones/email.gif" width="37" height="25" title="E-mail"></td>
				                                            <td width="11%" class="principalLstCab"><img src="../images/icones/impress.gif" width="30" height="28" title="Imprimir"></td>
				                                            <td width="14%" class="principalLstCab"><img src="../images/icones/carta.gif" width="35" height="20" title="Carta"></td>
				                                            <td width="15%" class="principalLstCab"><img src="../images/icones/resposta.gif" width="30" height="29" title="Resposta"></td>
				                                          </tr-->
				                                          <tr valign="top"> 
				                                            <td colspan="7" height="150px">
														      <!--Inicio Ifrme Lst Destinatario-->
														      <iframe name="lstDestinatario" src='ManifestacaoDestinatario.do?workflow=followup&acao=showAll&tela=lstDestinatario&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />' width="100%" height="150px" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
														      <!--Final Ifrme Lst Destinatario-->
				                                            </td>
				                                          </tr>
				                                        </table>
				                                      </td>
				                                    </tr>
				                                    <tr> 
				                                      <td colspan="3" class="principalLabel"><bean:message key="prompt.conclusaofinalprocesso" /></td>
				                                    </tr>
				                                    <tr> 
				                                      <td colspan="3">
				                                      	<table class="principalLabel" width="100%" cellpadding="0" cellspacing="0" border="0">
														<tr> 
															<td width="95%">
																<html:textarea name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta" styleClass="principalObjForm" rows="8" onkeyup="textCounter(this, 4000);validaConclusao(false);" onkeydown="validaConclusao(false);" onblur="textCounter(this, 4000);validaConclusao(false);" />						                                        
												    		</td>    
												    		<td width="5%" valign="baseline">
												    			<img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="setarObjBinoculo(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta']);showModalDialog('webFiles/workflow/ifrmTxManifFoup.jsp',window,'help:no;scroll:no;status:no;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')" title="<bean:message key="prompt.visualizar"/>">
												    		</td>
												    	</tr>
												    	</table>
				                                      </td>
				                                    </tr>
				                                    <tr> 
				                                      <td colspan="3"> 
				                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                                          <tr> 
				                                            <td width="28%" class="principalLabel"><plusoft:message key="prompt.formaretorno" /></td>
				                                            <td width="28%" class="principalLabel"><plusoft:message key="prompt.resultadoManif" /></td>
				                                            <td width="28%" class="principalLabel"><plusoft:message key="prompt.grausatisfacao" /></td>
				                                            <td width="10%" class="principalLabel"><plusoft:message key="prompt.dataconclusao" /></td>
				                                            <td width="3%">&nbsp;</td>
				                                            <td width="5%">&nbsp;</td>
				                                          </tr>
				                                          <tr> 
				                                            <td width="28%" height="23">
														  <html:select name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.idTpreCdTipoRetorno" styleClass="principalObjForm" disabled="true">
															<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
															<logic:present name="csCdtbTipoRetornoTpreVector">
															  <html:options collection="csCdtbTipoRetornoTpreVector" property="idTpreCdTipoRetorno" labelProperty="tpreDsTipoRetorno"/>
															</logic:present>
														  </html:select>
			                                            </td>
			                                            <td width="28%">

															<html:select name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idComaCdConclusaoManif" styleClass="principalObjForm" >
																<html:option value=''><bean:message key="prompt.combo.sel.opcao"/></html:option>
																<logic:present name="csCdtbConclusaoManifComaVector">
																	<html:options collection="csCdtbConclusaoManifComaVector" property="field(id_coma_cd_conclusaomanif)" labelProperty="field(coma_ds_conclusaomanif)"/>
																</logic:present>
														 	 </html:select>
														 	 
														</td>
														<td width="28%">

															<html:select name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao" styleClass="principalObjForm" >
																<html:option value=''><bean:message key="prompt.combo.sel.opcao"/></html:option>
																<logic:present name="csCdtbGrausatisfacaoGrsaVector">
																	<html:options collection="csCdtbGrausatisfacaoGrsaVector" property="idGrsaCdGrauSatisfacao" labelProperty="grsaDsGrauSatisfacao"/>
																</logic:present>
														 	 </html:select>
														 	 
														</td>
			                                            <td width="10%"> 
			                                              <html:text name="manifestacaoForm" styleId="maniDhEncerramento" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento" styleClass="principalObjForm" readonly="true" />
			                                            </td>
			                                            <td width="3%" align="center">
			                                              <input type="checkbox" id="chkDataConclusao" name="chkDataConclusao" value="true" onclick="preencheData(this, manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento']);">
													      <script>
													      	//movido para o onload
				                                            //if ('<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento" />' != 'null' && '<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento" />' != '')
				                                              //document.all.item("chkDataConclusao").disabled = true;
				                                          </script>
				                                          
				                                          	<script>
																	//movido para o onload
																	//if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_CONCLUSAO_PERMISSAO_CHAVE%>')){
																	//	manifestacaoForm.chkDataConclusao.disabled=true;
																	//	manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].disabled=true;
																	//}

														</script>
		                                            </td>
		                                            <td width="5%"> 
			                                              &nbsp;
		                                            </td>
		                                          </tr>
		                                        </table>
		                                      </td>
		                                    </tr>
		                                  </table>
		                                 </html:form>
		                              </td></tr>
		                           </table>
                                </div>

                                <div id="divFOLLOWUP" style="width:99%; height:393px; display: none"> 
									<table width="100%" border=0 cellpadding=0 cellspacing=0>
										<tr>
											<td width="10"></td>
											<td>

			                                 <html:form action="/ManifestacaoFollowup.do" styleId="manifestacaoFollowupForm">
												<input type="hidden" name="registro">
												<input type="hidden" name="efetivo">
												<input type="hidden" name="codigo" value="0">
												<input type="hidden" name="nomeGerador" value="">
												<!--input type="hidden" name="codigoGerador" value="0"-->
												<input type="hidden" name="codigoGerador" value="<%=idFuncGerador%>">
												<input type="hidden" name="altLinha" value="0">
			
			                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                    <tr> 
			                                      <td class="principalLabel" width="38%"><plusoft:message key="prompt.eventos" /></td>
			                                      <td class="principalLabel" width="34%"><plusoft:message key="prompt.responsavel" /></td>
			                                      <td rowspan="8" align="center" width="28%"> 
			                                        <table width="90%" border="0" cellspacing="0" cellpadding="0" height="186">
			                                          <tr> 
			                                            <td class="principalLabel" height="2"><bean:message key="prompt.prazoresolucao" /></td>
			                                          </tr>
			                                          <tr> 
			                                            <td class="principalBordaQuadro" align="center" height="180"> 
			                                              <table width="97%" border="0" cellspacing="0" cellpadding="0" height="154">
			                                                <tr> 
			                                                  <td colspan="2" class="principalLabel"> 
			                                                    <input type="checkbox" id="chkEncerramento" name="encerramento" value="true" onclick="encerrarFollowup();//preencheDataAtualDoBanco(this, document.all.item('txtDtEfetiva'), document.all.item('txtHrEfetiva'))">
			                                                    <bean:message key="prompt.encerrafollowup" /></td>
			                                                </tr>
			                                                <tr> 
			                                                  <td colspan="2" class="principalLabel"><bean:message key="prompt.dataprevista" /></td>
			                                                </tr>
			                                                <tr> 
			                                                  <td width="91%"> 
			                                                    <input type="text" name="dataPrevista" class="principalObjForm" onkeydown="return validaDigito(this, event);" onblur="verificaData(this);" maxlength="10">
			                                                  </td>
			                                                  <td width="9%" align="center"><img  id="imgDataFoup" src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="show_calendar('manifestacaoFollowupForm.dataPrevista');" title="<bean:message key="prompt.calendario" />"></td>
			                                                </tr>
			                                                <tr> 
			                                                  <td colspan="2" class="principalLabel"><bean:message key="prompt.horaprevista" /> </td>
			                                                </tr>
			                                                <tr> 
			                                                  <td width="91%" class="principalLabel"> 
			                                                    <input type="text" name="horaPrevista" class="principalObjForm" onkeydown="return validaDigitoHora(this, event);" maxlength="5">
			                                                  </td>
			                                                  <td align="center" class="principalLabel" width="9%">&nbsp;</td>
			                                                </tr>
			                                                <tr> 
			                                                  <td colspan="2" class="principalLabel"><bean:message key="prompt.dataefetiva" /></td>
			                                                </tr>
			                                                <tr> 
			                                                  <td width="91%"> 
			                                                    <input type="text" name="txtDtEfetiva" class="principalObjForm" disabled="true">
			                                                  </td>
			                                                  <td width="9%" align="center">&nbsp;</td>
			                                                </tr>
			                                                <tr> 
			                                                  <td colspan="2" class="principalLabel"><bean:message key="prompt.horaefetiva" /></td>
			                                                </tr>
			                                                <tr> 
			                                                  <td width="91%" height="2"> 
			                                                    <input type="text" name="txtHrEfetiva" class="principalObjForm" disabled="true">
			                                                  </td>
			                                                  <td width="9%" align="center" height="2">&nbsp;</td>
			                                                </tr>
			                                              </table>
			                                            </td>
			                                          </tr>
			                                          <tr> 
			                                            <td height="41" align="left">&nbsp;<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="addFollowup()" title="<bean:message key="prompt.inserir" />"></td>
			                                          </tr>
			                                        </table>
			                                      </td>
			                                    </tr>
			                                    <tr> 
			                                      <td width="38%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
										            <!--Inicio Ifram FollowUp Eventos-->
										            <iframe name="cmbEvento" id="cmbEvento"  src="" width="100%" height="23px" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
										            <script>atualizaCmbEvento(0); //cham 67348</script>
										            <!--Final Ifram FollowUp Eventos-->
			                                      </td>
			                                      <td valign="middle" width="34%" height="23px"><!--Jonathan | Adequa��o para o IE 10-->
										            <!--Inicio Ifram FollowUp Responsavel-->
										            <iframe name="cmbResponsavel" src="ManifestacaoFollowup.do?tela=cmbFuncFollowupManif" width="100%" height="23px" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
										            <!--Final Ifram FollowUp Responsavel-->
			                                      </td>
			                                    </tr>
			                                    <tr> 
			                                      <td width="38%" class="principalLabel"><bean:message key="prompt.area" /></td>
			                                    </tr>
			                                    <tr> 
			                                      <td width="38%" class="principalLabel" height="23px" valign="top"> <!--Jonathan | Adequa��o para o IE 10-->
										            <!--Inicio Ifram FollowUp Area-->
										            <iframe name="cmbArea" src="ManifestacaoFollowup.do?acao=showAll&tela=cmbAreaFollowupManif" width="100%" height="23px" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
										            <script>cmbArea.location = "ManifestacaoFollowup.do?acao=showAll&tela=cmbAreaFollowupManif";</script>
										            <!--Final Ifram FollowUp Area-->
			                                      </td>
			                                    </tr>
			                                    <tr> 
			                                      <td colspan="2" class="principalLabel"><bean:message key="prompt.historicofollowup" /></td>
			                                    </tr>
			                                    <tr> 
			                                      <td colspan="2" class="principalLabel"> 
			                                        <table width="100%" cellspacing="0" cellpadding="0">
		                                          <tr>
		                                            <td width="98%">
									                  <textarea name="textoHistorico" id="textoHistorico" class="principalObjForm" rows="3" style="width:540px"></textarea>
									                </td>
									                <td width="2%">
								                  <img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="setarObjBinoculo(document.getElementsByName('textoHistorico')[0]);showModalDialog('webFiles/workflow/ifrmTxManifFoup.jsp',window,'help:no;scroll:no;status:no;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')" title="<bean:message key="prompt.visualizar"/>"/>
								                </td>
								              </tr>
			                                       </table>
			                                     </td>
			                                   </tr>
			                                   <tr> 
			                                     <td colspan="2" class="principalLabel"><plusoft:message key="prompt.complementohistorico" /></td>
			                                   </tr>
			                                   <tr> 
			                                     <td colspan="2" class="principalLabel"> 
									            <textarea name="textoComplemento" class="principalObjForm" rows="3" readonly="true" style="width:540px"></textarea>
			                                     </td>
			                                   </tr>
			                                   <tr> 
			                                     <td colspan="3" class="principalLabel"> 
			                                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                         <!--tr> 
			                                           <td width="2%" class="principalLstCab">&nbsp;</td>
			                                           <td width="20%" class="principalLstCab">Respons&aacute;vel</td>
			                                           <td width="15%" class="principalLstCab">Evento</td>
			                                           <td width="20%" class="principalLstCab">Hist&oacute;rico</td>
			                                           <td width="14%" class="principalLstCab">Dt. Registro</td>
			                                           <td width="13%" class="principalLstCab">Dt. Prevista</td>
			                                           <td width="16%" class="principalLstCab">Dt. Conclus&atilde;o</td>
			                                         </tr-->
			                                         <tr> 
			                                           <td colspan="7" class="principalLabel" height="140px" valign="top">
											          <!--Inicio Ifram FollowUp Lst-->
											          <iframe name="lstFollowup" src='ManifestacaoFollowup.do?workflow=followup&acao=showAll&tela=lstFollowup&csNgtbFollowupFoupVo.foupNrSequencia=<bean:write name="manifestacaoForm" property="foupNrSequencia" />&csNgtbFollowupFoupVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />&csNgtbFollowupFoupVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />&csNgtbFollowupFoupVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />&csNgtbFollowupFoupVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />' width="101%" height="140px" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
											          <!--Final Ifram FollowUp Lst-->
			                                           </td>
			                                         </tr>
			                                       </table>
			                                     </td>
			                                   </tr>
			                                 </table>
			                                </html:form>
			                            </td></tr>
			                         </table>
                                </div>
                                
								<!--Divs com os conte?dos dos bot?es (abas) gerados-->
								<div id="divBotoes" style="width:99%; display: none"></div>
                                
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td height="18" align="right">
                          <table width="94%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="72%" height="9">
                              <!-- 
                              	<div id="divBotoesReclama" style="visibility: hidden">
	                              	<iframe name="ifrmManifBotoesReclama" src="" width="310" height="28" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	                            </div>
	                             -->
                              </td>
							  <td valign="top" height="9">
								  <iframe name="manifestacaoBotoes" id="manifestacaoBotoes" src="" width="100%" height="20px" frameborder=0></iframe>
							  </td>
                              
                            <script>
          						var nCountLoad=0;
          						function recarregar(){
          							try{
          							    //Chamado: 90832 - 25/09/2013 - Carlos Nunes
          								//Feito a sobrecarga de m�todo para que o espec possa sobrescrever o m�todo, e utilizar o idAsn1 e o idAsn2 para alguma valida��o especifica.
          								var urlBotao = "Manifestacao.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_BOTOES_TPMA%>";
									    urlBotao += "&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />";
									    urlBotao += "&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>";
									    urlBotao += "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />";
									    urlBotao += "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />";
      									manifestacaoBotoes.location.href = urlBotao;
      									
          							}catch(e){
          								if(nCountLoad < 5){
											nCountLoad++;
											setTimeout('recarregar()',200);
										}
          							}
          						}
          						recarregar();
          					</script>
          					  <td width="22%" height="9">
								  <table width="100%" border="0" cellspacing="0" cellpadding="0">
								    <tr> 
								      <td width="24%" align=left><img src="webFiles/images/icones/impressora.gif" width="26" height="25" class="geralCursoHand" onclick="consultaManifestacao()" title="<bean:message key="prompt.imprimir" />"></td>
								      <td class="principalLabel" width="22%" align="center"><img name="bt_CriarCarta" id="bt_CriarCarta" src="webFiles/images/botoes/bt_CriarCarta.gif" width="25" height="22" title="<bean:message key="prompt.Responder" />" class="geralCursoHand" onClick="chamaCarta()"> 
								      </td>
								      <td name="CriarCartaTD" id="CriarCartaTD" class="principalLabelValorFixo" onClick="chamaCarta()" width="56%"><span name="CriarCarta" id="CriarCarta" class="geralCursoHand"><bean:message key="prompt.Responder" /></span></td>
								    </tr>
								  </table>
          					  </td>
                              
                              <td width="6%" height="9">
                                <img name="bntConfirmar" id="bntConfirmar" src="webFiles/images/botoes/bt_confirmar.gif" width="74" height="20" class="geralCursoHand" onclick="submeteSalvar();">
								 <script>
								 	if(<%=idNivelAcesso%> != 1){
								 		if('<bean:write name="manifestacaoForm" property="csAstbFuncresolvedorFureVo.fureInResolver" />' == 'false') {
								 			document.all.item("bntConfirmar").disabled = true;
								 			document.all.item("bntConfirmar").className = 'geralImgDisable';
										}
									}
								 </script>
                                <!--img src="webFiles/images/botoes/bt_voltar.gif" width="60" height="20" class="geralCursoHand" border="0" onclick="voltar()"-->
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>
<form name="correspondenciaForm" action="/csicrm/Correspondencia.do" method="post">
<input type="hidden" name="fcksource" value="true" />
<input type="hidden" name="classificador" value="S" />
<input type="hidden" name="tela" value="compose" />
<input type="hidden" name="origem" value="workflow" />
<input type="hidden" name="acaoSistema" value="R" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrInEnviaEmail" value="S" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.idPessCdPessoa" />
<input type="hidden" name="idEmprCdEmpresa" />
<input type="hidden" name="idMatmCdManiftemp" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailDe" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailPara" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailCC" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailCCO" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsTitulo" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrTxCorrespondencia" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.maniNrSequencia" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.idAsn1CdAssuntonivel1" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.idAsn2CdAssuntonivel2" />
<input type="hidden" name="csNgtbCorrespondenciCorrVo.idChamCdChamado" />

</form>
<script>

if ('<bean:write name="manifestacaoForm" property="foupNrSequencia" />' != '0' && '<bean:write name="manifestacaoForm" property="foupNrSequencia" />' != '') {
	AtivarPasta('FOLLOWUP');
	MM_showHideLayers('manifestacao','','hide','destinatario','','hide','followup','','show');
	
	//Chamado: 96832 - 08/10/2014 - Carlos Nunes
	var desabilitarDest = false;
	
	//Verificar se o destinatario da manifestaca � o mesmo que o funcionario logado,
	//Se n�o for verificar se � pertecente a �rea resolvedora, se n�o for desabilita a resposta de destinatario
	if(manifestacaoForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value != '<%=idFuncGerador%>'){
		if('<bean:write name="manifestacaoForm" property="csAstbFuncresolvedorFureVo.fureInResolver" />' == 'false'){
			desabilitarDest = true;
		}
		//Verifica se o destinatario mesmo sendo o mesmo � o responsavel, se n�o for desabilita.
	}else if('<bean:write name="manifestacaoForm" property="csAstbManifestacaoDestMadsVo.madsInParaCc" />' == 'false'){
		desabilitarDest = true;
	}
	
	if(desabilitarDest){
		document.getElementsByName("csAstbManifestacaoDestMadsVo.madsTxResposta")[0].readOnly = true;
		document.all.item("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta").readOnly = true;
		document.all.item("chkDataConclusao").disabled = true;
		document.all.item("setaDest").disabled = true;
		setTimeout('desabilitaStatus()',300);
	}
	
	document.all.item("bntConfirmar").disabled = false;
	document.all.item("bntConfirmar").className = 'geralCursoHand';
}

var nCountDesab = 0;

function desabilitaStatus(){
	try{
		cmbStatusManif.desabilitar();
	}catch(e){
		if(nCountDesab < 5){
			setTimeout("desabilitaStatus()",300);
			nCountDesab++;
		}
	}
}

//Chamado 106250 - 14/01/2016 Victor Godinho
if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACA_CABECALHO_VISUALIZACAO%>')){
	document.getElementById('cabecalho').style.visibility='visible';
	document.getElementById('cabecalho').style.display='block';
}

if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACA_ABAMANIFESTACAO_VISUALIZACAO%>')){
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACA_ABADESTINATARIO_VISUALIZACAO%>')){		
		document.getElementById("divFOLLOWUP").style.display = "block";
		document.getElementById("abaFOLLOWUP").className = "principalPstQuadroLinkSelecionado";		
	}else{
		document.getElementById("divDESTINATARIO").style.display = "block";
		document.getElementById("abaDESTINATARIO").className = "principalPstQuadroLinkSelecionado";		
	}

	document.getElementById('abaMANIFESTACAO').style.display='none';
	document.getElementById('divMANIFESTACAO').style.display='none';
	
	
}

if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACA_ABADESTINATARIO_VISUALIZACAO%>')){
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACA_ABAMANIFESTACAO_VISUALIZACAO%>')){
		document.getElementById("divFOLLOWUP").style.display = "block";
		document.getElementById("abaFOLLOWUP").className = "principalPstQuadroLinkSelecionado";		
	}else{
		document.getElementById("divMANIFESTACAO").style.display = "block";
		document.getElementById("abaMANIFESTACAO").className = "principalPstQuadroLinkSelecionado";		
	}
	document.getElementById('abaDESTINATARIO').style.display='none';
	document.getElementById('divDESTINATARIO').style.display='none';
	
}

setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_CARTA_VISUALIZA%>', window.document.all.item("bt_CriarCarta"));
if(!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_CARTA_VISUALIZA%>')){
	document.getElementById('CriarCartaTD').disabled=true;
}

if(trim(document.getElementsByName('csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta')[0].value) != ""){
		document.getElementsByName('csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta')[0].readOnly = true;
		document.getElementById('chkDataConclusao').disabled = true;
}

</script>
</body>
</html>