<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title><bean:message key="prompt.msdworkflow" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<script language="JavaScript">
<!--
	function openWindow(theURL,winName,features) { //v2.0
		window.open(theURL,winName,features);
	}

	function fechaJanela() {
		window.opener = self;
		window.close()
	}	

function ValidaCampos(){

	if(loginForm.funcDsLoginname.value == ""){
		alert("<bean:message key="prompt.alert.login" />");
		loginForm.funcDsLoginname.focus();
		return false;
	}
	if(loginForm.funcDsPassword.value == ""){
		alert("<bean:message key="prompt.alert.senha" />");
		loginForm.funcDsPassword.focus();
		return false;
	}
	return true;
}
//-->
</script>

</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onLoad="MM_preloadImages('webFiles/images/login/bt_ok_wf_02.gif','webFiles/images/login/bt_cancelar02.gif');showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="loginForm"	action="/Login.do" focus="funcDsLoginname">
<html:hidden property="tela" value="workFlow" />
<table width="600" border="0" cellspacing="0" cellpadding="0" height="400">
  <tr>
    <td background="webFiles/images/background/tela_login_workflow.jpg" align="center">
      <table width="70%" border="0" cellspacing="0" cellpadding="0" height="235">
        <tr> 
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr> 
          <td valign="bottom" class="principalLabel" colspan="2">&nbsp;</td>
        </tr>
        <tr> 
          <td height="22" width="83%" class="principalLabel" valign="bottom">
            <bean:message key="prompt.login" />
          </td>
          <td height="38" rowspan="5" width="17%">&nbsp; </td>
        </tr>
        <tr> 
          <td valign="top" class="principalLabel" width="83%"> 
            <html:text property="funcDsLoginname" styleClass="principalObjForm" maxlength="20" />
          </td>
        </tr>
        <tr> 
          <td height="16" width="83%" class="principalLabel" valign="bottom">
            <bean:message key="prompt.senha" />
          </td>
        </tr>
        <tr> 
          <td width="83%" valign="top"> 
            <html:password property="funcDsPassword" styleClass="principalObjForm" maxlength="20" />
          </td>
        </tr>
        <tr>
          <td width="52%" align="center" class="principalLabel"><font color="red"><html:errors /></font></td>
        </tr>
        <tr> 
          <td width="83%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr align="center"> 
                <td align="right">
                  <input type="image" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','webFiles/images/login/bt_ok_wf_02.gif',1)" name="Image1" border="0" src="webFiles/images/login/bt_ok_wf_01.gif" width="58" height="58" onclick="return ValidaCampos()">
                </td>
                <td>
                  <a href="#" onclick="window.close();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image2','','webFiles/images/login/bt_cancelar02.gif',1)"><img name="Image2" border="0" src="webFiles/images/login/bt_cancelar01.gif" width="49" height="47"></a>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>