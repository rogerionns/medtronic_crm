<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" 
		import="java.util.*,br.com.plusoft.fw.util.*,br.com.plusoft.csi.adm.vo.MenuItensVo,br.com.plusoft.fw.base64.mVertImages,br.com.plusoft.csi.adm.helper.MAConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<html>
	<head>
		<title>Menu Vertical</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="expires" content="0">

		<link rel="stylesheet" href="/plusoft-resources/css/menu-lite.css" type="text/css">
		<script type="text/javascript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
		
		
		<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
		
		
		<style type="text/css">
		body {
			border: 1px #9bb9c8 solid;
		}
		</style>
	</head>
	<body>
		<logic:present name="menusVector">
		<ul>
			<li class="plusoft">
				PLUSOFT
			</li>
		<logic:iterate id="menu" name="menusVector" indexId="menuId">
			<li class="listmenu">
				<a href="#" class="menu" menuid="<bean:write name="menuId" />"><bean:write name="menu" property="label" />
				</a><ul class="submenu" menuid="<bean:write name="menuId" />">
			<logic:iterate id="item" name="menu" property="itensMenu" indexId="itemId">
				<li permission="<bean:write name="item" property="permissiondepends" />"><a href="<bean:write name="item" property="link" />" link="<bean:write name="item" property="link" />" botaoname="<bean:write name="item" property="name" />" botao="<bean:write name="item" property="csCdtbBotaoBotaVo.idBotaCdBotao" />" class="menuitem">
					<img src="<bean:write name="item" property="image" />" />&nbsp;
					<bean:write name="item" property="label" filter="false" />
				</a></li> 
			</logic:iterate>
				<li permission=""><a href="#" botaoname="voltar" botao="0" class="menuitem">
					<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
					<img src="webFiles/Menu/images/mvert/mnvopt13.gif" width="16" />&nbsp;
					<% } else { %>
					<img src="<%=mVertImages.getImageData("mnvopt13.gif") %>" />&nbsp;
					<% } %>
					<plusoft:message key="prompt.voltar" />
				</a></li>
				</ul></li>
		</logic:iterate>
		</ul>
		</logic:present>
		<logic:notPresent name="menusVector">
			N�o foi poss�vel carregar o menu.
		</logic:notPresent>
		
		<script type="text/javascript">
			function MM_openBrWindow(theURL,winName,features) { //v2.0
				modalWin = window.open(theURL,winName,features);
				if (modalWin!=null && !modalWin.closed){
					//self.blur();
					modalWin.focus();
				}
			}

		
			/**
			  * Novo mVert 
			  * Desenvolvido para acelerar o tempo de carregamento de menu atrav�s de HTML, e n�o via javascript
			  * 
			  * @author jvarandas
			  * @since 21/01/2011
			  */
			$(document).ready(function() {
				// Clique dos menus, para abrir os itens do menu selecionado
				$(".menu").click(function(event) {
					event.preventDefault();

					var s = $("ul[menuid="+this.getAttribute("menuId")+"]").css("display");

					if(s!="block") {
						$(".submenu").hide();

						$("ul[menuid="+this.getAttribute("menuId")+"]").show();
					}
				});

				// Clique dos itens de menu para carregar o item respectivo no ifrmConteudo e marca ele como selected
				$(".menuitem").click(function(event) {
					event.preventDefault();
					 
					if(this.getAttribute("botaoname")=="voltar") {
						parent.parent.voltar();
						return;
					}

					
					$(".menuitem").removeClass("selected");
					$(this).addClass("selected");
					//<!--Jonathan | Adequa��o para o IE 10-->
					if(this.getAttribute("botao") != undefined && this.getAttribute("botao") != "0") {
						acionarLink(this.getAttribute("botao"));
					} else {
						parent.acaoMenu(this.getAttribute("botaoname"), this.getAttribute("link"), new Array(), this.getAttribute("botao"));
					}
					
				});


				
				// Varre os menus verificando a permiss�o e exibindo
				var visibleMenus = 0;
				var firstMenu = -1;
				$(".listmenu").each(function() {
					var hasPermission = false;
					var itemPermission = "";
	
					// Varre cada item do menu
					for(var i = 0; i<$(this).find("li").length; i++) {
						itemPermission = $(this).find("li").get(i).getAttribute("permission");
	
						// Se o item for permissionado, verifica permiss�o
						if(itemPermission!="") {
							if(window.top.getPermissao(itemPermission)) {
								hasPermission = true;
								if(firstMenu==-1) firstMenu = i;
							} else {
							// Se n�o tiver permiss�o, esconde o item
								$(this).find("li").get(i).style.display="none";
							}
						} else {
							hasPermission = true;
							if(firstMenu==-1) firstMenu = i;
						}
					}
	
					// Se algum item tiver permissao, mostra o menu
					if(hasPermission) { 
						$(this).show();
						visibleMenus++;
					}
				});

				// Chamado: 85553 - 31/12/2012 - Carlos Nunes
				$(".submenu").css("height", (580-(visibleMenus*24))+"px");
				$(".submenu").css("overflow", "auto");

				$(".menu[menuid=0]").click();
				$(".menuitem").get(firstMenu).click();
			});
			
			
			function acionarLink(idBotao)
			{
				var ajax = new ConsultaBanco("<%=MAConstantes.ENTITY_CS_CDTB_BOTAO_BOTA %>", "ConsultaBanco.do");	
				ajax.addField("statementName","select-botaoparametros-by-pk");
				ajax.addField("id_bota_cd_botao", idBotao);

				ajax.executarConsulta(retornoAcionarLink, false, true);
			}

<%
/*
Henrique 08/09/2013 - Tratamento pois estava abrindo em duplicidade as funcoes extras no workflow
e tambem n�o estava abrindo as funcoes que s�o do tipo nao modal, pois o metodo MM_openBrWindow nao estava no escopo global.
*/
%>
			
			function retornoAcionarLink(ajax){ 
				if(ajax.getMessage() != ''){
					alert("<bean:message key='prompt.naoFoiPossivelObterDadosParametrosParaBotao'/>\n\n"+ ajax.getMessage());
					return;
				}

				ret = ajax.getRecordset();

				var parametrosBotao = new Array();
				var botaLink = "";
				var botaId = "";
				var botaDimensao = "";
				var botaInModal = "";
				
				while(ret.next()){
					

					if(ret.get("pabo_ds_parametrobotao")!="") {
		
						var paramBotao = new Array();

						paramBotao.push(ret.get("pabo_ds_parametrobotao") );
						paramBotao.push(ret.get("pabo_ds_nomeinterno") );
						paramBotao.push(ret.get("pabo_ds_parametrointerno") );
						paramBotao.push(ret.get("pabo_in_obrigatorio") );

						parametrosBotao.push(paramBotao);

					}

					botaLink = ret.get("bota_ds_link");
					botaId = ret.get("id_bota_cd_botao");
					botaDimensao = ret.get("bota_ds_dimensao");
					botaInModal = ret.get("bota_in_modal");
				}

				if(botaInModal=='S') {
					var linkP =  parent.montarLink(botaLink, parametrosBotao, botaId);
					if(linkP != "")//Chamado: 93411 - 06/03/2014 - Carlos Nunes
					{
	  	  				showModalOpen(linkP, window, botaDimensao);
					}
				}else if(botaInModal == 'O'){
					var linkP =  parent.montarLink(botaLink, parametrosBotao, botaId);
					if(linkP != "")//Chamado: 93411 - 06/03/2014 - Carlos Nunes
					{
	  	  				MM_openBrWindow(linkP, "FUNCAOEXTRA"+botaId, botaDimensao);
					}
				} else {
					parent.acaoMenu("botao"+botaId, botaLink, parametrosBotao, botaId);
				}			
			}
		</script>
	</body>
</html>	 