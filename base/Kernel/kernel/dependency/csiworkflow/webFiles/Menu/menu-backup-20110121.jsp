<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<html>
	<head>
		<title>Menu Vertical</title>
		<link name="lnkCss" id="lnkCss" rel="stylesheet" href="webFiles/Menu/css/mvert_Azul.css" type="text/css">
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
	</head>
	
	<script src="webFiles/funcoes/funcoes.js"></script>
	<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
	<script src="webFiles/Menu/menu.js"></script>
	<script language="JavaScript">
	
		// Tratamento para Itens de Menu *********************************************
		
		var links = new Array();
		 
		function itemMenuTagChanged(strTipo, nItemId, nItemTag, strOldTag, strTag) {}
		function itemMenuVisibleChanged(strTipo, nItemId, bolOldVisible, bolVisible) {}
		function itemMenuEnabledChanged(strTipo, nItemId, bolOldEnabled, bolEnabled) {}
		function itemMenuImagesPathChanged(nItemId, strOldPathEnabled, strOldPathDisabled, strPathEnabled, strPathDisabled) {}
		function itemMenuCaptionChanged(strTipo, nItemId, strOldCaption, strNewCaption) {}
		function itemMenuToolTipChanged(strTipo, nItemId, strOldToolTip, strNewToolTip) {}
		function cliqueMenuVertical(nId) {return false}
		
		function cliqueItemMenuVertical(nItemId) {	
			ehVoltar = true;

			var cLinkPadrao = '';

			//comentado pois ja coloca estes mesmos parametros na funcao montaLink do ifrmWorkflow
			/*cLinkPadrao = '';
			cLinkPadrao += '&idChamCdChamado=<%=request.getParameter("idChamCdChamado")%>';
			cLinkPadrao += '&maniNrSequencia=<%=request.getParameter("maniNrSequencia")%>';
			cLinkPadrao += '&idAsn1CdAssuntoNivel1=<%=request.getParameter("idAsn1CdAssuntoNivel1")%>';
			cLinkPadrao += '&idAsn2CdAssuntoNivel2=<%=request.getParameter("idAsn2CdAssuntoNivel2")%>';
			cLinkPadrao += '&idLinhCdLinha=<%=request.getParameter("idLinhCdLinha")%>';
			cLinkPadrao += '&idTpmaCdTpManifestacao=<%=request.getParameter("idTpmaCdTpManifestacao")%>';
			cLinkPadrao += '&idFuncCdFuncionario=<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>';
			cLinkPadrao += '&idGrmaCdGrupoManifestacao=<%=request.getParameter("idGrmaCdGrupoManifestacao")%>';
			cLinkPadrao += '&prasDsProdutoAssunto=';
			cLinkPadrao += '&idPessCdPessoa=<%=request.getParameter("idPessCdPessoa")%>';
			cLinkPadrao += '&idMatpCdManifTipo=<%=request.getParameter("idMatpCdManifTipo")%>';
			cLinkPadrao += '&idMatmCdManifTemp=0';
			cLinkPadrao += '&idEmprCdEmpresa=<%=request.getParameter("idEmprCdEmpresa")%>';
			*/

/*
			cLinkPadrao += '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<%=request.getParameter("idChamCdChamado")%>';
			cLinkPadrao += '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<%=request.getParameter("maniNrSequencia")%>';
			cLinkPadrao += '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<%=request.getParameter("idAsn1CdAssuntoNivel1")%>';
			cLinkPadrao += '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<%=request.getParameter("idAsn2CdAssuntoNivel2")%>';
			cLinkPadrao += '&idFuncCdFuncionario=<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>';
			cLinkPadrao += '&csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha=<%=request.getParameter("idLinhCdLinha")%>';
			cLinkPadrao += '&csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=<%=request.getParameter("idTpmaCdTpManifestacao")%>';
			cLinkPadrao += '&csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao=<%=request.getParameter("idGrmaCdGrupoManifestacao")%>';
*/
		
		  <!-- MENUS -->    
		  <logic:iterate id="menus" name="menusVector" indexId="seqmenu">
				  
				  <!-- ITENS DE MENUS -->    
				  <logic:iterate id="itens" name="menus" property="itensMenu" indexId="seqitem">

						<!-- PARAMETROS DO BOTAO -->
						//Mudando a acao do sistema para altera��o
												
						links[<%=seqitem%>] = new Array();
						links[<%=seqitem%>][1] = new Array();
						links[<%=seqitem%>][1][0] = "";
						links[<%=seqitem%>][1][1] = "btn<bean:write name="itens" property="csCdtbBotaoBotaVo.idBotaCdBotao" />";
						
						var parametrosBotao = new Array();
						var nParametrosBotao = 0;
						
						<logic:iterate id="csDmtbParametrobotaoPaboVector" name="itens" property="csCdtbBotaoBotaVo.csDmtbParametrobotaoPaboVector" indexId="numeroParametro">
							
		  					parametrosBotao[nParametrosBotao] = new Array();
			  				
			  				parametrosBotao[nParametrosBotao][1] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrobotao" />';
			  				parametrosBotao[nParametrosBotao][2] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsNomeinterno" />';
			  				parametrosBotao[nParametrosBotao][3] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrointerno" filter="false"/>';
			  				parametrosBotao[nParametrosBotao][4] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboInObrigatorio" />';
			  				
			  				//Danilo Prevides - 04/12/2009 - 67546 - INI
							if (parametrosBotao[nParametrosBotao][2] == 'pessDsCpf'){
								//parametrosBotao[nParametrosBotao][3] = "window.top.document.pessoaForm['pessDsCpf'].value";
								parametrosBotao[nParametrosBotao][3] = "window.top.ifrmTelaWorkflow.pessDsCpf";
							}

							//Danilo Prevides - 04/12/2009 - 67546 - FIM

			  				nParametrosBotao++;
			  				
						</logic:iterate>

						links[<%=seqitem%>][1][2] = parametrosBotao;
						
				  		if(nItemId == "<%= seqmenu%><%= seqitem%>"){
							  if("<bean:write name="itens" property="openmodal" />" == 'S'){
							  
							  	  var link = "<bean:write name="itens" property="link"  filter="false" />";
							  	  
							  	  if (link.indexOf('?') > -1){
							  			var linkP =  parent.montarLink(link, links[<%=seqitem%>][1][2], "<bean:write name="itens" property="csCdtbBotaoBotaVo.idBotaCdBotao" />");
							  	  		showModalDialog(linkP, window, "<bean:write name="itens" property="modaloptions" />");
							  	  }else{
							  	  		if(cLinkPadrao.indexOf('&') == 0){
								  	  		var linkP =  parent.montarLink(link ,links[<%=seqitem%>][1][2], "<bean:write name="itens" property="csCdtbBotaoBotaVo.idBotaCdBotao" />");
							  	  			showModalDialog(linkP, window, "<bean:write name="itens" property="modaloptions" />");
							  	  		}else{
							  	  			var linkP =  parent.montarLink(link ,links[<%=seqitem%>][1][2], "<bean:write name="itens" property="csCdtbBotaoBotaVo.idBotaCdBotao" />");
							  	  			showModalDialog(linkP, window, "<bean:write name="itens" property="modaloptions" />");
							  	  		}
							  	  }
								  return;
								  
							  }else{
									
							  	  var link = "<bean:write name="itens" property="link" filter="false" />";
							  	  
							  	  //if(link == "DadosPess.do" || link == "WorkFlow.do" || link == "Historico.do"){
							  	 		parent.acaoMenu("<bean:write name="itens" property="name" />", link, links[<%=seqitem%>][1][2], "<bean:write name="itens" property="csCdtbBotaoBotaVo.idBotaCdBotao" />");
							  	 		return;
							  	  //}
							  	  
								  //if (link.indexOf('?') > -1){
								  		//parent.acaoMenu("<bean:write name="itens" property="name" />", link, links[<%=seqitem%>][1][2], "<bean:write name="itens" property="csCdtbBotaoBotaVo.idBotaCdBotao" />");
								 // }else{
								  		//if(cLinkPadrao.indexOf('&') == 0){
								  			//parent.acaoMenu("<bean:write name="itens" property="name" />", link, links[<%=seqitem%>][1][2], "<bean:write name="itens" property="csCdtbBotaoBotaVo.idBotaCdBotao" />");
								  		//}else{
								  		//	parent.acaoMenu("<bean:write name="itens" property="name" />", link, links[<%=seqitem%>][1][2], "<bean:write name="itens" property="csCdtbBotaoBotaVo.idBotaCdBotao" />");
								  		//}
								  //}
								  //return;
							  }
							  ehVoltar = false;
						}
				  </logic:iterate>
				  <!-- ITENS DE MENUS -->    
				  				  
		  </logic:iterate>
			if(ehVoltar)
		  		parent.parent.voltar();
		  <!-- MENUS -->
		
			return false;
		}
		
	</script>

	<body name='bodyMenu' id='bodyMenu' bgcolor="#FFFFFF" text="#000000" topmargin=0 leftmargin=0 bottommargin=0 rightmargin=0 class="esquerdoBgrPageIFRM">
		<div class="menuvertBackColor" name="divMenuCont" id="divMenuCont" style="position:absolute; left:0; top:0; width:98%; height:99%; z-index:2; overflow: hidden"></div>
	</body>
</html>

<script language="JavaScript">	

    divMenuCont.height = 650
	setIconesPequenos(true);   

	  <!-- MENUS -->
	  <%
	  int menu = 0;
	  int itemmenu = 0;
	  %>
	  <logic:iterate id="menusVector" name="menusVector">
	  		objMenu = addMenu(<%= menu%>, '<bean:write name="menusVector" property="label" />', '<bean:write name="menusVector" property="label" />' );

	  		<!-- ITENS DE MENUS -->
		  	<logic:iterate id="itensMenu" name="menusVector" property="itensMenu">		  		
		  		if (getPermissao('<bean:write name="itensMenu" property="permissiondepends" />') || '<bean:write name="itensMenu" property="permissiondepends" />' == ''){
				  sobjItMenu = addItemMenu("<%= menu%><%= itemmenu%>", <%= menu%>, '<bean:write name="itensMenu" property="label" filter="html" />', '<bean:write name="itensMenu" property="label" filter="html" />', '<bean:write name="itensMenu" property="image" />', '<bean:write name="itensMenu" property="imageDisabled" />');
				<%	itemmenu++;	%>
				}
		  	</logic:iterate>
		  	<!-- ITENS DE MENUS -->		 
		 sobjItMenu = addItemMenu("<%= menu %><%= itemmenu%>", <%= menu%>, '<bean:message key="prompt.voltar"/>', '<bean:message key="prompt.voltar"/>', 'webFiles/images/botoes/out.gif', 'webFiles/images/botoes/out.gif');
		 <%	menu++;	%>
		 <%	itemmenu = 0;	%>
	  </logic:iterate>
	  
	  
	  
	  <!-- MENUS -->

	setItemMenuAtivo('MENU', 0, 640);
	
	mouseDownItemMenu(0);
	cliqueItemMenuVertical(0);
</script>