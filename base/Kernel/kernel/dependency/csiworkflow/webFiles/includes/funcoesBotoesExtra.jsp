<%@page import="br.com.plusoft.saas.SaasHelper"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<script language="JavaScript">

function obterLink(link, parametros, idBotao){
	//parametros[0...][1] - paboDsParametrobotao
	//parametros[0...][2] - paboDsNomeinterno
	//parametros[0...][3] - paboDsParametrointerno
	//parametros[0...][4] - paboInObrigatorio

		var valor = "";
		var newLink = link;
		var bExecutavel = (link.toUpperCase().indexOf("EXE") > 0);
		var encontrouParametro = false;
		
		if(parametros == undefined) return;
		
		if(bExecutavel && parametros.length > 0){
			if(newLink.indexOf(" ") > 0)
				newLink = "'"+ newLink.substring(0, newLink.indexOf(" ")) +"' '"+ newLink.substring(newLink.indexOf(" "));
			else
				newLink = "'"+ newLink +"' '";
		}
		else if(bExecutavel){ 
			newLink = "'"+ newLink;
		}
		
		for (var i = 0; i < parametros.length; i++){
			valor = "";
			
			try{			
				//onde obter a informacao
				if(parametros[i][2] == "perm"){
					//valor = findPermissoesByFuncionalidade("adm.fc." + idBotao + ".");
				}else{
					//if(parametros[i][3] == "c" && !isNaN(window.top.superiorBarra.barraCamp.chamado.innerText))
					//	valor = Number(window.top.superiorBarra.barraCamp.chamado.innerText);
					//else
						valor = eval(parametros[i][3]);					
				}
				encontrouParametro = true;
			}catch(e){
				encontrouParametro = false;
			}
			
			//Se o parametro e obrigatorio
			if (parametros[i][4] == 'S'){

				if(newLink.indexOf(parametros[i][2]) == -1){
				
					if(parametros[i][2] == "idChamCdChamado" && (valor == "" || valor == "0" || valor == "Novo")){
						alert("<bean:message key="prompt.alert.parametroObrigatorio"/>");
						return "";
					}

					if (parametros[i][2] == ""){
						alert("<bean:message key='prompt.naoFoiPossivelObterNomeInternoParametroObrigatorio'/>" + " " + parametros[i][1]);
						return "";
					}


					if (valor == "" || valor == "0"){
						alert("<bean:message key="prompt.alert.parametroObrigatorio"/>");
						return "";
					}
				}
			}
			
			if (valor != null && encontrouParametro){

				if(newLink.indexOf(parametros[i][2]) == -1){
					
					if (newLink.indexOf('?') == -1 && !bExecutavel){
						newLink = newLink + "?";
					}
					
					var ultimoCaracter = newLink.substring(newLink.length - 1);
					if (ultimoCaracter != "?" && ultimoCaracter != "&"){
						newLink = newLink + "&";
					}

					newLink = newLink + parametros[i][2] + "=" + valor;
				}
				
			}
		}
		
		if(bExecutavel)
			newLink += "'";
		
		if (!bExecutavel && newLink.indexOf('http') == -1){
			if (newLink.indexOf('?') == -1){
				newLink = newLink + "?";
			}
			
			var ultimoCaracter = newLink.substring(newLink.length - 1);
			if (ultimoCaracter != "?" && ultimoCaracter != "&"){
				newLink = newLink + "&";
			}
			newLink+= "idBotaCdBotao="+idBotao;
			<% if(SaasHelper.aplicacaoSaas) { %>
			newLink = obterLinkSaas(newLink);
			<% } %>
					
		}
		
		return newLink; 
	}

// -->
</script>


