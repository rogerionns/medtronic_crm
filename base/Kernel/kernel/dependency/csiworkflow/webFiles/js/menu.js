this.Codigo=0
this.MenuIndex=-1
this.ItemMenuIndex=-1
this.Caption=''
this.ToolTip=''
this.IsVisible=true
this.IsEnabled=true
this.ItemImagePath=''
this.DisabledItemImagePath=''
this.Tag1='' // Propriedade auxiliar
this.Tag2='' // Propriedade auxiliar


// ---------------------------------------------------------------------------------
// Vari�veis Globais para controle do Menu e itens ---------------------------------
colMenu = new Array
colItemMenu = new Array
nMenus = 0
nItensMenu=0
nIdMenuAtual = -1
nIdItemAtual = -1
nLastBtn=-1
nLastBtnClick=-1
nLastItmClick=-1
nLastItm=0
nLastCss=1;
bolStartMenuClickOnSetMenuAtivo=false;
bolStartItemClickOnSetMenuAtivo=false
bolSmallIcos = false

// ********************** Tratamento dos bot�es de menu ****************************

// Adiciona um Bot�o de Menu -------------------------------------------------------
function addMenu(nCodigo, strCaption, strToolTip) { 
  
	oMenu = new newMenu(nCodigo, strCaption, strToolTip)	
	insereMenuLista(oMenu, nMenus-1, divMenuCont)	
	
	return oMenu
  
} 


// Cria novo elemento no array de menus -------------------------------------------
function newMenu(nCodigo, strCaption, strToolTip) {
	this.Codigo = nCodigo
	this.Caption = strCaption
	this.ToolTip = strToolTip
	this.MenuIndex = nMenus	
	this.IsVisible = true
	this.IsEnabled = true
	
	colMenu[nMenus] = this
	nMenus++ 
	nIdMenuAtual = nMenus - 1
	
	
}


// Cria um template de tabela para inserir um bot�o no menu vertical ---------------
function templateMenu(bPicture) {
	var cTemplateTab1 = new String()
	
	cTemplateTab1 = ""	
	cTemplateTab1 = cTemplateTab1 + "<div name='layBotao@INDICE@' id='layBotao@INDICE@' style='left:0; top:" + ((nMenus-1) * 23) + "; width:99%; height:300; z-index:1; overflow: hidden; visibility: visible'>"
    cTemplateTab1 = cTemplateTab1 + "   <table name='tabBotao@INDICE@' id='tabBotao@INDICE@' class='objetoBotaoNormal' width='99%' border='0' cellspacing='0' cellpadding='0' >"
    cTemplateTab1 = cTemplateTab1 + "      <tr>"
    cTemplateTab1 = cTemplateTab1 + "          <td name='tdBotaoCaption@INDICE@' id='tdBotaoCaption@INDICE@' align='center' onmouseover='mouseOverBotao(@INDICE@)' onmousedown='mouseDownBotao(@INDICE@)' onmouseup='mouseUpBotao(@INDICE@)'  onclick='menuClick(@INDICE@)' ><span><ACRONYM TITLE='@MENU_TOOLTIP@' style='border: 0'>@MENU_CAPTION@</ACRONYM></span></td>"
    cTemplateTab1 = cTemplateTab1 + "      </tr>"
    cTemplateTab1 = cTemplateTab1 + "   </table>"
    cTemplateTab1 = cTemplateTab1 + "   <div align='center' name='layItemBotao@INDICE@' id='layItemBotao@INDICE@' style='left:0; top:24; width:99%; height:127; z-index:3; overflow: auto; visibility: visible'>"
    cTemplateTab1 = cTemplateTab1 + "   </div>"
    cTemplateTab1 = cTemplateTab1 + "   <div align='center' name='pqlayItemBotao@INDICE@' id='pqlayItemBotao@INDICE@' style='left:0; top:24; width:99%; height:127; z-index:3; overflow: auto; visibility: visible'>"
    cTemplateTab1 = cTemplateTab1 + "   </div>"
    cTemplateTab1 = cTemplateTab1 + "</div>"    
    
	return cTemplateTab1
	
}


// Adiciona um novo e-mail a lista em tela -----------------------------------------
function insereMenuLista(objMenu, nItem, objCamada){
		
	var cTemplate = new String();
	var cTemplateTabela = new String();
	
	if (objMenu.picture!='') {
		cTemplateTabela = templateMenu(false)
	}
	else {
		cTemplateTabela = templateMenu(true)
	}
	
	cTemplateTabela = cTemplateTabela.replace(/@MENU_CAPTION@/g, objMenu.Caption)
	cTemplateTabela = cTemplateTabela.replace(/@MENU_TOOLTIP@/g, objMenu.ToolTip)	
	cTemplateTabela = cTemplateTabela.replace(/@INDICE@/g, nItem)			
	objCamada.insertAdjacentHTML("BeforeEnd", cTemplateTabela);
			
}

// Oculta um menu da tela ----------------------------------------------------------
function menuHidden(nId) {
	window.document.all.item('layBotao' + nId).style.display = "none";
}


// Evento disparado quando passa o mouse sobre um menu -----------------------------
function mouseOverBotao(nId) {
	
	if (!colMenu[nId].IsEnabled) {
		return false
	}	
	
	if (nLastBtn>-1) {
		if (nLastBtn != nId) {
		    if (colMenu[nLastBtn].IsEnabled) {					
				window.document.all.item('tabBotao' + nLastBtn).className = 'objetoBotaoNormal';
			}
		}	
	}
	
	window.document.all.item('tabBotao' + nId).className = 'objetoBotaoMouseOver';
	nLastBtn = nId;
	
	return false
	
}


// Evento disparado quando pressionado o mouse sobre um menu ----------------------
function mouseDownBotao(nId) {
	
	hideMenuCss()
	
	if (!colMenu[nId].IsEnabled) {
		return false
	}	
	
	window.document.all.item('tabBotao' + nId).className = 'objetoBotaoMouseDown';
	// Volta imagem do bot�o para elevado
	window.document.all.item('tabBotao' + nId).className = 'objetoBotaoNormal';
	
	return false
	
}

// Evento disparado quando pressionado o mouse sobre um menu ----------------------
function mouseUpBotao(nId) {
	if (!colMenu[nId].IsEnabled) {
		return false
	}	
	
	// Volta imagem do bot�o para elevado
	window.document.all.item('tabBotao' + nId).className = 'objetoBotaoNormal';
	
	return false
	
}

// Evento disparado quando clicado sobre um bot�o ---------------------------------
function menuClick(nId) {
    if (!colMenu[nId].IsEnabled) {
		return false
	}	
	
	// O M�todo abaixo deve ser implantado na p�gina que teve o menu inclu�do
	setItemMenuAtivo('MENU', nId, divMenuCont.Height )
	nLastBtnClick = nId
	cliqueMenuVertical(nId)
	
	return false
	
}
	
// Define qual o menu / item deve estar ativo (selecionado) --------------------
function setItemMenuAtivo(strTipo, nItemId, nHeightCont) {
	var nHeightAtivo
	var nHeightItmsAtivo
	var nTop
	var nNumInvisible
	
	hideMenuCss()
	
	if (strTipo=='MENU') {	
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}		
	
		if (!colMenu[nItemId].IsEnabled) {
			alert('O menu [' + nItemId + '][' + colMenu[nItemId].Caption + '] est� desabilitado') 
			return false
		}
		
		if (!colMenu[nItemId].IsVisible) {
			alert('O menu [' + nItemId + '][' + colMenu[nItemId].Caption + '] est� invis�vel') 
			return false
		}
				
		nNumInvisible=0
		for (nLoop=0; nLoop < nMenus; nLoop++) {
			if (!colMenu[nLoop].IsVisible) 
				nNumInvisible++			
		}
		
		if (nHeightCont>0) {
		    divMenuCont.height=nHeightCont
		}
		nHeightAtivo = divMenuCont.height - ( 24 * (nMenus))	
		nHeightAtivo = nHeightAtivo + (24 * nNumInvisible)	
		nHeightItmsAtivo = nHeightAtivo - 24
    
		if (nLastBtnClick != nItemId) {    	
		   for (nLoop=0; nLoop < nMenus; nLoop++) {			       			
		   
				// Oculta os layers de �cones
				window.document.all.item('layItemBotao' + nLoop).style.display = "none";			
				window.document.all.item('pqlayItemBotao' + nLoop).style.display = "none";
							
				if (colMenu[nLoop].IsVisible) {
					window.document.all.item('layBotao' + nLoop).style.height = 23;			
				}
				else {
					window.document.all.item('layBotao' + nLoop).style.height = 0;
				}
				
		    }
		    
			if (bolSmallIcos) {
				window.document.all.item('pqlayItemBotao' + nItemId).style.display = "inline";
			}
			else {
				window.document.all.item('layItemBotao' + nItemId).style.display = "inline";			
			}
			
			window.document.all.item('layBotao' + nItemId).style.top=0
			window.document.all.item('layBotao' + nItemId).style.height = nHeightAtivo	
			window.document.all.item('layItemBotao' + nItemId).style.height = nHeightItmsAtivo
			window.document.all.item('pqlayItemBotao' + nItemId).style.height = nHeightItmsAtivo
		}
	
		if (bolStartMenuClickOnSetMenuAtivo)		
			cliqueMenuVertical(nItemId);
			
	    if (bolStartItemClickOnSetMenuAtivo) {
			nIdItemMenu = getFirstItemMenu(nItemId)
			if (nIdItemMenu>-1) {				
				mouseDownItemMenu(nIdItemMenu);
				cliqueItemMenuVertical(nIdItemMenu);
			}
		}
			
		nLastBtnClick = nItemId
		
		
	}	
	else {
		if (strTipo=='ITEM') {
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')				
				return false
			}
			
			if (!colItemMenu[nItemId].IsEnabled) {
				alert('O item de menu [' + nItemId + '][' + colItemMenu[nMenuId].Caption + '] est� desabilitado') 
				return false
			}
		
			if (!colItemMenu[nItemId].IsVisible) {
				alert('O item de menu [' + nItemId + '][' + colItemMenu[nItemId].Caption + '] est� invis�vel') 
				return false
			}
						
			nMenuId = colItemMenu[nItemId].MenuIndex;			
			if (!colMenu[nMenuId].IsEnabled) {
				alert('O menu [' + nMenuId + '][' + colMenu[nMenuId].Caption + '] do item [' + nItemId + '][' + colItemMenu[nItemId].Caption + '] est� desabilitado') 
				return false
			}
		
			if (!colMenu[nMenuId].IsVisible) {
				alert('O menu [' + nMenuId + '][' + colMenu[nMenuId].Caption + '] do item [' + nItemId + '][' + colItemMenu[nItemId].Caption + '] est� invis�vel') 
				return false
			}
						
			mouseDownItemMenu(nItemId);
			// Obtem o estado da vari�vel para definir se o item deve ser clicado na chamada
			// da pr�pra rotina (com strTipo=MENU)
			bOldClick = bolStartItemClickOnSetMenuAtivo			
			bolStartItemClickOnSetMenuAtivo = false
			setItemMenuAtivo('MENU', nMenuId, divMenuCont.Height) // Chama a pr�pria rotina para arivar o menu
			
			bolStartItemClickOnSetMenuAtivo = bOldClick // Volta a op��o de clique para a default			
			if (bolStartItemClickOnSetMenuAtivo) {
				cliqueItemMenuVertical(nItemId);
			}			
		}	
	}    
}

// Procura pelo primeiro item valido do menu passado por par�metro
function getFirstItemMenu(nIdMenu) {
	
	for (nLoop=0; nLoop<=nItensMenu-1; nLoop++) {
		if (colItemMenu[nLoop].MenuIndex == nIdMenu) {
			return nLoop
		}
	}
	// Se foi at� o final retorna -1
	return -1
}

// Adiciona um novo item de menu dentro do menu passado como par�metro -------------

// Obtem um elemento do menu pelo �ndice -------------------------------------------
function getItemMenuByPos(strTipo, nIndex) { 
 
	if (strTipo=='MENU') {
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}
		
		objItMenu = colMenu[nIndex]
		nIdItemAtual = nIndex
	}
	else {
		if (strTipo=='ITEM') {			
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}
		
			objItMenu = colItemMenu[nIndex]
			nIdItemAtual = nIndex
		}
	}
	return objItMenu;
  
}

function addItemMenu(nCodigo, MenuIndex, strCaption, strToolTip, strItemImagePath, strDisabledItemImagePath ) { 
	
	oItMenu = new newItemMenu(nCodigo, MenuIndex, strCaption, strToolTip, strItemImagePath, strDisabledItemImagePath)
	
    // Identifica o div que representa o menu passado para �cones Grandes
	divBigIco = window.document.all.item('layItemBotao' + MenuIndex)
	// Identifica o div que representa o menu passado para �cones Grandes
	divSmallIco = window.document.all.item('pqlayItemBotao' + MenuIndex)
		
	insereItemMenuLista(oItMenu, nItensMenu-1, divBigIco, divSmallIco)	
	
	return oItMenu
  
} 

// Adiciona novo Item ao array de Itens de menu ------------------------------------
function newItemMenu(nCodigo, MenuIndex, strCaption, strToolTip, strItemImagePath, strDisabledItemImagePath) {
	this.Codigo = nCodigo
	this.MenuIndex=MenuIndex
	this.ItemMenuIndex = nItensMenu	
	this.Caption = strCaption
	this.ToolTip = strToolTip	
	this.ItemImagePath = strItemImagePath
	this.DisabledItemImagePath = strDisabledItemImagePath
	this.IsVisible = true
	this.IsEnabled = true
	
	colItemMenu[nItensMenu] = this
	nItensMenu++ 
	nIdItemAtual = nItensMenu - 1
	
}

// Cria um template do item para adicionar a tela (�cones Granes) --------------------------------
function templateItemMenuGrande() {
	var cTemplateTab1 = new String()
	var strVisible	
	
	cTemplateTab1 = ""
	cTemplateTab1 = cTemplateTab1 + "<div align='center' name='layItemMenu@INDICE@' id='layItemMenu@INDICE@' width:99%; height:100%; z-index:2; visibility: visible'>"
	cTemplateTab1 = cTemplateTab1 + "<table name='tabItemMenu@INDICE@' id='tabItemMenu@INDICE@' width='100%' border='0' cellspacing='0' cellpadding='0' class='objetoItemNormal' align='center'>"
	cTemplateTab1 = cTemplateTab1 + "    <tr> "
	cTemplateTab1 = cTemplateTab1 + "      <td>" 
	cTemplateTab1 = cTemplateTab1 + "        <div align='center'> "
	cTemplateTab1 = cTemplateTab1 + "          <table name='tabInternaItemMenu@INDICE@' id='tabInternaItemMenu@INDICE@' width='90%' border='0' cellspacing='0' cellpadding='0' class='objetoItemNormal'>"
	cTemplateTab1 = cTemplateTab1 + "            <tr> "
	cTemplateTab1 = cTemplateTab1 + "              <td>" 
	cTemplateTab1 = cTemplateTab1 + "                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='objetoItemNormal'>"
	cTemplateTab1 = cTemplateTab1 + "                  <tr> "
	cTemplateTab1 = cTemplateTab1 + "                    <td name='tdEspacoItemMenu@INDICE@' id='tdEspacoItemMenu@INDICE@' class='objetoItemNormal' height='10'> "
	cTemplateTab1 = cTemplateTab1 + "                      <div align='center'></div>"
	cTemplateTab1 = cTemplateTab1 + "                    </td>"
	cTemplateTab1 = cTemplateTab1 + "                  </tr>"
	cTemplateTab1 = cTemplateTab1 + "                  <tr> "
	cTemplateTab1 = cTemplateTab1 + "                    <td name='tdImgItemMenu@INDICE@' id='tdImgItemMenu@INDICE@' class='objetoItemNormal'> "
	cTemplateTab1 = cTemplateTab1 + "                      <div align='center'><img src='@ICONE_MENU@' name='imgItemMenu@INDICE@' id='imgItemMenu@INDICE@' onmousedown='mouseDownItemMenu(@INDICE@)' onclick='itemMenuClick(@INDICE@)' alt='@MENU_TOOLTIP@' width='32' height='32'></div>"
	cTemplateTab1 = cTemplateTab1 + "                    </td>"
	cTemplateTab1 = cTemplateTab1 + "                  </tr>"
	cTemplateTab1 = cTemplateTab1 + "                  <tr> "    
	cTemplateTab1 = cTemplateTab1 + "                    <td name='tdCaptionItemMenu@INDICE@' id='tdCaptionItemMenu@INDICE@' class='objetoItemNormal' align='center' onmousedown='mouseDownItemMenu(@INDICE@)' onclick='itemMenuClick(@INDICE@)' ><span><ACRONYM TITLE='@MENU_TOOLTIP@' style='border: 0'>@MENU_CAPTION@</ACRONYM></span></td>"    
	cTemplateTab1 = cTemplateTab1 + "                  </tr>"
	cTemplateTab1 = cTemplateTab1 + "                </table>"
	cTemplateTab1 = cTemplateTab1 + "              </td>"
	cTemplateTab1 = cTemplateTab1 + "            </tr>"
	cTemplateTab1 = cTemplateTab1 + "          </table>"
	cTemplateTab1 = cTemplateTab1 + "        </div>"
	cTemplateTab1 = cTemplateTab1 + "      </td>"
	cTemplateTab1 = cTemplateTab1 + "    </tr>"
	cTemplateTab1 = cTemplateTab1 + "  </table>"
	cTemplateTab1 = cTemplateTab1 + "<div>"

	return cTemplateTab1
	
}

// Cria um template do item para adicionar a tela (�cones Pequenos) -----------------------------
function templateItemMenuPequeno() {
	var cTemplateTab1 = new String()
	var strVisible	
		
	cTemplateTab1 = ""	
	cTemplateTab1 = cTemplateTab1 + "<div align='center' name='pqlayItemMenu@INDICE@' id='pqlayItemMenu@INDICE@' width:99%; height:100%; z-index:2; visibility: visible'>"
	cTemplateTab1 = cTemplateTab1 + "<table name='pqtabItemMenu@INDICE@' id='pqtabItemMenu@INDICE@' width='100%' border='0' cellspacing='0' cellpadding='0' class='objetoItemNormal' align='center'>"
	cTemplateTab1 = cTemplateTab1 + "    <tr> "
	cTemplateTab1 = cTemplateTab1 + "      <td>" 
	cTemplateTab1 = cTemplateTab1 + "        <div align='center'> "
	cTemplateTab1 = cTemplateTab1 + "          <table name='pqtabInternaItemMenu@INDICE@' id='pqtabInternaItemMenu@INDICE@' width='90%' border='0' cellspacing='0' cellpadding='0' class='objetoItemNormal'>"
	cTemplateTab1 = cTemplateTab1 + "            <tr> "
	cTemplateTab1 = cTemplateTab1 + "              <td>" 
	cTemplateTab1 = cTemplateTab1 + "                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='objetoItemNormal'>"
	cTemplateTab1 = cTemplateTab1 + "                  <tr> "
	cTemplateTab1 = cTemplateTab1 + "                    <td colspan='2' name='pqtdEspacoItemMenu@INDICE@' id='pqtdEspacoItemMenu@INDICE@' class='objetoItemNormal' height='10'> "
	cTemplateTab1 = cTemplateTab1 + "                      <div align='center'></div>"
	cTemplateTab1 = cTemplateTab1 + "                    </td>"
	cTemplateTab1 = cTemplateTab1 + "                  </tr>"
	cTemplateTab1 = cTemplateTab1 + "                  <tr> "
	cTemplateTab1 = cTemplateTab1 + "                    <td name='pqtdImgItemMenu@INDICE@' id='pqtdImgItemMenu@INDICE@' class='objetoItemNormal'>"
	cTemplateTab1 = cTemplateTab1 + "                      <div align='left'><img src='@ICONE_MENU@' name='pqimgItemMenu@INDICE@' id='pqimgItemMenu@INDICE@' onmousedown='mouseDownItemMenu(@INDICE@)' onclick='itemMenuClick(@INDICE@)' alt='@MENU_TOOLTIP@' width='18' height='18'></div>"
	cTemplateTab1 = cTemplateTab1 + "                    </td>"															 
	cTemplateTab1 = cTemplateTab1 + "                    <td name='pqtdCaptionItemMenu@INDICE@' id='pqtdCaptionItemMenu@INDICE@' class='objetoItemNormal' height='8' valign='middle' width='98%' onmousedown='mouseDownItemMenu(@INDICE@)' onclick='itemMenuClick(@INDICE@)' ><span><ACRONYM TITLE='@MENU_TOOLTIP@' style='border: 0'>&nbsp;@MENU_CAPTION@</ACRONYM></span></td>"
	cTemplateTab1 = cTemplateTab1 + "                  </tr>"
	cTemplateTab1 = cTemplateTab1 + "                </table>"
	cTemplateTab1 = cTemplateTab1 + "              </td>"
	cTemplateTab1 = cTemplateTab1 + "            </tr>"
	cTemplateTab1 = cTemplateTab1 + "          </table>"
	cTemplateTab1 = cTemplateTab1 + "        </div>"
	cTemplateTab1 = cTemplateTab1 + "      </td>"
	cTemplateTab1 = cTemplateTab1 + "    </tr>"
	cTemplateTab1 = cTemplateTab1 + "  </table>"
	cTemplateTab1 = cTemplateTab1 + "<div>"

	return cTemplateTab1
	
}


// Adiciona um elemento do array de itens de menu na tela ------------------------
function insereItemMenuLista(objItemMenu, nItemMenu, objCamadaBig, objCamadaSmall){
		
	var cTemplate = new String();
	var cTemplateTabela = new String();
	
	// Adiciona �cones grandes	
	cTemplateTabela = templateItemMenuGrande()	
	cTemplateTabela = cTemplateTabela.replace(/@MENU_CAPTION@/g, objItemMenu.Caption)
	cTemplateTabela = cTemplateTabela.replace(/@MENU_TOOLTIP@/g, objItemMenu.ToolTip)	
	cTemplateTabela = cTemplateTabela.replace(/@ICONE_MENU@/g, objItemMenu.ItemImagePath)
	cTemplateTabela = cTemplateTabela.replace(/@INDICE@/g, nItemMenu)				
	objCamadaBig.insertAdjacentHTML("BeforeEnd", cTemplateTabela);
	
	// Adiciona �cones pequenos
	cTemplateTabela = templateItemMenuPequeno()	
	cTemplateTabela = cTemplateTabela.replace(/@MENU_CAPTION@/g, objItemMenu.Caption)
	cTemplateTabela = cTemplateTabela.replace(/@MENU_TOOLTIP@/g, objItemMenu.ToolTip)	
	cTemplateTabela = cTemplateTabela.replace(/@ICONE_MENU@/g, objItemMenu.ItemImagePath)
	cTemplateTabela = cTemplateTabela.replace(/@INDICE@/g, nItemMenu)	
	objCamadaSmall.insertAdjacentHTML("BeforeEnd", cTemplateTabela);
			
}


// Evento disparado quando pressionado o mouse sobre um item de menu --------------
function mouseDownItemMenu(nItemId) {
	
	hideMenuCss()
	
	nIdMenu = colItemMenu[nItemId].MenuIndex
	if ((!colItemMenu[nItemId].IsEnabled) || (!colMenu[nIdMenu].IsEnabled)) {
		return false
	}	
	
	if (nLastItm>-1) {
		if (nLastItm != nItemId) {
		    if (colItemMenu[nLastItm].IsEnabled) {							        
				// Trata �cones grandes
				window.document.all.item('tdEspacoItemMenu' + nLastItm).className = 'objetoItemNormal';				
				window.document.all.item('imgItemMenu' + nLastItm).className = 'objetoItemNormal';
				window.document.all.item('tdImgItemMenu' + nLastItm).className = 'objetoItemNormal';
				window.document.all.item('tdCaptionItemMenu' + nLastItm).className = 'objetoItemNormal';		
				window.document.all.item('tabInternaItemMenu' + nLastItm).className = 'objetoItemNormal';
				
				// Trata �cones pequenos
				window.document.all.item('pqtdEspacoItemMenu' + nLastItm).className = 'objetoItemNormal';				
				window.document.all.item('pqimgItemMenu' + nLastItm).className = 'objetoItemNormal';
				window.document.all.item('pqtdImgItemMenu' + nLastItm).className = 'objetoItemNormal';
				window.document.all.item('pqtdCaptionItemMenu' + nLastItm).className = 'objetoItemNormal';		
				window.document.all.item('pqtabInternaItemMenu' + nLastItm).className = 'objetoItemNormal';
				
			}
		}	
	}
	
	// Trata �cones grandes
	window.document.all.item('tdEspacoItemMenu' + nItemId).className = 'objetoItemMouseOver';
	window.document.all.item('imgItemMenu' + nItemId).className = 'objetoItemMouseOver';
	window.document.all.item('tdImgItemMenu' + nItemId).className = 'objetoItemMouseOver';
	window.document.all.item('tdCaptionItemMenu' + nItemId).className = 'objetoItemMouseOver';		
	window.document.all.item('tabInternaItemMenu' + nItemId).className = 'objetoItemBorda';
	
	// Trata �cones pequenos
	window.document.all.item('pqtdEspacoItemMenu' + nItemId).className = 'objetoItemMouseOver';
	window.document.all.item('pqimgItemMenu' + nItemId).className = 'objetoItemMouseOver';
	window.document.all.item('pqtdImgItemMenu' + nItemId).className = 'objetoItemMouseOver';
	window.document.all.item('pqtdCaptionItemMenu' + nItemId).className = 'objetoItemMouseOver';		
	window.document.all.item('pqtabInternaItemMenu' + nItemId).className = 'objetoItemBorda';
	
	nLastItm = nItemId;
	
	return false
	
}

// Evento disparado quando clicado sobre um bot�o ---------------------------------
function itemMenuClick(nItemId) {
	
	hideMenuCss()
	
	nIdMenu = colItemMenu[nItemId].MenuIndex
	if ((!colItemMenu[nItemId].IsEnabled) || (!colMenu[nIdMenu].IsEnabled)) {
		return false
	}	
	
	// O M�todo abaixo deve ser implantado na p�gina que teve o menu inclu�do
	nLastItmClick = nItemId
	cliqueItemMenuVertical(nItemId)
	
	return false
	
}

// Define novo Caption para um item do menu ---------------------------------------
function setCaptionItemMenu(strTipo, nItemId, strCaption) {

	if (strTipo=='MENU') {	
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')			
			return false
		}
	
		strOldCaption =colMenu[nItemId].Caption
		colMenu[nItemId].Caption = strCaption
	
		objMenu = window.document.all.item('tdBotaoCaption' + nItemId)
		objMenu.innerHTML = "<span><ACRONYM TITLE='" + colMenu[nItemId].ToolTip + "' style='border: 0'>" + colMenu[nItemId].Caption + "</ACRONYM></span>"	
		itemMenuCaptionChanged(strTipo, nItemId, strOldCaption, strCaption)
		return false
		
	}
	else {
		if (strTipo=='ITEM') {
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}
	
			strOldCaption = colItemMenu[nItemId].Caption
			colItemMenu[nItemId].Caption = strCaption
			
			// Trata �cones Grandes
			objItMenu = window.document.all.item('tdCaptionItemMenu' + nItemId)
			objItMenu.innerHTML = "<span><ACRONYM TITLE='" + colItemMenu[nItemId].ToolTip + "' style='border: 0'>" + colItemMenu[nItemId].Caption + "</ACRONYM></span>"
			
			// Trata �cones Pequenos
			objItMenu = window.document.all.item('pqtdCaptionItemMenu' + nItemId)
			objItMenu.innerHTML = "<span><ACRONYM TITLE='" + colItemMenu[nItemId].ToolTip + "' style='border: 0'>" + colItemMenu[nItemId].Caption + "</ACRONYM></span>"	
				
			itemMenuCaptionChanged(strTipo, nItemId, strOldCaption, strCaption)
			return false
		}
	}
}


// Define novo ToolTip para um item do menu ---------------------------------------
function setToolTipItemMenu(strTipo, nItemId, strToolTip) {
	
	if (strTipo=='MENU') {	
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}
	
		strOldToolTip = colMenu[nItemId].ToolTip
		colMenu[nItemId].ToolTip = strToolTip
	
		objMenu = window.document.all.item('tdBotaoCaption' + nItemId)
		objMenu.innerHTML = "<span><ACRONYM TITLE='" + colMenu[nItemId].ToolTip + "' style='border: 0'>" + colMenu[nItemId].Caption + "</ACRONYM></span>"	
		itemMenuToolTipChanged(strTipo, nItemId, strOldToolTip, strToolTip)
		return false
		
	}
	else {
		if (strTipo=='ITEM') {			
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}
	
			strOldToolTip = colItemMenu[nItemId].ToolTip
			colItemMenu[nItemId].ToolTip = strToolTip
			
			// Trata �cones grandes
			objItMenu = window.document.all.item('tdCaptionItemMenu' + nItemId)
			objItMenu.innerHTML = "<span><ACRONYM TITLE='" + colItemMenu[nItemId].ToolTip + "' style='border: 0'>" + colItemMenu[nItemId].Caption + "</ACRONYM></span>"	
			objImgItem = window.document.all.item('imgItemMenu' + nItemId)
			objImgItem.alt = colItemMenu[nItemId].ToolTip
    
			// Trata �cones Pequenos
			objItMenu = window.document.all.item('pqtdCaptionItemMenu' + nItemId)
			objItMenu.innerHTML = "<span><ACRONYM TITLE='" + colItemMenu[nItemId].ToolTip + "' style='border: 0'>" + colItemMenu[nItemId].Caption + "</ACRONYM></span>"	
			objImgItem = window.document.all.item('pqimgItemMenu' + nItemId)
			objImgItem.alt = colItemMenu[nItemId].ToolTip
				
			itemMenuToolTipChanged(strTipo, nItemId, strOldToolTip, strToolTip)
			return false
		}
	}
}


// Define se o menu est� Enabled / Disabled ---------------------------------------
function setItemMenuEnabled(strTipo, nItemId, bolEnabled) {
	
	if (strTipo=='MENU') {
	
		// Trata menu (Bot�o)
		
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}
	
		bolOldEnabled = colMenu[nItemId].IsEnabled
		colMenu[nItemId].IsEnabled = bolEnabled
	
		objMenu = window.document.all.item('tabBotao' + nItemId)
	
		if (bolEnabled) {
			objMenu.className = 'objetoBotaoNormal'
		}
		else {
			objMenu.className = 'objetoBotaoNormalDisabled'
		}
		
		// Trata �cones Grandes
		for (nLoop=0; nLoop<=nItensMenu-1; nLoop++) {						
			if (colItemMenu[nLoop].MenuIndex == nItemId) {
				
				objItMenu = window.document.all.item('tdCaptionItemMenu' + nLoop)
				objImgItem = window.document.all.item('imgItemMenu' + nLoop)
				objTdImgItem =  window.document.all.item('tdImgItemMenu' + nLoop)
				objItEspaco = window.document.all.item('tdEspacoItemMenu' + nLoop)
				objtdInterna = window.document.all.item('tabInternaItemMenu' + nLoop)
			
			
				if (!bolEnabled) {
					objtdInterna.className = 'objetoItemNormalDisabled'
					objItEspaco.className = 'objetoItemNormalDisabled'
					objItMenu.className = 'objetoItemNormalDisabled'
					objImgItem.className = 'objetoItemNormalDisabled'
					objTdImgItem.className = 'objetoItemNormalDisabled'				
					objImgItem.src = colItemMenu[nLoop].DisabledItemImagePath
				}
				else {
					if (colItemMenu[nLoop].IsEnabled) {
						objtdInterna.className = 'objetoItemNormal'
						objItEspaco.className = 'objetoItemNormal'
						objItMenu.className = 'objetoItemNormal'
						objImgItem.className = 'objetoItemNormal'
						objTdImgItem.className = 'objetoItemNormal'
						objImgItem.src = colItemMenu[nLoop].ItemImagePath					
					}
				}
				
			}
		}
		
		
		
		// Trata �cones Pequenos
		for (nLoop=0; nLoop<=nItensMenu-1; nLoop++) {						
			if (colItemMenu[nLoop].MenuIndex == nItemId) {
				
				objItMenu = window.document.all.item('pqtdCaptionItemMenu' + nLoop)
				objImgItem = window.document.all.item('pqimgItemMenu' + nLoop)
				objTdImgItem =  window.document.all.item('pqtdImgItemMenu' + nLoop)
				objItEspaco = window.document.all.item('pqtdEspacoItemMenu' + nLoop)
				objtdInterna = window.document.all.item('pqtabInternaItemMenu' + nLoop)
			
			
				if (!bolEnabled) {
					objtdInterna.className = 'objetoItemNormalDisabled'
					objItEspaco.className = 'objetoItemNormalDisabled'
					objItMenu.className = 'objetoItemNormalDisabled'
					objImgItem.className = 'objetoItemNormalDisabled'
					objTdImgItem.className = 'objetoItemNormalDisabled'				
					objImgItem.src = colItemMenu[nLoop].DisabledItemImagePath
				}
				else {
					if (colItemMenu[nLoop].IsEnabled) {
						objtdInterna.className = 'objetoItemNormal'
						objItEspaco.className = 'objetoItemNormal'
						objItMenu.className = 'objetoItemNormal'
						objImgItem.className = 'objetoItemNormal'
						objTdImgItem.className = 'objetoItemNormal'
						objImgItem.src = colItemMenu[nLoop].ItemImagePath					
					}
				}
				
			}
		}
		
			
		itemMenuEnabledChanged(strTipo, nItemId, bolOldEnabled, bolEnabled)	
		return false
	}
	else {
		if (strTipo=='ITEM') {	
			
			// Trata Item (�cone)
			
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}
	
			bolOldEnabled = colItemMenu[nItemId].IsEnabled	
			colItemMenu[nItemId].IsEnabled = bolEnabled
			
			// Trata �cones grandes
			objItMenu = window.document.all.item('tdCaptionItemMenu' + nItemId)
			objImgItem = window.document.all.item('imgItemMenu' + nItemId)
			objTdImgItem =  window.document.all.item('tdImgItemMenu' + nItemId)
			objItEspaco = window.document.all.item('tdEspacoItemMenu' + nItemId)
			objtdInterna = window.document.all.item('tabInternaItemMenu' + nItemId)
				
			if (bolEnabled) {		
				objtdInterna.className = 'objetoItemNormal'
				objItEspaco.className = 'objetoItemNormal'
				objItMenu.className = 'objetoItemNormal'
				objImgItem.className = 'objetoItemNormal'
				objTdImgItem.className = 'objetoItemNormal'
				objImgItem.src = colItemMenu[nItemId].ItemImagePath		
		
				if (nItemId == nLastBtnClick) {
					objItEspaco.className = 'objetoItemMouseOver';			
					objImgItem.className = 'objetoItemMouseOver';
					objItMenu.className = 'objetoItemMouseOver';		
					objTdImgItem.className = 'objetoItemMouseOver'
					objtdInterna.className = 'objetoItemBorda';	
				}
		
			}
			else {
				objtdInterna.className = 'objetoItemNormalDisabled'
				objItEspaco.className = 'objetoItemNormalDisabled'
				objItMenu.className = 'objetoItemNormalDisabled'
				objImgItem.className = 'objetoItemNormalDisabled'
				objTdImgItem.className = 'objetoItemNormalDisabled'
				objImgItem.src = colItemMenu[nItemId].DisabledItemImagePath
			}
			
			
			// Trata �cones Pequenos
			objItMenu = window.document.all.item('pqtdCaptionItemMenu' + nItemId)
			objImgItem = window.document.all.item('pqimgItemMenu' + nItemId)
			objTdImgItem =  window.document.all.item('pqtdImgItemMenu' + nItemId)
			objItEspaco = window.document.all.item('pqtdEspacoItemMenu' + nItemId)
			objtdInterna = window.document.all.item('pqtabInternaItemMenu' + nItemId)
				
			if (bolEnabled) {		
				objtdInterna.className = 'objetoItemNormal'
				objItEspaco.className = 'objetoItemNormal'
				objItMenu.className = 'objetoItemNormal'
				objImgItem.className = 'objetoItemNormal'
				objTdImgItem.className = 'objetoItemNormal'
				objImgItem.src = colItemMenu[nItemId].ItemImagePath		
		
				if (nItemId == nLastBtnClick) {
					objItEspaco.className = 'objetoItemMouseOver';			
					objImgItem.className = 'objetoItemMouseOver';
					objItMenu.className = 'objetoItemMouseOver';		
					objTdImgItem.className = 'objetoItemMouseOver'
					objtdInterna.className = 'objetoItemBorda';	
				}
		
			}
			else {
				objtdInterna.className = 'objetoItemNormalDisabled'
				objItEspaco.className = 'objetoItemNormalDisabled'
				objItMenu.className = 'objetoItemNormalDisabled'
				objImgItem.className = 'objetoItemNormalDisabled'
				objTdImgItem.className = 'objetoItemNormalDisabled'
				objImgItem.src = colItemMenu[nItemId].DisabledItemImagePath
			}
			
			
			itemMenuEnabledChanged(strTipo, nItemId, bolOldEnabled, bolEnabled)	
			return false
		}
	}
}




// Define se o Menu / Item est� Visible / Not Visible ----------------------------------
function setItemMenuVisible(strTipo, nItemId, bolVisible) {
	
	if (strTipo=='MENU') {
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}
	
		bolOldVisible = colMenu[nItemId].IsVisible
		colMenu[nItemId].IsVisible = bolVisible
				
		objMenu = window.document.all.item('tabBotao' + nItemId)	
		if (bolVisible) {
			objMenu.style.display = "inline";		
			if (nLastBtnClick==nItemId) {
				window.document.all.item('layItemBotao' + nItemId).style.display = "inline";
			}
		}
		else {		
			objMenu.style.display = "none";
			window.document.all.item('layItemBotao' + nItemId).style.display = "none";		
		}
		
		// Se o menu ativo for o que est� sendo oculto
		// Procura pelo pr�ximo abaixo para tornar-Ativo
		bOk=false
		if (!bolVisible) {
		
			// Procura abaixo pelo pr�ximo menu vis�vel
			for (nLoop=nItemId; nLoop<=nMenus-1; nLoop++) {			
				if (colMenu[nLoop].IsVisible) {				
					setItemMenuAtivo('MENU', nLoop, divMenuCont.Height)
					bOk = true
					break;
				}
			}	
		
			// Se n�o encontrou nenhum menu vis�vel abaixo ent�o procura acima
			if (!bOk) {
				for (nLoop=nItemId; nLoop >=0; nLoop--) {
					if (colMenu[nLoop].IsVisible) {
						setItemMenuAtivo('MENU', nLoop, divMenuCont.Height)					
						break;
					}
				}
			}
		}
		else {
			setItemMenuAtivo('MENU', nItemId, divMenuCont.Height)
		}
		
		itemMenuVisibleChanged(strTipo, nItemId, bolOldVisible, bolVisible)	
		return false
	}
	else {
		if (strTipo=='ITEM') {
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}
	
			bolOldVisible = colItemMenu[nItemId].IsVisible
			colItemMenu[nItemId].IsVisible = bolVisible
	
			// Trata �cones Grandes
			objItMenu = window.document.all.item('layItemMenu' + nItemId)	
			if (bolVisible) {
				objItMenu.style.display = "inline";		
				if (nLastBtnClick==nItemId) {
					//
				}
			}
			else {		
				objItMenu.style.display = "none";		
			}
	
	
			// Trata �cones Pequenos
			objItMenu = window.document.all.item('pqlayItemMenu' + nItemId)	
			if (bolVisible) {
				objItMenu.style.display = "inline";		
				if (nLastBtnClick==nItemId) {
					//
				}
			}
			else {		
				objItMenu.style.display = "none";		
			}
			
			
			// Se o menu ativo for o que est� sendo oculto
			// Procura pelo pr�ximo abaixo para tornar-Ativo
			bOk=false
			if (!bolVisible) {
				
				// Procura abaixo pelo pr�ximo menu vis�vel
				for (nLoop=nItemId; nLoop<=nItensMenu-1; nLoop++) {			
					if ((colItemMenu[nLoop].IsVisible) && (colItemMenu[nLoop].IsEnabled)) {				
						//setItemMenuAtivo(nLoop, divMenuCont.Height)
						bOk = true
						break;
					}
				}	
				
				// Se n�o encontrou nenhum menu vis�vel abaixo ent�o procura acima
				if (!bOk) {
					for (nLoop=nItemId; nLoop >=0; nLoop--) {
						if ((colItemMenu[nLoop].IsVisible) && (colItemMenu[nLoop].IsEnabled)) {	
							//setItemMenuAtivo(nLoop)					
							break;
						}
					}
				}
			}
			else {
				//setItemMenuAtivo(nItemId)
			}
			itemMenuVisibleChanged(strTipo, nItemId, bolOldVisible, bolVisible)	
			return false
		}
	}
}

// Obt�m o total de Menus / Itens
function getCount(strTipo) {

	if (strTipo=='MENU') {					
		return nMenus
		
	}
	else {		
		if (strTipo=='ITEM') {
			return nItensMenu			
		}
	}
}





// Obt�m os caption de um Menu / Item
function getCaption(strTipo, nItemId) {

	if (strTipo=='MENU') {	
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}		
		return colMenu[nItemId].Caption		
	}
	else {
		if (strTipo=='ITEM') {
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}			
			return colItemMenu[nItemId].Caption 
			
		}
	}
}




// Obt�m os ToolTip de um Menu / Item
function getToolTip(strTipo, nItemId) {

	if (strTipo=='MENU') {	
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}		
		return colMenu[nItemId].ToolTip		
	}
	else {
		if (strTipo=='ITEM') {
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}			
			return colItemMenu[nItemId].ToolTip 
			
		}
	}
}




// Verifica se um Menu / Item est� habilitado
function getIsEnabled(strTipo, nItemId) {

	if (strTipo=='MENU') {	
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}		
		return colMenu[nItemId].IsEnabled		
	}
	else {
		if (strTipo=='ITEM') {
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}
			
			return colItemMenu[nItemId].IsEnabled
			
		}
	}
}




// Verifica se um Menu / Item est� vis�vel
function getIsVisible(strTipo, nItemId) {

	if (strTipo=='MENU') {	
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}		
		return colMenu[nItemId].IsVisible
	}
	else {
		if (strTipo=='ITEM') {
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}
			
			return colItemMenu[nItemId].IsVisible
			
		}
	}
}





function setImagesPath(nItemId, strPathEnabled, strPathDisabled) {

	if (nItemId<0) {
		alert('�ndice [' + nItemId + '] inferior a 0')
		return false
	} 
	
	if (nItemId>=nItensMenu) {
		alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
		return false
	}
	
	strOldPathEnabled = colItemMenu[nItemId].ItemImagePath
	strOldPathDisabled = colItemMenu[nItemId].DisabledItemImagePath
	
	// Trata �cones Grandes			
	objImgItem = window.document.all.item('imgItemMenu' + nItemId)	
	colItemMenu[nItemId].ItemImagePath=strPathEnabled
	colItemMenu[nItemId].DisabledItemImagePath=strPathDisabled
	
	nIdMenu = colItemMenu[nItemId].MenuIndex
	if ((!colItemMenu[nItemId].IsEnabled) || (!colMenu[nIdMenu].IsEnabled)) {
		objImgItem.src = strPathDisabled
	}
	else {
		objImgItem.src = strPathEnabled
	}	
	
	
	// Trata �cones Pequenos
	objImgItem = window.document.all.item('pqimgItemMenu' + nItemId)	
	colItemMenu[nItemId].ItemImagePath=strPathEnabled
	colItemMenu[nItemId].DisabledItemImagePath=strPathDisabled
	
	nIdMenu = colItemMenu[nItemId].MenuIndex
	if ((!colItemMenu[nItemId].IsEnabled) || (!colMenu[nIdMenu].IsEnabled)) {
		objImgItem.src = strPathDisabled
	}
	else {
		objImgItem.src = strPathEnabled
	}	
		
	itemMenuImagesPathChanged(nItemId, strOldPathEnabled, strOldPathDisabled, strPathEnabled, strPathDisabled)
	
	return false

}

// Seta o estilo de visualiza��o dos �cones (Grandes / Pequenos)
function setIconesPequenos(bPequeno) {
	
	hideMenuCss()
	
	window.document.all.item('imgBigIcos').src = '../images/mvert/btn_grande_alto.gif'
	window.document.all.item('imgSmallIcos').src = '../images/mvert/btn_pequeno_alto.gif'
	
	bolSmallIcos = bPequeno
	if (bPequeno) {
		window.document.all.item('imgSmallIcos').src = '../images/mvert/btn_pequeno_baixo.gif'
		if 	(nLastBtnClick>-1) {
			window.document.all.item('layItemBotao' + nLastBtnClick).style.display = "none";
			window.document.all.item('pqlayItemBotao' + nLastBtnClick).style.display = "inline";
		}
	}
	else {	
		window.document.all.item('imgBigIcos').src = '../images/mvert/btn_grande_baixo.gif'	
		if 	(nLastBtnClick>-1) {
			window.document.all.item('pqlayItemBotao' + nLastBtnClick).style.display = "none";	
			window.document.all.item('layItemBotao' + nLastBtnClick).style.display = "inline";
		}
	}
			
}

function showMenuCss() {
	
	if (!window.document.all.item('divEstiloCss')) {
		strMenu = ""
		strMenu = strMenu + "<div name='divEstiloCss' id='divEstiloCss' style='position:absolute; left:3px; top:20px; width:88px; height:100px; z-index:10; visibility: visible' class='menuvertBackColor'>" 
		strMenu = strMenu + "   <table width='100%' border='0' cellspacing='0' cellpadding='0' class='objetoItemNormal'>"
		strMenu = strMenu + "   <tr> "
		strMenu = strMenu + "   <td name='DefCss1' id='DefCss1' onmouseover='selectItemCss(1)' onclick='setNewCss(1)' class='objetoItemNormal' >Plusoft</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   <tr> "
		strMenu = strMenu + "   <td name='DefCss2' id='DefCss2' onmouseover='selectItemCss(2)' onclick='setNewCss(2)' class='objetoItemNormal'>Cinza</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   <tr> "
		strMenu = strMenu + "   <td name='DefCss3' id='DefCss3' onmouseover='selectItemCss(3)' onclick='setNewCss(3)' class='objetoItemNormal'>P&ecirc;ssego</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   <tr> "
		strMenu = strMenu + "   <td name='DefCss4' id='DefCss4' onmouseover='selectItemCss(4)' onclick='setNewCss(4)' class='objetoItemNormal'>Marrom</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   <tr> "
		strMenu = strMenu + "   <td name='DefCss5' id='DefCss5' onmouseover='selectItemCss(5)' onclick='setNewCss(5)' class='objetoItemNormal'>Dia_Chuvoso</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   <tr> "
		strMenu = strMenu + "   <td name='DefCss6' id='DefCss6' onmouseover='selectItemCss(6)' onclick='setNewCss(6)' class='objetoItemNormal'>Caramelo</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   <tr> "
		strMenu = strMenu + "   <td name='DefCss7' id='DefCss7' onmouseover='selectItemCss(7)' onclick='setNewCss(7)' class='objetoItemNormal'>Azul</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   <tr>" 
		strMenu = strMenu + "   <td name='DefCss8' id='DefCss8' onmouseover='selectItemCss(8)' onclick='setNewCss(8)' class='objetoItemNormal'>Uva</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   <tr> "
		strMenu = strMenu + "   <td name='DefCss9' id='DefCss9' onmouseover='selectItemCss(9)' onclick='setNewCss(9)' class='objetoItemNormal'>Vermelho</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   <tr>" 
		strMenu = strMenu + "   <td name='DefCss10' id='DefCss10' onmouseover='selectItemCss(10)' onclick='setNewCss(10)' class='objetoItemNormal'>Verde</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   <tr>"
		strMenu = strMenu + "   <td name='DefCss11' id='DefCss11' onmouseover='selectItemCss(11)' onclick='setNewCss(11)' class='objetoItemNormal'>Preto_Branco</td>"
		strMenu = strMenu + "   </tr>"
		strMenu = strMenu + "   </table>"
		strMenu = strMenu + "   </div>"
	
		window.document.body.insertAdjacentHTML("BeforeEnd", strMenu);
		
		window.document.all.item('divEstiloCss').style.display = "inline";
	}
	else {
		objSelCss = window.document.all.item('divEstiloCss')
		if (objSelCss.style.display == "inline") {			
			objSelCss.style.display = "none";
		}
		else {
			objSelCss.style.display = "inline";
		}
	}

}

function setNewCss(nIdCss) {
	
	strCssPath='mvert_Azul'
	if (nIdCss==1) {
		strCssPath='mvert_Plusoft'
	}
	if (nIdCss==2) {
		strCssPath='mvert_Cinza'
	}
	if (nIdCss==3) {
		strCssPath='mvert_Pessego'
	}
	if (nIdCss==4) {
		strCssPath='mvert_Marrom'
	}
	
	if (nIdCss==5) {
		strCssPath='mvert_Dia_Chuvoso'
	}
	
	if (nIdCss==6) {
		strCssPath='mvert_Caramelo'
	}
	if (nIdCss==7) {
		strCssPath='mvert_Azul'
	}
	if (nIdCss==8) {
		strCssPath='mvert_Uva'
	}
	if (nIdCss==9) {
		strCssPath='mvert_Vermelho'
	}
	if (nIdCss==10) {
		strCssPath='mvert_Verde'
	}
	if (nIdCss==11) {
		strCssPath='mvert_Preto_Branco'
	}
	
	strCssPath = 'css/' + strCssPath + '.css'
	
	window.document.getElementsByTagName('link').item('lnkCss').href = strCssPath
	showMenuCss()
	
	return false
	
}

function selectItemCss(nItemCss) {
	objLast = window.document.all.item('DefCss' + nLastCss)
	objLast.className = 'objetoItemNormal';
	
	objAtual = window.document.all.item('DefCss' + nItemCss)	
	objAtual.className = 'defineCssMouseOver';
	
	nLastCss = nItemCss;
	
}	

function hideMenuCss() {
	
	if (window.document.all.item('divEstiloCss')) {
		window.document.all.item('divEstiloCss').style.display = "none";
	}
	return false
}

// ***************** Final do tramento dos itens de menu ***************************


function setTag(strTipo, nItemId, nItemTag,  strTag) {
	
	if (strTipo=='MENU') {	
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}		
		if (nItemTag==1) {
			strOldTag = colMenu[nItemId].Tag1
			colMenu[nItemId].Tag1 = strTag
		}
		else {
			strOldTag = colMenu[nItemId].Tag2
			colMenu[nItemId].Tag2 = strTag
		}
	}
	else {
		if (strTipo=='ITEM') {
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}
			
			if (nItemTag==1) {
				strOldTag = colItemMenu[nItemId].Tag1
				colItemMenu[nItemId].Tag1 = strTag
			}			
			else {
				strOldTag = colItemMenu[nItemId].Tag2
				colItemMenu[nItemId].Tag2 = strTag
			}					
		}
	}
	
	itemMenuTagChanged(strTipo, nItemId, nItemTag, strOldTag, strTag)
}

function getTag(strTipo, nItemId, nItemTag) {

	if (strTipo=='MENU') {	
		if (nItemId<0) {
			alert('�ndice [' + nItemId + '] inferior a 0')
			return false
		} 
	
		if (nItemId>=nMenus) {
			alert('�ndice [' + nItemId + '] superior ao n�mero de menus [' + (nMenus) + ']')
			return false
		}		
		
		if (nItemTag==1) {			
			return colMenu[nItemId].Tag1
		}
		else {
			return colMenu[nItemId].Tag2
		}
	}
	else {
		if (strTipo=='ITEM') {
			if (nItemId<0) {
				alert('�ndice [' + nItemId + '] inferior a 0')
				return false
			} 
	
			if (nItemId>=nItensMenu) {
				alert('�ndice [' + nItemId + '] superior ao n�mero de itens [' + (nItensMenu) + ']')
				return false
			}
			
			if (nItemTag==1) {				
				return colItemMenu[nItemId].Tag1
			}
			else {
				return colItemMenu[nItemId].Tag2
			}					
		}
	}
}


// Obtem um elemento do menu pelo C�digo -------------------------------------------
function getItemMenuByCodigo(strTipo, nCodigo) { 
 
	if (strTipo=='MENU') {
		for (nLoop=0; nLoop < nMenus; nLoop++) {			
			if (colMenu[nLoop].Codigo == nCodigo) {				
				nIdMenuAtual = nLoop
				return  colMenu[nLoop]
				break
			}
		}
		return false				
	}
	else {
		if (strTipo=='ITEM') {			
			for (nLoop=0; nLoop < nItensMenu; nLoop++) {
				if (colItemMenu[nLoop].Codigo == nCodigo) {
					nIdItemAtual = nLoop
					return  colItemMenu[nLoop]
					break
				}
			}				
			return false
		}
	}	
}

// Final do arquivo